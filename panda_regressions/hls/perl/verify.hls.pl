eval 'exec perl -S $0 ${1+"$@"}'
	if 0;

# PandA perl script used for regression testing.
# Inspired by the work of Martin Janssen, Synopsys, Inc., 2001-01-18
# for the PandA regression test

# use calling directory for include path
$0 =~ m|^(.*)/|;
push( @INC, $1 );

# set the path to the test suite
&set_panda_test( $1 );

# flush STDOUT after each print
$oldfh = select( STDOUT ); $| = 1; select( $oldfh );

&main;


# -----------------------------------------------------------------------------
#  SUB : set_panda_test
#
#  Set the path to the test suite, either specified by environment variable
#  PANDA_TEST, or derived from the location of this script.
# -----------------------------------------------------------------------------

sub set_panda_test
{
    local( $dir ) = @_;
    if( defined $ENV{ 'PANDA_TEST' } ) {
  $rt_panda_test = $ENV{ 'PANDA_TEST' };
    }
    else {
  local( $wdir );
  chop( $wdir = `pwd` );
  chdir( "$dir/../" );
  chop( $rt_panda_test = `pwd` );
  chdir( "$wdir" );
    }
}


# -----------------------------------------------------------------------------
#  SUB : print_log
#
#  Print routine used instead of 'print' as it allows dupping to output file
#  ($rt_output_file).
# -----------------------------------------------------------------------------

sub print_log
{
    print STDOUT @_;

    if( defined $rt_output_file ) {
  if( ! defined $RT_OUTPUT_FH ) {
     local( $f ) = "$rt_output_dir/$rt_output_file";
     $RT_OUTPUT_FH = $f;
     open( RT_OUTPUT_FH, ">$f" ) ||
    die "Error: cannot open output file '$f'\n";
     # chmod( $rt_file_permissions, "$f" );
  }
  print RT_OUTPUT_FH @_;
    }
}



# -----------------------------------------------------------------------------
#  SUB : compile_results
#
#  Print summary of passing and failing tests.
# -----------------------------------------------------------------------------

sub compile_results
{
    &print_log( "\n*** test results\n" );

    &print_log( "\n" );
    &print_log( "---------------------------------------" .
                "----------------------------------------\n" );
    &print_log( " Tests that passed :\n" );
    &print_log( "---------------------------------------" .
                "----------------------------------------\n" );
    foreach $tmp ( @rt_pass ) {
      &print_log( "$tmp\n" );
    }

    &print_log( "\n" );
    &print_log( "---------------------------------------" .
                "----------------------------------------\n" );
    &print_log( " Tests that failed :\n" );
    &print_log( "---------------------------------------" .
                "----------------------------------------\n" );
    foreach $tmp ( @rt_fail ) {
      &print_log( "$tmp\n" );
    }

    local( $pass ) = $#rt_pass + 1;
    local( $fail ) = $#rt_fail + 1;

    &print_log( "\n" );
    &print_log( "Total passed : " . ( $pass ) . "\n" );
    &print_log( "Total failed : " . ( $fail ) . "\n" );

    use integer;
    local( $gut ) = ( $pass * 100 ) / ( $pass + $fail );

    &print_log( "\n" );
    &print_log( "   GUT METER : " . ( $gut ) . " %\n" );

    local( $script );
    ( $script = $0 ) =~ s|^.*/||;
    local( $host );
    chop( $host = `hostname` );
    local( $uname_a );
    chop( $uname_a = `uname -a` );
    local( $date );
    chop( $date = `date` );

    &print_log( "\n*** $script finished successfully on host $host\n\n" );
    &print_log( "$uname_a\n" );
    &print_log( "$date\n" );
    &print_log( "\n" );
}


# -----------------------------------------------------------------------------
#  SUB : interrupt_handler
#
#  Routine for handling the SIGINT and SIGQUIT signals.
# -----------------------------------------------------------------------------

sub interrupt_handler
{
    local( $signal ) = @_;

    &print_log( "caught SIG$signal\n" );

    if( defined $rt_child_pid ) {
      &print_log( "killing process '$rt_child_pid'\n" );
      kill -9, getpgrp($rt_child_pid);
    }

    &compile_results;

    exit 1;
}


# -----------------------------------------------------------------------------
#  SUB : alarm_handler
#
#  Routine for handling the SIGALRM signal.
# -----------------------------------------------------------------------------

sub alarm_handler
{
    local( $signal ) = @_;

    &print_log( "caught SIG$signal\n" );

    # kill process and group of processes
    &print_log( "timing out process '$rt_child_pid'\n" );
    kill -9, getpgrp($rt_child_pid);

    # keep track of timed out tests
    push( @rt_alarm, $rt_current_test );

    # reset the SIGALRM signal handler - needed for Solaris, HP
    $SIG{ 'ALRM' } = 'alarm_handler';
}


# -----------------------------------------------------------------------------
#  SUB : get_panda_tool
#
#  Get the PANDA_TOOL environment variable's value.
# -----------------------------------------------------------------------------

sub get_panda_tool
{
    if( ! defined $ENV{ 'PANDA_TOOL' } ) {
        &print_log( "Error: " .
      "environment variable PANDA_TOOL is not defined!\n" );
  exit 1;
    }

    $ENV{ 'PANDA_TOOL' };
}

# -----------------------------------------------------------------------------
#  SUB : get_panda_tool_option
#
#  Get the PANDA_TOOL environment variable's value.
# -----------------------------------------------------------------------------

sub get_panda_tool_option
{
    if( ! defined $ENV{ 'PANDA_TOOL_OPTION' } ) {
        &print_log( "Error: " .
      "environment variable PANDA_TOOL_OPTION is not defined!\n" );
  exit 1;
    }

    $ENV{ 'PANDA_TOOL_OPTION' };
}

# -----------------------------------------------------------------------------
#  SUB : get_CC_bin
#
#  Get the CC environment variable's value.
# -----------------------------------------------------------------------------

sub get_cc_bin
{
    if( ! defined $ENV{ 'CC' } ) {
        &print_log( "Error: " .
      "environment variable CC is not defined!\n" );
  exit 1;
    }

    $ENV{ 'CC' };
}

# -----------------------------------------------------------------------------
#  SUB : get_panda_arch
#
#  Get the target architecture.
# -----------------------------------------------------------------------------

sub get_panda_arch
{
    local( $uname_s ) = `uname -s`;
    local( $uname_r ) = `uname -r`;
    local( $arch );
    local( $cc );

    chop( $uname_s );
    chop( $uname_r );

    $cc = &get_cc_bin;

    if( $uname_s eq "SunOS" ) {
      $arch = "gccsparcOS5";
    } elsif( $uname_s eq "HP-UX" ) {
      $arch = "gcchpux11";
    } elsif( $uname_s eq "Darwin" ) {
      $arch = "macosx";
    } elsif( $uname_s eq "Linux" ) {
      $arch = "linux";
    } elsif( $uname_s eq "CYGWIN_NT-4.0" ) {
      $arch = "cygwin";
    } elsif( $uname_s eq "MINGW32_NT-5.1") {
      $arch = "mingw";
    } else {
      die "Error: unsupported architecture '$uname_s $uname_r'\n";
    }

    $rt_cc = $cc;
    $arch;
}


# -----------------------------------------------------------------------------
#  SUB : init_globals
#
#  Initialization routine.
# -----------------------------------------------------------------------------

sub init_globals
{
  # setup signal handlers
  $SIG{ 'INT' }  = 'interrupt_handler';
  $SIG{ 'QUIT' } = 'interrupt_handler';
  $SIG{ 'ALRM' } = 'alarm_handler';

  $rt_panda_tool = &get_panda_tool;
  $rt_panda_tool_option = &get_panda_tool_option;
  $rt_panda_arch = &get_panda_arch;

  $rt_cleanup = 1;                    # cleanup temp dirs by default
  $rt_max_file_size = 5000000;         # limit the max size of the output file
  chop( $rt_output_dir = `pwd` );     # directory for output logs
  $rt_output_dir = "$rt_output_dir/out";
  &create_dir("$rt_output_dir");
  $rt_tests_dir = "$rt_panda_test";
  $rt_time_tests = 0;
  $rt_verbose  = 0;

  $rt_opts = '';                      # options to use for compile
  if( defined $ENV{ 'RT_OPTS' } ) {
    $rt_opts = $ENV{ 'RT_OPTS' };
  }

  $rt_simulate_options = '"--simulate=$linkdir/test.xml"';                      # additional tool options
  if( defined $ENV{ 'RT_SIMULATE_OPTS' } ) {
    $rt_simulate_options = $ENV{ 'RT_SIMULATE_OPTS' };
  }

  $rt_tool_options = '"--benchmark-name=$linkdir"';                      # additional tool options
  if( defined $ENV{ 'RT_TOOL_OPTS' } ) {
    $rt_tool_options = $ENV{ 'RT_TOOL_OPTS' };
  }

  $rt_output_file = "verify.log";     # copy output to file
  $rt_props = 0;                      # global properties (from args/config)
  $rt_diag = 0;                       # diag mode
  $rt_timeout = 5;                    # timeout in min (zero for off)
  %rt_testlist = ();

  # set the property file names
  $rt_dontrun = "DONTRUN";
  $rt_compile = "COMPILE";
  $rt_ig_retval = "IG_RETVAL";

  # file and directory permissions
  $rt_file_permissions = 0666;

  $ENV{ 'PANDA_REGRESSION' } = 1;

  # Set compiler and compiler flags
  if( $rt_panda_arch eq "gccsparcOS5" ) {
    $rt_ccflags = "-Wall -lm";
    $rt_ld = $rt_cc;
    $rt_ldflags = $rt_ccflags;
    $rt_debug_flag = "-g";
    $rt_optimize_flag = "-O";
  } elsif( $rt_panda_arch eq "gcchpux11" ) {
    $rt_ccflags = "-Wall -lm";
    $rt_ld = $rt_cc;
    $rt_ldflags = $rt_ccflags;
    $rt_debug_flag = "-g";
    $rt_optimize_flag = "-O";
  } elsif( $rt_panda_arch eq "linux" ) {
    $rt_ccflags = "-Wall -lm -fopenmp";
    $rt_ld = $rt_cc;
    $rt_ldflags = $rt_ccflags;
    $rt_debug_flag = "-g";
    $rt_optimize_flag = "-O";
  } elsif( $rt_panda_arch eq "macosx" ) {
    $rt_ccflags = "-Wall -lm";
    $rt_ld = $rt_cc;
    $rt_ldflags = $rt_ccflags;
    $rt_debug_flag = "-g";
    $rt_optimize_flag = "-O";
  } elsif( $rt_panda_arch eq "cygwin" ) {
    $rt_ccflags = "-Wall -lm";
    $rt_ld = $rt_cc;
    $rt_ldflags = $rt_ccflags;
    $rt_debug_flag = "-g";
    $rt_optimize_flag = "-O";
  } elsif( $rt_panda_arch eq "mingw" ) {
    $rt_ccflags = "-Wall -lm";
    $rt_ld = $rt_cc;
    $rt_ldflags = $rt_ccflags;
    $rt_debug_flag = "-g";
    $rt_optimize_flag = "-O";
  }


  # Defines for properties
  %rt_test_props = ( 'comp_only',  0x1,
                    'ig_retval',  0x2,'panda_only',  0x3,
                    'dontrun',    0x20,   # dir marked dontrun or 
                                          #   is not for this arch
  );


  %rt_test_type = ( 'c', 'c',            # single test of C files
  		    'f', 'f'           # include a list of commands
);

  # Defines the returned error codes and classifed failures
  # depending on where in run_test the error occurs
  %rt_error_code = (
                    'unknown',            '          unknown problem                         ',
                    'timeout',            '          timeout                                 ',
                    'signaled',           '          signaled                                ',
                    'pass',               '          passed                                  '
                    
  );
}


# -----------------------------------------------------------------------------
#  SUB : usage
#
#  Prints the usage.
# -----------------------------------------------------------------------------

sub usage
{
    print <<"EOM";
Usage: $0 [<options>] <directories|names>

  Arguments:
    <directories>  Search for tests in the specified directories.
    <names>        Run tests using the specified names as basenames.
                  Unless the extension is also given, all tests
                  sharing the same basename will be run.
    <options>
      -no-cleanup  Do not clean up temporary files and directories.
      -f <file>    Use file to supply tests.
      -o <opts>    Additional compiler options.
      -O <opts>    Additional tool options.
      -t <time>    Set the timeout for a test in minutes (default 5 minutes).
      -T           Measure runtime of tests in seconds.
      -v           Verbose output.

EOM
    exit 1;
}


# -----------------------------------------------------------------------------
#  SUB : parse_args
#
#  Parse the command line arguments.
# -----------------------------------------------------------------------------

sub parse_args
{
    local( @arglist ) = @_;

    # print usage if no arguments
    &usage if( $#arglist < 0 );

    local( $tests ) = '';
    local( $files ) = '';
  
    while( $#arglist >= 0 ) {

  # next argument
  local( $arg ) = shift @arglist;

  if( $arg =~ /^-/ ) {  # must be -arg
   
     # do not cleanup
     if( $arg =~ /^-no-cleanup/ ) {
    $rt_cleanup = 0;
    next;
     }
   
     # include file
     if( $arg =~ /^-f/ ) {
    $arg = shift @arglist;
    $files .= ( $files eq '' ) ? "$arg" : " $arg";
    next;
     }

     # additional compiler options
     if( $arg =~ /^-o/ ) {
    $arg = shift @arglist;
    $rt_opts .= ( $rt_opts eq '' ) ? "$arg" : " $arg";
    next;
     }

     # additional tool options
     if( $arg =~ /^-O/ ) {
    $arg = shift @arglist;
    $rt_tool_options .= ( $rt_tool_options eq '' ) ? "$arg" : " $arg";
    next;
     }
   
     # timeout value
     if( $arg =~ /^-t/ ) {
    $arg = shift @arglist;
    $rt_timeout = $arg;
    next;
     }

     # time tests
     if( $arg =~ /^-T/ ) {
    $rt_time_tests = 1;
    next;
     }

     # verbose
     if( $arg =~ /^-v/ ) {
    $rt_verbose = 1;
    next;
     }
   
     &print_log( "Error: unknown argument '$arg'\n");
     exit 1;
  }
      
  # must be test
  else {
     $tests .= ( $tests eq '' ) ? "$arg" : " $arg";
  }
    }

    # print usage if no tests specified
    &usage if( $tests eq '' ) && ( $files eq '' );

    ( $tests, $files );
}


# -----------------------------------------------------------------------------
#  SUB : print_intro
#
#  Print the intro.
# -----------------------------------------------------------------------------

sub print_intro
{
    local( $script );
    ( $script = $0 ) =~ s|^.*/||;
    local( $host );
    chop( $host = `hostname` );
    local( $uname_a );
    chop( $uname_a = `uname -a` );
    local( $date );
    chop( $date = `date` );
    local( $version );
    chop( $version = `$rt_panda_tool --version 2>& 1 | grep TOOLSET` );

    &print_log( "\n*** $script started on host $host\n\n" );
    &print_log( "$uname_a\n" );
    &print_log( "$date\n" );

    &print_log( "\n*** settings\n\n" );
    &print_log( "PANDA_ARCH : $rt_panda_arch\n" );
    &print_log( "$rt_panda_home\n" );
    &print_log( "PANDA_TEST : " );
    &print_log( "$rt_panda_test\n" );
    &print_log( "PANDA_TOOL : " );
    &print_log( "$rt_panda_tool\n" );
    &print_log( "$version\n");
    &print_log( "PANDA_TOOL_OPTION : " );
    &print_log( "$rt_panda_tool_option\n" );
    &print_log( "CC : " );
    &print_log( "$rt_cc\n" );
    
    &print_log( "OUTPUT_DIR  : " );
    &print_log( "$rt_output_dir\n" );

    &print_log( "\n*** running tests\n" );
}


# -----------------------------------------------------------------------------
#  SUB : get_props
#
#  - Find properties for directory;
#  - find directory test type (single, include, shell);
#  - return bits representing properties;
#  - return list of test files.
# -----------------------------------------------------------------------------

sub get_props
{
    local( $dir ) = @_;

    local( $props ) = 0;    # properties to return
    local( $arch ) = '';    # arch for this machine
    local( @tmp );
    local( @files );

    opendir( RT_DIR, $dir ) || die "Error: cannot open directory '$dir'\n";
    @files = readdir( RT_DIR );
    closedir( RT_DIR );
    
    # set .compile .ig_retval
    if( -e "$dir/$rt_compile" ) {
  $props = $props | $rt_test_props{ 'comp_only' };
    }
    if( -e "$dir/$rt_ig_retval" ) {
  $props = $props | $rt_test_props{ 'ig_retval' };
    }
    
    # set 'dontrun' for:
    #  - .dontrun
    #  - .only<arch> if were are not running on <arch>
    #  - .not<arch>  if were are running on <arch>
    if( -e "$dir/$rt_dontrun" ) {
  $props = $props | $rt_test_props{ 'dontrun' };
    } else {
  @tmp = grep( /^\.not/, @files );
  if( $#tmp >= 0 ) {       # found arch restrictions
     &print_log( "NYI: .not<arch> control\n" );
  }
  @tmp = grep( /^\.only/, @files );
  if( $#tmp >= 0 ) {       # found arch restrictions
     &print_log( "NYI: .only<arch> control\n" );
  }
    }
    
    # find directory type
    $dirtype = 'scr';
    @tmp = grep( /\.$rt_test_type{ 'scr' }$/, @files );
    $#tmp = -1;
    if( $#tmp < 0 ) {
	$dirtype = 'f';
	@tmp = grep( /\.$rt_test_type{ 'f' }$/, @files );
    }
    if( $#tmp < 0 ) {           # no include, so try single
	$dirtype = 'c';
	@tmp = grep( /\.$rt_test_type{ 'c' }$/, @files );
    }
    ( $props, $dirtype, @tmp );
}


# -----------------------------------------------------------------------------
#  SUB : add2testlist
#
#  - Find props for directory;
#  - add tests to %rt_testlist.
# -----------------------------------------------------------------------------

sub add2testlist
{
    # my( $testdir, $testprop, @tests ) = @_;
    local( $testdir ) = shift;
    local( $testprop ) = shift;
    local( @tests ) = @_;
    
    if( $rt_diag >=4 ) {
  &print_log( "DIAG: add2testlist: $testdir " . hex( $testprop ) . " " .
      join( ' ', @tests ) . "\n" );
    }
    
    foreach $test ( @_ ) {
  $rt_testlist{ "$testdir $test" } = $testprop;
    }
}


# -----------------------------------------------------------------------------
#  SUB : get_children
#
#  Return all directories below current.
# -----------------------------------------------------------------------------

sub get_children
{
    local( $dir, $base ) = @_;

    local( @files );

    opendir( RT_DIR, "$dir/$base" ) ||
  die "Error: cannot open directory '$dir/$base'\n";
    @files = grep( ! /^\.\.?$/,
      grep( ( -d "$dir/$base/$_" ) && ! ( -l "$dir/$base/$_" ),
      readdir( RT_DIR ) ) );
    closedir( RT_DIR );
    
    grep( $_ = "$base/$_", @files );
}


# -----------------------------------------------------------------------------
#  SUB : find_it
#
#  - arg is test file or directory.
#  - returns '' on success.
#  - returns level of path where search failed:
#      o test root dir if first part of path fails.
#      o sub-directory(s) if first part of path is found but
#        either directories can't be found, or test doesn't
#        exist.
#  - sets properties for tests in each directory.
#  - adds found tests to %rt_testlist
# -----------------------------------------------------------------------------

sub find_it
{
    local( $testname ) = @_;

    local( @found ) = ();
    local( @searchdirs );        # root dirs in search
    local( @tempsearch );
    local( $tempdir );
    local( $toplevel );          # save level of path
    local( $temp );
    local( $testbase );          # path to test
    local( $dirtype );           # type of dir (single, include, csh)
    local( $dirprops );          # properties for dir (ie dontrun...)

    # remove trailing '/'
    $testname =~ s|/$||;

    # do levelwise search

    ( $toplevel ) = ( $testname =~ m|^([^/]+)| );
    ( $testbase = $testname ) =~ s|/[^/]+$||;
    $testbase = '' if $testbase eq $testname;

    &print_log( "\nDIAG1: testname=$testname, toplevel=$toplevel, " .
       "testbase=$testbase\n" )
  if $rt_diag >= 1;

    # first try to find start of given path
    @tempsearch = ( '.' );
    while( $#found < 0 ) {
  @searchdirs = @tempsearch;
  &print_log( "\nDIAG2: \$#searchdirs=$#searchdirs, " ) if $rt_diag >= 8;
  &print_log( "\$#tempsearch=$#tempsearch\n" ) if $rt_diag >= 8;
  @tempsearch = ();
  return $rt_tests_dir if $#searchdirs < 0;
  foreach $tempdir ( @searchdirs ) {
     opendir( RT_DIR, "$rt_tests_dir/$tempdir" );

     # found test directory or matching file
     if( grep( ( $_ eq $toplevel ) || ( ! -d $_ ),
          grep( /^$toplevel/, readdir( RT_DIR ) ) ) ) {
    if( $tempdir =~ /^\.$/ ) {
        push( @found, "" );
    } else {
        push( @found, "$tempdir/" );
    }
     }

     closedir( RT_DIR ) && next if $#found >= 0;
     rewinddir( RT_DIR );
     foreach $temp ( grep( ! /^\.\.?$/,
          grep( ( -d "$rt_tests_dir/$tempdir/$_" ) &&
          ! ( -l "$rt_tests_dir/$tempdir/$_" ),
          readdir( RT_DIR ) ) ) ) {
    if( $tempdir =~ /^\.$/ ) {
        push( @tempsearch, "$temp" );
    } else {
        push( @tempsearch, "$tempdir/$temp" );
    }
     }
     closedir( RT_DIR );
  }
    }

    &print_log( "DIAG3: \$#found=$#found, found='" .
       join( ':', @found ) . "'\n" ) if $rt_diag >= 5;

    # create list of directories found to be used if
    #   file/dir can't be found
    local( @tmp ) = @found;
    # now toplevel is final part of name
    ( $toplevel ) = ( $testname =~ m|([^/]+)$| );
    local( $retval ) = join( ' ', grep( $_ = "$rt_tests_dir/$_", @tmp ) );
    while( $#found >= 0 ) {
  &print_log( "DIAG4: \$#found=$#found, found='" .
      join( ':', @found ) . "'\n" ) if $rt_diag >= 5;

  $temp = shift @found;

  &print_log( "DIAG5: $rt_tests_dir/$temp$testbase\n" ) if $rt_diag >= 4;

  # don't waste time if path can't be found
  next if ! -d "$rt_tests_dir/$temp$testbase";

  # full path to single test
  ( $_ ) = ( $testname =~ /\.([a-z]{1,3})$/ );
  &print_log( "DIAGx: $rt_tests_dir/$temp$testname $rt_tests_dir/$temp${testbase}/$toplevel\n" ) if $rt_diag >= 5;

  if( ( -f "$rt_tests_dir/$temp$testname" ) &&
     ( -f "$rt_tests_dir/$temp${testbase}/$toplevel" ) &&
     # test for invalid test extension
     ( ( $_ eq $rt_test_type{ 'c' } ) ||
       ( $_ eq $rt_test_type{ 'f' } ) ) ) {

     # get directory properties
     ( $dirprops, $dirtype, @tmp ) =
    &get_props( "$rt_tests_dir/$temp$testbase" );
     $dirprops = $dirprops | $rt_props;    # add global props

     ( $_ ) = ( $testname =~ m|/?([^/]+)$| );
     $testname =~ s|/?[^/]+$||;
     $temp =~ s|/$|| if $testname eq '';
     &add2testlist( "$temp$testname", $dirprops, $_ );
     $retval = '';
  }
  else {

     # full path to directory
     if( -d "$rt_tests_dir/$temp$testname" ) {
    &print_log( "DIAG6: dir='$rt_tests_dir/$temp$testname'\n" )
        if $rt_diag >= 4;

    # check if directory is execused
    ( $dirprops, $dirtype, @tmp ) =
        &get_props( "$rt_tests_dir/$temp$testname" );
    next if( $dirprops & $rt_test_props{ 'dontrun' } ) > 0;

    # first look for children
    @tmp = &get_children( "$rt_tests_dir", "$temp$testname" );
    if( $#tmp >= 0 ) {
        # we found children so update @found, add 
        #   $testname to $temp, and zero $testname
        push( @found, @tmp );   # adding children
        $temp .= $testname;
        $testname = '';
        $testbase = '';
    }

    # get directory properties
    ( $dirprops, $dirtype, @tmp ) =
        &get_props( "$rt_tests_dir/$temp$testname" );
    @tmp = grep( -f "$rt_tests_dir/$temp${testname}/$_", @tmp );
    next if $#tmp < 0;  # no tests in dir
    next if( $dirprops & $rt_test_props{ 'dontrun' } ) > 0;
    $dirprops = $dirprops | $rt_props;    # add global props

    &add2testlist( "$temp$testname", $dirprops, @tmp );
    $retval = '';
     }

     # find matching files that begin with same name
     else {
    # get directory properties
    ( $dirprops, $dirtype, @tmp ) =
        &get_props( "$rt_tests_dir/$temp${testbase}" );
    @tmp = grep( -f "$rt_tests_dir/$temp${testbase}/$_", @tmp );
    next if $#tmp < 0;  # no tests in dir
    next if( $dirprops & $rt_test_props{ 'dontrun' } ) > 0;
    $dirprops = $dirprops | $rt_props;    # add global props

    @tmp = grep( m|^$toplevel[^/]+$|, @tmp );
    if( $#tmp >= 0 ) {
        $temp =~ s|/$|| if $testbase eq '';
        &add2testlist( "$temp$testbase", $dirprops, @tmp );
        $retval = '';
    }
     }
  }
    }
    
    return $retval;
}


# -----------------------------------------------------------------------------
#  SUB : get_testlist
#
#  - Arguments:
#    string of whitespace delimited file or directory names
#    string containing name of -f include file
#  - Puts all testnames in associative array (%rt_testlist) using the
#    testname as the key and putting attributes about the test in
#    the value.  Testname is space separated path and name:
#    "private/bugs bug21.f"
#  - Uses dot files to set attributes.
#  - Returns 1 for success, 0 for failure
# -----------------------------------------------------------------------------

sub get_testlist
{
    local( $namelist,              # list of names
   $include_names          # list of include filenames
   ) = @_;
    local( @namearray );              # array for names and filenames
    local( $listlen );                # number of names in namelist
    local( $temp );
    local( $err_file );
    local( $temp_fname );
    local( $testnames );              # tests found in search

    # process command line names
    foreach $temp ( split( / /, $namelist ) ) {
  next if $temp =~ /^\s*$/;
  $testnames = &find_it( $temp );    # returns '<paths>' on failure
  if( $testnames ne '' ) {
     # error because dir exists but has no tests
     foreach $err_file ( split( ' ', $testnames ) ) {
    if( -d "$err_file$temp" ) {
        &print_log( "Error: test directory '$err_file$temp'\n\t" .
           "doesn't contain any tests\n" );

        # error because dir or tests don't exist
    } else {
        &print_log( "Error: '$temp' not found in '$err_file'\n" );
    }
     }
     return 0;
  }
    }

    # process include files
    foreach $temp_fname ( split( /\s+/, $include_names ) ) {
  next if $temp_fname =~ /^\s*$/;
  if( ! ( -e $temp_fname ) ) {
     &print_log( "Error: '$temp_fname' does not exist\n" );
     return 0;
  }
  if( ! open( RT_INCFILE, $temp_fname ) ) {
     &print_log( "Error: '$temp_fname' cannot be opened for reading\n" );
     return 0;
  }
  while( <RT_INCFILE> ) {
     s/\n//;
     next if /^#/;
     foreach $temp ( split( /\s+/, $_ ) ) {
    next if $temp =~ /^\s*$/;
    $temp =~ s/\r//g;
    $testnames = &find_it( $temp );  # returns '<paths>' on failure
    if( $testnames ne '' ) {
        # error because dir exists but has no tests
        foreach $err_file ( split( ' ', $testnames ) ) {
      if( -d "$err_file$temp" ) {
         &print_log( "Error: test directory " .
              "'$err_file$temp'\n\t" .
              "doesn't contain any tests\n" );
         
         # error because dir or tests don't exist
      } else {
         &print_log( "Error: '$temp' not found in " .
              "'$err_file'\n" );
      }
        }
        return 0;
    }
     }
  }
  close RT_INCFILE;
    }
    1;
}


# -----------------------------------------------------------------------------
#  SUB : create_dir
#
#  Create the given directory (if needed), and go there.
# -----------------------------------------------------------------------------

sub create_dir
{
    local( $dir ) = @_;

    # remove first / from $dir
    $dir =~ s|/||;

    # start at the root directory
    chdir( '/' );

    local( $d );
    foreach $d ( split( '/', $dir ) ) {
  # create directory $d if it doesn't exist
  if( ! ( -d "$d" ) ) {
     if( ! mkdir( $d ) ) {
    &print_log( "Error: cannot create directory '$d'\n" );
    # failed
    return 0;
     }
  }
  # go to directory $d
  if( ! chdir( $d ) ) {
     &print_log( "Error: cannot go to directory '$d'\n" );
     # failed
     return 0;
  }
    }

    # succeeded
    1;
}


# -----------------------------------------------------------------------------
#  SUB : setup_for_test
#
#  - cd to proper log directory
#  - create log directories as needed
#  - create symlink for source dir
#  - return last level of path to test
# -----------------------------------------------------------------------------

sub setup_for_test
{
    local( $currtestdir ) = @_;
    local( $linkdir );

    @splitted = split( '/', $currtestdir );
    $linkdir = @splitted[-1];

    # create log path and cd to it
    &create_dir( "$rt_output_dir/$currtestdir" );

  if( ! ( -e $linkdir ) ) {
    eval 'symlink( "$rt_tests_dir/$currtestdir", "$linkdir" );';
    if( ! ( -e $linkdir ) ) {
       &print_log( "Error: unable to establish symbolic link " .
        "'$rt_tests_dir/$currtestdir' to '$linkdir'\n" );
    }
  }


    # return last part of path to test
    $linkdir;
}


# -----------------------------------------------------------------------------
#  SUB : rt_system
#
#  - run system call with signal capture
#  - any call given through here should 
#    redirect output (have meta chars)
# -----------------------------------------------------------------------------

sub rt_system
{
    local( $command ) = @_;

    local( $ret_code );
    local( $signal );
    
  FORK: {
      if( $rt_child_pid = fork ) {
   # wait for command to complete
   waitpid( $rt_child_pid, 0 );
   undef $rt_child_pid;
   $ret_code = $? >> 8;
   $signal = $? & 255;
   return ( $ret_code, $signal );

      } elsif( defined $rt_child_pid ) {
   # execute command
   # (add exec so process is command, not sh)
   exec( "exec $command" ) || exit -1;

      } elsif( $! =~ /No more process/ ) {
   sleep 5;
   redo FORK;
      } else {
   return 1;
      }
  }
}


# -----------------------------------------------------------------------------
#  SUB : file_size
#
#  - check the size of the log file
#  - return the size of the file
# -----------------------------------------------------------------------------

sub file_size
{
    local( $file ) = @_;

    local( $size ) = -s( $file );
    $size;
}


# -----------------------------------------------------------------------------
#  SUB : rt_timed_system
#
#  ******PLEASE READ COMMENTS VERY CAREFULLY BEFORE MAKING CHANGES*******
#  - run system call with timeout and checking size of output file
# -----------------------------------------------------------------------------

sub rt_timed_system
{
    local( $command, $timeout, $file ) = @_;

    local( $size );
    local( $ret_code );
    local( $signal );

  FORK: {
      # fork here to monitor the cpu time for the simulator
      if( $rt_child_pid = fork ) {
   alarm( $timeout * 60 );
   # do another fork here to monitor the size of the log file
   if( $rt_size_pid = fork ) {
       # parent process
       # does nothing
   } elsif( defined $rt_size_pid ) {
       # child process
       # if filesize > maxfile size terminate
       while( 1 ) {
      sleep( 5 );
      $size = &file_size( $file );
      if( $size > $rt_max_file_size ) {
          kill -9, getpgrp($rt_child_pid);
          undef $rt_child_pid;
          exit 0;
      }
       }
   }
   # wait for termination of either child procees
   # note that this is ABSO necessary else there will be defunct 
   #   processes floating around
   # note that this is the process whose exit code we are interested
   #   in and not the size process
   waitpid( $rt_child_pid, 0 );
   $ret_code = ($? >> 8);
   $signal = ($? & 255);
   # now kill the size process and wait for it to terminate
   #   else there will be defunct processes floating around
   kill 9, $rt_size_pid;
   waitpid( $rt_size_pid, 0 );
   alarm 0;
   undef $rt_size_pid;
   undef $rt_child_pid;
   return( $ret_code, $signal );

      } elsif( defined $rt_child_pid ) {
       setpgrp;
   # execute command
   # (add exec so process is command, not sh)
   exec( "exec $command" ) || exit -1;
      } elsif( $! =~ /No more process/ ) {
   sleep 5;
   redo FORK;
      } else {
   undef $rt_child_pid;
   undef $rt_child_pid;
   return 1;
      }
  }
}


# -----------------------------------------------------------------------------
#  SUB : run_test
#
#  - first arg is testname without path
#  - second arg is path to test starting from testdir
# -----------------------------------------------------------------------------

sub run_test
{
  local( $currtestdir,       # path to tests
    $testname) = @_;

  local( $props ) = $rt_testlist{ "$currtestdir $testname" };

  local( $type );             # test type (t=pli, s=single, f=include, c=csh)
  local( $temp );
  local( $command );          # command given to system call
  local( $linkdir );          # final dir in path to test
  local( $opts );             # command line options passed to compiler
  local( $basename );         # basename for test
  local( $exit_code );        # exit code returned by system calls
  local( $signal );           # signal on which system call terminated
  local( $tmp );
  local( $comp_opts );        # options to compiler (vvc)
  local( $sym_opts );         # options to simulator (sym)
  local( $err_file );
  local( $scl_include );

  &print_log( "\n" );
  # time tests, before
  local( @before );
  if( $rt_time_tests ) {
    @before = times;
  }


  # ***** Basic Setup *****

  # determine test type
  $testname =~ s/\.([a-z]{1,3})$//;
  $type = $1;
  
  # making a global to keep track of progress
  $rt_current_test = "$currtestdir/$testname.$type";

  # check for compiler
  # include your compiler check in here

  # cd and create symlink
  $linkdir = &setup_for_test( $currtestdir );
  
  # extract base name
  $basename = "$linkdir/$testname";
  
  # read options file
  $opts = "$rt_opts";

  $additional_tool_options = eval $rt_tool_options;

  $simulate_options = eval $rt_simulate_options;

  $command = "";

  &print_log( "---------------------------------------" .
     "----------------------------------------\n" );
  local( $num_tests ) = scalar keys %rt_testlist;
  &print_log( " TEST : $rt_current_test ($rt_test_number/$num_tests)\n" );
  &print_log( "---------------------------------------" .
     "----------------------------------------\n" );

  local( $extra_cflags ) = "";
  local( $extra_ld_flags ) = "-lm";


  # ***** Single file analysis *****
  #
  # synthetize a single .c file
  #
  if( $type =~ /$rt_test_type{'c'}/ ) {

    #panda command
    $command  = "$rt_panda_tool $rt_panda_tool_option $simulate_options $opts $additional_tool_options ./$basename.$type 1> $testname.log  2>&1";

    &print_log( "PandA analysis\n" );
    &print_log( "  $command\n" ) if $rt_verbose;

    # run command
    ( $exit_code, $signal ) = &rt_timed_system( $command, $rt_timeout,
                "$testname.log");

    # timeout checking
    if( $signal == 9 ) {
      &print_log( "FAILED $rt_error_code{'timeout'}\n" );
      return $rt_error_code{'timeout'};
    }
    if( $signal != 0) {
      &print_log( "FAILED $rt_error_code{'signaled'}\n" );
      return $rt_error_code{'signaled'};
    }
    if( $exit_code != 0 ) {
      &print_log( "FAILED $rt_error_code{'unknown'}\n" );
      return $rt_error_code{'unknown'};
    }
  }

  # ***** Single file analysis *****
  #
  # synthetize the list of file included in ./$basename.$type with the given parameters
  #
  if( $type =~ /$rt_test_type{'f'}/ ) {

    open( FILE, "<./$basename.$type" ) ||
	&print_log( "Error: cannot open file './$basename.$type'\n" );
    $design_options = "";
    while( <FILE> ) {
        $temp_line = $_;
        chop( $temp_line );
        @temp_items = split(/ +/, $temp_line);
        foreach $item (@temp_items)
        {
            if( $item =~ /^-.+/ ) {
                $item =~ s/\$linkdir/$linkdir/;
                $design_options .= " $item";
            } else {
                $design_options .= " $linkdir/$item";
            }
        }
        next;
    }
    close FILE;

    #panda command
    $command  = "$rt_panda_tool $rt_panda_tool_option $opts $additional_tool_options $design_options 1> $testname.log  2>&1";

    &print_log( "PandA analysis\n" );
    &print_log( "  $command\n" ) if $rt_verbose;

    # run command
    ( $exit_code, $signal ) = &rt_timed_system( $command, $rt_timeout,
                "$testname.log");

    # timeout checking
    if( $signal == 9 ) {
      &print_log( "FAILED $rt_error_code{'timeout'}\n" );
      return $rt_error_code{'timeout'};
    }
    if( $signal != 0) {
      &print_log( "FAILED $rt_error_code{'signaled'}\n" );
      return $rt_error_code{'signaled'};
    }
    if( $exit_code != 0 ) {
      &print_log( "FAILED $rt_error_code{'unknown'}\n" );
      return $rt_error_code{'unknown'};
    }
  }


  # time tests, after
  if( $rt_time_tests ) {
    local( @after ) = times;
    &print_log( "  " . int( $after[2] - $before[2] ) . " s\n" );
  }


  &print_log( "PASSED\n" );
  return $rt_error_code{'pass'};

}


# -----------------------------------------------------------------------------
#  SUB : clean_up
#
#  Remove the current test directory, including any files.
# -----------------------------------------------------------------------------

sub clean_up
{
  local( $dir );
  ( $dir = $rt_current_test ) =~ s|/[^/]+$||;
  local( $full_dir ) = "$rt_output_dir/$dir";

  opendir( RT_DIR, $full_dir ) || return 0;
  local( @files );
  @files = readdir( RT_DIR );
  closedir( RT_DIR );

  local( $f );
  foreach $f ( @files ) {
    if( -d "$full_dir/$f" && $f !~ /^\.\.?$/ ) {
      local( @args );
      @args = ( "rm", "-rf", "$full_dir/$f" );
      system( @args );
    }
    if( $f !~ /^\.\.?$/ ) {
      unlink "$full_dir/$f" || return 0;
    }
  }

  if( $rt_panda_arch ne "msvc60" ) {
    rmdir $full_dir || return 0;
  }

  1;
}


# -----------------------------------------------------------------------------
#  SUB : main
#
#  The main routine.
# -----------------------------------------------------------------------------

sub main
{
  &init_globals;

  # parse the command line arguments
  local( $tests );
  local( $files );
  ( $tests, $files ) = &parse_args( @ARGV );

  &print_intro;

  &get_testlist( $tests, $files ) || exit 1;

  local( $t );
  $rt_test_number = 0;
  foreach $t ( sort( keys %rt_testlist ) ) {
    $rt_test_number ++;
    $error_code = &run_test( split( ' ', $t ) );
    $t =~ s| |/|;
    if( $error_code eq $rt_error_code{ 'pass' } ) {
      push( @rt_pass, $t );
      &clean_up if( $rt_cleanup );
    } else {
      push( @rt_fail, "$error_code : $t" );
    }
    &print_log( "(", $#rt_pass + 1, ":", $#rt_fail + 1, ")\n" );
  }
  &compile_results;
}
