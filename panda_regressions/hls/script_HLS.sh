#!/bin/bash
script_file=`which $0`
top_dir=`dirname $script_file`
sh $top_dir/launch_bambu.sh gcc_regression/gcc_regression_simple "-lm $1"
sh $top_dir/launch_bambu.sh gcc_regression/gcc_regression_simple "-lm -O1 $1"
sh $top_dir/launch_bambu.sh gcc_regression/gcc_regression_simple "-lm -O2 $1"
sh $top_dir/launch_bambu.sh gcc_regression/gcc_regression_simple "-lm -O3 $1"
sh $top_dir/launch_bambu.sh gcc_regression/gcc_regression_simple "-lm --generate-interface=WB4 $1"
sh $top_dir/launch_bambu.sh gcc_regression/gcc_regression_simple "-lm --channels-type=MEM_ACC_N1 $1"
sh $top_dir/launch_bambu.sh gcc_regression/gcc_regression_simple "-lm --channels-type=MEM_ACC_NN $1"
sh $top_dir/launch_bambu.sh gcc_regression/gcc_regression_simple "-lm --soft-float $1"
sh $top_dir/launch_bambu.sh libm-tests "$1"

