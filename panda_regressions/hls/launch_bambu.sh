#!/bin/bash
if [ "$#" != 2 ]
then
   echo "Usage: launch_bambu <dir> <option>"
   false
   exit
fi
script_file=`which $0`
top_dir=`dirname $script_file`
export PANDA_TOOL="/opt/panda/bin/bambu"
export PANDA_TOOL_OPTION="$2"
export CC=gcc
ulimit -m 8388608
ulimit -v 8388608
ulimit -d 8388608
$top_dir/perl/verify.hls.pl -no-cleanup -t 240 -T -v `basename $1`
mv out/verify.log out/`basename $1`_verify.log

