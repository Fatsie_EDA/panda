#!/bin/bash
export PATH=/opt/panda/bin:$PATH

ulimit -m 6291456
ulimit -v 6291456
ulimit -d 6291456

mkdir -p bambu_synthA_NN
cd bambu_synthA_NN
echo "#ALTERA synthesis"
DEVICE_PARAM="--device-name=EP2C70F896C6-DSP"
FPGA_TYPE="EP2"
OPTIMIZATION="-O3 --channels-type=MEM_ACC_NN --compiler=I386_GCC47 -fno-tree-loop-distribute-patterns --skip-pipe-parameter=0"
OPTIMIZATION_Os="-Os --channels-type=MEM_ACC_NN --compiler=I386_GCC47 -fno-tree-loop-distribute-patterns --skip-pipe-parameter=0"

OPT_NAME="O3"
OPT_NAME_Os="Os"
#SYNTH="--evaluation --simulator=ICARUS"
SYNTH="--evaluation --simulator=XSIM"
#SYNTH="--evaluation"

#adpcm
file=adpcm/adpcm.c
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION  -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#aes
file=aes/aes.c
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#blowfish
file=blowfish/bf.c 
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#dfadd
file=dfadd/dfadd.c
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#dfdiv
file=dfdiv/dfdiv.c
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#dfmul
file=dfmul/dfmul.c
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#dfsin
file=dfsin/dfsin.c
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#gsm
file=gsm/gsm.c
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#jpeg
file=jpeg/main.c
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME_Os-`dirname $file`" $OPTIMIZATION_Os -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#mips
file=mips/mips.c  
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#motion
file=motion/mpeg2.c 
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..

#sha
file=sha/sha_driver.c 
mkdir `dirname $file`
cd `dirname $file`
bambu ../../$file --benchmark-name="$FPGA_TYPE-$OPT_NAME-`dirname $file`" $OPTIMIZATION -fwhole-program -v4 $SYNTH -D'printf(fmt, ...)=' $DEVICE_PARAM --clock-period=15
cd ..


# end
cd ..

