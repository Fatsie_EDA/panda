#!/bin/bash
export PATH=/opt/panda/bin:$PATH

mkdir -p icrc
cd icrc
echo "#synthesis of icrc"
bambu ../spec.c --top-fname=icrc --simulator=ICARUS --device-name=LFE335EA8FN484C --simulate=../test_icrc.xml -v2 2>&1 | tee icrc.log
cd ..

mkdir -p main
cd main
echo "#synthesis of main"
bambu ../spec.c  --simulator=ICARUS  --device-name=LFE335EA8FN484C --simulate=../test.xml -v2 2>&1 | tee main.log


