#!/bin/bash
export PATH=/opt/panda/bin:$PATH

mkdir -p icrc
cd icrc
echo "#synthesis of icrc"
bambu ../spec.c --top-fname=icrc --simulator=ICARUS --simulate=../test_icrc.xml --channels-type=MEM_ACC_N1 -v2 2>&1 | tee icrc.log
cd ..

mkdir -p main
cd main
echo "#synthesis of main"
bambu ../spec.c  --simulator=ICARUS --simulate=../test.xml --channels-type=MEM_ACC_N1 -v2 2>&1 | tee main.log


