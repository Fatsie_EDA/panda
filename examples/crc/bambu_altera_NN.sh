#!/bin/bash
export PATH=/opt/panda/bin:$PATH

mkdir -p icrc
cd icrc
echo "#synthesis of icrc"
bambu ../spec.c --top-fname=icrc --simulator=ICARUS --device-name=EP2C70F896C6-R --simulate=../test_icrc.xml --channels-type=MEM_ACC_NN -v2 2>&1 | tee icrc.log
cd ..

mkdir -p main
cd main
echo "#synthesis of main"
bambu ../spec.c  --simulator=ICARUS  --device-name=EP2C70F896C6-R --simulate=../test.xml --channels-type=MEM_ACC_NN -v2 2>&1 | tee main.log


