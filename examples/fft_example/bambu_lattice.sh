#!/bin/bash
BAMBU_OPTION="-v4 -lm -fsingle-precision-constant --evaluation -Os --device-name=LFE335EA8FN484C -ffast-math"
rm -rf run_dir_lattice
mkdir run_dir_lattice
cd run_dir_lattice
/opt/panda/bin/bambu --generate-tb=../test_no_main.xml ../fft_float.c --generate-interface=WB4 --top-fname=FFT $BAMBU_OPTION
cd ..

rm -rf run_dir_lattice_1
mkdir run_dir_lattice_1
cd run_dir_lattice_1
/opt/panda/bin/bambu --generate-tb=../test.xml  ../fft_float.c -fwhole-program $BAMBU_OPTION
cd ..

