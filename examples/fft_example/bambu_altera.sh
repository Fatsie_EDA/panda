#!/bin/bash
BAMBU_OPTION="-v4 -lm -fsingle-precision-constant --evaluation -Os --device-name=EP2C70F896C6-R -ffast-math"
rm -rf run_dir_altera
mkdir run_dir_altera
cd run_dir_altera
/opt/panda/bin/bambu --generate-tb=../test_no_main.xml ../fft_float.c --generate-interface=WB4 --top-fname=FFT $BAMBU_OPTION
cd ..

rm -rf run_dir_altera_1
mkdir run_dir_altera_1
cd run_dir_altera_1
/opt/panda/bin/bambu --generate-tb=../test.xml  ../fft_float.c -fwhole-program $BAMBU_OPTION
cd ..

