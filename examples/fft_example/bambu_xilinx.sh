#!/bin/bash
BAMBU_OPTION="-v4 -lm -fsingle-precision-constant --evaluation -Os --device-name=xc7z020,-1,clg484,VVD -ffast-math"
rm -rf run_dir_xilinx
mkdir run_dir_xilinx
cd run_dir_xilinx
/opt/panda/bin/bambu --generate-tb=../test_no_main.xml ../fft_float.c --generate-interface=WB4 --top-fname=FFT $BAMBU_OPTION  
cd ..

rm -rf run_dir_xilinx_1
mkdir run_dir_xilinx_1
cd run_dir_xilinx_1
/opt/panda/bin/bambu --generate-tb=../test.xml  ../fft_float.c -fwhole-program $BAMBU_OPTION
cd ..

