#!/bin/bash
export PATH=/opt/panda/bin:$PATH

mkdir -p sim
cd sim
echo "# HLS synthesis, testbench generation and simulation"
bambu -v2 -O2 ../module.c --generate-tb=../test.xml --evaluation --objective=CYCLES --simulator=XSIM --pretty-print=a.c --target-file=../xc7z045-2ffg900-VVD.xml 2>&1 | tee factorial.log
cd ..

mkdir -p synth
cd synth
echo "# HLS synthesis, testbench generation, simulation with XSIM and RTL synthesis with VIVADO RTL"
bambu -v2 -O2 ../module.c --generate-tb=../test.xml --evaluation --simulator=XSIM --pretty-print=a.c --target-file=../xc7z045-2ffg900-VVD.xml 2>&1 | tee factorial.log
cd ..

