#!/bin/bash
export PATH=/opt/panda/bin:$PATH

ulimit -m 2097152
ulimit -v 2097152
ulimit -d 2097152

echo "#collect"
 /opt/panda/bin/spider `find $1 -name  bambu_results*xml` ./latex_format_bambu_results.xml table-$1.tex
cd ..
