#!/bin/bash

SIMULATOR=XSIM
mkdir -p libm_simulateA
cd libm_simulateA
for testcase_pair in acosf:x=\"0.5\" asinf:x=\"0.5\" atanf:x=\"0.5\" atan2f:y=\"1.390625\"x=\"0.9296875\" cosf:x=\"0.75\" sinf:x=\"0.75\" tanf:x=\"0.75\" acoshf:x=\"7\" asinhf:x=\"0.75\" atanhf:x=\"0.75\" coshf:x=\"0.75\" sinhf:x=\"0.75\" tanhf:x=\"0.75\" expf:x=\"0.75\" expm1f:x=\"0.75\" ldexpf:value=\"0.8\"exp=\"4\" logf:x=\"0.75\" log10f:x=\"0.75\" log1pf:x=\"-0.25\" logbf:x=\"-2000\" ilogbf:x=\"-2000\" scalbnf:x=\"-0.854375\"n=\"5\" significandf:x=\"6\" cbrtf:x=\"-27\" fabsf:X=\"-27\" hypotf:x=\"12.4\"y=\"0.7\" powf:x=\"0.75\"y=\"1.25\" sqrtf:x=\"15190.5625\" erff:x=\"0.75\" erfcf:x=\"0.75\" gammaf:x=\"0.5\" lgammaf:x=\"0.7\" ceilf:x=\"-1.7\" floorf:x=\"-1.7\" rintf:x=\"-1.5\" fmodf:x=\"-6.5\"y=\"2.25\" remainderf:x=\"-1.625\"y=\"1.0\" copysignf:x=\"-0\"y=\"-4\"; do
  testcase=`echo $testcase_pair | awk -F : '{print $1}'`
  value=`echo $testcase_pair | awk -F : '{print $2}'`
  mkdir -p $testcase-flopoco
  cd $testcase-flopoco
  echo "<?xml version=\"1.0\"?><function><testbench $value/></function>" > test_$testcase.xml
  echo "" > empty.c
  /opt/panda/bin/bambu --benchmark-name=$testcase-flopoco --simulate=test_$testcase.xml --top-fname=__builtin_$testcase empty.c -lm  -v2 --simulator=$SIMULATOR --device-name=EP2C70F896C6-R >& log.$testcase-flopoco.txt 
  cd ..
  mkdir -p $testcase-soft-float
  cd $testcase-soft-float
  echo "<?xml version=\"1.0\"?><function><testbench $value/></function>" > test_$testcase.xml
  echo "" > empty.c
  /opt/panda/bin/bambu --benchmark-name=$testcase-soft-float --simulate=test_$testcase.xml --top-fname=__builtin_$testcase empty.c -lm  --soft-float -v2 --simulator=$SIMULATOR --device-name=EP2C70F896C6-R >& log.$testcase-soft-float.txt 
  cd ..
done
cd ..


