bambu_altera.sh shows the impact of resource sharing on multipliers operations. Six synthesis are performed: three constrained and three non-constrained.
Synthesis performed use different Quartus set-up for a Cyclone II device EP2C70F896C6: EP2C70F896C6-DSP, EP2C70F896C6-NODSP and EP2C70F896C6-R.
The device tagged with EP2C70F896C6-DSP allows the use of the DSP blocks during RTL synthesis and during the RTL characterization.
The device tagged with EP2C70F896C6-NODSP allows the use of DSP blocks during the RTL synthesis but not during the RTL characterization.
The device tagged with EP2C70F896C6-R allows the use of DSPs both in RTL synthesis and in RTL characterization and the RTL synthesis script has the re-timing option enabled.

bambu_xilinx.sh compares a constrained and an unconstrained synthesis with a Xilinx Zynq target.

bambu_lattice.sh compares a constrained and an unconstrained synthesis with a Lattice ECP3 target.

In all the synthesis performed, the WB4 interface has been used to avoid issues with the high number of IO pins required by the arf function when synthesized alone.





