#!/bin/bash
export PATH=/opt/panda/bin:$PATH

rm -rf constrained_synth_xilinx
mkdir -p constrained_synth_xilinx
cd constrained_synth_xilinx
echo "# Vivado synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --evaluation --generate-interface=WB4 ../constraints_STD.xml --clock-period=5  >&1 | tee arf.log
cd ..

rm -rf unconstrained_synth_xilinx
mkdir -p unconstrained_synth_xilinx
cd unconstrained_synth_xilinx
echo "# Vivado synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --evaluation --generate-interface=WB4 --clock-period=5 2>&1 | tee arf.log
cd ..

rm -rf constrained_synth_xilinx
mkdir -p constrained_synth_xilinx
cd constrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc7z020,-1,clg484 --evaluation --generate-interface=WB4 ../constraints_STD.xml --clock-period=5  >&1 | tee arf.log
cd ..

rm -rf unconstrained_synth_xilinx
mkdir -p unconstrained_synth_xilinx
cd unconstrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc7z020,-1,clg484 --evaluation --generate-interface=WB4 --clock-period=5 2>&1 | tee arf.log
cd ..

rm -rf constrained_synth_xilinx
mkdir -p constrained_synth_xilinx
cd constrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc7vx330t,-1,ffg1157 --evaluation --generate-interface=WB4 ../constraints_STD.xml --clock-period=5  >&1 | tee arf.log
cd ..

rm -rf unconstrained_synth_xilinx
mkdir -p unconstrained_synth_xilinx
cd unconstrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc7vx330t,-1,ffg1157 --evaluation --generate-interface=WB4 --clock-period=5 2>&1 | tee arf.log
cd ..

rm -rf constrained_synth_xilinx
mkdir -p constrained_synth_xilinx
cd constrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc6vlx240t,-1,ff1156 --evaluation --generate-interface=WB4 ../constraints_STD.xml --clock-period=5  >&1 | tee arf.log
cd ..

rm -rf unconstrained_synth_xilinx
mkdir -p unconstrained_synth_xilinx
cd unconstrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc6vlx240t,-1,ff1156 --evaluation --generate-interface=WB4 --clock-period=5 2>&1 | tee arf.log
cd ..

rm -rf constrained_synth_xilinx
mkdir -p constrained_synth_xilinx
cd constrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc5vlx50,-3,ff1153 --evaluation --generate-interface=WB4 ../constraints_STD.xml --clock-period=5  >&1 | tee arf.log
cd ..

rm -rf unconstrained_synth_xilinx
mkdir -p unconstrained_synth_xilinx
cd unconstrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc5vlx50,-3,ff1153 --evaluation --generate-interface=WB4 --clock-period=5 2>&1 | tee arf.log
cd ..

rm -rf constrained_synth_xilinx
mkdir -p constrained_synth_xilinx
cd constrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc5vlx330t,-2,ff1738 --evaluation --generate-interface=WB4 ../constraints_STD.xml --clock-period=5  >&1 | tee arf.log
cd ..

rm -rf unconstrained_synth_xilinx
mkdir -p unconstrained_synth_xilinx
cd unconstrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc5vlx330t,-2,ff1738 --evaluation --generate-interface=WB4 --clock-period=5 2>&1 | tee arf.log
cd ..

rm -rf constrained_synth_xilinx
mkdir -p constrained_synth_xilinx
cd constrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc5vlx110t,-1,ff1136 --evaluation --generate-interface=WB4 ../constraints_STD.xml --clock-period=5  >&1 | tee arf.log
cd ..

rm -rf unconstrained_synth_xilinx
mkdir -p unconstrained_synth_xilinx
cd unconstrained_synth_xilinx
echo "# ISE synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=xc5vlx110t,-1,ff1136 --evaluation --generate-interface=WB4 --clock-period=5 2>&1 | tee arf.log
cd ..

