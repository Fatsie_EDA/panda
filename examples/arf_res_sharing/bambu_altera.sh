#!/bin/bash
export PATH=/opt/panda/bin:$PATH

mkdir -p constrained_synth_altera
cd constrained_synth_altera
echo "# Quartus II synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=EP2C70F896C6-DSP --evaluation --generate-interface=WB4 ../constraints_STD.xml >&1 | tee arf.log
cd ..

mkdir -p unconstrained_synth_altera
cd unconstrained_synth_altera
echo "# Quartus II synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=EP2C70F896C6-DSP --evaluation --generate-interface=WB4 2>&1 | tee arf.log
cd ..

cd constrained_synth_altera
echo "# Quartus II synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=EP2C70F896C6-R --evaluation --generate-interface=WB4 --clock-period=5 ../constraints_STD.xml 2>&1 | tee arf.log
cd ..

cd unconstrained_synth_altera
echo "# Quartus II synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=EP2C70F896C6-R --evaluation --generate-interface=WB4 --clock-period=5 2>&1 | tee arf.log
cd ..
