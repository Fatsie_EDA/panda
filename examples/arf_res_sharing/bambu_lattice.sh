#!/bin/bash
export PATH=/opt/panda/bin:$PATH

mkdir -p constrained_synth_lattice
cd constrained_synth_lattice
echo "# Diamond synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=LFE335EA8FN484C --evaluation --generate-interface=WB4 ../constraints_STD.xml --clock-period=9 --cprf=0.8367  >&1 | tee arf.log
cd ..

mkdir -p unconstrained_synth_lattice
cd unconstrained_synth_lattice
echo "# Diamond synthesis and ICARUS simulation"
bambu -v4 ../module.c --generate-tb=../test.xml --simulator=ICARUS --device-name=LFE335EA8FN484C --evaluation --generate-interface=WB4 --clock-period=9 --cprf=0.8367 2>&1 | tee arf.log
cd ..


