#!/bin/bash
export PATH=/opt/panda/bin:$PATH

mkdir -p sim
cd sim
echo "# HLS synthesis, testbench generation and simulation with ICARUS"
bambu -v2 -O3 ../module.c --simulate=../test.xml --simulator=ICARUS --pretty-print=a.c --channels-type=MEM_ACC_NN --device-name=EP2C70F896C6-R --memory-allocation-policy=EXT_PIPELINED_BRAM 2>&1 | tee matrix_multiplication.log
cd ..

mkdir -p synth
cd synth
echo "# HLS synthesis, testbench generation, simulation with ICARUS and RTL synthesis with Quartus"
bambu -v3 -O3 ../module.c --generate-tb=../test.xml --evaluation --simulator=ICARUS --pretty-print=a.c --channels-type=MEM_ACC_NN --device-name=EP2C70F896C6-R --memory-allocation-policy=EXT_PIPELINED_BRAM 2>&1 | tee matrix_multiplication.log
cd ..

