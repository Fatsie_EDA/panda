#!/bin/bash
export PATH=/opt/panda/bin:$PATH

mkdir -p hls
cd hls
echo "#synthesis"
bambu -v2 --print-dot ../module.c 2>&1 | tee arf.log
cd ..

mkdir -p testbench
cd testbench
echo "#synthesis and generation of test bench with ISIM"
bambu -v2 --print-dot ../module.c --generate-tb=../test.xml --simulator=ICARUS 2>&1 | tee arf.log

