/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file eucalyptus.cpp
 * @brief Tool for estimation of RTL descriptions.
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_XILINX.hpp"
#include "config_HAVE_ALTERA.hpp"
#include "config_HAVE_DESIGN_COMPILER.hpp"

#include "EucalyptusParameter.hpp"

#include "technology_manager.hpp"
#include "parse_technology.hpp"
#include "technology_builtin.hpp"
#include "target_device.hpp"
#include "target_manager.hpp"

#if HAVE_XILINX || HAVE_ALTERA || HAVE_DESIGN_COMPILER
#include "RTL_characterization.hpp"
#endif
#if HAVE_EXPERIMENTAL
#include "core_generation.hpp"
#endif

#include "global_variables.hpp"
#include "cpu_time.hpp"
#include "utility.hpp"

int main(int argc, char *argv[])
{
   // Program name

   ParameterRef Param;

   try
   {
      // ---------- General options ------------ //
      // Synthesis cpu time
      long total_time;
      START_TIME(total_time);
      // General options register

      // ---------- Initialization ------------ //

      // ---------- Parameter parsing ------------ //
      long cpu_time;
      START_TIME(cpu_time);
      Param = ParameterRef(new EucalyptusParameter(argv[0]));

      switch(Param->exec(argc, argv))
      {
         case PARAMETER_NOTPARSED:
         {
            exit_code = PARAMETER_NOTPARSED;
            throw "Bad Parameters format";
         }
         case EXIT_SUCCESS:
         {
            if(not (Param->getOption<bool>(OPT_no_clean)))
            {
               boost::filesystem::remove_all(Param->getOption<std::string>(OPT_output_temporary_directory));
            }
            return EXIT_SUCCESS;
         }
         case PARAMETER_PARSED:
         {
            exit_code = EXIT_FAILURE;
            break;
         }
         default:
         {
            THROW_ERROR("Bad Parameters parsing");
         }
      }

      int debug_level = Param->getOption<int>(OPT_debug_level);
      int output_level = Param->getOption<int>(OPT_output_level);
      STOP_TIME(cpu_time);
      if(output_level >= OUTPUT_LEVEL_MINIMUM)
         Param->PrintFullHeader(std::cerr);
      PRINT_OUT_MEX(DEBUG_LEVEL_MINIMUM, output_level, " ==== Parameters parsed in " + print_cpu_time(cpu_time) + " seconds; ====\n");

      /// eucalyptus does not perform a clock constrained synthesis
      if(!Param->isOption(OPT_clock_period))
         Param->setOption(OPT_clock_period, 0.0);

      /// ==== Loading resource library ==== ///
      START_TIME(cpu_time);

      // Technology library manager
      technology_managerRef TM = technology_managerRef(new technology_manager(Param));

      ///creating the datastructure representing the target device
      unsigned int target_device = Param->getOption<unsigned int>(OPT_target_device_type);
      target_deviceRef device = target_device::create_device(static_cast<target_device::type_t>(target_device), Param, TM);

      device->set_parameter("clock_period", Param->getOption<double>(OPT_clock_period));

      /// adds predefined resources
      add_builtin_resources(TM, Param, device);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Built-in resources added");

      /// load specific device information
      device->load_devices(device);

      // if technology file is given, it is parsed
      if (Param->isOption("techfile"))
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "parsing the technology file...");
         read_technology_File(Param->getOption<std::string>("techfile").c_str(), TM, Param, device);
         PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "==== Technology file parsed ====");
      }
      THROW_ASSERT(TM, "Technology datastructure not defined");

      // ---------- Save XML file ------------ //
      if (debug_level >= DEBUG_LEVEL_PEDANTIC)
         write_xml_technology_File("technology.out.XML", TM, device->get_type());

      STOP_TIME(cpu_time);
      PRINT_OUT_MEX(DEBUG_LEVEL_MINIMUM, output_level, " ==== Resource library loaded in " + print_cpu_time(cpu_time) + " seconds; ====\n");

      target_managerRef target = target_managerRef(new target_manager(Param, TM, device));
#if HAVE_XILINX || HAVE_ALTERA || HAVE_DESIGN_COMPILER
      if (Param->getOption<bool>(OPT_estimate_library))
      {
         START_TIME(cpu_time);
         RTL_characterizationRef rtl_char = RTL_characterizationRef(new RTL_characterization(Param, target));
         rtl_char->exec();
         STOP_TIME(cpu_time);
         PRINT_OUT_MEX(DEBUG_LEVEL_MINIMUM, output_level, " ==== RTL characterization performed in " + print_cpu_time(cpu_time) + " seconds; ====\n");

         // ---------- Save XML file ------------ //
         if (debug_level >= DEBUG_LEVEL_PEDANTIC)
            write_xml_technology_File("technology.out.XML", TM, device->get_type());
      }
#endif
#if HAVE_EXPERIMENTAL
      if (Param->isOption(OPT_import_ip_core))
      {
         START_TIME(cpu_time);
         std::string core_hdl = Param->getOption<std::string>(OPT_import_ip_core);
         core_generationRef core_gen = core_generationRef(new core_generation(Param));
         core_gen->convert_to_XML(core_hdl, device->get_type());
         STOP_TIME(cpu_time);
         PRINT_OUT_MEX(DEBUG_LEVEL_MINIMUM, output_level, " ==== Core generation performed in " + print_cpu_time(cpu_time) + " seconds; ====\n");
      }

      if (Param->isOption(OPT_export_ip_core))
      {
         START_TIME(cpu_time);
         std::string core_name = Param->getOption<std::string>(OPT_export_ip_core);
         core_generationRef core_gen = core_generationRef(new core_generation(Param));
         core_gen->export_core(TM, core_name);
         STOP_TIME(cpu_time);
         PRINT_OUT_MEX(DEBUG_LEVEL_MINIMUM, output_level, " ==== Core exported in " + print_cpu_time(cpu_time) + " seconds; ====\n");
      }
#endif
      STOP_TIME(total_time);
      PRINT_MSG(" ==== Total Execution Time: " + print_cpu_time(total_time) + " seconds; ====\n");

      if(not (Param->getOption<bool>(OPT_no_clean)))
      {
         boost::filesystem::remove_all(Param->getOption<std::string>(OPT_output_temporary_directory));
      }
      return EXIT_SUCCESS; // Eucalyptus tool has completed execution without errors
   }

   // exception catching
   catch (const char * str)
   {
      std::cerr << str << std::endl;
   }
   catch (const std::string& str)
   {
      std::cerr << str << std::endl;
   }
   catch (std::exception &e)
   {
      std::cerr << e.what() << std::endl;
   }
   catch (...)
   {
      std::cerr << "Unknown error type" << std::endl;
   }

   switch(exit_code)
   {
      case PARAMETER_NOTPARSED:
      {
         Param->PrintUsage(std::cout);
         break;
      }
      case EXIT_FAILURE:
      {
         Param->PrintBugReport(std::cout);
         break;
      }
      default:
      {

      }
   }
   if(Param && not (Param->getOption<bool>(OPT_no_clean)))
   {
      boost::filesystem::remove_all(Param->getOption<std::string>(OPT_output_temporary_directory));
   }
   return exit_code;

}

