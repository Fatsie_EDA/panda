/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file operations_graph_constructor.cpp
 * @brief This class provides methods to build a operations graph.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "operations_graph_constructor.hpp"

///Behavior include
#include "op_graph.hpp"

operations_graph_constructor::operations_graph_constructor(OpGraphsCollectionRef _og) :
   og(_og),
   op_graph(new OpGraph(og, -1))
{}


operations_graph_constructor::~operations_graph_constructor()
{
}


vertex operations_graph_constructor::getIndex(const std::string &source)
{
   if (index_map.find(source) != index_map.end())
      return index_map.find(source)->second;
   const vertex v_og = og->AddVertex(NodeInfoRef(new OpNodeInfo()));
   index_map[source] = v_og;
   GET_NODE_INFO(og, OpNodeInfo, v_og)->vertex_name = source;
   return index_map[source];
}


vertex operations_graph_constructor::CgetIndex(const std::string &source) const
{
   THROW_ASSERT(index_map.find(source) != index_map.end(), "Index with name " + source + " doesn't exist");
   return index_map.find(source)->second;
}

EdgeDescriptor operations_graph_constructor::AddEdge(const vertex source, const vertex dest, int selector)
{
   return og->AddEdge(source, dest, selector);
}


void operations_graph_constructor::RemoveEdge(const vertex source, const vertex dest, int selector)
{
   og->RemoveSelector(source, dest, selector);
}

void operations_graph_constructor::RemoveSelector(const EdgeDescriptor edge, const int selector)
{
   og->RemoveSelector(edge, selector);
}

void operations_graph_constructor::add_edge_info(const vertex src, const vertex tgt, const int selector, unsigned int NodeID)
{
   EdgeDescriptor e;
   bool inserted;
   boost::tie(e, inserted) = boost::edge(src, tgt, *og);
   THROW_ASSERT(inserted, "Edge from " + GET_NAME(og, src) + " to " + GET_NAME(og, tgt) + " doesn't exists");
   get_edge_info<OpEdgeInfo>(e, *(og))->add_nodeID(NodeID, selector);
}

void operations_graph_constructor::add_operation(const std::string &src, const std::string &operation_t, unsigned int bb_index)
{
   THROW_ASSERT(src != "", "Vertex name empty");
   THROW_ASSERT(operation_t != "", "Operation empty");
   vertex current = getIndex(src);
   GET_NODE_INFO(og, OpNodeInfo, current)->bb_index = bb_index;
   GET_NODE_INFO(og, OpNodeInfo, current)->node_operation = operation_t;
   if (src == ENTRY)
      op_graph->GetOpGraphInfo()->entry_vertex = current;
   if (src == EXIT)
      op_graph->GetOpGraphInfo()->exit_vertex = current;
}

void operations_graph_constructor::add_type(const std::string &src, unsigned int type_t)
{
   THROW_ASSERT(src != "", "Vertex name empty");
   THROW_ASSERT(type_t != 0, "Type of vertex " + src + " is zero");
   vertex src_index = getIndex(src);
   if(GET_NODE_INFO(og, OpNodeInfo, src_index)->node_type != TYPE_GENERIC)
      GET_NODE_INFO(og, OpNodeInfo, src_index)->node_type |= type_t;
   else
      GET_NODE_INFO(og, OpNodeInfo, src_index)->node_type = type_t;
}

void operations_graph_constructor::SetNodeId(const std::string &source, unsigned int nodeID)
{
   THROW_ASSERT(source != "", "Vertex name empty");
   THROW_ASSERT(nodeID > 0, "NodeID not positive");
   const vertex operation = getIndex(source);
   THROW_ASSERT(op_graph->CGetOpNodeInfo(operation)->node_id == 0, "Trying to set node_id " + boost::lexical_cast<std::string>(nodeID) + " to vertex " + source + " that has already node_id " + boost::lexical_cast<std::string>(op_graph->CGetOpNodeInfo(operation)->node_id));
   op_graph->GetOpNodeInfo(operation)->node_id = nodeID;
   op_graph->GetOpGraphInfo()->tree_node_to_operation[nodeID] = operation;
}

void operations_graph_constructor::AddScalar(const vertex op_vertex, const unsigned int variable, const VariableAccessType access_type)
{
   op_graph->GetOpNodeInfo(op_vertex)->scalars[access_type].insert(variable);
}

void operations_graph_constructor::AddVirtual_DATADEPS(const vertex op_vertex, const unsigned int variable, const VariableAccessType access_type)
{
   op_graph->GetOpNodeInfo(op_vertex)->virtuals_datadeps[access_type].insert(variable);
}

void operations_graph_constructor::AddVirtual_ANTIDEPS(const vertex op_vertex, const unsigned int variable, const VariableAccessType access_type)
{
   op_graph->GetOpNodeInfo(op_vertex)->virtuals_antideps[access_type].insert(variable);
}

#if HAVE_EXPERIMENTAL
void operations_graph_constructor::AddAggregate(const vertex op_vertex, const unsigned int variable, const VariableAccessType access_type)
{
   op_graph->GetOpNodeInfo(op_vertex)->aggregates[access_type].insert(variable);
}
#endif

void operations_graph_constructor::add_parameter(const vertex& Ver, unsigned int Var)
{
   op_graph->GetOpNodeInfo(Ver)->actual_parameters.push_back(Var);
}

void operations_graph_constructor::add_called_function(const std::string &source, unsigned int called_function)
{
   op_graph->GetOpNodeInfo(getIndex(source))->called.insert(called_function);
}

void operations_graph_constructor::AddSourceCodeVariable(const vertex& Ver, unsigned int Vargc)
{
   op_graph->GetOpNodeInfo(Ver)->cited_variables.insert(Vargc);
}
