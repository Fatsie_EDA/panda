/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file constraint_manager.hpp
 * @brief Definition of the class representing generic design constraints
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef _CONSTRAINT_MANAGER_HPP_
#define _CONSTRAINT_MANAGER_HPP_

///Utility include
#include "refcount.hpp"
CONSTREF_FORWARD_DECL(Parameter);

class constraint_manager
{
   protected:

      ///class containing all the parameters
      const ParameterConstRef Param;

   public:

      /**
       * Constructor
       * @param _Param is the reference to the class containing all the parameters
       */
      constraint_manager(const ParameterConstRef _Param);

      /**
       * Destructor
       */
      virtual ~constraint_manager();

};
///refcount definition of the class
typedef refcount<constraint_manager> constraint_managerRef;
///constant refcount definition of the class
typedef refcount<const constraint_manager> constraint_managerConstRef;

#endif
