/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file application_manager.cpp
 * @brief Implementation of some methods to manage a generic C application
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_POLIXML_BUILT.hpp"
#include "config_HAVE_PRAGMA_BUILT.hpp"
#include "config_HAVE_SOURCE_CODE_STATISTICS_XML.hpp"

///Header include
#include "application_manager.hpp"

///Behavior includes
#include "behavioral_helper.hpp"
#include "call_graph_manager.hpp"
#include "call_graph.hpp"
#include "function_behavior.hpp"
#include "loop.hpp"
#include "loops.hpp"
#include "op_graph.hpp"

///Constants
#if HAVE_SOURCE_CODE_STATISTICS_XML
#include "source_code_statistics_xml.hpp"
#endif

///POLIXML includes
#include "polixml.hpp"
#include "xml_helper.hpp"

///GCC wrapper include
#include "gcc_wrapper.hpp"

#if HAVE_PRAGMA_BUILT
#include "pragma_manager.hpp"
#endif

///Tree include
#include "ext_tree_node.hpp"
#include "tree_helper.hpp"
#include "tree_manager.hpp"
#include "tree_reindex.hpp"

///Utility includes
#include "Parameter.hpp"
#include "constant_strings.hpp"
#include "exceptions.hpp"
#include "utility.hpp"


application_manager::application_manager(const FunctionExpanderConstRef function_expander, const bool _single_root_function, const bool _allow_recursive_functions, const ParameterConstRef _Param) :
   TM(new tree_manager(_Param)),
   call_graph_manager(new CallGraphManager(function_expander, _single_root_function, _allow_recursive_functions, TM, _Param)),
   Param(_Param),
   single_root_function(_single_root_function),
#if HAVE_PRAGMA_BUILT
   PM(new pragma_manager(application_managerRef(this, null_deleter()), _Param)),
#endif
   debug_level(_Param->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE))
{
   const std::unordered_set<std::string> original_file_names = Param->getOption<const std::unordered_set<std::string> >(OPT_input_file);
   std::unordered_set<std::string>::const_iterator original_file_name, original_file_name_end = original_file_names.end();
   for(original_file_name = original_file_names.begin(); original_file_name != original_file_name_end; original_file_name++)
   {
      //At the beginning the file to be processed is the original one, so keys and values of the map are the same
      input_files[*original_file_name] = *original_file_name;
   }
}

application_manager::~application_manager()
{
}

const ParameterConstRef application_manager::get_parameter() const
{
   return Param;
}

const tree_managerRef application_manager::get_tree_manager() const
{
   return TM;
}

CallGraphManagerRef application_manager::GetCallGraphManager()
{
   return call_graph_manager;
}

const CallGraphManagerConstRef application_manager::CGetCallGraphManager() const
{
   return call_graph_manager;
}

bool application_manager::is_function(unsigned int index) const
{
   return functions.find(index) != functions.end();
}

FunctionBehaviorRef application_manager::GetFunctionBehavior(unsigned int index)
{
   THROW_ASSERT(functions.find(index) != functions.end(), "There is no function with index " + STR(index));
   return functions.find(index)->second;
}

const FunctionBehaviorConstRef application_manager::CGetFunctionBehavior(unsigned int index) const
{
   THROW_ASSERT(functions.find(index) != functions.end(), "There is no function with index " + STR(index));
   return functions.find(index)->second;
}

const CustomSet<unsigned int>& application_manager::get_global_variables() const
{
   return global_variables;
}

void application_manager::add_global_variable(unsigned int var)
{
   global_variables.insert(var);
}

void application_manager::add_function(unsigned int current, unsigned int index, const BehavioralHelperRef helper, unsigned int node_stmt)
{
   add_function(index, helper);
   call_graph_manager->add_function(current, index, functions[index], node_stmt);
}

void application_manager::add_function(unsigned int index, const BehavioralHelperRef helper)
{
   if(functions.find(index) == functions.end())
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Adding function " + helper->get_function_name());
      THROW_ASSERT(helper, "expected a valid helper "+ boost::lexical_cast<std::string>(index));
      functions[index] = FunctionBehaviorRef(new FunctionBehavior(helper, Param));
      call_graph_manager->add_function(index, functions[index]);
   }
}

void application_manager::get_functions_with_body(std::list<unsigned int> &withBody) const
{
   call_graph_manager->GetReachedBodyFunctions(withBody);
}

unsigned int application_manager::get_first_function_id() const
{
   std::map<unsigned int, FunctionBehaviorRef>::const_iterator beg, end;
   for(beg = functions.begin(), end = functions.end(); beg != end; beg++)
   {
      if(beg->second->CGetBehavioralHelper()->has_implementation())
         return beg->first;
   }
   THROW_UNREACHABLE("Not found any function with implementation");
   return 0;
}

void application_manager::get_functions_without_body(std::list<unsigned int> &withoutBody) const
{
   call_graph_manager->GetReachedLibraryFunctions(withoutBody);
}

#if HAVE_PRAGMA_BUILT
const pragma_managerRef application_manager::get_pragma_manager() const
{
   return PM;
}
#endif

#if HAVE_SOURCE_CODE_STATISTICS_XML
void application_manager::dump_source_code_statistics(xml_element * local_root) const
{
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Dumping source code statistics");
   xml_element * source_code_lines = local_root->add_child_element(STR_XML_source_code_statistics_lines);
   size_t value = GccWrapper::GetSourceCodeLines(Param);
   WRITE_XNVM(value, boost::lexical_cast<std::string>(value), source_code_lines);

   std::list<unsigned int> functions_with_body;
   get_functions_with_body(functions_with_body);
   value = functions_with_body.size();
   xml_element * functions_with_body_node = local_root->add_child_element(STR_XML_source_code_statistics_function_with_body);
   WRITE_XNVM(value, boost::lexical_cast<std::string>(value), functions_with_body_node);

   const CallGraphConstRef call_graph = call_graph_manager->CGetCallGraph();

   size_t loop_number = 0;
   unsigned int maximum_depth = 0;
   unsigned int conditional_construct = 0;
   size_t call_sites = 0;
   size_t parameters_number = 0;
   size_t pointer_parameters_number = 0;
   std::list<unsigned int>::const_iterator f, f_end = functions_with_body.end();
   for(f = functions_with_body.begin(); f != f_end; f++)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Dumping function " + boost::lexical_cast<std::string>(*f));
      const FunctionBehaviorConstRef function_behavior = CGetFunctionBehavior(*f);
      const BehavioralHelperConstRef behavioral_helper = function_behavior->CGetBehavioralHelper();
      const std::list<LoopConstRef> loops = function_behavior->CGetLoops()->GetList();
      ///Minus one because of loop zero
      loop_number += loops.size() - 1;
      std::list<LoopConstRef>::const_iterator l, l_end = loops.end();
      for(l = loops.begin(); l != l_end; l++)
      {
         if((*l)->depth > maximum_depth)
            maximum_depth = (*l)->depth;
      }
      const OpGraphConstRef cfg = CGetFunctionBehavior(*f)->CGetOpGraph(FunctionBehavior::CFG);
      VertexIterator v, v_end;
      for(boost::tie(v, v_end) = boost::vertices(*cfg); v != v_end; v++)
      {
         if(GET_TYPE(cfg, *v) & (TYPE_SWITCH | TYPE_IF))
         {
            conditional_construct++;
         }
      }
      const vertex function_vertex = call_graph_manager->GetVertex(*f);
      OutEdgeIterator oe, oe_end;
      for(boost::tie(oe, oe_end) = boost::out_edges(function_vertex, *call_graph); oe != oe_end; oe++)
      {
         call_sites += call_graph->CGetFunctionEdgeInfo(*oe)->call_points.size();
      }
      const std::list<unsigned int> function_parameters = behavioral_helper->get_parameters();
      parameters_number += function_parameters.size();
      std::list<unsigned int>::const_iterator function_parameter, function_parameter_end = function_parameters.end();
      for(function_parameter = function_parameters.begin(); function_parameter != function_parameter_end; function_parameter++)
      {
         pointer_parameters_number += tree_helper::CountPointers(TM->CGetTreeNode(*function_parameter));
      }
   }
   value = loop_number;
   xml_element * loop_number_node = local_root->add_child_element(STR_XML_source_code_statistics_loop_number);
   WRITE_XNVM(value, boost::lexical_cast<std::string>(value), loop_number_node);

   value = maximum_depth;
   xml_element * maximum_depth_node = local_root->add_child_element(STR_XML_source_code_statistics_maximum_depth);
   WRITE_XNVM(value, boost::lexical_cast<std::string>(value), maximum_depth_node);

   value = conditional_construct;
   xml_element * conditional_construct_node = local_root->add_child_element(STR_XML_source_code_statistics_conditional_construct);
   WRITE_XNVM(value, boost::lexical_cast<std::string>(value), conditional_construct_node);

   value = call_sites;
   xml_element * call_sites_node = local_root->add_child_element(STR_XML_source_code_statistics_call_sites);
   WRITE_XNVM(value, boost::lexical_cast<std::string>(value), call_sites_node);

   xml_element * parameters_number_node = local_root->add_child_element(STR_XML_source_code_statistics_parameters_number);
   parameters_number_node->set_attribute(STR_XML_source_code_statistics_value, boost::lexical_cast<std::string>(parameters_number));

   xml_element * pointer_parameters_number_node = local_root->add_child_element(STR_XML_source_code_statistics_pointer_parameters_number);
   pointer_parameters_number_node->set_attribute(STR_XML_source_code_statistics_value, boost::lexical_cast<std::string>(pointer_parameters_number));

   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Dumped source code statistics");

}
#endif

unsigned int application_manager::get_produced_value(unsigned int fun_id, const vertex& v) const
{
   const OpGraphConstRef cfg = CGetFunctionBehavior(fun_id)->CGetOpGraph(FunctionBehavior::CFG);
   const unsigned int node_id = cfg->CGetOpNodeInfo(v)->node_id;
   return node_id ? get_produced_value(TM->get_tree_node_const(node_id)) : 0;
}

unsigned int application_manager::get_produced_value(const tree_nodeRef& tn) const
{
   switch (tn->get_kind())
   {
      case gimple_while_K:
      {
         gimple_while * we = GetPointer<gimple_while>(tn);
         return GET_INDEX_NODE(we->op0);
      }
      case gimple_cond_K:
      {
         gimple_cond * gc = GetPointer<gimple_cond>(tn);
         return GET_INDEX_NODE(gc->op0);
      }
      case gimple_label_K:
      case gimple_return_K:
      case gimple_call_K:
      case gimple_goto_K:
      case CASE_PRAGMA_NODES:
      case gimple_multi_way_if_K:
      case gimple_nop_K:
      {
         break;
      }
      case gimple_phi_K:
      {
         gimple_phi* gp = GetPointer<gimple_phi>(tn);
         return GET_INDEX_NODE(gp->res);
      }
      case gimple_switch_K:
      {
         gimple_switch * se = GetPointer<gimple_switch>(tn);
         return GET_INDEX_NODE(se->op0);
      }
      case gimple_assign_K:
      {
         gimple_assign* gm = GetPointer<gimple_assign>(tn);
         tree_nodeRef op0 = GET_NODE(gm->op0);
         if(gm->init_assignment || gm->clobber)
            break;
         else if(op0->get_kind() == array_ref_K )
            break;
         else if (op0->get_kind() == indirect_ref_K)
            break;
         else if (op0->get_kind() == misaligned_indirect_ref_K)
            break;
         else if (op0->get_kind() == mem_ref_K)
            break;
         else if (op0->get_kind() == target_mem_ref_K)
            break;
         else if (op0->get_kind() == target_mem_ref461_K)
            break;
         else
         {
            return GET_INDEX_NODE(gm->op0);
         }
      }
      case gimple_asm_K:
      {
         gimple_asm * ga = GetPointer<gimple_asm>(tn);
         if(ga->out)
            return GET_INDEX_NODE(ga->out);
         else
            return 0;
         break;
      }
      case binfo_K:
      case block_K:
      case call_expr_K:
      case case_label_expr_K:
      case constructor_K:
      case gimple_bind_K:
      case gimple_for_K:
      case gimple_pragma_K:
      case gimple_predict_K:
      case gimple_resx_K:
      case identifier_node_K:
      case ssa_name_K:
      case statement_list_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_BINARY_EXPRESSION:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_FAKE_NODES:
      case CASE_QUATERNARY_EXPRESSION:
      case CASE_TERNARY_EXPRESSION:
      case CASE_TYPE_NODES:
      case CASE_UNARY_EXPRESSION:
         THROW_ERROR("Operation not yet supported: " + std::string(tn->get_kind_text()));
      default:
         {
            THROW_UNREACHABLE("");
         }
   }
   return 0;
}

#if HAVE_CODESIGN
void application_manager::AddActorGraphManager(const unsigned int function_index, const ActorGraphManagerRef actor_graph_manager)
{
   THROW_ASSERT(const_output_actor_graphs.find(function_index) == const_output_actor_graphs.end(), "Function " + boost::lexical_cast<std::string>(function_index) + " has alread an actor graph manager associated");
   const_output_actor_graphs[function_index] = actor_graph_manager;
   output_actor_graphs[function_index] = actor_graph_manager;
}

const std::unordered_map<unsigned int, ActorGraphManagerConstRef> & application_manager::CGetActorGraphs() const
{
   return const_output_actor_graphs;
}

const ActorGraphManagerConstRef application_manager::CGetActorGraph(const unsigned int function_index) const
{
   THROW_ASSERT(const_output_actor_graphs.find(function_index) != const_output_actor_graphs.end(), "Actor graph for function " + boost::lexical_cast<std::string>(function_index) + " not found");
   return const_output_actor_graphs.find(function_index)->second;
}

ActorGraphManagerRef application_manager::GetActorGraph(const unsigned int function_index)
{
   THROW_ASSERT(output_actor_graphs.find(function_index) != output_actor_graphs.end(), "Actor graph for function " + boost::lexical_cast<std::string>(function_index) + " not found ");
   return output_actor_graphs.find(function_index)->second;
}

std::unordered_map<unsigned int, ActorGraphManagerRef> application_manager::GetActorGraphs()
{
   return output_actor_graphs;
}
#endif
