/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file application_manager.hpp
 * @brief Definition of the class representing a generic C application
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef _APPLICATION_MANAGER_HPP_
#define _APPLICATION_MANAGER_HPP_

///Autoheader
#include "config_HAVE_ACTOR_GRAPHS_BUILT.hpp"
#include "config_HAVE_ARCH_BUILT.hpp"
#include "config_HAVE_CODE_ESTIMATION_BUILT.hpp"
#include "config_HAVE_CODESIGN.hpp"
#include "config_HAVE_PRAGMA_BUILT.hpp"
#include "config_HAVE_SOURCE_CODE_STATISTICS_XML.hpp"

///Graph include
#include "graph.hpp"

///STD include
#include <string>

///STL include
#include <list>
#include <map>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

///Utility include
#include "custom_set.hpp"
#include "refcount.hpp"

CONSTREF_FORWARD_DECL(ActorGraphManager);
REF_FORWARD_DECL(ActorGraphManager);
REF_FORWARD_DECL(BehavioralHelper);
CONSTREF_FORWARD_DECL(CallGraphManager);
REF_FORWARD_DECL(CallGraphManager);
CONSTREF_FORWARD_DECL(FunctionBehavior);
REF_FORWARD_DECL(FunctionBehavior);
CONSTREF_FORWARD_DECL(FunctionExpander);
CONSTREF_FORWARD_DECL(Parameter);
REF_FORWARD_DECL(pragma_manager);
REF_FORWARD_DECL(tree_manager);
REF_FORWARD_DECL(tree_node);
class xml_element;

class application_manager
{
   protected:
      ///class representing the application information at low level
      const tree_managerRef TM;

      ///class representing the call graph of the application
      const CallGraphManagerRef call_graph_manager;

#if HAVE_CODESIGN
      ///The actor graphs (const version): key is the function indexi
      std::unordered_map<unsigned int, ActorGraphManagerConstRef> const_output_actor_graphs;

      ///The actor graphs: key is the function index
      std::unordered_map<unsigned int, ActorGraphManagerRef> output_actor_graphs;
#endif

      ///class containing all the parameters
      const ParameterConstRef Param;

      ///put into relation the index of the function and the corresponding data structure
      std::map<unsigned int, FunctionBehaviorRef> functions;

      ///set of global variables
      CustomSet<unsigned int> global_variables;

      ///True if only one root function has to be considered
      const bool single_root_function;

#if HAVE_PRAGMA_BUILT
      ///class representing the source code pragmas
      pragma_managerRef PM;
#endif

      ///debugging level of the class
      const int debug_level;

      /**
       * Returns the values produced by a vertex (recursive version)
       */
      unsigned int get_produced_value(const tree_nodeRef& tn) const;

   public:
      ///The original input file and the actual source code file to be elaborated
      std::unordered_map<std::string, std::string> input_files;

      /**
       * Constructor
       * @param function_expander is the expander used to determine if a called funciton has to be examinedi
       * @param single_root_function specifies if only one root function has to be considered
       * @param allow_recursive_functions specifies if recursive functions are allowed
       * @param _Param is the reference to the class containing all the parameters
       */
      application_manager(const FunctionExpanderConstRef function_expander, const bool single_root_function, const bool allow_recursive_functions, const ParameterConstRef _Param);

      /**
       * Destructor
       */
      virtual ~application_manager();

      /**
       * Returns the tree manager associated with the application
       */
      const tree_managerRef get_tree_manager() const;

      /**
       * Returns the call graph associated with the application
       */
      CallGraphManagerRef GetCallGraphManager();

      /**
       * Returns the call graph associated with the application
       */
      const CallGraphManagerConstRef CGetCallGraphManager() const;

      /**
       * Creates a new FunctionBehavior for function index when required and update the called_by_graph.
       * @param current is the current function
       * @param index is the index of the called function
       * @param helper is the BehavioralHelper associated with the called function
       */
      void add_function(unsigned int current, unsigned int index, const BehavioralHelperRef helper, unsigned int node_stmt = 0);

      /**
       * Creates a new FunctionBehavior for function index when required and update the called_by_graph. Note that index could not be called by any other function.
       * @param index is the index of the function
       * @param helper is the BehavioralHelper associated with the called function
       */
      void add_function(unsigned int index, const BehavioralHelperRef helper);

      /**
       * Returns true if the function (represented by the identified) has been already analyzed, i.e., there is a data structure associated with.
       * @param index is the identifier of the function
       */
      bool is_function(unsigned int index) const;

      /**
       * Returns the datastructure associated with the given identifier. This method returns an error if the function does not exist.
       * @param index is the identified of the function to be returned
       * @return the FunctionBehavior associated with the given function
       */
      FunctionBehaviorRef GetFunctionBehavior(unsigned int index);

      /**
       * Returns the datastructure associated with the given identifier. This method returns an error if the function does not exist.
       * @param index is the identified of the function to be returned
       * @return the FunctionBehavior associated with the given function
       */
      const FunctionBehaviorConstRef CGetFunctionBehavior(unsigned int index) const;

      /**
       * Returns the lists of functions whose implementation is present in the parsed
       * input specification (i.e. which has a non empty graph)
       * @param withBody is the list of functions to be returned
       */
      void get_functions_with_body(std::list<unsigned int>& withBody) const;

      /**
       * Returns the list of functions whose implementation is not present in the parsed
       * input specification (i.e. the ones with an empty Control Flow Graph)
       * @param withoutBody is the list of functions to be returned
       */
      void get_functions_without_body(std::list<unsigned int>& withoutBody) const;

      /**
       * Returns the first id of a function with the body
       */
      unsigned int get_first_function_id() const;

      /**
       * Adds a global variable
       * @param var is the global variable to be added
       */
      void add_global_variable(unsigned int var);

      /**
       * Returns the set of original global variables
       * @return a set containing the identified of the global variables
       */
      const CustomSet<unsigned int>& get_global_variables() const;

#if HAVE_SOURCE_CODE_STATISTICS_XML
      /**
       * Prints statistics about input source code
       * @param node is the xml node which the results have to be nested to
       */
      void dump_source_code_statistics(xml_element* local_root) const;
#endif

#if HAVE_PRAGMA_BUILT
      /**
       * Returns the reference to the manager for the source code pragmas
       */
      const pragma_managerRef get_pragma_manager() const;
#endif

      /**
       * Returns the value produced by a vertex
       */
      unsigned int get_produced_value(unsigned int fun_id, const vertex & v) const;

      /**
       * Returns the parameter datastructure
       */
      const ParameterConstRef get_parameter() const;

#if HAVE_CODESIGN
      /**
       * Returns the top actor graph for each function
       * @return the top actor graph for each function
       */
      const std::unordered_map<unsigned int, ActorGraphManagerConstRef> & CGetActorGraphs() const;

      /**
       * Returns the top actor graph for a function
       * @param function_index is the index of the function
       * @return its top actor graph
       */
      const ActorGraphManagerConstRef CGetActorGraph(const unsigned int function_index) const;

      /**
       * Returns the top actor graph for each function
       * @return the top actor graph for each function
       */
      std::unordered_map<unsigned int, ActorGraphManagerRef> GetActorGraphs();

      /**
       * Returns the top actor graph for a function
       * @param function_index is the index of the function
       * @return its top actor graph
       */
      ActorGraphManagerRef GetActorGraph(const unsigned int function_index);

      /**
       * Associate an actor graph manager with a function
       * @param function_index is the index of the function to which the manager has to be associated
       * @param actor_graph_manager is the actor graph manager to be associated
       */
      void AddActorGraphManager(const unsigned int function_index, const ActorGraphManagerRef actor_graph_manager);
#endif
};
///refcount definition of the class
typedef refcount<application_manager> application_managerRef;
///constant refcount definition of the class
typedef refcount<const application_manager> application_managerConstRef;

#endif
