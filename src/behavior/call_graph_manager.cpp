/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file call_graph.cpp
 * @brief Call graph hierarchy.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_HOST_PROFILING_BUILT.hpp"
#include "config_HAVE_POLIXML_BUILT.hpp"

///Header include
#include "call_graph_manager.hpp"

///Behavior include
#include "behavioral_helper.hpp"
#include "call_graph.hpp"
#include "function_behavior.hpp"
#include "loop.hpp"
#include "loops.hpp"
#include "op_graph.hpp"
#if HAVE_HOST_PROFILING_BUILT
#include "profiling_information.hpp"
#endif
///Boost include
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

///Graph include
#include "graph.hpp"
#include <boost/graph/graphviz.hpp>

///Paramter include
#include "Parameter.hpp"

///Tree include
#include "tree_manager.hpp"

///Utility include
#include <boost/date_time/posix_time/posix_time.hpp>

///Wrapper include
#include "gcc_wrapper.hpp"

///XML include
#if HAVE_POLIXML_BUILT
#include "xml_helper.hpp"
#include "polixml.hpp"
#include "xml_dom_parser.hpp"
#include "xml_document.hpp"
#endif

/**
 * Helper macro adding a call point to an edge of the call graph
 * @param g is the graph
 * @param e is the edge
 * @param newstmt is the call point to be added
 */
#define ADD_CALL_POINT(g, e, newstmt) \
   get_edge_info<function_graph_edge_info>(e, *(g))->call_points.insert(newstmt)


/**
 * @name function graph selector
 */
//@{
/// Data line selector
#define STD_SELECTOR 1 << 0
/// Clock line selector
#define FEEDBACK_SELECTOR 1 << 1
//@}

CallGraphManager::CallGraphManager(const FunctionExpanderConstRef _function_expander, const bool _single_root_function, const bool _allow_recursive_functions, const tree_managerConstRef _tree_manager, const ParameterConstRef _Param) :
   call_graphs_collection(new CallGraphsCollection(CallGraphInfoRef(new CallGraphInfo()), _Param)),
   call_graph(new CallGraph(call_graphs_collection, STD_SELECTOR | FEEDBACK_SELECTOR)),
   FCG(new CallGraphConstructor(index_to_vertex)),
   tree_manager(_tree_manager),
   single_root_function(_single_root_function),
   allow_recursive_functions(_allow_recursive_functions),
   Param(_Param),
   debug_level(_Param->get_class_debug_level(GET_CLASS(*this))),
   function_expander(_function_expander)
{}

CallGraphManager::~CallGraphManager()
{}

void CallGraphManager::add_function(unsigned int current, unsigned int index, const FunctionBehaviorRef function_behavior, unsigned int node_stmt)
{
   THROW_ASSERT(FCG, "this function cannot be called after the call graph has been consolidated");
   add_function(index, function_behavior);
   THROW_ASSERT(FCG->check_vertex(index), "this function should be already added to the call_graph");
   vertex source = FCG->get_vertex(current);
   vertex target = FCG->get_vertex(index);
   if(called_by[current].find(index) == called_by[current].end())
   {
      called_by[current].insert(index);
      FCG->add_edge(call_graphs_collection, source, target, STD_SELECTOR);
      try
      {
         std::list<vertex> topological_sort;
         CallGraph(call_graphs_collection, STD_SELECTOR).TopologicalSort(topological_sort);
      }
      catch (std::exception &e)
      {
         call_graphs_collection->RemoveSelector(source, target, STD_SELECTOR);
         call_graphs_collection->AddSelector(source, target, FEEDBACK_SELECTOR);
      }
   }
   EdgeDescriptor e;
   bool found;
   boost::tie(e,found) = boost::edge(source, target, *CGetCallGraph());
   get_edge_info<FunctionEdgeInfo, CallGraphsCollection>(e, *call_graphs_collection)->call_points.insert(node_stmt);
}

void CallGraphManager::add_function(unsigned int index, const FunctionBehaviorRef function_behavior)
{
   if(!FCG->check_vertex(index))
      FCG->add_vertex(call_graphs_collection, index);
   call_graph->GetCallGraphInfo()->behaviors[index] = function_behavior;
}

const CallGraphConstRef CallGraphManager::CGetAcyclicCallGraph() const
{
   return CallGraphRef(new CallGraph(call_graphs_collection, STD_SELECTOR));
}

const CallGraphConstRef CallGraphManager::CGetCallGraph() const
{
   return call_graph;
}
const CallGraphConstRef CallGraphManager::CGetCallSubGraph(const std::unordered_set<vertex> vertices) const
{
   return CallGraphConstRef(new CallGraph(call_graphs_collection, STD_SELECTOR | FEEDBACK_SELECTOR, vertices));
}

vertex CallGraphManager::GetVertex(const unsigned int index) const
{
   THROW_ASSERT(index_to_vertex.find(index) != index_to_vertex.end(), "Function " + boost::lexical_cast<std::string>(index) + " not stored");
   return index_to_vertex.find(index)->second;
}

unsigned int CallGraphManager::get_function(vertex node) const
{
   for(std::map<unsigned int, vertex>::const_iterator i = index_to_vertex.begin(); i != index_to_vertex.end(); i++)
      if (i->second == node)
         return i->first;
   return 0;
}

void CallGraphManager::replace_call_point(EdgeDescriptor e, const unsigned int orig, const unsigned int repl)
{
   std::set<unsigned int> & call_points = get_edge_info<FunctionEdgeInfo>(e, *call_graph)->call_points;
   call_points.erase(call_points.find(orig));
   call_points.insert(repl);
}

const std::set<unsigned int> CallGraphManager::get_called_by(unsigned int index) const
{
   if(called_by.find(index) != called_by.end())
      return called_by.find(index)->second;
   else
      return std::set<unsigned int>();
}

const std::unordered_set<unsigned int> CallGraphManager::get_called_by(const OpGraphConstRef cfg, const vertex & caller) const
{
   return cfg->CGetOpNodeInfo(caller)->called;
}

void CallGraphManager::ComputeRootFunctions()
{
   const unsigned int main_index = tree_manager->function_index("main");
   ///If top function option has been passed
   if(Param->isOption(OPT_top_function_name))
   {
      const std::string top_function_name = Param->getOption<std::string>(OPT_top_function_name);
      const unsigned int top_function_index = tree_manager->function_index(top_function_name);
      if(top_function_index == 0)
      {
         ///If tree_manager is not empty, top function must exist
         if(tree_manager->get_next_available_tree_node_id() != 1)
         {
            THROW_ERROR("Function " + top_function_name + " not found");
         }
      }
      else
      {
         root_functions.insert(top_function_index);
      }
   }
   ///If not -c option has been passed we assume that whole program has been passed, so the main must be present
   else if (not Param->getOption<bool>(OPT_gcc_c))
   {
      ///Main not found
      if(not main_index)
      {
         ///If tree_manager is not empty, function main must exist
         if(tree_manager->get_next_available_tree_node_id() != 1)
         {
            THROW_ERROR("No main function found, but -c option not passed");
         }
         ///Main not found, but call graph not yet built
         else
         {
         }
      }
      else
      {
         root_functions.insert(main_index);
      }
   }
   ///If there is the main, we return it
   else if(main_index)
   {
      root_functions.insert(main_index);
   }
   ///Return all the functions not called by any other function
   else
   {
      VertexIterator function, function_end;
      for (boost::tie(function, function_end) = boost::vertices(*call_graph); function != function_end; function++)
      {
         unsigned int fun_id = get_function(*function);
         THROW_ASSERT(fun_id>0, "expected a meaningful function id");
         const std::map<unsigned int, FunctionBehaviorRef> & behaviors = call_graph->CGetCallGraphInfo()->behaviors;
         if (boost::in_degree(*function, *call_graph) == 0 and behaviors.find(fun_id) != behaviors.end() and behaviors.find(fun_id)->second->CGetBehavioralHelper()->has_implementation())
         {
            if(single_root_function)
            {
               if(root_functions.empty())
               {
                  root_functions.insert(fun_id);
               }
               else if(fun_id > *(root_functions.begin()))
               {
                  root_functions.clear();
                  root_functions.insert(fun_id);
               }

            }
            else
            {
               root_functions.insert(fun_id);
            }
         }
      }
   }
}
void CallGraphManager::ComputeReachedFunctions()
{
   //call the depth-first-search
   ///ordered list of functions to be analyzed
   reached_body_functions.clear();
   reached_library_functions.clear();
   CalledFunctionsVisitor vis(allow_recursive_functions, this, reached_body_functions, reached_library_functions);
   std::vector<boost::default_color_type> color_vec(boost::num_vertices(*call_graph));
   const std::unordered_set<unsigned int>::const_iterator root_function_end = root_functions.end();
   for(std::unordered_set<unsigned int>::const_iterator root_function = root_functions.begin(); root_function != root_function_end; root_function++)
   {
      const vertex top_vertex = GetVertex(*root_function);
      boost::depth_first_visit(*call_graph, top_vertex, vis, boost::make_iterator_property_map(color_vec.begin(), boost::get(boost::vertex_index_t(), *call_graph), boost::white_color));
   }
}

const std::unordered_set<unsigned int> CallGraphManager::GetRootFunctions() const
{
   THROW_ASSERT(boost::num_vertices(*call_graph) == 0 or root_functions.size(), "Root functions have not yet been computed");
   return root_functions;
}

void CallGraphManager::GetReachedBodyFunctions(std::list<unsigned int> &f_list) const
{
   THROW_ASSERT(boost::num_vertices(*call_graph) == 0 or reached_body_functions.size(), "Root functions have not yet been computed");
   f_list = reached_body_functions;
}

void CallGraphManager::GetReachedLibraryFunctions(std::list<unsigned int> &f_list) const
{
   //We check for body functions since library functions can be empty
   THROW_ASSERT(boost::num_vertices(*call_graph) == 0 or reached_body_functions.size(), "Root functions have not yet been computed");
   f_list = reached_library_functions;
}

unsigned int CallGraphManager::GetRootFunction() const
{
   THROW_ASSERT(root_functions.size() == 1, "Number of root functions is " + boost::lexical_cast<std::string>(root_functions.size()));
   return *(root_functions.begin());
}

CallGraphConstructor::CallGraphConstructor(std::map<unsigned int, vertex>& functionID_vertex_map_) :
   functionID_vertex_map(functionID_vertex_map_)
{}

vertex CallGraphConstructor::add_vertex(const CallGraphsCollectionRef call_graphs_collection, unsigned int functionID)
{
   THROW_ASSERT(functionID_vertex_map.find(functionID) == functionID_vertex_map.end(), "this vertex has been already added " + boost::lexical_cast<std::string>(functionID));
   vertex v = call_graphs_collection->AddVertex(NodeInfoRef(new FunctionInfo()));
   GET_NODE_INFO(call_graphs_collection.get(), FunctionInfo, v)->nodeID = functionID;
   functionID_vertex_map[functionID] = v;
   return v;
}

bool CallGraphConstructor::check_vertex(unsigned int functionID) const
{
   return functionID_vertex_map.find(functionID) != functionID_vertex_map.end();
}

vertex CallGraphConstructor::get_vertex(unsigned int functionID) const
{
   THROW_ASSERT(functionID_vertex_map.find(functionID) != functionID_vertex_map.end(), "this vertex does not exist " + boost::lexical_cast<std::string>(functionID));
   return functionID_vertex_map.find(functionID)->second;
}

void CallGraphConstructor::add_edge(const CallGraphsCollectionRef call_graphs_collection, vertex v1, vertex v2, int selector) const
{
   call_graphs_collection->AddEdge(v1, v2, selector);
}

CalledFunctionsVisitor::CalledFunctionsVisitor(const bool _allow_recursive_functions, const CallGraphManager * _call_graph_manager, std::list<unsigned int> & _body_functions, std::list<unsigned int> & _library_functions) :
   allow_recursive_functions(_allow_recursive_functions),
   call_graph_manager(_call_graph_manager),
   body_functions(_body_functions),
   library_functions(_library_functions)
{}

void CalledFunctionsVisitor::back_edge(const EdgeDescriptor &e, const CallGraph &g)
{
   if(not allow_recursive_functions)
   {
      const std::map<unsigned int, FunctionBehaviorRef> & behaviors = g.CGetCallGraphInfo()->behaviors;
      vertex source = boost::source(e, g);
      vertex target = boost::target(e, g);
      THROW_ERROR("Recursive functions not yet supported: " + behaviors.find(call_graph_manager->get_function(source))->second->CGetBehavioralHelper()->get_function_name() + "-->" + behaviors.find(call_graph_manager->get_function(target))->second->CGetBehavioralHelper()->get_function_name());
   }
}

void CalledFunctionsVisitor::finish_vertex(const vertex & u, const CallGraph & g)
{
   unsigned int function_id = Cget_node_info<FunctionInfo, graph>(u, g)->nodeID ;
   if(g.CGetCallGraphInfo()->behaviors.find(function_id)->second->CGetBehavioralHelper()->has_implementation())
      body_functions.push_back(function_id);
   else
      library_functions.push_back(function_id);
}

void CallGraphManager::GetBodyFunctionRecursivelyCalledBy(const unsigned int function_id, std::unordered_set<unsigned int> & called_functions) const
{
   const vertex function_vertex = GetVertex(function_id);
   GetBodyFunctionRecursivelyCalledBy(function_vertex, called_functions);
}

void CallGraphManager::GetBodyFunctionRecursivelyCalledBy(const vertex function_vertex, std::unordered_set<unsigned int> & called_functions) const
{
   called_functions.insert(get_function(function_vertex));
   OutEdgeIterator oe, oe_end;
   for(boost::tie(oe, oe_end) = boost::out_edges(function_vertex, *call_graph); oe != oe_end; oe++)
   {
      const vertex target = boost::target(*oe, *call_graph);
      const unsigned int target_function_index = get_function(target);
      if(called_functions.find(target_function_index) == called_functions.end() and call_graph->GetCallGraphInfo()->behaviors[target_function_index]->CGetBehavioralHelper()->has_implementation())
      {
         GetBodyFunctionRecursivelyCalledBy(target, called_functions);
      }
   }
}

