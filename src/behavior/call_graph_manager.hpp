/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file call_graph_manager.hpp
 * @brief Wrapper to call graph
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef CALL_GRAPH_MANAGER_HPP
#define CALL_GRAPH_MANAGER_HPP

///Graph include
#include "graph.hpp"

///STL include
#include <list>
#include <set>
#include <unordered_set>

///Utility include
#include "refcount.hpp"

CONSTREF_FORWARD_DECL(application_manager);
CONSTREF_FORWARD_DECL(CallGraph);
REF_FORWARD_DECL(CallGraph);
REF_FORWARD_DECL(CallGraphsCollection);
REF_FORWARD_DECL(CallGraphConstructor);
CONSTREF_FORWARD_DECL(FunctionExpander);
REF_FORWARD_DECL(FunctionBehavior);
CONSTREF_FORWARD_DECL(OpGraph);
CONSTREF_FORWARD_DECL(Parameter);
CONSTREF_FORWARD_DECL(tree_manager);
REF_FORWARD_DECL(tree_node);

/**
 * This class manages the accesses to the CallGraph
 */
class CallGraphManager
{
   private:
      ///definition of the class that can access the private methods/attributes
      friend class application_manager;

      ///describe the relation called_by in term of graph.
      const CallGraphsCollectionRef call_graphs_collection;

      ///The view of call graph with all the edges
      const CallGraphRef call_graph;

      ///datastructure used to build the call_graph
      const CallGraphConstructorRef FCG;

      ///The tree manager
      const tree_managerConstRef tree_manager;

      ///put into relation function F_i and the list of functions called by F_i
      std::map<unsigned int, std::set<unsigned int> > called_by;

      ///put into relation function F_i and the list of functions called by F_i
      std::map<unsigned int, vertex> index_to_vertex;

      ///True if only
      const bool single_root_function;

      ///True if recursive calls are allowed
      const bool allow_recursive_functions;

      ///Root functions
      std::unordered_set<unsigned int> root_functions;

      /// source code functions directly or indirectly called by the root functions
      std::list<unsigned int> reached_body_functions;

      /// library functions directly or indirectly called by the root functions
      std::list<unsigned int> reached_library_functions;

      ///set of input parameters
      const ParameterConstRef Param;

      ///the debug level
      const int debug_level;

      /**
       * Creates a new FunctionBehavior for function index when required and update the called_by_graph.
       * @param current is the current function
       * @param index is the index of the called function
       * @param node_stmt is the id of the instruction where the call is performed
       */
      void add_function(unsigned int current, unsigned int index, const FunctionBehaviorRef function_behavior, unsigned int node_stmt = 0);

      /**
       * @param index is the index of the function
       * @param function_behavior is the corresponding function behavior
       */
      void add_function(unsigned int index, const FunctionBehaviorRef function_behavior);

      /**
       * Return the recursively called function by a function
       * @param function_vertex is the starting function to be considered
       * @param called_functions is where the result will be stored
       */
      void GetBodyFunctionRecursivelyCalledBy(const vertex function_vertex, std::unordered_set<unsigned int> & called_functions) const;

  public:
      /// Functor used to check if the analysis should consider the body of a function
      const FunctionExpanderConstRef function_expander;

      /**
       * Constructor. The data structure is initialized.
       * @param function_expander is the functor used to determine if a function has to be considered during construction of call graph
       * @param single_root_function specifies if only one root function has to be considered
       * @param allow_recursive_functions specifies if recursive functions are allowed
       * @param tree_manager is the tree manager
       * @param Param is the set of input parameters
       */
      CallGraphManager(const FunctionExpanderConstRef function_expander, const bool single_root_function, const bool allow_recursive_functions, const tree_managerConstRef tree_manager, const ParameterConstRef Param);

      /**
       * Destructor
       */
      ~CallGraphManager();

      /**
       * Return the call graph
       */
      CallGraphRef GetCallGraph();

      /**
       * Return an acyclic version of the call graph
       */
      const CallGraphConstRef CGetAcyclicCallGraph() const;

      /**
       * Return the call graph
       */
      const CallGraphConstRef CGetCallGraph() const;

      /**
       * Return a subset of the call graph
       * @param vertices is the subset of vertices to be considered
       */
      const CallGraphConstRef CGetCallSubGraph(const std::unordered_set<vertex> vertices) const;

      /**
       * Returns the set of functions called by a function
       * @param index is the index of the caller function
       */
      const std::set<unsigned int> get_called_by(unsigned int index) const;

      /**
       * Returns the set of functions called by an operation vertex
       * @param cfg is the pointer to the graph which the operation belongs to
       * @param caller is the caller vertex
       */
      const std::unordered_set<unsigned int> get_called_by(const OpGraphConstRef cfg, const vertex & caller) const;

      /**
       * Given a vertex of the call graph, this returns the index of the corresponding function
       * @param node is the vertex of the function
       * @return the index of the function
       */
      unsigned int get_function(vertex node) const;

      /**
       * Return the vertex given the function id
       * @param index is the function index
       * @return the corresponding vertex in the call graph
       */
      vertex GetVertex(const unsigned int index) const;

      /**
       * Replaces a call point
       * @param e is the edge in the call graph
       * @param orig is the old call graph point
       * @param repl is the new call graph point
       */
      void replace_call_point(EdgeDescriptor e, const unsigned int orig, const unsigned int repl);

      /**
       * Returns the root functions (i.e., the functions that are not called by any other ones
       * @return the set of top function
       */
      const std::unordered_set<unsigned int> GetRootFunctions() const;

      /**
       * Returns the source code functions called by the root functions
       * @return the set of top function
       */
      void GetReachedBodyFunctions(std::list<unsigned int> &f_list) const;

      /**
       * Returns the library functions called by the root functions
       * @return the set of top function
       */
      void GetReachedLibraryFunctions(std::list<unsigned int> &f_list) const;

      /**
       * Return a single root function
       */
      unsigned int GetRootFunction() const;

      /**
       * Return the recursively called function by a function
       * @param function_id is the index of the starting function to be considered
       * @param called_functions is where the result will be stored
       */
      void GetBodyFunctionRecursivelyCalledBy(const unsigned int function_id, std::unordered_set<unsigned int> & called_functions) const;

      /**
       * Compute the root functions
       */
      void ComputeRootFunctions();

      /**
       * Compute the functions directly or indirectly called by the root functions
       */
      void ComputeReachedFunctions();


};
typedef refcount<CallGraphManager> CallGraphManagerRef;
typedef refcount<const CallGraphManager> CallGraphManagerConstRef;

/**
 * Class used to build the graph describing the relation called_by
 */
struct CallGraphConstructor
{
   private:
      /// put into relation functionID with vertex
      std::map<unsigned int, vertex>& functionID_vertex_map;

   public:
      /**
       * Constructor.
      */
      CallGraphConstructor(std::map<unsigned int, vertex>& functionID_vertex_map_);

      /**
       * Add vertex to the call graph
       * @param fg is the call graph 
       * @param functionID is the function identifier
       */
      vertex add_vertex(const CallGraphsCollectionRef call_graphs_collection, unsigned int functionID);

      /**
       * return true in case the vertex has been already created
       * @param functionID is the function identifier
       */
      bool check_vertex(unsigned int functionID) const;

      /**
       * return a vertex of the graph given the functionID.
       * @param functionID is the function identifier
       */
      vertex get_vertex(unsigned int functionID) const;

      /**
       * add an edge to the call graph
       * @param fg is the call graph
       * @param v1 is the source vertex
       * @param v2 is the target vertex
       * @param selector is the type of the edge
       */
      void add_edge(const CallGraphsCollectionRef call_graph , vertex v1, vertex v2, int selector) const;
};

/**
 * Visitor to identify the list of called functions
 */
struct CalledFunctionsVisitor : public boost::default_dfs_visitor
{
   private:
      ///True if recursive calls are allowed
      const bool allow_recursive_functions;

      ///The call graph manager
      const CallGraphManager * call_graph_manager;

      ///The list of encountered body functions
      std::list<unsigned int> & body_functions;

      ///The list of encountered library functions
      std::list<unsigned int> & library_functions;

   public:
      /**
       * Constructor
       * @param allow_recursive_functions tells if recursive functions are allowed
       * @param call_graph_manager is the call graph manager
       * @param body_functions is where results will be stored
       * @param library_functions is where results will be stored
       */
      CalledFunctionsVisitor(const bool allow_recursive_functions, const CallGraphManager * call_graph_manager, std::list<unsigned int> & body_functions, std::list<unsigned int> & library_functions);

      void back_edge(const EdgeDescriptor & edge, const CallGraph & call_graph);

      /**
       * Function called when a vertex has been finished
       * @param u is the vertex
       * @param call_graph is the call graph
       */
      void finish_vertex(const vertex & u, const CallGraph & call_graph);
};
#endif
