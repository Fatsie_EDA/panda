/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file call_graph.cpp
 * @brief Call graph hierarchy.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_HOST_PROFILING_BUILT.hpp"
#include "config_HAVE_POLIXML_BUILT.hpp"

///Header include
#include "call_graph.hpp"

///Behavior include
#include "behavioral_helper.hpp"
#include "function_behavior.hpp"
#include "loop.hpp"
#include "loops.hpp"
#include "op_graph.hpp"
#if HAVE_HOST_PROFILING_BUILT
#include "profiling_information.hpp"
#endif
///Boost include
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

///Graph include
#include "graph.hpp"
#include <boost/graph/graphviz.hpp>

///Paramter include
#include "Parameter.hpp"

///Utility include
#include <boost/date_time/posix_time/posix_time.hpp>

///Wrapper include
#include "gcc_wrapper.hpp"

///XML include
#if HAVE_POLIXML_BUILT
#include "xml_helper.hpp"
#include "polixml.hpp"
#include "xml_dom_parser.hpp"
#include "xml_document.hpp"
#endif

/**
 * @name function graph selector
 */
//@{
/// Data line selector
#define STD_SELECTOR 1 << 0
/// Clock line selector
#define FEEDBACK_SELECTOR 1 << 1
//@}

FunctionInfo::FunctionInfo() :
   nodeID(0)
{}

FunctionEdgeInfo::FunctionEdgeInfo()
{}

CallGraphsCollection::CallGraphsCollection(const CallGraphInfoRef call_graph_info, const ParameterConstRef _parameters) :
   graphs_collection(call_graph_info, _parameters)
{}

CallGraphsCollection::~CallGraphsCollection()
{}

CallGraph::CallGraph(const CallGraphsCollectionRef call_graphs_collection, const int _selector) :
   graph(call_graphs_collection.get(), _selector)
{}

CallGraph::CallGraph(const CallGraphsCollectionRef call_graphs_collection, const int _selector, const std::unordered_set<vertex> _vertices) :
   graph(call_graphs_collection.get(), _selector, _vertices)
{}

CallGraph::~CallGraph()
{}

void CallGraph::WriteDot(const std::string & file_name) const
{
   const std::string output_directory = collection->parameters->getOption<std::string>(OPT_dot_directory);
   const std::string full_name = output_directory + file_name;
   const VertexWriterConstRef function_writer(new FunctionWriter(this));
   const EdgeWriterConstRef function_edge_writer(new FunctionEdgeWriter(this));
   InternalWriteDot<const FunctionWriter, const FunctionEdgeWriter>(full_name, function_writer, function_edge_writer);
}

FunctionWriter::FunctionWriter(const CallGraph * call_graph) :
   VertexWriter(call_graph, 0),
   behaviors(call_graph->CGetCallGraphInfo()->behaviors)
{}

void FunctionWriter::operator()(std::ostream & out, const vertex & v) const
{
   THROW_ASSERT(behaviors.find(Cget_node_info<FunctionInfo, graph>(v, *printing_graph)->nodeID) != behaviors.end(), "Function " + boost::lexical_cast<std::string>(Cget_node_info<FunctionInfo, graph>(v, *printing_graph)->nodeID) + " not found");
   const FunctionBehaviorRef FB = behaviors.find(Cget_node_info<FunctionInfo, graph>(v, *printing_graph)->nodeID)->second;
   out << "[shape=box, label=\"" << FB->CGetBehavioralHelper()->get_function_name();
   const std::set<unsigned int>& mem_nodeID = FB->get_function_mem();
   if (mem_nodeID.size())
   {
      out << "\\n(";
      bool first = true;
      for(std::set<unsigned int>::const_iterator l = mem_nodeID.begin(); l != mem_nodeID.end(); l++)
      {
         if (!first) out << " ";
         out << FB->CGetBehavioralHelper()->PrintVariable(*l);
         first = false;
      }
      out << ")";
   }
   out << "\"]" ;
}

FunctionEdgeWriter::FunctionEdgeWriter(const CallGraph * call_graph) :
   EdgeWriter(call_graph, 0),
   behaviors(call_graph->CGetCallGraphInfo()->behaviors)
{}

FunctionEdgeWriter::~FunctionEdgeWriter()
{}

void FunctionEdgeWriter::operator()(std::ostream& out, const EdgeDescriptor& e) const
{
   if (STD_SELECTOR & printing_graph->GetSelector(e))
      out << "[color=blue, label=\"";
   else if (FEEDBACK_SELECTOR & printing_graph->GetSelector(e))
      out << "[color=red, label=\"";
   else
      THROW_ERROR(std::string("InconsistentDataStructure"));
   const std::set<unsigned int> & call_points = Cget_edge_info<FunctionEdgeInfo, graph>(e, *printing_graph)->call_points;
   std::set<unsigned int>::const_iterator s, s_end = call_points.end();
   for(s = call_points.begin(); s != s_end; s++)
   {
      out << *s << " ";
   }
   out << "\"]";
}

