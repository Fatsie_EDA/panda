/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file op_graph.cpp
 * @brief Data stuctures used in operations graph
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "op_graph.hpp"

///Behavior include
#include "behavioral_helper.hpp"
#include "behavioral_writer_helper.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <fstream>

///tree includes
#include "tree_manager.hpp"
#include "tree_node.hpp"

///Utility include
#include <boost/filesystem/path.hpp>

OpEdgeInfo::OpEdgeInfo()
{}

OpEdgeInfo::~OpEdgeInfo()
{}

bool OpEdgeInfo::FlgEdgeT() const
{
   if(labels.find(FLG_SELECTOR) == labels.end())
      return false;
   return labels.find(FLG_SELECTOR)->second.find(T_COND) != labels.find(FLG_SELECTOR)->second.end();
}

bool OpEdgeInfo::FlgEdgeF() const
{
   if(labels.find(FLG_SELECTOR) == labels.end())
      return false;
   return labels.find(FLG_SELECTOR)->second.find(F_COND) != labels.find(FLG_SELECTOR)->second.end();
}

OpNodeInfo::OpNodeInfo() :
   node_id(0),
   bb_index(0),
   cer(0)
{
   ///This is necessary to be sure that the set exists (even if empty)
   scalars[AT_USE] = CustomSet<unsigned int>();
   scalars[AT_DEF] = CustomSet<unsigned int>();
   scalars[AT_ADDRESS] = CustomSet<unsigned int>();
   virtuals_datadeps[AT_USE] = CustomSet<unsigned int>();
   virtuals_datadeps[AT_DEF] = CustomSet<unsigned int>();
   virtuals_datadeps[AT_ADDRESS] = CustomSet<unsigned int>();
   virtuals_antideps[AT_USE] = CustomSet<unsigned int>();
   virtuals_antideps[AT_DEF] = CustomSet<unsigned int>();
   virtuals_antideps[AT_ADDRESS] = CustomSet<unsigned int>();
#if HAVE_EXPERIMENTAL
   aggregates[AT_USE] = CustomSet<unsigned int>();
   aggregates[AT_DEF] = CustomSet<unsigned int>();
   aggregates[AT_ADDRESS] = CustomSet<unsigned int>();
#endif
}


OpNodeInfo::~OpNodeInfo()
{}

const CustomSet<unsigned int> & OpNodeInfo::GetScalarVariables(const VariableAccessType access_type) const
{
   return scalars.find(access_type)->second;
}

const CustomSet<unsigned int> & OpNodeInfo::GetVirtualVariables_DATADEPS(const VariableAccessType access_type) const
{
   return virtuals_datadeps.find(access_type)->second;
}

const CustomSet<unsigned int> & OpNodeInfo::GetVirtualVariables_ANTIDEPS(const VariableAccessType access_type) const
{
   return virtuals_antideps.find(access_type)->second;
}

#if HAVE_EXPERIMENTAL
const CustomSet<unsigned int> & OpNodeInfo::GetAggregateVariables(const VariableAccessType access_type) const
{
   return aggregates.find(access_type)->second;
}
#endif

void PrintVariablesList(std::ostream & stream, const std::string name, const CustomSet<unsigned int> variables, const BehavioralHelperConstRef behavioral_helper, const bool dotty_format)
{
   if(variables.size())
   {
      stream << name << ":";
      CustomSet<unsigned int>::const_iterator variable, variable_end = variables.end();
      for(variable = variables.begin(); variable != variable_end; variable++)
      {
         stream << " " << behavioral_helper->PrintVariable(*variable) << "(" << *variable << ")";
      }
      stream << (dotty_format ? "\\n" : "\n");
   }
}

void OpNodeInfo::Print(std::ostream & stream, const BehavioralHelperConstRef behavioral_helper, bool dotty_format) const
{
   PrintVariablesList(stream, "source code variables", cited_variables, behavioral_helper, dotty_format);
   PrintVariablesList(stream, "scalar uses", scalars.find(AT_USE)->second, behavioral_helper, dotty_format);
   PrintVariablesList(stream, "virtual uses DATADEPS", virtuals_datadeps.find(AT_USE)->second, behavioral_helper, dotty_format);
   PrintVariablesList(stream, "virtual uses ANTIDEPS", virtuals_antideps.find(AT_USE)->second, behavioral_helper, dotty_format);
#if HAVE_EXPERIMENTAL
   PrintVariablesList(stream, "aggregate uses", aggregates.find(AT_USE)->second, behavioral_helper, dotty_format);
#endif
   PrintVariablesList(stream, "scalar addr", scalars.find(AT_ADDRESS)->second, behavioral_helper, dotty_format);
   PrintVariablesList(stream, "virtual addr DATADEPS", virtuals_datadeps.find(AT_ADDRESS)->second, behavioral_helper, dotty_format);
   PrintVariablesList(stream, "virtual addr ANTIDEPS", virtuals_antideps.find(AT_ADDRESS)->second, behavioral_helper, dotty_format);
#if HAVE_EXPERIMENTAL
   PrintVariablesList(stream, "aggregate defs", aggregates.find(AT_DEF)->second, behavioral_helper, dotty_format);
#endif
   PrintVariablesList(stream, "scalar defs", scalars.find(AT_DEF)->second, behavioral_helper, dotty_format);
   PrintVariablesList(stream, "virtual defs DATADEPS", virtuals_datadeps.find(AT_DEF)->second, behavioral_helper, dotty_format);
   PrintVariablesList(stream, "virtual defs ANTIDEPS", virtuals_antideps.find(AT_DEF)->second, behavioral_helper, dotty_format);
#if HAVE_EXPERIMENTAL
   PrintVariablesList(stream, "aggregate addr", aggregates.find(AT_ADDRESS)->second, behavioral_helper, dotty_format);
#endif
}

OpGraphInfo::OpGraphInfo(const BehavioralHelperConstRef _BH) :
   BH(_BH)
{}

OpGraphInfo::~OpGraphInfo()
{}

OpGraphsCollection::OpGraphsCollection(const OpGraphInfoRef _info, const ParameterConstRef _parameters) :
   graphs_collection(RefcountCast<GraphInfo>(_info), _parameters)
{}

OpGraphsCollection::~OpGraphsCollection()
{}

#if HAVE_UNORDERED
OpVertexSet::OpVertexSet(const OpGraphConstRef) :
   std::unordered_set<vertex>()
{}

#else
OpVertexSorter::OpVertexSorter(const OpGraphConstRef _op_graph) :
   op_graph(_op_graph)
{}

bool OpVertexSorter::operator()(const vertex x, const vertex y) const
{
   return GET_NAME(op_graph, x) < GET_NAME(op_graph, y);
}

OpVertexSet::OpVertexSet(OpGraphConstRef _op_graph) :
   std::set<vertex, OpVertexSorter>(OpVertexSorter(_op_graph))
{}
#endif

OpGraph::OpGraph(OpGraphsCollectionRef _op_graphs_collection, int _selector) :
   graph(_op_graphs_collection.get(), _selector)
{}

OpGraph::OpGraph(OpGraphsCollectionRef _op_graphs_collection, int _selector, std::unordered_set<boost::graph_traits<OpGraphsCollection>::vertex_descriptor > _sub) :
   graph(_op_graphs_collection.get(), _selector, _sub)
{}

OpGraph::~OpGraph()
{}

void OpGraph::WriteDot(const std::string & file_name, const int detail_level) const
{
   const BehavioralHelperConstRef helper = CGetOpGraphInfo()->BH;
   std::string output_directory = collection->parameters->getOption<std::string>(OPT_dot_directory) + "/" + helper->get_function_name() + "/";
   if (!boost::filesystem::exists(output_directory))
      boost::filesystem::create_directories(output_directory);
   const std::string full_name = output_directory + file_name;
   const VertexWriterConstRef op_label_writer(new OpWriter(this, detail_level));
   const EdgeWriterConstRef op_edge_property_writer(new OpEdgeWriter(this));
   InternalWriteDot<const OpWriter, const OpEdgeWriter>(full_name, op_label_writer, op_edge_property_writer);
}

std::unordered_map<vertex, OpVertexSet> OpGraph::GetSrcVertices(const OpVertexSet & toCheck, int edgeType) const
{
   null_deleter null;
   OpGraphConstRef thisRef(this, null);
   std::unordered_map<vertex, OpVertexSet> retVal;
   OpVertexSet::const_iterator vertIter, vertIterEnd;
   for(vertIter = toCheck.begin(), vertIterEnd = toCheck.end(); vertIter != vertIterEnd; vertIter++)
   {
      InEdgeIterator inE, inEEnd;
      for(boost::tie(inE, inEEnd) = boost::in_edges(*vertIter, *this); inE != inEEnd; inE++)
      {
         int origEdgeType = GetSelector(*inE);
         if((edgeType & origEdgeType) != 0)
         {
            const vertex src = boost::source(*inE, *this);
            if(retVal.find(src) == retVal.end())
            {
               retVal.insert(std::pair<vertex, OpVertexSet>(src, OpVertexSet(thisRef)));
            }
            retVal.find(src)->second.insert(*vertIter);
         }
      }
   }
   return retVal;
}

