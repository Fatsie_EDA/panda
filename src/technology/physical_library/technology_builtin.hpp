/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file technology_builtin.hpp
 * @brief function that add built-in resources to the technology manager.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef TECHNOLOGY_BUILTIN_HPP
#define TECHNOLOGY_BUILTIN_HPP

#include "refcount.hpp"

#include <map>
#include <string>

/**
 * @name forward declarations
*/
//@{
/// RefCount type definition of the technology_manager class structure
REF_FORWARD_DECL(technology_manager);
REF_FORWARD_DECL(target_device);
CONSTREF_FORWARD_DECL(Parameter);
//@}

void add_builtin_resources(const technology_managerRef TM, const ParameterConstRef Param, const target_deviceRef device);

/**
 * @name BUILT-IN ports and modules
*/
//@{
// Logic Ports
#define AND_GATE_STD       "AND_GATE"
#define NAND_GATE_STD      "NAND_GATE"
#define OR_GATE_STD        "OR_GATE"
#define NOR_GATE_STD       "NOR_GATE"
#define XOR_GATE_STD       "XOR_GATE"
#define XNOR_GATE_STD      "XNOR_GATE"
#define NOT_GATE_STD       "NOT_GATE"
#define DFF_GATE_STD       "DFF_GATE"
#define BUFF_GATE_STD      "BUFF_GATE"

#define MUX_GATE_STD       "MUX_GATE"
#define DEMUX_GATE_STD     "DEMUX_GATE"
#define MULTIPLIER_STD     "mult_expr_FU"
#define UI_MULTIPLIER_STD  "ui_mult_expr_FU"
#define UI_CONST_MULTIPLIER_STD "ui_const_mult_expr_FU"
#define ADDER_STD          "plus_expr_FU"
#define UI_ADDER_STD       "ui_plus_expr_FU"
#define CONCAT4_STD        "concat_4_constructor"
#define SIGNED_BITFIELD_FU_STD "Sbitfield_FU"
#define UNSIGNED_BITFIELD_FU_STD "Ubitfield_FU"
#define CONSTANT_STD       "constant_value"
#define ASSIGN_SIGNED_STD  "ASSIGN_SIGNED_FU"
#define ASSIGN_UNSIGNED_STD "ASSIGN_UNSIGNED_FU"
#define ASSIGN_REAL_STD    "ASSIGN_REAL_FU"
#define ASSIGN_VECTOR_BOOL_STD "ASSIGN_VECTOR_BOOL_FU"
#define ADDR_EXPR_STD      "addr_expr_FU"
#define ASSERT_EXPR_SIGNED_STD "assert_expr_FU"
#define ASSERT_EXPR_UNSIGNED_STD "ui_assert_expr_FU"
#define ASSERT_EXPR_REAL_STD "fp_assert_expr_FU"
#define MEMORY_STD         "MEMORY_CTRL"
#define MEMORY_STD_D10     "MEMORY_CTRL_D10"
#define MEMORY_STD_D11     "MEMORY_CTRL_D11"
#define MEMORY_STD_D21     "MEMORY_CTRL_D21"
#define MEMORY_STDN        "MEMORY_CTRLN"
#define MEMORY_STDN_D10    "MEMORY_CTRLN_D10"
#define MEMORY_STDN_D11    "MEMORY_CTRLN_D11"
#define MEMORY_STDN_D21    "MEMORY_CTRLN_D21"
#define BMEMORY_STD        "BMEMORY_CTRL"
#define BMEMORY_STD_D10    "BMEMORY_CTRL_D10"
#define BMEMORY_STD_D11    "BMEMORY_CTRL_D11"
#define BMEMORY_STD_D21    "BMEMORY_CTRL_D21"
#define BMEMORY_STDN       "BMEMORY_CTRLN"
#define BMEMORY_STDN_D10   "BMEMORY_CTRLN_D10"
#define BMEMORY_STDN_D11   "BMEMORY_CTRLN_D11"
#define BMEMORY_STDN_D21   "BMEMORY_CTRLN_D21"
#define MEMCPY_STD         "__builtin_memcpy"
#define ARRAY_1D_STD_BRAM  "ARRAY_1D_STD_BRAM"
#define ARRAY_1D_STD_BRAM_SDS  "ARRAY_1D_STD_BRAM_SDS"
#define ARRAY_1D_STD_DISTRAM "ARRAY_1D_STD_DISTRAM"
#define ARRAY_1D_STD_BRAM_N1 "ARRAY_1D_STD_BRAM_N1"
#define ARRAY_1D_STD_BRAM_N1_SDS "ARRAY_1D_STD_BRAM_N1_SDS"
#define ARRAY_1D_STD_BRAM_NN "ARRAY_1D_STD_BRAM_NN"
#define ARRAY_1D_STD_BRAM_NN_SDS "ARRAY_1D_STD_BRAM_NN_SDS"
#define STD_BRAM           "STD_BRAM"
#define STD_BRAMN          "STD_BRAMN"
#define STD_PRINTF         "PRINTF"
#define STD_PRINTFN        "PRINTFN"
#define MEMLOAD_STD        "__builtin_memload"
#define MEMSTORE_STD       "__builtin_memstore"
#define MEMLOAD_STDN        "__builtin_memload_N"
#define MEMSTORE_STDN       "__builtin_memstore_N"
#define BUILTIN_EXIT_STD   "__builtin_exit"
#define BUILTIN_ABORT_STD  "__builtin_abort"
#define BUILTIN_WAIT_CALL_STD  "__builtin_wait_call"
#define PROXY_CTRL         "PROXY_CTRL"
#define PROXY_CTRLN        "PROXY_CTRLN"

#define UUDATA_CONVERTER_STD "UUdata_converter_FU"
#define IUDATA_CONVERTER_STD "IUdata_converter_FU"
#define UIDATA_CONVERTER_STD "UIdata_converter_FU"
#define IIDATA_CONVERTER_STD "IIdata_converter_FU"
#define FFDATA_CONVERTER_STD "FFdata_converter_FU"
#define SF_FFDATA_CONVERTER_32_64_STD "sf_FFdata_converter_FU_32_64"
#define SF_FFDATA_CONVERTER_64_32_STD "sf_FFdata_converter_FU_64_32"

#define UUCONVERTER_EXPR_STD "UUconvert_expr_FU"
#define IUCONVERTER_EXPR_STD "IUconvert_expr_FU"
#define UICONVERTER_EXPR_STD "UIconvert_expr_FU"
#define IICONVERTER_EXPR_STD "IIconvert_expr_FU"

//For distributed controller
#define CE_STD             "CE_FU"
#define CE_FSM             "CE_FSM"
#define CE_BYPASS          "CE_BYPASS"
#define JOIN_STD           "JOIN_FU"
#define PT_STD             "PT_FU"
#define START_STD          "START_FU"
#define COND_STD           "COND_FU"
#define SWITCH_CASE_STD    "SWITCH_CASE_FU"
#define MC_STD             "MC_FU"
#define FC_STD             "FC_FU"

/// simple register without reset
#define register_STD     "register_STD"
/// register with synchronous reset
#define register_SR     "register_SR"
/// register with asynchronous reset
#define register_AR     "register_AR"
/// register with asynchronous reset
#define register_AR_NORETIME     "register_AR_NORETIME"
/// register with synchronous enable
#define register_SE    "register_SE"
/// register with synchronous reset and synchronous enable
#define register_SRSE    "register_SRSE"
/// register with asynchronous reset and synchronous enable
#define register_ARSE   "register_ARSE"
/// register with synchronized asynchronous reset and synchronous enable
#define register_SARSE  "register_SARSE"

///artificial functional units
#define GIMPLE_RETURN_STD         "gimple_return_FU"
#define GIMPLE_PHI_STD            "gimple_phi_FU"
#define GIMPLE_ASM_STD            "gimple_asm_FU"
#define GIMPLE_LABEL_STD          "gimple_label_FU"
#define GIMPLE_GOTO_STD           "gimple_goto_FU"
#define GIMPLE_NOP_STD            "gimple_nop_FU"
#define GIMPLE_PRAGMA_STD         "gimple_pragma_FU"
#define READ_COND_STD             "read_cond_FU"
#define MULTI_READ_COND_STD       "multi_read_cond_FU"
#define SWITCH_COND_STD           "switch_cond_FU"
#define ENTRY_STD                 "entry_FU"
#define EXIT_STD                  "exit_FU"
#define NOP_STD                   "nop_FU"
#define VIEW_CONVERT_STD          "view_convert_expr_FU"

///standard name for ports
#define CLOCK_PORT_NAME "clock"
#define WENABLE_PORT_NAME "wenable"
#define START_PORT_NAME "start_port"
#define RESET_PORT_NAME "reset"
#define DONE_PORT_NAME "done_port"
#define RETURN_PORT_NAME "return_port"

///FPGA modules
#define LUT_GATE_STD  "LUT"
#define IBUF_GATE_STD  "IBUF"
#define OBUF_GATE_STD  "OBUF"

//@}

/**
  this class provides some methods to convert a standard gate to a verilog built-in object
*/
class builtin_verilog_gates
{
      /// map putting into relation standard gates with the corresponding built-in verilog statements.
      static
      std::map<std::string, std::string> from_std_gate_to_verilog;

   public:
      /// check if the gate is a built-in verilog gate.
      /// @param gate is the name of the gate.
      static
      bool is_builtin(const std::string &gate);
      /// return the built-in verilog keyword given the standard gate name
      /// @param gate is the name of the gate
      static
      std::string get_verilog_builtin(const std::string &gate);
      /// create the relation between standard gate and verilog keyword.
      /// @param std_gate_name is the name of the standard gate.
      /// @param verilog_keyword is the name of the verilog operator used by the verilog backend
      static
      void add_builtin(std::string std_gate_name, std::string verilog_keyword);
};

#endif
