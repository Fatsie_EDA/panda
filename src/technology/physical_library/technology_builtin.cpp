/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file technology_builtin.cpp
 * @brief Add to the technology manager a set of standard resources: AND_GATE, OR_GATE, NOT_GATE... etc.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_FROM_C_BUILT.hpp"
#include "config_HAVE_KOALA_BUILT.hpp"

#include "technology_builtin.hpp"

#include "technology_manager.hpp"
#include "library_manager.hpp"
#include "technology_node.hpp"
#include "target_device.hpp"

#include "structural_manager.hpp"
#include "structural_objects.hpp"
#include "NP_functionality.hpp"

#include "utility.hpp"
#include "exceptions.hpp"

#include "fileIO.hpp"
#include "polixml.hpp"
#include "xml_dom_parser.hpp"

#include "Parameter.hpp"
#include "constant_strings.hpp"

#include <iostream>
#include <string>
#include <boost/filesystem.hpp>

#if HAVE_FROM_C_BUILT
#include "op_graph.hpp"
#endif

void add_builtin_resources(const technology_managerRef TM, const ParameterConstRef Param, const target_deviceRef device)
{
   /// Load default resources
   const char * builtin_resources_data[] = {
   #include "STD_IPs.data"
       ,
   #include "HLS_IPs.data"
       ,
   #include "MEM_IPs.data"
      ,
   #include "FP_IPs.data"
      ,
   #include "COMPLEX_IPs.data"
      ,
   #include "VEC_IPs.data"
      ,
   #include "wishbone_IPs.data"
      ,
   #include "SF_IPs.data"
#if HAVE_EXPERIMENTAL
      ,
   #include "PC_IPs.data"
#endif
   };

   try
   {
      for (size_t i = 0; i < sizeof(builtin_resources_data)/sizeof(char *); ++i)
      {
         fileIO_istreamRef default_stream = fileIO_istream_open_from_string(builtin_resources_data[i]);
         xml_dom_parser parser;
         parser.parse_stream(*default_stream);
         if (parser)
         {
            //Walk the tree:
            const xml_element* node = parser.get_document()->get_root_node(); //deleted by DomParser.
            technology_manager::xload(builtin_resources_data[i], node, TM, Param, device);
         }
      }
   }
   catch (const char * msg)
   {
      std::cerr << msg << std::endl;
      THROW_ERROR("Error during parsing of technology file");
   }
   catch (const std::string & msg)
   {
      std::cerr << msg << std::endl;
      THROW_ERROR("Error during parsing of technology file");
   }
   catch (const std::exception& ex)
   {
      std::cout << "Exception caught: " << ex.what() << std::endl;
      THROW_ERROR("Error during parsing of technology file");
   }
   catch ( ... )
   {
      std::cerr << "unknown exception" << std::endl;
      THROW_ERROR("Error during parsing of technology file");
   }

   std::string fu_name;
   structural_objectRef top;
   structural_managerRef CM;
   structural_type_descriptorRef b_type = structural_type_descriptorRef(new structural_type_descriptor("bool", 0));
   structural_type_descriptorRef module_type;
   std::string NP_parameters;
   std::string Library;

#if HAVE_KOALA_BUILT
   //LUT
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = LUT_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port_vector("I", port_o::IN, port_vector_o::PARAMETRIC_PORT, top, b_type);
   CM->add_port("O", port_o::OUT, top, b_type);
   NP_parameters = fu_name + " in init";
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   TM->add_resource(FPGA_LIBRARY, fu_name, CM);

   //IBUF
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = IBUF_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port("I", port_o::IN, top, b_type);
   CM->add_port("O", port_o::OUT, top, b_type);
   NP_parameters = fu_name;
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   TM->add_resource(FPGA_LIBRARY, fu_name, CM);

   //OBUF
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = OBUF_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port("I", port_o::IN, top, b_type);
   CM->add_port("O", port_o::OUT, top, b_type);
   NP_parameters = fu_name;
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   TM->add_resource(FPGA_LIBRARY, fu_name, CM);
#endif

   Library = LIBRARY_STD;

   //AND
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = AND_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port_vector("in", port_o::IN, port_o::PARAMETRIC_PORT, top, b_type);
   CM->add_port("out1", port_o::OUT, top, b_type);
   NP_parameters = fu_name + " in";
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   CM->add_NP_functionality(top, NP_functionality::EQUATION, "out1=[*]");
   TM->add_resource(Library, fu_name, CM);
   builtin_verilog_gates::add_builtin(AND_GATE_STD, "and");

   //NAND
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = NAND_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port_vector("in", port_o::IN, port_o::PARAMETRIC_PORT, top, b_type);
   CM->add_port("out1", port_o::OUT, top, b_type);
   NP_parameters = fu_name + " in";
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   CM->add_NP_functionality(top, NP_functionality::EQUATION, "out1=![*]");
   TM->add_resource(Library, fu_name, CM);
   builtin_verilog_gates::add_builtin(NAND_GATE_STD, "nand");

   //OR
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = OR_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port_vector("in", port_o::IN, port_o::PARAMETRIC_PORT, top, b_type);
   CM->add_port("out1", port_o::OUT, top, b_type);
   NP_parameters = fu_name + " in";
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   CM->add_NP_functionality(top, NP_functionality::EQUATION, "out1=[+]");
   TM->add_resource(Library, fu_name, CM);
   builtin_verilog_gates::add_builtin(OR_GATE_STD, "or");

   //NOR
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = NOR_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port_vector("in", port_o::IN, port_o::PARAMETRIC_PORT, top, b_type);
   CM->add_port("out1", port_o::OUT, top, b_type);
   NP_parameters = fu_name + " in";
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   CM->add_NP_functionality(top, NP_functionality::EQUATION, "out1=![+]");
   TM->add_resource(Library, fu_name, CM);
   builtin_verilog_gates::add_builtin(NOR_GATE_STD, "nor");

   //XOR
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = XOR_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port_vector("in", port_o::IN, port_o::PARAMETRIC_PORT, top, b_type);
   CM->add_port("out1", port_o::OUT, top, b_type);
   NP_parameters = fu_name + " in";
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   CM->add_NP_functionality(top, NP_functionality::EQUATION, "out1=[^]");
   TM->add_resource(Library, fu_name, CM);
   builtin_verilog_gates::add_builtin(XOR_GATE_STD, "xor");

   //XNOR
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = XNOR_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port_vector("in", port_o::IN, port_o::PARAMETRIC_PORT, top, b_type);
   CM->add_port("out1", port_o::OUT, top, b_type);
   NP_parameters = fu_name + " in";
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   CM->add_NP_functionality(top, NP_functionality::EQUATION, "out1=![^]");
   TM->add_resource(Library, fu_name, CM);
   builtin_verilog_gates::add_builtin(XNOR_GATE_STD, "xnor");

   //NOT
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = NOT_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port("in1", port_o::IN, top, b_type);
   CM->add_port("out1", port_o::OUT, top, b_type);
   NP_parameters = fu_name;
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   CM->add_NP_functionality(top, NP_functionality::EQUATION, "out1=!in1");
   TM->add_resource(Library, fu_name, CM);
   builtin_verilog_gates::add_builtin(NOT_GATE_STD, "not");

   //DFF
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = DFF_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port("in1", port_o::IN, top, b_type);
   CM->add_port("out1", port_o::OUT, top, b_type);
   NP_parameters = fu_name;
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   TM->add_resource(Library, fu_name, CM);
   builtin_verilog_gates::add_builtin(DFF_GATE_STD, "dff");

   //BUFF
   CM = structural_managerRef(new structural_manager(Param));
   fu_name = BUFF_GATE_STD;
   module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name));
   CM->set_top_info(fu_name, module_type);
   top = CM->get_circ();
   CM->add_port("in1", port_o::IN, top, b_type);
   CM->add_port("out1", port_o::OUT, top, b_type);
   NP_parameters = fu_name;
   CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
   TM->add_resource(Library, fu_name, CM);
   builtin_verilog_gates::add_builtin(BUFF_GATE_STD, "buf");



}
std::map<std::string, std::string> builtin_verilog_gates::from_std_gate_to_verilog;

bool builtin_verilog_gates::is_builtin(const std::string &gate)
{
   return from_std_gate_to_verilog.find(gate) != from_std_gate_to_verilog.end();
}

std::string builtin_verilog_gates::get_verilog_builtin(const std::string &gate)
{
   THROW_ASSERT(from_std_gate_to_verilog.find(gate) != from_std_gate_to_verilog.end(), "call is_builtin before of get_verilog_builtin");
   return from_std_gate_to_verilog.find(gate)->second;
}

void builtin_verilog_gates::add_builtin(std::string std_gate_name, std::string verilog_keyword)
{
   from_std_gate_to_verilog[std_gate_name] = verilog_keyword;
}

