/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file FPGA_device.cpp
 * @brief
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 */
#include "FPGA_device.hpp"

#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_XILINX_VIVADO.hpp"

#include "target_technology.hpp"
#include "technology_builtin.hpp"

///XML includes
#include "polixml.hpp"
#include "xml_dom_parser.hpp"
#include "fileIO.hpp"
#include "xml_helper.hpp"

///parameters' includes
#include "Parameter.hpp"
#include "constant_strings.hpp"

///Boost includes
#include "boost/filesystem.hpp"

FPGA_device::FPGA_device(const ParameterConstRef _Param, const technology_managerRef _TM, const type_t type) :
   target_device(_Param, _TM, type)
{
   ///creating the datastructure representing the target technology
   target = target_technology::create_technology(target_technology::FPGA, Param);
}

FPGA_device::~FPGA_device( )
{
}

void FPGA_device::load_devices(const target_deviceRef device)
{
   ///map between the default device string and the corresponding configuration stream
   std::map<std::string, std::string> default_device_data;
   /// Load default device options
   default_device_data["xc5vlx50-3ff1153"] =
      #include "xc5vlx50-3ff1153.data"
         ;
   default_device_data["xc5vlx110t-1ff1136"] =
      #include "xc5vlx110t-1ff1136.data"
         ;
   default_device_data["xc5vlx330t-2ff1738"] =
      #include "xc5vlx330t-2ff1738.data"
         ;
   default_device_data["xc6vlx240t-1ff1156"] =
      #include "xc6vlx240t-1ff1156.data"
         ;
   default_device_data["xc7z020-1clg484"] =
      #include "xc7z020-1clg484.data"
         ;
#if HAVE_XILINX_VIVADO
   default_device_data["xc7z020-1clg484-VVD"] =
      #include "xc7z020-1clg484-VVD.data"
         ;
#endif
   default_device_data["xc7vx330t-1ffg1157"] =
      #include "xc7vx330t-1ffg1157.data"
         ;

#if (0 && HAVE_EXPERIMENTAL)
   default_device_data["xc3s1500l-4fg676"] =
      #include "Spartan-3-xc3s1500l-4fg676.data"
         ;
#endif


   default_device_data["EP2C70F896C6-DSP"] =
      #include "EP2C70F896C6-DSP.data"
         ;

   default_device_data["EP2C70F896C6-R"] =
      #include "EP2C70F896C6-R.data"
         ;

   default_device_data["LFE335EA8FN484C"] =
      #include "LFE335EA8FN484C.data"
         ;

   unsigned int output_level = Param->getOption<unsigned int>(OPT_output_level);

   try
   {
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Available devices:");
      for (std::map<std::string, std::string>::const_iterator d = default_device_data.begin(); d != default_device_data.end(); d++)
      {
         PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, " - " + d->first);
      }

      std::string device_string = Param->getOption<std::string>(OPT_device_string);

      fileIO_istreamRef sname;

      if (Param->isOption(OPT_target_device_file))
      {
         PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Imported user data for target device");
         std::string device_string_file = Param->getOption<std::string>(OPT_target_device_file);
         sname = fileIO_istream_open(device_string_file);
      }
      else if (default_device_data.find(device_string) != default_device_data.end())
      {
         sname = fileIO_istream_open_from_string(default_device_data[device_string]);
      }
      else
         THROW_ERROR("Target device not supported: " + device_string);
      xml_dom_parser parser;
      parser.parse_stream(*sname);
      if (parser)
      {
         const xml_element* node = parser.get_document()->get_root_node(); //deleted by DomParser.
         xload(device, device_string, node);
      }
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Imported data for target device: Vendor=" + parameters["vendor"] + " " + "Family=" + parameters["family"] + " " + "Model=" + parameters["model"] + " " + "Package=" + parameters["package"] + " " + "Speed Grade=" + parameters["speed_grade"]);
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "");

      return;
   }
   catch (const char * msg)
   {
      std::cerr << msg << std::endl;
   }
   catch (const std::string & msg)
   {
      std::cerr << msg << std::endl;
   }
   catch (const std::exception& ex)
   {
      std::cout << "Exception caught: " << ex.what() << std::endl;
   }
   catch ( ... )
   {
      std::cerr << "unknown exception" << std::endl;
   }
   THROW_ERROR("Error during XML parsing of device files");

}

void FPGA_device::xwrite(xml_element* nodeRoot)
{
   xml_element* tmRoot = nodeRoot->add_child_element("device");

   THROW_ASSERT(has_parameter("vendor"), "vendor value is missing");
   xml_element* vendor_el = tmRoot->add_child_element("vendor");
   std::string vendor = get_parameter<std::string>("vendor");
   WRITE_XNVM2("value", vendor, vendor_el);

   THROW_ASSERT(has_parameter("family"), "family value is missing");
   xml_element* family_el = tmRoot->add_child_element("family");
   std::string family = get_parameter<std::string>("family");
   WRITE_XNVM2("value", family, family_el);

   THROW_ASSERT(has_parameter("model"), "model value is missing");
   xml_element* model_el = tmRoot->add_child_element("model");
   std::string model = get_parameter<std::string>("model");
   WRITE_XNVM2("value", model, model_el);

   THROW_ASSERT(has_parameter("package"), "package value is missing");
   xml_element* package_el = tmRoot->add_child_element("package");
   std::string package = get_parameter<std::string>("package");
   WRITE_XNVM2("value", package, package_el);

   THROW_ASSERT(has_parameter("speed_grade"), "speed_grade value is missing");
   xml_element* speed_grade_el = tmRoot->add_child_element("speed_grade");
   std::string speed_grade = get_parameter<std::string>("speed_grade");
   WRITE_XNVM2("value", speed_grade, speed_grade_el);

   for(std::map<std::string, std::string>::iterator p = parameters.begin(); p != parameters.end(); p++)
   {
      if(p->first == "vendor" || p->first == "family"  || p->first == "model"  || p->first == "package" || p->first == "speed_grade" || p->first == "clock_period") continue;
      xml_element* elRoot = tmRoot->add_child_element(p->first);
      WRITE_XNVM2("value", p->second, elRoot);
   }

}

void FPGA_device::initialize()
{
}

