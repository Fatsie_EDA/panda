/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file RTL_characterization.cpp
 * @brief Class implementing RTL characterization
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Marco Minutoli <mminutoli@gmail.com>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader includes
#include "config_HAVE_FLOPOCO.hpp"

#include "RTL_characterization.hpp"

#include "technology_manager.hpp"
#include "library_manager.hpp"
#include "technology_builtin.hpp"

#include "target_device.hpp"
#include "target_manager.hpp"
#include "area_model.hpp"
#include "time_model.hpp"
#include "clb_model.hpp"
#include "LUT_model.hpp"
#include "structural_manager.hpp"
#include "structural_objects.hpp"
#include "NP_functionality.hpp"

#include "BackendFlow.hpp"

#include "HDL_manager.hpp"
#include "language_writer.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"
#include "op_graph.hpp"
#include "Parameter.hpp"

#if HAVE_FLOPOCO
#include "flopoco_wrapper.hpp"
#endif

#define DUMMY_SYNTHESIS 0

#define PORT_VECTOR_N_PORTS 2

RTL_characterization::RTL_characterization(const ParameterConstRef _Param, const target_managerRef _target) :
   Param(_Param),
   debug_level(_Param->getOption<int>(OPT_debug_level)),
   output_level(_Param->getOption<int>(OPT_output_level)),
   target(_target),
   TM(target->get_technology_manager())
{

}

RTL_characterization::~RTL_characterization()
{

}

void RTL_characterization::exec()
{
   const target_deviceRef device = target->get_target_device();
   const std::vector<std::string>& libraries = TM->get_library_list();

   for(unsigned int l = 0; l < libraries.size(); l++)
   {
      const library_managerRef LM = TM->get_library_manager(libraries[l]);
      if(LM->get_library_name() != LIBRARY_STD_FU && LM->get_library_name() != LIBRARY_PC && LM->get_library_name() != LIBRARY_STD) continue;
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, " - Analyzing the library " + LM->get_library_name());
      characterize_library(LM);
   }
   //fix_execution_time_std();
   fix_proxies_execution_time_std();
   xwrite_device_file(device);
}

void RTL_characterization::fix_execution_time_std()
{
   technology_nodeRef f_unit_br = TM->get_fu(ASSIGN_VECTOR_BOOL_STD, LIBRARY_STD_FU);
   functional_unit * fu_br= GetPointer<functional_unit>(f_unit_br);
   technology_nodeRef op_br_node =fu_br->get_operation(ASSIGN);
   operation * op_br_LOAD = GetPointer<operation>(op_br_node);
   double ASSIGN_exec_time = op_br_LOAD->time_m->get_execution_time();

   const std::vector<std::string>& libraries = TM->get_library_list();
   for(unsigned int l = 0; l < libraries.size(); l++)
   {
      const library_managerRef& LM = TM->get_library_manager(libraries[l]);
      if(LM->get_library_name() != LIBRARY_STD_FU && LM->get_library_name() != LIBRARY_PC && LM->get_library_name() != LIBRARY_STD) continue;
      const library_manager::fu_map_type& fus = LM->get_library_fu();
      for(library_manager::fu_map_type::const_iterator f = fus.begin(); f != fus.end(); f++)
      {
         technology_nodeRef tn = f->second;
         functional_unit * current_fu = GetPointer<functional_unit>(tn);
         if (current_fu)
         {
            if ((!Param->isOption(OPT_component_name) || current_fu->get_operations_num() == 0) && completed.find(current_fu->functional_unit_name) == completed.end()) continue;
            const functional_unit::operation_vec &ops = current_fu->get_operations();
            for(unsigned int o = 0; o < ops.size(); o++)
            {
               operation * current_op = GetPointer<operation>(ops[o]);
               if(!current_op->time_m) continue;
               double curr_exec = current_op->time_m->get_execution_time();
               unsigned int curr_cycles = current_op->time_m->get_cycles();
               if(curr_exec-ASSIGN_exec_time<0)
                  current_op->time_m->set_execution_time(0, curr_cycles);
               else
                  current_op->time_m->set_execution_time(curr_exec-ASSIGN_exec_time, curr_cycles);
            }
         }
      }
   }
}

void RTL_characterization::fix_proxies_execution_time_std()
{
   technology_nodeRef f_unit_br = TM->get_fu(ARRAY_1D_STD_BRAM, LIBRARY_STD_FU);
   functional_unit * fu_br= GetPointer<functional_unit>(f_unit_br);
   technology_nodeRef op_br_node =fu_br->get_operation(STORE);
   operation * op_br_STORE = GetPointer<operation>(op_br_node);
   double STORE_exec_time = op_br_STORE->time_m->get_execution_time();
   technology_nodeRef f_unit_br_nn = TM->get_fu(ARRAY_1D_STD_BRAM_NN, LIBRARY_STD_FU);
   functional_unit * fu_br_nn= GetPointer<functional_unit>(f_unit_br_nn);
   technology_nodeRef op_br_nn_node =fu_br_nn->get_operation(STORE);
   operation * op_br_nn_STORE = GetPointer<operation>(op_br_nn_node);
   double nn_STORE_exec_time = op_br_nn_STORE->time_m->get_execution_time();

   const std::vector<std::string>& libraries = TM->get_library_list();
   for(unsigned int l = 0; l < libraries.size(); l++)
   {
      const library_managerRef& LM = TM->get_library_manager(libraries[l]);
      if(LM->get_library_name() != LIBRARY_STD_FU && LM->get_library_name() != LIBRARY_PC && LM->get_library_name() != LIBRARY_STD) continue;
      const library_manager::fu_map_type& fus = LM->get_library_fu();
      for(library_manager::fu_map_type::const_iterator f = fus.begin(); f != fus.end(); f++)
      {
         technology_nodeRef tn = f->second;
         functional_unit * current_fu = GetPointer<functional_unit>(tn);
         std::string fu_name = current_fu ? current_fu->functional_unit_name : "";
         if (current_fu && (fu_name == PROXY_CTRL || fu_name == BMEMORY_STD))
         {
            if ((!Param->isOption(OPT_component_name) || current_fu->get_operations_num() == 0) && completed.find(fu_name) == completed.end()) continue;
            const functional_unit::operation_vec &ops = current_fu->get_operations();
            for(unsigned int o = 0; o < ops.size(); o++)
            {
               operation * current_op = GetPointer<operation>(ops[o]);
               if(!current_op->time_m) continue;
               unsigned int curr_cycles = current_op->time_m->get_cycles();
               if(curr_cycles>0)
                  current_op->time_m->set_stage_period(STORE_exec_time);
               else
                  current_op->time_m->set_execution_time(STORE_exec_time, curr_cycles);
            }
         }
         if (current_fu && (fu_name == PROXY_CTRLN || fu_name == BMEMORY_STDN))
         {
            if ((!Param->isOption(OPT_component_name) || current_fu->get_operations_num() == 0) && completed.find(fu_name) == completed.end()) continue;
            const functional_unit::operation_vec &ops = current_fu->get_operations();
            for(unsigned int o = 0; o < ops.size(); o++)
            {
               operation * current_op = GetPointer<operation>(ops[o]);
               if(!current_op->time_m) continue;
               unsigned int curr_cycles = current_op->time_m->get_cycles();
               if(curr_cycles>0)
                  current_op->time_m->set_stage_period(nn_STORE_exec_time);
               else
                  current_op->time_m->set_execution_time(nn_STORE_exec_time, curr_cycles);
            }
         }
      }
   }
}

void RTL_characterization::xwrite_device_file(const target_deviceRef device)
{
   std::string file_name = Param->getOption<std::string>(OPT_estimation_output);
   try
   {
      xml_document document;
      xml_element* nodeRoot = document.create_root_node("target");

      device->xwrite(nodeRoot);

      xwrite_characterization(device, nodeRoot);

      document.write_to_file_formatted(file_name);
   }
   catch (const char * msg)
   {
      std::cerr << msg << std::endl;
   }
   catch (const std::string & msg)
   {
      std::cerr << msg << std::endl;
   }
   catch (const std::exception& ex)
   {
      std::cout << "Exception caught: " << ex.what() << std::endl;
   }
   catch ( ... )
   {
      std::cerr << "unknown exception" << std::endl;
   }
}

void RTL_characterization::xwrite_characterization(const target_deviceRef device, xml_element* nodeRoot)
{
   xml_element* tmRoot = nodeRoot->add_child_element("technology");

   const std::vector<std::string>& libraries = TM->get_library_list();
   for(unsigned int l = 0; l < libraries.size(); l++)
   {
      const library_managerRef& LM = TM->get_library_manager(libraries[l]);
      xml_element* lmRoot = tmRoot->add_child_element("library");
      xml_element* name_el = lmRoot->add_child_element("name");
      name_el->add_child_text(LM->get_library_name());

      const library_manager::fu_map_type& fus = LM->get_library_fu();
      for(library_manager::fu_map_type::const_iterator f = fus.begin(); f != fus.end(); f++)
      {
         technology_nodeRef tn = f->second;
         functional_unit * current_fu = GetPointer<functional_unit>(tn);
         if (!current_fu)
         {
            functional_unit_template * current_fu_temp = GetPointer<functional_unit_template>(tn);
            if(current_fu_temp->specialized != "")
            {
               xml_element* template_el = lmRoot->add_child_element("template");
               current_fu_temp->xwrite(template_el,tn,Param, device->get_type());
            }
         }
         else
         {
            if ((!Param->isOption(OPT_component_name) || current_fu->get_operations_num() == 0) && completed.find(current_fu->functional_unit_name) == completed.end()) continue;

            if (!current_fu->area_m)
            {
               /// set to the default value
               current_fu->area_m = area_model::create_model(device->get_type(), Param);
            }

            xml_element* cell_el = lmRoot->add_child_element("cell");
            xml_element* cell_name_el = cell_el->add_child_element("name");
            cell_name_el->add_child_text(f->first);

            xml_element* attribute_el = cell_el->add_child_element("attribute");
            WRITE_XNVM2("name", "area", attribute_el);
            WRITE_XNVM2("value_type", "float64", attribute_el);
            attribute_el->add_child_text(STR(current_fu->area_m->get_area_value()));
            clb_model* clb = GetPointer<clb_model>(current_fu->area_m);
            if(clb && clb->get_resource_value(clb_model::REGISTERS) != 0)
            {
               attribute_el = cell_el->add_child_element("attribute");
               WRITE_XNVM2("name", "REGISTERS", attribute_el);
               WRITE_XNVM2("value_type", "float64", attribute_el);
               attribute_el->add_child_text(STR(clb->get_resource_value(clb_model::REGISTERS)));
            }
            if(clb && clb->get_resource_value(clb_model::SLICE_LUTS) != 0)
            {
               attribute_el = cell_el->add_child_element("attribute");
               WRITE_XNVM2("name", "SLICE_LUTS", attribute_el);
               WRITE_XNVM2("value_type", "float64", attribute_el);
               attribute_el->add_child_text(STR(clb->get_resource_value(clb_model::SLICE_LUTS)));
            }
            if(clb && clb->get_resource_value(clb_model::LUT_FF_PAIRS) != 0)
            {
               attribute_el = cell_el->add_child_element("attribute");
               WRITE_XNVM2("name", "LUT_FF_PAIRS", attribute_el);
               WRITE_XNVM2("value_type", "float64", attribute_el);
               attribute_el->add_child_text(STR(clb->get_resource_value(clb_model::LUT_FF_PAIRS)));
            }
            if(clb && clb->get_resource_value(clb_model::DSP) != 0)
            {
               attribute_el = cell_el->add_child_element("attribute");
               WRITE_XNVM2("name", "DSP", attribute_el);
               WRITE_XNVM2("value_type", "float64", attribute_el);
               attribute_el->add_child_text(STR(clb->get_resource_value(clb_model::DSP)));
            }
            if(clb && clb->get_resource_value(clb_model::BRAM) != 0)
            {
               attribute_el = cell_el->add_child_element("attribute");
               WRITE_XNVM2("name", "BRAM", attribute_el);
               WRITE_XNVM2("value_type", "float64", attribute_el);
               attribute_el->add_child_text(STR(clb->get_resource_value(clb_model::BRAM)));
            }

            if (current_fu->fu_template_name != "" && current_fu->fu_template_parameters != "")
            {
               xml_element* template_el = cell_el->add_child_element("template");
               WRITE_XNVM2("name", current_fu->fu_template_name, template_el);
               WRITE_XNVM2("parameter", current_fu->fu_template_parameters, template_el);

               if(current_fu->characterizing_constant_value != "")
               {
                  xml_element* constant_el = cell_el->add_child_element("characterizing_constant_value");
                  constant_el->add_child_text(STR(current_fu->characterizing_constant_value));
               }
            }

            const functional_unit::operation_vec &ops = current_fu->get_operations();
            for(unsigned int o = 0; o < ops.size(); o++)
            {
               operation * current_op = GetPointer<operation>(ops[o]);
               current_op->xwrite(cell_el, ops[o], Param, device->get_type());
            }
            if(current_fu->CM && current_fu->CM->get_circ() && GetPointer<module>(current_fu->CM->get_circ()) && GetPointer<module>(current_fu->CM->get_circ())->get_specialized() != "")
               current_fu->CM->xwrite(cell_el);
         }
      }
   }
}

void RTL_characterization::resize_port(const structural_objectRef& port, unsigned int prec)
{
   if (port->get_typeRef()->type == structural_type_descriptor::BOOL) return;
   if(GetPointer<port_o>(port)->get_is_doubled())
   {
      if(port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_INT || port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_UINT || port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_REAL)
         port_o::resize_std_port(2*prec,128/(2*prec), 0, port);
      else
         port_o::resize_std_port(2*prec, 0, 0, port);
   }
   else if(GetPointer<port_o>(port)->get_is_halved())
   {
      if(port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_INT || port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_UINT || port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_REAL)
         port_o::resize_std_port(prec/2,128/(prec/2), 0, port);
      else
         port_o::resize_std_port(prec/2, 0, 0, port);
   }
   else if(port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_INT || port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_UINT || port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_REAL)
      port_o::resize_std_port(prec,128/prec, 0, port);
   else
      port_o::resize_std_port(prec, 0, 0, port);
}

void RTL_characterization::specialize_fu(const module* mod, unsigned int prec, unsigned int bus_data_bitsize, unsigned int bus_addr_bitsize, unsigned int bus_size_bitsize)
{
   for(unsigned int i = 0; i < mod->get_in_port_size(); i++)
   {
      const structural_objectRef& port = mod->get_in_port(i);
      if (port->get_kind() == port_vector_o_K)
      {
         if(GetPointer<port_o>(port)->get_ports_size() == 0) GetPointer<port_o>(port)->add_n_ports(PORT_VECTOR_N_PORTS, port);
         if(GetPointer<port_o>(port)->get_is_data_bus() || GetPointer<port_o>(port)->get_is_addr_bus() || GetPointer<port_o>(port)->get_is_size_bus())
            port_o::resize_busport(bus_size_bitsize, bus_addr_bitsize, bus_data_bitsize, port);
         else
         {
            for(unsigned int p = 0; p < GetPointer<port_o>(port)->get_ports_size() ; ++p)
               resize_port(GetPointer<port_o>(port)->get_port(p), prec);
         }
      }
      else
      {
         if(GetPointer<port_o>(port)->get_is_data_bus() || GetPointer<port_o>(port)->get_is_addr_bus() || GetPointer<port_o>(port)->get_is_size_bus())
            port_o::resize_busport(bus_size_bitsize, bus_addr_bitsize, bus_data_bitsize, port);
         else
            resize_port(port, prec);
      }
   }
   for(unsigned int i = 0; i < mod->get_out_port_size(); i++)
   {
      const structural_objectRef& port = mod->get_out_port(i);
      if (port->get_kind() == port_vector_o_K)
      {
         if(GetPointer<port_o>(port)->get_ports_size() == 0) GetPointer<port_o>(port)->add_n_ports(PORT_VECTOR_N_PORTS, port);
         if(GetPointer<port_o>(port)->get_is_data_bus() || GetPointer<port_o>(port)->get_is_addr_bus() || GetPointer<port_o>(port)->get_is_size_bus())
            port_o::resize_busport(bus_size_bitsize, bus_addr_bitsize, bus_data_bitsize, port);
         else
         {
            for(unsigned int p = 0; p < GetPointer<port_o>(port)->get_ports_size() ; ++p)
               resize_port(GetPointer<port_o>(port)->get_port(p), prec);
         }
      }
      else
      {
         if(GetPointer<port_o>(port)->get_is_data_bus() || GetPointer<port_o>(port)->get_is_addr_bus() || GetPointer<port_o>(port)->get_is_size_bus())
            port_o::resize_busport(bus_size_bitsize, bus_addr_bitsize, bus_data_bitsize, port);
         else
            resize_port(port, prec);
      }
   }
}

void RTL_characterization::characterize_library(const library_managerRef LM)
{
   // A copy of the map is needed becouse we are going to add functional unit to the library.
   const library_manager::fu_map_type fus = LM->get_library_fu();
   size_t n_fus = fus.size();
   size_t index = 0;
   for(library_manager::fu_map_type::const_iterator f = fus.begin(), end = fus.end(); f != end; f++)
   {
      characterize_fu(f->second, LM);
      if(index%10 == 0)
         PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "||||||| Functional units analyzed " + STR(index) + "/" + STR(n_fus));
      ++index;
   }
}



void RTL_characterization::add_input_register(structural_objectRef port_in, std::string register_library, std::string port_prefix, structural_objectRef reset_port, structural_objectRef circuit, structural_objectRef clock_port, structural_objectRef e_port, structural_managerRef SM)
{
   structural_objectRef r_signal;
   structural_objectRef reg_mod = SM->add_module_from_technology_library(port_prefix+"_REG", register_AR_NORETIME, register_library, circuit, TM);
   GetPointer<module>(reg_mod)->get_in_port(2)->type_resize(GET_TYPE_SIZE(port_in));
   GetPointer<module>(reg_mod)->get_out_port(0)->type_resize(GET_TYPE_SIZE(port_in));

   structural_objectRef port_ck = reg_mod->find_member(CLOCK_PORT_NAME, port_o_K, reg_mod);
   SM->add_connection(clock_port, port_ck);

   structural_objectRef reset_ck = reg_mod->find_member(RESET_PORT_NAME, port_o_K, reg_mod);
   if(reset_ck)
      SM->add_connection(reset_port, reset_ck);

   r_signal = SM->add_sign(port_prefix+"_SIGI1", circuit, port_in->get_typeRef());
   SM->add_connection(e_port, r_signal);
   SM->add_connection(GetPointer<module>(reg_mod)->get_in_port(2), r_signal);

   r_signal = SM->add_sign(port_prefix+"_SIGI2", circuit, port_in->get_typeRef());
   SM->add_connection(GetPointer<module>(reg_mod)->get_out_port(0), r_signal);
   SM->add_connection(port_in, r_signal);
}

void RTL_characterization::add_output_register(structural_managerRef SM, structural_objectRef e_port, structural_objectRef circuit, structural_objectRef reset_port, structural_objectRef port_out, std::string port_prefix, structural_objectRef clock_port, std::string register_library)
{
   structural_objectRef r_signal;
   structural_objectRef reg_mod = SM->add_module_from_technology_library(port_prefix+"_REG", register_AR_NORETIME, register_library, circuit, TM);
   GetPointer<module>(reg_mod)->get_in_port(2)->type_resize(GET_TYPE_SIZE(port_out));
   GetPointer<module>(reg_mod)->get_out_port(0)->type_resize(GET_TYPE_SIZE(port_out));

   structural_objectRef port_ck = reg_mod->find_member(CLOCK_PORT_NAME, port_o_K, reg_mod);
   SM->add_connection(clock_port, port_ck);

   structural_objectRef reset_ck = reg_mod->find_member(RESET_PORT_NAME, port_o_K, reg_mod);
   if(reset_ck)
      SM->add_connection(reset_port, reset_ck);

   r_signal = SM->add_sign(port_prefix+"_SIGO1", circuit, port_out->get_typeRef());
   SM->add_connection(port_out, r_signal);
   SM->add_connection(GetPointer<module>(reg_mod)->get_in_port(2), r_signal);
   r_signal = SM->add_sign(port_prefix+"_SIGO2", circuit, port_out->get_typeRef());
   SM->add_connection(GetPointer<module>(reg_mod)->get_out_port(0), r_signal);
   SM->add_connection(e_port, r_signal);
}

void RTL_characterization::characterize_fu(const technology_nodeRef f_unit, const library_managerRef LM)
{
   unsigned int BRAM_BITSIZE = 16;
   bool is_aligned = false;
   std::string customized_string;

   const target_deviceRef device = target->get_target_device();
   if(BRAM_BITSIZE > device->get_parameter<unsigned int>("BRAM_bitsize_max"))
      BRAM_BITSIZE = device->get_parameter<unsigned int>("BRAM_bitsize_max");

   unsigned int ALIGNED_BITSIZE = 2*BRAM_BITSIZE;
   unsigned int BUS_DATA_BITSIZE = BRAM_BITSIZE * (is_aligned ? 2 : 1);
   unsigned int BUS_ADDR_BITSIZE = 15;
   unsigned int BUS_SIZE_BITSIZE = 7;
   unsigned int NUMBER_OF_BYTES_ALLOCATED = 1024;
   bool is_commutative = true;

   std::set<unsigned int> precision;
   std::map<unsigned int, std::vector<std::string> > pipe_parameters;
   functional_unit * fu_curr = GetPointer<functional_unit>(f_unit);
   if(fu_curr && fu_curr->fu_template_name != "") return; ///previous characterization is not considered
   bool isTemplate = false;
   if (!fu_curr && GetPointer<functional_unit_template>(f_unit))
   {
      fu_curr = GetPointer<functional_unit>(GetPointer<functional_unit_template>(f_unit)->FU);
      isTemplate = true;
      customized_string = GetPointer<functional_unit_template>(f_unit)->characterizing_constant_value;
   }
   std::string fu_name = fu_curr->functional_unit_name;
   std::string fu_base_name = fu_name;
   const functional_unit::operation_vec & Ops = fu_curr->get_operations();
   functional_unit::operation_vec::const_iterator ops_end = Ops.end();
   for (functional_unit::operation_vec::const_iterator ops = Ops.begin(); ops != ops_end; ops++)
   {
      operation* curr_op = GetPointer<operation>(*ops);
      is_commutative = is_commutative && curr_op->commutative;
      std::map< std::string, std::vector<unsigned int> >::const_iterator supported_type_it_end =  curr_op->supported_types.end();
      if(curr_op->supported_types.begin() == curr_op->supported_types.end())
      {
         if(isTemplate)
         {
            precision.insert(8);
            precision.insert(16);
            precision.insert(64);
         }
         precision.insert(32);
      }
      else
      {
         for(std::map< std::string, std::vector<unsigned int> >::const_iterator supported_type_it =  curr_op->supported_types.begin(); supported_type_it != supported_type_it_end; ++supported_type_it)
         {
            std::vector<unsigned int>::const_iterator prec_it_end = supported_type_it->second.end();
            std::vector<unsigned int>::const_iterator prec_it = supported_type_it->second.begin();
            if(prec_it == prec_it_end)
            {
               if(isTemplate)
               {
                  precision.insert(8);
                  precision.insert(16);
                  precision.insert(64);
               }
               precision.insert(32);
            }
            else
            {
               for(; prec_it != prec_it_end; ++prec_it)
                  precision.insert(*prec_it);
            }

         }
      }
      std::string pipe_parameters_str = curr_op->pipe_parameters;
      if(pipe_parameters_str != "")
      {
         std::vector<std::string> parameters_split;
         boost::algorithm::split(parameters_split, pipe_parameters_str, boost::algorithm::is_any_of("|"));
         const std::vector<std::string>::const_iterator pp_it_end = parameters_split.end();
         for(std::vector<std::string>::const_iterator pp_it = parameters_split.begin(); pp_it != pp_it_end; ++pp_it)
         {
            std::vector<std::string> precision_pipe_param_pair;
            boost::algorithm::split(precision_pipe_param_pair, *pp_it, boost::algorithm::is_any_of(":"));
            THROW_ASSERT(precision_pipe_param_pair.size()==2, "malformed pipe parameter string");
            std::vector<std::string> pipe_params;
            boost::algorithm::split(pipe_params, precision_pipe_param_pair[1], boost::algorithm::is_any_of(","));
            THROW_ASSERT(pipe_params.size()>0, "malformed pipe parameter string");
            if(precision_pipe_param_pair[0] == "*")
            {
               for (std::set<unsigned int>::iterator prec = precision.begin(), end = precision.end(); prec != end; ++prec)
                  for(std::vector<std::string>::const_iterator param_it=pipe_params.begin(), param_it_end = pipe_params.end(); param_it != param_it_end; ++param_it)
                     if(std::find(pipe_parameters[*prec].begin(), pipe_parameters[*prec].end(), *param_it) == pipe_parameters[*prec].end())
                        pipe_parameters[*prec].push_back(*param_it);
            }
            else if(precision.find(boost::lexical_cast<unsigned int>(precision_pipe_param_pair[0])) != precision.end())
            {
               for(std::vector<std::string>::const_iterator param_it=pipe_params.begin(), param_it_end = pipe_params.end(); param_it != param_it_end; ++param_it)
                  if(std::find(pipe_parameters[boost::lexical_cast<unsigned int>(precision_pipe_param_pair[0])].begin(), pipe_parameters[boost::lexical_cast<unsigned int>(precision_pipe_param_pair[0])].end(), *param_it) == pipe_parameters[boost::lexical_cast<unsigned int>(precision_pipe_param_pair[0])].end())
                     pipe_parameters[boost::lexical_cast<unsigned int>(precision_pipe_param_pair[0])].push_back(*param_it);
            }
            else
               THROW_ERROR("malformed pipe parameter string");

         }
      }
   }

   if(Ops.begin()==Ops.end())
      is_commutative = false;
   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, " - Characterizing " + fu_name);
   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, std::string(" - Resource is ") + (is_commutative ? "" : "not ") +  "commutative");

   for (std::set<unsigned int>::iterator prec = precision.begin(), end = precision.end(); prec != end; ++prec)
   {
      if (!fu_curr->CM)
      {
         completed.insert(fu_base_name);
      }
      else
      {
         const structural_objectRef obj = fu_curr->CM->get_circ();
         NP_functionalityRef NPF = GetPointer<module>(obj)->get_NP_functionality();
         bool is_xilinx = device->has_parameter("vendor") && device->get_parameter<std::string>("vendor") == "Xilinx";
         bool is_altera = device->has_parameter("vendor") && device->get_parameter<std::string>("vendor") == "Altera";
         bool is_lattice = device->has_parameter("vendor") && device->get_parameter<std::string>("vendor") == "Lattice";


         if (!(NPF->exist_NP_functionality(NP_functionality::VERILOG_PROVIDED)
#if HAVE_FLOPOCO
               || (NPF->exist_NP_functionality(NP_functionality::FLOPOCO_PROVIDED) && (is_xilinx || is_altera || is_lattice))
#endif
               || (NPF->exist_NP_functionality(NP_functionality::VHDL_PROVIDED))
               || (NPF->exist_NP_functionality(NP_functionality::SYSTEM_VERILOG_PROVIDED))
               )
             || (fu_base_name == ARRAY_1D_STD_BRAM && !is_xilinx && !is_altera && !is_lattice)
             || (fu_base_name == ARRAY_1D_STD_BRAM_N1 && !is_xilinx && !is_altera && !is_lattice)
             || (fu_base_name == ARRAY_1D_STD_BRAM_NN && !is_xilinx && !is_altera && !is_lattice)
             || (fu_base_name == ARRAY_1D_STD_BRAM_SDS && !is_xilinx && !is_altera && !is_lattice)
             || (fu_base_name == ARRAY_1D_STD_BRAM_N1_SDS && !is_xilinx && !is_altera && !is_lattice)
             || (fu_base_name == ARRAY_1D_STD_BRAM_NN_SDS && !is_xilinx && !is_altera && !is_lattice)
             || (fu_base_name == STD_BRAM && !is_xilinx && !is_altera && !is_lattice)
             || (fu_base_name == STD_BRAMN && !is_xilinx && !is_altera && !is_lattice)
             || (fu_base_name == ARRAY_1D_STD_DISTRAM && !is_xilinx && !is_altera)
             || fu_base_name == LUT_GATE_STD
             || fu_base_name == AND_GATE_STD
             || fu_base_name == NAND_GATE_STD
             || fu_base_name == OR_GATE_STD
             || fu_base_name == NOR_GATE_STD
             || fu_base_name == XOR_GATE_STD
             || fu_base_name == XNOR_GATE_STD
             || fu_base_name == "split_signal"
             || fu_base_name == "FSL_handler"
             || (Param->isOption(OPT_component_name) && (
                    (Param->getOption<std::string>(OPT_component_name) != fu_base_name)
                    //&& ASSIGN_VECTOR_BOOL_STD !=  fu_base_name
                    )
                 )
             //|| fu_base_name != "mult_expr_DSP"
             //|| fu_base_name != "trunc_div_expr_FU"
             //|| fu_base_name != "fp_fix_trunc_expr_FU"
             //|| fu_base_name == "fp_log_FU"
             || fu_base_name.find(CONSTANT_STD) != std::string::npos)
         {
            // keep the existing one
            completed.insert(fu_base_name);
         }
         else
         {
            const module* mod = GetPointer<module>(obj);
            unsigned int n_ports = mod->get_in_port_size();
            ///check for a single port
            unsigned int n_port_to_be_specialized = 0;
            for (unsigned int i = 0; i < n_ports; ++i)
            {
               structural_objectRef port_c = mod->get_in_port(i);
               if (port_c && (port_c->get_id() == CLOCK_PORT_NAME || port_c->get_id() == RESET_PORT_NAME || port_c->get_id() == START_PORT_NAME || (GetPointer<port_o>(port_c) && GetPointer<port_o>(port_c)->get_is_memory()))) continue;
               ++n_port_to_be_specialized;
            }
            unsigned int constPort;
            /// check if the resource can be pipelined
            size_t n_pipe_parameters = pipe_parameters[*prec].size() ;
            size_t n_iterations = pipe_parameters[*prec].size() > 1 ? pipe_parameters[*prec].size() : 1;

            for(size_t stage_index=0; stage_index<n_iterations; ++stage_index)
            {
               if(n_port_to_be_specialized <= 1 || !isTemplate || fu_base_name == MUX_GATE_STD || fu_base_name == DEMUX_GATE_STD)
                  constPort = n_ports; // Set constPort to in_port_size to immediately stop the loop after one iteration.
               else
                  constPort = 0;
               area_modelRef prev_area_characterization;
               time_modelRef prev_timing_characterization;
               unsigned int  has_first_synthesis_id = n_ports + 2;
               for (; constPort < n_ports + 1; ++constPort)
               {
                  structural_objectRef port_c = n_ports >  constPort ? mod->get_in_port(constPort) : structural_objectRef();
                  if (port_c && (port_c->get_id() == CLOCK_PORT_NAME || port_c->get_id() == RESET_PORT_NAME || port_c->get_id() == START_PORT_NAME || (GetPointer<port_o>(port_c) && GetPointer<port_o>(port_c)->get_is_memory()))) continue;
                  std::string template_parameters;
                  fu_name = fu_base_name;
                  template_parameters="";
                  if (isTemplate)
                  {
                     for (unsigned int iport = 0; iport < n_ports; ++iport)
                     {
                        structural_objectRef port = mod->get_in_port(iport);
                        if (port->get_id() == CLOCK_PORT_NAME || port->get_id() == RESET_PORT_NAME || port->get_id() == START_PORT_NAME || (GetPointer<port_o>(port) && GetPointer<port_o>(port)->get_is_memory())) continue;
                        if (template_parameters != "")
                           template_parameters += " ";
                        THROW_ASSERT(port, "expected a port");
                        if (port->get_typeRef()->type == structural_type_descriptor::BOOL)
                        {
                           fu_name += "_" + STR(1);
                           template_parameters += STR(1);
                        }
                        else if (iport != constPort)
                        {
                           fu_name += "_" + STR(*prec);
                           template_parameters += STR(*prec);
                           if(port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_INT || port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_UINT || port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_REAL)
                           {
                              fu_name += "_" + STR(128 / *prec);
                              template_parameters += " " + STR(128 / *prec);
                           }

                        }
                        else
                        {
                           fu_name += "_" + STR(0);
                           template_parameters += STR(0);
                        }
                     }
                     // output port
                     for (unsigned int oport = 0; oport < mod->get_out_port_size(); ++oport)
                     {
                        structural_objectRef port = mod->get_out_port(oport);
                        THROW_ASSERT(port, "expected a port");
                        if (port->get_id() == DONE_PORT_NAME || (GetPointer<port_o>(port)->get_is_memory())) continue;
                        if(GetPointer<port_o>(port)->get_is_doubled())
                        {
                           fu_name += "_" + STR(2 * *prec);
                           template_parameters += " " + STR(2 * *prec);
                        }
                        else if(GetPointer<port_o>(port)->get_is_halved())
                        {
                           fu_name += "_" + STR(*prec/2);
                           template_parameters += " " + STR(*prec/2);
                        }
                        else
                        {
                           fu_name += "_" + STR(*prec);
                           template_parameters += " " + STR(*prec);
                        }
                        if(port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_INT || port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_UINT || port->get_typeRef()->type ==  structural_type_descriptor::VECTOR_REAL)
                        {
                           if(GetPointer<port_o>(port)->get_is_doubled())
                           {
                              fu_name += "_" + STR(128 / (2 * *prec));
                              template_parameters += " " + STR(128 / (2 * *prec));
                           }
                           else if(GetPointer<port_o>(port)->get_is_halved())
                           {
                              fu_name += "_" + STR(128 / (*prec/2));
                              template_parameters += " " + STR(128 / (*prec/2));
                           }
                           else
                           {
                              fu_name += "_" + STR(128 / *prec);
                              template_parameters += " " + STR(128 / *prec);
                           }

                        }
                     }
                  }
                  if(n_pipe_parameters>0)
                  {
                        fu_name += "_" + pipe_parameters[*prec][stage_index];
                        template_parameters += " " + pipe_parameters[*prec][stage_index];
                  }

                  functional_unit* fu;
                  technology_nodeRef tn = TM->get_fu(fu_name, LM->get_library_name());
                  if (!tn)
                  {
                     // Analizing a template, specializations of that template won't be found in the library.
                     technology_nodeRef fun_unit;
                     if(GetPointer<functional_unit_template>(f_unit))
                        fun_unit = GetPointer<functional_unit_template>(f_unit)->FU;
                     else
                        fun_unit = f_unit;
                     tn = create_template_instance(fun_unit, fu_name, device, *prec);
                     fu = GetPointer<functional_unit>(tn);
                     fu->fu_template_parameters = template_parameters;
                     LM->add(tn);
                  }
                  else
                      fu = GetPointer<functional_unit>(tn);
                  if(constPort < n_ports)
                  {
                     /// some modules has to be characterized with respect to a different constant value
                     if(isTemplate && customized_string != "")
                        fu->characterizing_constant_value = customized_string;
                     else
                        /// The constant 6148914691236517205 has been choosen because from a binary point of view is made of alternating 1 and 0.
                        fu->characterizing_constant_value = STR(6148914691236517205);
                  }
                  else
                     fu->characterizing_constant_value = "";
#ifndef NDEBUG
                  const bool assertion_argument = NPF->exist_NP_functionality(NP_functionality::VERILOG_PROVIDED)
             #if HAVE_FLOPOCO
                               || NPF->exist_NP_functionality(NP_functionality::FLOPOCO_PROVIDED)
             #endif
                               || NPF->exist_NP_functionality(NP_functionality::VHDL_PROVIDED)
                               || NPF->exist_NP_functionality(NP_functionality::SYSTEM_VERILOG_PROVIDED);
                  THROW_ASSERT(assertion_argument, "Verilog, VHDL, SystemVerilog or Flopoco description not provided for functional unit " + fu_base_name);
#endif
                  structural_managerRef SM = structural_managerRef(new structural_manager(Param));
                  /// main circuit type
                  structural_type_descriptorRef module_type = structural_type_descriptorRef(new structural_type_descriptor(fu_name + "_wrapper"));
                  /// setting top circuit component
                  SM->set_top_info(fu_name + "_wrapper", module_type);
                  structural_objectRef circuit = SM->get_circ();
                  THROW_ASSERT(circuit, "Top circuit is missing");
                  structural_objectRef template_circuit = SM->add_module_from_technology_library(fu_base_name, fu_base_name, LM->get_library_name(), circuit, TM);

                  PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, " - Generating HDL of functional unit " + fu_name);
                  module* spec_module = GetPointer<module>(template_circuit);
                  specialize_fu(spec_module, *prec, BUS_DATA_BITSIZE, BUS_ADDR_BITSIZE, BUS_SIZE_BITSIZE);

                  if(fu_base_name == "MC_FU") /// add further specializations for this module
                  {
                     spec_module->set_parameter("EXECUTION_TIME", STR(2));
                     spec_module->set_parameter("BITSIZE", STR(8));
                     spec_module->set_parameter("INITIATION_TIME", STR(1));
                  }
                  else if(fu_base_name == ARRAY_1D_STD_BRAM || fu_base_name == ARRAY_1D_STD_BRAM_NN || fu_base_name == ARRAY_1D_STD_BRAM_N1 ||
                          fu_base_name == ARRAY_1D_STD_BRAM_SDS || fu_base_name == ARRAY_1D_STD_BRAM_NN_SDS || fu_base_name == ARRAY_1D_STD_BRAM_N1_SDS ||
                          fu_base_name == ARRAY_1D_STD_DISTRAM ||
                          fu_base_name == STD_BRAM || fu_base_name == STD_BRAMN )
                  {
                     unsigned int base_address = 0;
                     std::string init_filename="array_ref_"+boost::lexical_cast<std::string>(base_address)+".data";
                     unsigned int counter=0;
                     unsigned int nbyte_on_memory = BRAM_BITSIZE/8;
                     unsigned int elts_size = BUS_DATA_BITSIZE;
                     unsigned int vec_size = NUMBER_OF_BYTES_ALLOCATED/(elts_size/8);
                     if(fu_base_name == ARRAY_1D_STD_DISTRAM)
                     {
                        nbyte_on_memory = 2*nbyte_on_memory;
                        vec_size = nbyte_on_memory*8/elts_size;
                        base_address = 0;
                     }
                     if(fu_base_name == ARRAY_1D_STD_BRAM_SDS || fu_base_name == ARRAY_1D_STD_BRAM_NN_SDS || fu_base_name == ARRAY_1D_STD_BRAM_N1_SDS )
                     {
                        elts_size = 2*elts_size;
                        vec_size = (NUMBER_OF_BYTES_ALLOCATED)/(elts_size/8);
                        BRAM_BITSIZE = elts_size;
                        nbyte_on_memory = elts_size/8;
                     }
                     if(fu_base_name == ARRAY_1D_STD_BRAM_N1 || fu_base_name == ARRAY_1D_STD_BRAM_NN || fu_base_name == STD_BRAMN)
                     {
                        std::ofstream init_file_a((init_filename+"_a").c_str());
                        std::ofstream init_file_b((init_filename+"_b").c_str());
                        bool is_even = true;
                        for(unsigned int i = 0; i < vec_size; ++i)
                        {
                           for(unsigned int j = 0; j < elts_size; ++j)
                           {
                              if(is_even)
                                 init_file_a << "0";
                              else
                                 init_file_b << "0";
                              counter++;
                              if(counter%(nbyte_on_memory*8)==0)
                              {
                                 if(is_even)
                                    init_file_a << std::endl;
                                 else
                                    init_file_b << std::endl;
                                 is_even = !is_even;
                              }
                           }
                        }
                        init_file_a.close();
                        init_file_b.close();
                     }
                     else
                     {
                        std::ofstream init_file(init_filename.c_str());
                        for(unsigned int i = 0; i < vec_size; ++i)
                        {
                           for(unsigned int j = 0; j < elts_size; ++j)
                           {
                              init_file << "0";
                              counter++;
                              if(counter%(nbyte_on_memory*8)==0)
                                 init_file << std::endl;
                           }
                        }
                        init_file.close();
                     }
                     spec_module->set_parameter("address_space_begin", boost::lexical_cast<std::string>(base_address));
                     spec_module->set_parameter("address_space_rangesize", boost::lexical_cast<std::string>((elts_size/8)*vec_size));
                     spec_module->set_parameter("USE_SPARSE_MEMORY", boost::lexical_cast<std::string>(1));
                     if(fu_base_name == ARRAY_1D_STD_BRAM_N1 || fu_base_name == ARRAY_1D_STD_BRAM_NN || fu_base_name == STD_BRAMN)
                     {
                        spec_module->set_parameter("INIT_file_a", "\"\""+init_filename+"_a"+"\"\"");
                        spec_module->set_parameter("INIT_file_b", "\"\""+init_filename+"_b"+"\"\"");
                     }
                     else
                        spec_module->set_parameter("INIT_file", "\"\""+init_filename+"\"\"");
                     spec_module->set_parameter("n_elements", boost::lexical_cast<std::string>(vec_size));
                     spec_module->set_parameter("data_size", boost::lexical_cast<std::string>(elts_size));
                     spec_module->set_parameter("BRAM_BITSIZE", boost::lexical_cast<std::string>(BRAM_BITSIZE));
                     spec_module->set_parameter("BUS_PIPELINED", boost::lexical_cast<std::string>(1));
                     spec_module->set_parameter("PRIVATE_MEMORY", boost::lexical_cast<std::string>(0));
                  }
                  else if(fu_base_name == MEMLOAD_STD)
                     spec_module->set_parameter("base_address", boost::lexical_cast<std::string>(8));
                  else if(fu_base_name == MEMSTORE_STD)
                     spec_module->set_parameter("base_address", boost::lexical_cast<std::string>(8));
                  structural_objectRef e_port, one_port;

                  if(n_pipe_parameters>0)
                  {
                     spec_module->set_parameter(PIPE_PARAMETER, pipe_parameters[*prec][stage_index]);
                     PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, " - PIPE_PARAMETER=" + pipe_parameters[*prec][stage_index]);
                  }
                  if(NPF)
                  {
                     std::vector<std::string> param;
                     NPF->get_library_parameters(param);
                     std::vector<std::string>::const_iterator it_end = param.end();
                     for (std::vector<std::string>::const_iterator it = param.begin(); it != it_end; ++it)
                        if(*it == "PRECISION")
                           spec_module->set_parameter("PRECISION", boost::lexical_cast<std::string>(*prec));
                        else if(*it == "ALIGNED_BITSIZE")
                           spec_module->set_parameter("ALIGNED_BITSIZE", boost::lexical_cast<std::string>(ALIGNED_BITSIZE));

                  }
                  if(NPF)
                  {
                     std::vector<std::string> param;
                     NPF->get_library_parameters(param);
                     std::vector<std::string>::const_iterator it_end = param.end();
                     for (std::vector<std::string>::const_iterator it = param.begin(); it != it_end; ++it)
                        THROW_ASSERT(template_circuit->find_member(*it, port_o_K, template_circuit) || template_circuit->find_member(*it, port_vector_o_K, template_circuit) ||  spec_module->is_parameter(*it), "parameter not yet specialized: " + *it + " for module " + spec_module->get_typeRef()->get_name());
                  }


                  structural_type_descriptorRef bool_type = structural_type_descriptorRef(new structural_type_descriptor("bool", 1));
                  one_port = SM->add_constant(fu_name+"_constant_" + STR(1), circuit, bool_type, STR(1));
                  std::string register_library = TM->get_library(register_AR_NORETIME);
                  structural_objectRef clock_port, reset_port;

                  ///add clock and reset
                  for(unsigned int i = 0; i < spec_module->get_in_port_size(); i++)
                  {
                     const structural_objectRef port_in = spec_module->get_in_port(i);
                     if (port_in->get_id() == CLOCK_PORT_NAME)
                     {
                        clock_port = SM->add_port(GetPointer<port_o>(port_in)->get_id(), port_o::IN, circuit, port_in->get_typeRef());
                        SM->add_connection(port_in, clock_port);
                     }
                     if (port_in->get_id() == RESET_PORT_NAME)
                     {
                        reset_port = SM->add_port(GetPointer<port_o>(port_in)->get_id(), port_o::IN, circuit, port_in->get_typeRef());
                        SM->add_connection(port_in, reset_port);
                     }
                     else if(port_in->get_id() == START_PORT_NAME)
                     {
                        e_port = SM->add_port(GetPointer<port_o>(port_in)->get_id(), port_o::IN, circuit, port_in->get_typeRef());
                        SM->add_connection(port_in, e_port);
                     }
                  }
                  if(!clock_port)
                  {
                     clock_port = SM->add_port(CLOCK_PORT_NAME, port_o::IN, circuit, bool_type);
                  }
                  if(!reset_port)
                  {
                     reset_port = SM->add_port(RESET_PORT_NAME, port_o::IN, circuit, bool_type);
                  }

                  for(unsigned int i = 0; i < spec_module->get_in_port_size(); i++)
                  {
                     structural_objectRef port_in = spec_module->get_in_port(i);
                     if (port_in->get_id() == CLOCK_PORT_NAME || port_in->get_id() == RESET_PORT_NAME || port_in->get_id() == START_PORT_NAME) continue;
                     if (isTemplate && i == constPort)
                     {
                        THROW_ASSERT(fu->characterizing_constant_value != "", "expected a value");
                        e_port = SM->add_constant("constant_" + STR(constPort), circuit, port_in->get_typeRef(), fu->characterizing_constant_value);
                        SM->add_connection(port_in, e_port);
                     }
                     else if(0 && GetPointer<port_o>(port_in)->get_is_size_bus())
                     {
                        if(port_in->get_kind() == port_vector_o_K)
                        {
                           for(unsigned int p = 0; p < GetPointer<port_o>(port_in)->get_ports_size() ; ++p)
                           {
                              e_port = SM->add_constant("constant_" + STR(i) + "_" + STR(p), circuit, port_in->get_typeRef(), STR(BUS_DATA_BITSIZE));
                              SM->add_connection(GetPointer<port_o>(port_in)->get_port(p), e_port);
                           }
                        }
                        else
                        {
                           e_port = SM->add_constant("constant_" + STR(i), circuit, port_in->get_typeRef(), STR(BUS_DATA_BITSIZE));
                           SM->add_connection(port_in, e_port);
                        }
                     }
                     else
                     {
                        if(port_in->get_kind() == port_vector_o_K)
                           e_port = SM->add_port_vector(GetPointer<port_o>(port_in)->get_id(), port_o::IN, GetPointer<port_o>(port_in)->get_ports_size(), circuit, GetPointer<port_o>(port_in)->get_port(0)->get_typeRef());
                        else
                           e_port = SM->add_port(GetPointer<port_o>(port_in)->get_id(), port_o::IN, circuit, port_in->get_typeRef());
                        std::string port_prefix = GetPointer<port_o>(port_in)->get_id();

                        /// add register on inputs
                        if(port_in->get_kind() == port_vector_o_K)
                        {
                           for(unsigned int p = 0; p < GetPointer<port_o>(port_in)->get_ports_size() ; ++p)
                           {
                              add_input_register(GetPointer<port_o>(port_in)->get_port(p), register_library, port_prefix+GetPointer<port_o>(port_in)->get_port(p)->get_id(), reset_port, circuit, clock_port, GetPointer<port_o>(e_port)->get_port(p), SM);
                           }
                        }
                        else
                           add_input_register(port_in, register_library, port_prefix, reset_port, circuit, clock_port, e_port, SM);
                     }

                  }

#if HAVE_FLOPOCO
                  bool is_doubled_out = false;
                  bool is_halved_out = false;
#endif
                  for(unsigned int i = 0; i < spec_module->get_out_port_size(); i++)
                  {
                     structural_objectRef port_out = spec_module->get_out_port(i);
#if HAVE_FLOPOCO
                     if(GetPointer<port_o>(port_out)->get_is_doubled())
                        is_doubled_out = true;
                     else if(GetPointer<port_o>(port_out)->get_is_halved())
                        is_halved_out = true;
#endif
                     if(port_out->get_kind() == port_vector_o_K)
                        e_port = SM->add_port_vector(GetPointer<port_o>(port_out)->get_id(), port_o::OUT, GetPointer<port_o>(port_out)->get_ports_size(), circuit, GetPointer<port_o>(port_out)->get_port(0)->get_typeRef());
                     else
                        e_port = SM->add_port(GetPointer<port_o>(port_out)->get_id(), port_o::OUT, circuit, port_out->get_typeRef());
                     std::string port_prefix = GetPointer<port_o>(port_out)->get_id();

                     /// add register on outputs
                     if(port_out->get_kind() == port_vector_o_K)
                     {
                        for(unsigned int p = 0; p < GetPointer<port_o>(port_out)->get_ports_size() ; ++p)
                        {
                           add_output_register(SM, GetPointer<port_o>(e_port)->get_port(p), circuit, reset_port, GetPointer<port_o>(port_out)->get_port(p), port_prefix+GetPointer<port_o>(port_out)->get_port(p)->get_id(), clock_port, register_library);
                        }
                     }
                     else
                        add_output_register(SM, e_port, circuit, reset_port, port_out, port_prefix, clock_port, register_library);
                  }

                  // get the wrapped circuit.
                  HDL_managerRef HDL = HDL_managerRef(new HDL_manager(device, Param));
                  HDL->hdl_gen(fu_name, circuit);
                  int PipelineDepth = -1;
#if HAVE_FLOPOCO
                  if(n_pipe_parameters > 0 && NPF && NPF->exist_NP_functionality(NP_functionality::FLOPOCO_PROVIDED) &&  HDL->get_flopocowrapper())
                  {
                     if(is_doubled_out)
                        PipelineDepth = static_cast<int>(HDL->get_flopocowrapper()->get_FUPipelineDepth(fu_base_name, *prec, 2 * *prec, pipe_parameters[*prec][stage_index]));
                     else if(is_halved_out)
                        PipelineDepth = static_cast<int>(HDL->get_flopocowrapper()->get_FUPipelineDepth(fu_base_name, *prec, *prec/2, pipe_parameters[*prec][stage_index]));
                     else
                        PipelineDepth = static_cast<int>(HDL->get_flopocowrapper()->get_FUPipelineDepth(fu_base_name, *prec, *prec, pipe_parameters[*prec][stage_index]));
                  }
#endif

                  try
                  {
                     /// generate the synthesis scripts
                     BackendFlowRef flow = BackendFlow::CreateFlow(Param, "Characterization", target);
                     flow->GenerateSynthesisScripts(fu->get_name(), SM, HDL);
                     PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Performing characterization of functional unit " + fu_name);
#if !DUMMY_SYNTHESIS
                     if(constPort < n_ports && is_commutative && constPort>has_first_synthesis_id)
                        fu->area_m = prev_area_characterization;
                     else
                     {
                        flow->ExecuteSynthesis();
                        has_first_synthesis_id = constPort;
                        ///the synthesis has been successfully completed
                        ///setting the used resources
                        fu->area_m = flow->get_used_resources();
                     }
#else
                     fu->area_m = area_model::create_model(device->get_type(), Param);

#endif
                     ///setting the timing values for each operation
                     const functional_unit::operation_vec &ops = fu->get_operations();
                     for(unsigned int o = 0; o < ops.size(); o++)
                     {
                        operation * new_op = GetPointer<operation>(ops[o]);
#if !DUMMY_SYNTHESIS
                        time_modelRef synthesis_results;
                        if(constPort < n_ports && is_commutative && constPort>has_first_synthesis_id)
                           synthesis_results = prev_timing_characterization;
                        else
                           synthesis_results = flow->get_timing_results();
#else
                        time_modelRef synthesis_results = time_model::create_model(device->get_type(), Param);
                        synthesis_results->set_execution_time(7.75, time_model::cycles_time_DEFAULT);
#endif
                        double exec_time = synthesis_results->get_execution_time();

                        if(!new_op->time_m)
                           new_op->time_m = time_model::create_model(device->get_type(), Param);

                        if(n_pipe_parameters > 0)
                        {
                           new_op->time_m->set_stage_period(time_model::stage_period_DEFAULT);
                           new_op->time_m->set_execution_time(time_model::execution_time_DEFAULT, time_model::cycles_time_DEFAULT);
                           new_op->time_m->set_initiation_time(time_model::initiation_time_DEFAULT);

                           unsigned int n_cycles;
                           n_cycles = boost::lexical_cast<unsigned int>(pipe_parameters[*prec][stage_index]);
                           new_op->pipe_parameters = pipe_parameters[*prec][stage_index];

                           if(n_cycles>0 && PipelineDepth != 0)
                           {
                              new_op->time_m->set_stage_period(exec_time);
                              new_op->time_m->set_initiation_time(1);
                              if(PipelineDepth==-1)
                                 new_op->time_m->set_execution_time(exec_time, n_cycles+1);
                              else
                                 new_op->time_m->set_execution_time(exec_time, static_cast<unsigned int>(PipelineDepth)+1);

                           }
                           else if (PipelineDepth == 0)
                              new_op->time_m->set_execution_time(exec_time, time_model::cycles_time_DEFAULT);
                           else
                              new_op->time_m->set_execution_time(exec_time, n_cycles);
                        }
                        else if(synthesis_results && new_op->time_m->get_cycles() == 0)
                        {
                           new_op->time_m->set_execution_time(exec_time, time_model::cycles_time_DEFAULT);
                        }
                        else
                        {
                           new_op->time_m->set_stage_period(exec_time);
                        }
                     }


#if !DUMMY_SYNTHESIS
                     if(constPort < n_ports && is_commutative && constPort==has_first_synthesis_id)
                     {
                        prev_area_characterization = flow->get_used_resources();
                        prev_timing_characterization = flow->get_timing_results();
                        THROW_ASSERT(prev_area_characterization, "expected a previous synthesis result");
                        THROW_ASSERT(prev_timing_characterization, "expected a previous synthesis result");
                     }
#endif
                     completed.insert(fu->functional_unit_name);
                     continue;
                  }
                  catch (const char * str)
                  {
                     std::cerr << str << std::endl;
                  }
                  catch (const std::string& str)
                  {
                     std::cerr << str << std::endl;
                  }
                  catch (std::exception &e)
                  {
                     std::cerr << e.what() << std::endl;
                  }
                  catch (...)
                  {
                     std::cerr << "Unknown error type" << std::endl;
                  }
                  THROW_WARNING("Error during the characterization of unit " + fu_name);
               }
            }
         }
      }
   }
}


technology_nodeRef RTL_characterization::create_template_instance(const technology_nodeRef& fu_template, std::string & name, const target_deviceRef& device, unsigned int prec)
{
   functional_unit * curr_fu = GetPointer<functional_unit>(fu_template);
   THROW_ASSERT(curr_fu, "Null functional unit template");

   functional_unit * specialized_fu = new functional_unit;
   specialized_fu->functional_unit_name = name;
   specialized_fu->fu_template_name = curr_fu->functional_unit_name;
   specialized_fu->CM = curr_fu->CM;
   specialized_fu->XML_description = curr_fu->XML_description;

   for (functional_unit::operation_vec::const_iterator itr = curr_fu->get_operations().begin(), end = curr_fu->get_operations().end(); itr < end; ++itr)
   {
      operation * const op = GetPointer<operation>(*itr);
      operation * new_op = new operation;
      new_op->operation_name = op->operation_name;
      new_op->bounded = op->bounded;

      new_op->time_m = time_model::create_model(device->get_type(), Param);
      if(op->time_m)
      {
         new_op->time_m->set_execution_time(op->time_m->get_execution_time(), op->time_m->get_cycles());
         new_op->time_m->set_synthesis_dependent(op->time_m->get_synthesis_dependent());
         new_op->time_m->set_initiation_time(op->time_m->get_initiation_time());
      }
      new_op->commutative = op->commutative;
      std::map< std::string, std::vector<unsigned int> >::const_iterator supported_type_it_end =  op->supported_types.end();
      if(op->supported_types.begin() != op->supported_types.end())
      {
         for(std::map< std::string, std::vector<unsigned int> >::const_iterator supported_type_it =  op->supported_types.begin(); supported_type_it != supported_type_it_end; ++supported_type_it)
         {
            new_op->supported_types[supported_type_it->first].push_back(prec);
         }
      }
      specialized_fu->add(technology_nodeRef(new_op));
   }

   specialized_fu->area_m = area_model::create_model(device->get_type(), Param);

   return technology_nodeRef(specialized_fu);
}

