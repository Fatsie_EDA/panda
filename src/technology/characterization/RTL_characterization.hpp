/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file RTL_characterization.hpp
 * @brief Class for performing RTL characterization
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef RTL_CHARACTERIZATION_HPP
#define RTL_CHARACTERIZATION_HPP

#include "refcount.hpp"

/**
 * @name forward declarations
 */
//@{
//parameters
CONSTREF_FORWARD_DECL(Parameter);
REF_FORWARD_DECL(target_device);
REF_FORWARD_DECL(target_manager);
REF_FORWARD_DECL(target_technology);
REF_FORWARD_DECL(technology_manager);
REF_FORWARD_DECL(technology_node);
REF_FORWARD_DECL(library_manager);
REF_FORWARD_DECL(language_writer);
REF_FORWARD_DECL(structural_object);
REF_FORWARD_DECL(structural_manager);
class xml_element;
class module;
//@}

#include <vector>
#include <set>

class RTL_characterization
{
   private:

      ///reference to the datastructure where all the parameters and options have been stored
      const ParameterConstRef Param;

      ///debug level of the class
      int debug_level;

      /// debug level of the class
      int output_level;

      ///target information
      const target_managerRef target;

      ///target libraries
      const technology_managerRef TM;

      /**
       * Characterize the specific library with respect to the target device
       */
      void characterize_library(const library_managerRef LM);

      /**
       * Characterize the given functional unit with respect to the target device
       */
      void characterize_fu(const technology_nodeRef functional_unit, const library_managerRef LM);

      /**
       * Create a template instance to be specialized
       */
      technology_nodeRef create_template_instance(const technology_nodeRef& fu_template, std::string& name, const target_deviceRef& device, unsigned int prec);

      /**
       * @brief resize the port w.r.t a given precision
       * @param port
       */
      void resize_port(const structural_objectRef& port, unsigned int prec);
      /**
       * Performing the specialization of the given object
       */
      void specialize_fu(const module* mod, unsigned int prec, unsigned int bus_data_bitsize, unsigned int bus_addr_bitsize, unsigned int bus_size_bitsize);

      /**
       * Generate the output file
       */
      void xwrite_device_file(const target_deviceRef device);

      /**
       * Add the characterization to the output file
       */
      void xwrite_characterization(const target_deviceRef device, xml_element* nodeRoot);

      ///set of units completed with success
      std::set<std::string> completed;

      /**
       * Fix the execution time by removing set/hold/pad timings
       */
      void fix_execution_time_std();

      /**
        * Fix execution/stage period value for proxies and bounded memory controllers
        */
      void fix_proxies_execution_time_std();


      void add_input_register(structural_objectRef port_in, std::string register_library, std::string port_prefix, structural_objectRef reset_port, structural_objectRef circuit, structural_objectRef clock_port, structural_objectRef e_port, structural_managerRef SM);

      void add_output_register(structural_managerRef SM, structural_objectRef e_port, structural_objectRef circuit, structural_objectRef reset_port, structural_objectRef port_out, std::string port_prefix, structural_objectRef clock_port, std::string register_library);

   public:

      /**
       * Constructor
       */
      RTL_characterization(const ParameterConstRef Param, const target_managerRef target);

      /**
       * Destructor
       */
      ~RTL_characterization();

      /**
       * Perform RTL characterization of the modules with respect to the target device
       */
      void exec();
};

/// RefCount definition of the class
typedef refcount<RTL_characterization> RTL_characterizationRef;

#endif
