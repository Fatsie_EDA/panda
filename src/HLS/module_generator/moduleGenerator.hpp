/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file moduleGenerator.hpp
 * @brief
 *
 *
 *
 * @author Alessandro Nacci <alenacci@gmail.com>
 * @author Gianluca Durelli <durellinux@gmail.com>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef _MODULE_GENERATOR_HPP_
#define _MODULE_GENERATOR_HPP_

///Superclass include
#include "hls_step.hpp"

///Graph include
#include "graph.hpp"

///STD include
#include <string>

///Technology include
#include "target_device.hpp"

///Utility include
#include "refcount.hpp"

CONSTREF_FORWARD_DECL(FunctionBehavior);
REF_FORWARD_DECL(structural_object);
REF_FORWARD_DECL(structural_type_descriptor);
REF_FORWARD_DECL(technology_manager);
REF_FORWARD_DECL(technology_node);


class moduleGenerator : public HLS_step
{
   public:

      /**
       * Constructor.
       */
      moduleGenerator(const ParameterConstRef Param, const HLS_managerRef HLSMgr);

      /**
       * Destructor.
       */
      virtual ~moduleGenerator();

      structural_type_descriptorRef getDataType(unsigned int variable, const FunctionBehaviorConstRef FB);

      void add_port_parameters(structural_objectRef generated_port, structural_objectRef currentPort);

      std::string generate_verilog(std::string verilog_template, std::vector<std::tuple<unsigned int,unsigned int> >& required_variables, const FunctionBehaviorConstRef FB, std::string path_dynamic_generators);

      std::string get_specialized_name(std::vector<std::tuple<unsigned int,unsigned int> >& required_variables, const FunctionBehaviorConstRef FB);

      void specialize_fu(std::string fuName, vertex ve, std::string libraryId, const technology_managerRef TM, const FunctionBehaviorConstRef FB, std::string new_fu_name, std::map<std::string,technology_nodeRef> & new_fu, const target_device::type_t dv_type);

      /**
       * Nothing to do
       */
      void exec();

      /**
       * Returns the name of the step
       */
      std::string get_kind_text() const;

};
///Refcount type definition of the class
typedef refcount<moduleGenerator> moduleGeneratorRef;

#endif
