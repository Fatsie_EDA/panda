/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file moduleGenerator.cpp
 * @brief
 *
 *
 *
 * @author Alessandro Nacci <alenacci@gmail.com>
 * @author Gianluca Durelli <durellinux@gmail.com>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "moduleGenerator.hpp"

#include "hls.hpp"
#include "hls_manager.hpp"

#include "technology_manager.hpp"
#include "library_manager.hpp"
#include "technology_builtin.hpp"
#include "area_model.hpp"

#include "function_behavior.hpp"
#include "behavioral_helper.hpp"

#include "structural_objects.hpp"
#include "structural_manager.hpp"

#include "Parameter.hpp"

#include <iostream>
#include <fstream>


moduleGenerator::moduleGenerator(const ParameterConstRef _Param, const HLS_managerRef _AppM) :
   HLS_step(_Param, _AppM, 0)
{

}

moduleGenerator::~moduleGenerator()
{

}

#define NAMESEPARATOR "_"

void moduleGenerator::exec()
{

}

std::string moduleGenerator::get_kind_text() const
{
   return "module-generator";
}

structural_type_descriptorRef moduleGenerator::getDataType(unsigned int variable, const FunctionBehaviorConstRef function_behavior)
{
   return structural_type_descriptorRef(new structural_type_descriptor(variable, function_behavior->CGetBehavioralHelper()));
}

std::string moduleGenerator::get_specialized_name(std::vector<std::tuple<unsigned int,unsigned int> >& required_variables, const FunctionBehaviorConstRef FB)
{
   std::string fuName="";
   for(std::vector<std::tuple<unsigned int,unsigned int> >::iterator l = required_variables.begin(); l != required_variables.end(); l++){
      unsigned int dataSize=getDataType(std::get<0>((*l)), FB)->vector_size!=0?getDataType(std::get<0>((*l)), FB)->vector_size:getDataType(std::get<0>((*l)), FB)->size;
      structural_type_descriptorRef typeRef=getDataType(std::get<0>((*l)), FB);
      fuName=fuName+NAMESEPARATOR+typeRef->get_name()+STR(dataSize);
   }
   return fuName;
}

std::string moduleGenerator::generate_verilog(std::string verilog_template, std::vector<std::tuple<unsigned int,unsigned int> >& required_variables, const FunctionBehaviorConstRef FB, std::string path_dynamic_generators)
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Reading cpp-template input file '" << (path_dynamic_generators+"/"+verilog_template).c_str() << "'...");

   std::string cpp_input_file_path_string=path_dynamic_generators+"/"+verilog_template;

   const char *cpp_input_file_path = cpp_input_file_path_string.c_str();
   std::string cpp_input = "";
   std::string line;
   std::ifstream cpp_infile;
   cpp_infile.open(cpp_input_file_path,std::ifstream::in);
   if (cpp_infile.is_open()){
      while ( cpp_infile.good() ){
         getline (cpp_infile,line);
         cpp_input += line + "\n";
      }
      cpp_infile.close();
   }
   else
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Unable to open file " << verilog_template);

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Starting dynamic verilog generation...");

   std::string cpp_code_header = "";
   std::string cpp_code_body = "";
   std::string cpp_code_footer = "";

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Initializing temporary c++ file for Verilog generation...");

   cpp_code_header += "#include <iostream>\n";
   cpp_code_header += "#include <string>\n";
   cpp_code_header += "#include <fstream>\n";
   cpp_code_header += "#include <boost/lexical_cast.hpp>\n";
   cpp_code_header += "#define STR(x) boost::lexical_cast<std::string>(x)\n\n";
   cpp_code_header += "int main(int argc, char **argv)\n";
   cpp_code_header += "{\n";

   cpp_code_header += "	struct parameter\n";
   cpp_code_header += "	{\n";
   cpp_code_header += "		std::string name;\n";
   cpp_code_header += "		std::string type;\n";
   cpp_code_header += "	};\n";

   cpp_code_footer += "\n\n\n";
   cpp_code_footer += "	return 0;\n}\n";

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Importing XML description...");

   cpp_code_body += "";

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Setting up parameters structure...");

   unsigned int parNum = static_cast<unsigned int>(required_variables.size());

   cpp_code_body += "	int _np = "+STR(parNum)+";\n";
   cpp_code_body += "	parameter _p["+ STR(parNum) +"];\n";

   int portNum=0;

   for(std::vector<std::tuple<unsigned int,unsigned int> >::iterator l = required_variables.begin(); l != required_variables.end(); l++){
      structural_type_descriptorRef typeRef=getDataType(std::get<0>((*l)), FB);
      cpp_code_body += "	_p["+STR(portNum)+"].name = \"in"+STR(portNum+1)+"\";\n";
      cpp_code_body += "	_p["+STR(portNum)+"].type = \""+typeRef->get_name()+"\";\n";
      portNum++;
   }

   cpp_code_body += "\n\n\n";

   cpp_code_body += cpp_input;

   std::string cpp_code = cpp_code_header + cpp_code_body + cpp_code_footer;

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Creating temp c++ file...");

   std::fstream File;

   File.open("temp_verilog_generator.cpp", std::ios::out);
   if (File.is_open ())
      File << cpp_code;
   File.close();

   int err;

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Compiling temp c++ file...");
   err = system("g++ temp_verilog_generator.cpp -o temp_verilog_generator");
   if(IsError(err))
   {
      THROW_ERROR("Error in generating temp_verilog_generator");
   }
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Executing temp c++ file...");
   err = system("./temp_verilog_generator > temp_verilog_file.v");
   if(IsError(err))
   {
      THROW_ERROR("Error in generating temp_verilog_file.v");
   }

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Verilog file generated successfully!...");
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Importing verilog...");

   std::string verilogOutput = "";
   line = "";
   std::ifstream verilogFile ("temp_verilog_file.v");
   if (verilogFile.is_open()){
      while ( verilogFile.good() ){
         getline (verilogFile,line);
         verilogOutput += line + "\n";
      }
      verilogFile.close();
   }
   else
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Unable to open file temp_verilog_file.v");

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ Deleting all temp files...");
   boost::filesystem::remove_all("temp_verilog_generator.cpp");
   boost::filesystem::remove_all("temp_verilog_generator");
   boost::filesystem::remove_all("temp_verilog_file.v");
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ DONE");

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "dynamic_generators @ The generated Dynamic-Verilog is:");
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, verilogOutput);

   return verilogOutput;
}

void moduleGenerator::add_port_parameters(structural_objectRef generated_port,structural_objectRef original_port)
{
   original_port->copy(generated_port);
   generated_port->get_typeRef()->size=original_port->get_typeRef()->size;
   generated_port->get_typeRef()->vector_size=original_port->get_typeRef()->vector_size;
}

void moduleGenerator::specialize_fu(std::string fuName, vertex ve, std::string libraryId, const technology_managerRef TM, const FunctionBehaviorConstRef FB, std::string new_fu_name, std::map<std::string,technology_nodeRef> & new_fu, target_device::type_t dv_type)
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Found variable component: "+fuName);
   std::vector<std::tuple<unsigned int,unsigned int> > required_variables = HLSMgr->get_required_values(FB->CGetBehavioralHelper()->get_function_index(), ve);

   const library_managerRef libraryManager = TM->get_library_manager(libraryId);

   technology_nodeRef techNode_obj=libraryManager->get_fu(fuName);
   structural_managerRef structManager_obj=GetPointer<functional_unit>(techNode_obj)->CM;
   structural_objectRef fu_obj=structManager_obj->get_circ();
   module *fu_module=GetPointer<module>(fu_obj);

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Specializing: "+fuName+" as "+new_fu_name);

   if(new_fu.find(new_fu_name)!=new_fu.end()){
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, new_fu_name+" already in the library");
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Specialization completed");
   }
   else{
      structural_objectRef top;
      structural_managerRef CM;
      unsigned int n_ports = Param->isOption(OPT_channels_number) ? Param->getOption<unsigned int>(OPT_channels_number) : 0;

      std::string NP_parameters;

      //std::cout<<"Start creation"<<std::endl;

      CM = structural_managerRef(new structural_manager(Param));
      structural_type_descriptorRef module_type = structural_type_descriptorRef(new structural_type_descriptor(new_fu_name));
      CM->set_top_info(new_fu_name, module_type);
      top = CM->get_circ();
      GetPointer<module>(top)->set_generated();
      ///add description and license
      GetPointer<module>(top)->set_description(fu_module->get_description());
      GetPointer<module>(top)->set_copyright(fu_module->get_copyright());
      GetPointer<module>(top)->set_authors(fu_module->get_authors());
      GetPointer<module>(top)->set_license(fu_module->get_license());

      //std::cout<<"Module created, adding ports"<<std::endl;

      int portNum=1;
      std::string param_list= fu_module->get_NP_functionality()->get_NP_functionality(NP_functionality::LIBRARY);

      /*Adding ports*/
      unsigned int inPortSize=static_cast<unsigned int>(fu_module->get_in_port_size());
      unsigned int outPortSize=static_cast<unsigned int>(fu_module->get_out_port_size());


      structural_objectRef generated_port;
      std::string port_name="";
      unsigned int currentPort=0;
      for(currentPort=0;currentPort<inPortSize;currentPort++){
         structural_objectRef curr_port = fu_module->get_in_port(currentPort);
         if(GetPointer<port_o>(curr_port)->get_is_var_args()){
            for(std::vector<std::tuple<unsigned int,unsigned int> >::iterator l = required_variables.begin(); l != required_variables.end(); l++)
            {
               unsigned int var = std::get<0>(*l);
               structural_type_descriptorRef dt = getDataType(var,FB);
               /// normalize type
               if(dt->vector_size == 0)
               {
                  if(dt->size <= 8)
                     dt->size = 8;
                  else if(dt->size <= 16)
                     dt->size = 16;
                  else if(dt->size <= 32)
                     dt->size = 32;
                  else if(dt->size <= 64)
                     dt->size = 64;
                  else
                     THROW_ERROR("not expected size");
               }
               port_name="in"+STR(portNum);
               generated_port=CM->add_port(port_name, port_o::IN, top, dt);
               generated_port->get_typeRef()->size=dt->size;
               generated_port->get_typeRef()->vector_size=dt->vector_size;
               param_list=param_list+" "+port_name;
               portNum++;
               //std::cout<<"Added port NAME: "<<generated_port->get_id()<<" TYPE: "<<generated_port->get_typeRef()->get_name()<<" CLOCK: "<<GetPointer<port_o>(generated_port)->get_is_clock()<<" DATA_SIZE:"<<STR(generated_port->get_typeRef()->size)<<" VECTOR_SIZE:"<<STR(generated_port->get_typeRef()->vector_size)<<std::endl;
            }
         }
         else{
            port_name=curr_port->get_id();
            if(curr_port->get_kind() == port_vector_o_K)
            {
               generated_port=CM->add_port_vector(port_name, port_o::IN, n_ports, top, curr_port->get_typeRef());
            }
            else
               generated_port=CM->add_port(port_name, port_o::IN, top, curr_port->get_typeRef());
            add_port_parameters(generated_port, curr_port);
            //std::cout<<"Added port NAME: "<<generated_port->get_id()<<" TYPE: "<<generated_port->get_typeRef()->get_name()<<" CLOCK: "<<GetPointer<port_o>(generated_port)->get_is_clock()<<" DATA_SIZE:"<<STR(generated_port->get_typeRef()->size)<<" VECTOR_SIZE:"<<STR(generated_port->get_typeRef()->vector_size)<<std::endl;
         }
      }

      //std::cout<<"Adding output ports"<<std::endl;

      for(currentPort=0;currentPort<outPortSize;currentPort++)
      {
         structural_objectRef curr_port = fu_module->get_out_port(currentPort);
         if(curr_port->get_kind() == port_vector_o_K)
         {
            generated_port=CM->add_port_vector(curr_port->get_id(), port_o::OUT, n_ports, top, curr_port->get_typeRef());
         }
         else
            generated_port=CM->add_port(curr_port->get_id(), port_o::OUT, top, curr_port->get_typeRef());
         add_port_parameters(generated_port, curr_port);
      }

      NP_parameters = new_fu_name + std::string(" ") + param_list;
      CM->add_NP_functionality(top, NP_functionality::LIBRARY, NP_parameters);
      std::string verilog_template=fu_module->get_NP_functionality()->get_NP_functionality(NP_functionality::VERILOG_GENERATOR);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, new_fu_name+": Generating dynamic verilog code");
      std::string verilog_code=generate_verilog(verilog_template, required_variables, FB, Param->getOption<std::string>("dynamic_generators_dir"));

      CM->add_NP_functionality(top, NP_functionality::VERILOG_PROVIDED, verilog_code);

      technology_nodeRef new_techNode_obj = technology_nodeRef(new functional_unit);
      if (GetPointer<functional_unit>(techNode_obj)->area_m)
      {
         GetPointer<functional_unit>(new_techNode_obj)->area_m = area_model::create_model(dv_type, Param);
      }
      GetPointer<functional_unit>(new_techNode_obj)->functional_unit_name = new_fu_name;
      GetPointer<functional_unit>(new_techNode_obj)->CM = CM;

      new_fu.insert(std::make_pair(new_fu_name,new_techNode_obj));

      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, new_fu_name+" created successfully");

      std::vector<technology_nodeRef> op_vec=GetPointer<functional_unit>(techNode_obj)->get_operations();
      for(std::vector<technology_nodeRef>::iterator techIter=op_vec.begin();techIter!=op_vec.end();techIter++)
      {
         technology_nodeRef techNode_fu=*techIter;
         GetPointer<functional_unit>(new_techNode_obj)->add(techNode_fu);
      }

      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Specialization completed");
   }
}
