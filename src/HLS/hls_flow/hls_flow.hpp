/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file hls_flow.hpp
 * @brief Base class for the high-level synthesis flow (i.e., predefined collection of passes).
 *
 * This struct contains all the information useful to create a high-level synthesis flow.
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef HLS_FLOW_HPP
#define HLS_FLOW_HPP

///Autoheader include
#include "config_HAVE_BEAGLE.hpp"
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "hls_step.hpp"
REF_FORWARD_DECL(hls_flow);
REF_FORWARD_DECL(application_manager);
REF_FORWARD_DECL(HLS_target);
REF_FORWARD_DECL(HLS_constraints);

#include <map>
#include <set>
#include <string>

/**
 * @defgroup HLS High-Level Synthesis
 * This group contains files that are used to implement high-level synthesis process.
 *
 * This package contains different subproject:
 * - \ref allocation this is a package to manage information about allocation and binding constraints
 *
 * To get more details into this process see \ref src_HLS_page
 */

#include "graph.hpp"
#include <unordered_set>

/**
 * @class hls_flow
 * @ingroup HLS
 * Data structure that contains all the information about the HLS flow
 */
struct hls_flow : public HLS_step
{
   ///implementations resulting from the synthesis
   std::set<hlsRef> resulting_implementations;

   enum flow_type
   {
      STANDARD,
      VIRTUAL
#if HAVE_EXPERIMENTAL
      ,
      EXPLORE_MUX,
      FU_REG_BINDING,
      DUMP
#endif
#if HAVE_BEAGLE
      ,
      DSE,
#endif
   };

   /**
    * Constructor
    */
   hls_flow(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

   /**
    * Destructor
    */
   virtual ~hls_flow();

   /**
    * Creates desidered high level synthesis flow specialization. You have to give the desidered design flow identifier.
    * @param type is the desidered high level synthesis flow descriptor
    * @param Param is the reference to the parameter register
    * @param HLS is the class containing the specification of the HLS synthesis
    */
   static
   hls_flowRef factory(flow_type type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

   /**
    * Returns all the implementations resulting from the synthesis
    */
   std::set<hlsRef> get_all_implementations() const;

};
///refcount definition of the class
typedef refcount<hls_flow> hls_flowRef;

#endif
