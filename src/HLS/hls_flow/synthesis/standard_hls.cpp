/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file standard_hls.cpp
 * @brief Implementation of the methods to create the structural description of the component
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "standard_hls.hpp"

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "add_library.hpp"
#include "datapath_creator.hpp"
#include "controller_creator.hpp"
#include "top_entity.hpp"
#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"
#include "cpu_time.hpp"
#include "call_graph_manager.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"
#include "application_manager.hpp"
#include "module_interface.hpp"
#include "technology_manager.hpp"

#include "hls.hpp"
#include "hls_constraints.hpp"
#include "hls_target.hpp"
#include "hls_manager.hpp"

standard_hls::standard_hls(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   hls_flow (_Param, _HLSMgr, _funId)
{

}

standard_hls::~standard_hls()
{

}

std::string standard_hls::get_kind_text() const
{
   return "Standard HLS flow";
}

void standard_hls::exec()
{
   START_TIME(HLS->HLS_execution_time);

   if (output_level >= OUTPUT_LEVEL_VERBOSE)
   {
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "************************************************");
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*                                              *");
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*      HIGH LEVEL SYNTHESIS STANDARD FLOW      *");
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*                                              *");
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "************************************************");
   }

   hls_flow::flow_type synthesis_flow = hls_flow::VIRTUAL;
#if HAVE_EXPERIMENTAL
   if (Param->isOption("explore-mux") && Param->getOption<bool>("explore-mux"))
      synthesis_flow = hls_flow::EXPLORE_MUX;
   if (Param->isOption("explore-fu-reg") && Param->getOption<bool>("explore-fu-reg"))
      synthesis_flow = hls_flow::FU_REG_BINDING;
#endif
   hls_flowRef virt_flow = hls_flow::factory(synthesis_flow, Param, HLSMgr, funId);
   virt_flow->exec();

   datapath_creator::datapath_type dt = static_cast<datapath_creator::datapath_type>(Param->getOption<unsigned int>(OPT_datapath_architecture));
   datapath_creatorRef DC = datapath_creator::factory(dt, Param, HLSMgr, funId);
   long datapath_time;
   START_TIME(datapath_time);
   DC->exec();
   STOP_TIME(datapath_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Datapath creation (" + DC->get_kind_text() + ") completed in " + print_cpu_time(datapath_time) + " seconds");

   controller_creator::controller_type ct = static_cast<controller_creator::controller_type>(Param->getOption<unsigned int>(OPT_controller_architecture));
   controller_creatorRef CC = controller_creator::factory(ct, Param, HLSMgr, funId);
   long controller_time;
   START_TIME(controller_time);
   CC->exec();
   STOP_TIME(controller_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Controller creation (" + CC->get_kind_text() + ") completed in " + print_cpu_time(controller_time) + " seconds");

   long top_time;
   START_TIME(top_time);
   top_entityRef TOP = top_entityRef(new top_entity(Param, HLSMgr, funId));
   TOP->exec();
   STOP_TIME(top_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Top entity creation (" + TOP->get_kind_text() + ") completed in " + print_cpu_time(top_time) + " seconds");

   long library_time;
   START_TIME(library_time);
   add_libraryRef lib = add_libraryRef(new add_library(Param, HLSMgr, funId));
   lib->exec();
   STOP_TIME(top_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Component added to WORK_LIBRARY (" + TOP->get_kind_text() + ") completed in " + print_cpu_time(library_time) + " seconds");

   const BehavioralHelperConstRef BH = HLS->FB->CGetBehavioralHelper();
   bool is_top = (BH->get_function_index() == HLSMgr->CGetCallGraphManager()->GetRootFunction());
   if(is_top)
   {
      /// generate module interface
      if (Param->getOption<bool>(OPT_interface))
      {
         if (!HLS->top) THROW_ERROR("Interface can be created only for the top entity");
         long interface_time;
         START_TIME(interface_time);
         module_interface::interface_t int_type = static_cast<module_interface::interface_t>(Param->getOption<unsigned int>(OPT_interface_type));
         module_interfaceRef miRef = module_interface::factory(int_type, Param, HLSMgr, funId);
         miRef->exec();
         STOP_TIME(interface_time);
         PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Interface creation (" + miRef->get_kind_text() + ") completed in " + print_cpu_time(interface_time) + " seconds");

         START_TIME(library_time);
         add_libraryRef lib_int = add_libraryRef(new add_library(Param, HLSMgr, funId));
         lib_int->exec();
         STOP_TIME(top_time);
         PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Interface added to WORK_LIBRARY (" + TOP->get_kind_text() + ") completed in " + print_cpu_time(library_time) + " seconds");

      }
   }

   STOP_TIME(HLS->HLS_execution_time);

   resulting_implementations.insert(HLS);

   PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "");
   return;
}
