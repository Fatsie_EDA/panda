/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file virtual_hls.cpp
 * @brief A brief description of the C++ Source File
 *
 * Here goes a detailed description of the file
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "hls.hpp"
#include "virtual_hls.hpp"

#include "allocation.hpp"
#include "structural_objects.hpp"

#include "call_graph.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"

#include "scheduling.hpp"
#include "schedule.hpp"
#include "chaining.hpp"
#include "state_transition_graph.hpp"
#include "state_transition_graph_manager.hpp"
#include "STG_creator.hpp"
#include "liveness_computer.hpp"
#include "storage_value_insertion.hpp"
#include "easy_module_binding.hpp"
#include "reg_binding_creator.hpp"
#include "conn_binding_creator.hpp"
#include "fu_binding_creator.hpp"


#include "Parameter.hpp"

#include "dbgPrintHelper.hpp"
#include "cpu_time.hpp"

#include "polixml.hpp"

virtual_hls::virtual_hls(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
      hls_flow (_Param, _HLSMgr, _funId)
{

}

virtual_hls::~virtual_hls()
{

}

std::string virtual_hls::get_kind_text() const
{
   return "Virtual HLS flow";
}

void virtual_hls::exec()
{
   const std::string function_name = HLS->FB->CGetBehavioralHelper()->get_function_name();
   if (debug_level >= DEBUG_LEVEL_VERBOSE)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "************************************************");
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "*                                              *");
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "*       HIGH LEVEL SYNTHESIS VIRTUAL FLOW      *");
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "*                                              *");
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "************************************************");
   }

   long module_allocation_time;
   START_TIME(module_allocation_time);
   HLS->ALL = allocation::factory(Param, HLSMgr, funId);
   HLS->ALL->exec();
   STOP_TIME(module_allocation_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Module allocation (" + HLS->ALL->get_kind_text() + ") completed in " + print_cpu_time(module_allocation_time) + " seconds");

   /// execute scheduling algorithm
   scheduling::scheduling_type scheduling_algorithm = static_cast<scheduling::scheduling_type>(Param->getOption<unsigned int>(OPT_scheduling_algorithm));
   schedulingRef sch_algorithm = scheduling::factory(scheduling_algorithm, Param, HLSMgr, funId);
   long schedule_time;
   START_TIME(schedule_time);
   sch_algorithm->exec();
   STOP_TIME(schedule_time);
   HLS->Rsch->set_spec(sch_algorithm->get_spec());
   //print scheduling results
   HLS->print_scheduling(std::cerr);
   if(Param->getOption<bool>(OPT_print_dot))
   {
      HLS->Rsch->WriteDot("HLS_scheduling.dot");
   }
   if (debug_level >= DEBUG_LEVEL_VERBOSE)
   {
      xml_document document;
      xml_element* nodeRoot = document.create_root_node("hls");
      HLS->xwrite(nodeRoot);
      document.write_to_file_formatted(function_name + "_scheduling.XML");
   }
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Scheduling computation (" + sch_algorithm->get_kind_text() + ") completed in " + print_cpu_time(schedule_time) + " seconds");

   STG_creator::stg_type type = static_cast<STG_creator::stg_type>(Param->getOption<unsigned int>(OPT_stg_algorithm));
   STG_creatorRef stg_creator = STG_creator::factory(type, Param, HLSMgr, funId);
   long stg_time;
   START_TIME(stg_time);
   stg_creator->exec();
   STOP_TIME(stg_time);
   HLS->STG->print_statistics();
   if(Param->getOption<bool>(OPT_print_dot))
   {
      HLS->STG->CGetStg()->WriteDot("HLS_STGraph.dot");
   }
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "State Transition Graph creation (" + stg_creator->get_kind_text() + ") completed in " + print_cpu_time(stg_time) + " seconds");


   long chaining_time;
   START_TIME(chaining_time);
   chaining::algorithm_t chaining_algorithm = static_cast<chaining::algorithm_t>(Param->getOption<unsigned int>(OPT_chaining_algorithm));
   HLS->Rchain = chaining::factory(chaining_algorithm, Param, HLSMgr, funId);
   HLS->Rchain->exec();
   STOP_TIME(chaining_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Chaining analysis (" + HLS->Rchain->get_kind_text() + ") completed in " + print_cpu_time(chaining_time) + " seconds");

   long partial_binding_time;
   START_TIME(partial_binding_time);
   easy_module_bindingRef eb = easy_module_bindingRef(new easy_module_binding(Param, HLSMgr, funId));
   eb->exec();
   STOP_TIME(partial_binding_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Partial module binding analysis (" + eb->get_kind_text() + ") completed in " + print_cpu_time(partial_binding_time) + " seconds");


   long dataflow_time;
   START_TIME(dataflow_time);
   liveness_computer::liveness_algorithm liveness_algorithm = static_cast<liveness_computer::liveness_algorithm>(Param->getOption<unsigned int>(OPT_liveness_algorithm));
   liveness_computerRef livc = liveness_computer::factory(liveness_algorithm, Param, HLSMgr, funId);
   livc->exec();
   STOP_TIME(dataflow_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Liveness calculation (" + livc->get_kind_text() + ") completed in " + print_cpu_time(dataflow_time) + " seconds");

   storage_value_insertionRef svi_algorithm;

   long storage_value_insertion_time;
   START_TIME(storage_value_insertion_time);
   storage_value_insertion::storage_value_insertion_type svi_algorithm_t = static_cast<storage_value_insertion::storage_value_insertion_type>(Param->getOption<unsigned int>(OPT_storage_value_insertion_algorithm));
   svi_algorithm = storage_value_insertion::factory(svi_algorithm_t, Param, HLSMgr, funId);
   svi_algorithm->exec();
   HLS->svi_data = svi_algorithm;
   STOP_TIME(storage_value_insertion_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Storage value insertion analysis (" + svi_algorithm->get_kind_text() + ") completed in " + print_cpu_time(storage_value_insertion_time) + " seconds");

#define REGISTER_BEFORE 0
#if REGISTER_BEFORE

   long register_time;
   START_TIME(register_time);
   reg_binding_creator::type_t reg_binding_algorithm = static_cast<reg_binding_creator::type_t>(Param->getOption<unsigned int>(OPT_register_allocation_algorithm));
   reg_binding_creatorRef reg_algorithm = reg_binding_creator::factory(reg_binding_algorithm, Param, HLSMgr, funId);
   reg_algorithm->exec();
   STOP_TIME(register_time);
   HLS->print_register_binding(std::cerr);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Register allocation and binding analysis (" + reg_algorithm->get_kind_text() + ") completed in "  + print_cpu_time(register_time) + " seconds");

   long module_time;
   START_TIME(module_time);
   fu_binding_creator::type_t fu_binding_algorithm = static_cast<fu_binding_creator::type_t>(Param->getOption<unsigned int>(OPT_fu_binding_algorithm));
   fu_binding_creatorRef mdb = fu_binding_creator::factory(fu_binding_algorithm, Param, HLSMgr, funId);
   mdb->exec();
   STOP_TIME(module_time);
   if (debug_level >= DEBUG_LEVEL_VERBOSE)
      HLS->print_scheduling(std::cerr);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Module binding analysis (" + mdb->get_kind_text() + ") completed in " + print_cpu_time(module_time) + " seconds");

#else

   long module_time;
   START_TIME(module_time);
   fu_binding_creator::type_t fu_binding_algorithm = static_cast<fu_binding_creator::type_t>(Param->getOption<unsigned int>(OPT_fu_binding_algorithm));
   fu_binding_creatorRef mdb = fu_binding_creator::factory(fu_binding_algorithm, Param, HLSMgr, funId);
   mdb->exec();
   STOP_TIME(module_time);
   if (debug_level >= DEBUG_LEVEL_VERBOSE)
      HLS->print_scheduling(std::cerr);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Module binding analysis (" + mdb->get_kind_text() + ") completed in " + print_cpu_time(module_time) + " seconds");

   long register_time;
   START_TIME(register_time);
   reg_binding_creator::type_t reg_binding_algorithm = static_cast<reg_binding_creator::type_t>(Param->getOption<unsigned int>(OPT_register_allocation_algorithm));
   reg_binding_creatorRef reg_algorithm = reg_binding_creator::factory(reg_binding_algorithm, Param, HLSMgr, funId);
   reg_algorithm->exec();
   STOP_TIME(register_time);
   HLS->print_register_binding(std::cerr);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Register allocation and binding analysis (" + reg_algorithm->get_kind_text() + ") completed in "  + print_cpu_time(register_time) + " seconds");

#endif

   long connection_time;
   START_TIME(connection_time);
   conn_binding_creator::algorithm_t datapath_architecture = static_cast<conn_binding_creator::algorithm_t>(Param->getOption<unsigned int>(OPT_datapath_interconnection_algorithm));
   conn_binding_creatorRef conn_alg = conn_binding_creator::factory(datapath_architecture, Param, HLSMgr, funId);
   conn_alg->exec();
   STOP_TIME(connection_time);
   HLS->print_connection_binding(std::cerr);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Datapath interconnection binding analysis (" + conn_alg->get_kind_text() + ") completed in " + print_cpu_time(connection_time) + " seconds");

   resulting_implementations.insert(HLS);
   return;
}
