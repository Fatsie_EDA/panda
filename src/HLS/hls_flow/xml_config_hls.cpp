/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file xml_config_hls.cpp
 * @brief Implementation of the methods to perform HLS based on a flow configured via XML file
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "config_HAVE_EXPERIMENTAL.hpp"

///Header include
#include "xml_config_hls.hpp"

#include "hls.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"
#include "application_manager.hpp"
#include "call_graph.hpp"
#include "call_graph_manager.hpp"
#include "tree_manager.hpp"
#include "tree_helper.hpp"

#include "memory.hpp"
///supported steps
#include "add_library.hpp"
#include "allocation.hpp"
#include "generate_hdl.hpp"
#include "chaining.hpp"
#include "memory_allocation.hpp"
#include "scheduling.hpp"
#include "STG_creator.hpp"
#include "liveness_computer.hpp"
#include "storage_value_insertion.hpp"
#include "reg_binding_creator.hpp"
#include "fu_binding_creator.hpp"
#include "conn_binding_creator.hpp"
#include "datapath_creator.hpp"
#include "controller_creator.hpp"
#include "top_entity.hpp"
#include "module_interface.hpp"
#if HAVE_EXPERIMENTAL
#include "evaluation.hpp"
#include "generate_resp.hpp"
#endif

#include "hls_constraints.hpp"
#include "hls_manager.hpp"
#include "hls_target.hpp"
#include "hls_flow.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"
#include "xml_dom_parser.hpp"

#include "fileIO.hpp"
#include "Parameter.hpp"

xml_config_hls::xml_config_hls(const ParameterConstRef _Param, const HLS_managerRef _AppM) :
   HLS_synthesis_flow(_Param, _AppM)
{

}

xml_config_hls::~xml_config_hls()
{

}

bool xml_config_hls::checkNode(const xml_element* node, unsigned int funId, const std::string& ref_step)
{
   if (node->get_name() != ref_step)
   {
      return false;
   }
   const tree_managerRef TreeM = AppM->get_tree_manager();
   std::string function_name;
   if (CE_XVM(function_name, node))
   {
      LOAD_XVM(function_name, node);
      std::cerr << "found function_name" << std::endl;
      unsigned int function_id = funId;
      if (function_name == "$TOP")
      {
         const unsigned int top_function_id = AppM->CGetCallGraphManager()->GetRootFunction();
         if (!function_id || top_function_id != function_id)
            return false;
      }
      else if (!function_id || function_name != tree_helper::name_function(TreeM, function_id))
      {
         return false;
      }
   }
   return true;
}

void xml_config_hls::xload(const xml_element* node, unsigned int funId)
{
   const tree_managerRef TreeM = AppM->get_tree_manager();
   const xml_node::node_list list = node->get_children();
   for (xml_node::node_list::const_iterator l = list.begin(); l != list.end(); l++)
   {
      const xml_element* child = GetPointer<xml_element>(*l);
      if (!child) continue;

      if (checkNode(child, funId, "HLS_functions"))
      {
         const xml_node::node_list list1 = child->get_children();
         for (xml_node::node_list::const_iterator l1 = list1.begin(); l1 != list1.end(); l1++)
         {
            const xml_element* child1 = GetPointer<xml_element>(*l1);
            if (!child1) continue;
            if (checkNode(child1, funId, "HLS_function"))
            {
               std::string name;
               LOAD_XVM(name, child1);
               unsigned int functionId = TreeM->function_index(name);
               functionConfig[functionId] = child1;
            }
         }
         for(std::list<unsigned int>::iterator f = functions.begin(); f != functions.end(); f++)
         {
            if (functionConfig.find(*f) != functionConfig.end())
               xload(functionConfig[*f], *f);
            else
               xload(child, *f);
         }
      }
      else if (checkNode(child, funId, "function"))
      {
         std::string name;
         LOAD_XVM(name, child);
         unsigned int functionId = TreeM->function_index(name);
         xload(child, functionId);
      }
      else if (checkNode(child, funId, "memory_allocation"))
      {
         HLS_stepRef step = memory_allocation::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "scheduling"))
      {
         HLS_stepRef step = scheduling::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "module_allocation"))
      {
         AppM->get_HLS(funId)->ALL = allocation::xload_factory(child, Param, AppM, funId);
         steps.push_back(AppM->get_HLS(funId)->ALL);
      }
      else if (checkNode(child, funId, "chaining"))
      {
         AppM->get_HLS(funId)->Rchain = chaining::xload_factory(child, Param, AppM, funId);
         steps.push_back(AppM->get_HLS(funId)->Rchain);
      }
      else if (checkNode(child, funId, "stg_creation"))
      {
         HLS_stepRef step = STG_creator::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "liveness_computation"))
      {
         HLS_stepRef step = liveness_computer::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "storage_value_insertion"))
      {
         storage_value_insertionRef step = storage_value_insertion::xload_factory(child, Param, AppM, funId);
         AppM->get_HLS(funId)->svi_data = step;
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "generate_hdl"))
      {
         HLS_stepRef step = generate_hdl::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "register_allocation"))
      {
         HLS_stepRef step = reg_binding_creator::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "add_library"))
      {
         HLS_stepRef step = add_library::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "fu_binding"))
      {
         HLS_stepRef step = fu_binding_creator::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "interconnection_binding"))
      {
         HLS_stepRef step = conn_binding_creator::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "datapath_creation"))
      {
         HLS_stepRef step = datapath_creator::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "controller_creation"))
      {
         HLS_stepRef step = controller_creator::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "interface"))
      {
         HLS_stepRef step = module_interface::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "top_creation"))
      {
         HLS_stepRef step = top_entityRef(new top_entity(Param, AppM, funId));
         steps.push_back(step);
      }
#if HAVE_EXPERIMENTAL
      else if (checkNode(child, funId, "evaluation"))
      {
         HLS_stepRef step = evaluation::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
      else if (checkNode(child, funId, "generate_resp"))
      {
         HLS_stepRef step = generate_resp::xload_factory(child, Param, AppM, funId);
         steps.push_back(step);
      }
#endif
   }
}

void xml_config_hls::performSynthesis()
{
   const CallGraphManagerConstRef call_graph_manager = AppM->CGetCallGraphManager();
   call_graph_manager->GetReachedBodyFunctions(functions);

   const tree_managerRef TreeM = AppM->get_tree_manager();
   for(std::list<unsigned int>::iterator f = functions.begin(); f != functions.end(); f++)
   {
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Function to be synthetized: " + tree_helper::name_function(TreeM, *f));
      HLS_manager::create_HLS(AppM, *f);
   }

   std::string xml_file = Param->getOption<std::string>(OPT_xml_input_configuration);
   if (!boost::filesystem::exists(xml_file)) THROW_ERROR("File \"" + xml_file + "\" does not exist!");

   INDENT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "-->Parsing of configuration file " + xml_file);
   xml_nodeRef default_config, function_config;
   fileIO_istreamRef sname = fileIO_istream_open(xml_file);
   xml_dom_parser parser;
   parser.parse_stream(*sname);
   if (parser)
   {
      const xml_element* node = parser.get_document()->get_root_node(); //deleted by DomParser.
      const xml_node::node_list list = node->get_children();
      for (xml_node::node_list::const_iterator l = list.begin(); l != list.end(); l++)
      {
         const xml_element* child = GetPointer<xml_element>(*l);
         if (!child) continue;

         if (child->get_name() == "HLS_flow")
         {
            std::string function_name;
            if (CE_XVM(function_name, child))
            {
               LOAD_XVM(function_name, child);
               ///TODO: name of the top function
               std::string fname = AppM->get_HLS(0)->HLS_C->get_function_name();
               if (fname == function_name)
               {
                  function_config = *l;
               }
            }
            else if (!default_config)
            {
               default_config = *l;
            }
            else
            {
               THROW_ERROR("It is possible to specify only one default configuration");
            }
         }
      }
   }
   if (function_config)
   {
      xload(GetPointer<xml_element>(function_config), 0);
   }
   else if (default_config)
   {
      xload(GetPointer<xml_element>(default_config), 0);
   }
   else
   {
      THROW_ERROR("No configuration specified for the HLS flow in the configuration file " + xml_file);
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "<--Parsing completed!");

   INDENT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "-->Starting flow execution");
   for (unsigned int n = 0; n < steps.size(); n++)
   {
      if (n) INDENT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "");
      std::string stepName = steps[n]->get_kind_text();
      unsigned int functionId = steps[n]->get_HLS()->functionId;
      if (functionId)
      {
         stepName += ":" + tree_helper::name_function(TreeM, functionId);
      }
      else
      {
         stepName += ":<global>";
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "-->Starting the execution of step: " + stepName);
      steps[n]->exec();
      INDENT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "<--Completed the execution of step: " + stepName);
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "<--Execution flow completed!");
}
