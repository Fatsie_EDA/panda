/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file hls_flow.cpp
 * @brief Data structure implementation for high-level synthesis flow.
 *
 * This file contains all the implementations used by hls class to manage the
 * high level synthesis flow
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_BEAGLE.hpp"
#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_SKIP_WARNING_SECTIONS.hpp"

#include "hls_flow.hpp"

///application datastructure
#include "application_manager.hpp"

/// Implemented flows
#include "standard_hls.hpp"
#include "virtual_hls.hpp"
#if HAVE_EXPERIMENTAL
#include "explore_mux_hls.hpp"
#include "fu_reg_binding_hls.hpp"
#include "dump_hls.hpp"
#if HAVE_BEAGLE
#if SKIP_WARNING_SECTIONS
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Woverloaded-virtual"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wold-style-cast"
#pragma GCC diagnostic ignored "-Wextra"
#pragma GCC diagnostic ignored "-Wignored-qualifiers"
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wsign-promo"
#endif
#include "dse_hls.hpp"
#endif
#endif
/// HLS datastructure
#include "hls.hpp"

/// Parameters class
#include "Parameter.hpp"

/// General PandA includes
#include "utility.hpp"
#include "exceptions.hpp"

hls_flow::hls_flow(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
      HLS_step(_Param, _HLSMgr, _funId)
{

}

hls_flow::~hls_flow()
{

}

hls_flowRef hls_flow::factory(flow_type _flow, const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId)
{
   switch (_flow)
   {
      case STANDARD:
         return hls_flowRef(new standard_hls(_Param, _HLSMgr, _funId));
      case VIRTUAL:
         return hls_flowRef(new virtual_hls(_Param, _HLSMgr, _funId));
#if HAVE_EXPERIMENTAL
      case DUMP:
         return hls_flowRef(new dump_hls(_Param, _HLSMgr, _funId));
      case EXPLORE_MUX:
         return hls_flowRef(new explore_mux_hls(_Param, _HLSMgr, _funId));
      case FU_REG_BINDING:
         return hls_flowRef(new fu_reg_binding_hls(_Param, _HLSMgr, _funId));
#endif
#if HAVE_BEAGLE
      case DSE:
         return hls_flowRef(new dse_hls(_Param, _HLSMgr, _funId));
#endif
      default:
         THROW_UNREACHABLE("Not supported high-level-synthesis flow " + boost::lexical_cast<std::string>(_flow));
   }
   return hls_flowRef();
}

std::set<hlsRef> hls_flow::get_all_implementations() const
{
   return resulting_implementations;
}
