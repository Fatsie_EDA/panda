/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file classical_synthesis_flow.cpp
 * @brief
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */
#include "classical_synthesis_flow.hpp"

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_VCD_BUILT.hpp"

#include "hls_flow.hpp"

/// ----------- Intermediate Representation ----------- ///
/// Intermediate Representation Parsing
#include "parse_tree.hpp"
/// Intermediate Representation Datastructure
#include "tree_manager.hpp"
#include "tree_helper.hpp"
/// Intermediate Representation Management
#include "hls_manager.hpp"
#include "call_graph_manager.hpp"
#include "call_graph.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"
#include "hls.hpp"
#include "hls_target.hpp"
#include "HLStestbench.hpp"
#include "structural_manager.hpp"
#include "evaluation.hpp"

/// ----------- Scripts generation ----------- ///
#include "export_core.hpp"
#include "BackendFlow.hpp"

/// ----------- Resource Library ----------- ///
/// technology information
#include "technology_manager.hpp"
#include "target_technology.hpp"
#include "target_device.hpp"
#include "technology_node.hpp"
#include "time_model.hpp"
#include "area_model.hpp"
/// Memories
#include "memory_allocation.hpp"
#include "memory.hpp"

/// ----------- High-Level Synthesis Constraints ----------- ///
/// HLS Constraints Datastructure
#include "hls_constraints.hpp"

/// ----------- vcd debug ----------- ///
#if HAVE_EXPERIMENTAL &&  HAVE_VCD_BUILT
/// ----------- HDL backend ----------- ///
#include "HDL_manager.hpp"
#include "language_writer.hpp"
#include "structural_manager.hpp"
/// ----------- C backend ----------- ///
#include "c_backend_step_factory.hpp"
#include "design_flow_manager.hpp"
/// ----------- VCD datastructures ----------- ///
#include "vcd_data.hpp"
#include "vcd_parser.hpp"
#include "vcd_utility.hpp"
#include "vcd_debug_instruction_writer.hpp"
#endif

#include "Parameter.hpp"
#include "cpu_time.hpp"

HLS_classical_synthesis::HLS_classical_synthesis(const ParameterConstRef _Param, const HLS_managerRef _AppM) :
   HLS_synthesis_flow(_Param, _AppM)
{

}

HLS_classical_synthesis::~HLS_classical_synthesis()
{

}

void HLS_classical_synthesis::performSynthesis()
{
   START_TIME(AppM->HLS_execution_time);
   const HLS_targetRef HLS_T = AppM->get_HLS_target();
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Starting implementation generation...");

   ///performing the memory allocation
   long memory_time;
   START_TIME(memory_time);
   memory_allocation::algorithm_t algorithm = static_cast<memory_allocation::algorithm_t>(Param->getOption<unsigned int>(OPT_memory_allocation_algorithm));
   memory_allocation::policy_t policy = static_cast<memory_allocation::policy_t>(Param->getOption<unsigned int>(OPT_memory_allocation_policy));
   memory_allocationRef m_alloc = memory_allocation::factory(algorithm, policy, Param, AppM, 0);
   m_alloc->exec();
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Total amount of internally allocated memory: " + STR(AppM->Rmem->get_allocated_space()) + " bytes");
   unsigned int base_address = AppM->base_address;
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Total amount of externally allocated memory: " + STR(AppM->Rmem->get_memory_address()-base_address) + " bytes");
   STOP_TIME(memory_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Memory allocation completed in " + print_cpu_time(memory_time) + " seconds\n");


   std::list<unsigned int> functions;
   const CallGraphManagerConstRef call_graph_manager = AppM->CGetCallGraphManager();
   call_graph_manager->GetReachedBodyFunctions(functions);
   const tree_managerRef TreeM = AppM->get_tree_manager();
   const unsigned int top_function = call_graph_manager->GetRootFunction();

   /// check if __builtin_memcpy has to be synthesized
   if(AppM->Rmem->has_implicit_memcpy())
   {
      unsigned int memcpy_function_id = TreeM->function_index("__builtin_memcpy");
      if(std::find(functions.begin(), functions.end(), memcpy_function_id) != functions.end())
         functions.erase(std::find(functions.begin(), functions.end(), memcpy_function_id));
      functions.push_front(memcpy_function_id);
   }
   for(std::list<unsigned int>::iterator f = functions.begin(); f != functions.end(); f++)
   {
      PRINT_OUT_MEX(OUTPUT_LEVEL_PEDANTIC, output_level, "Function to be synthetized: " + tree_helper::name_function(TreeM, *f));
   }
   PRINT_OUT_MEX(OUTPUT_LEVEL_PEDANTIC, output_level, "");

   const technology_managerRef TM = HLS_T->get_technology_manager();
   std::map<unsigned int, hlsRef> implementations;
   for(std::list<unsigned int>::iterator f = functions.begin(); f != functions.end(); f++)
   {
      std::string function_name = tree_helper::normalized_ID(tree_helper::name_function(TreeM, *f));
      if(TM->get_library(function_name) != "") //function already implemented
         continue;
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Synthesizing function: " + function_name);
      structural_managerRef SM;
      TM->add_resource(WORK_LIBRARY, function_name, SM);
      TM->add_operation(WORK_LIBRARY, function_name, function_name);
      functional_unit* fu = GetPointer<functional_unit>(TM->get_fu(function_name, WORK_LIBRARY));

      const hlsRef HLS = HLS_manager::create_HLS(AppM, *f);
      fu->set_clock_period(HLS->HLS_C->get_clock_period());
      fu->set_clock_period_resource_fraction(HLS->HLS_C->get_clock_period_resource_fraction());

      double clock_period = HLS->HLS_C->get_clock_period();
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Clock period: " << clock_period << "ns (" << 1000.0 / clock_period << "MHz)");
      // Creates the specificated high-level synthesis flow
      hls_flow::flow_type flow = static_cast<hls_flow::flow_type>(Param->getOption<unsigned int>(OPT_hls_flow));
      hls_flowRef HlsFlow = hls_flow::factory(flow, Param, AppM, *f);
      // Executes the high-level synthesis on the specified module
      HlsFlow->exec();
      ///save the resulting implementation
      implementations[*f] = HLS;

///vcd-debug
#if HAVE_EXPERIMENTAL && HAVE_VCD_BUILT
      if (Param->getOption<bool>("vcd-debug"))
      {
         const target_deviceRef device = HLS_T->get_target_device();
         HDL_managerRef HDL = HDL_managerRef(new HDL_manager(device, Param));
         HDL->hdl_gen(function_name, SM->get_circ());
         PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "Starting vcd-debug\n");
         vcd_utilityRef vcd_util = vcd_utilityRef(new vcd_utility(AppM, debug_level));

         const unsigned int function_id = AppM->CGetCallGraphManager()->GetRootFunction();
         const FunctionBehaviorConstRef FB = AppM->CGetFunctionBehavior(function_id);
         vcd_util->compute_graph(FB, HLS);
         std::vector<std::string> fl = vcd_util->map_reg_to_signal();
         std::string vcd_output_directory = Param->getOption<std::string>(OPT_output_directory) + "/simulation/";
         vcd_util->parse_vcd_file(vcd_output_directory + "test.vcd", fl);

         const DesignFlowManagerRef design_flow_manager(new DesignFlowManager(Param));
         const DesignFlowStepFactoryConstRef c_backend_step_factory(new CBackendStepFactory(design_flow_manager, AppM, Param));
         const DesignFlowStepRef c_backend = GetPointer<const CBackendStepFactory>(c_backend_step_factory)->CreateCBackendStep(CBackend::CB_VCD, function_name + "_vcd_debug.c", CBackendInformationConstRef());
         c_backend->Exec();
         PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "Terminating vcd-debug\n");
      }
#endif
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "");
   }
   STOP_TIME(AppM->HLS_execution_time);

   /// export the generated core
   if (Param->getOption<int>(OPT_export_core))
   {
      long exp_time;
      START_TIME(exp_time);
      export_core::export_t exp_type = static_cast<export_core::export_t>(Param->getOption<unsigned int>(OPT_export_core_mode));
      export_coreRef expRef = export_core::factory(exp_type, Param, AppM, top_function);
      expRef->exec();
      STOP_TIME(exp_time);
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Core export (" + expRef->get_kind_text() + ") completed in " + print_cpu_time(exp_time) + " seconds\n");
   }

   THROW_ASSERT(implementations.find(top_function) != implementations.end(), "Implementation of top function does not exist at the end of the analysis");
   hlsRef HLS = implementations[top_function];
   if (output_level >= OUTPUT_LEVEL_VERBOSE)
   {
      HLS->print_resources(std::cerr);
      std::string out_file_name = "hls_summary";
      unsigned int progressive = 0;
      std::string candidate_out_file_name;
      do
      {
         candidate_out_file_name = out_file_name + "_" + boost::lexical_cast<std::string>(progressive++) + ".xml";
      } while (boost::filesystem::exists(candidate_out_file_name));
      out_file_name = candidate_out_file_name;
      AppM->xwrite(out_file_name);
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
   }

   /// check if codesign simulation has to be performed
   if(Param->isOption(OPT_generate_testbench) &&  Param->getOption<bool>(OPT_generate_testbench))
   {
      /// generate the C wrapper, the stimuli and collect the outputs
      HLStestbenchRef hls_tb(HLStestbench::Create(Param, AppM));
      hls_tb->generate_C_testbench();

      double clock_period_value = HLS->HLS_C->get_clock_period();
      /// create the HDL testbench
      std::string bench_name = "testbench_" + HLS->top->get_circ()->get_id();
      HLS->filename_bench = hls_tb->generate(bench_name, HLS->top->get_circ(), clock_period_value);
      if (HLS->filename_bench.size() == 0) THROW_ERROR("Testbench cannot be created!");
   }
   /// generate backend scripts
   const BackendFlowRef BEflow = AppM->get_backend_flow();
   const BehavioralHelperConstRef behavioral_helper = HLS->FB->CGetBehavioralHelper();
   BEflow->generateBackendScripts(behavioral_helper->get_function_name(), HLS->top, HLS->filename_bench);

   /// evaluate some metrics
   if (Param->getOption<int>(OPT_evaluation))
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "\nHigh-level synthesis statistics:");
      objective_evaluator::evaluation_mode mode = static_cast<objective_evaluator::evaluation_mode>(Param->getOption<unsigned int>(OPT_evaluation_method));
      evaluationRef estimate = evaluationRef(new evaluation(Param, AppM, top_function, mode));
      estimate->exec();
   }
}
