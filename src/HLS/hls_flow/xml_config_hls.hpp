/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file xml_config_hls.hpp
 * @brief Class performing an HLS flow based on an XML configuration
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef _XML_CONFIG_HLS_HPP_
#define _XML_CONFIG_HLS_HPP_

#include "hls_synthesis_flow.hpp"
REF_FORWARD_DECL(HLS_step);
REF_FORWARD_DECL(hls);
class xml_element;

#include <map>
#include <vector>
#include <list>

class xml_config_hls : public HLS_synthesis_flow
{
      ///map between the function_id and the corresponding HLS datastructure
      std::map<unsigned int, const xml_element*> functionConfig;

      ///list of functions to be analyzed
      std::list<unsigned int> functions;

      ///vector of the HLS steps to be performed in the correct order
      std::vector<HLS_stepRef> steps;

      /**
       * Parses the configuration from the specified node
       */
      void xload(const xml_element* node, unsigned int funId);

      /**
       * Verifies if the current node has to be added to the list of steps
       */
      bool checkNode(const xml_element* node, unsigned int funId, const std::string& ref_step);

   public:

      /**
       * Constructor.
       */
      xml_config_hls(const ParameterConstRef Param, const HLS_managerRef AppM);

      /**
       * Destructor
       */
      virtual ~xml_config_hls();

      /**
       * Performs the HLS synthesis
       */
      virtual void performSynthesis();

};
///refcount definition of the class
typedef refcount<xml_config_hls> xml_config_hlsRef;

#endif
