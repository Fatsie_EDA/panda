/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file ASLAP.cpp
 * @brief Class implementation for ASLAP class methods.
 *
 * This file implements some of the ASLAP member functions.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#include "ASLAP.hpp"
#include "utility.hpp"
#include "graph.hpp"
#include "function_behavior.hpp"
#include "basic_block.hpp"
#include "hls.hpp"
#include "hls_constraints.hpp"
#include "allocation.hpp"
#include "technology_node.hpp"
#include "exceptions.hpp"
#include "op_graph.hpp"

#include "schedule.hpp"
#include "fu_binding.hpp"
#include "Vertex.hpp"

///. include
#include "Parameter.hpp"

///utility include
#include "utility.hpp"

#include <boost/graph/reverse_graph.hpp>
#include <cmath>

#define epsilon 0.0000001
//in case asap and alap are computed with/without constraints on the resources available
#define WITH_CONSTRAINT 0

ASLAP::ASLAP(const hlsRef HLS, const bool _speculation, const OpVertexSet & operations, const ParameterConstRef _parameters) :
   ASAP(scheduleRef(new schedule(HLS->FB->CGetOpGraph(FunctionBehavior::FLSAODG), _parameters))),
   ALAP(scheduleRef(new schedule(HLS->FB->CGetOpGraph(FunctionBehavior::FLSAODG), _parameters))),
   min_tot_csteps(0),
   max_tot_csteps(0),
   has_branching_blocks(false),
   speculation(_speculation),
   debug_level(_parameters->get_class_debug_level(GET_CLASS(*this)))
{
   if(speculation)
      beh_graph = HLS->FB->CGetOpGraph(FunctionBehavior::SG, operations);
   else
      beh_graph = HLS->FB->CGetOpGraph(FunctionBehavior::FLSAODG, operations);
   VertexIterator it, end_it;
   for(boost::tie(it, end_it) = boost::vertices(*beh_graph); !has_branching_blocks && it != end_it; it++)
   {
      if( GET_TYPE(beh_graph, *it) & (TYPE_IF | TYPE_SWITCH | TYPE_WHILE | TYPE_FOR))
         has_branching_blocks = true;
   }
   const std::deque<vertex>& ls = HLS->FB->get_levels();
   for(std::deque<vertex>::const_iterator l = ls.begin(); l != ls.end(); l++)
   {
      if (operations.find(*l) != operations.end()) levels.push_back(*l);
   }
}

const OpGraphConstRef ASLAP::CGetOpGraph() const
{
   return beh_graph;
}

void ASLAP::print(std::ostream& os) const
{
   if (ASAP->num_scheduled())
   {
      os << "ASAP\n";
      ASAP->print(os, beh_graph);
      os << std::endl;
   }
   if (ALAP->num_scheduled())
   {
      os << "ALAP\n";
      ALAP->print(os, beh_graph);
      os << std::endl;
   }
}


/**
 * Terminate function used during improve_ASAP_with_constraints visiting and updating of ASAP_p vector
 */
struct p_update_check: public boost::dfs_visitor<>
{
private:

   /// vertex
   vertex s;
   /// string that identifies operation name
   const std::string & op_name;
   /// asap values
   vertex2int & ASAP_p;
   /// behavioral specification in terms of graph
   const OpGraphConstRef beh_graph;

public:

   /**
    * Constructor
    */
   p_update_check(vertex v, const std::string &name, vertex2int & A_p, const OpGraphConstRef g) :
      s(v),
      op_name(name),
      ASAP_p(A_p),
      beh_graph(g)
   {}

   /**
    * Template function used to discover vertex
    */
   template<class Vertex, class Graph>
   void discover_vertex(Vertex v, const Graph&) const
   {
      if (v != s && GET_OP(beh_graph, v) == op_name)
         ASAP_p[v]++;
   }
};


void ASLAP::add_constraints_to_ASAP(const HLS_constraintsRef &HLS_C, const allocationRef& ALL)
{
   vertex v;
   InEdgeIterator ei, ei_end;
   unsigned int cur_et, m_k;
   double clock_period = HLS_C->get_clock_period();

   /** ASAP_nip[i] contains the number of non immediate predecessor of node i with the same type of operation.*/
   vertex2int ASAP_nip;

   /** ASAP_p[i] contains the number of predecessor of node i with the same type of operation.*/
   vertex2int ASAP_p;

   ASAP_nip.resize(levels.begin(), levels.end(), 0);
   ASAP_p.resize(levels.begin(), levels.end(), 0);

   for (std::deque<vertex>::const_iterator i = levels.begin(); i != levels.end(); ++i)
   {
      if(!beh_graph->is_in_subset(*i)) continue;
      //Updating ASAP_p information
      p_update_check vis(*i, GET_OP(beh_graph, *i), ASAP_p, beh_graph);
      std::vector<boost::default_color_type> color_vec(boost::num_vertices(*beh_graph));
      boost::depth_first_visit(*beh_graph, *i, vis, boost::make_iterator_property_map(color_vec.begin(), boost::get(boost::vertex_index_t(), *beh_graph), boost::white_color));
      ASAP_nip[*i] = ASAP_p[*i];
      for (boost::tie(ei, ei_end) = boost::in_edges(*i, *beh_graph); ei != ei_end; ei++)
      {
         v = boost::source(*ei, *beh_graph);
         if (GET_OP(beh_graph, v) == GET_OP(beh_graph, *i))
            ASAP_nip[*i]--;
      }

   }

   VertexIterator vi, vi_end;

   for (boost::tie(vi, vi_end) = boost::vertices(*beh_graph); vi != vi_end; vi++)
   {
      v = *vi;
      if (ASAP_p[v] == 0) continue;
      m_k = ALL->max_number_of_resources(v);
      cur_et = ALL->op_et_to_cycles(ALL->get_attribute_of_fu_per_op(v, beh_graph, allocation::min, allocation::execution_time), clock_period) *
               MAX(static_cast<unsigned int>(ceil(static_cast<double>(ASAP_p[v]) / m_k)), (1 + static_cast<unsigned int>(ceil(static_cast<double>(ASAP_nip[v]) / m_k))));
      min_tot_csteps = min_tot_csteps < cur_et ? cur_et : min_tot_csteps;
      unsigned int schedule = ASAP->get_cstep(v) < cur_et ? cur_et : ASAP->get_cstep(v);
      ASAP->set_execution(v, schedule);
   }
   if (debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
   {
      PRINT_DBG_STRING(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "ASAP_p: ");
      vertex2int::const_iterator i_end = ASAP_p.end();
      for (vertex2int::const_iterator i = ASAP_p.begin(); i != i_end; ++i)
      {
         if(!beh_graph->is_in_subset(i->first)) continue;
         PRINT_DBG_STRING(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, GET_NAME(beh_graph, i->first) + " - " + boost::lexical_cast<std::string>(i->second));
      }
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "");

      PRINT_DBG_STRING(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "ASAP_nip: ");
      vertex2int::const_iterator ai_end = ASAP_nip.end();
      for (vertex2int::const_iterator ai = ASAP_nip.begin(); ai != ai_end; ++ai)
      {
         if(!beh_graph->is_in_subset(ai->first)) continue;
         PRINT_DBG_STRING(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, GET_NAME(beh_graph, ai->first) + " - " + boost::lexical_cast<std::string>(ai->second));
      }
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "");
   }
}

void ASLAP::compute_ASAP(const HLS_constraintsRef &HLS_C, const allocationRef& ALL, const scheduleRef partial_schedule)
{
   vertex vi;
   InEdgeIterator ei, ei_end;
   double clock_period = HLS_C->get_clock_period();
   //Store the current execution time
   double cur_start;
   double op_cycles;
   vertex2float finish_time;//


   ASAP->clear();
   finish_time.clear();//
   finish_time.resize(levels.begin(), levels.end(), 0);//
   min_tot_csteps = 0;
   
   if(partial_schedule)
   {
      for (std::deque<vertex>::const_iterator i = levels.begin(); i != levels.end(); ++i)
      {
         if(!beh_graph->is_in_subset(*i)) continue;
         if (partial_schedule && partial_schedule->is_scheduled(*i))
         {
            ASAP->set_execution(*i, partial_schedule->get_cstep(*i));
         }
      }
   }

   if (WITH_CONSTRAINT && !has_branching_blocks) //When no IF statements are present this function returns 1.
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "ASAP: add_constraints_to_ASAP");
      add_constraints_to_ASAP(HLS_C, ALL);
   }

   for (std::deque<vertex>::const_iterator i = levels.begin(); i != levels.end(); ++i)
   {
      if(!beh_graph->is_in_subset(*i)) continue;
      op_cycles = ALL->op_et_to_cycles(ALL->get_attribute_of_fu_per_op(*i, beh_graph, allocation::min, allocation::execution_time), clock_period);
      if(op_cycles > 0.0)
         op_cycles -= epsilon;
      cur_start = 0.0;
      
      for (boost::tie(ei, ei_end) = boost::in_edges(*i, *beh_graph); ei != ei_end; ei++)
      {
         vi = boost::source(*ei, *beh_graph);
         cur_start = finish_time[vi] < cur_start ? cur_start : finish_time[vi];
//       PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, GET_NAME(beh_graph, vi) + " -> " +GET_NAME(beh_graph, *i) + " cur_start " + boost::lexical_cast<std::string>(cur_start));
      }
      
      finish_time[*i] = cur_start + op_cycles;
      unsigned int curr_asap;
      if(ASAP->is_scheduled(*i))
         curr_asap = ASAP->get_cstep(*i);
      else
         curr_asap = 0;
      unsigned int finish_time_val = static_cast<unsigned int>(floor(finish_time[*i]));
      ASAP->set_execution(*i, finish_time_val > curr_asap ? finish_time_val : curr_asap);
//    PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, GET_NAME(beh_graph, *i) + " cur_start " + boost::lexical_cast<std::string>(cur_start) + " finish_time[*i] " + boost::lexical_cast<std::string>(finish_time[*i]) );
      min_tot_csteps = min_tot_csteps < ASAP->get_cstep(*i) ? ASAP->get_cstep(*i) : min_tot_csteps;
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, GET_NAME(beh_graph, *i) + " - " + boost::lexical_cast<std::string>(ASAP->get_cstep(*i)));
   }
   ASAP->set_csteps(min_tot_csteps + 1);

}

void ASLAP::add_constraints_to_ALAP(const HLS_constraintsRef &HLS_C, const allocationRef& ALL)
{
   //
   //ALAP is computed as ASAP: 0 is the last step and at this point of the implementation ALAP[*i] is the distance from last step
   vertex v;
   OutEdgeIterator ei, ei_end;
   unsigned int cur_et, m_k;
   double clock_period = HLS_C->get_clock_period();

   /** ALAP_nip[i] contains the number of non immediate predecessor of node i with the same type of operation.*/
   vertex2int ALAP_nip;
   /** ALAP_p[i] contains the number of predecessor of node i with the same type of operation.*/
   vertex2int ALAP_p;
   const boost::reverse_graph<graph> R(*beh_graph);

   ALAP_nip.resize(levels.begin(), levels.end(), 0);
   ALAP_p.resize(levels.begin(), levels.end(), 0);
   std::deque<vertex>::const_reverse_iterator iend = levels.rend();
   for (std::deque<vertex>::const_reverse_iterator i = levels.rbegin(); i != iend; ++i)
   {
      if(!beh_graph->is_in_subset(*i)) continue;
      p_update_check vis(*i, GET_OP(beh_graph, *i), ALAP_p, beh_graph);
      std::vector<boost::default_color_type> color_vec(boost::num_vertices(R));
      boost::depth_first_visit(R, *i, vis, boost::make_iterator_property_map(color_vec.begin(), boost::get(boost::vertex_index_t(), R), boost::white_color));
      ALAP_nip[*i] = ALAP_p[*i];
      for (boost::tie(ei, ei_end) = boost::out_edges(*i, *beh_graph); ei != ei_end; ei++)
      {
         v = boost::target(*ei, *beh_graph);
         if (GET_OP(beh_graph, v) == GET_OP(beh_graph, *i))
            ALAP_nip[*i]--;
      }
   }
   VertexIterator vi, vi_end;
   for (boost::tie(vi, vi_end) = boost::vertices(*beh_graph); vi != vi_end; vi++)
   {
      v = *vi;
      if (ALAP_p(v) == 0) continue;
      m_k = ALL->min_number_of_resources(v);
      cur_et = ALL->op_et_to_cycles(ALL->get_attribute_of_fu_per_op(v, beh_graph, allocation::min, allocation::execution_time) , clock_period) *
               MAX(static_cast<unsigned int>(ceil(static_cast<double>(ALAP_p[v]) / m_k)), (1 + static_cast<unsigned int>(ceil(static_cast<double>(ALAP_nip[v]) / m_k))));
      max_tot_csteps = max_tot_csteps < cur_et ? cur_et : max_tot_csteps;

      unsigned int schedule = ALAP->get_cstep(v) < cur_et ? cur_et : ALAP->get_cstep(v);
      ALAP->set_execution(v, schedule);
   }
   if (debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
   {
      PRINT_DBG_STRING(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "ALAP_p: ");
      vertex2int::const_iterator i_end = ALAP_p.end();
      for (vertex2int::const_iterator i = ALAP_p.begin(); i != i_end; ++i)
      {
         if(!beh_graph->is_in_subset(i->first)) continue;
         PRINT_DBG_STRING(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, GET_NAME(beh_graph, i->first) + " - " + boost::lexical_cast<std::string>(i->second));
      }
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "");

      PRINT_DBG_STRING(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "ASAP_nip: ");
      vertex2int::const_iterator ai_end = ALAP_nip.end();
      for (vertex2int::const_iterator ai = ALAP_nip.begin(); ai != ai_end; ++ai)
      {
         if(!beh_graph->is_in_subset(ai->first)) continue;
         PRINT_DBG_STRING(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, GET_NAME(beh_graph, ai->first) + " - " + boost::lexical_cast<std::string>(ai->second));
      }
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "");

      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Reverse ALAP:");
      for (std::deque<vertex>::const_iterator i = levels.begin(); i != levels.end(); ++i)
      {
         if(!beh_graph->is_in_subset(*i)) continue;
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, GET_NAME(beh_graph, *i) + " - " + boost::lexical_cast<std::string>(ALAP->get_cstep(*i)));
      }
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "end");
   }
}

void ASLAP::compute_ALAP(const hlsRef& HLS, ALAP_method met, const scheduleRef partial_schedule, bool *feasible, unsigned int est_upper_bound)
{
   const HLS_constraintsRef &HLS_C = HLS->HLS_C;
   const allocationRef& ALL = HLS->ALL;
   switch (met)
   {
      case ALAP_fast:
         max_tot_csteps = 0;
         THROW_ASSERT(!partial_schedule, "ASLAP::compute_ALAP - partial_schedule not expected");
         update_ALAP(0, HLS_C, ALL, feasible);
         break;
      case ALAP_worst_case:
         //THROW_ASSERT(!part_sch, "ASLAP::compute_ALAP - !part_sch failed into ALAP_worst_case");
         ALAP->clear();
         max_tot_csteps = 0;
         compute_ALAP_worst_case(HLS_C, ALL);
         break;
      case ALAP_with_upper:
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Computing alap starting from list based");
         THROW_ASSERT(partial_schedule, "ASLAP::compute_ALAP - partial_schedule expected");
         max_tot_csteps = partial_schedule->get_csteps() - 1;
         update_ALAP(max_tot_csteps, HLS_C, ALL);
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Computed alap");
         break;
      }
      case ALAP_with_upper_minus_one:
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Computing alap starting from list based minus one");
         THROW_ASSERT(partial_schedule, "ASLAP::compute_ALAP - partial_schedule expected");
         if(partial_schedule->get_csteps() < 2)
            *feasible=false;
         else
         {
            max_tot_csteps = partial_schedule->get_csteps() - 2;
            update_ALAP(max_tot_csteps, HLS_C, ALL, feasible);
         }
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Computed alap");
         break;
      }
      case ALAP_with_partial_scheduling:
      {
         THROW_ASSERT(partial_schedule, "ASLAP::compute_ALAP - partial_schedule expected");
         update_ALAP(est_upper_bound-1, HLS_C, ALL, feasible, partial_schedule);
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Computed alap");
         break;
      }
      default:
         THROW_ERROR("InconsistentDataStructure");
         break;
   }
   ALAP->set_csteps(max_tot_csteps + 1);
}

void ASLAP::update_ALAP(unsigned int maxc, const HLS_constraintsRef &HLS_C, const allocationRef& ALL, bool *feasible, const scheduleRef partial_schedule)
{
   ALAP->clear();
   max_tot_csteps = maxc;
   if(partial_schedule)
   {
      for (std::deque<vertex>::const_iterator i = levels.begin(); i != levels.end(); ++i)
      {
         if(!beh_graph->is_in_subset(*i)) continue;
         if (partial_schedule && partial_schedule->is_scheduled(*i))
         {
            ALAP->set_execution(*i, max_tot_csteps - partial_schedule->get_cstep(*i));
         }
      }
   }
   if (WITH_CONSTRAINT && !has_branching_blocks) //When no IF statements are present this function returns 1.
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "ALAP: add_constraints_to_ALAP");
      add_constraints_to_ALAP(HLS_C, ALL);
   }
   compute_ALAP_fast(HLS_C, ALL, feasible);
   if (feasible && *feasible && max_tot_csteps > maxc)
      *feasible = false;
}

void ASLAP::compute_ALAP_fast(const HLS_constraintsRef &HLS_C, const allocationRef& ALL, bool *feasible)
{
   //This function is used both in fast case and
   vertex vi;
   OutEdgeIterator ei, ei_end;
   double cur_rev_start;
   double op_cycles;
   double clock_period = HLS_C->get_clock_period();
   vertex2float Rev_finish_time;//
   Rev_finish_time.clear();//
   Rev_finish_time.resize(levels.begin(), levels.end(), 0);//

   std::deque<vertex>::const_reverse_iterator i_end = levels.rend();
   for (std::deque<vertex>::const_reverse_iterator i = levels.rbegin(); i != i_end; ++i)
   {
      if(!beh_graph->is_in_subset(*i)) continue;
      op_cycles = ALL->op_et_to_cycles(ALL->get_attribute_of_fu_per_op(*i, beh_graph, allocation::min, allocation::execution_time), clock_period);
      if(op_cycles > 0.0)
         op_cycles -= epsilon;
      cur_rev_start = 0.0;
      for (boost::tie(ei, ei_end) = boost::out_edges(*i, *beh_graph); ei != ei_end; ei++)
      {
         vi = boost::target(*ei, *beh_graph);
         cur_rev_start = Rev_finish_time[vi] < cur_rev_start ? cur_rev_start : Rev_finish_time[vi];
      }
      Rev_finish_time[*i] = cur_rev_start + op_cycles;
      unsigned int rev_curr_alap;
      if(ALAP->is_scheduled(*i))
         rev_curr_alap = ALAP->get_cstep(*i);
      else
         rev_curr_alap = 0;
      unsigned int rev_finish_time = static_cast<unsigned int>(floor(Rev_finish_time[*i]));
      ALAP->set_execution(*i, rev_finish_time > rev_curr_alap ? rev_finish_time : rev_curr_alap);
      max_tot_csteps = max_tot_csteps < ALAP->get_cstep(*i) ? ALAP->get_cstep(*i) : max_tot_csteps;
   }

   PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "ALAP fast");
   for (std::deque<vertex>::const_iterator i = levels.begin(); i != levels.end(); ++i)
   {
      if(!beh_graph->is_in_subset(*i)) continue;
      ALAP->set_execution(*i, max_tot_csteps - ALAP->get_cstep(*i));
      if (feasible && *feasible)
      {
         *feasible = ALAP->get_cstep(*i) >= ASAP->get_cstep(*i);
      }
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, GET_NAME(beh_graph, *i) + " - " + boost::lexical_cast<std::string>(ALAP->get_cstep(*i)));
   }
}

void ASLAP::compute_ALAP_worst_case(const HLS_constraintsRef &HLS_C, const allocationRef& ALL)
{

   vertex vi;
   OutEdgeIterator ei, ei_end;
   //Store the max reverse level
   unsigned int max_rev_level = 0;
   //Store the current reverse level
   unsigned int cur_rev_level;
   std::map<unsigned int, unsigned int> rev_levels_to_cycles;
   std::map<unsigned int, unsigned int> max_et;
   double clock_period = HLS_C->get_clock_period();

   std::deque<vertex>::const_reverse_iterator i_end = levels.rend();
   for (std::deque<vertex>::const_reverse_iterator i = levels.rbegin(); i != i_end; ++i)
   {
      if(!beh_graph->is_in_subset(*i)) continue;
      for (boost::tie(ei, ei_end) = boost::out_edges(*i, *beh_graph); ei != ei_end; ei++)
      {
         vi = boost::target(*ei, *beh_graph);
         cur_rev_level = ALAP->get_cstep(vi) + 1;
         max_rev_level = MAX(max_rev_level, cur_rev_level);
         unsigned int schedule = ALAP->get_cstep(*i) < cur_rev_level ? cur_rev_level : ALAP->get_cstep(*i);
         ALAP->set_execution(*i, schedule);
      }
      if (rev_levels_to_cycles.find(ALAP->get_cstep(*i)) == rev_levels_to_cycles.end())
      {
         rev_levels_to_cycles[ALAP->get_cstep(*i)] = 0;
         max_et[ALAP->get_cstep(*i)] = 0;
      }
      rev_levels_to_cycles[ALAP->get_cstep(*i)] += static_cast<unsigned int>(ALL->get_attribute_of_fu_per_op(*i, beh_graph, allocation::max, allocation::initiation_time));
      max_et[ALAP->get_cstep(*i)] = MAX(max_et[ALAP->get_cstep(*i)], ALL->op_et_to_cycles(ALL->get_attribute_of_fu_per_op(*i, beh_graph, allocation::max, allocation::execution_time), clock_period));

   }
   for (int i = static_cast<int>(max_rev_level) - 1; i >= 0; i--)
      rev_levels_to_cycles[static_cast<unsigned int>(i)] += MAX(rev_levels_to_cycles[static_cast<unsigned int>(i+1)], max_et[static_cast<unsigned int>(i+1)]);
   for (std::deque<vertex>::const_iterator i = levels.begin(); i != levels.end(); ++i)
   {
      if(!beh_graph->is_in_subset(*i)) continue;
      ALAP->set_execution(*i,
                          rev_levels_to_cycles[ALAP->get_cstep(*i)] - static_cast<unsigned int>(ALL->get_attribute_of_fu_per_op(*i, beh_graph, allocation::max, allocation::initiation_time)));
      max_tot_csteps = MAX(max_tot_csteps, ALAP->get_cstep(*i));
   }
}
