/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file scheduling.hpp
 * @brief Base class for all scheduling algorithms.
 *
 * This class is a pure virtual one, that has to be specilized in order to implement a particular scheduling algorithm.
 * It has a internal attribute to check if a vertex is speculate. To specialize this class into an algorithm
 *
 * @author Matteo Barbati <mbarbati@gmail.com>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef SCHEDULING_HPP
#define SCHEDULING_HPP

///Autoheader include
#include "config_HAVE_ILP_BUILT.hpp"

#include "hls_step.hpp"
CONSTREF_FORWARD_DECL(OpGraph);
REF_FORWARD_DECL(scheduling);

#include "graph.hpp"

#include <unordered_map>

/**
 * Generic class managing scheduling algorithms.
 */
class scheduling : public HLS_step
{
   private:

      /**
       * @name switch statements data structures and functions.
       */
      //@{
      ///store for each switch the number of outgoing branches.
      std::unordered_map<vertex, unsigned int > switch_map_size;

      ///for each controlling vertex, it defines a relation between switch tags and branch tags
      std::unordered_map<vertex, std::unordered_map< unsigned int, unsigned int> > switch_normalizing_map;
      //@}

   protected:

      ///Map for speculation property of each operation vertex. If true, it means that vertex is speculative executed,
      ///false otherwise
      std::unordered_map<vertex, bool> spec;

      ///flag to check speculation
      const bool speculation;

      /**
       * compute the branch tag given the edge and the control data flow graph.
       * @param e is the edge.
       * @param cdg is the controlling dependency graph.
       * @param switch_set is filled with the switch tags in case the source of e is a switch vertex.
       */
      unsigned int compute_b_tag(const EdgeDescriptor& e, const OpGraphConstRef cdg, std::set<unsigned int>::const_iterator &switch_it, std::set<unsigned int>::const_iterator &switch_it_end) const;

      /**
       * Return the number of branches associated with the controlling vertex.
       * @param cdg is the control dependency graph.
       * @param controlling_vertex is the controlling vertex.
       */
      unsigned int compute_b_tag_size(const OpGraphConstRef cdg, vertex controlling_vertex) const;

      /**
       * return an unsigned int from 0 to compute_b_tag_size(controlling_vertex)-1 given a branch tag not normalized.
       * @param controlling_vertex is the controlling vertex (used only for switch vertex).
       * @param b_tag_not_normalized is the non normalized tag.
       */
      unsigned int b_tag_normalize(vertex controlling_vertex, unsigned int b_tag_not_normalized) const;

      /**
       * initialize the maps associated with the switch vertices.
       * @param controlling_vertex is a switch vertex.
       * @param cdg is the control dependency graph.
       */
      void init_switch_maps(vertex controlling_vertex, const OpGraphConstRef cdg);

      /**
       * Move up zero time operations in the previous step when possible
       * @param HLS is the structure storing all the High level synthesis information
       * @param dependence_graph is the graph to be scheduled
       * @return the new last step in which an operation is scheduled
       */
      unsigned int anticipate_operations(const OpGraphConstRef dependence_graph);

   public:

      /// Scheduling algorithms implemented
      enum scheduling_type
      {
         LIST_BASED = 0,
#if HAVE_ILP_BUILT
         ILP,
         ILP_NEW_FORM,
         SILP,
#if 0
         ILP_LOOP,
#endif
#endif
         FIXED
      };

      /**
       * Constructor
       * @param speculation is the flag to check speculation
       */
      scheduling(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId, bool speculation);

      /**
       * Destructor.
       */
      virtual ~scheduling();

      /**
       * It returns speculation property map
       * @return the map associated
       */
      const std::unordered_map<vertex, bool>& get_spec() const { return spec; }

      /**
       * Creates solver based on selected option
       */
      static
      schedulingRef factory(scheduling_type type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Creates solver based on configuration stored in the XML node
       */
      static
      schedulingRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

};
///refcount definition of the class
typedef refcount<scheduling> schedulingRef;

#endif
