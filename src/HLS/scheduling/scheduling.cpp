/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file scheduling.cpp
 * @brief Base class for all scheduling algorithms.
 *
 *
 * @author Matteo Barbati <mbarbati@gmail.com>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "scheduling.hpp"

#include "hls.hpp"
#include "hls_manager.hpp"

#include "Parameter.hpp"

#include "schedule.hpp"
#include "fu_binding.hpp"
#include "refcount.hpp"
#include "hls_constraints.hpp"
#include "allocation.hpp"

///implemented algorithms
#include "parametric_list_based.hpp"
#if HAVE_ILP_BUILT
#include "ilp_loop_pipelining.hpp"
#include "meilp_solver.hpp"
#include "ilp_scheduling.hpp"
#include "silp_scheduling.hpp"
#include "ilp_scheduling_new.hpp"
#endif
#include "fixed_scheduling.hpp"

#include "xml_helper.hpp"
#include "polixml.hpp"

#include "utility.hpp"
#include "dbgPrintHelper.hpp"
#include "op_graph.hpp"

scheduling::scheduling(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId, bool _speculation) :
   HLS_step(_Param, _HLSMgr, _funId),
   speculation(_speculation)
{
   HLS->Rsch = scheduleRef(new schedule(HLS->FB->CGetOpGraph(FunctionBehavior::FLSAODG),_Param));
   HLS->Rfu = fu_bindingRef(new fu_binding(HLS->ALL, HLSMgr->get_tree_manager()));
}

scheduling::~scheduling()
{

}

schedulingRef scheduling::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string algorithm = "LIST";
   if (CE_XVM(algorithm, node)) LOAD_XVM(algorithm, node);

   scheduling::scheduling_type scheduling_algorithm = LIST_BASED;
   if (algorithm == "LIST")
      scheduling_algorithm = LIST_BASED;
   else
      THROW_ERROR("Scheduling algorithm \"" + algorithm + "\" currently not supported");
   return factory(scheduling_algorithm, Param, HLSMgr, funId);
}

schedulingRef scheduling::factory(scheduling_type type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   bool speculation = Param->getOption<bool>(OPT_speculative);

   switch (type)
   {
      case LIST_BASED:
      {
         parametric_list_based::LB_method LB_method = static_cast<parametric_list_based::LB_method>(Param->getOption<int>(OPT_scheduling_priority));
         return schedulingRef(new parametric_list_based(Param, HLSMgr, funId, speculation, LB_method));
      }
#if HAVE_ILP_BUILT
#if 0
      case ILP_LOOP:
      {
         // ilp loop pipelining is set to resolve scheduling problem
         meilp_solver::supported_solvers ilp_solver = static_cast<meilp_solver::supported_solvers>(Param->getOption<int>(OPT_ilp_solver));
         // choose the right solver according to options
         meilp_solverRef solver = meilp_solver::create_solver(ilp_solver);
         // ILP algorithm is set to resolve scheduling problem. Solver has been also specified
         solver->set_debug_level(Param->getOption<int>(OPT_debug_level));
         return schedulingRef(new ilp_loop_pipelining(10,/*result of unrolling degree!!!*/solver, HLS,
            Param->getOption<int>(OPT_debug_level)));
      }
#endif
      case ILP:
      {
         meilp_solver::supported_solvers ilp_solver = static_cast<meilp_solver::supported_solvers>(Param->getOption<int>(OPT_ilp_solver));
         // choose the right solver according to options
         meilp_solverRef solver = meilp_solver::create_solver(ilp_solver);
         // set max time (in seconds); it was specified as parameter
         if (Param->getOption<int>(OPT_ilp_max_time))
            solver->setMaximumSeconds(Param->getOption<int>(OPT_ilp_max_time));
         // set verbose level for solver
         solver->set_debug_level(Param->getOption<int>(OPT_debug_level));
         // ILP algorithm is set to resolve scheduling problem. Solver has been also specified
         return schedulingRef(new ilp_scheduling(Param, HLSMgr, funId, speculation, solver));
      }
      case ILP_NEW_FORM:
      {
         meilp_solver::supported_solvers ilp_solver = static_cast<meilp_solver::supported_solvers>(Param->getOption<int>(OPT_ilp_solver));
         // choose the right solver according to options
         meilp_solverRef solver = meilp_solver::create_solver(ilp_solver);
         // set max time (in seconds); it was specified as parameter
         if (Param->getOption<int>(OPT_ilp_max_time))
            solver->setMaximumSeconds(Param->getOption<int>(OPT_ilp_max_time));
         // set verbose level for solver
         solver->set_debug_level(Param->getOption<int>(OPT_debug_level));
         // ILP algorithm is set to resolve scheduling problem. Solver has been also specified
         return schedulingRef(new ilp_scheduling_new(Param, HLSMgr, funId, speculation, solver));
      }
#define WORKING_IN_PROGRESS 1
#if WORKING_IN_PROGRESS
      case SILP:
      {
         meilp_solver::supported_solvers silp_solver = static_cast<meilp_solver::supported_solvers>(Param->getOption<int>(OPT_ilp_solver));
         // choose the right solver in according to options
         meilp_solverRef solver = meilp_solver::create_solver(silp_solver);
         // set max time (in seconds); it was specified as parameter
         if (Param->getOption<int>(OPT_ilp_max_time))
            solver->setMaximumSeconds(Param->getOption<int>(OPT_ilp_max_time));
         // set verbose level for solver
         solver->set_debug_level(Param->getOption<int>(OPT_debug_level));
         // SILP algorithm is set to resolve scheduling problem. Solver has been also specified
         return schedulingRef(new silp_scheduling(Param, HLSMgr, funId,  speculation, solver));
      }
#endif
#endif
      case FIXED:
      {
         return schedulingRef(new fixed_scheduling(Param, HLSMgr, funId));
      }
      default:
         THROW_UNREACHABLE("Scheduling algorithm not supported " + boost::lexical_cast<std::string>(type));
   }
   return schedulingRef();
}

unsigned int scheduling::compute_b_tag_size(const OpGraphConstRef cdg, vertex controlling_vertex) const
{
   if(GET_TYPE (cdg.get(), controlling_vertex) & TYPE_ENTRY)
      return 1;
   else if (GET_TYPE (cdg.get(), controlling_vertex) & TYPE_IF)
      return 2;
   else if (GET_TYPE (cdg.get(), controlling_vertex) & TYPE_SWITCH)
   {
      THROW_ASSERT(switch_map_size.find(controlling_vertex) != switch_map_size.end(), "missing a controlling_vertex from the switch_map_size");
      return switch_map_size.find(controlling_vertex)->second;
   }
   else
      THROW_ERROR("Not yet supported conditional vertex");
   return 0;
}

unsigned int scheduling::compute_b_tag(const EdgeDescriptor & e, const OpGraphConstRef cdg, std::set<unsigned int>::const_iterator &switch_it, std::set<unsigned int>::const_iterator &switch_it_end) const
{
   vertex controlling_vertex = boost::source(e, *cdg);
   if(GET_TYPE(cdg.get(), controlling_vertex) & TYPE_ENTRY)
      return 0;
   else if (GET_TYPE(cdg.get(), controlling_vertex) & TYPE_IF)
   {
      if(Cget_edge_info<OpEdgeInfo>(e, *cdg) && CDG_TRUE_CHECK(cdg.get(), e))
         return 0;
      else
         return 1;
   }
   else if (GET_TYPE(cdg, controlling_vertex) & TYPE_SWITCH)
   {
      const std::set<unsigned int> &switch_set = EDGE_GET_NODEID(cdg.get(), e, CDG_SELECTOR);
      switch_it = switch_set.begin();
      switch_it_end = switch_set.end();
      return 2;
   }
   else
      THROW_ERROR("Not yet supported conditional vertex");
   return 0;
}

unsigned int scheduling::b_tag_normalize(vertex controlling_vertex, unsigned int b_tag_not_normalized) const
{
   std::unordered_map<vertex, std::unordered_map< unsigned int, unsigned int> >::const_iterator snp_it = switch_normalizing_map.find(controlling_vertex);
   THROW_ASSERT(snp_it != switch_normalizing_map.end(), "this controlling vertex is not of switch type");
   std::unordered_map< unsigned int, unsigned int>::const_iterator snp_el_it = snp_it->second.find(b_tag_not_normalized);
   THROW_ASSERT(snp_el_it != snp_it->second.end(), "switch_normalizing_map not correctly initialized");
   return snp_el_it->second;
}

void scheduling::init_switch_maps(vertex controlling_vertex, const OpGraphConstRef cdg)
{
   unsigned int curr_b_tag = 0;
   switch_normalizing_map.insert(std::pair<vertex, std::unordered_map< unsigned int, unsigned int> >(controlling_vertex, std::unordered_map< unsigned int, unsigned int>()));
   std::unordered_map<vertex, std::unordered_map< unsigned int, unsigned int> >::iterator snp_it = switch_normalizing_map.find(controlling_vertex);
   OutEdgeIterator eo, eo_end;
   for(boost::tie(eo, eo_end) = boost::out_edges(controlling_vertex, *cdg); eo != eo_end; eo++)
   {
      const std::set<unsigned int> &switch_set = EDGE_GET_NODEID(cdg, *eo, CDG_SELECTOR);
      const std::set<unsigned int>::const_iterator switch_it_end = switch_set.end();
      for(std::set<unsigned int>::const_iterator switch_it = switch_set.begin(); switch_it != switch_it_end; switch_it++)
         if(snp_it->second.find(*switch_it) == snp_it->second.end())
            snp_it->second[*switch_it] = curr_b_tag++;
   }
   switch_map_size[controlling_vertex] = curr_b_tag;

}

unsigned int scheduling::anticipate_operations(const OpGraphConstRef dependence_graph)
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "scheduling::anticipate_operations - Begin");
   ///The clock cycle
   const double clock_cycle = HLS->HLS_C->get_clock_period();

   ///Last control step
   unsigned int last_cs = 0;

   std::list<vertex> operations;
   boost::topological_sort(*dependence_graph, std::front_inserter(operations));
   std::list<vertex>::const_iterator v, v_end = operations.end();
   ///Checking vertex in topological order
   for(v = operations.begin(); v != v_end; v++)
   {
      ///Checking if operations is time zero
      double execution_time = HLS->ALL->get_execution_time(HLS->Rfu->get_assign(*v), *v, dependence_graph);
      if(execution_time == 0.0)
      {
         InEdgeIterator ei, ei_end;
         for(boost::tie(ei, ei_end) = boost::in_edges(*v, *dependence_graph); ei != ei_end; ei++)
         {
            vertex source = boost::source(*ei, *dependence_graph);
            unsigned int ending_time = HLS->Rsch->get_cstep(source) + HLS->ALL->op_et_to_cycles(HLS->ALL->get_execution_time(HLS->Rfu->get_assign(source), source, dependence_graph), clock_cycle);
            ///Operation can not be anticipated
            if(ending_time > HLS->Rsch->get_cstep(*v))
               break;

            if(ending_time == HLS->Rsch->get_cstep(*v) && (GET_TYPE(dependence_graph, source) & (TYPE_IF | TYPE_WHILE | TYPE_FOR | TYPE_SWITCH)))
               break;
         }
         ///Operation can be anticipated
         if(ei == ei_end && HLS->Rsch->get_cstep(*v) > 0)
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Anticipating " + GET_NAME(dependence_graph, *v));
            HLS->Rsch->set_execution(*v, HLS->Rsch->get_cstep(*v) - 1);
         }
         if(HLS->Rsch->get_cstep(*v) > last_cs)
            last_cs = HLS->Rsch->get_cstep(*v);
      }
   }
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "scheduling::anticipate_operations - End");
   return last_cs;
}
