/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file parametric_list_based.hpp
 * @brief Class definition of the list_based structure.
 *
 * This file defines the class performing the list_based scheduling.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef PARAMETRIC_LIST_BASED_HPP
#define PARAMETRIC_LIST_BASED_HPP

#include <iostream>
#include <set>
#include <map>
#include <vector>

#include "rehashed_heap.hpp"
#include "scheduling.hpp"
#include "Vertex.hpp"
#include "refcount.hpp"
#include <unordered_map>
/**
 * @name forward declarations
*/
//@{
REF_FORWARD_DECL(hls);
CONSTREF_FORWARD_DECL(OpGraph);
class schedule;
class graph;
class resource_ordering_functor;
class fu_binding;
class OpVertexSet;
//@}

/**
 * Class managing list based scheduling algorithms.
*/
class parametric_list_based : public scheduling
{
   public:

      enum LB_method
      {
         dynamic_mobility = 0,
         static_fixed,
         static_mobility
      };

      /**
       * This is the constructor of the list_based.
       * @param met is the method used to perform the list based scheduling.
       * @param speculation is the flag to check speculation
      */
      parametric_list_based(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId, bool speculation, LB_method met);

      /**
       * Destructor.
      */
      ~parametric_list_based();

      /**
       * Function that computes the List-Based scheduling of the graph.
       * @param HLS is the high level synthesis global data structure.
       */
      virtual void exec();

      /**
      * Function printing the name of the scheduling performed.
      */
      virtual std::string get_kind_text() const;

      /**
       * Function that computes the List-Based scheduling of the graph.
       */
      void exec(const OpVertexSet & operations, unsigned int current_cycle);

   private:

      /// method used to perform the list based scheduling
      LB_method lb_meth;

      ///The dependence graph
      OpGraphConstRef flow_graph;

      ///Entry vertex
      vertex entry_vertex;

      ///The starting time given the scheduling (used for chaining)
      vertex2obj<double> starting_time;

      ///The ending time given the scheduling (used for chaining)
      vertex2obj<double> ending_time;

      ///The clock cycle
      double clock_cycle;

      /**
       * Given the control step in which an operation is scheduled, compute the exact starting and ending time of an operation
       * @param v is the vertex of the operation
       * @param fu_type is the functional unit type
       * @param control_step is the control_step
       * @param starting_time is where starting_time will be stored
       * @param ending_time is where ending_time will be stored
       * @param stage_period is the minimum period of the pipelined unit fu_type
      */
      void compute_starting_ending_time_asap(const vertex v, const unsigned int fu_type, const unsigned int control_step, double & starting_time, double & ending_time, double &stage_period, bool &has_a_store_unbounded_as_in, fu_binding &res_binding, schedule &LB) const;
      void compute_starting_ending_time_alap(const vertex v, const unsigned int fu_type, unsigned int control_step, double & starting_time, double & ending_time, double &op_execution_time, double &stage_period, bool &has_a_store_unbounded_as_in, fu_binding &res_binding) const;

      /**
       * update starting and ending time by moving candidate_v as late as possible without increasing the whole latency
      */
      void update_starting_ending_time(vertex candidate_v, const double EPSILON, fu_binding &res_binding, const double setup_hold_time);

      /**
       * @brief update the starting and ending time of the vertices scheduled in the same cs of current_v
       * @param vertex_cstep is the control step of vertex current_v before its rescheduling
       * @param current_v is the current vertex
       * @param LB is the current scheduling
       * @param vertices_analyzed is the set of vertices updated
       * @param EPSILON is a small constant
       * @param res_binding is the current resource binding
       */
      void update_vertices_timing(unsigned int vertex_cstep, vertex current_v, schedule & LB, std::list<vertex> &vertices_analyzed, const double EPSILON, fu_binding & res_binding, const double setup_hold_time);

      /**
       * Update the resource map
       * @param used_resources_fu is the resource map.
       * @param current_vertex is the vertex for which an update is required.
       * @param current_starting_time is the starting time of the operation of current_vertex
       * @param fu_type is the functional unit type on which operation is scheduled
       * @return true if the assignment is feasible
      */
      bool BB_update_resources_use(unsigned int & used_resources, const vertex & current_vertex, const double & current_starting_time, const unsigned int fu_type) const;

      /**
       * Adds the vertex v to the priority queues
       * @param priority_trees are the priority_queues
       * @param ready_resources is the set of resources which have at least one ready operation
       * @param v is the vertex.
      */
      void add_to_priority_queues(std::vector<rehashed_heap<int> > & priority_queue, std::set<unsigned int, resource_ordering_functor> & ready_resources, const vertex v) const;

      /**
       * compute the slack of a given operation and in case specify if is pipelined
       */
      double compute_slack(vertex current_op, unsigned int current_vertex_cstep);

      /**
       * @brief update the slack of vertices of current_v and of all the vertices in chaining with it
       * @param current_v is the considered vertex
       * @param LB is the scheduling data structure
       * @param vertices_analyzed is the set of vertices having the slack updated
       */
      void update_vertices_slack(vertex current_v, schedule & LB, std::unordered_set<vertex> &vertices_analyzed);

      /**
       * @brief store_in_chaining_with_load checks if a store is chained with a load operation or vice versa
       * @param current_vertex_cstep control step of vertex v
       * @param v vertex considered
       * @return true in case vertex v is a store or a load operation and it is chained with a load or store operation
       */
      bool store_in_chaining_with_load_in(unsigned int current_vertex_cstep, vertex v);
      bool store_in_chaining_with_load_out(unsigned int current_vertex_cstep, vertex v);

      /**
       * perform balanced scheduling on an existing solution
       * @param sub_levels is the list of sorted vertices
       * @param LB is the current scheduling
       * @param res_binding is the current resource binding
       * @param EPSILON is a small constant
       * @param setup_hold_time is the delay for the setup and hold of registers
       */
      void do_balanced_scheduling(std::deque<vertex> &sub_levels, schedule &LB, fu_binding &res_binding, const double EPSILON, const double setup_hold_time);

      /**
       * different version of balanced scheduling. It re-schedules the operation trying to improve their slack.
       * @param sub_levels is the list of sorted vertices
       * @param LB is the current scheduling
       * @param res_binding is the current resource binding
       * @param EPSILON is a small constant
       * @param setup_hold_time is the delay for the setup and hold of registers
       */
      void do_balanced_scheduling1(std::deque<vertex> &sub_levels, schedule & LB, fu_binding & res_binding, const double EPSILON, const double setup_hold_time);

      bool check_non_direct_operation_chaining(vertex current_v, unsigned int cs, schedule & LB, fu_binding &res_binding) const;

};
#endif
