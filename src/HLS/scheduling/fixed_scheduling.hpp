/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file fixed_scheduling.hpp
 * @brief 
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$ $
 * Last modified by 
 *
 */
#ifndef FIXED_SCHEDULING_HPP
#define FIXED_SCHEDULING_HPP

#include "scheduling.hpp"

class fixed_scheduling : public scheduling
{
   public:

      /**
       * Constructor
       */
      fixed_scheduling(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Deconstructor
       */
      virtual ~fixed_scheduling();

      /**
       * Function that stores the scheduling of the graph.
       */
      void exec();

      /**
       * Print method
       */
      std::string get_kind_text() const;
};

#endif
