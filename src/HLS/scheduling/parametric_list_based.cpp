/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file parametric_list_based.cpp
 * @brief Class implementation of the parametric_list_based structure.
 *
 * This file implements some of the parametric_list_based member functions.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#include "parametric_list_based.hpp"
// #include "call_graph.hpp"
#include "function_behavior.hpp"
#include "exceptions.hpp"
#include "utility.hpp"

#include "schedule.hpp"
#include "fu_binding.hpp"
#include "hls.hpp"
#include "hls_constraints.hpp"
#include "allocation.hpp"
#include "ASLAP.hpp"
#include "technology_builtin.hpp"
#include "technology_node.hpp"

#include "tree_basic_block.hpp"
#include "basic_block.hpp"

#include "BambuParameter.hpp"
#include "cpu_time.hpp"
#include "global_enums.hpp"

/**
 * Functor used to compare two vertices with respect to an order based on the control steps associated with the vertices.
 */
struct cs_ordering_functor
{
   private:
      ///copy of the order: control step associated with the vertices.
      const vertex2obj<double> & order;

   public:
      /**
       * functor function used to compare two vertices with respect to the control step associated with the vertices.
       * @param a is the first vertex
       * @param bis the second vertex
       * @return true when order(a) < order(b)
       */
      bool operator() (const vertex & a, const vertex & b) const 
      {
         return order(a) < order(b) || (order(a) == order(b) && a < b);
      }

      /**
       * Constructor
       * @param o is the order.
       */
      cs_ordering_functor(const vertex2obj<double> & o) : order(o) {}

      /**
       * Destructor
       */
      ~cs_ordering_functor() {}

};

/**
 * Functor used to compare which of two resources has to be considered first in the scheduling
 */
struct resource_ordering_functor
{
   private:
      /// copy of the order: control step associated with the vertices.
      allocationRef & all;

   public:
      /**
       * functor function used to compare two resources with respect their performances
       * @param a is the first vertex
       * @param bis the second vertex
       * @return true when a is faster than b
       */
      bool operator() (const unsigned int & a, const unsigned int & b) const
      {
         double we_a = all->get_worst_execution_time(a);
         double we_b = all->get_worst_execution_time(b);
         double wa_a = all->get_area(a);
         double wa_b = all->get_area(b);
         return ((we_a < we_b) ||
                  (we_a == we_b && wa_a < wa_b) ||
                  (we_a == we_b && wa_a == wa_b && a < b));
      }

      /**
       * Constructor
       * @param o is the order.
       */
      resource_ordering_functor(allocationRef & _all) : all(_all) {}

      /**
       * Destructor
       */
      ~resource_ordering_functor() {}

};

/**
 * The key comparison function for edge-integer set based on levels
 */
class edge_integer_order_by_map : std::binary_function<vertex, vertex, bool>
{
   private:
      /// Topological sorted vertices
      const std::map<vertex, unsigned int> & ref;

      /// Graph
      const graph * g;

   public:
      /**
       * Constructor
       * @param ref is the map with the topological sort of vertices
       * @param g is a graph used only for debugging purpose to print name of vertex
       */
      edge_integer_order_by_map(const std::map<vertex, unsigned int> & _ref, const graph * _g) :
         ref(_ref),
         g(_g)
      {}

      /**
       * Compare position of two vertices in topological sorted
       * @param x is the first pair of vertex
       * @param y is the second pair of vertex
       * @return true if x precedes y in topological sort, false otherwise
       */
         bool operator()(const std::pair<std::pair<vertex, vertex>, unsigned int> & x, const std::pair<std::pair<vertex, vertex>, unsigned int> & y) const
      {
         THROW_ASSERT(ref.find(x.first.first) != ref.end(), "Vertex " + GET_NAME(g,x.first.first) + " is not in topological_sort");
         THROW_ASSERT(ref.find(y.first.first) != ref.end(), "Vertex " + GET_NAME(g,y.first.first) + " is not in topological_sort");
         THROW_ASSERT(ref.find(x.first.second) != ref.end(), "Vertex " + GET_NAME(g,x.first.second) + " is not in topological_sort");
         THROW_ASSERT(ref.find(y.first.second) != ref.end(), "Vertex " + GET_NAME(g,y.first.second) + " is not in topological_sort");
         if(ref.find(x.first.first)->second != ref.find(y.first.first)->second)
            return ref.find(x.first.first)->second < ref.find(y.first.first)->second;
         else if(ref.find(x.first.second)->second != ref.find(y.first.second)->second)
            return ref.find(x.first.second)->second < ref.find(y.first.second)->second;
         else
            return x.second < x.second;
      }
};

static
bool check_if_is_live_in_next_cycle(const std::set<vertex, cs_ordering_functor> &live_vertices, unsigned int current_cycle, const vertex2obj<double> &ending_time, double clock_cycle)
{
   std::set<vertex, cs_ordering_functor>::const_iterator live_vertex_it = live_vertices.begin();
   while(live_vertex_it != live_vertices.end())
   {
      if(ending_time(*live_vertex_it) > (current_cycle+1) * clock_cycle)
      {
         return true;
      }
      ++live_vertex_it;
   }
   return false;
}

parametric_list_based::parametric_list_based(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId, bool _speculation, LB_method met) :
   scheduling(_Param, _HLSMgr, _funId, _speculation),
   lb_meth(met)
{

}

parametric_list_based::~parametric_list_based()
{

}

void parametric_list_based::exec(const OpVertexSet & operations, unsigned int current_cycle)
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Executing parametric_list_based::exec...");

   /// The scheduling
   schedule & LB = *(HLS->Rsch);

   /// Current ASAP
   vertex2obj<unsigned int> current_ASAP;

   ///The binding
   fu_binding & res_binding = *(HLS->Rfu);

   /// Number of predecessors of a vertex already scheduled
   vertex2obj<double> scheduled_predecessors;

   /// The clock cycle
   clock_cycle = HLS->HLS_C->get_clock_period_resource_fraction()*HLS->HLS_C->get_clock_period();

   /// The setup+hold time delay
   double setup_hold_time = HLS->ALL->get_setup_hold_time();

   resource_ordering_functor r_functor(HLS->ALL);
   ///The set of resources for which the list of ready operations is not empty
   std::set<unsigned int, resource_ordering_functor> ready_resources(r_functor);

   ///Set of ready operations (needed for update priority)
   std::set<vertex> ready_vertices;

   ///vertices still live at the beginning of the current control step
   cs_ordering_functor et_order(ending_time);
   std::set<vertex, cs_ordering_functor> live_vertices(et_order);

   ///select the type of graph
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "   Selecting the type of the graph...");
   switch(lb_meth)
   {
      case static_mobility:
      case static_fixed:
      case dynamic_mobility:
         if(speculation)
            flow_graph = HLS->FB->CGetOpGraph(FunctionBehavior::SG, operations);
         else
            flow_graph = HLS->FB->CGetOpGraph(FunctionBehavior::FLSAODG, operations);
         break;
      default:
         THROW_ERROR("not supported list based scheduling method");
   }

   ///Number of operation to be scheduled
   size_t operations_number = operations.size();

   long int cpu_time;
   /// compute asap and alap
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "   Computing asap and alap...");
   START_TIME(cpu_time);
   ASLAPRef aslap;
   switch(lb_meth)
   {
      case static_mobility:
      case dynamic_mobility:
      {
         aslap = ASLAPRef(new ASLAP(HLS, speculation, operations, Param));
         if(HLS->Rsch->num_scheduled() == 0)
         {
            aslap->compute_ASAP(HLS->HLS_C, HLS->ALL);
            aslap->compute_ALAP(HLS, ASLAP::ALAP_fast);
         }
         else
         {
            aslap->compute_ASAP(HLS->HLS_C, HLS->ALL, HLS->Rsch);
            unsigned int est_upper_bound = static_cast<unsigned int>(operations_number);
            aslap->compute_ALAP(HLS, ASLAP::ALAP_with_partial_scheduling, HLS->Rsch, nullptr, est_upper_bound);
         }
         break;
      }
      case static_fixed:
         break;
      default:
         THROW_ERROR("not supported list based scheduling method");
   }
   STOP_TIME(cpu_time);

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "   Computing free input vertices...");
   ///compute the set of vertices without input edges.
   /// At least one vertex is expected
   for(OpVertexSet::const_iterator vi = operations.begin(); vi != operations.end(); vi++)
   {
      ///Skip vertex if it is not in the current subgraph
      if(!flow_graph->is_in_subset(*vi))
         continue;
      ///Updating structure for already scheduled operations
      if(LB.is_scheduled(*vi))
      {
         live_vertices.insert(*vi);

      }
      else if(boost::in_degree(*vi, *flow_graph) <= 0)
      {
         ready_vertices.insert(*vi);
         if(GET_TYPE(flow_graph, *vi) == TYPE_ENTRY)
            entry_vertex = *vi;
      }
      else
      {
         ///Check if all its predecessors have been scheduled. In this case the vertex is ready
         InEdgeIterator ei, ei_end;
         for(boost::tie(ei, ei_end) = boost::in_edges(*vi, *flow_graph); ei != ei_end; ei++)
         {
            vertex source = boost::source(*ei, *flow_graph);
            if(!LB.is_scheduled(source))
               break;
         }
         if(ei == ei_end)
         {
            ready_vertices.insert(*vi);
         }
      }
   }
   THROW_ASSERT(ready_vertices.size(), "At least one vertex is expected");
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "   Number of vertices: " + STR(ready_vertices.size()));

   ///Setting priority
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "   Setting priority...");
   refcount<priority_data<int> > Priority;
   switch(lb_meth)
   {
      case static_mobility:
         Priority = refcount<priority_data<int> >(new priority_static_mobility(aslap));
         break;
      case dynamic_mobility:
      {
         Priority = refcount<priority_data<int> >(new priority_dynamic_mobility(aslap, ready_vertices));
         break;
      }
      case static_fixed:
      {
         std::unordered_map<vertex, int> priority_value; 
         std::unordered_map<std::string, int> string_priority_value = HLS->HLS_C->get_scheduling_priority();
         VertexIterator ki, ki_end;
         for (boost::tie(ki, ki_end) = boost::vertices(*flow_graph); ki != ki_end; ki++)
         {
            priority_value[*ki] = string_priority_value[GET_NAME(flow_graph, *ki)];
         }
         Priority = refcount<priority_data<int> >(new priority_fixed(priority_value));
         break;
      }
      default:
         THROW_ERROR("not supported list based scheduling method");
   }

   ///priory queues set up
   unsigned int n_resources = HLS->ALL->get_number_fu_types();
   priority_compare_functor<int>  priority_functors(*Priority, flow_graph);
   std::vector<rehashed_heap<int> > priority_queues(n_resources, priority_functors);
   std::unordered_set<unsigned int> cstep_vuses_ARRAYs;
   std::unordered_set<unsigned int> cstep_vuses_others;

   std::set<vertex>::const_iterator rv, rv_end = ready_vertices.end();

   for(rv = ready_vertices.begin(); rv != rv_end; rv++)
      add_to_priority_queues(priority_queues, ready_resources, *rv);

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "   Starting scheduling...");
   unsigned int already_sch = LB.num_scheduled();
   bool unbounded = false;
   bool store_unbounded_check = false;
   while((LB.num_scheduled() - already_sch) != operations_number)
   {
      unbounded = false;
      store_unbounded_check = false;
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "      LB.num_scheduled() " + boost::lexical_cast<std::string>(LB.num_scheduled()));
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "      already_sch " + boost::lexical_cast<std::string>(already_sch));
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "      operations_number " + boost::lexical_cast<std::string>(operations_number));
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "      Scheduling in control step " + boost::lexical_cast<std::string>(current_cycle) + " (Time: " + boost::lexical_cast<std::string>(current_cycle * clock_cycle)+ ")");
      /// definition of the data structure used to check if a resource is available given a vertex
      ///in case a vertex is not included in a map this mean that the used resources are zero.
      ///First index is the functional unit type, second index is the controller node, third index is the condition
      std::unordered_map<unsigned int, unsigned int> used_resources;

      ///Operations which can be scheduled in this control step because precedence has satisfied, but can't be scheduled in this control step
      ///Index is the functional unit type
      std::unordered_map<unsigned int, std::unordered_set<vertex> > black_list;

      ///Adding information about operation still live
      std::set<vertex, cs_ordering_functor>::const_iterator live_vertex_it = live_vertices.begin();
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "         Considering live vertices...");
      while(live_vertex_it != live_vertices.end())
      {
         if(ending_time(*live_vertex_it) <= current_cycle * clock_cycle)
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "            Vertex " + GET_NAME(flow_graph, *live_vertex_it) + " dies");
            live_vertices.erase(*live_vertex_it);
            live_vertex_it = live_vertices.begin();
         }
         else
         {
            unsigned int II = HLS->ALL->get_initiation_time(res_binding.get_assign(*live_vertex_it), *live_vertex_it, flow_graph);

            if(II == 0 || current_cycle < (II + floor(starting_time[*live_vertex_it]/clock_cycle)))
            {
               bool schedulable;
               if(used_resources.find(res_binding.get_assign(*live_vertex_it)) == used_resources.end())
                  used_resources[res_binding.get_assign(*live_vertex_it)] = 0;
               schedulable = BB_update_resources_use(used_resources[res_binding.get_assign(*live_vertex_it)], *live_vertex_it, starting_time[*live_vertex_it], res_binding.get_assign(*live_vertex_it));
               if(!schedulable)
                  THROW_ERROR("Unfeasible scheduling");
            }
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "            Vertex " + GET_NAME(flow_graph, *live_vertex_it) + " lives until " + boost::lexical_cast<std::string>(ending_time(*live_vertex_it)));
            live_vertex_it++;
         }
      }

      for(unsigned int fu_type = 0; fu_type < n_resources; fu_type++)
      {
         if(priority_queues[fu_type].size())
            ready_resources.insert(fu_type);
      }


      std::unordered_map<unsigned int, std::unordered_set<vertex> > postponed_resources;
      bool do_again;

      do
      {
         do_again = false;
         while(ready_resources.size())
         {
            unsigned int fu_type;
            fu_type = *(ready_resources.begin());
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "         Considering functional unit type " + boost::lexical_cast<std::string>(fu_type) + "("+ HLS->ALL->get_fu_name(fu_type).first + "-" + HLS->ALL->get_fu_name(fu_type).second + ")" + " at clock cycle " + STR(current_cycle));
            ready_resources.erase(ready_resources.begin());

            rehashed_heap<int> & queue = priority_queues[fu_type];
            ///Ignore empty list
            while(queue.size())
            {
               vertex current_vertex = queue.top();
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "               First operation ready for this unit is " + GET_NAME(flow_graph, current_vertex));
               queue.pop();
               ///Ignore already scheduled operation
               if(LB.is_scheduled(current_vertex))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "               Already scheduled");
                  ///remove current_vertex from the queue
                  continue;
               }
               bool is_live = check_if_is_live_in_next_cycle(live_vertices, current_cycle, ending_time, clock_cycle);
               THROW_ASSERT(!(GET_TYPE(flow_graph,current_vertex) & (TYPE_WHILE | TYPE_FOR)), "not expected operation type");
               ///put these type of operations as last operation scheduled for the basic block
               if((GET_TYPE(flow_graph,current_vertex) & (TYPE_IF | TYPE_RET | TYPE_SWITCH | TYPE_MULTIIF | TYPE_GOTO)) &&
                  ((LB.num_scheduled() - already_sch) != operations_number-1))
               {
                  postponed_resources[fu_type].insert(current_vertex);
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "            Scheduling of Control Vertex " + GET_NAME(flow_graph, current_vertex) + " postponed ");
                  continue;
               }
               if((GET_TYPE(flow_graph,current_vertex) & (TYPE_IF | TYPE_RET | TYPE_SWITCH | TYPE_MULTIIF | TYPE_GOTO)) &&
                  (unbounded || is_live))
               {
                  black_list[fu_type].insert(current_vertex);
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "            Scheduling of Control Vertex " + GET_NAME(flow_graph, current_vertex) + " postponed to the next cycle");
                  continue;
               }

               /// true if operation is schedulable
               bool schedulable = false;

               /// starting time of the operation
               double current_starting_time;

               /// ending time of the operation
               double current_ending_time;

               /// stage period for pipelined units
               double current_stage_period;

               ///checking if predecessors have finished
               if(current_ASAP[current_vertex] > current_cycle)
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Depends on a live operation");
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }

               bool has_store_load_unbounded_as_in = false;

               /// checking if chaining is feasible
               compute_starting_ending_time_asap(current_vertex, fu_type, current_cycle, current_starting_time, current_ending_time, current_stage_period, has_store_load_unbounded_as_in, res_binding, LB);
               bool is_pipelined = HLS->ALL->get_initiation_time(fu_type, current_vertex, flow_graph) != 0;
               if(is_pipelined &&  current_starting_time > (current_cycle * clock_cycle) && ((current_stage_period+current_starting_time+setup_hold_time>((current_cycle + 1) * clock_cycle)) || unbounded))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Pipelined unit can not be chained: starting time is " + boost::lexical_cast<std::string>(current_starting_time) + " stage period is " + boost::lexical_cast<std::string>(current_stage_period)) ;
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }
               else if((current_starting_time >= ((current_cycle + 1) * clock_cycle)) || ((!is_pipelined && current_starting_time > (current_cycle * clock_cycle)) && current_ending_time+setup_hold_time > (current_cycle + 1) * clock_cycle))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Can not be chained: starting time is " + boost::lexical_cast<std::string>(current_starting_time) + " ending time is " + boost::lexical_cast<std::string>(current_ending_time)) ;
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }
               else if ((unbounded || current_starting_time > (current_cycle * clock_cycle)) && (GET_TYPE(flow_graph,current_vertex) & TYPE_RET))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Chaining of return expression operation is not allowed by construction");
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }
               else if((current_starting_time >= (current_cycle * clock_cycle)) && has_store_load_unbounded_as_in)
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Chaining with a store or a load or an unbounded in input is not possible -> starting time " + boost::lexical_cast<std::string>(current_starting_time) + " ending time: " + boost::lexical_cast<std::string>(current_ending_time));
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }
               /*else if((current_starting_time > (current_cycle * clock_cycle)) && GET_OP(flow_graph, current_vertex) == STORE && !HLS->ALL->is_direct_access_memory_unit(fu_type))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Chaining with a store is not possible -> starting time " + boost::lexical_cast<std::string>(current_starting_time) + " ending time: " + boost::lexical_cast<std::string>(current_ending_time));
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }
               else if((current_starting_time > (current_cycle * clock_cycle)) && GET_OP(flow_graph, current_vertex) == LOAD && !HLS->ALL->is_direct_access_memory_unit(fu_type))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Chaining with a load is not possible -> starting time " + boost::lexical_cast<std::string>(current_starting_time) + " ending time: " + boost::lexical_cast<std::string>(current_ending_time));
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }*/
               else if ((current_starting_time > (current_cycle * clock_cycle)) && !HLS->Param->getOption<bool>(OPT_chaining))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Chaining is possible but not allowed -> starting time " + boost::lexical_cast<std::string>(current_starting_time) + " ending time: " + boost::lexical_cast<std::string>(current_ending_time));
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }
               else if(current_starting_time == (current_cycle * clock_cycle) && current_ending_time+setup_hold_time > (current_cycle + 1) * clock_cycle && unbounded)
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Multi-cycles operations cannot be scheduled together with unbounded operations");
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }
               else if((GET_OP(flow_graph, current_vertex) == STORE || GET_OP(flow_graph, current_vertex) == LOAD) && !HLS->ALL->is_direct_access_memory_unit(fu_type) && unbounded)
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Non-direct memory access operations may conflict with unbounded operations");
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }
               /*else if(store_in_chaining_with_load_in(current_cycle, current_vertex))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Load and store cannot be chained");
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }*/


               /// check compatibility
               const CustomSet<unsigned int> & curr_vuses = flow_graph->CGetOpNodeInfo(current_vertex)->GetVirtualVariables_ANTIDEPS(AT_USE);
               bool postponed = false;
               if(GET_OP(flow_graph, current_vertex) == LOAD || GET_OP(flow_graph, current_vertex) == STORE)
               {
                  bool is_array = HLS->ALL->is_direct_access_memory_unit(fu_type);
                  CustomSet<unsigned int>::const_iterator cv_it_end = curr_vuses.end();
                  for(CustomSet<unsigned int>::const_iterator cv_it = curr_vuses.begin(); cv_it != cv_it_end && !postponed; ++cv_it)
                  {
                     if(is_array && !cstep_vuses_others.empty())
                        postponed = true;
                     else if(!is_array && !cstep_vuses_ARRAYs.empty())
                        postponed = true;
                  }
               }
               if(postponed)
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  MEMORY_CTRL cannot run together with BRAM direct accessess " + GET_NAME(flow_graph, current_vertex) + " mapped on " + HLS->ALL->get_fu_name(fu_type).first + "at cstep " + STR(current_cycle));
                  black_list[fu_type].insert(current_vertex);
                  continue;
               }


               /// check if there exist enough resources available
               schedulable = BB_update_resources_use(used_resources[fu_type], current_vertex, current_starting_time, fu_type);
               if (!HLS->ALL->is_operation_bounded(flow_graph, current_vertex, fu_type))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  " + GET_NAME(flow_graph, current_vertex) + " is unbounded");
                  if (unbounded || is_live || store_unbounded_check) schedulable = false;
                  else
                  {
                     double ex_time = HLS->ALL->get_execution_time(fu_type, current_vertex, flow_graph);
                     if(ex_time>clock_cycle)
                        THROW_WARNING("Operation execution time of the unbounded operation is greater than the clock period resource fraction (" + STR(clock_cycle)+ ").\n\tExecution time " + STR(ex_time) + " of " + GET_NAME(flow_graph, current_vertex) + "\nThis may prevent meeting the timing constraints.\n");
                     unbounded = true;
                  }
               }
               else
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  " + GET_NAME(flow_graph, current_vertex) + " is bounded");
               }

               if(schedulable)
               {
                  /// update cstep_vuses
                  if(GET_OP(flow_graph, current_vertex) == LOAD || GET_OP(flow_graph, current_vertex) == STORE)
                  {
                     if(HLS->ALL->is_direct_access_memory_unit(fu_type))
                        cstep_vuses_ARRAYs.insert(curr_vuses.begin(), curr_vuses.end());
                     else
                        cstep_vuses_others.insert(curr_vuses.begin(), curr_vuses.end());
                  }
                  if((GET_OP(flow_graph, current_vertex) == STORE || GET_OP(flow_graph, current_vertex) == LOAD) && !HLS->ALL->is_direct_access_memory_unit(fu_type))
                  {
                     store_unbounded_check = true; ///even if it is bounded we would like to prevent non-direct memory accesses running together with unbounded operations
                  }
                  /// set the schedule information
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  " + GET_NAME(flow_graph, current_vertex) + " scheduled at " + boost::lexical_cast<std::string>(current_cycle*clock_cycle));
                  LB.set_execution(current_vertex, current_cycle);
                  starting_time[current_vertex] = current_starting_time;
                  if(!HLS->ALL->is_operation_bounded(flow_graph, current_vertex, fu_type))
                  {
                     ending_time[current_vertex] = clock_cycle*ceil(current_ending_time/clock_cycle) + (current_starting_time == clock_cycle*ceil(current_ending_time/clock_cycle) ? clock_cycle : 0);
                     starting_time[current_vertex] = ending_time[current_vertex] - (current_ending_time -current_starting_time);
                  }
                  else
                     ending_time[current_vertex] = current_ending_time;
                  LB.set_execution_end(current_vertex, current_cycle+static_cast<unsigned int>(floor((ending_time[current_vertex]-starting_time[current_vertex])/clock_cycle)));
                  ready_vertices.erase(current_vertex);

                  ///set the binding information
                  if (HLS->HLS_C->has_binding_to_fu(GET_NAME(flow_graph, current_vertex)))
                     res_binding.bind(current_vertex, fu_type, 0);
                  else
                     res_binding.bind(current_vertex, fu_type);

                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Current cycles ends at " + boost::lexical_cast<std::string>((current_cycle + 1) * clock_cycle) + "  - operation ends at " + boost::lexical_cast<std::string>(ending_time[current_vertex]));
                  if(ending_time[current_vertex] > (current_cycle + 1) * clock_cycle)
                     live_vertices.insert(current_vertex);

                  ///Check if some successors have become ready
                  OutEdgeIterator eo, eo_end;

                  std::list<std::pair<std::string, vertex> > successors;
                  for(boost::tie(eo, eo_end) = boost::out_edges(current_vertex, *flow_graph); eo != eo_end; eo++)
                  {
                     vertex target = boost::target(*eo, *flow_graph);
                     successors.push_back(std::make_pair(GET_NAME(flow_graph, target), target));
                  }
                  //successors.sort();

                  for(std::list<std::pair<std::string, vertex> >::iterator s = successors.begin(); s != successors.end(); s++)
                  {
                     PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Considering successor " + s->first);
                     scheduled_predecessors[s->second]++;
                     current_ASAP[s->second] = MAX(static_cast<unsigned int>(floor(ending_time(current_vertex) / clock_cycle)), current_ASAP[s->second]);
                     ///check if to_v should be considered as ready
                     if(boost::in_degree(s->second, *flow_graph) == scheduled_predecessors(s->second))
                     {
                        PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                     Become ready");
                        ready_vertices.insert(s->second);
                        add_to_priority_queues(priority_queues, ready_resources, s->second);
                     }
                  }
                  continue;
               }
               else
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "               No free resource");
                  black_list[fu_type].insert(current_vertex);
               }
            }
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "         Finished with unit type " + boost::lexical_cast<std::string>(fu_type));
         }
         if(!postponed_resources.empty())
         {
            std::unordered_map<unsigned int, std::unordered_set<vertex> >::const_iterator bl_end = postponed_resources.end();
            for(std::unordered_map<unsigned int, std::unordered_set<vertex> >::const_iterator bl_it = postponed_resources.begin(); bl_it != bl_end; ++bl_it)
            {
               std::unordered_set<vertex>::const_iterator v_end = bl_it->second.end();
               for(std::unordered_set<vertex>::const_iterator v = bl_it->second.begin(); v != v_end; ++v)
               {
                  priority_queues[bl_it->first].push(*v);
               }
               ready_resources.insert(bl_it->first);
            }
            postponed_resources.clear();
            if(black_list.empty())
            {
               do_again = true;
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "         Restarted the schedulign loop to accomodate postponed vertices");
            }
         }
      } while(do_again);

      std::unordered_map<unsigned int, std::unordered_set<vertex> >::const_iterator bl_end = black_list.end();
      for(std::unordered_map<unsigned int, std::unordered_set<vertex> >::const_iterator bl_it = black_list.begin(); bl_it != bl_end; ++bl_it)
      {
         std::unordered_set<vertex>::const_iterator v_end = bl_it->second.end();
         for(std::unordered_set<vertex>::const_iterator v = bl_it->second.begin(); v != v_end; ++v)
         {
            priority_queues[bl_it->first].push(*v);
         }
      }
      /// update priorities and possibly rehash the queues
      if(Priority->update())
         for(unsigned int i = 0; i < n_resources; i++)
            priority_queues[i].rehash();
      /// clear the vuses
      cstep_vuses_ARRAYs.clear();
      cstep_vuses_others.clear();
      /// move to the next cycle
      current_cycle++;
   }

   const double EPSILON = 0.001;
   /// update starting and ending time of operations
   /// by running them as late as possible
   const std::deque<vertex>& levels = HLS->FB->get_levels();
   std::deque<vertex> sub_levels;
   for(std::deque<vertex>::const_reverse_iterator rit = levels.rbegin(); rit != levels.rend(); ++rit)
   {
      vertex candidate_v = *rit;
      if(operations.find(candidate_v) == operations.end()) continue;
      sub_levels.push_back(candidate_v);
      update_starting_ending_time(candidate_v, EPSILON, res_binding, setup_hold_time);
   }
   /// compute the vertices slack
   std::unordered_set<vertex> vertices_analyzed;
   for(std::deque<vertex>::const_iterator vi = sub_levels.begin(); vi != sub_levels.end(); ++vi)
   {
      if(vertices_analyzed.find(*vi) != vertices_analyzed.end())
      {
         //std::cerr << "slack for " << GET_NAME(flow_graph, *vi) << "=" << LB.get_slack(*vi) << std::endl;
         continue;
      }
      update_vertices_slack(*vi, LB, vertices_analyzed);
      //std::cerr << "updating slack for " << GET_NAME(flow_graph, *vi) << "=" << LB.get_slack(*vi) << std::endl;
   }

#if 0
   do_balanced_scheduling1(sub_levels, LB, res_binding, EPSILON, setup_hold_time);
#endif

   unsigned int steps = current_cycle;
   LB.set_csteps(steps);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "   List Based finished");
}

std::string parametric_list_based::get_kind_text() const
{
   std::string name;
   switch(lb_meth)
   {
      case static_mobility:
         name = "Static ";
         break;
      case static_fixed:
         name = "Priority-Fixed ";
         break;
      case dynamic_mobility:
         name = "Dynamic mobility based ";
         break;
      default:
         THROW_ERROR("not supported list based scheduling method");
   }
   return name + "List-Based Scheduling";
}

void parametric_list_based::compute_starting_ending_time_asap(vertex v, unsigned int fu_type, unsigned int cs, double & current_starting_time, double & current_ending_time, double &stage_period, bool &has_store_load_unbounded_as_in, fu_binding &res_binding, schedule &LB) const
{
   current_ending_time = cs * clock_cycle;
   double op_execution_time = HLS->ALL->get_execution_time(fu_type, v, flow_graph);
   has_store_load_unbounded_as_in = (GET_OP(flow_graph, v) == LOAD || GET_OP(flow_graph, v) == STORE) && check_non_direct_operation_chaining(v,  cs, LB, res_binding);
#ifdef COMPRESS_CHAINED_OPS
   double max_chained_prev = 0.0;
   bool is_comprimibile = HLS->ALL->compute_normalized_area(fu_type, boost::in_degree(v, *flow_graph)) < 1.0 && op_execution_time < clock_cycle;
#endif
   InEdgeIterator ei, ei_end;
   for(boost::tie(ei, ei_end) = boost::in_edges(v, *flow_graph); ei != ei_end; ei++)
   {
      vertex from_vertex = boost::source(*ei, *flow_graph);
      unsigned int from_fu_type = res_binding.get_assign(from_vertex);
#ifdef COMPRESS_CHAINED_OPS
      unsigned int cs_prev = LB.get_cstep(from_vertex);
      if(is_comprimibile && cs_prev == cs)
      {
         double current_exec_time = HLS->ALL->get_execution_time(from_fu_type, from_vertex, flow_graph);
         if(HLS->ALL->compute_normalized_area(from_fu_type, boost::in_degree(from_vertex, *flow_graph)) < 1.0 &&  current_exec_time < clock_cycle)
         {
            if(max_chained_prev < current_exec_time)
               max_chained_prev = current_exec_time;
         }
      }
#endif
      current_ending_time = MAX(current_ending_time, ending_time(from_vertex));
      if(GET_OP(flow_graph, from_vertex) == STORE && LB.get_cstep_end(from_vertex) == cs) has_store_load_unbounded_as_in = true;
      if(!HLS->ALL->is_operation_bounded(flow_graph, from_vertex, from_fu_type) && LB.get_cstep_end(from_vertex) == cs) has_store_load_unbounded_as_in = true;
   }
   current_starting_time = current_ending_time;

#ifdef COMPRESS_CHAINED_OPS
   if(max_chained_prev != 0.0)
   {
      std::cerr << "max_chained_prev " << max_chained_prev << std::endl;
      std::cerr << "op_execution_time " << op_execution_time << std::endl;
      if(op_execution_time > max_chained_prev)
         op_execution_time = op_execution_time - max_chained_prev;
      else
         op_execution_time = 0.0;
   }
#endif

   op_execution_time = op_execution_time-HLS->ALL->get_correction_time(fu_type);

   if(op_execution_time<0.0)
      op_execution_time = 0.0;

   /// try to take into account the controller delay
   if(GET_TYPE(flow_graph, v) & (TYPE_IF | TYPE_SWITCH | TYPE_MULTIIF))
   {
      op_execution_time += HLS->ALL->estimate_controller_delay();
      if(op_execution_time > clock_cycle)
         op_execution_time = clock_cycle;
   }

   stage_period = HLS->ALL->get_stage_period(fu_type, v, flow_graph)-HLS->ALL->get_correction_time(fu_type);
   if(stage_period<0.0)
      stage_period = 0.0;
   if(stage_period != 0.0)
   {
      /// recompute the execution time from scratch
      unsigned int n_cycles = HLS->ALL->get_cycles(fu_type, v, flow_graph);
      if(n_cycles > 1)
      {
         op_execution_time = clock_cycle * (n_cycles-1);
         op_execution_time += stage_period;
         THROW_ASSERT(current_starting_time-cs*clock_cycle<clock_cycle, "unexpected condition");
         op_execution_time -= current_starting_time-cs*clock_cycle;
      }
      else
         stage_period = 0.0;
   }
   else
      stage_period = 0.0;

   current_ending_time += op_execution_time;

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Operation " + GET_NAME(flow_graph, v) + " starts at " + boost::lexical_cast<std::string>(current_starting_time) + " and ends at " + boost::lexical_cast<std::string>(current_ending_time));
}

void parametric_list_based::compute_starting_ending_time_alap(vertex v, unsigned int fu_type, unsigned int cs, double & current_starting_time, double & current_ending_time, double &op_execution_time, double &stage_period, bool &has_store_load_unbounded_as_in, fu_binding &res_binding) const
{
   InEdgeIterator ei, ei_end;
   current_starting_time = cs * clock_cycle;
   for(boost::tie(ei, ei_end) = boost::in_edges(v, *flow_graph); ei != ei_end; ei++)
   {
      vertex from_vertex = boost::source(*ei, *flow_graph);
      unsigned int from_fu_type = res_binding.get_assign(from_vertex);
      current_starting_time = MAX(current_starting_time, ending_time(from_vertex));
      if(GET_OP(flow_graph, from_vertex) == STORE) has_store_load_unbounded_as_in = true;
      if(GET_OP(flow_graph, from_vertex) == LOAD && !HLS->ALL->is_direct_access_memory_unit(fu_type)) has_store_load_unbounded_as_in = true;
      if(!HLS->ALL->is_operation_bounded(flow_graph, from_vertex, from_fu_type)) has_store_load_unbounded_as_in = true;
   }
   OutEdgeIterator oi, oi_end;
   current_ending_time = std::numeric_limits<double>::max();
   for(boost::tie(oi, oi_end) = boost::out_edges(v, *flow_graph); oi != oi_end; oi++)
   {
      vertex to_vertex = boost::target(*oi, *flow_graph);
      current_ending_time = MIN(current_ending_time, starting_time(to_vertex));
   }

   op_execution_time = HLS->ALL->get_execution_time(fu_type, v, flow_graph)-HLS->ALL->get_correction_time(fu_type);
   if(op_execution_time < 0.0)
      op_execution_time = 0.0;
   /// try to take into account the controller delay
   if(GET_TYPE(flow_graph, v) & (TYPE_IF | TYPE_SWITCH | TYPE_MULTIIF ))
   {
      op_execution_time += HLS->ALL->estimate_controller_delay();
      if(op_execution_time > clock_cycle)
         op_execution_time = clock_cycle;
   }
   stage_period = HLS->ALL->get_stage_period(fu_type, v, flow_graph)-HLS->ALL->get_correction_time(fu_type);
   if(stage_period < 0.0)
      stage_period = 0.0;

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "                  Operation " + GET_NAME(flow_graph, v) + " starts at " + boost::lexical_cast<std::string>(current_starting_time) + " and ends at " + boost::lexical_cast<std::string>(current_ending_time));
}

bool parametric_list_based::BB_update_resources_use(unsigned int & used_resources, const vertex & DEBUG_PARAMETER(current_vertex), const double & DEBUG_PARAMETER(current_starting_time), const unsigned int fu_type) const
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "               Checking if operation " + GET_NAME(flow_graph, current_vertex) + " is schedulable in this step on " + boost::lexical_cast<std::string>(fu_type) + " - starting_time is " + boost::lexical_cast<std::string>(current_starting_time));
   if(used_resources == HLS->ALL->get_number_fu(fu_type))
      return false;
   else
   {
      used_resources++;
      return true;
   }
}

void parametric_list_based::add_to_priority_queues(std::vector<rehashed_heap<int> > & priority_queue, std::set<unsigned int, resource_ordering_functor> & ready_resources, const vertex v) const
{
   unsigned int fu_name;
   if(HLS->ALL->is_vertex_bounded_with(v, fu_name))
   {
      priority_queue[fu_name].push(v);
      ready_resources.insert(fu_name);
   }
   else
   {
      const std::set<unsigned int> & fu_set = HLS->ALL->can_implement_set(v);
      const std::set<unsigned int>::const_iterator fu_set_it_end = fu_set.end();
      for(std::set<unsigned int>::const_iterator fu_set_it = fu_set.begin(); fu_set_it != fu_set_it_end; fu_set_it++)
      {
         priority_queue[*fu_set_it].push(v);
         ready_resources.insert(*fu_set_it);
      }
   }
}

void parametric_list_based::exec()
{
   if (!HLS->functionId) THROW_ERROR("Scheduling cannot be performed as a global step");
   if (!HLS->ALL) THROW_ERROR("Module allocation has not been performed yet");
#if 1
   const BBGraphConstRef bbg = HLS->FB->CGetBBGraph();
   const OpGraphConstRef op_graph = HLS->FB->CGetOpGraph(FunctionBehavior::CFG);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Number of basic blocks: " + STR(boost::num_vertices(*bbg)));
   std::deque<vertex> vertices;
   boost::topological_sort(*bbg, std::front_inserter(vertices));
   std::deque<vertex>::const_iterator viend = vertices.end();
   unsigned int ctrl_steps = 0;
   for(std::deque<vertex>::const_iterator vi = vertices.begin(); vi != viend; vi++)
   {
      OpVertexSet operations(op_graph);
      std::list<vertex> bb_operations = bbg->CGetBBNodeInfo(*vi)->statements_list;
      for(std::list<vertex>::iterator l = bb_operations.begin(); l != bb_operations.end(); l++)
      {
         if(HLS->operations.find(*l) != HLS->operations.end())
            operations.insert(*l);
      }
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "performing scheduling of basic block " + STR(bbg->CGetBBNodeInfo(*vi)->block->number));
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "  .operations: " + STR(operations.size()));
      exec(operations, ctrl_steps);
      ctrl_steps = HLS->Rsch->get_csteps();
   }
   HLS->Rsch->set_csteps(ctrl_steps);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Number of control steps: "+boost::lexical_cast<std::string>(ctrl_steps));
   if (output_level >= OUTPUT_LEVEL_PEDANTIC)
   {
      HLS->Rsch->WriteDot("HLS_scheduling.dot");
   }
#else
   std::set<vertex> operations;
   for(std::unordered_set<vertex>::const_iterator l = HLS->operations.begin(); l != HLS->operations.end(); l++)
   {
      operations.insert(*l);
   }
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "  .operations: " + STR(operations.size()));
   exec(operations);
#endif
}

double parametric_list_based::compute_slack(vertex current_op, unsigned int current_vertex_cstep)
{
   double real_clock_period = HLS->HLS_C->get_clock_period();
   double starting_time_current_vertex = starting_time(current_op);
   return starting_time_current_vertex-(current_vertex_cstep)* clock_cycle + real_clock_period - clock_cycle;
}

void parametric_list_based::update_vertices_slack(vertex current_v, schedule & LB, std::unordered_set<vertex> &vertices_analyzed)
{
   /// compute the set of operations on the clock frontier
   unsigned int current_vertex_cstep =LB.get_cstep(current_v);
   std::queue<vertex> fifo;
   std::list<vertex> curr_list;
   double worst_slack=compute_slack(current_v, current_vertex_cstep);
   InEdgeIterator eo, eo_end;
   for(boost::tie(eo, eo_end) = boost::in_edges(current_v, *flow_graph); eo != eo_end; eo++)
      fifo.push(boost::source(*eo, *flow_graph));
   while(!fifo.empty())
   {
      vertex current_op = fifo.front();
      fifo.pop();
      if(current_vertex_cstep == LB.get_cstep(current_op))
      {
         curr_list.push_back(current_op);
         worst_slack = std::min(worst_slack, compute_slack(current_op, current_vertex_cstep));
         for(boost::tie(eo, eo_end) = boost::in_edges(current_op, *flow_graph); eo != eo_end; eo++)
            fifo.push(boost::source(*eo, *flow_graph));
      }
   }
   const std::list<vertex>::const_iterator cl_it_end = curr_list.end();
   for(std::list<vertex>::const_iterator cl_it = curr_list.begin(); cl_it != cl_it_end; ++cl_it)
   {
      vertices_analyzed.insert(*cl_it);
      LB.set_slack(*cl_it, worst_slack);
   }
   vertices_analyzed.insert(current_v);
   LB.set_slack(current_v, worst_slack);
}

void parametric_list_based::update_vertices_timing(unsigned int vertex_cstep, vertex current_v, schedule & LB, std::list<vertex> &vertices_analyzed, const double EPSILON, fu_binding & res_binding, const double setup_hold_time)
{
   /// compute the set of operations on the clock frontier
   std::queue<vertex> fifo;
   update_starting_ending_time(current_v, EPSILON, res_binding, setup_hold_time);
   vertices_analyzed.push_back(current_v);
   InEdgeIterator eo, eo_end;
   for(boost::tie(eo, eo_end) = boost::in_edges(current_v, *flow_graph); eo != eo_end; eo++)
      fifo.push(boost::source(*eo, *flow_graph));
   while(!fifo.empty())
   {
      vertex current_op = fifo.front();
      fifo.pop();
      if(vertex_cstep-1 <= LB.get_cstep(current_op))
      {
         vertices_analyzed.push_back(current_op);
         update_starting_ending_time(current_op, EPSILON, res_binding, setup_hold_time);
         for(boost::tie(eo, eo_end) = boost::in_edges(current_op, *flow_graph); eo != eo_end; eo++)
            fifo.push(boost::source(*eo, *flow_graph));
      }
   }
}


void parametric_list_based::update_starting_ending_time(vertex candidate_v, const double EPSILON, fu_binding & res_binding, const double setup_hold_time)
{
   unsigned int fu_type = res_binding.get_assign(candidate_v);
   /// slightly update unbounded operations by moving the ending time an epsilon before
   if(!HLS->ALL->is_operation_bounded(flow_graph, candidate_v, fu_type))
   {
      if(ceil(ending_time[candidate_v]/clock_cycle) == floor(ending_time[candidate_v]/clock_cycle))
      {
         //std::cerr << "before candidate_v0 " << GET_NAME(flow_graph, candidate_v) << " starting_time " << starting_time[candidate_v] << " ending_time " << ending_time[candidate_v] << std::endl;
         ending_time[candidate_v] -= EPSILON;
         starting_time[candidate_v] -= EPSILON;
         //std::cerr << "candidate_v0 " << GET_NAME(flow_graph, candidate_v) << " starting_time " << starting_time[candidate_v] << " ending_time " << ending_time[candidate_v] << std::endl;
      }
      return;
   }
   OutEdgeIterator eo, eo_end;
   const double DOUBLE_BIG_NUM = std::numeric_limits<double>::max();
   double current_ending_time;
   current_ending_time = DOUBLE_BIG_NUM;
   for(boost::tie(eo, eo_end) = boost::out_edges(candidate_v, *flow_graph); eo != eo_end; eo++)
   {
      vertex target = boost::target(*eo, *flow_graph);
      current_ending_time = MIN(current_ending_time, starting_time(target));
   }
   if(current_ending_time == DOUBLE_BIG_NUM)
   {
      if(ceil(ending_time[candidate_v]/clock_cycle) != floor(ending_time[candidate_v]/clock_cycle))
         current_ending_time = ceil(ending_time[candidate_v]/clock_cycle) * clock_cycle - EPSILON;
      else
         current_ending_time = (1+floor(ending_time[candidate_v]/clock_cycle)) * clock_cycle - EPSILON;
   }

   //std::cerr << "---candidate_v " << GET_NAME(flow_graph, candidate_v) << " current_ending_time " << current_ending_time << " ending_time " << ending_time[candidate_v] << " starting_time " << starting_time[candidate_v] << std::endl;

   if(current_ending_time > ending_time[candidate_v] && floor(current_ending_time/clock_cycle) == floor(starting_time[candidate_v]/clock_cycle))
   {
      double delta = current_ending_time - ending_time[candidate_v];
      //std::cerr << "before candidate_v1 " << GET_NAME(flow_graph, candidate_v) << " starting_time " << starting_time[candidate_v] << " ending_time " << ending_time[candidate_v] << std::endl;
      ending_time[candidate_v] = current_ending_time;
      starting_time[candidate_v] += delta;
      //std::cerr << "candidate_v1 " << GET_NAME(flow_graph, candidate_v) << " starting_time " << starting_time[candidate_v] << " ending_time " << ending_time[candidate_v] << std::endl;
   }
   else if(current_ending_time > ending_time[candidate_v] && floor(ending_time[candidate_v]/clock_cycle) == floor(starting_time[candidate_v]/clock_cycle))
   {
      if(floor(ending_time[candidate_v]/clock_cycle) != ceil(ending_time[candidate_v]/clock_cycle))
         current_ending_time = ceil(ending_time[candidate_v]/clock_cycle)*clock_cycle - EPSILON;
      else
         current_ending_time = (1+floor(ending_time[candidate_v]/clock_cycle))*clock_cycle - EPSILON;
      double delta = current_ending_time - ending_time[candidate_v];
      //std::cerr << "before candidate_v2 " << GET_NAME(flow_graph, candidate_v) << " starting_time " << starting_time[candidate_v] << " ending_time " << ending_time[candidate_v] << std::endl;
      ending_time[candidate_v] = current_ending_time;
      starting_time[candidate_v] += delta;
      //std::cerr << "candidate_v2 " << GET_NAME(flow_graph, candidate_v) << " starting_time " << starting_time[candidate_v] << " ending_time " << ending_time[candidate_v] << std::endl;
   }
   else
   {
      double stage_period = HLS->ALL->get_stage_period(fu_type, candidate_v, flow_graph)-setup_hold_time;
      if(stage_period<0.0)
         stage_period = 0.0;
      if(floor(current_ending_time/clock_cycle) != floor(starting_time[candidate_v]/clock_cycle) && (stage_period != 0.0))
      {
         //std::cerr << "before candidate_v3 " << GET_NAME(flow_graph, candidate_v) << " starting_time " << starting_time[candidate_v] << " ending_time " << ending_time[candidate_v] << std::endl;
         starting_time[candidate_v] = (floor(starting_time[candidate_v]/clock_cycle)+1)*clock_cycle-stage_period;
         //std::cerr << "candidate_v3 " << GET_NAME(flow_graph, candidate_v) << " starting_time " << starting_time[candidate_v] << " ending_time " << ending_time[candidate_v] << std::endl;
      }
   }
}

bool parametric_list_based::store_in_chaining_with_load_in(unsigned int current_vertex_cstep, vertex v)
{
   if(GET_OP(flow_graph, v) != STORE && GET_OP(flow_graph, v) != LOAD) return false;
   std::queue<vertex> fifo;
   InEdgeIterator eo, eo_end;
   for(boost::tie(eo, eo_end) = boost::in_edges(v, *flow_graph); eo != eo_end; eo++)
      fifo.push(boost::source(*eo, *flow_graph));
   while(!fifo.empty())
   {
      vertex current_op = fifo.front();
      fifo.pop();
      if(current_vertex_cstep == static_cast<unsigned int>(floor(ending_time(current_op)/clock_cycle)))
      {
         if(GET_OP(flow_graph, current_op) == LOAD || GET_OP(flow_graph, current_op) == STORE)
            return true;
         for(boost::tie(eo, eo_end) = boost::in_edges(current_op, *flow_graph); eo != eo_end; eo++)
            fifo.push(boost::source(*eo, *flow_graph));
      }
   }
   return false;
}

bool parametric_list_based::store_in_chaining_with_load_out(unsigned int current_vertex_cstep, vertex v)
{
   if(GET_OP(flow_graph, v) != STORE && GET_OP(flow_graph, v) != LOAD) return false;
   std::queue<vertex> fifo;
   OutEdgeIterator eo, eo_end;
   for(boost::tie(eo, eo_end) = boost::out_edges(v, *flow_graph); eo != eo_end; eo++)
      fifo.push(boost::source(*eo, *flow_graph));
   while(!fifo.empty())
   {
      vertex current_op = fifo.front();
      fifo.pop();
      if(current_vertex_cstep == static_cast<unsigned int>(floor(starting_time(current_op)/clock_cycle)))
      {
         if(GET_OP(flow_graph, current_op) == LOAD || GET_OP(flow_graph, current_op) == STORE)
            return true;
         for(boost::tie(eo, eo_end) = boost::out_edges(current_op, *flow_graph); eo != eo_end; eo++)
            fifo.push(boost::source(*eo, *flow_graph));
      }
   }
   return false;
}

void parametric_list_based::do_balanced_scheduling(std::deque<vertex> &sub_levels, schedule & LB, fu_binding & res_binding, const double EPSILON, const double setup_hold_time)
{
   /// balanced scheduling
   /// step 1: dependencies computation
   std::map<vertex, std::set<vertex> > DMAP;
   std::map<vertex, size_t> D;
   for(std::deque<vertex>::const_reverse_iterator rit = sub_levels.rbegin(); rit != sub_levels.rend(); ++rit)
   {
      vertex candidate_v = *rit;
      InEdgeIterator ei, ei_end;
      for(boost::tie(ei, ei_end) = boost::in_edges(candidate_v, *flow_graph); ei != ei_end; ++ei)
      {
         vertex src = boost::source(*ei, *flow_graph);
         if(DMAP.find(src) != DMAP.end())
            DMAP[candidate_v].insert(DMAP.find(src)->second.begin(), DMAP.find(src)->second.end());
         DMAP[candidate_v].insert(src);
      }
      D[candidate_v] = DMAP[candidate_v].size();
   }
   /// step 2: compute operation distribution
   std::map<unsigned int, std::deque<vertex> > T;
   std::map<unsigned int, double > T_area;
   unsigned int min_cycle=std::numeric_limits<unsigned int>::max();
   unsigned int max_cycle=0, curr_cs;
   double total_resource_area = 0;
   for(std::deque<vertex>::const_iterator vi = sub_levels.begin(); vi != sub_levels.end(); vi++)
   {
      vertex current_op = *vi;
      curr_cs = LB.get_cstep(current_op);
      min_cycle = std::min(min_cycle, curr_cs);
      max_cycle = std::max(max_cycle, curr_cs);
      T[curr_cs].push_back(current_op);
      unsigned int fu_type = res_binding.get_assign(current_op);
      double curr_area = HLS->ALL->get_area(fu_type);
      if(T_area.find(curr_cs) == T_area.end())
         T_area[curr_cs] = curr_area;
      else
         T_area[curr_cs] += curr_area;
      total_resource_area += curr_area;
   }
   unsigned int cycles=max_cycle-min_cycle+1;
   double average_load = total_resource_area/cycles; //static_cast<double>(operations.size())/cycles;//std::numeric_limits<double>::max();//
   //std::cerr << "AVG LOAD: " << average_load << std::endl;
   std::map<unsigned int, std::deque<vertex> >::const_reverse_iterator t_it_end = T.rend();
   for(std::map<unsigned int, std::deque<vertex> >::const_reverse_iterator t_it = T.rbegin(); t_it != t_it_end; ++t_it)
   {
      unsigned int time = t_it->first;
      if(time == 0) continue;
      if(T[time].size() >= average_load) continue;
      unsigned int ptime = time -1;
      //std::cerr << "considering cs " << time << std::endl;
      /// check if time has unbounded vertices or the return statement
      bool has_unbounded = false;
      bool has_return = false;
      std::deque<vertex>::const_iterator time_it_end = T.find(time)->second.end();
      for(std::deque<vertex>::const_iterator time_it = T.find(time)->second.begin(); !has_unbounded && !has_return && time_it != time_it_end; ++time_it)
      {
         vertex candidate_v = *time_it;
         unsigned int fu_type = res_binding.get_assign(candidate_v);
         if(!HLS->ALL->is_operation_bounded(flow_graph, candidate_v, fu_type))
            has_unbounded = true;
         if(GET_TYPE(flow_graph,candidate_v) & TYPE_RET)
            has_return = true;
      }
      if(has_return) continue;

      while(T_area[time] < average_load && ptime>=min_cycle)
      {
         while(T.find(ptime) != T.end())
         {
            double best_starting_time=0.0;
            double best_ending_time=0.0;
            double best_starting_area=0;
            size_t max_dependencies = 0;
            vertex best_node = NULL_VERTEX;
            std::deque<vertex>::const_iterator ptime_it_end = T.find(ptime)->second.end();
            for(std::deque<vertex>::const_iterator ptime_it = T.find(ptime)->second.begin(); ptime_it != ptime_it_end; ++ptime_it)
            {
               vertex candidate_v = *ptime_it;
               //std::cerr << "candidate_v " << GET_NAME(flow_graph, candidate_v) << std::endl;
               unsigned int fu_type = res_binding.get_assign(candidate_v);
               if(HLS->ALL->get_number_fu(fu_type) != INFINITE_UINT && !HLS->ALL->is_vertex_bounded(fu_type)) continue;
               /// only 1 unbounded operation is allowed in a control step
               if(!HLS->ALL->is_operation_bounded(flow_graph, candidate_v, fu_type)) continue; /// unbounded operations could not be moved
               if(GET_TYPE(flow_graph, candidate_v) & (TYPE_PHI|TYPE_VPHI|TYPE_INIT|TYPE_LABEL)) continue; /// PHIs could not be moved
               if(GET_OP(flow_graph, candidate_v) == LOAD || GET_OP(flow_graph, candidate_v) == STORE) continue; /// LOADs&STOREs could not be moved

               double candidate_starting_time;
               double candidate_ending_time;
               double candidate_stage_period;
               double candidate_op_execution_time;
               bool forward_node_p = true;
               bool has_store_load_unbounded_as_in = false;
               compute_starting_ending_time_alap(candidate_v, fu_type, time, candidate_starting_time, candidate_ending_time, candidate_op_execution_time, candidate_stage_period, has_store_load_unbounded_as_in, res_binding);
               //std::cerr << "***candidate_starting_time " << candidate_starting_time << " candidate_ending_time " << candidate_ending_time << " candidate_op_execution_time " << candidate_op_execution_time << " candidate_stage_period " << candidate_stage_period << std::endl;

               if ((candidate_starting_time > (time * clock_cycle)) && !HLS->Param->getOption<bool>(OPT_chaining)) continue; /// Chaining is possible but not allowed

               if ((candidate_starting_time > (time * clock_cycle)) && has_store_load_unbounded_as_in) continue; /// Chaining with STOREs/LOADs is not possible
               if(candidate_op_execution_time > clock_cycle && has_unbounded) continue; /// Multi-cycles operations cannot be scheduled together with unbounded operations

               if (candidate_starting_time+candidate_op_execution_time+setup_hold_time>candidate_ending_time) continue; /// operation does not fit

               bool is_pipelined = HLS->ALL->get_initiation_time(fu_type, candidate_v, flow_graph) != 0;

               if(is_pipelined && (candidate_starting_time > (time * clock_cycle)) && candidate_stage_period+candidate_starting_time+setup_hold_time>((time + 1) * clock_cycle)) continue; /// pipelined operation does not fit at the beginning
               if(is_pipelined &&  (floor(candidate_ending_time/clock_cycle) == floor((candidate_starting_time+candidate_op_execution_time)/clock_cycle)) && (clock_cycle - (ceil(candidate_ending_time/clock_cycle)*clock_cycle-candidate_ending_time+candidate_stage_period) < 0)) continue;

               if(!is_pipelined && candidate_stage_period == 0.0 && candidate_op_execution_time < clock_cycle &&
                  candidate_starting_time + candidate_op_execution_time + setup_hold_time > (time + 1) * clock_cycle) continue; /// operation does not fit in the cycle

               if(!is_pipelined && candidate_stage_period != 0.0 &&
                  candidate_starting_time + candidate_stage_period + setup_hold_time > (time + 1) * clock_cycle) continue; /// operation does not fit in the cycle

               if(!is_pipelined && candidate_stage_period != 0.0 &&
                  candidate_starting_time + candidate_stage_period + setup_hold_time > candidate_ending_time) continue; /// operation does not fit in the cycle

               if(store_in_chaining_with_load_out(time,candidate_v)) continue; /// store and load cannot be chained

               if(is_pipelined)
               {
                  if(floor(candidate_ending_time/clock_cycle) == floor((candidate_starting_time+candidate_op_execution_time)/clock_cycle))
                     candidate_starting_time = clock_cycle - (ceil(candidate_ending_time/clock_cycle)*clock_cycle-candidate_ending_time+candidate_stage_period)+time*clock_cycle;
                  else
                     candidate_starting_time = (time+1)*clock_cycle - candidate_stage_period - EPSILON;
                  candidate_ending_time = candidate_starting_time + candidate_op_execution_time;
               }
               else if(candidate_stage_period != 0.0)
               {
                  if(candidate_ending_time> (time + 1) * clock_cycle)
                  {
                     candidate_starting_time = (time+1)*clock_cycle - candidate_stage_period - EPSILON;
                     candidate_ending_time = (time+1)*clock_cycle - EPSILON;
                  }
                  else
                  {
                     candidate_starting_time = candidate_ending_time - candidate_stage_period - EPSILON;
                  }
               }
               else if(candidate_op_execution_time < clock_cycle)
               {
                  if(candidate_ending_time> (time + 1) * clock_cycle)
                  {
                     candidate_starting_time = (time+1)*clock_cycle - candidate_op_execution_time - EPSILON;
                     candidate_ending_time = (time+1)*clock_cycle - EPSILON;
                  }
                  else
                  {
                     candidate_starting_time = candidate_ending_time - candidate_op_execution_time - EPSILON;
                  }
               }
               else
                  candidate_ending_time = candidate_starting_time + candidate_op_execution_time;

               /// try to keep the previous latency
               if(candidate_ending_time> (max_cycle+1)*clock_cycle) continue;

               //std::cerr << "candidate_starting_time " << candidate_starting_time << " candidate_ending_time " << candidate_ending_time << " candidate_stage_period " << candidate_stage_period << std::endl;

               //std::cerr << "check the successor starting_time" << std::endl;
               OutEdgeIterator eo, eo_end;
               for(boost::tie(eo, eo_end) = boost::out_edges(candidate_v, *flow_graph); forward_node_p && eo != eo_end; eo++)
               {
                  vertex target = boost::target(*eo, *flow_graph);
                  //std::cerr << GET_NAME(flow_graph, target) << " " << starting_time[target] << std::endl;
                  if(candidate_ending_time>starting_time[target])
                     forward_node_p = false;
               }
               if(!forward_node_p) continue;

               if(best_node == NULL_VERTEX || D[candidate_v]>max_dependencies || (D[candidate_v]==max_dependencies && best_starting_area < T_area[ptime]))
               {
                  max_dependencies=D[candidate_v];
                  best_node=candidate_v;
                  best_starting_time = candidate_starting_time;
                  best_ending_time = candidate_ending_time;
                  best_starting_area=T_area[ptime];
               }
            }
            if(best_node != NULL_VERTEX)
            {
               LB.set_execution(best_node, time);
               starting_time[best_node] = best_starting_time;
               ending_time[best_node] = best_ending_time;
               LB.set_execution_end(best_node, time+static_cast<unsigned int>(floor((ending_time[best_node]-starting_time[best_node])/clock_cycle)));
               T[ptime].erase(std::find(T[ptime].begin(), T[ptime].end(), best_node));
               T[time].push_back(best_node);
               unsigned int fu_type = res_binding.get_assign(best_node);
               double curr_area = HLS->ALL->get_area(fu_type);
               T_area[time] += curr_area;
               T_area[ptime] -= curr_area;
               InEdgeIterator ei, ei_end;
               for(boost::tie(ei, ei_end) = boost::in_edges(best_node, *flow_graph); ei != ei_end; ++ei)
               {
                  vertex from_vertex = boost::source(*ei, *flow_graph);
                  update_starting_ending_time(from_vertex, EPSILON, res_binding, setup_hold_time);
               }
               //std::cerr << "best node " << GET_NAME(flow_graph, best_node) << " = " << T_area[time] << std::endl;
            }
            else
               break;
            if(T_area[time] >= average_load)
               break;
         }
         ptime = ptime -1;
         //std::cerr << "ptime " << ptime << std::endl;
      }
   }
   std::unordered_set<vertex> vertices_analyzed;
   for(std::deque<vertex>::const_iterator vi = sub_levels.begin(); vi != sub_levels.end(); ++vi)
   {
      if(vertices_analyzed.find(*vi) != vertices_analyzed.end())
      {
         //std::cerr << "slack for " << GET_NAME(flow_graph, *vi) << "=" << LB.get_slack(*vi) << std::endl;
         continue;
      }
      update_vertices_slack(*vi, LB, vertices_analyzed);
      //std::cerr << "updating slack for " << GET_NAME(flow_graph, *vi) << "=" << LB.get_slack(*vi) << std::endl;
   }

}

void parametric_list_based::do_balanced_scheduling1(std::deque<vertex> &sub_levels, schedule & LB, fu_binding & res_binding, const double EPSILON, const double setup_hold_time)
{
//#define SLACK_BASED
   /// balanced scheduling
   /// step 1: dependencies computation
   std::map<vertex, std::set<vertex> > DMAP;
   std::map<vertex, size_t> D;
   for(std::deque<vertex>::const_reverse_iterator rit = sub_levels.rbegin(); rit != sub_levels.rend(); ++rit)
   {
      vertex candidate_v = *rit;
      InEdgeIterator ei, ei_end;
      for(boost::tie(ei, ei_end) = boost::in_edges(candidate_v, *flow_graph); ei != ei_end; ++ei)
      {
         vertex src = boost::source(*ei, *flow_graph);
         if(DMAP.find(src) != DMAP.end())
            DMAP[candidate_v].insert(DMAP.find(src)->second.begin(), DMAP.find(src)->second.end());
         DMAP[candidate_v].insert(src);
      }
      D[candidate_v] = DMAP[candidate_v].size();
   }
   /// step 2: compute operation distribution
   std::map<unsigned int, std::deque<vertex> > T;

   std::map<unsigned int, double> total_obj;
   std::map<unsigned int, std::map<unsigned int, double > > T_obj;
   unsigned int min_cycle=std::numeric_limits<unsigned int>::max();
   unsigned int max_cycle=0, curr_cs;
   for(std::deque<vertex>::const_iterator vi = sub_levels.begin(); vi != sub_levels.end(); vi++)
   {
      vertex current_op = *vi;
      curr_cs = LB.get_cstep(current_op);
      min_cycle = std::min(min_cycle, curr_cs);
      max_cycle = std::max(max_cycle, curr_cs);
      T[curr_cs].push_back(current_op);
      unsigned int fu_type = res_binding.get_assign(current_op);
#ifdef SLACK_BASED
      if(T_obj.find(fu_type) == T_obj.end())
         T_obj[fu_type][curr_cs] = LB.get_slack(current_op);
      if(T_obj.find(fu_type)->second.find(curr_cs) == T_obj.find(fu_type)->second.end())
         T_obj[fu_type][curr_cs] = LB.get_slack(current_op);
      else
         T_obj[fu_type][curr_cs] = std::min(LB.get_slack(current_op), T_obj[fu_type][curr_cs]);
#else
      double curr_area = HLS->ALL->get_area(fu_type);
      if(T_obj.find(fu_type) == T_obj.end())
         T_obj[fu_type][curr_cs] = curr_area;
      if(T_obj.find(fu_type)->second.find(curr_cs) == T_obj.find(fu_type)->second.end())
         T_obj[fu_type][curr_cs] = curr_area;
      else
         T_obj[fu_type][curr_cs] += curr_area;

      if(total_obj.find(fu_type) == total_obj.end())
         total_obj[fu_type] = curr_area;
      else
         total_obj[fu_type] += curr_area;
#endif
   }
#ifdef SLACK_BASED
   for(std::map<unsigned int, std::map<unsigned int, double > >::const_iterator to_it = T_obj.begin(); to_it != T_obj.end(); ++to_it)
   {
      total_obj[to_it->first] = 0;
      for(std::map<unsigned int, double >::const_iterator si = to_it->second.begin(); si != to_it->second.end(); ++si)
      {
         total_obj[to_it->first] += si->second;
      }
   }
#endif
   std::unordered_set<vertex> vertices_analyzed;
   unsigned int cycles=max_cycle-min_cycle+1;
   std::map<unsigned int, std::deque<vertex> >::const_reverse_iterator t_it_end = T.rend();
   for(std::map<unsigned int, std::deque<vertex> >::const_reverse_iterator t_it = T.rbegin(); t_it != t_it_end; ++t_it)
   {
      unsigned int time = t_it->first;
      if(time == min_cycle) continue;
      unsigned int ptime = time -1;
      //std::cerr << "considering cs " << time << std::endl;
      /// check if time has unbounded vertices or the return statement
      bool has_unbounded = false;
      bool has_return = false;
      std::deque<vertex>::const_iterator time_it_end = T.find(time)->second.end();
      for(std::deque<vertex>::const_iterator time_it = T.find(time)->second.begin(); !has_unbounded && !has_return && time_it != time_it_end; ++time_it)
      {
         vertex candidate_v = *time_it;
         unsigned int fu_type = res_binding.get_assign(candidate_v);
         if(!HLS->ALL->is_operation_bounded(flow_graph, candidate_v, fu_type))
            has_unbounded = true;
         if(GET_TYPE(flow_graph,candidate_v) & TYPE_RET)
            has_return = true;
      }
      if(has_return)
      {
         /// recompute the number of cycles removing the return cycle
         --cycles;
         continue;
      }

      while(ptime>=min_cycle)
      {
         while(T.find(ptime) != T.end())
         {
            double best_starting_time=0.0;
            double best_ending_time=0.0;
#ifdef SLACK_BASED
            double best_objective=std::numeric_limits<double>::max();
            double best_candidate_slack = 0;
#else
            double best_objective=0;
#endif
            size_t max_dependencies = 0;
            vertex best_node = NULL_VERTEX;
            std::deque<vertex>::const_iterator ptime_it_end = T.find(ptime)->second.end();
            for(std::deque<vertex>::const_iterator ptime_it = T.find(ptime)->second.begin(); ptime_it != ptime_it_end; ++ptime_it)
            {
               vertex candidate_v = *ptime_it;
               //std::cerr << "candidate_v " << GET_NAME(flow_graph, candidate_v) << "(" <<GET_OP(flow_graph, candidate_v) << ")" << std::endl;
               unsigned int fu_type = res_binding.get_assign(candidate_v);
               if(HLS->ALL->get_number_fu(fu_type) != INFINITE_UINT && !HLS->ALL->is_vertex_bounded(fu_type)) continue;
               /// only 1 unbounded operation is allowed in a control step
               if(!HLS->ALL->is_operation_bounded(flow_graph, candidate_v, fu_type)) continue; /// unbounded operations could not be moved
               if(GET_TYPE(flow_graph, candidate_v) & (TYPE_PHI|TYPE_VPHI|TYPE_INIT|TYPE_LABEL)) continue; /// PHIs could not be moved
               if(GET_OP(flow_graph, candidate_v) == LOAD || GET_OP(flow_graph, candidate_v) == STORE) continue; /// LOADs&STOREs could not be moved

               double candidate_starting_time;
               double candidate_ending_time;
               double candidate_stage_period;
               double candidate_op_execution_time;
               bool forward_node_p = true;
               bool has_store_load_unbounded_as_in = false;
               compute_starting_ending_time_alap(candidate_v, fu_type, time, candidate_starting_time, candidate_ending_time, candidate_op_execution_time, candidate_stage_period, has_store_load_unbounded_as_in, res_binding);

               //std::cerr << "***candidate_starting_time " << candidate_starting_time << " candidate_ending_time " << candidate_ending_time << " candidate_op_execution_time " << candidate_op_execution_time << " candidate_stage_period " << candidate_stage_period << std::endl;

               if (candidate_starting_time+candidate_op_execution_time>candidate_ending_time) continue; /// operation does not fit

               if(T_obj.find(fu_type) != T_obj.end() && T_obj.find(fu_type)->second.find(time) != T_obj.find(fu_type)->second.end() &&
#ifdef SLACK_BASED
                     T_obj[fu_type][time] < (total_obj[fu_type]/cycles /*static_cast<double>(T_obj[fu_type].size())*/) //
#else
                     T_obj[fu_type][time] >= (total_obj[fu_type]/ cycles/*static_cast<double>(T_obj[fu_type].size())*/) //
#endif
                     )
                  continue; /// no room for fu_type at time time

               if ((candidate_starting_time > (time * clock_cycle)) && !HLS->Param->getOption<bool>(OPT_chaining)) continue; /// Chaining is possible but not allowed

               if ((candidate_starting_time > (time * clock_cycle)) && has_store_load_unbounded_as_in) continue; /// Chaining with STOREs/LOADs is not possible
               if(candidate_op_execution_time > clock_cycle && has_unbounded) continue; /// Multi-cycles operations cannot be scheduled together with unbounded operations


               bool is_pipelined = HLS->ALL->get_initiation_time(fu_type, candidate_v, flow_graph) != 0;

               if(is_pipelined && (candidate_starting_time > (time * clock_cycle)) && candidate_stage_period+candidate_starting_time>((time + 1) * clock_cycle)) continue; /// pipelined operation does not fit at the beginning
               if(is_pipelined &&  (floor(candidate_ending_time/clock_cycle) == floor((candidate_starting_time+candidate_op_execution_time)/clock_cycle)) && (clock_cycle - (ceil(candidate_ending_time/clock_cycle)*clock_cycle-candidate_ending_time+candidate_stage_period) < 0)) continue;

               if(!is_pipelined && candidate_stage_period == 0.0 && candidate_op_execution_time < clock_cycle &&
                  candidate_starting_time + candidate_op_execution_time > (time + 1) * clock_cycle) continue; /// operation does not fit in the cycle

               if(!is_pipelined && candidate_stage_period != 0.0 &&
                  candidate_starting_time + candidate_stage_period > (time + 1) * clock_cycle) continue; /// operation does not fit in the cycle

               if(!is_pipelined && candidate_stage_period != 0.0 &&
                  candidate_starting_time + candidate_stage_period > candidate_ending_time) continue; /// operation does not fit in the cycle

               if(store_in_chaining_with_load_out(time,candidate_v)) continue; /// store and load cannot be chained

               if(is_pipelined)
               {
                  if(floor(candidate_ending_time/clock_cycle) == floor((candidate_starting_time+candidate_op_execution_time)/clock_cycle))
                     candidate_starting_time = clock_cycle - (ceil(candidate_ending_time/clock_cycle)*clock_cycle-candidate_ending_time+candidate_stage_period)+time*clock_cycle;
                  else
                     candidate_starting_time = (time+1)*clock_cycle - candidate_stage_period - EPSILON;
                  candidate_ending_time = candidate_starting_time + candidate_op_execution_time;
               }
               else if(candidate_stage_period != 0.0)
               {
                  if(candidate_ending_time> (time + 1) * clock_cycle)
                  {
                     candidate_starting_time = (time+1)*clock_cycle - candidate_stage_period - EPSILON;
                     candidate_ending_time = (time+1)*clock_cycle - EPSILON;
                  }
                  else
                  {
                     candidate_starting_time = candidate_ending_time - candidate_stage_period - EPSILON;
                  }
               }
               else if(candidate_op_execution_time < clock_cycle)
               {
                  if(candidate_ending_time> (time + 1) * clock_cycle)
                  {
                     candidate_starting_time = (time+1)*clock_cycle - candidate_op_execution_time - EPSILON;
                     candidate_ending_time = (time+1)*clock_cycle - EPSILON;
                  }
                  else
                  {
                     candidate_starting_time = candidate_ending_time - candidate_op_execution_time - EPSILON;
                  }
               }
               else
                  candidate_ending_time = candidate_starting_time + candidate_op_execution_time;

               /// try to keep the previous latency
               if(candidate_ending_time> (max_cycle+1)*clock_cycle) continue;

               /*if((candidate_ending_time < (time+1)*clock_cycle - EPSILON) && (candidate_starting_time-time*clock_cycle > clock_cycle/3))
               {
                  std::cerr << "candidate_v skipped to avoid chaining as much as possible " <<GET_NAME(flow_graph, candidate_v) << "(" <<GET_OP(flow_graph, candidate_v) << ")" << std::endl;
                  continue; /// try to reduce as much as possible the chaining between operations
               }*/


#ifdef SLACK_BASED
               double candidate_slack = candidate_starting_time-time*clock_cycle + (HLS->HLS_C->get_clock_period() -clock_cycle);

               //std::cerr << "avg obj " << total_obj[fu_type]/cycles << std::endl;
               //std::cerr << "slack " << candidate_slack << std::endl;
               //std::cerr << "size " << T_obj[fu_type].size() << std::endl;
               if(candidate_slack < total_obj[fu_type]/cycles /*static_cast<double>(T_obj[fu_type].size())*/)
                  continue; /// no room for fu_type at time time
#else
               double curr_area = HLS->ALL->get_area(fu_type);
               if(curr_area >= total_obj[fu_type]/ /*static_cast<double>(T_obj[fu_type].size())*/cycles)
                  continue; /// no room for fu_type at time time
#endif

               //std::cerr << "candidate_starting_time " << candidate_starting_time << " candidate_ending_time " << candidate_ending_time << " candidate_stage_period " << candidate_stage_period << std::endl;

               //std::cerr << "check the successor starting_time" << std::endl;
               OutEdgeIterator eo, eo_end;
               for(boost::tie(eo, eo_end) = boost::out_edges(candidate_v, *flow_graph); forward_node_p && eo != eo_end; eo++)
               {
                  vertex target = boost::target(*eo, *flow_graph);
                  //std::cerr << GET_NAME(flow_graph, target) << " " << starting_time[target] << std::endl;
                  if(candidate_ending_time>starting_time[target])
                     forward_node_p = false;
               }
               if(!forward_node_p) continue;

               if(best_node == NULL_VERTEX || D[candidate_v]>max_dependencies || (D[candidate_v]==max_dependencies &&
#ifdef SLACK_BASED
                                                                                  (best_objective > LB.get_slack(candidate_v) ||
                                                                                   (best_objective == LB.get_slack(candidate_v) && best_candidate_slack < candidate_slack))
#else
                                                                                  best_objective < HLS->ALL->get_area(fu_type)
#endif
                                                                                  ))
               {
                  max_dependencies=D[candidate_v];
                  best_node=candidate_v;
                  best_starting_time = candidate_starting_time;
                  best_ending_time = candidate_ending_time;
#ifdef SLACK_BASED
                  best_objective=LB.get_slack(candidate_v);
                  best_candidate_slack = candidate_slack;
#else
                  best_objective=HLS->ALL->get_area(fu_type);
#endif
               }
            }
            if(best_node != NULL_VERTEX)
            {
               //std::cerr << "best " << GET_NAME(flow_graph, best_node) << "=" << time << std::endl;
               LB.set_execution(best_node, time);
               starting_time[best_node] = best_starting_time;
               ending_time[best_node] = best_ending_time;
               LB.set_execution_end(best_node, time+static_cast<unsigned int>(floor((ending_time[best_node]-starting_time[best_node])/clock_cycle)));

               T[ptime].erase(std::find(T[ptime].begin(), T[ptime].end(), best_node));
               T[time].push_back(best_node);
               vertices_analyzed.clear();
               std::list<vertex> vertices_slack_analyzed;
               update_vertices_timing(ptime, best_node, LB, vertices_slack_analyzed, EPSILON, res_binding, setup_hold_time);
               for(std::list<vertex>::const_iterator va_it = vertices_slack_analyzed.begin(); va_it != vertices_slack_analyzed.end(); ++va_it)
               {
                  if(vertices_analyzed.find(*va_it) != vertices_analyzed.end())
                  {
                     //std::cerr << "slack for " << GET_NAME(flow_graph, *va_it) << "=" << LB.get_slack(*va_it) << std::endl;
                     continue;
                  }
                  update_vertices_slack(*va_it, LB, vertices_analyzed);
                  //std::cerr << "updating slack for " << GET_NAME(flow_graph, *va_it) << "=" << LB.get_slack(*va_it) << std::endl;
               }


               unsigned int fu_type = res_binding.get_assign(best_node);
#ifdef SLACK_BASED
               //std::cerr << "total_obj[fu_type] " << total_obj[fu_type] << std::endl;
               if(T_obj.find(fu_type)->second.find(time) != T_obj.find(fu_type)->second.end() &&
                     T_obj[fu_type][time] != std::numeric_limits<double>::max())
                  total_obj[fu_type] -= T_obj[fu_type][time];
               //std::cerr << "total_obj[fu_type] " << total_obj[fu_type] << std::endl;
               T_obj[fu_type][time] = std::numeric_limits<double>::max();
               const std::deque<vertex>::const_iterator utime_it_end = T.find(time)->second.end();
               for(std::deque<vertex>::const_iterator utime_it = T.find(time)->second.begin(); utime_it != utime_it_end; ++utime_it)
               {
                  unsigned int cur_fu_type = res_binding.get_assign(*utime_it);
                  if(cur_fu_type == fu_type)
                     T_obj[fu_type][time] = std::min(LB.get_slack(*utime_it), T_obj[fu_type][time]);
               }

               if(T_obj[fu_type][time] != std::numeric_limits<double>::max())
                  total_obj[fu_type] += T_obj[fu_type][time];
               else
                  T_obj[fu_type].erase(time);
               //std::cerr << "total_obj[fu_type] " << total_obj[fu_type] << std::endl;

               total_obj[fu_type] -= T_obj[fu_type][ptime];
               //std::cerr << "total_obj[fu_type] " << total_obj[fu_type] << std::endl;
               T_obj[fu_type][ptime] = std::numeric_limits<double>::max();
               const std::deque<vertex>::const_iterator uptime_it_end = T.find(ptime)->second.end();
               for(std::deque<vertex>::const_iterator uptime_it = T.find(ptime)->second.begin(); uptime_it != uptime_it_end; ++uptime_it)
               {
                  //std::cerr << "|slack for " << GET_NAME(flow_graph, *uptime_it) << "=" << LB.get_slack(*uptime_it) << std::endl;
                  unsigned int cur_fu_type = res_binding.get_assign(*uptime_it);
                  if(cur_fu_type == fu_type)
                     T_obj[fu_type][ptime] = std::min(LB.get_slack(*uptime_it), T_obj[fu_type][ptime]);
               }
               if(T_obj[fu_type][ptime] != std::numeric_limits<double>::max())
                  total_obj[fu_type] += T_obj[fu_type][ptime];
               else
                  T_obj[fu_type].erase(ptime);
               //std::cerr << "total_obj[fu_type] " << total_obj[fu_type] << std::endl;
#else
               double curr_area = HLS->ALL->get_area(fu_type);
               T_obj[fu_type][time] += curr_area;
               T_obj[fu_type][ptime] -= curr_area;
#endif

               //std::cerr << "best node " << GET_NAME(flow_graph, best_node) << " = " << T_area[time] << std::endl;
            }
            else
               break;
         }
         ptime = ptime -1;
         //std::cerr << "ptime " << ptime << std::endl;
      }
   }
   vertices_analyzed.clear();
   for(std::deque<vertex>::const_iterator vi = sub_levels.begin(); vi != sub_levels.end(); ++vi)
   {
      if(vertices_analyzed.find(*vi) != vertices_analyzed.end())
      {
         //std::cerr << "slack for " << GET_NAME(flow_graph, *vi) << "=" << LB.get_slack(*vi) << std::endl;
         continue;
      }
      update_vertices_slack(*vi, LB, vertices_analyzed);
      //std::cerr << "updating slack for " << GET_NAME(flow_graph, *vi) << "=" << LB.get_slack(*vi) << std::endl;
   }
}

bool parametric_list_based::check_non_direct_operation_chaining(vertex current_v, unsigned int cs, schedule & LB, fu_binding &res_binding) const
{
   /// compute the set of operations on the control step frontier
   std::queue<vertex> fifo;
   InEdgeIterator eo, eo_end;
   for(boost::tie(eo, eo_end) = boost::in_edges(current_v, *flow_graph); eo != eo_end; eo++)
      fifo.push(boost::source(*eo, *flow_graph));
   while(!fifo.empty())
   {
      vertex current_op = fifo.front();
      fifo.pop();
      if(cs == LB.get_cstep_end(current_op))
      {
         unsigned int from_fu_type = res_binding.get_assign(current_op);
         if(GET_OP(flow_graph, current_op) == LOAD && !HLS->ALL->is_direct_access_memory_unit(from_fu_type))
            return true;
         for(boost::tie(eo, eo_end) = boost::in_edges(current_op, *flow_graph); eo != eo_end; eo++)
            fifo.push(boost::source(*eo, *flow_graph));
      }
   }
   return false;
}
