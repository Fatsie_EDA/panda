/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file schedule.cpp
 * @brief Class implementation of the schedule data structure.
 *
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include <ostream>

#include "schedule.hpp"
#include "behavioral_writer_helper.hpp"
#include "op_graph.hpp"
#include "behavioral_helper.hpp"
#include "function_behavior.hpp"

///. include
#include "Parameter.hpp"

schedule::schedule(const OpGraphConstRef _op_graph, const ParameterConstRef _parameters) :
   tot_csteps(0),
   op_graph(_op_graph),
   parameters(_parameters)
{

}

schedule::~schedule()
{

}

void schedule::print(std::ostream& os, const OpGraphConstRef data) const
{
   std::map<vertex, unsigned int>::const_iterator it, it_end = op_starting_cycle.end();
   for(it = op_starting_cycle.begin(); it != it_end; it++)
   {
      if(!data->is_in_subset(it->first)) continue;
      os << "Operation: ";
      os.width(COLUMN_SIZE);
      os.setf(std::ios_base::left, std::ios_base::adjustfield);
      os << GET_NAME(data, it->first) + "(" + GET_OP(data, it->first) + ")";
      os.width(0);

      os << " scheduled at control step (";
      os.width(0);
      os.setf(std::ios_base::left, std::ios_base::adjustfield);
      os << it->second;
      if(op_ending_cycle.find(it->first) != op_ending_cycle.end())
         os << "-" << op_ending_cycle.find(it->first)->second;
      os << ")";
      os.width(3);

      os << std::endl;
   }
   os << "Total number of control steps: " << tot_csteps << std::endl;
}

class ScheduleWriter : public GraphWriter
{
   private:
      ///The schedule to be printed
      const schedule * sch;

   public:
      /**
       * Constructor
       * @param op_graph is the operation graph to be printed
       * @param _sch is the schedule
       */
      ScheduleWriter(const OpGraphConstRef op_graph, const schedule * _sch) :
         GraphWriter(op_graph.get(), 0),
         sch(_sch)
      {}

      /**
       * Redifinition of operator()
       */
      void operator()(std::ostream& os) const
      {
         os << "//Scheduling solution\n";
         os << "splines=polyline;\n";
         std::multimap<unsigned int, vertex> inverse_relation;
         VertexIterator v, v_end;
         for(boost::tie(v, v_end) = boost::vertices(*printing_graph); v != v_end; v++)
         {
            inverse_relation.insert(std::multimap<unsigned int, vertex>::value_type(sch->get_cstep(*v), *v));
         }
         for (unsigned int level = 0; level < sch->get_csteps(); level++)
         {
            std::pair<std::multimap<unsigned int, vertex>::const_iterator, std::multimap<unsigned int, vertex>::const_iterator > p = inverse_relation.equal_range(level);
            if (p.first == p.second) continue;
            os << "//Control Step: " << level << std::endl;
            os << "CS" << level << " [style=plaintext]\n{rank=same; CS" << level << " ";
            for (std::multimap<unsigned int, vertex>::const_iterator i = p.first; i != p.second; ++i)
            {
               os << boost::get(boost::vertex_index_t(), *printing_graph)[i->second] << " ";
            }
            os << ";}\n";
         }
         for (unsigned int level = 1; level < sch->get_csteps(); level++)
            os << "CS" << level - 1 << "-> CS" << level << ";\n";
      }

};

void schedule::WriteDot(const std::string & file_name) const
{
   const BehavioralHelperConstRef helper = op_graph->CGetOpGraphInfo()->BH;
   std::string output_directory = parameters->getOption<std::string>(OPT_dot_directory) + "/" + helper->get_function_name() + "/";
   if (!boost::filesystem::exists(output_directory))
      boost::filesystem::create_directories(output_directory);
   const VertexWriterConstRef op_label_writer(new OpWriter(op_graph.get(), 0));
   const EdgeWriterConstRef op_edge_property_writer(new OpEdgeWriter(op_graph.get()));
   const GraphWriterConstRef graph_writer(new ScheduleWriter(op_graph, this));
   op_graph->InternalWriteDot<const OpWriter, const OpEdgeWriter, const ScheduleWriter>(output_directory + file_name, op_label_writer, op_edge_property_writer, graph_writer);
}

void schedule::set_execution(const vertex& op, unsigned int c_step)
{
   op_starting_cycle[op] = c_step;
}

void schedule::set_execution_end(const vertex& op, unsigned int c_step_end)
{
   op_ending_cycle[op] = c_step_end;
}


bool schedule::is_scheduled(const vertex& op) const
{
   return op_starting_cycle.find(op) != op_starting_cycle.end();
}

unsigned int schedule::get_cstep(const vertex& op) const
{
   THROW_ASSERT(is_scheduled(op), "Operation " + GET_NAME(op_graph, op) + " has not been scheduled");
   return op_starting_cycle.find(op)->second;
}

unsigned int schedule::get_cstep_end(const vertex& op) const
{
   THROW_ASSERT(is_scheduled(op), "Operation " + GET_NAME(op_graph, op) + " has not been scheduled");
   return op_ending_cycle.find(op)->second;
}

unsigned int schedule::num_scheduled() const
{
   return static_cast<unsigned int>(op_starting_cycle.size());
}

void schedule::clear()
{
   tot_csteps = 0;
   spec.clear();
   op_starting_cycle.clear();
}

