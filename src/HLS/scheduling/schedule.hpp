/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file schedule.hpp
 * @brief Data structure used to store the schedule of the operations.
 *
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef SCHEDULE_HPP
#define SCHEDULE_HPP

#include "refcount.hpp"
/**
 * @name Forward declarations.
 */
//@{
REF_FORWARD_DECL(behavioral_manager);
CONSTREF_FORWARD_DECL(FunctionBehavior);
CONSTREF_FORWARD_DECL(OpGraph);
CONSTREF_FORWARD_DECL(Parameter);
//@}

#include "graph.hpp"
#include <unordered_map>
#include <iostream>

/**
 * Class managing the schedule of the operations.
 */
class schedule
{
   private:

      /// total number of control steps
      unsigned int tot_csteps;

      /// map between the operation vertex and the clock cycle on which the operation starts its execution
      std::map<vertex, unsigned int> op_starting_cycle;

      /// map between the operation vertex and the clock cycle on which the operations ends its execution
      std::map<vertex, unsigned int> op_ending_cycle;

      /// Map for speculation property of each operation vertex. If true, it means that vertex is speculative executed,
      /// false otherwise
      std::unordered_map<vertex, bool> spec;

      /// slack map
      std::map<vertex, double> op_slack;

      ///The operation graph (for scheduling purpose) (cannot be const because of = operator)
      OpGraphConstRef op_graph;

      ///The set of input parameters
      const ParameterConstRef parameters;

   public:
      /**
       * Constructor.
       */
      schedule(const OpGraphConstRef op_graph, const ParameterConstRef parameters);

      /**
       * Destructor.
       */
      ~schedule();

      /**
       * This method returns the number of control steps.
       * @return the number of control steps of the stored scheduling.
       */
      unsigned int get_csteps() const { return tot_csteps; }

      /**
       * This method sets the number of control steps.
       * @param cs is the number of control steps.
       */
      void set_csteps(unsigned int cs) { tot_csteps = cs; }

      /**
       * Function that prints the class schedule.
       */
      void print(std::ostream& os, const OpGraphConstRef data) const;

      /**
       * Function that writes the dot file of the scheduling by using the AT&T direct graph representation.
       * @param file_name is the file name
       */
      void WriteDot(const std::string& file_name) const;

      /**
       * Sets the speculation map
       */
      void set_spec(const std::unordered_map<vertex, bool>& spec_map) { spec = spec_map; }

      /**
       * Returns the speculation map
       */
      std::unordered_map<vertex,bool> get_spec() const { return spec; }

      /**
       * Sets the starting clock cycle for the given operation
       * @param op the the operation vertex
       * @param c_step is an integer representing the clock cycle where the operation starts the computation
       */
      void set_execution(const vertex& op, unsigned int c_step);

      /**
       * Sets the ending clock cycle for the given operation
       * @param op the the operation vertex
       * @param c_step_end is an integer representing the clock cycle where the operation ends the computation
       */
      void set_execution_end(const vertex& op, unsigned int c_step_end);

      /**
       * Returns true if the given operation has been already scheduled, false otherwise
       */
      bool is_scheduled(const vertex& op) const;

      /**
       * Returns the clock cycle where the given operation has been scheduled
       * @param op is the vertex of the operation
       * @return an integer representing the clock cycle where it starts the execution
       */
      unsigned int get_cstep(const vertex& op) const;

      /**
       * Return the last clock cycle in which the operation execute
       * @param op is the operation
       * @return the last clock cycle
       */
      unsigned int get_cstep_end(const vertex& op) const;

      /**
       * Returns the number of scheduled operations
       */
      unsigned int num_scheduled() const;

      /**
       * Erases the current results
       */
      void clear();

      /**
       * set the slack associated with the vertex with respect to the clock period
       */
      void set_slack(vertex op, double v_slack) {op_slack[op]=v_slack;}

      double get_slack(vertex op) const { if (op_slack.find(op) != op_slack.end()) return op_slack.find(op)->second; else return 0.0;}

};
///Refcount definition of the class
typedef refcount<schedule> scheduleRef;

#endif
