/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file fixed_scheduling.cpp
 * @brief 
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by 
 *
 */
#include "fixed_scheduling.hpp"

#include "hls.hpp"

#include "scheduling.hpp"
#include "Parameter.hpp"

#include "schedule.hpp"
#include "fu_binding.hpp"

#include "fileIO.hpp"
#include "exceptions.hpp"

#include "polixml.hpp"
#include "xml_dom_parser.hpp"

#include <iostream>
#include <string>

#include "dbgPrintHelper.hpp"

fixed_scheduling::fixed_scheduling(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   scheduling(_Param, _HLSMgr, _funId, false)
{
}

fixed_scheduling::~fixed_scheduling()
{

}

void fixed_scheduling::exec()
{
   std::string File = Param->getOption<std::string>("fixed_scheduling_file");

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Reading file: " + File);
   try
   {
      fileIO_istreamRef sname = fileIO_istream_open(File);
      xml_dom_parser parser;
      parser.parse_stream(*sname);
      if (parser)
      {
         //Walk the tree:
         const xml_element* node = parser.get_document()->get_root_node(); //deleted by DomParser.
         HLS->xload(node);
      }
   }
   catch (const char * msg)
   {
      std::cerr << msg << std::endl;
      THROW_ERROR("Error in fixed_scheduling");
   }
   catch (const std::string & msg)
   {
      std::cerr << msg << std::endl;
      THROW_ERROR("Error in fixed_scheduling");
   }
   catch (const std::exception& ex)
   {
      std::cout << "Exception caught: " << ex.what() << std::endl;
      THROW_ERROR("Error in fixed_scheduling");
   }
   catch ( ... )
   {
      std::cerr << "unknown exception" << std::endl;
      THROW_ERROR("Error in fixed_scheduling");
   }
}

std::string fixed_scheduling::get_kind_text() const
{
   return "Fixed scheduling";
}
