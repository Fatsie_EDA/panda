/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file HLS_synthesis_flow.cpp
 * @brief
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */

#include "hls_synthesis_flow.hpp"

///implemented specializations
#include "classical_synthesis_flow.hpp"
#include "xml_config_hls.hpp"

#include "Parameter.hpp"
#include "exceptions.hpp"

HLS_synthesis_flow::HLS_synthesis_flow(const ParameterConstRef _Param, const HLS_managerRef _AppM) :
   Param(_Param),
   AppM(_AppM),
   debug_level(_Param->getOption<int>(OPT_debug_level)),
   output_level(_Param->getOption<unsigned int>(OPT_output_level))
{

}

HLS_synthesis_flow::~HLS_synthesis_flow()
{

}

HLS_synthesis_flowRef HLS_synthesis_flow::createSynthesisFlow(synthesis_t type, const ParameterConstRef Param, const HLS_managerRef AppM)
{
   switch(type)
   {
      case CLASSICAL_SYNTHESIS:
         return HLS_synthesis_flowRef(new HLS_classical_synthesis(Param, AppM));
      case XML_CONFIG:
         return HLS_synthesis_flowRef(new xml_config_hls(Param, AppM));
      default:
         THROW_ERROR("Not implemented synthesis flow");
   }
   return HLS_synthesis_flowRef();
}
