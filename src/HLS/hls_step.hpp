/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file HLS_step.hpp
 * @brief Base class for all HLS algorithms
 * 
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef _HLS_STEP_HPP_
#define _HLS_STEP_HPP_

#include "refcount.hpp"
CONSTREF_FORWARD_DECL(Parameter);
REF_FORWARD_DECL(hls);
REF_FORWARD_DECL(HLS_manager);
REF_FORWARD_DECL(HLS_step);
class xml_element;
#include <string>

class HLS_step
{
   protected:

      ///reference to the class containing all the parameters
      const ParameterConstRef Param;

      ///debug level of the class
      int debug_level;

      ///verbosity level of the class
      unsigned int output_level;

      ///information about all the HLS synthesis
      const HLS_managerRef HLSMgr;

      ///identifier of the function to be processed (0 means that it is a global step)
      unsigned int funId;

      ///HLS datastructure of the function to be analyzed
      const hlsRef HLS;

   public:

      /**
       * Constructor
       * @param Param class containing all the parameters
       * @param HLS class containing all the HLS datastructures
       */
      HLS_step(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor
       */
      virtual ~HLS_step();

      /**
       * Function that computes the HLS step
       */
      virtual void exec() = 0;

      /**
       * Returns an identifier for the solver
       * @return a string that represents the name for the implemented algorithm
       */
      virtual std::string get_kind_text() const = 0;

      /**
       * Returns the HLS datastructure
       */
      hlsRef get_HLS() const;

};
///refcount definition of the class
typedef refcount<HLS_step> HLS_stepRef;

#endif
