/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file chaining.cpp
 * @brief class supporting the chaining optimization in high level synthesis
 * @author Vito Giovanni Castellana <vcastellana@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "chaining.hpp"

#include "sched_based_chaining_computation.hpp"
#if HAVE_EXPERIMENTAL
#include "epdg_sched_based_chaining_computation.hpp"
#endif

#include "hls.hpp"
#include "Parameter.hpp"
#include "exceptions.hpp"
#include "dbgPrintHelper.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"

///STD include
#include <set>
#include <boost/pending/disjoint_sets.hpp>
#include <boost/graph/incremental_components.hpp>
#include <boost/graph/properties.hpp>
#include <boost/pending/disjoint_sets.hpp>
#include <boost/property_map/property_map.hpp>


struct chaining_set
{
      typedef boost::property_map<OpGraph, boost::vertex_index_t>::const_type const_vertex_index_pmap_t;
      typedef boost::iterator_property_map<std::vector<std::size_t>::iterator, boost::identity_property_map, std::vector<std::size_t>::value_type> rank_pmap_type;
      typedef boost::iterator_property_map<std::vector<std::size_t>::iterator, boost::identity_property_map, std::vector<std::size_t>::value_type> pred_pmap_type;

      const_vertex_index_pmap_t cindex_pmap;

      boost::graph_traits<graph>::vertices_size_type n_vert;
      std::vector<std::size_t> rank_map;
      std::vector<std::size_t> pred_map;
      rank_pmap_type rank_pmap;
      pred_pmap_type pred_pmap;
      boost::disjoint_sets<rank_pmap_type,pred_pmap_type> ds;

      chaining_set(const OpGraphConstRef flow_graph) :
         cindex_pmap(boost::get(boost::vertex_index_t(), *flow_graph)),
         n_vert(boost::num_vertices(*flow_graph)),
         rank_map(2*n_vert), pred_map(2*n_vert),
         rank_pmap(rank_map.begin()),
         pred_pmap(pred_map.begin()),
         ds(boost::disjoint_sets<rank_pmap_type,pred_pmap_type>(rank_pmap, pred_pmap))
      {
      }

      std::size_t get_index0(vertex v)
      {
         return cindex_pmap[v]*2;
      }
      std::size_t get_index1(vertex v)
      {
         return cindex_pmap[v]*2+1;
      }
};

chaining::chaining(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
      HLS_step(_Param, _HLSMgr, _funId)
{
   const OpGraphConstRef flow_graph = HLS->FB->CGetOpGraph(FunctionBehavior::FLSAODG);

   chaining_relation = refcount<chaining_set>(new chaining_set(flow_graph));
   VertexIterator vi, vi_end;
   for (boost::tie(vi, vi_end) = boost::vertices(*flow_graph); vi != vi_end; ++vi)
   {
      vertex s = *vi;
      chaining_relation->ds.make_set(chaining_relation->get_index0(s));
      chaining_relation->ds.make_set(chaining_relation->get_index1(s));
   }

}

void chaining::add_chained_vertices_in(vertex op1, vertex src)
{
   chaining_relation->ds.union_set(chaining_relation->get_index0(op1),chaining_relation->get_index1(src));
}

void chaining::add_chained_vertices_out(vertex op1, vertex tgt)
{
   chaining_relation->ds.union_set(chaining_relation->get_index1(op1),chaining_relation->get_index0(tgt));
}

void chaining::set_bb_index(vertex op1, unsigned int bb_index)
{
   actual_bb_index_map[op1] = bb_index;
}
chaining::~chaining()
{

}

chainingRef chaining::factory(algorithm_t algorithm, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   switch(algorithm)
   {
      case SCHED_CHAINING:
         return chainingRef(new sched_based_chaining_computation(Param, HLSMgr, funId));
#if HAVE_EXPERIMENTAL
      case EPDG_SCHED_CHAINING:
         return chainingRef(new epdg_sched_based_chaining_computation(Param, HLSMgr, funId));
#endif
      default:
         THROW_UNREACHABLE("Chaining method not yet supported");
   }
   return chainingRef();
}

chainingRef chaining::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string algorithm;
   if (CE_XVM(algorithm, node)) LOAD_XVM(algorithm, node);
   else algorithm = "SCHED_CHAINING";

   algorithm_t alg_t = SCHED_CHAINING;
   if (algorithm == "SCHED_CHAINING")
      alg_t = SCHED_CHAINING;
#if HAVE_EXPERIMENTAL
   else if (algorithm == "EPDG_SCHED_CHAINING")
      alg_t = EPDG_SCHED_CHAINING;
#endif
   else
      THROW_ERROR("Chaining algorithm \"" + algorithm + "\" currently not supported");
   return factory(alg_t, Param, HLSMgr, funId);
}

bool chaining::is_chained_vertex(vertex v)
{
   return is_chained_with.find(v) != is_chained_with.end();
}

bool chaining::may_be_chained_ops(vertex tgt, vertex src)
{
   return (chaining_relation->ds.find_set(chaining_relation->get_index0(tgt)) == chaining_relation->ds.find_set(chaining_relation->get_index1(src)) || chaining_relation->ds.find_set(chaining_relation->get_index1(tgt)) == chaining_relation->ds.find_set(chaining_relation->get_index0(src)));
}

std::size_t chaining::get_representative_in(vertex op1)
{
   return chaining_relation->ds.find_set(chaining_relation->get_index0(op1));
}

std::size_t chaining::get_representative_out(vertex op1)
{
   return chaining_relation->ds.find_set(chaining_relation->get_index1(op1));
}

unsigned int chaining::get_actual_bb(vertex op1)
{
   THROW_ASSERT(actual_bb_index_map.find(op1) != actual_bb_index_map.end(), "bb index not computed");
   return actual_bb_index_map.find(op1)->second;
}
