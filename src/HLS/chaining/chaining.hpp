/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file chaining.hpp
 * @brief class supporting the chaining optimization in high level synthesis
 * @author Vito Giovanni Castellana <vcastellana@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */
#ifndef CHAINING_HPP
#define CHAINING_HPP

#include "hls_step.hpp"
REF_FORWARD_DECL(chaining);

#include "graph.hpp"
#include "op_graph.hpp"

#include <map>
#include <set>

struct chaining_set;

class chaining : public HLS_step
{
   public:

      typedef enum
      {
         SCHED_CHAINING = 0,
#if HAVE_EXPERIMENTAL
         EPDG_SCHED_CHAINING
#endif
      } algorithm_t;

   private:

      /// relation between vertices in terms of chaining in input or in output
      refcount<chaining_set>chaining_relation;

      /// relation between operation and basic block
      std::map<vertex, unsigned int> actual_bb_index_map;

   protected:

      /// set of vertices chained with something
      std::set<vertex> is_chained_with;

      /**
       * put into relation the vertices whith respect the chained vertices connected with the input
       * @param op1 is the considered vertex
       * @param src is the chained vertex chained in input
       */
      void add_chained_vertices_in(vertex op1, vertex src);

      /**
       * put into relation the vertices whith respect the chained vertices connected with the output
       * @param op1 is the considered vertex
       * @param tgt is the chained vertex chained in output
       */
      void add_chained_vertices_out(vertex op1, vertex tgt);

      /**
       * set the actual bb index of the operation
       * @param op1 is the operation
       */
      void set_bb_index(vertex op1, unsigned int bb_index);

   public:

      /**
       * Constructor
       */
      chaining(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor
       */
      ~chaining();

      /**
       * return true in case the vertex is in chaining with something
       * @param v is the operation
       */
      bool is_chained_vertex(vertex v);

      /**
       * check if two operations are chained in at least one state
       * @param op1 is the first vertex
       * @param op2 is the second vertex
       */
      bool may_be_chained_ops(vertex op1, vertex op2);

      /**
       * Return the representative vertex associated with the chained vertices set in input.
       * It is assumed that chaining define an equivalent relation between vertices.
       * @param op1 is the considered vertex
       * @return the representative vertex
       */
      std::size_t get_representative_in(vertex op1);

      /**
       * Return the representative vertex associated with the chained vertices set in output.
       * It is assumed that chaining define an equivalent relation between vertices.
       * @param op1 is the considered vertex
       * @return the representative vertex
       */
      std::size_t get_representative_out(vertex op1);

      /**
       * return the actual basic block index. Useful in case of speculation.
       * @param op1 is the vertex
       * @return basic block index
       */
      unsigned int get_actual_bb(vertex op1);

      /**
       * Factory method
       */
      static
      chainingRef factory(algorithm_t algorithm, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Factory method from XML file
       */
      static
      chainingRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

};
///refcount definition of the class
typedef refcount<chaining> chainingRef;

#endif
