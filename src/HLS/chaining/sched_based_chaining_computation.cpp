/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file sched_based_chaining_computation.cpp
 * @brief chaining computation starting from the results of the scheduling step
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 */

#include "sched_based_chaining_computation.hpp"

#include "hls.hpp"
#include "function_behavior.hpp"
#include "exceptions.hpp"

///STD include
#include <set>
#include <map>

#include "schedule.hpp"
#include "dbgPrintHelper.hpp"

sched_based_chaining_computation::sched_based_chaining_computation(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   chaining(_Param, _HLSMgr, _funId)
{

}

sched_based_chaining_computation::~sched_based_chaining_computation()
{
}

void sched_based_chaining_computation::exec()
{
   const OpGraphConstRef flow_graph = HLS->FB->CGetOpGraph(FunctionBehavior::FLSAODG);

   VertexIterator op, op_end;
   for(boost::tie(op, op_end) = boost::vertices(*flow_graph); op != op_end; ++op)
   {
      unsigned int current_starting_cycle = HLS->Rsch->get_cstep(*op);
      unsigned int current_ending_cycle = HLS->Rsch->get_cstep_end(*op);
      if(current_starting_cycle == current_ending_cycle)
         add_chained_vertices_in(*op,*op);
      InEdgeIterator ei, ei_end;
      bool is_chained_test = false;
      for(boost::tie(ei, ei_end) = boost::in_edges(*op, *flow_graph); ei != ei_end; ei++)
      {
         vertex src = boost::source(*ei, *flow_graph);
         if(HLS->Rsch->get_cstep_end(src) == current_starting_cycle)
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, std::string("Operations ") + GET_NAME(flow_graph, src) + " and " + GET_NAME(flow_graph, *op) + " are chained in");
            add_chained_vertices_in(*op,src);
            is_chained_test = true;
         }
      }
      if(is_chained_test)
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, std::string("Operations ") +  GET_NAME(flow_graph, *op) + " is chained with something");
         is_chained_with.insert(*op);
      }
      OutEdgeIterator eo, eo_end;
      for(boost::tie(eo, eo_end) = boost::out_edges(*op, *flow_graph); eo != eo_end; eo++)
      {
         vertex tgt = boost::target(*eo, *flow_graph);
         if(HLS->Rsch->get_cstep(tgt) == current_ending_cycle)
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, std::string("Operations ") + GET_NAME(flow_graph, tgt) + " and " + GET_NAME(flow_graph, *op) + " are chained out");
            add_chained_vertices_out(*op,tgt);
         }
      }
   }


}

std::string sched_based_chaining_computation::get_kind_text() const
{
   return "sched_based_chaining_computation";
}
