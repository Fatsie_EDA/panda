/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file export_pcore.cpp
 * @brief Implementation of the class to generate models for the ReSP simulator
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "export_pcore.hpp"

#include "HDL_manager.hpp"

#include "hls.hpp"
#include "hls_manager.hpp"
#include "hls_target.hpp"

#include "tree_helper.hpp"

#include "structural_manager.hpp"
#include "structural_objects.hpp"
#include "target_device.hpp"

#include "boost/filesystem.hpp"
#include "Parameter.hpp"

export_pcore::export_pcore(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId):
   export_core(_Param, _HLSMgr, _funId)
{

}

export_pcore::~export_pcore()
{

}

std::string export_pcore::get_kind_text() const
{
   return "Export Xilinx XPS pcore";
}

void export_pcore::exec()
{
   /// initialize the pcore directory
   if (!boost::filesystem::exists("pcores")) boost::filesystem::create_directories("pcores");

   std::string core_directory = "pcores/" + HLS->top->get_circ()->get_id() + "_v1_00_a";
   /// replace any previous version of the same core
   if (boost::filesystem::exists(core_directory))
   {
      boost::filesystem::remove_all(core_directory);
   }
   boost::filesystem::create_directories(core_directory);

   create_hdl_files(core_directory);

   create_data_files(core_directory);

   std::string include_directory = core_directory + "/include";
   if (!boost::filesystem::exists(include_directory)) boost::filesystem::create_directories(include_directory);

   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Xilinx pcore exported in directory \"" + core_directory + "\"");
}

void export_pcore::create_data_files(const std::string& core_directory)
{
   std::string data_directory = core_directory + "/data";
   if (!boost::filesystem::exists(data_directory)) boost::filesystem::create_directories(data_directory);
   std::string core_name = HLS->top->get_circ()->get_id() + "_v2_1_0";
   create_bbd_file(data_directory, core_name);
   create_mpd_file(data_directory, core_name);
   create_pao_file(data_directory, core_name);
}

void export_pcore::manage_hdl_file(const std::string& file_name)
{
   if (boost::filesystem::extension(file_name) == ".v" || boost::filesystem::extension(file_name) == ".V" || boost::filesystem::extension(file_name) == ".sv" || boost::filesystem::extension(file_name) == ".SV")
      verilog_files.push_back(file_name);
   else if (boost::filesystem::extension(file_name) == ".vhdl" || boost::filesystem::extension(file_name) == ".vhd")
      vhdl_files.push_back(file_name);
   else
      THROW_ERROR("Unsupported file extension");
   hdl_files.push_back(file_name);
}


void export_pcore::create_hdl_files(const std::string& core_directory)
{
   std::string synhdl_directory = core_directory + "/synhdl";
   if (!boost::filesystem::exists(synhdl_directory)) boost::filesystem::create_directories(synhdl_directory);

   HDL_managerRef HM = HDL_managerRef(new HDL_manager(HLS->HLS_T->get_target_device(), Param));
   std::string file_name = Param->getOption<std::string>(OPT_top_file);
   HM->hdl_gen(file_name, HLS->top->get_circ());

   std::string aux_file_string = HM->get_aux_files();
   if (aux_file_string.size() > 0)
   {
      std::vector<std::string> aux_files = convert_string_to_vector<std::string>(aux_file_string, ";");
      for(unsigned int a = 0; a < aux_files.size(); a++)
      {
         manage_hdl_file(aux_files[a]);
      }
   }
   std::string HDL_file_string = HM->get_HDL_files();
   std::vector<std::string> HDL_files = convert_string_to_vector<std::string>(HDL_file_string, ";");
   for(unsigned int f = 0; f < HDL_files.size(); f++)
   {
      manage_hdl_file(HDL_files[f]);
   }

   if (verilog_files.size())
   {
      std::string verilog_directory = core_directory + "/synhdl/verilog";
      if (!boost::filesystem::exists(verilog_directory)) boost::filesystem::create_directories(verilog_directory);
      for(unsigned int v1 = 0; v1 < verilog_files.size(); v1++)
      {
         std::string new_location = verilog_directory + "/" + verilog_files[v1];
         boost::filesystem::rename(verilog_files[v1], new_location);
         verilog_files[v1] = new_location;
      }
   }

   if (vhdl_files.size())
   {
      std::string vhdl_directory = core_directory + "/synhdl/vhdl";
      if (!boost::filesystem::exists(vhdl_directory)) boost::filesystem::create_directories(vhdl_directory);
      for(unsigned int v1 = 0; v1 < vhdl_files.size(); v1++)
      {
         std::string new_location = vhdl_directory + "/" + vhdl_files[v1];
         boost::filesystem::rename(vhdl_files[v1], new_location);
         vhdl_files[v1] = new_location;
      }
   }

}

void export_pcore::write_header(std::ostringstream& oss)
{
   oss << "##" << std::endl;
   oss << "## File automatically generated by bambu" << std::endl;
   oss << "## Copyright (C) 2004-2014 Politecnico di Milano." << std::endl;
   oss << "##" << std::endl << std::endl;
}

void export_pcore::create_bbd_file(const std::string& data_directory, const std::string& core_name)
{
   std::ostringstream oss;
   write_header(oss);

   std::string filename = data_directory + "/" + core_name + ".bbd";
   std::ofstream out(filename.c_str());
   out << oss.str();
   out.close();
}

void export_pcore::create_mpd_file(const std::string& data_directory, const std::string& core_name)
{
   std::ostringstream oss;
   write_header(oss);
   oss << "BEGIN " << HLS->top->get_circ()->get_id() << std::endl << std::endl;

   oss << "## Peripheral Options" << std::endl;
   oss << "OPTION IPTYPE = PERIPHERAL" << std::endl;
   oss << "OPTION IMP_NETLIST = TRUE" << std::endl;
   oss << "OPTION HDL = VERILOG" << std::endl;
   target_deviceRef device = HLS->HLS_T->get_target_device();
   std::string family = device->get_parameter<std::string>("family");
   boost::to_lower(family);
   oss << "OPTION ARCH_SUPPORT_MAP = (" << family << "=DEVELOPMENT)" << std::endl;
   oss << "OPTION IP_GROUP = MICROBLAZE:PPC:USER" << std::endl;
   oss << "OPTION DESC = " << HLS->top->get_circ()->get_id() << std::endl << std::endl;

   structural_objectRef obj = HLS->top->get_circ();

   std::set<std::string> bundle;
   structural_objectRef pclk = obj->find_member("aclk", port_o_K, obj);
   for(unsigned int p = 0; p < GetPointer<port_o>(pclk)->get_connections_size(); p++)
   {
      const structural_objectRef port = GetPointer<port_o>(pclk)->get_connection(p);
      if (GetPointer<port_o>(port)->get_bus_bundle().size() == 0) continue;
      bundle.insert(GetPointer<port_o>(port)->get_bus_bundle());
   }
   std::string bus_bundle;
   for(std::set<std::string>::iterator b = bundle.begin(); b != bundle.end(); b++)
   {
      if (bus_bundle.size()) bus_bundle += ":";
      bus_bundle += *b;
   }
   oss << "PORT aclk = \"\", DIR = I, SIGIS = Clk, BUS = " << bus_bundle << ", ASSIGNMENT = REQUIRE" << std::endl;
   oss << "PORT aresetn = ARESETN, DIR = I, SIGIS = Rst, BUS = " << bus_bundle << ", ASSIGNMENT = REQUIRE" << std::endl << std::endl;

   for(std::set<std::string>::iterator b = bundle.begin(); b != bundle.end(); b++)
   {
      bool is_slave = true;
      for (unsigned int i = 0; i < GetPointer<module>(obj)->get_out_port_size(); i++)
      {
         const structural_objectRef port = GetPointer<module>(obj)->get_out_port(i);
         bool is_in_bundle = false;
         for(unsigned int p = 0; p < GetPointer<port_o>(port)->get_connections_size(); p++)
         {
            const structural_objectRef cport = GetPointer<port_o>(port)->get_connection(p);
            if (GetPointer<port_o>(cport)->get_bus_bundle() != *b) continue;
            is_in_bundle = true;
         }
         if (!is_in_bundle) continue;
         if (GetPointer<port_o>(port)->get_is_master()) is_slave = false;
      }

      oss << "BUS_INTERFACE BUS = " << *b << ", BUS_STD = AXI, BUS_TYPE = " << (is_slave ? "SLAVE" : "MASTER") << std::endl;
      oss << "PARAMETER C_" << *b << "_BASEADDR = 0xffffffff, DT = std_logic_vector(31 downto 0), ADDR_TYPE = REGISTER, ASSIGNMENT = REQUIRE, ";
      oss << "PAIR = C_" << *b << "_HIGHADDR, ADDRESS = BASE, MIN_SIZE = 0x1000, TYPE = NON_HDL, BUS = " << *b << std::endl;
      oss << "PARAMETER C_" << *b << "_HIGHADDR = 0x00000000, DT = std_logic_vector(31 downto 0), ADDR_TYPE = REGISTER, ASSIGNMENT = REQUIRE, ";
      oss << "PAIR = C_" << *b << "_BASEADDR, ADDRESS = HIGH, TYPE = NON_HDL, BUS = " << *b << std::endl;
      oss << "PARAMETER C_" << *b << "_ADDR_WIDTH = 32, DT = INTEGER, ASSIGNMENT = CONSTANT, BUS = " << *b << std::endl;
      oss << "PARAMETER C_" << *b << "_DATA_WIDTH = 32, DT = INTEGER, ASSIGNMENT = CONSTANT, BUS = " << *b << std::endl;
      oss << "PARAMETER C_" << *b << "_PROTOCOL = AXI4LITE, DT = STRING, ASSIGNMENT = CONSTANT, TYPE = NON_HDL, BUS = " << *b << std::endl;
      for (unsigned int i = 0; i < GetPointer<module>(obj)->get_in_port_size(); i++)
      {
         const structural_objectRef port = GetPointer<module>(obj)->get_in_port(i);
         const std::string& id = port->get_id();
         if (id == "aclk" || id == "aresetn") continue;
         bool is_in_bundle = false;
         for(unsigned int p = 0; p < GetPointer<port_o>(port)->get_connections_size(); p++)
         {
            const structural_objectRef cport = GetPointer<port_o>(port)->get_connection(p);
            if (GetPointer<port_o>(cport)->get_bus_bundle() != *b) continue;
            is_in_bundle = true;
         }
         if (!is_in_bundle) continue;
         std::string trimmed = id.substr(id.find_last_of("_")+1, id.size());
         oss << "PORT " << id << " = " << trimmed << ", DIR = I, ";
         const structural_type_descriptorRef type = GetPointer<port_o>(port)->get_typeRef();
         unsigned int size = type->size*type->vector_size;
         if (size > 1)
            oss << "VEC = [" << (size-1) << ":0], ";
         if (GetPointer<port_o>(port)->get_port_endianess() == port_o::LITTLE)
            oss << "ENDIAN = LITTLE, ";
         else if (GetPointer<port_o>(port)->get_port_endianess() == port_o::BIG)
            oss << "ENDIAN = BIG, ";
         oss << "BUS = " << *b << std::endl;
      }
      for (unsigned int i = 0; i < GetPointer<module>(obj)->get_out_port_size(); i++)
      {
         const structural_objectRef port = GetPointer<module>(obj)->get_out_port(i);
         const std::string& id = port->get_id();
         bool is_in_bundle = false;
         for(unsigned int p = 0; p < GetPointer<port_o>(port)->get_connections_size(); p++)
         {
            const structural_objectRef cport = GetPointer<port_o>(port)->get_connection(p);
            if (GetPointer<port_o>(cport)->get_bus_bundle() != *b) continue;
            is_in_bundle = true;
         }
         if (!is_in_bundle) continue;
         if (id == "interrupt")
         {
            oss << "PORT interrupt = \"\", DIR = O, SIGIS = INTERRUPT, SENSITIVITY = LEVEL_HIGH, INTERRUPT_PRIORITY = MEDIUM" << std::endl;
            continue;
         }
         std::string trimmed = id.substr(id.find_last_of("_")+1, id.size());
         oss << "PORT " << id << " = " << trimmed << ", DIR = O, ";
         const structural_type_descriptorRef type = GetPointer<port_o>(port)->get_typeRef();
         unsigned int size = type->size*type->vector_size;
         if (size > 1)
            oss << "VEC = [" << (size-1) << ":0], ";
         if (GetPointer<port_o>(port)->get_port_endianess() == port_o::LITTLE)
            oss << "ENDIAN = LITTLE, ";
         else if (GetPointer<port_o>(port)->get_port_endianess() == port_o::BIG)
            oss << "ENDIAN = BIG, ";
         oss << "BUS = " << *b << std::endl;

      }
      oss << std::endl;
   }

   oss << "END" << std::endl;
   std::string filename = data_directory + "/" + core_name + ".mpd";
   std::ofstream out(filename.c_str());
   out << oss.str();
   out.close();
}

void export_pcore::create_pao_file(const std::string& data_directory, const std::string& core_name)
{
   std::ostringstream oss;
   write_header(oss);

   std::string core_str = HLS->top->get_circ()->get_id() + "_v1_00_a";
   for(unsigned int f = 0; f < hdl_files.size(); f++)
   {
      oss << "synlib " << core_str << " " << boost::filesystem::basename(verilog_files[f]) << " ";
      if (boost::filesystem::extension(hdl_files[f]) == ".v")
         oss << "verilog";
      else if (boost::filesystem::extension(hdl_files[f]) == ".vhdl")
         oss << "vhdl";
      oss << std::endl;
   }

   std::string filename = data_directory + "/" + core_name + ".pao";
   std::ofstream out(filename.c_str());
   out << oss.str();
   out.close();
}
