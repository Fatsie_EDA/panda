/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file export_core.cpp
 * @brief Implementation of the basic methods to export cores
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "export_core.hpp"

#include "export_pcore.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"
#include "Parameter.hpp"

export_core::export_core(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId):
   HLS_step(_Param, _HLSMgr, _funId)
{

}

export_core::~export_core()
{

}

export_coreRef export_core::factory(export_t type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   switch(type)
   {
      case PCORE:
         return export_coreRef(new export_pcore(Param, HLSMgr, funId));
      default:
         THROW_ERROR("Not supported exporting");
   }
   return export_coreRef();
}

export_coreRef export_core::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string type;
   if (CE_XVM(type, node)) LOAD_XVM(type, node);
   else type = "PCORE";

   export_t type_t = PCORE;
   if (type == "PCORE")
      type_t = PCORE;
   else
      THROW_ERROR("Export mode \"" + type + "\" currently not supported");
   return factory(type_t, Param, HLSMgr, funId);
}
