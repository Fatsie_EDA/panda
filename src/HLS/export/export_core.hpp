/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file export_pcore.hpp
 * @brief Generic class used to export the generated accelerator
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef EXPORT_CORE_HPP
#define EXPORT_CORE_HPP

///superclass include
#include "hls_step.hpp"
REF_FORWARD_DECL(export_core);

class export_core : public HLS_step
{
   public:

      ///implemented export
      typedef enum
      {
         PCORE = 0
      } export_t;

   public:

      /**
       * Constructor.
       */
      export_core(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      ~export_core();

      /**
       * Factory method
       */
      static
      export_coreRef factory(export_t type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Factory method from XML file
       */
      static
      export_coreRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

};

#endif
