/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file export_pcore.hpp
 * @brief Class used to generate pcore datastructure to import the accelerator in Xilinx XPS
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef EXPORT_PCORE_HPP
#define EXPORT_PCORE_HPP

///superclass include
#include "export_core.hpp"

#include <vector>

class export_pcore : public export_core
{
      /**
       * Creates the 'data' files
       */
      void create_data_files(const std::string& core_directory);

      /**
       * Creates the 'synhdl' files
       */
      void create_hdl_files(const std::string& core_directory);

      /**
       * Creates the .bbd file
       */
      void create_bbd_file(const std::string& data_directory, const std::string& core_name);

      /**
       * Creates the .mpd file
       */
      void create_mpd_file(const std::string& data_directory, const std::string& core_name);

      /**
       * Creates the .pao file
       */
      void create_pao_file(const std::string& data_directory, const std::string& core_name);

      /**
       * Writes file header
       */
      void write_header(std::ostringstream& oss);

      /**
       * Manages the given HDL file
       */
      void manage_hdl_file(const std::string& file_name);

      ///list of generated verilog files
      std::vector<std::string> verilog_files;
      ///list of generated VHDL files
      std::vector<std::string> vhdl_files;
      ///ordered list of files
      std::vector<std::string> hdl_files;

   public:

      /**
       * Constructor.
       */
      export_pcore(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      ~export_pcore();

      /**
       * Generates the pcore datastructure
       */
      void exec();

      /**
       * Returns the name of the implemented step
       */
      std::string get_kind_text() const;

};

#endif
