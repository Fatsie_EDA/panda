/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file hls_manager.cpp
 * @brief Data structure containing all the information for HLS.
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "hls_manager.hpp"

#include "hls.hpp"
#include "hls_constraints.hpp"
#include "hls_target.hpp"
#include "memory.hpp"

#include "BackendFlow.hpp"

#include "ext_tree_node.hpp"

#include "call_graph_manager.hpp"

#include "function_behavior.hpp"
#include "behavioral_helper.hpp"
#include "tree_helper.hpp"
#include "tree_manager.hpp"
#include "tree_reindex.hpp"

#include "xml_helper.hpp"
#include "polixml.hpp"
#include "xml_dom_parser.hpp"

#include "op_graph.hpp"

HLS_manager::HLS_manager(const ParameterConstRef _Param, const HLS_targetRef _HLS_T) :
   application_manager(FunctionExpanderConstRef(new FunctionExpander()), true, false, _Param),
   HLS_T(_HLS_T),
   base_address(0),
   HLS_execution_time(0)
{

}

HLS_manager::~HLS_manager()
{

}

hlsRef HLS_manager::get_HLS(unsigned int funId) const
{
   if (!funId) return hlsRef();
   THROW_ASSERT(hlsMap.find(funId) != hlsMap.end(), "missing datastructure for function @" + STR(funId));
   return hlsMap.find(funId)->second;
}

HLS_targetRef HLS_manager::get_HLS_target() const
{
   return HLS_T;
}

hlsRef HLS_manager::create_HLS(const HLS_managerRef HLSMgr, unsigned int functionId)
{
   THROW_ASSERT(functionId, "No function");
   const std::string function_name = tree_helper::name_function(HLSMgr->get_tree_manager(), functionId);
   HLS_constraintsRef HLS_C = HLS_constraintsRef(new HLS_constraints(HLSMgr->get_parameter(), function_name));
   const std::deque<vertex>& OperationsList = HLSMgr->CGetFunctionBehavior(functionId)->get_levels();
   OpVertexSet Operations(HLSMgr->CGetFunctionBehavior(functionId)->CGetOpGraph(FunctionBehavior::CFG));
   Operations.insert(OperationsList.begin(), OperationsList.end());
   ///creates the new HLS datastructure associated with the function
   return HLSMgr->hlsMap[functionId] = hlsRef(new hls(HLSMgr->get_parameter(), HLSMgr, functionId, Operations, HLSMgr->get_HLS_target(), HLS_C));
}

std::string HLS_manager::get_constant_string(unsigned int node, unsigned int precision)
{
   std::string trimmed_value;
   if(tree_helper::is_real(TM, tree_helper::get_type_index(TM, node)))
   {
      THROW_ASSERT(tree_helper::size(TM, tree_helper::get_type_index(TM, node)) == precision, "real precision mistmatch");
      tree_nodeRef rc_node = TM->get_tree_node_const(node);
      real_cst *rc = GetPointer<real_cst>(rc_node);
      std::string C_value = rc->valr;
      if(C_value == "Inf") C_value = rc->valx;
      trimmed_value = convert_fp_to_string(C_value, precision);
   }
   else if(tree_helper::is_a_vector(TM, tree_helper::get_type_index(TM, node)))
   {
      tree_nodeRef vc_node = TM->get_tree_node_const(node);
      vector_cst *vc = GetPointer<vector_cst>(vc_node);
      unsigned int n_elm = static_cast<unsigned int>(vc->list_of_valu.size());
      unsigned int elm_prec = precision/n_elm;
      trimmed_value = "";
      for (unsigned int i = 0; i < n_elm; ++i)
         trimmed_value = get_constant_string(GET_INDEX_NODE(vc->list_of_valu[i]), elm_prec) + trimmed_value;
   }
   else
   {
      tree_nodeRef ic_node = TM->get_tree_node_const(node);
      integer_cst *ic = GetPointer<integer_cst>(ic_node);
      unsigned long long int ull_value = static_cast<unsigned long long int>(tree_helper::get_integer_cst_value(ic));
      trimmed_value = convert_to_binary(ull_value, precision);
   }
   return trimmed_value;
}

const BackendFlowRef HLS_manager::get_backend_flow()
{
   if (!back_flow)
      back_flow = BackendFlow::CreateFlow(Param, "Synthesis", HLS_T);;
   return back_flow;
}

void HLS_manager::xwrite(const std::string& filename)
{
   try
   {
      xml_document document;
      xml_element* nodeRoot = document.create_root_node("HLS");
      Rmem->xwrite(nodeRoot);
      std::list<unsigned int> called_functions;
      call_graph_manager->GetReachedBodyFunctions(called_functions);
      const unsigned int top_function = call_graph_manager->GetRootFunction();
      hlsRef HLS = hlsMap[top_function];
      HLS->xwrite(nodeRoot);
      document.write_to_file_formatted(filename);
   }
   catch (const char * msg)
   {
      std::cerr << msg << std::endl;
   }
   catch (const std::string & msg)
   {
      std::cerr << msg << std::endl;
   }
   catch (const std::exception& ex)
   {
      std::cout << "Exception caught: " << ex.what() << std::endl;
   }
   catch ( ... )
   {
      std::cerr << "unknown exception" << std::endl;
   }
}

std::vector<HLS_manager::io_binding_type> HLS_manager::get_required_values(unsigned int fun_id, const vertex& v) const
{
   const OpGraphConstRef cfg = CGetFunctionBehavior(fun_id)->CGetOpGraph(FunctionBehavior::CFG);
   const unsigned int node_id = cfg->CGetOpNodeInfo(v)->node_id;
   std::vector<io_binding_type> required;
   if (node_id)
      get_required_values(required, TM->get_tree_node_const(node_id));
   return required;
}

void HLS_manager::get_required_values(std::vector<io_binding_type>& required, const tree_nodeRef& tn, unsigned int index) const
{
   switch (tn->get_kind())
   {
      case constructor_K:
      {
         constructor * co = GetPointer<constructor>(tn);
         if(tree_helper::is_a_vector(TM, GET_INDEX_NODE(co->type)))
         {
            std::vector<std::pair< tree_nodeRef, tree_nodeRef> >::const_iterator vend = co->list_of_idx_valu.end();
            for (std::vector<std::pair< tree_nodeRef, tree_nodeRef> >::const_iterator i = co->list_of_idx_valu.begin(); i != vend; i++)
               required.push_back(io_binding_type(GET_INDEX_NODE(i->second),0));
         }
         else
            required.push_back(io_binding_type(index,0));
         break;
      }
      case ssa_name_K:
      case string_cst_K:
      case real_cst_K:
      case integer_cst_K:
      case vector_cst_K:
      case var_decl_K:
      case parm_decl_K:
      {
         required.push_back(io_binding_type(index,0));
         break;
      }
      case gimple_while_K:
      {
         const gimple_while * we = GetPointer<const gimple_while>(tn);
         get_required_values(required, GET_NODE(we->op0), GET_INDEX_NODE(we->op0));
         break;
      }
      case CASE_BINARY_EXPRESSION:
      {
         binary_expr* be = GetPointer<binary_expr>(tn);
         get_required_values(required, GET_NODE(be->op0), GET_INDEX_NODE(be->op0));
         if(tn->get_kind() != assert_expr_K)
            get_required_values(required, GET_NODE(be->op1), GET_INDEX_NODE(be->op1));
         break;
      }
      case CASE_UNARY_EXPRESSION:
      {
         unary_expr* ue = GetPointer<unary_expr>(tn);
         if(tn->get_kind() == addr_expr_K || tn->get_kind() == imagpart_expr_K || tn->get_kind() == realpart_expr_K)
         {
            required.push_back(io_binding_type(index,0));
         }
         else
            get_required_values(required, GET_NODE(ue->op), GET_INDEX_NODE(ue->op));
         break;
      }
      case cond_expr_K:
      {
         cond_expr* ce = GetPointer<cond_expr>(tn);
         get_required_values(required, GET_NODE(ce->op0), GET_INDEX_NODE(ce->op0));
         get_required_values(required, GET_NODE(ce->op1), GET_INDEX_NODE(ce->op1));
         get_required_values(required, GET_NODE(ce->op2), GET_INDEX_NODE(ce->op2));
         break;
      }
      case vec_cond_expr_K:
      {
         vec_cond_expr* vce = GetPointer<vec_cond_expr>(tn);
         get_required_values(required, GET_NODE(vce->op0), GET_INDEX_NODE(vce->op0));
         get_required_values(required, GET_NODE(vce->op1), GET_INDEX_NODE(vce->op1));
         get_required_values(required, GET_NODE(vce->op2), GET_INDEX_NODE(vce->op2));
         break;
      }
      case vec_perm_expr_K:
      {
         vec_perm_expr* vpe = GetPointer<vec_perm_expr>(tn);
         get_required_values(required, GET_NODE(vpe->op0), GET_INDEX_NODE(vpe->op0));
         get_required_values(required, GET_NODE(vpe->op1), GET_INDEX_NODE(vpe->op1));
         get_required_values(required, GET_NODE(vpe->op2), GET_INDEX_NODE(vpe->op2));
         break;
      }
      case dot_prod_expr_K:
      {
         dot_prod_expr* dpe = GetPointer<dot_prod_expr>(tn);
         get_required_values(required, GET_NODE(dpe->op0), GET_INDEX_NODE(dpe->op0));
         get_required_values(required, GET_NODE(dpe->op1), GET_INDEX_NODE(dpe->op1));
         get_required_values(required, GET_NODE(dpe->op2), GET_INDEX_NODE(dpe->op2));
         break;
      }
      case gimple_cond_K:
      {
         gimple_cond* gc = GetPointer<gimple_cond>(tn);
         get_required_values(required, GET_NODE(gc->op0), GET_INDEX_NODE(gc->op0));
         break;
      }
      case gimple_switch_K:
      {
         gimple_switch* se = GetPointer<gimple_switch>(tn);
         get_required_values(required, GET_NODE(se->op0), GET_INDEX_NODE(se->op0));
         break;
      }
      case gimple_multi_way_if_K:
      {
         gimple_multi_way_if* gmwi = GetPointer<gimple_multi_way_if>(tn);
         const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it_end = gmwi->list_of_cond.end();
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it = gmwi->list_of_cond.begin(); gmwi_it != gmwi_it_end; ++gmwi_it)
            if((*gmwi_it).first)
               get_required_values(required, GET_NODE((*gmwi_it).first), GET_INDEX_NODE((*gmwi_it).first));
         break;
      }
      case array_ref_K:
      {
         array_ref* ar = GetPointer<array_ref>(tn);
         required.push_back(io_binding_type(GET_INDEX_NODE(ar->op0),0));
         get_required_values(required, GET_NODE(ar->op1), GET_INDEX_NODE(ar->op1));
         break;
      }
      case target_mem_ref_K:
      {
         target_mem_ref* tmr = GetPointer<target_mem_ref>(tn);
         if(tmr->symbol)
            required.push_back(io_binding_type(GET_INDEX_NODE(tmr->symbol),0));
         else
            required.push_back(io_binding_type(0,0));
         if(tmr->base)
            required.push_back(io_binding_type(GET_INDEX_NODE(tmr->base),0));
         else
            required.push_back(io_binding_type(0,0));
         if(tmr->idx)
            required.push_back(io_binding_type(GET_INDEX_NODE(tmr->idx),0));
         else
            required.push_back(io_binding_type(0,0));
         if(tmr->step)
            required.push_back(io_binding_type(GET_INDEX_NODE(tmr->step),0));
         else
            required.push_back(io_binding_type(0,0));
         if(tmr->offset)
            required.push_back(io_binding_type(GET_INDEX_NODE(tmr->offset),0));
         else
            required.push_back(io_binding_type(0,0));
         break;
      }
      case target_mem_ref461_K:
      {
         target_mem_ref461* tmr = GetPointer<target_mem_ref461>(tn);
         if(tmr->base)
            required.push_back(io_binding_type(GET_INDEX_NODE(tmr->base),0));
         else
            required.push_back(io_binding_type(0,0));
         if(tmr->idx)
            required.push_back(io_binding_type(GET_INDEX_NODE(tmr->idx),0));
         else
            required.push_back(io_binding_type(0,0));
         if(tmr->step)
            required.push_back(io_binding_type(GET_INDEX_NODE(tmr->step),0));
         else
            required.push_back(io_binding_type(0,0));
         if(tmr->idx2)
            required.push_back(io_binding_type(GET_INDEX_NODE(tmr->idx2),0));
         else
            required.push_back(io_binding_type(0,0));
         if(tmr->offset)
            required.push_back(io_binding_type(GET_INDEX_NODE(tmr->offset),0));
         else
            required.push_back(io_binding_type(0,0));
         break;
      }
      case gimple_assign_K:
      {
         gimple_assign* gm = GetPointer<gimple_assign>(tn);
         if(!gm->init_assignment && !gm->clobber)
         {
            tree_nodeRef op0 = GET_NODE(gm->op0);
            tree_nodeRef op1 = GET_NODE(gm->op1);

            if (op0->get_kind() == component_ref_K || op0->get_kind() == indirect_ref_K || op0->get_kind() == misaligned_indirect_ref_K  || op0->get_kind() == mem_ref_K || op0->get_kind() == array_ref_K ||
                op0->get_kind() == target_mem_ref_K || op0->get_kind() == target_mem_ref461_K || op0->get_kind() ==  bit_field_ref_K)
            {
               get_required_values(required, op1, GET_INDEX_NODE(gm->op1));
               get_required_values(required, op0, GET_INDEX_NODE(gm->op0));
            }
            else
            {
               bool is_a_vector_bitfield = false;
               if(op1->get_kind() == bit_field_ref_K)
               {
                  bit_field_ref* bfr = GetPointer<bit_field_ref>(op1);
                  if(tree_helper::is_a_vector(TM, GET_INDEX_NODE(bfr->op0)))
                  is_a_vector_bitfield = true;
               }
               if (op1->get_kind() == component_ref_K || op1->get_kind() == indirect_ref_K || op1->get_kind() == misaligned_indirect_ref_K || op1->get_kind() == mem_ref_K || op1->get_kind() == array_ref_K ||
                   op1->get_kind() == target_mem_ref_K || op1->get_kind() == target_mem_ref461_K || (op1->get_kind() == bit_field_ref_K && !is_a_vector_bitfield))
                  required.push_back(io_binding_type(0,0));
               get_required_values(required, op1, GET_INDEX_NODE(gm->op1));
            }
         }
         break;
      }
      case gimple_return_K:
      {
         gimple_return* rt = GetPointer<gimple_return>(tn);
         if (rt->op) get_required_values(required, GET_NODE(rt->op), GET_INDEX_NODE(rt->op));
         break;
      }
      case gimple_phi_K:
      {
         gimple_phi* gp = GetPointer<gimple_phi>(tn);
         std::vector<std::pair< tree_nodeRef, unsigned int> >::const_iterator lde_it_end = gp->list_of_def_edge.end();
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::const_iterator lde_it = gp->list_of_def_edge.begin(); lde_it != lde_it_end; ++lde_it)
            required.push_back(io_binding_type(GET_INDEX_NODE(lde_it->first),0));
         break;
      }
      case label_decl_K:
      case gimple_label_K:
      case gimple_goto_K:
      case gimple_nop_K:
      case CASE_PRAGMA_NODES:
      {
         ///this has not to be synthesized
         break;
      }
      case call_expr_K:
      {
         call_expr* ce = GetPointer<call_expr>(tn);
         const std::vector<tree_nodeRef> & args = ce->args;
         std::vector<tree_nodeRef>::const_iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            required.push_back(io_binding_type(GET_INDEX_NODE(*arg), 0));
         }
         break;
      }
      case gimple_call_K:
      {
         gimple_call* ce = GetPointer<gimple_call>(tn);
         function_decl * fd = nullptr;
         tree_nodeRef temp_node = GET_NODE(ce->fn);
         if(temp_node->get_kind() == addr_expr_K)
         {
            unary_expr* ue = GetPointer<unary_expr>(temp_node);
            temp_node = ue->op;
            fd = GetPointer<function_decl>(GET_NODE(temp_node));
         }
         else if(temp_node->get_kind() == obj_type_ref_K)
         {
            temp_node = tree_helper::find_obj_type_ref_function(ce->fn);
            fd = GetPointer<function_decl>(GET_NODE(temp_node));
         }
         if(!fd || !tree_helper::is_a_nop_function_decl(fd))
         {
            const std::vector<tree_nodeRef> & args = ce->args;
            std::vector<tree_nodeRef>::const_iterator arg, arg_end = args.end();
            for(arg = args.begin(); arg != arg_end; arg++)
            {
               required.push_back(io_binding_type(GET_INDEX_NODE(*arg), 0));
            }
         }
         break;
      }
      case tree_list_K:
      {
         tree_list* tl = GetPointer<tree_list>(tn);
         required.push_back(io_binding_type(GET_INDEX_NODE(tl->valu),0));
         if (tl->chan) get_required_values(required, GET_NODE(tl->chan), GET_INDEX_NODE(tl->chan));
         break;
      }
      case component_ref_K:
      {
         component_ref* cr = GetPointer<component_ref>(tn);
         required.push_back(io_binding_type(GET_INDEX_NODE(cr->op0),0));
         get_required_values(required, GET_NODE(cr->op1), GET_INDEX_NODE(cr->op1));
         break;
      }
      case bit_field_ref_K:
      {
         bit_field_ref* bfr = GetPointer<bit_field_ref>(tn);
         required.push_back(io_binding_type(GET_INDEX_NODE(bfr->op0),0));
         get_required_values(required, GET_NODE(bfr->op1), GET_INDEX_NODE(bfr->op1));
         get_required_values(required, GET_NODE(bfr->op2), GET_INDEX_NODE(bfr->op2));
         break;
      }
      case field_decl_K:
      {
         field_decl* fd = GetPointer<field_decl>(tn);
         integer_cst *ic = GetPointer<integer_cst>(GET_NODE(fd->bpos));
         THROW_ASSERT(ic, "non-constant field offset (variable lenght object) currently not supported: " + STR(GET_INDEX_NODE(fd->bpos)));
         unsigned long long int ull_value = static_cast<unsigned long long int>(tree_helper::get_integer_cst_value(ic));
         required.push_back(io_binding_type(0, static_cast<unsigned int>(ull_value/8))); /// bpos has an offset in bits
         if(ull_value%8 != 0)
            THROW_ERROR("bitfields are not yet supported");
         break;
      }
      case gimple_asm_K:
      {
         gimple_asm * ga = GetPointer<gimple_asm>(tn);
         if(ga->in) get_required_values(required, GET_NODE(ga->in), GET_INDEX_NODE(ga->in));
         break;
      }
      case array_range_ref_K:
      case binfo_K:
      case block_K:
      case case_label_expr_K:
      case complex_cst_K:
      case const_decl_K:
      case function_decl_K:
      case identifier_node_K:
      case namespace_decl_K:
      case result_decl_K:
      case statement_list_K:
      case target_expr_K:
      case translation_unit_decl_K:
      case tree_vec_K:
      case type_decl_K:
      case CASE_CPP_NODES:
      case CASE_FAKE_NODES:
      case CASE_TYPE_NODES:
      case vtable_ref_K:
      case with_cleanup_expr_K:
      case obj_type_ref_K:
      case save_expr_K:
      case gimple_bind_K:
      case gimple_for_K:
      case gimple_pragma_K:
      case gimple_predict_K:
      case gimple_resx_K:
      {
         THROW_ERROR("Operation not yet supported: " + std::string(tn->get_kind_text()));
      }
      default:
         THROW_UNREACHABLE("");
   }
}
