/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file fu_binding_creator.hpp
 * @brief Base class for all module binding algorithms.
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef _FU_BINDING_CREATOR_HPP
#define _FU_BINDING_CREATOR_HPP

///superclass include
#include "hls_step.hpp"
REF_FORWARD_DECL(fu_binding_creator);

/**
 * Generic class managing module binding algorithms.
*/
class fu_binding_creator : public HLS_step
{
    public:

      ///Module binding algorithms
      typedef enum
      {
         CDFC_TTT_FAST = 0,
         CDFC_TTT_FAST2,
         CDFC_TTT_FULL,
         CDFC_TTT_FULL2,
         CDFC_TS,
         CDFC_WEIGHTED_TS,
         CDFC_COLORING,
         CDFC_WEIGHTED_COLORING,
         CDFC_BIPARTITE_MATCHING,
         UNIQUE
      } type_t;

      /**
       * Constructor.
       */
      fu_binding_creator(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      virtual ~fu_binding_creator();

      /**
        * Factory method
        */
      static
      fu_binding_creatorRef factory(type_t type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
        * Factory method based on XML node
        */
      static
      fu_binding_creatorRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

};
///refcount definition of the class
typedef refcount<fu_binding_creator> fu_binding_creatorRef;

#endif
