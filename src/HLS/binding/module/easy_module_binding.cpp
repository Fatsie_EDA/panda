   /*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file easy_module_binding.cpp
 * @brief Class implementation of the partial module binding based on simple conditions.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "easy_module_binding.hpp"

#include "hls_manager.hpp"
#include "function_behavior.hpp"

#include "allocation.hpp"
#include "hls.hpp"
#include "fu_binding.hpp"
#include "graph.hpp"
#include "op_graph.hpp"

#include "Parameter.hpp"
#include "utility.hpp"
#include "dbgPrintHelper.hpp"

#include <iostream>
#include <map>
#include <string>


easy_module_binding::easy_module_binding(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   HLS_step(_Param, _HLSMgr, _funId)
{
   if (!HLS->Rfu) HLS->Rfu = fu_bindingRef(new fu_binding(HLS->ALL, HLSMgr->get_tree_manager()));
}

easy_module_binding::~easy_module_binding()
{

}

void easy_module_binding::exec()
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Starting partial module binding based on simple conditions");

   // resource binding and allocation  info
   fu_binding& fu = *(HLS->Rfu);
   allocation& all = *(HLS->ALL);
   // pointer to a Control, Data dependence and antidependence graph graph
   const OpGraphConstRef sdg = HLS->FB->CGetOpGraph(FunctionBehavior::SDG);

   VertexIterator vIt, vItEnd;
   unsigned int fu_unit;
   /// compute unshared resources
   std::map<unsigned int, unsigned int> n_shared_fu;
   for(boost::tie(vIt, vItEnd) = boost::vertices(*sdg); vIt != vItEnd; vIt++)
   {
      fu_unit = fu.get_assign(*vIt);
      if(all.is_vertex_bounded(fu_unit) || (all.get_number_channels(fu_unit) == 1 && all.is_memory_unit(fu_unit))) continue;
      if(n_shared_fu.find(fu_unit) == n_shared_fu.end())
         n_shared_fu[fu_unit] = 1;
      else
         n_shared_fu[fu_unit] = 1 + n_shared_fu[fu_unit];
   }
   /// check easy binding and compute the list of vertices for which a sharing is possible
   std::set<vertex> easy_binded_vertices;
   for(boost::tie(vIt, vItEnd) = boost::vertices(*sdg); vIt != vItEnd; vIt++)
   {
      if(fu.get_index(*vIt) != INFINITE_UINT) continue;
      fu_unit = fu.get_assign(*vIt);
      if(all.is_vertex_bounded(fu_unit) || (all.get_number_channels(fu_unit) == 1 && all.is_memory_unit(fu_unit)) || n_shared_fu.find(fu_unit)->second == 1)
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Easy binding for -> " + GET_NAME(sdg, *vIt));
         fu.bind(*vIt, fu_unit, 0);
         easy_binded_vertices.insert(*vIt);
      }
   }

}


std::string easy_module_binding::get_kind_text() const
{
   return "easy-module-binding";
}

