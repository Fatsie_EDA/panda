/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file fu_binding_creator.cpp
 * @brief Implementation of module binding class.
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "fu_binding_creator.hpp"

#include "hls.hpp"
#include "hls_manager.hpp"
#include "fu_binding.hpp"

///implemented algorithms
#include "unique_binding.hpp"
#include "cdfc_module_binding.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"
#include "Parameter.hpp"
#include "exceptions.hpp"

fu_binding_creator::fu_binding_creator(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   HLS_step(_Param, _HLSMgr, _funId)
{
   if (!HLS->Rfu) HLS->Rfu = fu_bindingRef(new fu_binding(HLS->ALL, HLSMgr->get_tree_manager()));
}

fu_binding_creator::~fu_binding_creator()
{

}

fu_binding_creatorRef fu_binding_creator::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string algorithm = "CDFC_WEIGHTED_COLORING";
   type_t algorithm_t = CDFC_WEIGHTED_COLORING;

   if (CE_XVM(algorithm, node))
   {
      LOAD_XVM(algorithm, node);
      if (algorithm == "CDFC_TTT_FAST")
         algorithm_t = CDFC_TTT_FAST;
      else if (algorithm == "CDFC_TTT_FAST2")
         algorithm_t = CDFC_TTT_FAST2;
      else if (algorithm == "CDFC_TTT_FULL")
         algorithm_t = CDFC_TTT_FULL;
      else if (algorithm == "CDFC_TTT_FULL2")
         algorithm_t = CDFC_TTT_FULL2;
      else if (algorithm == "CDFC_TS")
         algorithm_t = CDFC_TS;
      else if (algorithm == "CDFC_WEIGHTED_TS")
         algorithm_t = CDFC_WEIGHTED_TS;
      else if (algorithm == "CDFC_COLORING")
         algorithm_t = CDFC_COLORING;
      else if (algorithm == "CDFC_WEIGHTED_COLORING")
         algorithm_t = CDFC_WEIGHTED_COLORING;
      else if (algorithm == "CDFC_BIPARTITE_MATCHING")
         algorithm_t = CDFC_BIPARTITE_MATCHING;
      else if (algorithm == "UNIQUE")
         algorithm_t = UNIQUE;
      else
         THROW_ERROR("FU binding algorithm \"" + algorithm + "\" currently not supported");
   }
   return factory(algorithm_t, Param, HLSMgr, funId);
}

fu_binding_creatorRef fu_binding_creator::factory(type_t type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   switch(type)
   {
      case CDFC_TTT_FAST:
        return fu_binding_creatorRef(new cdfc_module_binding(Param, HLSMgr, funId, cdfc_module_binding::TTT_FAST));
      case CDFC_TTT_FAST2:
        return fu_binding_creatorRef(new cdfc_module_binding(Param, HLSMgr, funId, cdfc_module_binding::TTT_FAST2));
      case CDFC_TTT_FULL:
        return fu_binding_creatorRef(new cdfc_module_binding(Param, HLSMgr, funId, cdfc_module_binding::TTT_FULL));
      case CDFC_TTT_FULL2:
        return fu_binding_creatorRef(new cdfc_module_binding(Param, HLSMgr, funId, cdfc_module_binding::TTT_FULL2));
      case CDFC_TS:
        return fu_binding_creatorRef(new cdfc_module_binding(Param, HLSMgr, funId, cdfc_module_binding::TS));
      case CDFC_WEIGHTED_TS:
        return fu_binding_creatorRef(new cdfc_module_binding(Param, HLSMgr, funId, cdfc_module_binding::WEIGHTED_TS));
      case CDFC_COLORING:
        return fu_binding_creatorRef(new cdfc_module_binding(Param, HLSMgr, funId, cdfc_module_binding::COLORING));
      case CDFC_WEIGHTED_COLORING:
        return fu_binding_creatorRef(new cdfc_module_binding(Param, HLSMgr, funId, cdfc_module_binding::WEIGHTED_COLORING));
      case CDFC_BIPARTITE_MATCHING:
         return fu_binding_creatorRef(new cdfc_module_binding(Param, HLSMgr, funId, cdfc_module_binding::BIPARTITE_MATCHING));
      case UNIQUE:
        return fu_binding_creatorRef(new unique_binding(Param, HLSMgr, funId));
      default:
        THROW_UNREACHABLE("Module binding algorithm not yet supported " + boost::lexical_cast<std::string>(type));
   }
   return fu_binding_creatorRef();
}
