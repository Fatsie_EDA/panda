/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file unique_binding.hpp
 * @brief Class to create a unique binding
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @version $Revision$
 * @date $Date$
*/

#ifndef UNIQUE_BINDING_HPP
#define UNIQUE_BINDING_HPP

#include "fu_binding_creator.hpp"

/**
 * Class managing the module allocation.
 */
class unique_binding : public fu_binding_creator
{

   public:

      /**
       * Constructor
       */
      unique_binding(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      ~unique_binding();

      /**
       * Performs module binding assigning each operation to a free resource
       */
      void exec();

      /**
       * Returns the name of the implemented algorithm
       */
      std::string get_kind_text() const;

};

#endif
