/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file cdfc_module_binding.hpp
 * @brief Module binding based on the analysis of the control data flow chained graph
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @version $Revision$
 * @date $Date$
*/

#ifndef CDFC_MODULE_BINDING_HPP
#define CDFC_MODULE_BINDING_HPP

#include "fu_binding_creator.hpp"

#include "graph.hpp"

#include <unordered_map>

class fu_binding;
REF_FORWARD_DECL(CdfcGraph);
CONSTREF_FORWARD_DECL(OpGraph);
CONSTREF_FORWARD_DECL(Parameter);

struct CdfcEdgeInfo : public EdgeInfo
{
   const int edge_weight;

   /**
    * Constructor
    * @param edge_weight is the weight to be set
    */
   CdfcEdgeInfo(const int edge_weight);
};
typedef refcount<CdfcEdgeInfo> CdfcEdgeInfoRef;
typedef refcount<const CdfcEdgeInfo> CdfcEdgeInfoConstRef;

/**
 * The info associated with a cdfc graph
 */
struct CdfcGraphInfo : public GraphInfo
{
   const std::map<vertex,vertex> & c2s;

   ///The operation graph associated with the cdfc graph
   const OpGraphConstRef operation_graph;

   /**
    * Constructor
    */
   CdfcGraphInfo(const std::map<vertex,vertex> & c2s, const OpGraphConstRef operation_graph);
};
typedef refcount<CdfcGraphInfo> CdfcGraphInfoRef;
typedef refcount<const CdfcGraphInfo> CdfcGraphInfoConstRef;

/**
 * Cdfc collection of graphs
 */
class CdfcGraphsCollection : public graphs_collection
{
   public:
      /**
       * Constructor
       * @param cdfc_graph_info is the info to be associated with the graph
       * @param parameters is the set of input parameters
       */
      CdfcGraphsCollection(const CdfcGraphInfoRef cdfc_graph_info, const ParameterConstRef parameters);

      /**
       * Destructor
       */
      ~CdfcGraphsCollection();

      /**
       * Add an edge with a weight
       * @param source is the source of the edge
       * @param target is the target of the edge
       * @param selector is the selector to be added
       * @param weight is the weight to be set
       * @return the created edge
       */
      inline
      EdgeDescriptor AddEdge(const vertex source, const vertex target, const int selector, const int weight)
      {
         THROW_ASSERT(not ExistsEdge(source, target), "Trying to add an already existing edge");
         return InternalAddEdge(source, target, selector, EdgeInfoRef(new CdfcEdgeInfo(weight)));
      }

      /**
       * Add an edge with empty information associated
       * @param source is the source of the edge
       * @param target is the target of the edge
       * @param selector is the selector to be added
       * @return the created edge
       */
      inline
      EdgeDescriptor AddEdge(const vertex source, const vertex target, const int selector)
      {
         if(ExistsEdge(source, target))
            return AddSelector(source, target, selector);
         else
            return InternalAddEdge(source, target, selector, EdgeInfoRef(new CdfcEdgeInfo(0)));
      }
};
typedef refcount<CdfcGraphsCollection> CdfcGraphsCollectionRef;
typedef refcount<const CdfcGraphsCollection> CdfcGraphsCollectionConstRef;

/**
 * Cdfc graph
 */
class CdfcGraph : public graph
{
   public:
      /**
       * Constructor
       * @param cdfc_graphs_collection is the collections of graph to which this graph belongs
       * @param selector is the selector which identifies the edges of this graph
       */
      CdfcGraph(const CdfcGraphsCollectionRef cdfc_graphs_collection, const int selector);

      /**
       * Constructor
       * @param cdfc_graphs_collection is the collections of graph to which this graph belongs
       * @param selector is the selector which identifies the edges of this graph
       * @param vertices is the set of vertexes on which the graph is filtered.
       */
      CdfcGraph(const CdfcGraphsCollectionRef cdfc_graphs_collection, const int selector, const std::unordered_set<vertex> vertices);

      /**
       * Destructor
       */
      ~CdfcGraph();

      /**
       * Returns the info associated with an edge
       */
      inline
      const CdfcEdgeInfoConstRef CGetCdfcEdgeInfo(const EdgeDescriptor e) const
      {
         return RefcountCast<const CdfcEdgeInfo>(graph::CGetEdgeInfo(e));
      }

      /**
       * Writes this graph in dot format
       * @param file_name is the file where the graph has to be printed
       * @param detail_level is the detail level of the printed graph
       */
      void WriteDot(const std::string & file_name, const int detail_level = 0) const;
};
typedef refcount<CdfcGraph> CdfcGraphRef;
typedef refcount<const CdfcGraph> CdfcGraphConstRef;

/**
 * Class managing the module allocation.
 */
class cdfc_module_binding : public fu_binding_creator
{
   public:

      ///Module binding algorithms
      typedef enum
      {
         TTT_FAST = 0,
         TTT_FAST2,
         TTT_FULL,
         TTT_FULL2,
         TS,
         WEIGHTED_TS,
         COLORING,
         WEIGHTED_COLORING,
         BIPARTITE_MATCHING
      } type_t;

      /**
       * This is the constructor of the class.
       */
      cdfc_module_binding(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId, type_t _clique_covering_method);

      /**
       * Destructor.
       */
      ~cdfc_module_binding();

      /**
       * Performs module binding exploiting the control data flow chained graph
       */
      void exec();

      /**
       * Returns the name of the implemented algorithm
       */
      std::string get_kind_text() const;

   private:

      type_t clique_covering_method;

      bool false_loop_search(vertex src, unsigned k, vertex start, const CdfcGraphConstRef cdfc, const CdfcGraphConstRef cg, std::deque<EdgeDescriptor>& candidate_edges);
      bool false_loop_search_cdfc_1(vertex src, unsigned int level, unsigned k, vertex start, const CdfcGraphConstRef cdfc, const CdfcGraphConstRef cg, std::deque<EdgeDescriptor>& candidate_edges);
      bool false_loop_search_cdfc_more(vertex src, unsigned int level, unsigned k, vertex start, const CdfcGraphConstRef cdfc, const CdfcGraphConstRef cg, std::deque<EdgeDescriptor>& candidate_edges);

      int weight_computation(vertex v1, vertex v2, unsigned int fu_s1, const double mux_time, const OpGraphConstRef fdfg, fu_binding& fu, const std::unordered_map<vertex,double> &slack_time);

      static const int CD_EDGE = 1;
      static const int COMPATIBILITY_EDGE = 2;
};

#endif
