/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file cdfc_module_binding.cpp
 * @brief Class implementation of the module binding algorithm.
 * *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"

///Header include
#include "cdfc_module_binding.hpp"

#include "hls_manager.hpp"
#include "hls_target.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"
#include "behavioral_writer_helper.hpp"
#include "function_behavior.hpp"
#include "hls_manager.hpp"
#include "op_graph.hpp"
#include "technology_manager.hpp"
#include "time_model.hpp"

///Graph include
#include "graph.hpp"

///HLS include
#include "allocation.hpp"
#include "chaining.hpp"
#include "clique_covering.hpp"
#include "fu_binding.hpp"
#include "hls.hpp"
#include "hls_constraints.hpp"
#include "liveness.hpp"
#include "reg_binding.hpp"
#include "storage_value_insertion.hpp"
#include "technology_node.hpp"

///Parameter include
#include "Parameter.hpp"
#include "utility.hpp"
#include "dbgPrintHelper.hpp"
#include "mux_connection_binding.hpp"
#include "technology_builtin.hpp"

#include <iostream>
#include <string>

///Tree helper
#include "tree_helper.hpp"


#if HAVE_EXPERIMENTAL
#include "controller_creator.hpp"
#endif

template <typename OutputIterator>
struct topological_based_sorting_visitor : public boost::dfs_visitor<>
{
      topological_based_sorting_visitor(std::map<vertex,vertex> &_c2s, const OpGraphConstRef _sdg, OutputIterator _iter)
         : m_iter(_iter), c2s(_c2s), sdg(_sdg) { }

      template <typename Vertex, typename Graph>
      void finish_vertex(const Vertex& u, Graph&) { *m_iter++ = u; }
      template <typename Edge, typename Graph>
      void back_edge(const Edge&e, Graph&g) { std::cerr << GET_NAME(&g, c2s[boost::source(e,g)]) << "(" << GET_OP(&g, c2s[boost::source(e,g)]) << ")-" << GET_NAME(&g, c2s[boost::target(e,g)]) << "(" << GET_OP(&g, c2s[boost::target(e,g)]) << ")" << std::endl; BOOST_THROW_EXCEPTION(boost::not_a_dag());}

      OutputIterator m_iter;
      std::map<vertex,vertex> &c2s;
      const OpGraphConstRef sdg;

};


template <typename VertexListGraph, typename OutputIterator,  typename P, typename T, typename R>
void topological_based_sorting(const VertexListGraph& g, std::map<vertex,vertex> &c2s, const OpGraphConstRef sdg, OutputIterator result, const boost::bgl_named_params<P, T, R>& params)
{
   typedef topological_based_sorting_visitor<OutputIterator> TopoVisitor;
   boost::depth_first_search(g, params.visitor(TopoVisitor(c2s, sdg, result)));
}

template <typename VertexListGraph, typename OutputIterator>
void topological_based_sorting(const VertexListGraph& g, std::map<vertex,vertex> &c2s, const OpGraphConstRef sdg, OutputIterator result)
{
   topological_based_sorting(g, c2s, sdg, result, boost::bgl_named_params<int, boost::buffer_param_t>(0));
}


/**
 * Functor used to compare which of two resources has to be considered first during the binding
 */
struct cdfc_resource_ordering_functor
{
   private:
      /// copy of the order: control step associated with the vertices.
      allocationRef & all;

   public:
      /**
       * functor function used to compare two resources with respect to their area
       * @param a is the first vertex
       * @param bis the second vertex
       * @return true when a is faster than b
       */
      bool operator() (const unsigned int & a, const unsigned int & b) const
      {
         double wa_a = all->get_area(a);
         double wa_b = all->get_area(b);
         return ((wa_a < wa_b) || (wa_a == wa_b && a < b));
      }

      /**
       * Constructor
       * @param o is the order.
       */
      cdfc_resource_ordering_functor(allocationRef & _all) : all(_all) {}

      /**
       * Destructor
       */
      ~cdfc_resource_ordering_functor() {}

};


template<typename vertex_type>
void estimate_muxes(const typename std::set<vertex_type> &cluster, unsigned int & total_muxes, unsigned int & max_mux_ins, unsigned int & max_mux_out, const typename std::map<vertex_type, vertex> &converter, const HLS_managerRef HLSMgr, const hlsRef HLS)
{
   fu_binding& fu = *(HLS->Rfu);
   const tree_managerRef TreeM = HLSMgr->get_tree_manager();
   const OpGraphConstRef data = HLS->CGetOpGraph(FunctionBehavior::FDFG);
   std::map<unsigned int, std::set<unsigned int> > regs_in;
   std::map<unsigned int, std::set<unsigned int> > chained_in;
   std::map<unsigned int, std::set<std::pair<unsigned int, unsigned int> > > module_in;
   std::map<unsigned int, std::set<std::pair<unsigned int, unsigned int> > > module_in_reg;
   std::set<unsigned int> regs_out;
   //std::set<unsigned int> chained_out;
   unsigned int max_port_index = 0;
   typename std::set< vertex_type>::const_iterator vert_it_end = cluster.end();
   for(typename std::set<vertex_type>::const_iterator vert_it = cluster.begin(); vert_it != vert_it_end; ++vert_it)
   {
      vertex current_v = converter.find(*vert_it)->second;
      std::vector<HLS_manager::io_binding_type> vars_read = HLSMgr->get_required_values(HLS->functionId, current_v);
      max_port_index = std::max(max_port_index, static_cast<unsigned int>(vars_read.size()));
      for(unsigned int port_index = 0; port_index < vars_read.size(); ++port_index)
      {

         unsigned int tree_var = std::get<0>(vars_read[port_index]);
         if(tree_var != 0)
         {
            const std::set<vertex>& running_states = HLS->Rliv->get_state_where_run(current_v);
            const std::set<vertex>::const_iterator rs_it_end = running_states.end();
            for(std::set<vertex>::const_iterator rs_it = running_states.begin(); rs_it != rs_it_end; ++rs_it)
            {
               vertex state = *rs_it;
               if(tree_helper::is_parameter(TreeM, tree_var) || !HLS->Rliv->has_op_where_defined(tree_var))
               {
                  chained_in[port_index].insert(tree_var); /// it is not chained but from the mux binding it counts as input to the mux tree
                  //std::cerr << "port " << port_index << " chained " << tree_var << std::endl;
               }
               else
               {
                  vertex def_op = HLS->Rliv->get_op_where_defined(tree_var);
                  const std::set<vertex>& def_op_ending_states = HLS->Rliv->get_state_where_end(def_op);
                  if((GET_TYPE(data, def_op) & TYPE_PHI)==0)
                  {
                     if(def_op_ending_states.find(state) != def_op_ending_states.end())
                     {
                        if(fu.get_index(def_op) != INFINITE_UINT)
                           module_in[port_index].insert(std::make_pair(fu.get_assign(def_op), fu.get_index(def_op)));
                        else
                           chained_in[port_index].insert(tree_var);
                        //std::cerr << "port " << port_index << " chained " << tree_var << std::endl;
                     }
                     else if(HLS->svi_data->is_a_storage_value(state, tree_var))
                     {
                        unsigned int storage_value = HLS->svi_data->get_storage_value_index(state, tree_var);
                        if(HLS->Rreg && HLS->Rreg->size() != 0)
                           regs_in[port_index].insert(HLS->Rreg->get_register(storage_value));
                        else if(fu.get_index(def_op) != INFINITE_UINT)
                           module_in_reg[port_index].insert(std::make_pair(fu.get_assign(def_op), fu.get_index(def_op)));
                        else
                           regs_in[port_index].insert(storage_value);
                        //std::cerr << "port " << port_index << " reg " << HLS->Rreg->get_register(storage_value) << std::endl;
                     }
                  }
                  else
                  {
                     unsigned int storage_value = HLS->svi_data->get_storage_value_index(state, tree_var);
                     if(HLS->Rreg && HLS->Rreg->size() != 0)
                        regs_in[port_index].insert(HLS->Rreg->get_register(storage_value));
                     else if(fu.get_index(def_op) != INFINITE_UINT)
                        module_in_reg[port_index].insert(std::make_pair(fu.get_assign(def_op), fu.get_index(def_op)));
                     else
                        regs_in[port_index].insert(storage_value);
                     //std::cerr << "port " << port_index << " reg " << HLS->Rreg->get_register(storage_value) << std::endl;
                  }
               }
            }
         }
      }

      if(HLS->Rreg && HLS->Rreg->size() != 0)
      {
         unsigned int var_written = HLSMgr->get_produced_value(HLS->functionId, current_v);
         if(var_written)
         {
            const std::set<vertex>& end = HLS->Rliv->get_state_where_end(current_v);
            const std::set<vertex>::const_iterator e_it_end = end.end();
            for(std::set<vertex>::const_iterator e_it = end.begin(); e_it != e_it_end; ++e_it)
            {
               vertex state = *e_it;
               if(HLS->svi_data->is_a_storage_value(state, var_written))
               {
                  regs_out.insert(HLS->Rreg->get_register(HLS->svi_data->get_storage_value_index(state, var_written)));
               }
               /*else if(var_written != 0)
               chained_out.insert(var_written);*/
            }
         }
      }
   }

   /// compute the maximum number of mux ins
   max_mux_ins=0;
   total_muxes=0;
   unsigned int current_muxs;
   for(unsigned int port_index=0; port_index < max_port_index; ++port_index)
   {
      current_muxs=0;
      if(module_in.find(port_index) != module_in.end())
      {
         current_muxs += static_cast<unsigned int>(module_in.find(port_index)->second.size());
      }
      if(module_in_reg.find(port_index) != module_in_reg.end())
      {
         current_muxs += static_cast<unsigned int>(module_in_reg.find(port_index)->second.size());
      }
      if(regs_in.find(port_index) != regs_in.end())
      {
         current_muxs += static_cast<unsigned int>(regs_in.find(port_index)->second.size());
      }
      if(chained_in.find(port_index) != chained_in.end())
      {
         current_muxs += static_cast<unsigned int>(chained_in.find(port_index)->second.size());
      }
      max_mux_ins = std::max(max_mux_ins, current_muxs);
      total_muxes += current_muxs;
   }
   if(HLS->Rreg && HLS->Rreg->size() != 0)
   {
      current_muxs = static_cast<unsigned int>(regs_out.size()/*+chained_out.size()*/);
      max_mux_out=static_cast<unsigned int>(current_muxs);
      total_muxes += current_muxs;
   }
   else
      max_mux_out= 0;
   //std::cerr << GET_NAME(data, converter.find(*cluster.begin())->second) << ":" << GET_OP(data, converter.find(*cluster.begin())->second) << " max_mux_ins=" << max_mux_ins << " total_muxes=" << total_muxes << std::endl;
}


struct slack_based_filtering : public filter_clique<vertex>
{
      slack_based_filtering(const std::unordered_map<vertex,double> &_slack_time, double _mux_delay, const hlsRef _HLS, const HLS_managerRef _HLSMgr) :
         slack_time(_slack_time),
         mux_delay(_mux_delay),
         HLS(_HLS),
         HLSMgr(_HLSMgr),
         data(_HLS->CGetOpGraph(FunctionBehavior::FDFG)),
         TreeM(_HLSMgr->get_tree_manager())
      {}

      bool select_candidate_to_remove(const std::set<C_vertex> &candidate_clique, C_vertex &v, const std::map<C_vertex, vertex> &converter, const cc_compatibility_graph &cg) const
      {
         THROW_ASSERT(!candidate_clique.empty(), "candidate clique cannot be empty");
         double min_slack=std::numeric_limits<double>::max();
         C_vertex min_slack_vertex=*candidate_clique.begin();
         double curr_slack;
         vertex current_v;
         std::set<C_vertex>::const_iterator vert_it_end = candidate_clique.end();
         for(std::set<C_vertex>::const_iterator vert_it = candidate_clique.begin(); vert_it != vert_it_end; ++vert_it)
         {
            THROW_ASSERT(converter.find(*vert_it) != converter.end(), "non-existing vertex");
            current_v = converter.find(*vert_it)->second;
            curr_slack = slack_time.find(current_v)->second;
            THROW_ASSERT(curr_slack>=0.0, "negative slack not allowed");
            //std::cerr << "Current slack " << curr_slack << " for vertex " << GET_NAME(data, current_v) << std::endl;
            if(curr_slack < min_slack)
            {
               min_slack_vertex = *vert_it;
               min_slack = curr_slack;
            }
            else if(curr_slack == min_slack && min_slack_vertex != *vert_it)
            {
               int weight1=0;
               boost::graph_traits<cc_compatibility_graph>::out_edge_iterator ei,ei_end;
               boost::tie(ei,ei_end) = boost::out_edges(*vert_it, cg);
               for(;ei != ei_end; ++ei)
                  if(candidate_clique.find(boost::target(*ei, cg)) != candidate_clique.end())
                    weight1+=cg[*ei].weight;
               int weight2=0;
               boost::tie(ei,ei_end) = boost::out_edges(min_slack_vertex, cg);
               for(;ei != ei_end; ++ei)
                  if(candidate_clique.find(boost::target(*ei, cg)) != candidate_clique.end())
                    weight2+=cg[*ei].weight;

               if(weight1 < weight2)
                  min_slack_vertex = *vert_it;
            }
         }
         //std::cerr << "Min_slack " << min_slack << std::endl;
         /// special case
         if(min_slack == 0)
            return false;

         unsigned int total_muxes;
         unsigned int max_mux_ins;
         unsigned int max_mux_out;
         estimate_muxes<C_vertex>(candidate_clique, total_muxes, max_mux_ins, max_mux_out, converter, HLSMgr, HLS);

         unsigned int levels_in = 1;
         for(; max_mux_ins > (1u << levels_in); ++levels_in);
         //std::cerr << " Slack "<< min_slack << " levels " << levels_in << " max_mux_ins " << max_mux_ins << std::endl;
         unsigned int levels_out = max_mux_out>1 ? 1 : 0; /// very very inaccurate

#if 0
         unsigned int virtual_levels;
         if(levels_in+levels_out>0)
            virtual_levels = 1+(3*(levels_in+levels_out-1))/2; /// try to discourage large mux trees
         else
            virtual_levels = 0;

         if(virtual_levels*mux_delay>min_slack)
#else
         if((levels_in+levels_out)*mux_delay>min_slack)
#endif
         {
            v = min_slack_vertex;
            //std::cerr << "Removed " << GET_NAME(data, converter.find(v)->second) << " Slack "<< min_slack << " levels " << levels_in << std::endl;
            return true;
         }
         else
         {
            //std::cerr << "No vertex removed " << " Slack "<< min_slack << " levels " << levels_in << std::endl;
            return false;
         }
      }

   private:
      const std::unordered_map<vertex,double> & slack_time;
      const double mux_delay;
      const hlsRef HLS;
      const HLS_managerRef HLSMgr;
      const OpGraphConstRef data;
      const tree_managerRef TreeM;
};

CdfcEdgeInfo::CdfcEdgeInfo(const int _edge_weight) :
   edge_weight(_edge_weight)
{}

class CdfcWriter : public VertexWriter
{
   private:
      ///The info associated with the graph to be printed
      const CdfcGraphInfo * cdfc_graph_info;

      const std::map<vertex,vertex> & c2s;

      ///The functor used to print labels which correspond to vertices of the graph to be printed
      const OpWriter operation_writer;

   public:
      /**
       * Constructor
       * @param _printing_graph is the graph to be printed
       */
      CdfcWriter(const CdfcGraph * _printing_graph) :
         VertexWriter(_printing_graph, 0),
         cdfc_graph_info(GetPointer<const CdfcGraphInfo>(_printing_graph->CGetGraphInfo())),
         c2s(cdfc_graph_info->c2s),
         operation_writer(cdfc_graph_info->operation_graph.get(), 0)
      {}

      /**
       * Operator used to print the label of a vertex
       * @param out is the stream where label has to printed
       * @param v is the vertex to be printed
       */
      void operator()(std::ostream& out, const vertex& v) const
      {
         operation_writer(out, c2s.find(v)->second);
      }
};

class CdfcEdgeWriter : public EdgeWriter
{
   public:
      /**
       * Constructor
       * @param _printing_graph is the graph to be printed
       * @param _selector is the selector of the graph to be printed
       */
      CdfcEdgeWriter(const CdfcGraph * _printing_graph) :
         EdgeWriter(_printing_graph, 0)
      {}

      /**
       * Operator which print label of an EdgeDescriptor
       * @param out is the stream
       * @param e is the edge
       */
      void operator()(std::ostream& out, const EdgeDescriptor& e) const
      {
         const CdfcEdgeInfo * edge_info = Cget_edge_info<CdfcEdgeInfo>(e, *printing_graph);
         if(edge_info)
            out << "[label=\"" << edge_info->edge_weight << "\"]";
         else
            out << "[color=red3]";
      }
};

CdfcGraphInfo::CdfcGraphInfo(const std::map<vertex,vertex> & _c2s, const OpGraphConstRef _operation_graph) :
   c2s(_c2s),
   operation_graph(_operation_graph)
{}

CdfcGraphsCollection::CdfcGraphsCollection(const CdfcGraphInfoRef cdfc_graph_info, const ParameterConstRef _parameters) :
   graphs_collection(RefcountCast<GraphInfo>(cdfc_graph_info), _parameters)
{}

CdfcGraphsCollection::~CdfcGraphsCollection()
{}

CdfcGraph::CdfcGraph(const CdfcGraphsCollectionRef  cdfc_graphs_collection, const int _selector) :
   graph(cdfc_graphs_collection.get(), _selector)
{}

CdfcGraph::CdfcGraph(const CdfcGraphsCollectionRef  cdfc_graphs_collection, const int _selector, const std::unordered_set<vertex> vertices) :
   graph(cdfc_graphs_collection.get(), _selector, vertices)
{}

CdfcGraph::~CdfcGraph()
{}

void CdfcGraph::WriteDot(const std::string & file_name, const int) const
{
   const CdfcGraphInfo * cdfc_graph_info = GetPointer<const CdfcGraphInfo>(CGetGraphInfo());
   const BehavioralHelperConstRef behavioral_helper = cdfc_graph_info->operation_graph->CGetOpGraphInfo()->BH;
   const std::string output_directory = collection->parameters->getOption<std::string>(OPT_dot_directory) + "/" + behavioral_helper->get_function_name() + "/";
   if (!boost::filesystem::exists(output_directory))
      boost::filesystem::create_directories(output_directory);
   const std::string full_name = output_directory + file_name;
   const VertexWriterConstRef cdfc_writer(new CdfcWriter(this));
   const EdgeWriterConstRef cdfc_edge_writer(new CdfcEdgeWriter(this));
   InternalWriteDot<const CdfcWriter, const CdfcEdgeWriter>(full_name, cdfc_writer, cdfc_edge_writer);
}


cdfc_module_binding::cdfc_module_binding(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId, type_t _clique_covering_method) :
   fu_binding_creator(_Param, _HLSMgr, _funId),
   clique_covering_method(_clique_covering_method)
{

}

cdfc_module_binding::~cdfc_module_binding()
{

}

void cdfc_module_binding::exec()
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Starting module allocation based on ''control data flow chained graph''");

#if HAVE_EXPERIMENTAL
   //added for managing chaining in PC
   std::map<vertex, std::set<vertex> > unreachable_nodes;
#endif

   // resource binding and allocation  info
   fu_binding& fu = *(HLS->Rfu);
   allocation& all = *(HLS->ALL);

   cdfc_resource_ordering_functor r_functor(HLS->ALL);
   double setup_hold_time = HLS->ALL->get_setup_hold_time();
   double controller_delay = HLS->ALL->estimate_controller_delay();

   // pointer to a Control, Data dependence and antidependence graph graph
   const OpGraphConstRef sdg = HLS->CGetOpGraph(FunctionBehavior::SDG);
   size_t total_modules_allocated = 0;
   double total_resource_area = 0, total_DSPs = 0;

   VertexIterator vIt, vItEnd;
   unsigned int fu_unit;
   /// compute unshared resources
   std::map<unsigned int, unsigned int> n_shared_fu;
   for(boost::tie(vIt, vItEnd) = boost::vertices(*sdg); vIt != vItEnd; vIt++)
   {
      fu_unit = fu.get_assign(*vIt);
      if(all.is_vertex_bounded(fu_unit) || (all.get_number_channels(fu_unit) == 1 && all.is_memory_unit(fu_unit))) continue;
      if(n_shared_fu.find(fu_unit) == n_shared_fu.end())
         n_shared_fu[fu_unit] = 1;
      else
         n_shared_fu[fu_unit] = 1 + n_shared_fu[fu_unit];
   }
   /// check easy binding and compute the list of vertices for which a sharing is possible
   std::map<unsigned int, std::set<vertex>, cdfc_resource_ordering_functor > candidate_vertices(r_functor);
   std::unordered_set<vertex>  all_candidate_vertices;
   std::unordered_map<unsigned int, std::set<vertex> > easy_binded_vertices;
   for(boost::tie(vIt, vItEnd) = boost::vertices(*sdg); vIt != vItEnd; vIt++)
   {
      fu_unit = fu.get_assign(*vIt);
      if(fu.get_index(*vIt) != INFINITE_UINT)
      {
         ++total_modules_allocated;
         total_resource_area += all.get_area(fu_unit);
         total_DSPs += all.get_DSPs(fu_unit);
         if(!all.is_vertex_bounded(fu_unit) && n_shared_fu.find(fu_unit)->second != 1)
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "1Easy binding for -> " + GET_NAME(sdg, *vIt) + "-" + GET_OP(sdg, *vIt) + "(" + all.get_string_name(fu_unit) + ")");
            easy_binded_vertices[fu_unit].insert(*vIt);
         }
      }
      else
      {
         if(all.is_vertex_bounded(fu_unit) || (all.get_number_channels(fu_unit) == 1 && all.is_memory_unit(fu_unit)) || n_shared_fu.find(fu_unit)->second == 1)
         {
            ++total_modules_allocated;
            total_resource_area += all.get_area(fu_unit);
            total_DSPs += all.get_DSPs(fu_unit);
            fu.bind(*vIt, fu_unit, 0);
            if(all.is_memory_unit(fu_unit))
            {
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "2Easy binding for -> " + GET_NAME(sdg, *vIt) + "-" + GET_OP(sdg, *vIt));
               easy_binded_vertices[fu_unit].insert(*vIt);
            }
         }
         else
         {
            candidate_vertices[fu_unit].insert(*vIt);
            all_candidate_vertices.insert(*vIt);
         }
      }
   }
   /// in case no vertices was left we have done
   if(!candidate_vertices.empty())
   {
      std::map<vertex,vertex> c2s;
      std::map<vertex,vertex> s2c;

      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Creating the cdfc for the module binding...");
      const CdfcGraphInfoRef cdfc_graph_info(new CdfcGraphInfo(c2s, sdg));
      const CdfcGraphsCollectionRef cdfc_graphs_collection = CdfcGraphsCollectionRef(new CdfcGraphsCollection(cdfc_graph_info, Param));


      std::deque<vertex> sorted_vertices;
      boost::topological_sort(*sdg, std::front_inserter(sorted_vertices));
      std::unordered_map<vertex,double> starting_time;
      std::unordered_map<vertex,double> ending_time;
      std::unordered_map<vertex,double> slack_time;
      double clock_budget = HLS->HLS_C->get_clock_period();
      const std::deque<vertex>::const_iterator it_sv_end = sorted_vertices.end();
      for(std::deque<vertex>::const_iterator it_sv = sorted_vertices.begin(); it_sv != it_sv_end; ++it_sv)
      {
         starting_time[*it_sv] = 0.0;
         InEdgeIterator ie, ie_end;
         for(boost::tie(ie, ie_end) = boost::in_edges(*it_sv, *sdg); ie != ie_end; ++ie)
         {
            vertex src = boost::source(*ie, *sdg);
            if(HLS->Rchain->may_be_chained_ops(*it_sv, src))
            {
               unsigned int fu_type = fu.get_assign(src);
               unsigned int ii_time = all.get_initiation_time(fu_type, src, sdg);
               double src_ex_delay = all.get_execution_time(fu_type, src, sdg);
               double src_stage_delay = all.get_stage_period(fu_type, src, sdg);
               double src_delay = (ii_time != 0 ? src_stage_delay : src_ex_delay)-HLS->ALL->get_correction_time(fu_type);
               if(src_delay<0.0)
                  src_delay = 0.0;
               starting_time[*it_sv] = std::max(starting_time[*it_sv], src_delay+starting_time[src]);
            }
         }
      }
      const std::deque<vertex>::const_reverse_iterator rit_sv_end = sorted_vertices.rend();
      for(std::deque<vertex>::const_reverse_iterator rit_sv = sorted_vertices.rbegin(); rit_sv != rit_sv_end; ++rit_sv)
      {
         unsigned int fu_type = fu.get_assign(*rit_sv);
         double ex_delay = all.get_execution_time(fu_type, *rit_sv, sdg);
         double stage_delay = all.get_stage_period(fu_type, *rit_sv, sdg);
         unsigned int ii_time = all.get_initiation_time(fu_type, *rit_sv, sdg);

         double delay = (ii_time != 0 ? stage_delay : ex_delay)-HLS->ALL->get_correction_time(fu_type);
         if(GET_TYPE(sdg, *rit_sv) & (TYPE_IF | TYPE_SWITCH))
         {
            delay += controller_delay;
            if(delay>clock_budget)
               delay = clock_budget;
         }
         if(delay<0.0)
            delay = 0.0;
         ending_time[*rit_sv] = delay+starting_time[*rit_sv];
         double current_budget = clock_budget - ending_time[*rit_sv]-setup_hold_time;
         //std::cerr << GET_NAME(sdg, *rit_sv) << "(" <<GET_OP(sdg, *rit_sv) << ")" << " current_budget " << current_budget << std::endl;
         if(current_budget<=0.0)
            current_budget=0.0;
         OutEdgeIterator oe, oe_end;
         for(boost::tie(oe, oe_end) = boost::out_edges(*rit_sv, *sdg); oe != oe_end; ++oe)
         {
            vertex tgt =  boost::target(*oe, *sdg);
            if(HLS->Rchain->may_be_chained_ops(*rit_sv, tgt))
            {
               ending_time[*rit_sv] = std::max(ending_time[tgt], ending_time[*rit_sv]);
               current_budget = std::min(current_budget, slack_time[tgt]);
            }
         }
         //std::cerr << "current_budget true " << current_budget << std::endl << std::endl;
         slack_time[*rit_sv] = current_budget;
      }

      boost::graph_traits<graph>::vertices_size_type n_vert = boost::num_vertices(*sdg);
      std::vector<boost::graph_traits<OpGraph>::vertices_size_type> rank_map(n_vert);
      std::vector<vertex> pred_map(n_vert);
      typedef boost::property_map<OpGraph, boost::vertex_index_t>::const_type const_vertex_index_pmap_t;
      const_vertex_index_pmap_t cindex_pmap = boost::get(boost::vertex_index_t(), *sdg);
      /// rank property map definition
      typedef boost::iterator_property_map<std::vector<boost::graph_traits<OpGraph>::vertices_size_type>::iterator, const_vertex_index_pmap_t> rank_pmap_type;
      rank_pmap_type rank_pmap = boost::make_iterator_property_map(rank_map.begin(), cindex_pmap, rank_map[0]);
      /// parent property map definition
      typedef boost::iterator_property_map<std::vector<vertex>::iterator, const_vertex_index_pmap_t> pred_pmap_type;
      pred_pmap_type pred_pmap = boost::make_iterator_property_map(pred_map.begin(), cindex_pmap, pred_map[0]);
      boost::disjoint_sets<rank_pmap_type,pred_pmap_type> ds(rank_pmap, pred_pmap);
      VertexIterator vi, vi_end;
      for (boost::tie(vi, vi_end) = boost::vertices(*sdg); vi != vi_end; ++vi)
      {
         vertex s = *vi;
         ds.make_set(s);
      }

      /// merge easy binded vertices
      const std::unordered_map<unsigned int, std::set<vertex> >::const_iterator fu_eb_it_end = easy_binded_vertices.end();
      for(std::unordered_map<unsigned int, std::set<vertex> >::const_iterator fu_eb_it = easy_binded_vertices.begin(); fu_eb_it != fu_eb_it_end; ++fu_eb_it)
      {
         std::map<unsigned int, vertex> rep_vertex;
         const std::set<vertex>::const_iterator eb_it_end = fu_eb_it->second.end();
         for(std::set<vertex>::const_iterator eb_it = fu_eb_it->second.begin(); eb_it != eb_it_end; ++eb_it)
         {
            vertex cur_v = *eb_it;
            unsigned int vertex_index = fu.get_index(cur_v);
            if(rep_vertex.find(vertex_index) == rep_vertex.end())
               rep_vertex[vertex_index] = cur_v;
            else
            {
               vertex rep = rep_vertex.find(vertex_index)->second;
               ds.union_set(cur_v, rep);
               starting_time[cur_v] = starting_time[rep] = std::max(starting_time[rep], starting_time[cur_v]);
               ending_time[cur_v] = ending_time[rep] = std::max(ending_time[rep], ending_time[cur_v]);
            }
         }
      }

      /// add the vertices to the cdfc graph
      for (boost::tie(vi, vi_end) = boost::vertices(*sdg); vi != vi_end; ++vi)
      {
         vertex s = *vi;
         vertex rep = ds.find_set(s);
         vertex c;
         if(rep == s && s2c.find(rep) == s2c.end())
         {
            c = cdfc_graphs_collection->AddVertex(NodeInfoRef());
            c2s[c] = rep;
         }
         else if(s2c.find(rep) == s2c.end())
         {
            c = cdfc_graphs_collection->AddVertex(NodeInfoRef());
            c2s[c] = rep;
            s2c[rep] = c;
         }
         else
            c = s2c[rep];
         s2c[s] = c;
      }

      /// add the control dependences edges and the chained edges to the cdfc graph
      const OpGraphConstRef dfg = HLS->CGetOpGraph(FunctionBehavior::DFG);
      EdgeIterator ei,ei_end;
      for(boost::tie(ei,ei_end) = boost::edges(*sdg); ei != ei_end; ++ei)
      {
         vertex src = boost::source(*ei, *sdg);
         unsigned int fu_unit_src = fu.get_assign(src);
         unsigned int II_src = all.get_initiation_time(fu_unit_src, src, sdg);
         vertex tgt = boost::target(*ei, *sdg);
         if(HLS->Rchain->may_be_chained_ops(tgt, src) /// only the chained operations are relevant
               && II_src == 0 /// pipelined operations break false loops
               )
         {
            bool exists;
            vertex cdfc_src = s2c[src];
            vertex cdfc_tgt = s2c[tgt];
            if(cdfc_src != cdfc_tgt)
            {
               EdgeDescriptor e;
               boost::tie(e, exists) = boost::edge(cdfc_src, cdfc_tgt, *cdfc_graphs_collection);
               THROW_ASSERT(!exists, "unexpected case: edge already added");
               cdfc_graphs_collection->AddEdge(cdfc_src, cdfc_tgt, CD_EDGE);
            }
            else
               THROW_ERROR(std::string(GET_NAME(sdg, src)) + "(" + GET_OP(sdg, src) + ")--" + GET_NAME(sdg, tgt) + "(" + GET_OP(sdg, tgt) + ")" );
         }
      }

      int _w = 1;
      unsigned int fu_s1;
      double clock_cycle = HLS->HLS_C->get_clock_period_resource_fraction()*HLS->HLS_C->get_clock_period();

      const std::map<unsigned int, std::set<vertex>, cdfc_resource_ordering_functor >::const_iterator fu_cv_it_end = candidate_vertices.end();
      for(std::map<unsigned int, std::set<vertex>, cdfc_resource_ordering_functor >::const_iterator fu_cv_it = candidate_vertices.begin(); fu_cv_it != fu_cv_it_end; ++fu_cv_it)
      {
         fu_s1 = fu_cv_it->first;
         const double mux_time = all.estimate_mux_time(fu_s1);

         const std::set<vertex>::const_iterator cv_it_end = fu_cv_it->second.end();
         for(std::set<vertex>::const_iterator cv_it = fu_cv_it->second.begin(); cv_it != cv_it_end;)
         {
            std::set<vertex>::const_iterator cv1_it = cv_it;
            ++cv_it;
            for(std::set<vertex>::const_iterator cv2_it = cv_it; cv2_it != cv_it_end; ++cv2_it)
            {
               if(HLS->Rliv->are_in_conflict(*cv1_it, *cv2_it))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "conflict between operations " + GET_NAME(sdg, *cv1_it) + "(" + GET_OP(sdg, *cv1_it) + ")" + GET_NAME(sdg, *cv2_it) + "(" + GET_OP(sdg, *cv2_it) + ") (" + all.get_string_name(fu_s1) + ")=" + STR(clock_cycle) + "<" + STR(starting_time[*cv1_it]+ending_time[*cv2_it]-starting_time[*cv2_it]));
                  continue;
               }

               if(HLS->ALL->get_number_channels(fu_s1) < 2 && (HLS->ALL->is_indirect_access_memory_unit(fu_s1) || (!HLS->ALL->is_direct_access_memory_unit(fu_s1) && HLS->ALL->get_cycles(fu_s1, *cv1_it, sdg) <= 1  && HLS->ALL->get_cycles(fu_s1, *cv2_it, sdg) <= 1)) &&
                     (clock_cycle<(setup_hold_time+starting_time[*cv1_it]+ending_time[*cv2_it]-starting_time[*cv2_it])))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "performance based problem in sharing " + GET_NAME(sdg, *cv1_it) + "(" + GET_OP(sdg, *cv1_it) + ")" + GET_NAME(sdg, *cv2_it) + "(" + GET_OP(sdg, *cv2_it) + ")=" + STR(clock_cycle) + "<" + STR(setup_hold_time+starting_time[*cv1_it]+ending_time[*cv2_it]-starting_time[*cv2_it]));
                  continue;
               }
               if(HLS->ALL->get_number_channels(fu_s1) < 2 && (HLS->ALL->is_indirect_access_memory_unit(fu_s1) || (!HLS->ALL->is_direct_access_memory_unit(fu_s1) && HLS->ALL->get_cycles(fu_s1, *cv1_it, sdg) <= 1  && HLS->ALL->get_cycles(fu_s1, *cv2_it, sdg) <= 1)) &&
                     (clock_cycle<(setup_hold_time+starting_time[*cv2_it]+ending_time[*cv1_it]-starting_time[*cv1_it])))
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "performance based problem in sharing " + GET_NAME(sdg, *cv1_it) + "(" + GET_OP(sdg, *cv1_it) + ")" + GET_NAME(sdg, *cv2_it) + "(" + GET_OP(sdg, *cv2_it) + ")=" + STR(clock_cycle) + "<" + STR(setup_hold_time+starting_time[*cv2_it]+ending_time[*cv1_it]-starting_time[*cv1_it]));
                  continue;
               }
               if(GET_TYPE(dfg, *cv1_it) & (TYPE_ENTRY|TYPE_EXIT|TYPE_PHI|TYPE_VPHI|TYPE_GOTO|TYPE_LABEL|TYPE_RET|TYPE_SWITCH)) continue;
               if(GET_TYPE(dfg, *cv2_it) & (TYPE_ENTRY|TYPE_EXIT|TYPE_PHI|TYPE_VPHI|TYPE_GOTO|TYPE_LABEL|TYPE_RET|TYPE_SWITCH)) continue;

               _w = weight_computation(*cv1_it, *cv2_it, fu_s1, mux_time, dfg, fu, slack_time);

               //code for managing chaining in PC

#if HAVE_EXPERIMENTAL
               if(static_cast<controller_creator::controller_type>(Param->getOption<unsigned int>(OPT_controller_architecture))==controller_creator::PARALLEL_CONTROLLER)
               {
                   const OpGraphConstRef saodg = HLS->CGetOpGraph(FunctionBehavior::SAODG);
                   bool mem_op =(GET_OP(saodg, *cv1_it) == LOAD or GET_OP(saodg, *cv1_it) == STORE) or (GET_OP(saodg, *cv2_it) == LOAD or GET_OP(saodg, *cv2_it) == STORE);
                   //FIXME sharing on MCs still needs to be preserved
                   std::set<vertex> workingset, workingset2;
                   if(!mem_op & (HLS->Rchain->get_representative_in(*cv1_it) + HLS->Rchain->get_representative_in(*cv2_it)
                           + HLS->Rchain->get_representative_out(*cv1_it) + HLS->Rchain->get_representative_out(*cv2_it) > 0))
                   {
                       if(std::find(unreachable_nodes[*cv1_it].begin(), unreachable_nodes[*cv1_it].end(), *cv2_it)!= unreachable_nodes[*cv1_it].end())
                       {
                          _w=0;
                          std::cerr << "possible sharing between " << GET_NAME(sdg, *cv1_it) << " and " << GET_NAME(sdg, *cv2_it) << ", but.... maybe next time" << std::endl;
                       }
                       //if(!(are_reaching_nodes(*cv1_it, *cv2_it, &workingset, saodg, &reachable_from, &unreachable_nodes) || are_reaching_nodes(*cv2_it, *cv1_it, &workingset2, saodg, &reachable_from, &unreachable_nodes)))
                       if(!(saodg->IsReachable(*cv1_it, *cv2_it) || saodg->IsReachable(*cv2_it, *cv1_it)))
                       {
                           unreachable_nodes[*cv1_it].insert(*cv2_it);
                           unreachable_nodes[*cv2_it].insert(*cv1_it);
                           _w=0;
                           //std::cerr << "possible sharing between " << GET_NAME(sdg, *cv1_it) << " and " << GET_NAME(sdg, *cv2_it) << ", but.... maybe next time" << std::endl;
                       }
                   }

               }
#endif




               /// add compatibility edge with computed weight on both the directions
               if(_w>0)
               {
                  cdfc_graphs_collection->AddEdge(s2c[*cv1_it], s2c[*cv2_it], COMPATIBILITY_EDGE, _w);
                  cdfc_graphs_collection->AddEdge(s2c[*cv2_it], s2c[*cv1_it], COMPATIBILITY_EDGE, _w);
               }
            }
         }
      }

      const CdfcGraphRef CG = CdfcGraphRef(new CdfcGraph(cdfc_graphs_collection, COMPATIBILITY_EDGE));
      const CdfcGraphConstRef CD_chained_graph = CdfcGraphConstRef(new CdfcGraph(cdfc_graphs_collection, CD_EDGE));
      std::string functionName = HLS->FB->CGetBehavioralHelper()->get_function_name();
      if(Param->getOption<bool>(OPT_print_dot))
      {
         CD_chained_graph->WriteDot("HLS_CD_CHAINED.dot");
      }
      /// compute levels
      std::unordered_map<vertex, int> cd_levels;
      /// topologically sort vertex of CD_EDGE based graph
      std::deque<vertex> Csorted_vertices;
      topological_based_sorting(*CD_chained_graph, c2s, sdg, std::front_inserter(Csorted_vertices));
      std::deque<vertex>::const_iterator sv_it_end = Csorted_vertices.end();
      for(std::deque<vertex>::const_iterator sv_it = Csorted_vertices.begin(); sv_it != sv_it_end; ++sv_it)
      {
         cd_levels[c2s[*sv_it]] = 0;
         InEdgeIterator ie, ie_end;
         for(boost::tie(ie, ie_end) = boost::in_edges(*sv_it, *CD_chained_graph); ie != ie_end; ++ie)
            if(boost::in_degree(boost::source(*ie,*CD_chained_graph), *CG) != 0)
               cd_levels[c2s[*sv_it]] = std::max(cd_levels[c2s[*sv_it]], 1+cd_levels[c2s[boost::source(*ie, *CD_chained_graph)]]);
            else
               cd_levels[c2s[*sv_it]] = std::max(cd_levels[c2s[*sv_it]], cd_levels[c2s[boost::source(*ie, *CD_chained_graph)]]);
      }

      /// remove all cycles from the cdfc graph
      const CdfcGraphConstRef cdfc = CdfcGraphConstRef(new CdfcGraph(cdfc_graphs_collection, CD_EDGE|COMPATIBILITY_EDGE));

      unsigned int k=2;
      std::deque<EdgeDescriptor> candidate_edges;
      std::unordered_set<vertex> no_cycles;
      bool restart;

      do
      {
         restart = false;
         std::unordered_set<vertex>::const_iterator cit_end = all_candidate_vertices.end();
         for(std::unordered_set<vertex>::const_iterator cit = all_candidate_vertices.begin(); cit != cit_end; ++cit)
         {
            if(no_cycles.find(*cit) != no_cycles.end()) continue;
            vertex start = s2c[*cit];

            if(cd_levels.find(*cit)->second != 0 && boost::in_degree(start, *CG) != 0)
            {
               bool found_a_loop;
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Search loops starting from -> " + GET_NAME(sdg, *cit) + " iteration " + STR(k));
               found_a_loop = false_loop_search(start, k, start, cdfc, CG, candidate_edges);
               if(!found_a_loop)
                  no_cycles.insert(*cit);
               restart |= found_a_loop;
               THROW_ASSERT(!found_a_loop || candidate_edges.size()>=1, "something of unexpected happen");
               while(found_a_loop)
               {
                  /// remove the loop
                  const std::deque<EdgeDescriptor>::const_iterator ce_it_end = candidate_edges.end();
                  std::deque<EdgeDescriptor>::const_iterator ce_it = candidate_edges.begin();
                  EdgeDescriptor cand_e = *ce_it;
                  ++ce_it;
                  vertex cand_src = boost::source(cand_e, *CG);
                  vertex cand_tgt = boost::target(cand_e, *CG);
                  int cand_level_difference = std::abs(cd_levels[c2s[cand_src]]-cd_levels[c2s[cand_tgt]]);
                  size_t cand_out_degree = boost::out_degree(cand_src, *CG) + boost::out_degree(cand_tgt, *CG);
                  int cand_edge_weight = CG->CGetCdfcEdgeInfo(cand_e)->edge_weight;
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "analyzing compatibility between operations " + GET_NAME(sdg, c2s[cand_src]) + " and " + GET_NAME(sdg, c2s[cand_tgt]) + " - ld = " + STR(cand_level_difference) + " - d= " + STR(cand_out_degree) + " - w = " + STR(cand_edge_weight));

                  for(; ce_it != ce_it_end; ++ce_it)
                  {
                     EdgeDescriptor e = *ce_it;
                     vertex src = boost::source(e, *CG);
                     vertex tgt = boost::target(e, *CG);
                     int level_difference = std::abs(cd_levels[c2s[src]]-cd_levels[c2s[tgt]]);
                     size_t out_degree = boost::out_degree(src, *CG) + boost::out_degree(tgt, *CG);
                     int edge_weight = CG->CGetCdfcEdgeInfo(e)->edge_weight;
                     PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "analyzing compatibility between operations " + GET_NAME(sdg, c2s[src]) + " and " + GET_NAME(sdg, c2s[tgt]) + " - ld = " + STR(level_difference) + " - d= " + STR(out_degree) + " - w = " + STR(edge_weight));
                     if(level_difference> cand_level_difference ||
                           (level_difference == cand_level_difference && out_degree > cand_out_degree) ||
                           (level_difference == cand_level_difference && out_degree == cand_out_degree && edge_weight < cand_edge_weight)
                           )
                     {
                        cand_src = src;
                        cand_tgt = tgt;
                        cand_e = e;
                        cand_level_difference = level_difference;
                        cand_out_degree = out_degree;
                        cand_edge_weight = edge_weight;
                     }
                  }
                  /// remove both the compatibility edges
                  cdfc_graphs_collection->RemoveSelector(cand_e);
                  bool exists;
                  boost::tie(cand_e, exists) = boost::edge(cand_tgt, cand_src, *CG);
                  THROW_ASSERT(exists, "edge already removed");
                  cdfc_graphs_collection->RemoveSelector(cand_e);
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Removed compatibility between operations " + GET_NAME(sdg, c2s[cand_src]) + "(" + GET_OP(sdg, c2s[cand_src]) + ")" + " and " + GET_NAME(sdg, c2s[cand_tgt]));
                  candidate_edges.clear();

                  /// search another loop
                  found_a_loop = false_loop_search(start, k, start, cdfc, CG, candidate_edges);
               }

               THROW_ASSERT(candidate_edges.empty(), "candidate_cycle has to be empty");
            }
            else
               no_cycles.insert(*cit);
         }
         ++k;
      } while(restart);

      if(Param->getOption<bool>(OPT_print_dot))
      {
         cdfc->WriteDot("HLS_CD_COMP.dot");
      }

      /// partition vertices for clique covering or bind the easy functional units
      std::map<unsigned int, unsigned int> numModule;
      std::map<unsigned int, std::unordered_set<vertex>, cdfc_resource_ordering_functor > partitions(r_functor);
      for(std::map<unsigned int, std::set<vertex>, cdfc_resource_ordering_functor >::const_iterator fu_cv_it = candidate_vertices.begin(); fu_cv_it != fu_cv_it_end; ++fu_cv_it)
      {
         fu_unit = fu_cv_it->first;
         const std::set<vertex>::const_iterator cv_it_end = fu_cv_it->second.end();
         for(std::set<vertex>::const_iterator cv_it = fu_cv_it->second.begin(); cv_it != cv_it_end;++cv_it)
         {
            /// check easy binding
            if(boost::out_degree(s2c[*cv_it], *CG) == 0)
            {
               unsigned int num=0;
               if(numModule.find(fu_unit) == numModule.end())
                  numModule[fu_unit] = 1;
               else
                  num = numModule[fu_unit]++;
               ++total_modules_allocated;
               total_resource_area += all.get_area(fu_unit);
               total_DSPs += all.get_DSPs(fu_unit);
               fu.bind(*cv_it, fu_unit, num);
            }
            else
            {
               partitions[fu_unit].insert(s2c[*cv_it]);
            }
         }
      }

      /// solve the binding problem for all the partitions
      std::map<unsigned int, std::unordered_set<vertex>, cdfc_resource_ordering_functor >::const_iterator p_it_end = partitions.end();
      for(std::map<unsigned int, std::unordered_set<vertex>, cdfc_resource_ordering_functor >::const_iterator p_it = partitions.begin(); p_it_end != p_it; ++p_it)
      {
         THROW_ASSERT(p_it->second.size()>1, "bad projection");

         const double mux_time = all.estimate_mux_time(p_it->first);

         type_t clique_covering_method_used;
         if(HLS->ALL->get_number_channels(p_it->first) > 1)
            clique_covering_method_used = BIPARTITE_MATCHING;
         else
            clique_covering_method_used = clique_covering_method;

         /// build the clique covering solver
         refcount< clique_covering<vertex> > module_clique;
         switch(clique_covering_method_used)
         {
            case TTT_FAST:
            {
               module_clique = clique_covering<vertex>::create_solver(clique_covering<vertex>::TTT_CLIQUE_COVERING_FAST);
               break;
            }
            case TTT_FAST2:
            {
               module_clique = clique_covering<vertex>::create_solver(clique_covering<vertex>::TTT_CLIQUE_COVERING_FAST2);
               break;
            }
            case TTT_FULL:
            {
               module_clique = clique_covering<vertex>::create_solver(clique_covering<vertex>::TTT_CLIQUE_COVERING);
               break;
            }
            case TTT_FULL2:
            {
               module_clique = clique_covering<vertex>::create_solver(clique_covering<vertex>::TTT_CLIQUE_COVERING2);
               break;
            }
            case TS:
            {
               module_clique = clique_covering<vertex>::create_solver(clique_covering<vertex>::TS_CLIQUE_COVERING);
               break;
            }
            case WEIGHTED_TS:
            {
               module_clique = clique_covering<vertex>::create_solver(clique_covering<vertex>::TS_WEIGHTED_CLIQUE_COVERING);
               break;
            }
            case COLORING:
            {
               module_clique = clique_covering<vertex>::create_solver(clique_covering<vertex>::COLORING);
               break;
            }
            case WEIGHTED_COLORING:
            {
               module_clique = clique_covering<vertex>::create_solver(clique_covering<vertex>::WEIGHTED_COLORING);
               break;
            }
            case BIPARTITE_MATCHING:
            {
               module_clique = clique_covering<vertex>::create_solver(clique_covering<vertex>::BIPARTITE_MATCHING);
               break;
            }
            default:
               THROW_ERROR("This clique covering algorithm has not been implemented");
         }
         /// add vertex to the clique covering solver
         std::unordered_set<vertex>::const_iterator vert_it_end = p_it->second.end();
         for(std::unordered_set<vertex>::const_iterator vert_it = p_it->second.begin(); vert_it != vert_it_end; ++vert_it)
         {
            std::string el1_name = GET_NAME(sdg, c2s[*vert_it]) + "(" + GET_OP(sdg, c2s[*vert_it]) + ")";
            module_clique->add_vertex(c2s[*vert_it],el1_name);
         }

         if(clique_covering_method_used==BIPARTITE_MATCHING)
         {
            std::map<vertex, size_t> v2id;
            size_t max_id = 0, curr_id;
            for(std::unordered_set<vertex>::const_iterator vert_it = p_it->second.begin(); vert_it != vert_it_end; ++vert_it)
            {
               const std::set<vertex>& running_states = HLS->Rliv->get_state_where_run(c2s[*vert_it]);
               const std::set<vertex>::const_iterator rs_it_end = running_states.end();
               for(std::set<vertex>::const_iterator rs_it = running_states.begin(); rs_it != rs_it_end; ++rs_it)
               {
                  if(v2id.find(*rs_it) == v2id.end())
                  {
                     curr_id = max_id;
                     v2id[*rs_it] = max_id;
                     ++max_id;
                  }
                  else
                     curr_id = v2id.find(*rs_it)->second;
                  module_clique->add_subpartitions(curr_id, c2s[*vert_it]);
               }
            }
         }
         /// add the edges
         EdgeIterator cg_ei,cg_ei_end;
         const CdfcGraphConstRef CG_subgraph(new CdfcGraph(cdfc_graphs_collection, COMPATIBILITY_EDGE, p_it->second));
         for(boost::tie(cg_ei,cg_ei_end) = boost::edges(*CG_subgraph); cg_ei != cg_ei_end; ++cg_ei)
         {
            vertex src = c2s[boost::source(*cg_ei, *CG_subgraph)];
            vertex tgt = c2s[boost::target(*cg_ei, *CG_subgraph)];
            if(src > tgt) continue;/// only one edge is needed to build the undirected compatibility graph
            _w = weight_computation(src,tgt,p_it->first, mux_time, dfg, fu, slack_time);
            //_w = CGET_EDGE_INFO(CG, weight_edgeInfo, *cg_ei)->edge_weight;
            if(_w>0)
               module_clique->add_edge(src, tgt, _w);
         }

         if (debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
            module_clique->writeDot(functionName+"_"+all.get_string_name(p_it->first) +"_cdfc_module_binding_graph.dot");

         module_clique->suggest_min_resources(HLS->ALL->get_number_channels(p_it->first));

         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Compatibility graph created!");

         PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Starting clique covering on a graph with "+ STR(p_it->second.size()) + " vertices (method:"+ STR(clique_covering_method_used)+")");

         /// performing clique covering
         std::string res_name = all.get_fu_name(p_it->first).first;
         std::string lib_name = HLS->HLS_T->get_technology_manager()->get_library(res_name);
         ///std::cerr << "Clique covering for " << res_name << std::endl;
         if(
               (HLS->ALL->get_number_channels(p_it->first) > 1) ||
               lib_name  == WORK_LIBRARY ||
               (HLS->ALL->get_number_fu(p_it->first) != INFINITE_UINT)
               )
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Disabled slack based clique covering for: "+ res_name);
            module_clique->exec(no_filter_clique<vertex>());
         }
         else
            module_clique->exec(slack_based_filtering(slack_time, mux_time, HLS, HLSMgr));

         PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Number of cliques covering the graph: "+ STR(module_clique->num_vertices()));
         PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Clique covering algorithm completed!");
         total_modules_allocated +=module_clique->num_vertices();

         /// retrieve the solution
         for (unsigned int i = 0; i < module_clique->num_vertices(); ++i)
         {
            const std::set<vertex> &clique = module_clique->get_clique(i);
            if(clique.empty()) continue;
            fu_unit = fu.get_assign(*(clique.begin()));
            THROW_ASSERT(fu_unit == p_it->first, "unexpected case");
            unsigned int num=0;
            if(numModule.find(fu_unit) == numModule.end())
               numModule[fu_unit] = 1;
            else
               num = numModule[fu_unit]++;
            total_resource_area += all.get_area(fu_unit);
            total_DSPs += all.get_DSPs(fu_unit);
            PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Sharing degree for " + all.get_string_name(fu_unit) + "_" + STR(num) + " = " + STR(clique.size()));

            std::set<vertex>::iterator v = clique.begin();
            for (; v != clique.end(); v++)
            {
               /// storing new binding results
               fu.bind(*v, fu_unit, num);
            }
         }
      }
   }
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Module allocation performed!");
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Number of modules instantiated: " + STR(total_modules_allocated));
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Estimated resources area (no Muxes and address logic): " + STR(total_resource_area));
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Estimated number of DSPs: " + STR(total_DSPs));
}

std::string cdfc_module_binding::get_kind_text() const
{
   return "cdfc-module-binding";
}

bool cdfc_module_binding::false_loop_search(vertex src, unsigned k, vertex start, const CdfcGraphConstRef cdfc, const CdfcGraphConstRef cg, std::deque<EdgeDescriptor> &candidate_edges)
{
   OutEdgeIterator oe_cg, oe_end_cg;
   for(boost::tie(oe_cg, oe_end_cg) = boost::out_edges(src, *cg); oe_cg != oe_end_cg; ++oe_cg)
   {
      if(false_loop_search_cdfc_1(boost::target(*oe_cg, *cg), 1, k, start, cdfc, cg, candidate_edges))
      {
         candidate_edges.push_front(*oe_cg);
         return true;
      }
   }
   return false;
}

bool cdfc_module_binding::false_loop_search_cdfc_1(vertex src, unsigned int level, unsigned k, vertex start, const CdfcGraphConstRef cdfc, const CdfcGraphConstRef cg, std::deque<EdgeDescriptor> &candidate_edges)
{
   OutEdgeIterator oe_cdfc, oe_end_cdfc;
   if(level>k)
      return false;
   for(boost::tie(oe_cdfc, oe_end_cdfc) = boost::out_edges(src, *cdfc); oe_cdfc != oe_end_cdfc; ++oe_cdfc)
   {
      vertex tgt = boost::target(*oe_cdfc, *cdfc);
      bool is_cg_edge;
      EdgeDescriptor cg_e;
      boost::tie(cg_e, is_cg_edge) = boost::edge(src, tgt, *cg);
      if(!is_cg_edge && false_loop_search_cdfc_more(tgt, level, k, start, cdfc, cg, candidate_edges))
         return true;
   }
   return false;
}

bool cdfc_module_binding::false_loop_search_cdfc_more(vertex src, unsigned int level, unsigned k, vertex start, const CdfcGraphConstRef cdfc, const CdfcGraphConstRef cg, std::deque<EdgeDescriptor> &candidate_edges)
{
   OutEdgeIterator oe_cdfc, oe_end_cdfc;
   if(start==src) return true;
   for(boost::tie(oe_cdfc, oe_end_cdfc) = boost::out_edges(src, *cdfc); oe_cdfc != oe_end_cdfc; ++oe_cdfc)
   {
      vertex tgt = boost::target(*oe_cdfc, *cdfc);
      EdgeDescriptor cg_e=*oe_cdfc;
      if(cdfc->GetSelector(cg_e) & COMPATIBILITY_EDGE)
      {
         if(false_loop_search_cdfc_1(tgt, level+1, k, start, cdfc, cg, candidate_edges))
         {
            candidate_edges.push_front(cg_e);
            return true;
         }
      }
      else
      {
         if(false_loop_search_cdfc_more(tgt, level, k, start, cdfc, cg, candidate_edges))
            return true;
      }
   }
   return false;
}


int cdfc_module_binding::weight_computation(vertex v1, vertex v2, unsigned int fu_s1, const double mux_time, const OpGraphConstRef fdfg, fu_binding& /*fu*/, const std::unordered_map<vertex, double> &slack_time)
{
   int _w = 1;
   unsigned int threshold;
   //std::cerr << "weight computation" << std::endl;
   std::vector<HLS_manager::io_binding_type> vars_read1 = HLSMgr->get_required_values(HLS->functionId, v1);
   std::vector<HLS_manager::io_binding_type> vars_read2 = HLSMgr->get_required_values(HLS->functionId, v2);
   if(vars_read1.size() == vars_read2.size())
   {
      threshold = 2+static_cast<unsigned int>(vars_read1.size());
      std::map<vertex, vertex> converter;
      std::set<vertex> cluster;
      converter[v1] = v1;
      cluster.insert(v1);
      converter[v2] = v2;
      cluster.insert(v2);
      unsigned int total_muxes;
      unsigned int max_mux_ins;
      unsigned int max_mux_out;
      int n_inputs = static_cast<int>(vars_read1.size());
      estimate_muxes(cluster, total_muxes, max_mux_ins, max_mux_out, converter, HLSMgr, HLS);
      //std::cerr << "total_muxes " << total_muxes << " max_mux_ins " << max_mux_ins << " max_mux_out " << max_mux_out << " n_inputs " << n_inputs << " " << GET_OP(sdg, v1) + "-" + GET_OP(sdg, v2) << std::endl;
      if(static_cast<int>(total_muxes-max_mux_out)>=2*n_inputs)
      {
         if(max_mux_out>1)
            _w = 1;
         else
            _w = 1+n_inputs;
      }
      else if(max_mux_ins>1)
      {
         if(max_mux_out>1)
            _w = 1+2*n_inputs-static_cast<int>(total_muxes-max_mux_out);
         else
            _w = 2+2*n_inputs-static_cast<int>(total_muxes-max_mux_out);
      }
      else if(max_mux_out>1)
         _w = 2+n_inputs;
      else
         _w = 3+n_inputs;

   }
   else
      threshold = 2+static_cast<unsigned int>(std::max(vars_read1.size(), vars_read2.size()));
#if 0
   /// check for data flow edges
   /// try to put together communicating operations
   boost::graph_traits<graph>::adjacency_iterator ai, a_end;
   boost::tie(ai, a_end) = boost::adjacent_vertices(v2, *sdg);
   for(;ai!=a_end; ++ai)
      if(*ai == v2)
      {
         _w += 2;
         break;
      }
   boost::tie(ai, a_end) = boost::adjacent_vertices(v2, *sdg);
   for(;ai!=a_end; ++ai)
      if(*ai == v1)
      {
         _w += 2;
         break;
      }
#endif

   double resource_area = HLS->ALL->compute_normalized_area(fu_s1, std::max(vars_read1.size(), vars_read2.size()));

#define SMALL_NORMALIZED_RESOURCE_AREA 1.1
   //std::cerr << HLS->ALL->get_fu_name(fu_s1).first << "|" << GET_NAME(fdfg, v1) << "(" << GET_OP(fdfg, v1) << ")-" << GET_NAME(fdfg, v2) << "=" << _w << " Slack v1 " << slack_time.find(v1)->second << " Slack v2 " << slack_time.find(v2)->second << " mux time " << mux_time << std::endl;
   if(HLS->ALL->get_number_channels(fu_s1) < 2 && _w <= static_cast<int>(threshold) && HLS->ALL->get_cycles(fu_s1, v1, fdfg) <= 1  && ((slack_time.find(v1)->second < mux_time) || (slack_time.find(v2)->second < mux_time)))
      _w=0;
   else if((
              HLS->ALL->get_number_channels(fu_s1) < 2 &&
              HLS->ALL->get_number_fu(fu_s1) == INFINITE_UINT &&
            resource_area <= SMALL_NORMALIZED_RESOURCE_AREA &&
            _w < static_cast<int>(threshold))/* || HLS->ALL->get_worst_execution_time(fu_s1) == 0.0*/)
      _w=0;
   /*else if(resource_area == 1.0 && _w <= 2)
   _w=0;*/
   /*else if(resource_area <= 1.0 && _w > 4)
   ++_w;
else if(resource_area/3 < 1.0)
   --_w;
if(resource_area/3>1.0)
   ++_w;*/
   //std::cerr << GET_NAME(fdfg, v1) << "(" << GET_OP(fdfg, v1) << ")-" << "-" << GET_NAME(fdfg, v2) << "=" << _w << "|" << resource_area<< std::endl;

   if(_w>31) _w=31;
   return _w;
}

