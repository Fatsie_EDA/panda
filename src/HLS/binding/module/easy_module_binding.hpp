/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file easy_module_binding.hpp
 * @brief Partial module binding based on simple conditions
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @version $Revision$
 * @date $Date$
*/

#ifndef EASY_MODULE_BINDING_HPP
#define EASY_MODULE_BINDING_HPP

///superclass include
#include "hls_step.hpp"
REF_FORWARD_DECL(easy_module_binding);

/**
 * Class managing a partial module binding based on simple conditions
 */
class easy_module_binding : public HLS_step
{
   public:

      /**
       * Constructor
       */
      easy_module_binding(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      ~easy_module_binding();

      /**
       * Performs module binding exploiting the control data flow chained graph
       */
      void exec();

      /**
       * Returns the name of the implemented algorithm
       */
      std::string get_kind_text() const;

};
///refcount definition of the class
typedef refcount<easy_module_binding> easy_module_bindingRef;

#endif
