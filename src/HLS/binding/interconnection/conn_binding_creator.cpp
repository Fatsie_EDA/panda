/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file conn_binding_creator.cpp
 * @brief Base class for all interconnection binding algorithms.
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "conn_binding_creator.hpp"

///implemented algorithms
#include "mux_connection_binding.hpp"

#include "function_behavior.hpp"
#include "behavioral_helper.hpp"

#include "hls.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"

#include "Parameter.hpp"

conn_binding_creator::conn_binding_creator(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   HLS_step(_Param, _HLSMgr, _funId)
{

}

conn_binding_creator::~conn_binding_creator()
{

}

conn_binding_creatorRef conn_binding_creator::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string algorithm = "MUX_INTERCONNECTION_BINDING";
   if (CE_XVM(algorithm, node)) LOAD_XVM(algorithm, node);

   algorithm_t alg_t = MUX_INTERCONNECTION_BINDING;
   if (algorithm == "MUX_INTERCONNECTION_BINDING")
      alg_t = MUX_INTERCONNECTION_BINDING;
   else
      THROW_ERROR("Datapath creation algorithm \"" + algorithm + "\" currently not supported");
   return factory(alg_t, Param, HLSMgr, funId);
}

conn_binding_creatorRef conn_binding_creator::factory(algorithm_t type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   switch(type)
   {
      case MUX_INTERCONNECTION_BINDING:
         return conn_binding_creatorRef(new mux_connection_binding(Param, HLSMgr, funId));
      default:
          THROW_UNREACHABLE("Connection type not yet supported " + boost::lexical_cast<std::string>(type));
   }
   return conn_binding_creatorRef();
}


void conn_binding_creator::add_parameter_ports()
{
   const BehavioralHelperConstRef BH = HLS->FB->CGetBehavioralHelper();

   ///list containing the parameters of the original function (representing input and output values)
   const std::list<unsigned int>& parameters = BH->get_parameters();
   PRINT_DBG_STRING(DEBUG_LEVEL_PEDANTIC, debug_level, "Parameters values: ");
   for (std::list<unsigned int>::const_iterator i = parameters.begin(); i != parameters.end(); i++)
   {
      PRINT_DBG_STRING(DEBUG_LEVEL_PEDANTIC, debug_level, BH->PrintVariable(*i) + "(");
      PRINT_DBG_STRING(DEBUG_LEVEL_PEDANTIC, debug_level, "IN)-@"+STR(*i) + " ");
      input_ports[*i] = HLS->Rconn->bind_port(*i, conn_binding::IN);
   }
   const unsigned int return_type_index = BH->GetFunctionReturnType(BH->get_function_index());
   if(return_type_index)
   {
      PRINT_DBG_STRING(DEBUG_LEVEL_PEDANTIC, debug_level, "Return format " + BH->print_type(return_type_index) + " (OUT) ");
      output_ports[return_type_index] = HLS->Rconn->bind_port(return_type_index, conn_binding::OUT);
   }
   PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "");
}

