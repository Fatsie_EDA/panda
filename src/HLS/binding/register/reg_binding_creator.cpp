/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file reg_binding_creator.cpp
 * @brief Base class for all register allocation algorithms
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "reg_binding_creator.hpp"

#include "refcount.hpp"
#include "utility.hpp"
#include "dbgPrintHelper.hpp"

#include "function_behavior.hpp"

#include "vertex_coloring_register.hpp"
#if HAVE_EXPERIMENTAL
#include "left_edge_register.hpp"
#include "k_cofamily_register.hpp"
#endif
#include "chordal_coloring_register.hpp"
#include "unique_binding_register.hpp"
#include "weighted_clique_register.hpp"

#include "hls.hpp"
#include "liveness.hpp"
#include "reg_binding.hpp"
#include "storage_value_insertion.hpp"
#include "Parameter.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"

#include <iostream>
#include <boost/version.hpp>


reg_binding_creator::reg_binding_creator(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   HLS_step(_Param, _HLSMgr, _funId),
   register_lower_bound(0),
   svi_algorithm(HLS->svi_data)
{

}

reg_binding_creator::~reg_binding_creator()
{

}

reg_binding_creatorRef reg_binding_creator::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string algorithm = "COLORING";
   if (CE_XVM(algorithm, node)) LOAD_XVM(algorithm, node);

   type_t reg_algorithm = COLORING;
   if (algorithm == "COLORING")
      reg_algorithm = COLORING;
   else if (algorithm == "CHORDAL_COLORING")
      reg_algorithm = CHORDAL_COLORING;
   else if (algorithm == "WEIGHTED_COLORING")
      reg_algorithm = WEIGHTED_COLORING;
   else if (algorithm == "BIPARTITE_MATCHING")
      reg_algorithm = BIPARTITE_MATCHING;
   else if (algorithm == "TTT_CLIQUE_COVERING")
      reg_algorithm = TTT_CLIQUE_COVERING;
   else if (algorithm == "UNIQUE_BINDING")
      reg_algorithm = UNIQUE_BINDING;
#if HAVE_EXPERIMENTAL
   else if (algorithm == "LEFT_EDGE")
      reg_algorithm = LEFT_EDGE;
   else if (algorithm == "K_COFAMILY")
      reg_algorithm = K_COFAMILY;
#endif
   else
      THROW_ERROR("Register allocation algorithm \"" + algorithm + "\" currently not supported");
   return factory(reg_algorithm, Param, HLSMgr, funId);
}

reg_binding_creatorRef reg_binding_creator::factory(type_t type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
#ifndef NDEBUG
   int debug_level = Param->getOption<int>(OPT_debug_level);
#endif
   switch (type)
   {
      case COLORING:
         PRINT_DBG_MEX(OUTPUT_LEVEL_PEDANTIC, debug_level, "\t   creating COLORING solver");
         return reg_binding_creatorRef(new vertex_coloring_register(Param, HLSMgr, funId));
      case CHORDAL_COLORING:
         PRINT_DBG_MEX(OUTPUT_LEVEL_PEDANTIC, debug_level, "\t   creating CHORDAL_COLORING solver");
         return reg_binding_creatorRef(new chordal_coloring_register(Param, HLSMgr, funId));
      case WEIGHTED_COLORING:
         PRINT_DBG_MEX(OUTPUT_LEVEL_PEDANTIC, debug_level, "\t   creating WEIGHTED_COLORING solver");
         return reg_binding_creatorRef(new weighted_clique_register(Param, HLSMgr, funId, weighted_clique_register::WEIGHTED_COLORING));
      case BIPARTITE_MATCHING:
         PRINT_DBG_MEX(OUTPUT_LEVEL_PEDANTIC, debug_level, "\t   creating BIPARTITE_MATCHING solver");
         return reg_binding_creatorRef(new weighted_clique_register(Param, HLSMgr, funId, weighted_clique_register::BIPARTITE_MATCHING));
      case TTT_CLIQUE_COVERING:
         PRINT_DBG_MEX(OUTPUT_LEVEL_PEDANTIC, debug_level, "\t   creating TTT_CLIQUE_COVERING solver");
         return reg_binding_creatorRef(new weighted_clique_register(Param, HLSMgr, funId, weighted_clique_register::TTT_CLIQUE_COVERING));
      case UNIQUE_BINDING:
         PRINT_DBG_MEX(OUTPUT_LEVEL_PEDANTIC, debug_level, "\t   creating UNIQUE_BINDING solver");
         return reg_binding_creatorRef(new unique_binding_register(Param, HLSMgr, funId));
#if HAVE_EXPERIMENTAL
      case LEFT_EDGE:
         PRINT_DBG_MEX(OUTPUT_LEVEL_PEDANTIC, debug_level, "\t   creating LEFT_EDGE solver");
         return reg_binding_creatorRef(new left_edge_register(Param, HLSMgr, funId));
      case K_COFAMILY:
         PRINT_DBG_MEX(OUTPUT_LEVEL_PEDANTIC, debug_level, "\t   creating K_COFAMILY solver");
         return reg_binding_creatorRef(new k_cofamily_register(Param, HLSMgr, funId));
#endif
      default:
         THROW_UNREACHABLE("Register allocation algorithm not yet supported: " + STR(type));
   }
   return reg_binding_creatorRef();
}
