/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file unique_binding_register.cpp
 * @brief Class implementation of unique binding register allocation algorithm
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @version $Revision$
 * @date $Date$

*/
#include "unique_binding_register.hpp"

#include "hls.hpp"

#include "reg_binding.hpp"
#include "storage_value_insertion.hpp"
#include "liveness.hpp"

#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"
#include "boost/lexical_cast.hpp"

unique_binding_register::unique_binding_register(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   reg_binding_creator(_Param, _HLSMgr, _funId)
{

}

unique_binding_register::~unique_binding_register()
{

}

void unique_binding_register::exec()
{
   THROW_ASSERT(HLS->Rliv, "Liveness analysis not yet computed");
   HLS->Rreg = reg_bindingRef(new reg_binding(HLS));
   unsigned int num_regs = 0;
   const std::list<vertex> & support = HLS->Rliv->get_support();

   const std::list<vertex>::const_iterator vEnd = support.end();
   for(std::list<vertex>::const_iterator vIt = support.begin(); vIt != vEnd; vIt++)
   {
      const std::set<unsigned int>& live = HLS->Rliv->get_live_in(*vIt);
      std::set<unsigned int>::iterator k_end = live.end();
      for(std::set<unsigned int>::iterator k = live.begin(); k != k_end; k++)
      {
         unsigned int storage_value_index = svi_algorithm->get_storage_value_index(*vIt, *k);
         HLS->Rreg->bind(storage_value_index, storage_value_index);
      }
   }
   HLS->Rreg->set_used_regs(svi_algorithm->get_number_of_storage_values());
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, std::string("Register allocation algorithm obtains ") + boost::lexical_cast<std::string>(num_regs) + " registers");

}

std::string unique_binding_register::get_kind_text() const
{
   return "unique-reg-binding";
}
