/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file weighted_clique_register.cpp
 * @brief Weighted clique covering register allocation procedure
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "weighted_clique_register.hpp"

#include "clique_covering.hpp"
#include "hls.hpp"
#include "reg_binding.hpp"
#include "liveness.hpp"
#include "storage_value_insertion.hpp"


weighted_clique_register::weighted_clique_register(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId, clique_covering_method _method)
   : compatibility_based_register(_Param, _HLSMgr, _funId),
     method(_method)
{

}

weighted_clique_register::~weighted_clique_register()
{

}

void weighted_clique_register::exec()
{

   refcount< clique_covering<CG_vertex_descriptor> > register_clique;
   switch(method)
   {
      case TTT_CLIQUE_COVERING:
      {
         register_clique = clique_covering<CG_vertex_descriptor>::create_solver(clique_covering<CG_vertex_descriptor>::TTT_CLIQUE_COVERING);
         break;
      }
      case WEIGHTED_COLORING:
      {
         register_clique = clique_covering<CG_vertex_descriptor>::create_solver(clique_covering<CG_vertex_descriptor>::WEIGHTED_COLORING);
         break;
      }
      case BIPARTITE_MATCHING:
      {
         register_clique = clique_covering<CG_vertex_descriptor>::create_solver(clique_covering<CG_vertex_descriptor>::BIPARTITE_MATCHING);
         break;
      }
      default:
         THROW_ERROR("This clique covering algorithm has not been implemented");
   }

   create_compatibility_graph();

   std::vector<CG_vertex_descriptor>::const_iterator v_it_end = verts.end();
   unsigned int vertex_index = 0;
   unsigned int num_registers;
   for(std::vector<CG_vertex_descriptor>::const_iterator v_it = verts.begin(); v_it != v_it_end; ++v_it, ++vertex_index)
      register_clique->add_vertex(*v_it, STR(vertex_index));
   if(vertex_index>0)
   {
      boost::graph_traits<compatibility_graph>::edge_iterator cg_ei,cg_ei_end;
      for(boost::tie(cg_ei,cg_ei_end) = boost::edges(CG); cg_ei != cg_ei_end; ++cg_ei)
      {
         CG_vertex_descriptor src = boost::source(*cg_ei, CG);
         CG_vertex_descriptor tgt = boost::target(*cg_ei, CG);
         register_clique->add_edge(src, tgt, CG[*cg_ei].weight);
      }
      /// performing clique covering
      register_clique->exec(no_filter_clique<CG_vertex_descriptor>());
      /// vertex to clique map
      std::map<CG_vertex_descriptor, unsigned int> v2c;
      /// retrieve the solution
      num_registers = static_cast<unsigned int>(register_clique->num_vertices());
      for (unsigned int i = 0; i < num_registers; ++i)
      {
         std::set<CG_vertex_descriptor> clique = register_clique->get_clique(i);
         std::set<CG_vertex_descriptor>::const_iterator v_end = clique.end();
         for (std::set<CG_vertex_descriptor>::const_iterator v = clique.begin(); v != v_end; ++v)
         {
            v2c[*v] = i;
         }
      }
      /// finalize
      HLS->Rreg = reg_bindingRef(new reg_binding(HLS));
      const std::list<vertex> & support = HLS->Rliv->get_support();

      const std::list<vertex>::const_iterator vEnd = support.end();
      for(std::list<vertex>::const_iterator vIt = support.begin(); vIt != vEnd; vIt++)
      {
         const std::set<unsigned int>& live = HLS->Rliv->get_live_in(*vIt);
         std::set<unsigned int>::iterator k_end = live.end();
         for(std::set<unsigned int>::iterator k = live.begin(); k != k_end; k++)
         {
            unsigned int storage_value_index = svi_algorithm->get_storage_value_index(*vIt, *k);
            HLS->Rreg->bind(storage_value_index, v2c[verts[storage_value_index]]);
         }
      }
   }
   else
   {
      HLS->Rreg = reg_bindingRef(new reg_binding(HLS));
      num_registers = 0;
   }
   HLS->Rreg->set_used_regs(num_registers);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, std::string("Register allocation algorithm obtains ") + (num_registers == register_lower_bound ? "an optimal" : "a sub-optimal") + " result: " + STR(num_registers) + " registers");

}

std::string weighted_clique_register::get_kind_text() const
{
   return "weighted_clique_register_"+STR(method);
}
