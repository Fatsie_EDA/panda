/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file vertex_coloring_register.cpp
 * @brief Class implementation of a coloring based register allocation algorithm
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 * 
*/
#include "vertex_coloring_register.hpp"

#include "hls.hpp"

#include "reg_binding.hpp"
#include "liveness.hpp"
#include "storage_value_insertion.hpp"

#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"
#include "dsatur2_coloring.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <vector>

vertex_coloring_register::vertex_coloring_register(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId)
   : conflict_based_register(_Param, _HLSMgr, _funId)
{

}

vertex_coloring_register::~vertex_coloring_register()
{

}

void vertex_coloring_register::exec()
{
   create_conflict_graph();
   /// coloring based on DSATUR 2 heuristic
   cg_vertices_size_type num_colors = dsatur2_coloring(cg, color);

   /// finalize
   HLS->Rreg = reg_bindingRef(new reg_binding(HLS));
   const std::list<vertex> & support = HLS->Rliv->get_support();

   const std::list<vertex>::const_iterator vEnd = support.end();
   for(std::list<vertex>::const_iterator vIt = support.begin(); vIt != vEnd; vIt++)
   {
      const std::set<unsigned int>& live = HLS->Rliv->get_live_in(*vIt);
      std::set<unsigned int>::iterator k_end = live.end();
      for(std::set<unsigned int>::iterator k = live.begin(); k != k_end; k++)
      {
         unsigned int storage_value_index = svi_algorithm->get_storage_value_index(*vIt, *k);
         HLS->Rreg->bind(storage_value_index, static_cast<unsigned int>(color[storage_value_index]));
      }
   }
   HLS->Rreg->set_used_regs(static_cast<unsigned int>(num_colors));
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, std::string("Register allocation algorithm obtains ") + (num_colors == register_lower_bound ? "an optimal" : "a sub-optimal") + " result: " + boost::lexical_cast<std::string>(num_colors) + " registers");
}

std::string vertex_coloring_register::get_kind_text() const
{
   return "vertex_coloring_register";
}
