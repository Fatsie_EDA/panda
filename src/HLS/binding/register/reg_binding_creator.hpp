/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file reg_binding_creator.hpp
 * @brief Base class for all the register allocation algorithms.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef _REG_BINDING_CREATOR_HPP_
#define _REG_BINDING_CREATOR_HPP_

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"

///superclass include
#include "hls_step.hpp"
REF_FORWARD_DECL(storage_value_insertion);
REF_FORWARD_DECL(reg_binding_creator);

/**
 * Generic class managing the different register allocation algorithms.
 */
class reg_binding_creator : public HLS_step
{
   public:

      ///implemented algorithms
      typedef enum
      {
         COLORING=0,
         CHORDAL_COLORING,
         WEIGHTED_COLORING,
         BIPARTITE_MATCHING,
         TTT_CLIQUE_COVERING,
   #if HAVE_EXPERIMENTAL
         LEFT_EDGE,
         K_COFAMILY,
   #endif
         UNIQUE_BINDING
      } type_t;

   protected:

      ///lower bound
      unsigned int register_lower_bound;

      ///class that represents the values to be stored in memory
      const storage_value_insertionRef svi_algorithm;

   public:

      /**
       * Constructor
       */
      reg_binding_creator(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      virtual ~reg_binding_creator();

      /**
       * Factory method
       */
      static
      reg_binding_creatorRef factory(type_t type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Factory method based on XML node
       */
      static
      reg_binding_creatorRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

};
///refcount definition of the class
typedef refcount<reg_binding_creator> reg_binding_creatorRef;

#endif
