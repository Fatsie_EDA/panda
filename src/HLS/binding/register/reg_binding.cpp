/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file reg_binding.cpp
 * @brief Class implementation of the register binding data structure.
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "reg_binding.hpp"
#include "register_obj.hpp"

#include "function_behavior.hpp"
#include "behavioral_helper.hpp"

#include "hls.hpp"
#include "hls_target.hpp"
#include "target_device.hpp"
#include "FPGA_device.hpp"
#include "liveness.hpp"

#include "structural_manager.hpp"
#include "technology_builtin.hpp"
#include "technology_manager.hpp"
#include "storage_value_insertion.hpp"
#include "Parameter.hpp"
#include "boost/lexical_cast.hpp"

reg_binding::reg_binding(const hlsRef& HLS_) :
   debug(HLS_->debug_level), used_regs(0), HLS(HLS_)
{
}

reg_binding::~reg_binding()
{
}

void reg_binding::print_el(std::ostream& os, const OpGraphConstRef, const_iterator &it) const
{
   os.width(VARIABLE_COLUMN_SIZE);
   os << " stored into register ";
   os.width(VARIABLE_COLUMN_SIZE);
   os << it->second->get_string();
   os.width(0);
}

void reg_binding::print_rowHead(std::ostream& os, const OpGraphConstRef, reg_binding::const_iterator & it) const
{
   const BehavioralHelperConstRef BH = HLS->FB->CGetBehavioralHelper();

   os.width(VARIABLE_COLUMN_SIZE);
   os.setf(std::ios_base::left, std::ios_base::adjustfield);
   os << "Storage Value: " << STR(it->first) << " ";
   os.width(VARIABLE_COLUMN_SIZE);
   os << "for variable " << BH->PrintVariable(HLS->svi_data->get_variable_index(it->first)) << " ";
}

std::set<unsigned int> reg_binding::get_vars(const unsigned int & r) const
{
   std::set<unsigned int> vars;
   THROW_ASSERT(reg2storage_values.find(r) != reg2storage_values.end() && !reg2storage_values.find(r)->second.empty(), "at least a storage value has to be mapped on register r");

   std::set<unsigned int>::const_iterator rs_it_end = reg2storage_values.find(r)->second.end();
   for(std::set<unsigned int>::const_iterator rs_it = reg2storage_values.find(r)->second.begin(); rs_it != rs_it_end; ++rs_it)
      vars.insert(HLS->svi_data->get_variable_index(*rs_it));
   return vars;
}

unsigned int reg_binding::determine_bitsize(unsigned int r) const
{
   std::set<unsigned int> reg_vars = get_vars(r);
   unsigned int max_bits = 0;
   for (std::set<unsigned int>::const_iterator var = reg_vars.begin(); var != reg_vars.end(); var++)
   {
      structural_type_descriptorRef node_type = structural_type_descriptorRef(new structural_type_descriptor(*var, HLS->FB->CGetBehavioralHelper()));
      unsigned int node_size = STD_GET_SIZE(node_type);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, HLS->debug_level, "- Examinating node " + STR(*var) + ", whose type is " + node_type->get_name() + " (size: " + STR(node_type->size) + ", vector_size: " + STR(node_type->vector_size) + ")");
      max_bits = max_bits < node_size ? node_size : max_bits;
   }
   return max_bits;
}

void reg_binding::specialise_reg(structural_objectRef & reg, unsigned int r) const
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, HLS->debug_level, "Specialising " + reg->get_path() + ":");
   const structural_type_descriptorRef &in_type = GetPointer<module>(reg)->get_in_port(0)->get_typeRef();
   const structural_type_descriptorRef &out_type = GetPointer<module>(reg)->get_out_port(0)->get_typeRef();
   unsigned int max_bits = STD_GET_SIZE(in_type);
   max_bits = max_bits < STD_GET_SIZE(out_type) ? STD_GET_SIZE(out_type) : max_bits;
   unsigned int bits = determine_bitsize(r);
   max_bits = max_bits < bits ? bits : max_bits;
   unsigned int offset = 0;
   if (GetPointer<module>(reg)->get_in_port(0)->get_id() == CLOCK_PORT_NAME)
   {
      if(GetPointer<module>(reg)->get_in_port(1)->get_id() == RESET_PORT_NAME)
         offset = 2;
      else
         offset = 1;
   }
   if (STD_GET_SIZE(in_type) < max_bits)
   {
      GetPointer<module>(reg)->get_in_port(offset)->type_resize(max_bits); // in1
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, HLS->debug_level, "- " + GetPointer<module>(reg)->get_in_port(0)->get_path() + " -> " + in_type->get_name() + " (size: " + STR(in_type->size) + ", vector_size: " + STR(in_type->vector_size) + ")");
   }
   if (STD_GET_SIZE(out_type) < max_bits)
   {
      GetPointer<module>(reg)->get_out_port(0)->type_resize(max_bits); // out1
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, HLS->debug_level, "- " + GetPointer<module>(reg)->get_out_port(0)->get_path() + " -> " + out_type->get_name() + " (size: " + STR(out_type->size) + ", vector_size: " + STR(out_type->vector_size) + ")");
   }
}

void reg_binding::compute_is_with_out_enable()
{
   const BehavioralHelperConstRef  BH = HLS->FB->CGetBehavioralHelper();
   std::map<unsigned int, unsigned int> n_in;
   std::map<unsigned int, unsigned int> n_out;
   const std::list<vertex> & support_set = HLS->Rliv->get_support();
   const std::list<vertex>::const_iterator ss_it_end = support_set.end();
   for(std::list<vertex>::const_iterator ss_it = support_set.begin(); ss_it != ss_it_end; ++ss_it)
   {
      vertex v = *ss_it;
      const std::set<unsigned int>& LI = HLS->Rliv->get_live_in(v);
      const std::set<unsigned int>::const_iterator li_it_end = LI.end();
      for(std::set<unsigned int>::const_iterator li_it = LI.begin(); li_it != li_it_end; ++li_it)
      {
         if(n_in.find(*li_it) == n_in.end())
            n_in[*li_it] = 1;
         else
            n_in[*li_it] = n_in[*li_it] + 1;
      }
      const std::set<unsigned int>& LO = HLS->Rliv->get_live_out(v);
      const std::set<unsigned int>::const_iterator lo_it_end = LO.end();
      for(std::set<unsigned int>::const_iterator lo_it = LO.begin(); lo_it != lo_it_end; ++lo_it)
      {
         if(n_out.find(*lo_it) == n_out.end())
         {
            n_out[*lo_it] = 1;
            if(LI.find(*lo_it) != LI.end())
               n_out[*lo_it] = 2;
         }
         else
            n_out[*lo_it] = n_out[*lo_it] + 1;
      }
   }

   for (unsigned int i = 0; i < get_used_regs(); i++)
   {
      const std::set<unsigned int> &store_vars_set = get_vars(i);
      const std::set<unsigned int>::const_iterator svs_it_end = store_vars_set.end();
      bool all_woe = true;
      for(std::set<unsigned int>::const_iterator svs_it = store_vars_set.begin(); svs_it != svs_it_end && all_woe; ++svs_it)
      {
         if(n_in.find(*svs_it) == n_in.end() || n_out.find(*svs_it) == n_out.end())
            all_woe = false;
         if(n_in.find(*svs_it)->second != 1 || n_out.find(*svs_it)->second != 1)
            all_woe = false;
      }
      if(all_woe)
      {
         //std::cerr << "register STD " << i << std::endl;
         is_with_out_enable.insert(i);
      }
   }

}
void reg_binding::add_to_SM(structural_objectRef clock_port, structural_objectRef reset_port)
{
   const structural_managerRef& SM = HLS->datapath;

   const structural_objectRef& circuit = SM->get_circ();

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug, "reg_binding::add_registers - Start");

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug, "Number of registers: " +
                                                         boost::lexical_cast<std::string>(get_used_regs()));

   compute_is_with_out_enable();
   /// define boolean type for command signals
   for (unsigned int i = 0; i < get_used_regs(); i++)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug, "Allocating register number: " + boost::lexical_cast<std::string>(i));
      generic_objRef regis = get(i);
      std::string name = regis->get_string();
      std::string synch_reset = HLS->Param->getOption<std::string>(OPT_sync_reset);
      std::string register_type_name;
      if(is_with_out_enable.find(i) != is_with_out_enable.end())
         register_type_name = register_STD;
      else if(synch_reset == "no")
         register_type_name = register_SE;
      else if(synch_reset == "sync")
            register_type_name = register_SRSE;
      else
         register_type_name = register_SARSE;
      std::string library = HLS->HLS_T->get_technology_manager()->get_library(register_type_name);
      structural_objectRef reg_mod = SM->add_module_from_technology_library(name, register_type_name, library, circuit, HLS->HLS_T->get_technology_manager());
      this->specialise_reg(reg_mod, i);
      structural_objectRef port_ck = reg_mod->find_member(CLOCK_PORT_NAME, port_o_K, reg_mod);
      SM->add_connection(clock_port, port_ck);
      structural_objectRef port_rst = reg_mod->find_member(RESET_PORT_NAME, port_o_K, reg_mod);
      SM->add_connection(reset_port, port_rst);
      regis->set_structural_obj(reg_mod);

      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug, "Register " + boost::lexical_cast<std::string>(i) + " successfully allocated");

   }
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug, "reg_binding::add_registers - End");
}



