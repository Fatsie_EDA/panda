/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file reg_binding.hpp
 * @brief Data structure used to store the register binding of variables
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef REG_BINDING_HPP
#define REG_BINDING_HPP

#include <iostream>
#include <map>
#include <string>

#include "Variable.hpp"
#include "generic_obj.hpp"
#include "register_obj.hpp"

REF_FORWARD_DECL(hls);
REF_FORWARD_DECL(reg_binding);

#include "refcount.hpp"

/**
 * Class managing the register binding.
 * Store the register binding, that is, the mapping of operations in the behavioral description into the set of
 * selected register elements.
*/
class reg_binding : public variable2obj< generic_objRef >
{
   public:

      typedef enum
      {
         STG = 0,
         CDFG
      } type_t;

   protected:

      /// level of the verbosity during the debugging
      int debug;

      /// number of used register
      unsigned int used_regs;

      /// map between register index and object
      std::map<unsigned int, generic_objRef> unique_table;

      /// bind the storage value with the register instance
      std::map<unsigned int, unsigned int> reverse_map;

      ///HLS datastructure
      hlsRef HLS;

      ///map between the register and the associated storage value
      std::map<unsigned int, std::set<unsigned int> > reg2storage_values;

      /// store the set of register without enable
      std::set<unsigned int> is_with_out_enable;

      /**
       * compute the is with out enable relation
       */
      void compute_is_with_out_enable();

      /**
       * Specialise a register according to the type of the variables crossing it.
       * @param reg is the register
       * @param reg is the id of the register
       */
      void specialise_reg(structural_objectRef & reg, unsigned int r) const;

   public:

      /**
      * Constructor.
      */
      reg_binding(const hlsRef& HLS);

      /**
       * Destructor.
       */
      virtual ~reg_binding();

      /**
       *
       */
      void bind(unsigned int sv, unsigned int index)
      {
         reverse_map[sv] = index;
         if (unique_table.find(index) == unique_table.end())
            unique_table[index] = generic_objRef(new register_obj(std::string("reg_")+STR(index)));
         iterator i = this->find(sv);
         if (i == this->end())
            this->insert(std::make_pair(sv,unique_table[index]));
         else
            i->second = unique_table[index];
         reg2storage_values[index].insert(sv);
      }

      /**
       * returns number of used register
       * @return the number of used register
       */
      unsigned int get_used_regs() const { return used_regs; }

      /**
       * sets number of used register
       * @param regs is new number of used register
       */
      void set_used_regs(unsigned int regs) { used_regs = regs; }

      /**
       * return the register index where the storage value is stored
       * @param sv is the storage value
       * @return the index of the register assigned to the storage value.
       */
      unsigned int get_register(unsigned int sv) const { return reverse_map.find(sv)->second; }

      /**
       * Function that print the register binding associated with a storage value.
       */
      void print_el(std::ostream& os, const OpGraphConstRef data, const_iterator &it) const;

      /**
       * Function that print the storage value and the variable associated with the register
       * @param os is the output stream
      */
      void print_rowHead(std::ostream& os, const OpGraphConstRef , reg_binding::const_iterator & it) const;

      /**
       * Friend definition of the << operator.
       */
      friend std::ostream& operator<<(std::ostream& os, reg_binding& s)
      {
         s.print(os);
         return os ;
      }

      /**
      * Friend definition of the << operator. Pointer version.
      */
      friend std::ostream& operator<<(std::ostream& os, const reg_binding* s)
      {
         if (s)
            s->print(os);
         return os;
      }

      /**
       * Returns reference to register object associated to a given index
       * @param r is the register index
       * @return the associated reference
       */
      generic_objRef get(const unsigned int& r) const
      {
         return unique_table.find(r) != unique_table.end() ? unique_table.find(r)->second : generic_objRef();
      }

      /**
       * redefinition of the [] operator
       */
      const register_obj& operator[](unsigned int v)
      {
         THROW_ASSERT(this->find(v) != this->end(), "variable not preset");
         return *GetPointer<register_obj>(this->find(v)->second);
      }

      /**
       * Add the resulting registers to the structural description of the datapath
       */
      virtual void add_to_SM(structural_objectRef clock_port, structural_objectRef reset_port);

      /**
       * Determines bit size
       */
      unsigned int determine_bitsize(unsigned int r) const;

   private:
      /**
       * Returns the set of variable associated with the register
       * @param r is the register
       * @return the set of associated variables
       */
      std::set<unsigned int> get_vars(const unsigned int & r) const;



};

/**
 * RefCount type definition of the reg_binding class structure
 */
typedef refcount<reg_binding> reg_bindingRef;

#endif
