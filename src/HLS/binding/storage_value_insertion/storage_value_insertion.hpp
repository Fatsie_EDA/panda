/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file storage_value_insertion.hpp
 * @brief This package is used to define the storage value scheme adopted by the register allocation algorithms.
 *
 * @defgroup Storage Value Insertion
 * @ingroup HLS
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef STORAGE_VALUE_INSERTION_HPP
#define STORAGE_VALUE_INSERTION_HPP

#include "config_HAVE_EXPERIMENTAL.hpp"

#include "hls_step.hpp"
REF_FORWARD_DECL(storage_value_insertion);

#include "graph.hpp"

class storage_value_insertion : public HLS_step
{
   protected:

      ///current number of storage values
      unsigned int number_of_storage_values;

   public:

      /// storage value insertion algorithms
      enum storage_value_insertion_type
      {
         VALUES = 0      //! Default solution, it just uses the results of the dataflow analysis
                          //! since we use single statement assignments (SSA) it is equivalent to the scheme 3(VALUES)-2(VARIABLES) described in the Stok's paper.
#if HAVE_EXPERIMENTAL
         ,
         VALUES_INSTANCES,
         LIMITED_VALUES_INSTANCES,
         PAULIN_SCHEME
#endif
      };

      /**
       * Constructor
       */
      storage_value_insertion(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      virtual ~storage_value_insertion();

      /**
       * Factory method
       */
      static
      storage_value_insertionRef factory(storage_value_insertion_type type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);
      
      /**
       * Factory method based on XML node
       */
      static
      storage_value_insertionRef xload_factory(const xml_element *node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Returns the number of storage values inserted
       */
      unsigned int get_number_of_storage_values() const;

      /**
       * return true in case a storage value exist for the pair vertex variable
       * @param curr_vertex is the vertex
       * @param var_index is the variable
      */
      virtual bool is_a_storage_value(vertex curr_vertex, unsigned int var_index) const = 0;

      /**
       * Returns the index of the storage value associated with the variable in a given vertex
       * @param curr_vertex is the vertex
       * @param var_index is the variable
       */
      virtual unsigned int get_storage_value_index(vertex curr_vertex, unsigned int var_index) = 0;

      /**
       * Returns the index of the variable associated with the storage value in a given vertex
       */
      virtual unsigned int get_variable_index(unsigned int storage_value_index) = 0;

      /**
       * return a weight that estimate how much two storage values are compatible.
       * An high value returned means an high compatibility between the two storage values.
       */
      virtual int get_compatibility_weight(unsigned int storage_value_index1, unsigned int storage_value_index2) = 0;

      /**
       * return the bitsize of a storage value
       * @param storage_value_index is the storage value
       */
      virtual unsigned int get_storage_value_bitsize(unsigned int storage_value_index) = 0;
};

#endif
