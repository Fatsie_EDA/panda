/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file storage_value_insertion.cpp
 * @brief This package is used to define the storage value scheme adopted by the register allocation algorithms.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 * $Locker:  $
 * $State: Exp $
 *
*/

#include "storage_value_insertion.hpp"
#include "hls.hpp"
#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"
#include "values_scheme.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"

storage_value_insertion::storage_value_insertion(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   HLS_step(_Param, _HLSMgr, _funId),
   number_of_storage_values(0)
{

}

storage_value_insertion::~storage_value_insertion()
{

}

unsigned int storage_value_insertion::get_number_of_storage_values() const
{
   return number_of_storage_values;
}

storage_value_insertionRef storage_value_insertion::xload_factory(const xml_element *node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string algorithm;
   if (CE_XVM(algorithm, node)) LOAD_XVM(algorithm, node);
   else algorithm = "VALUES";

   storage_value_insertion_type algorithm_t;
   if (algorithm == "VALUES")
      algorithm_t = VALUES;
   else
      THROW_ERROR("Storage value insertion algorithm \"" + algorithm + "\" currently not supported");
   return factory(algorithm_t, Param, HLSMgr, funId);
}

storage_value_insertionRef storage_value_insertion::factory(storage_value_insertion_type type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
#ifndef NDEBUG
   int debug_level = Param->getOption<int>(OPT_debug_level);
#endif
   switch (type)
   {
      case VALUES:
         PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "\t   creating VALUES solver");
         return storage_value_insertionRef(new values_scheme(Param, HLSMgr, funId));
#if HAVE_EXPERIMENTAL
      case LIMITED_VALUES_INSTANCES:
      case PAULIN_SCHEME:
      case VALUES_INSTANCES:
#endif
      default:
         THROW_UNREACHABLE("Storage value insertion algorithm not supported: " + boost::lexical_cast<std::string>(type));
   }
   return storage_value_insertionRef();
}
