/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file values_scheme.hpp
 * @brief Class specification of values scheme for the storage value insertion phase
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 * $Locker:  $
 * $State: Exp $
 *
*/
#ifndef VALUES_SCHEME_HPP
#define VALUES_SCHEME_HPP

#include "storage_value_insertion.hpp"
#include "refcount.hpp"

/**
 * @name Forward declarations.
*/
//@{
REF_FORWARD_DECL(dataflow_analysis);
REF_FORWARD_DECL(fu_binding);
REF_FORWARD_DECL(hls);
CONSTREF_FORWARD_DECL(OpGraph);
REF_FORWARD_DECL(Parameter);
//@}

#include <unordered_map>
#include <vector>

class values_scheme : public storage_value_insertion
{
      /// put into relation variables/values with storage values
      std::unordered_map<unsigned int,unsigned int> storage_index_map;

      /// put into relation storage value index with variables
      std::vector<unsigned int> variable_index_vect;

      /// relation between var written and operations
      std::unordered_map<unsigned int, vertex> vw2vertex;

      /// operation graph used to compute the affinity between storage values
      const OpGraphConstRef data;

      /// functional unit assignments
      const fu_bindingRef fu;

   public:

      /**
       * Constructor of the class.
       */
      values_scheme(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor of the class.
       */
      ~values_scheme();

      /**
       * return true in case a storage value exist for the pair vertex variable
       * @param curr_vertex is the vertex
       * @param var_index is the variable
      */
      bool is_a_storage_value(vertex curr_vertex, unsigned int var_index) const;

      /**
       * values scheme algorithm. Since the IR adopted is SSA the values scheme is equivalent to the variables scheme
       */
      void exec();

      /**
       * return the index of the storage value associated with the variable in a given state
       * @param curr_state is the state
       * @param var_index is the variable
       */
      unsigned int get_storage_value_index(vertex curr_state, unsigned int var_index);
      
      /**
       * return the index of the variable associated with the storage value in a given state
       */
      unsigned int get_variable_index(unsigned int storage_value_index);

      /**
       * return a weight that estimate how much two storage values are compatible.
       * An high value returned means an high compatibility between the two storage values.
       */
      int get_compatibility_weight(unsigned int storage_value_index1, unsigned int storage_value_index2);

      /**
       * return the bitsize of a storage value
       * @param storage_value_index is the storage value
       */
      unsigned int get_storage_value_bitsize(unsigned int storage_value_index);

      /**
       * Returns the name of the algorithm
       */
      std::string get_kind_text() const;

};

#endif
