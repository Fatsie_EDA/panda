/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file values_scheme.cpp
 * @brief Class implementation of values scheme for the storage value insertion phase
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$ 
*/
#include "values_scheme.hpp"

#include "hls.hpp"

#include "state_transition_graph_manager.hpp"
#include "liveness.hpp"
#include "fu_binding.hpp"

///Behavior include
#include "hls_manager.hpp"
#include "behavioral_helper.hpp"
#include "function_behavior.hpp"
#include "op_graph.hpp"

#include "tree_helper.hpp"

#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"
#include "boost/lexical_cast.hpp"

values_scheme::values_scheme(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   storage_value_insertion(_Param, _HLSMgr, _funId),
   data(HLS->FB->CGetOpGraph(FunctionBehavior::DFG)),
   fu(HLS->Rfu)
{
   const tree_managerRef TreeM = HLSMgr->get_tree_manager();

   /// initialize the vw2vertex relation
   VertexIterator ki, ki_end;
   for (boost::tie(ki, ki_end) = boost::vertices(*data); ki != ki_end; ++ki)
   {
      const CustomSet<unsigned int> & scalar_defs = data->CGetOpNodeInfo(*ki)->GetScalarVariables(AT_DEF);
      if(not scalar_defs.empty())
      {
         CustomSet<unsigned int>::const_iterator it_end = scalar_defs.end();
         size_t counter = 0;
         for(CustomSet<unsigned int>::const_iterator it = scalar_defs.begin(); it != it_end; ++it)
         {
            if(tree_helper::is_ssa_name(TreeM, *it) &&
                  !tree_helper::is_virtual(TreeM, *it) &&
                  !tree_helper::is_parameter(TreeM, *it))
            {
               vw2vertex[*it] = *ki;
               ++counter;
            }
         }
         THROW_ASSERT(counter<=1 || (GET_TYPE(data, *ki) & TYPE_ENTRY), "more than one definition");
      }
   }
}

values_scheme::~values_scheme()
{

}
bool values_scheme::is_a_storage_value(vertex, unsigned int var_index) const
{
   return storage_index_map.find(var_index) != storage_index_map.end();
}

void values_scheme::exec()
{
   THROW_ASSERT(HLS->Rliv, "Liveness analysis not yet computed");
   unsigned int i = 0;
   const std::list<vertex> & support = HLS->Rliv->get_support();

   const std::list<vertex>::const_iterator vEnd = support.end();
   for(std::list<vertex>::const_iterator vIt = support.begin(); vIt != vEnd; vIt++)
   {
      //std::cerr << "current state for sv " << HLS->Rliv->get_name(*vIt) << std::endl;
      const std::set<unsigned int>& live = HLS->Rliv->get_live_in(*vIt);
      const std::set<unsigned int>::const_iterator k_end = live.end();
      for(std::set<unsigned int>::const_iterator k = live.begin(); k != k_end; k++)
      {
         if(storage_index_map.find(*k) == storage_index_map.end())
         {
            storage_index_map[*k] = i;
            variable_index_vect.push_back(*k);
            i++;
         }
      }
   }
   number_of_storage_values = i;
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Number of storage values inserted: "+boost::lexical_cast<std::string>(i));
}

unsigned int values_scheme::get_storage_value_index(vertex, unsigned int var_index)
{
   THROW_ASSERT(storage_index_map.find(var_index) != storage_index_map.end(), "the storage value is missing");
   return storage_index_map.find(var_index)->second;
}

unsigned int values_scheme::get_variable_index(unsigned int storage_value_index)
{
   THROW_ASSERT(variable_index_vect.size() > storage_value_index, "the storage value is missing");
   return variable_index_vect[storage_value_index];
}

int values_scheme::get_compatibility_weight(unsigned int storage_value_index1, unsigned int storage_value_index2)
{
   unsigned int var1 = get_variable_index(storage_value_index1);
   unsigned int var2 = get_variable_index(storage_value_index2);
   if(vw2vertex.find(var1) == vw2vertex.end())
   {
      //std::cerr << var1 << std::endl;
      return 1;
   }

   THROW_ASSERT(vw2vertex.find(var2) != vw2vertex.end(), "variable not in the map");
   vertex v1 = vw2vertex.find(var1)->second;
   bool is_a_phi1 = (GET_TYPE(data, v1) & TYPE_PHI)!=0;
   vertex v2 = vw2vertex.find(var2)->second;
   bool is_a_phi2 = (GET_TYPE(data, v2) & TYPE_PHI)!=0;
   const CustomSet<unsigned int> & ssa_read1 = data->CGetOpNodeInfo(v1)->GetScalarVariables(AT_USE);
   if(is_a_phi1)
   {
      if(ssa_read1.find(var2) != ssa_read1.end())
         return 5;
   }
   const CustomSet<unsigned int> & ssa_read2 = data->CGetOpNodeInfo(v2)->GetScalarVariables(AT_USE);
   if(is_a_phi2)
   {
      if(ssa_read2.find(var1) != ssa_read2.end())
         return 5;
   }
   if(fu)
   {
      unsigned int fu_unit1 = fu->get_assign(v1);
      unsigned int fu_unit2 = fu->get_assign(v2);
      if(fu_unit1 == fu_unit2)
      {
         if(fu->get_index(v1) != INFINITE_UINT)
         {
            if(fu->get_index(v1) == fu->get_index(v2))
               return 5;
            else
               return 1;
         }
         bool they_have_common_inputs = false;
         CustomSet<unsigned int>::const_iterator it1_end = ssa_read1.end();
         for(CustomSet<unsigned int>::const_iterator it1 = ssa_read1.begin(); it1 != it1_end; ++it1)
         {
            if(ssa_read2.find(*it1) != ssa_read2.end())
            {
               they_have_common_inputs = true;
               break;
            }
            else if(vw2vertex.find(*it1) != vw2vertex.end())
            {
               vertex from_v1 = vw2vertex.find(*it1)->second;
               CustomSet<unsigned int>::const_iterator it2_end = ssa_read2.end();
               for(CustomSet<unsigned int>::const_iterator it2 = ssa_read2.begin(); it2 != it2_end; ++it2)
               {
                  if(vw2vertex.find(*it2) != vw2vertex.end())
                  {
                     vertex from_v2 = vw2vertex.find(*it2)->second;
                     if(fu->get_assign(from_v1) == fu->get_assign(from_v2) && fu->get_index(from_v1) != INFINITE_UINT && fu->get_index(from_v1) == fu->get_index(from_v2))
                     {
                        they_have_common_inputs = true;
                        break;
                     }
                  }
               }
               if(they_have_common_inputs)
                  break;
            }
         }
         if(they_have_common_inputs)
            return 4;
         if(ssa_read1.find(var2) != ssa_read1.end())
            return 3;
         if(ssa_read2.find(var1) != ssa_read2.end())
            return 3;
         return 2;
      }
   }
   return 1;
}

/**
 * return the bitsize of a storage value
 * @param storage_value_index is the storage value
 */
unsigned int values_scheme::get_storage_value_bitsize(unsigned int storage_value_index)
{
   return tree_helper::size(HLSMgr->get_tree_manager(), get_variable_index(storage_value_index));
}

std::string values_scheme::get_kind_text() const
{
   return "values-scheme";
}
