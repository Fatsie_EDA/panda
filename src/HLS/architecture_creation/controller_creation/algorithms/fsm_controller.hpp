/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file fsm_controller.hpp
 * @brief Header class for the creation of the classical FSM controller.
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 * $Locker:  $
 * $State: Exp $
 *
*/

#ifndef FSM_CONTROLLER_HPP
#define FSM_CONTROLLER_HPP

#include "controller_creator.hpp"
REF_FORWARD_DECL(tree_manager);
CONSTREF_FORWARD_DECL(OpGraph);

class fsm_controller : public controller_creator
{   
      /**
       * Generates the string representation of the FSM
       */
      void create_state_machine(std::string &parse);

      /**
       * Returns the value of the guard value of a case_label_expr
       * default is not managed
      */
      std::string get_guard_value(const tree_managerRef TM, const unsigned int index, vertex op, const OpGraphConstRef data);

   public:

      /**
       * Constructor.
       */
      fsm_controller(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      ~fsm_controller();

      /**
       * Function that actually creates the controller.
       */
      void do_exec();

      /**
       * Function printing the name of the algorithm.
       * @return a string representing the name of algorithm
       */
      std::string get_kind_text() const;

};
///Refcount definition of the class
typedef refcount<fsm_controller> fsm_controllerRef;

#endif
