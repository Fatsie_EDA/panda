/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file controller.hpp
 * @brief Base class for all the controller creation algorithms.
 *
 * This class is a pure virtual one, that has to be specilized in order to implement a particular algorithm to create the
 * controller.
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef _CONTROLLER_CREATOR_HPP_
#define _CONTROLLER_CREATOR_HPP_

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "hls_step.hpp"

#include <fstream>
#include <map>

#include "refcount.hpp"
#include "graph.hpp"

/**
 * @name forward declarations
*/
//@{
REF_FORWARD_DECL(hls);
REF_FORWARD_DECL(controller_creator);
REF_FORWARD_DECL(structural_object);
REF_FORWARD_DECL(generic_obj);
REF_FORWARD_DECL(structural_manager);
class xml_element;
//@}

/**
 * Generic class managing controller creation algorithms.
 */
class controller_creator : public HLS_step
{
   public:

      /// Controller creation algorithms implemented
      enum controller_type
      {
         FSM_CONTROLLER = 0,
#if HAVE_EXPERIMENTAL
         PARALLEL_CONTROLLER = 1
#endif
      };

      /**
       * Constructor
       */
      controller_creator(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      virtual ~controller_creator();

      /**
       * Function that creates the controller.
       * Initializes common data and then calls do_exec()
       */
      virtual void exec();

      /**
       * Creates "creator" based on selected option
       * @param type is the desidered controller creation algorithm
       * @return the reference to controller specialization
       */
      static
      controller_creatorRef factory(controller_type type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Creates "creator" based on the XML configuration
       * @param node is the XML node
       * @return the reference to controller specialization
       */
      static
      controller_creatorRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

   protected:
 
      /**
       * Subclasses must implement this member function.
       * It has to be specialized in order to implement the desired creation algorithm.
       */
      virtual void do_exec() = 0;
      
      /**
       * This member function adds the standard ports (clock, reset, done and command ones) to a circuit.
       * \param circuit it is the datastructure of the component where to add these ports
       */
      void add_common_ports(structural_objectRef circuit);
      
      /// structural representation of the resulting controller circuit
      structural_managerRef SM;

      /// This contains all the ports that go from the controller to the datapath, used to enable the registers
      /// and to control muxes in the datapath. The first element in the map
      /// is the port object. Despite its type is generic_objRef it appears that this map contains only objects
      /// of type commandport_obj. This is suggested by the dynamic cast hidden behind a GetPointer call in
      /// controller::add_command_ports(). The second element of the map is the integer identifier of the port
      /// Initialized only after add_common_ports is called
      std::map<generic_objRef, unsigned int> out_ports;
      
      /// These are the lines that go from the datapath to the controller containing results of conditions of
      /// constructs such as for, if etc. The key is the object representing the port, while the value is the
      /// integer value associated with the port.
      /// Initialized only after add_common_ports is called
      std::map<generic_objRef, unsigned int> in_ports;
      
      /// This is the same as in_ports except that the first element is of type vertex. Each element is obtained by
      /// calling GetPointer<commandport_obj>(j->second)->get_vertex() to the elements into in_ports. The second
      /// element is the same number of the generic_objRef into in_ports to which get_vertex() was called
      /// Initialized after add_common_ports is called
      std::map<vertex, unsigned int> cond_ports;
      /// Map between the operation vertex and the corresponding control port
      std::map<vertex, generic_objRef> cond_obj;

      /// Initialized after add_common_ports is called. It represents the current number of output ports
      /// (out_num==out_ports.size())
      unsigned int out_num;

      /// Initialized after add_common_ports is called. It represents the current number of input ports
      /// (in_num==in_ports.size()==cond_ports.size())
      unsigned int in_num;

   private:
      
      /**
       * Adds the clock and reset ports to a circuit. Called by add_common_ports
       * \param circuit the circuit where to add the clock and reset ports
       */
      void add_clock_reset(structural_objectRef circuit);

      /**
       * Adds the done port to a circuit. Called by add_common_ports
       * The done port appears to go high once all the calculation of a function are completed
       * \param circuit the circuit where to add the done port
       */
      void add_done_port(structural_objectRef circuit);

      /**
       * Adds the start port to a circuit. Called by add_common_ports
       * \param circuit the circuit where to add the start port
       */
      void add_start_port(structural_objectRef circuit);

      /**
       * Adds the command ports to a circuit. Called by add_common_ports
       * Command ports are of three types:
       * - Selectors to enable functional units in the datapath, these go from the controller to the datapath
       * - Selectors of mux in the datapath, these go from the controller to the datapath
       * - Conditions, used to modify instruction flow in constructs such as for, if, while,
       *   these go in the opposite direction, from the datapath to the controller
       * \param circuit the circuit where to add the command ports
       */
      void add_command_ports(structural_objectRef circuit);
};
///refcount definition for the class
typedef refcount<controller_creator> controller_creatorRef;

#endif
