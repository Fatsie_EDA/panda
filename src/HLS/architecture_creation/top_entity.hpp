/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file top_entity.hpp
 * @brief Base class for the top entity creation.
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef TOP_ENTITY_HPP
#define TOP_ENTITY_HPP

#include "hls_step.hpp"
REF_FORWARD_DECL(structural_manager);
REF_FORWARD_DECL(structural_object);

class top_entity : public HLS_step
{
      ///reference to the resulting circuit
      structural_managerRef SM;

      /**
       * Adds the input/output ports to the circuit
       * @param circuit is the reference to the datastructure representing the circuit
       */
      void add_ports(structural_objectRef circuit);

      /**
       * Adds the command signals to the circuit
       * @param circuit is the reference to the datastructure representing the circuit
       */
      void add_command_signals(structural_objectRef circuit);

   public:

      /**
       * Constructor
       */
      top_entity(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor
       */
      virtual ~top_entity();

      /**
       * Function that creates the top entity
       * @param HLS is the data structure storing all the HLS data structures
       */
      virtual void exec();

      /**
       * Returns the name of the step
       */
      std::string get_kind_text() const;

};
///refcount definition for the class
typedef refcount<top_entity> top_entityRef;

#endif
