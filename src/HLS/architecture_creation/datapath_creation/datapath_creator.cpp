/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file datapath.cpp
 * @brief Base class for all datapath creation algorithms.
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "datapath_creator.hpp"

#include "hls.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"

#include "utility.hpp"
#include "classic_datapath.hpp"
#include "dbgPrintHelper.hpp"

datapath_creator::datapath_creator(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   HLS_step(_Param, _HLSMgr, _funId)
{

}

datapath_creator::~datapath_creator()
{

}

datapath_creatorRef datapath_creator::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string algorithm;
   if (CE_XVM(algorithm, node)) LOAD_XVM(algorithm, node);
   else algorithm = "CLASSIC";

   datapath_type algorithm_t;
   if (algorithm == "CLASSIC")
      algorithm_t = CLASSIC;
   else
      THROW_ERROR("Datapath creation algorithm \"" + algorithm + "\" currently not supported");
   return factory(algorithm_t, Param, HLSMgr, funId);
}

datapath_creatorRef datapath_creator::factory(datapath_type type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   switch (type)
   {
      case CLASSIC:
      {
         return datapath_creatorRef(new classic_datapath(Param, HLSMgr, funId));
      }
      default:
         THROW_UNREACHABLE("Datapath creation algorithm not yet supported");
   }
   return datapath_creatorRef();
}
