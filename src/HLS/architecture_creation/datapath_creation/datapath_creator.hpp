/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file datapath.hpp
 * @brief Base class for all datapath creation algorithms.
 *
 * This class is a pure virtual one, that has to be specilized in order to implement a particular algorithm to create
 * datapath.
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef _DATAPATH_CREATOR_HPP_
#define _DATAPATH_CREATOR_HPP_

#include "hls_step.hpp"
REF_FORWARD_DECL(datapath_creator);

/**
 * @class datapath_creator
 * Generic class managing datapath creation algorithms.
 */
class datapath_creator : public HLS_step
{

   public:

      ///implemented algorithms
      enum datapath_type
      {
         CLASSIC = 0
      };

      /**
       * Constructor
       */
      datapath_creator(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      virtual ~datapath_creator();

      /**
       * Factory method
       */
      static
      datapath_creatorRef factory(datapath_type type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Creates "creator" based on the XML configuration
       */
      static
      datapath_creatorRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

};
///refcount definition of the class
typedef refcount<datapath_creator> datapath_creatorRef;

#endif
