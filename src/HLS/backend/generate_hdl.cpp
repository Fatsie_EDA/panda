/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file generate_hdl.cpp
 * @brief Implementation of the class to generate HDL code
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "generate_hdl.hpp"

#include "application_manager.hpp"
#include "structural_manager.hpp"
#include "behavioral_helper.hpp"

#include "hls.hpp"
#include "hls_manager.hpp"
#include "hls_constraints.hpp"
#include "hls_target.hpp"
#include "HLStestbench.hpp"

#include "technology_manager.hpp"

#include "evaluation.hpp"
#include "objective_evaluator.hpp"

#include "BackendFlow.hpp"

#include "Parameter.hpp"

generate_hdl::generate_hdl(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId):
   HLS_step(_Param, _HLSMgr, _funId)
{

}

generate_hdl::~generate_hdl()
{

}

void generate_hdl::exec()
{
   /// check if codesign simulation has to be performed
   if(Param->getOption<bool>(OPT_generate_testbench))
   {
      /// prepare the codesign simulation

      /// generate the C wrapper, the stimuli and collect the outputs
      HLStestbenchRef hls_tb(HLStestbench::Create(Param, HLSMgr));
      hls_tb->generate_C_testbench();

      double clock_period_value = HLS->HLS_C->get_clock_period();
      /// create the HDL testbench
      structural_managerRef SM = HLS->top;
      std::string bench_name = "testbench_" + SM->get_circ()->get_id();
      HLS->filename_bench = hls_tb->generate(bench_name, SM->get_circ(), clock_period_value);
      if (HLS->filename_bench.size() == 0)
         THROW_ERROR("Testbench not created!");
   }

   /// generate backend scripts
   const BackendFlowRef BEflow = HLSMgr->get_backend_flow();
   BEflow->generateBackendScripts(HLS->FB->CGetBehavioralHelper()->get_function_name(), HLS->top, HLS->filename_bench);

   if (Param->getOption<int>(OPT_evaluation))
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "\nHigh-level synthesis statistics:");
      objective_evaluator::evaluation_mode mode = static_cast<objective_evaluator::evaluation_mode>(Param->getOption<unsigned int>(OPT_evaluation_method));
      evaluationRef estimate = evaluationRef(new evaluation(Param, HLSMgr, funId, mode));
      estimate->exec();
   }
}

std::string generate_hdl::get_kind_text() const
{
   return "Generate the HDL code";
}

generate_hdlRef generate_hdl::xload_factory(const xml_element*, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   return generate_hdlRef(new generate_hdl(Param, HLSMgr, funId));
}
