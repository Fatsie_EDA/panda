/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file memory.hpp
 * @brief Datastructure to represent memory information in high-level synthesis
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef _MEMORY_HPP_
#define _MEMORY_HPP_

#include "refcount.hpp"
/**
 * @name forward declarations
 */
//@{
REF_FORWARD_DECL(tree_manager);
REF_FORWARD_DECL(memory_symbol);
REF_FORWARD_DECL(structural_manager);
REF_FORWARD_DECL(structural_object);
//@}
class xml_element;

#include <set>
#include <map>

class memory
{
      ///datastructure containing tree information
      const tree_managerRef TreeM;

      ///set of variables allocated outside the top module
      std::map<unsigned int, memory_symbolRef> external;

      ///set of variables allocated internally to the cores, classified by function id
      std::map<unsigned int, std::map<unsigned int, memory_symbolRef> > internal;

      /// set of variable proxies accessed by a function
      std::map<unsigned int, std::set<unsigned int> > internal_variable_proxy;

      /// is the set of proxied variables
      std::set<unsigned int> proxied_variables;

      std::set<unsigned int> read_only_vars;

      /// set of variable proxies accessed or passed through a given function
      std::map<unsigned int, std::set<unsigned int> > referred_variable_proxy;

      ///set of all the internal variables
      std::map<unsigned int, memory_symbolRef> in_vars;

      /// for each var store the address space rangesize associated with it
      std::map<unsigned int, unsigned int> rangesize;

      ///set of variables allocated in registers of the interface
      std::map<unsigned int, std::map<unsigned int, memory_symbolRef> > parameter;
      ///set of all the internal parameters
      std::map<unsigned int, memory_symbolRef> params;

      /// store the objects that does not need to be attached to the bus
      std::set<unsigned int> private_memories;

      /// store if a given variable is accessed always with the same data_size or not
      std::map<unsigned int, unsigned int> same_data_size_accesses;

      /// parm_decl that has to be copied from the caller
      std::set<unsigned int> parm_decl_copied;

      /// parm_decl storage has to be initialized from the formal parameter
      std::set<unsigned int> parm_decl_stored;

      /// actual parameter that has to be loaded from a stored value
      std::set<unsigned int> actual_parm_loaded;

      ///it represents the next address that is available for internal allocation
      unsigned int next_base_address;

      /// is the start internal address
      unsigned int internal_base_address_start;

      /// is the maximum amount of private memory allocated
      unsigned int maximum_private_memory_size;

      /// total amount of internal memory allocated
      unsigned int total_amount_of_private_memory;

      ///it represents the base address of the external memory
      unsigned int off_base_address;

      ///it represents the next address that is available to allocate a variable out of the top module
      unsigned int next_off_base_address;

      /// bus data bitsize
      unsigned int bus_data_bitsize;

      /// bus address bitsize
      unsigned int bus_addr_bitsize;

      /// bus size bitsize
      unsigned int bus_size_bitsize;

      /// maximum bitsize that can be accessed aligned
      unsigned int aligned_bitsize;

      /// bram bitsize
      unsigned int bram_bitsize;

      /// maximum bram bitsize
      unsigned int maxbram_bitsize;

      /// define if the Spec has data that can be externally accessed
      bool intern_shared_data;

      /// Spec accesses data having an address unknown at compile time
      bool use_unknown_addresses;

      /// when true an implicit memcpy is called
      bool implicit_memcpy;

      /// internal address alignment
      unsigned int internal_base_address_alignment;

      /// external address alignment
      unsigned int external_base_address_alignment;

      /**
       * Compute the new base address based on the size of the given variable and align the memory as needed
       * @param address is the address to be evaluated
       * @param var is the variable that has to be reserved
       * @param size is the size to be reserved. If 0, it is internally computed based on the size of the variable
       * @param alignment is the address alignment
       */
      void compute_next_base_address(unsigned int& address, unsigned int var, unsigned int size, unsigned int alignment);

      /**
       * Alignment utility function
       */
      void align(unsigned int& address, unsigned int alignment)
      {
         if(address % alignment != 0)
            address = ((address / alignment) + 1) * alignment;
      }

   public:

      /**
       * Constructor
       */
      memory(const tree_managerRef TreeM, unsigned int off_base_address, unsigned int max_bram);

      /**
       * Destructor
       */
      ~memory();

      /**
       * Return variables allocated out of the top module
       */
      std::map<unsigned int, memory_symbolRef> get_ext_memory_variables() const;

      /**
       * Allocates a variable to the set of variables allocated outside to outermost function
       */
      void add_external_variable(unsigned int var);

      /**
       * Allocates a variable to the set of variables allocated outside to outermost function. The corresponding symbol is already provided
       */
      void add_external_symbol(unsigned int var, const memory_symbolRef m_sym);

      /**
       * Allocates a variable to the set of variables allocated internally to the given function
       */
      void add_internal_variable(unsigned int funID_scope, unsigned int var);

      /**
       * allocate a proxy for the variable for the specified function
       * @param funID_scope is the function id
       * @param var is the variable
       */
      void add_internal_variable_proxy(unsigned int funID_scope, unsigned int var);

      /**
       * add the reference to the variable proxy for a given function
       * @param funID_scope is the function id
       * @param var is the variable
       */
      void add_referred_variable_proxy(unsigned int funID_scope, unsigned int var);

      /**
       * return the proxied internal variables associated with the function
       * @param funID_scope is the function id
       * @return the set of variables proxied in funID_scope
       */
      const std::set<unsigned int>& get_proxied_internal_variables(unsigned int funID_scope) const;

      /**
       * return the proxied referred variables associated with the function
       * @param funID_scope is the function id
       * @return the set of referred variables proxied in funID_scope
       */
      const std::set<unsigned int>& get_proxied_referred_variables(unsigned int funID_scope) const;

      /**
       * check if the function has proxied variables
       * @param funID_scope is the function id
       * @return true when there are proxied variables, false otherwise
       */
      bool has_proxied_internal_variables(unsigned int funID_scope) const;

      /**
       * return true if the variable is a proxied variable
       * @param var is the id of the variable
       * @return true when var is a proxied variable
       */
      bool is_a_proxied_variable(unsigned int var) const;

      /**
       * check if the function has proxied referred variables
       * @param funID_scope is the function id
       * @return true when there are proxied referred variables, false otherwise
       */
      bool has_proxied_referred_variables(unsigned int funID_scope) const;

      /**
       * add a read only variable
       * @param var is the variable id
       */
      void add_read_only_variable(unsigned var);

      /**
       * return true when the variable is only read
       * @param var is the variable id
       * @return true when variable is only read, false otherwise
       */
      bool is_read_only_variable(unsigned var) const;

      /**
       * Allocates a variable to the set of variables allocated internally to the given function. The corresponding symbol is already provided
       */
      void add_internal_symbol(unsigned int funID_scope, unsigned int var, const memory_symbolRef m_sym);

      /**
       * add a var that safely cannot be attached to the bus
       * @param var is var index
       */
      void add_private_memory(unsigned int var);

      /**
       * set if a variable is always accessed with the same data size or not
       * @param var is the variable id
       * @param value is true when the variable is always accessed with the same data size or false otherwise
       */
      void set_sds_var(unsigned int var, bool value);

      /**
       * Allocates a parameter to the set of the interface registers
       */
      void add_parameter(unsigned int funID_scope, unsigned int var, unsigned int size);

      /**
       * Allocates a parameter to the set of the interface registers
       */
      void add_parameter_symbol(unsigned int funID_scope, unsigned int var, const memory_symbolRef m_sym, unsigned int size);

      /**
       * Test if a variable is allocated into the specified function
       */
      bool is_internal_variable(unsigned int funID_scope, unsigned int var) const;

      /**
       * Test if a variable is into the set of variables out of the top function
       */
      bool is_external_variable(unsigned int var) const;

      /**
       * Return true in case the variable is private
       * @param var is the index of the variable
       * @return true in case the variable is private
       */
      bool is_private_memory(unsigned int var);

      /**
       * check if the variable is always accessed with the same data size
       * @param var is the variable id
       * @return true when the variable is always accessed with the same data size, false otherwise
       */
      bool is_sds_var(unsigned int var);

      /**
       * return true if the var has been classified in term of same data size relation
       * @param var is the variable id
       * @return true when var is in the sds relation, false otherwise
       */
      bool has_sds_var(unsigned int var);

      /**
       * Test if a variable is into the set of interface registers
       */
      bool is_parameter(unsigned int funID_scope, unsigned int var) const;

      /**
       * Get the current base address of the given variable
       */
      unsigned int get_internal_base_address(unsigned int var) const;

      /**
       * Get the current base address of the given variable
       */
      unsigned int get_external_base_address(unsigned int var) const;

      /**
       * Get the current base address of the given variable
       */
      unsigned int get_parameter_base_address(unsigned int var) const;

      /**
       * Get the current base address of the given variable
       */
      unsigned int get_base_address(unsigned int var) const;

      /**
       * Return the symbol associated with the given variable
       */
      memory_symbolRef get_symbol(unsigned int var) const;

      /**
       * return the address space rangesize associated with the given var
       * @param var is the variable considered
       */
      unsigned int get_rangesize(unsigned int var) const;

      /**
       * Check if there is a base address for the given variable
       */
      bool has_internal_base_address(unsigned int var) const;

      /**
       * Check if there is a base address for the given variable
       */
      bool has_external_base_address(unsigned int var) const;

      /**
       * Check if there is a base address for the given parameter
       */
      bool has_parameter_base_address(unsigned int var) const;

      /**
       * Check if there is a base address for the given variable
       */
      bool has_base_address(unsigned int var) const;

      /**
       * return true in case the parm_decl parameter has to be copied from the caller
       */
      bool is_parm_decl_copied(unsigned int var) const;

      /**
       * add a parm_decl to the set of parm_decl written
       */
      void add_parm_decl_copied(unsigned int var);

      /**
       * return true in case the parm_decl parameter has to be initialized from the formal value
       */
      bool is_parm_decl_stored(unsigned int var) const;

      /**
       * add a parm_decl to the set of parm_decl that has to be initialized
       */
      void add_parm_decl_stored(unsigned int var);

      /**
       * return true in case the actual parameter has to be initialized from a stored value
       */
      bool is_actual_parm_loaded(unsigned int var) const;

      /**
       * add an actual parameter to the set of parameter that has to be initialized from a stored value
       */
      void add_actual_parm_loaded(unsigned int var);


      /**
       * Return the variables allocated within the space of a given function
       */
      std::map<unsigned int, memory_symbolRef> get_function_vars(unsigned int funID_scope) const;

      /**
       * Return parameters allocated in register of the interface
       */
      std::map<unsigned int, memory_symbolRef> get_function_parameters(unsigned int funID_scope) const;

      /**
       * Return the first memory address not yet allocated
       */
      unsigned int get_memory_address() const;

      /**
       * Explicitly allocate a certain space in the external memory
       */
      void reserve_space(unsigned int space);

      /**
       * Returns the amount of memory allocated internally to the module
       */
      unsigned int get_allocated_space() const;

      /**
       * return the maximum address allocated
       */
      unsigned int get_max_address() const;

      /**
       * set the bus data bitsize
       */
      void set_bus_data_bitsize(unsigned int bitsize) {bus_data_bitsize=bitsize;}

      /**
       * return the bitsize of the data bus
       */
      unsigned int get_bus_data_bitsize() const {return bus_data_bitsize;}

      /**
       * set the bus address bitsize
       */
      void set_bus_addr_bitsize(unsigned int bitsize) {bus_addr_bitsize=bitsize;}

      /**
       * return the bitsize of the address bus
       */
      unsigned int get_bus_addr_bitsize() const {return bus_addr_bitsize;}

      /**
       * set the bus size bitsize
       */
      void set_bus_size_bitsize(unsigned int bitsize) {bus_size_bitsize=bitsize;}

      /**
       * return the bitsize of the size bus
       */
      unsigned int get_bus_size_bitsize() const {return bus_size_bitsize;}

      /**
       * set the maximum the bitsize of the aligned accesses
       */
      void set_aligned_bitsize(unsigned int bitsize) {aligned_bitsize=bitsize;}

      /**
       * return the maximum bitsize of the aligned accesses
       */
      unsigned int get_aligned_bitsize() const {return aligned_bitsize;}

      /**
       * set the BRAM bitsize
       */
      void set_bram_bitsize(unsigned int bitsize) {bram_bitsize=bitsize;}

      /**
       * return the BRAM bitsize
       */
      unsigned int get_bram_bitsize() const {return bram_bitsize;}

      /**
       * set the maximum BRAM bitsize
       */
      void set_maxbram_bitsize(unsigned int bitsize) {maxbram_bitsize=bitsize;}

      /**
       * return the BRAM bitsize
       */
      unsigned int get_maxbram_bitsize() const {return maxbram_bitsize;}

      /**
       * define if the Spec has data that can be externally accessed
       */
      void set_intern_shared_data(bool has_accesses) { intern_shared_data=has_accesses;}

      /**
       *return true in case the specification has data that can be externally accessed
       */
      bool has_intern_shared_data() const {return intern_shared_data;}

      /**
       * define if there exist an object used by the Spec with an address not known at compile time
       */
      void set_use_unknown_addresses(bool accesses) {use_unknown_addresses=accesses;}

      /**
       *return true in case the specification use addresses not known at compile time
       */
      bool has_unknown_addresses() const {return use_unknown_addresses;}

      /**
       * define if there exist an implicit call of memcpy
       */
      void set_implicit_memcpy(bool cond) {implicit_memcpy=cond;}

      /**
       *return true in case the specification has an implicit call of memcpy
       */
      bool has_implicit_memcpy() const {return implicit_memcpy;}

      /**
       * return the internal base address alignment.
       */
      unsigned int get_internal_base_address_alignment() const { return internal_base_address_alignment; }

      /**
       * set the internal base address alignment
       * @param _internal_base_address_alignment is the new alignment
       */
      void set_internal_base_address_alignment(unsigned int _internal_base_address_alignment);

      /**
       * Propagates the memory parameters from the source (innermost) module to the target (outermost) one
       */
      static
      void propagate_memory_parameters(const structural_objectRef src, const structural_managerRef tgt);

      /**
       * Adds the given memory parameter to the corresponding object
       */
      static
      void add_memory_parameter(const structural_managerRef SM, const std::string& name, const std::string& value);

      /**
       * Writes the current memory allocation into an XML description
       */
      void xwrite(xml_element* node);

};
///refcount definition of the class
typedef refcount<memory> memoryRef;

#endif
