/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file memory_allocation.cpp
 * @brief Memory allocation based on the dominator tree of the call graph.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "memory_allocation.hpp"

#include "hls_manager.hpp"
#include "hls_target.hpp"
#include "target_device.hpp"
#include "memory.hpp"

#include "behavioral_helper.hpp"
#include "call_graph.hpp"
#include "call_graph_manager.hpp"
#include "function_behavior.hpp"
#include "op_graph.hpp"
#include "tree_manager.hpp"
#include "tree_helper.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"

///implemented algorithms
#include "mem_dominator_allocation.hpp"
#include "mem_xml_allocation.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"

#include "Parameter.hpp"


static inline unsigned int resize_to_8_16_32_64_128(unsigned int value)
{
   if(value  <= 8)
      return 8;
   else if (value <= 16)
      return 16;
   else if(value <= 32)
      return 32;
   else if(value <= 64)
      return 64;
   else if(value <= 128)
      return 128;
   else
      THROW_ERROR("not expected size " + boost::lexical_cast<std::string>(value));
   return 0;
}


memory_allocation::memory_allocation(policy_t _policy, const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   HLS_step(_Param, _HLSMgr, _funId),
   policy(_policy)
{

}

memory_allocation::~memory_allocation()
{

}

memory_allocationRef memory_allocation::factory(algorithm_t algorithm, policy_t policy, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   switch(algorithm)
   {
      case DOMINATOR:
         return memory_allocationRef(new mem_dominator_allocation(policy, Param, HLSMgr, funId));
      case XML_SPECIFICATION:
         return memory_allocationRef(new mem_xml_allocation(policy, Param, HLSMgr, funId));
      default:
         THROW_UNREACHABLE("Not supported allocation algorithm");
   }
   THROW_UNREACHABLE("");
   return memory_allocationRef();
}

memory_allocationRef memory_allocation::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string algorithm;
   if (CE_XVM(algorithm, node)) LOAD_XVM(algorithm, node);

   std::string policy;
   if (CE_XVM(policy, node)) LOAD_XVM(policy, node);

   algorithm_t algorithm_e = static_cast<algorithm_t>(Param->getOption<unsigned int>(OPT_memory_allocation_algorithm));
   policy_t policy_e = static_cast<policy_t>(Param->getOption<unsigned int>(OPT_memory_allocation_policy));

   if (algorithm == "DOMINATOR")
      algorithm_e = DOMINATOR;
   else
      THROW_ERROR("Memory allocation algorithm \"" + algorithm + "\" currently not supported");

   if (policy == "LSS")
      policy_e = LSS;
   else if (policy == "GSS")
      policy_e = GSS;
   else if (policy == "ALL_BRAM")
      policy_e = ALL_BRAM;
   else if (policy == "NO_BRAM")
      policy_e = NO_BRAM;
   else if (policy == "EXT_PIPELINED_BRAM")
      policy_e = EXT_PIPELINED_BRAM;
   else if (policy != "")
      THROW_ERROR("Memory allocation policy \"" + policy + "\" currently not supported");

   return memory_allocation::factory(algorithm_e, policy_e, Param, HLSMgr, funId);
}

void memory_allocation::setup_memory_allocation()
{
   const CallGraphManagerConstRef call_graph_manager = HLSMgr->CGetCallGraphManager();
   /// the analysis has to be performed only on the reachable functions
   call_graph_manager->GetReachedBodyFunctions(sort_list);

   std::list<unsigned int>::iterator It, End = sort_list.end();
   for(It = sort_list.begin(); It != End; ++It)
   {
      const FunctionBehaviorConstRef function_behavior = HLSMgr->CGetFunctionBehavior(*It);
#ifndef NDEBUG
      const BehavioralHelperConstRef behavioral_helper = function_behavior->CGetBehavioralHelper();
#endif
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Analyzing function: " + behavioral_helper->get_function_name());
      /// add parm_decls that have to be copied
      const std::set<unsigned int>& parm_decl_copied = function_behavior->get_parm_decl_copied();
      for(std::set<unsigned int>::const_iterator p = parm_decl_copied.begin(); p != parm_decl_copied.end(); ++p)
      {
         HLSMgr->Rmem->add_parm_decl_copied(*p);
      }
      /// add parm_decls that have to be stored
      const std::set<unsigned int>& parm_decl_stored = function_behavior->get_parm_decl_stored();
      for(std::set<unsigned int>::const_iterator p = parm_decl_stored.begin(); p != parm_decl_stored.end(); ++p)
      {
         HLSMgr->Rmem->add_parm_decl_stored(*p);
      }
      /// add actual parameters that have to be loaded
      const std::set<unsigned int>& parm_decl_loaded = function_behavior->get_parm_decl_loaded();
      for(std::set<unsigned int>::const_iterator p = parm_decl_loaded.begin(); p != parm_decl_loaded.end(); ++p)
      {
         HLSMgr->Rmem->add_actual_parm_loaded(*p);
      }
   }
}

void memory_allocation::finalize_memory_allocation()
{
   THROW_ASSERT(sort_list.size(), "Empty list of functions to be analyzed");
   bool use_unknown_address = false;
   const tree_managerRef TreeM = HLSMgr->get_tree_manager();
   std::list<unsigned int>::iterator It = sort_list.begin(), End = sort_list.end();
   for(; It != End;It=++It)
   {
      const FunctionBehaviorConstRef FB = HLSMgr->CGetFunctionBehavior(*It);
      const BehavioralHelperConstRef BH = FB->CGetBehavioralHelper();
      const std::list<unsigned int>& parameters = BH->get_parameters();

      if(FB->get_dereference_unknown_addr())
         std::cerr <<  "This function uses unknown addresses deref: " + BH->get_function_name() << std::endl;

      use_unknown_address |= FB->get_dereference_unknown_addr();


      for(std::list<unsigned int>::const_iterator p = parameters.begin(); p != parameters.end(); p++)
      {
         if(HLSMgr->Rmem->is_parm_decl_copied(*p) && !HLSMgr->Rmem->is_parm_decl_stored(*p))
         {
            HLSMgr->Rmem->set_implicit_memcpy(true);
         }
      }
   }

   if(HLSMgr->Rmem->has_implicit_memcpy())
   {
      unsigned int memcpy_function_id = TreeM->function_index("__builtin_memcpy");
      if(std::find(sort_list.begin(), sort_list.end(), memcpy_function_id) != sort_list.end())
         sort_list.erase(std::find(sort_list.begin(), sort_list.end(), memcpy_function_id));
      sort_list.push_front(memcpy_function_id);
   }

   unsigned int maximum_bus_size = 0, maximum_accessed_bitsize = 0;
   bool use_databus_width = false;
   bool has_intern_shared_data = false;
   bool has_misaligned_indirect_ref = false;

   /// looking for the maximum data bus size needed
   End = sort_list.end();
   std::list<unsigned int>::iterator It_next = It = sort_list.begin();
   for(; It != End;It=It_next)
   {
      ++It_next;
      const FunctionBehaviorConstRef function_behavior = HLSMgr->CGetFunctionBehavior(*It);
      const BehavioralHelperConstRef behavioral_helper = function_behavior->CGetBehavioralHelper();
      bool is_top = (behavioral_helper->get_function_index() == HLSMgr->CGetCallGraphManager()->GetRootFunction());
      const std::list<unsigned int>& parameters = behavioral_helper->get_parameters();
      for(std::list<unsigned int>::const_iterator p = parameters.begin(); p != parameters.end(); p++)
      {
         if(HLSMgr->Rmem->is_parm_decl_copied(*p) && !HLSMgr->Rmem->is_parm_decl_stored(*p))
         {
            use_databus_width = true;
            maximum_bus_size = std::max(maximum_bus_size, 8u);
         }
         if(It_next == End && !use_unknown_address)
         {
            if(*p && tree_helper::is_a_pointer(TreeM, *p))
            {
               if(is_top)
               {
                  use_unknown_address = true;
                  std::cerr <<  "This function uses unknown addresses: " + behavioral_helper->get_function_name() << std::endl;
               }
            }
         }
      }
      const std::set<unsigned int>& parm_decl_stored = function_behavior->get_parm_decl_stored();
      for(std::set<unsigned int>::const_iterator p = parm_decl_stored.begin(); p != parm_decl_stored.end(); ++p)
      {
         maximum_bus_size = std::max(maximum_bus_size, tree_helper::size(TreeM, tree_helper::get_type_index(TreeM,*p)));
      }
      const std::map<unsigned int, memory_symbolRef>& function_vars = HLSMgr->Rmem->get_function_vars(*It);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Analyzing function for bus size: " + behavioral_helper->get_function_name());
      const OpGraphConstRef g = function_behavior->CGetOpGraph(FunctionBehavior::CFG);
      graph::vertex_iterator v, v_end;
      for (boost::tie(v, v_end) = boost::vertices(*g); v != v_end; ++v)
      {
         std::string current_op = GET_OP(g, *v);
         PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "- Processing operation for bus size: " + current_op + " - " + GET_NAME(g, *v));
         std::vector<HLS_manager::io_binding_type> var_read = HLSMgr->get_required_values(*It, *v);
         if (LOAD == current_op || STORE == current_op)
         {
            const tree_nodeRef curr_tn = TreeM->get_tree_node_const(g->CGetOpNodeInfo(*v)->node_id);
            gimple_assign* me = GetPointer<gimple_assign>(curr_tn);
            THROW_ASSERT(me, "only gimple_assign's are allowed as memory operations");
            unsigned int var = 0;
            if (GET_OP(g, *v) == STORE)
            {
               var = tree_helper::get_base_index(TreeM, GET_INDEX_NODE(me->op0));
               if(tree_helper::is_a_misaligned_vector(TreeM, GET_INDEX_NODE(me->op0)))
                  has_misaligned_indirect_ref = true;

            }
            else
            {
               var = tree_helper::get_base_index(TreeM, GET_INDEX_NODE(me->op1));
               if(tree_helper::is_a_misaligned_vector(TreeM, GET_INDEX_NODE(me->op1)))
                  has_misaligned_indirect_ref = true;
            }
            /// check if is a global variable may be accessed from an external component
            if(!has_intern_shared_data && var)
            {
               const tree_nodeRef var_tn = TreeM->get_tree_node_const(var);
               var_decl *vd = GetPointer<var_decl>(var_tn);
               if(vd && (((!vd->scpe || GET_NODE(vd->scpe)->get_kind() == translation_unit_decl_K) && !vd->static_flag) || tree_helper::is_volatile(TreeM,var)))
                  has_intern_shared_data = true; /// an external component can access the var eventually (global and volative vars)
            }
            unsigned int prec = 0;
            unsigned int return_index;
            unsigned int algn = 0;
            unsigned int value_bitsize;
            const tree_nodeRef type = tree_helper::get_type_node(GET_NODE(me->op0), return_index);
            if (type && (type->get_kind() == integer_type_K))
               prec = GetPointer<integer_type>(type)->prec;
            if (type && (type->get_kind() == integer_type_K))
               algn = GetPointer<integer_type>(type)->algn;
            if (prec != algn && prec % algn) ///bitfield management
            {
               value_bitsize = prec;
            }
            if (GET_OP(g, *v) == STORE)
            {
               unsigned int size_var = std::get<0>(var_read[0]);
               unsigned int size_type_index = tree_helper::get_type_index(TreeM, size_var);
               value_bitsize = tree_helper::size(TreeM, size_type_index);
            }
            else
            {
               unsigned int size_var = HLSMgr->get_produced_value(*It, *v);
               unsigned int size_type_index = tree_helper::get_type_index(TreeM, size_var);
               value_bitsize = tree_helper::size(TreeM, size_type_index);
            }
            if (var == 0 || function_vars.find(var) == function_vars.end())
            {
               maximum_bus_size = std::max(maximum_bus_size, value_bitsize);
            }
            else
            {
               maximum_accessed_bitsize = std::max(maximum_accessed_bitsize, value_bitsize);
            }
         }
         else
         {
            if(current_op == MEMCPY || current_op == MEMCMP || current_op == MEMSET)
            {
               use_databus_width = true;
               maximum_bus_size = std::max(maximum_bus_size, 8u);
            }
            std::vector<HLS_manager::io_binding_type>::const_iterator vr_it_end = var_read.end();
            for(std::vector<HLS_manager::io_binding_type>::const_iterator vr_it = var_read.begin(); vr_it != vr_it_end; ++vr_it)
            {
               unsigned int var = std::get<0>(*vr_it);
               if(var && tree_helper::is_a_pointer(TreeM, var))
               {
                  unsigned int type_index;
                  const tree_nodeRef var_node = TreeM->get_tree_node_const(var);
                  const tree_nodeRef type_node = tree_helper::get_type_node(var_node, type_index);
                  pointer_type * pt = GetPointer<pointer_type>(type_node);
                  unsigned bitsize = 1;
                  tree_helper::accessed_greatest_bitsize(TreeM, GET_NODE(pt->ptd), GET_INDEX_NODE(pt->ptd), bitsize);
                  maximum_bus_size = std::max(maximum_bus_size, bitsize);
               }
            }
         }
      }
   }

   const HLS_targetRef HLS_T = HLSMgr->get_HLS_target();
   unsigned int aligned_bitsize;
   unsigned int bram_bitsize, data_bus_bitsize, addr_bus_bitsize, size_bus_bitsize;
   unsigned int bram_bitsize_min = HLS_T->get_target_device()->get_parameter<unsigned int>("BRAM_bitsize_min");
   unsigned int bram_bitsize_max = HLS_T->get_target_device()->get_parameter<unsigned int>("BRAM_bitsize_max");
   HLSMgr->Rmem->set_maxbram_bitsize(bram_bitsize_max);

   maximum_bus_size = resize_to_8_16_32_64_128(maximum_bus_size);

   if(has_misaligned_indirect_ref)
   {
      if(maximum_bus_size>bram_bitsize_max)
         THROW_ERROR("Unsoppurted data bus size. In case, try a device supporting this BRAM BITSIZE: " + STR(maximum_bus_size) + " available maximum BRAM BITSIZE: " + STR(bram_bitsize_max));
      else
         bram_bitsize = maximum_bus_size;
   }
   else if(maximum_bus_size/2>bram_bitsize_max)
   {
      THROW_ERROR("Unsoppurted data bus size. In case, try a device supporting this BRAM BITSIZE: " + STR(maximum_bus_size/2) + " available maximum BRAM BITSIZE: " + STR(bram_bitsize_max));
   }
   else
   {
      bram_bitsize = maximum_bus_size/2;
   }

   if(bram_bitsize < bram_bitsize_min)
      bram_bitsize = bram_bitsize_min;

   if(bram_bitsize < 8)
      bram_bitsize = 8;


   if(!use_databus_width || use_unknown_address)
      aligned_bitsize = 8;
   else
      aligned_bitsize = 2*bram_bitsize;

   data_bus_bitsize = maximum_bus_size;
   HLSMgr->Rmem->set_bus_data_bitsize(data_bus_bitsize);
   if(Param->isOption(OPT_addr_bus_bitsize))
      addr_bus_bitsize = Param->getOption<unsigned int>(OPT_addr_bus_bitsize);
   else if(use_unknown_address)
      addr_bus_bitsize = 32;
   else if(has_intern_shared_data)
      addr_bus_bitsize = 32;
   else if(HLSMgr->Rmem->get_memory_address()-HLSMgr->base_address>0)
      addr_bus_bitsize = 32;
   else
   {
      unsigned int addr_range = HLSMgr->Rmem->get_max_address();
      unsigned int index;
      for(index = 1; addr_range >= (1u<<index) ; ++index);
      addr_bus_bitsize = index+1;
   }
   HLSMgr->Rmem->set_bus_addr_bitsize(addr_bus_bitsize);
   maximum_accessed_bitsize = std::max(maximum_accessed_bitsize, data_bus_bitsize);
   for(size_bus_bitsize=4; maximum_accessed_bitsize >= (1u<<size_bus_bitsize); ++size_bus_bitsize);
   HLSMgr->Rmem->set_bus_size_bitsize(size_bus_bitsize);

   HLSMgr->Rmem->set_aligned_bitsize(aligned_bitsize);
   HLSMgr->Rmem->set_bram_bitsize(bram_bitsize);
   HLSMgr->Rmem->set_intern_shared_data(has_intern_shared_data);
   HLSMgr->Rmem->set_use_unknown_addresses(use_unknown_address);

   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "BRAM bitsize: " + STR(bram_bitsize));
   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, (use_databus_width ? std::string("Spec may exploit DATA bus width") : std::string("Spec may not exploit DATA bus width")));
   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, (!use_unknown_address ? std::string("All the data have a known address") : std::string("Spec accesses data having an address unknown at compile time")));
   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, (!has_intern_shared_data ? std::string("Intern data is not externally accessible") : std::string("Intern data may be accessed")));
   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Aligned bitsize: " + STR(aligned_bitsize));
   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "DATA bus bitsize: " + STR(data_bus_bitsize));
   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "ADDRESS bus bitsize: " + STR(addr_bus_bitsize));
   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "SIZE bus bitsize: " + STR(size_bus_bitsize));

}
