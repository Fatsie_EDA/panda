/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file mem_dominator_allocation.hpp
 * @brief Class to allocate memories in HLS based on dominators
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef MEMORY_DOMINATOR_ALLOCATION_HPP
#define MEMORY_DOMINATOR_ALLOCATION_HPP

#include "memory_allocation.hpp"

CONSTREF_FORWARD_DECL(CallGraphManager);
CONSTREF_FORWARD_DECL(CallGraph);

class mem_dominator_allocation : public memory_allocation
{

      /// recursively compute referred
      void recursively_referred_proxies(unsigned int curr_funID, unsigned int funID, unsigned int var_index, const CallGraphConstRef cg, const CallGraphManagerConstRef CG);

   public:

      /**
       * Constructor
       */
      mem_dominator_allocation(policy_t policy, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor
       */
      ~mem_dominator_allocation();

      /**
       * Executes the memory allocation and creates the memory datastructure
       */
      void exec();

      /**
       * Return the name of the step
       */
      std::string get_kind_text() const;

};

#endif
