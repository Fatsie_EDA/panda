/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file mem_dominator_allocation.cpp
 * @brief Memory allocation based on the dominator tree of the call graph.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "mem_dominator_allocation.hpp"

#include "memory.hpp"
#include "hls.hpp"
#include "hls_target.hpp"
#include "hls_manager.hpp"
#include "target_device.hpp"

#include "call_graph.hpp"
#include "call_graph_manager.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"
#include "op_graph.hpp"

#include "tree_helper.hpp"
#include "tree_manager.hpp"
#include "tree_reindex.hpp"
#include "tree_node.hpp"

#include "Parameter.hpp"
#include "constant_strings.hpp"
#include "cpu_time.hpp"
#include "Dominance.hpp"

mem_dominator_allocation::mem_dominator_allocation(policy_t _policy, const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   memory_allocation(_policy, _Param, _HLSMgr, _funId)
{

}

mem_dominator_allocation::~mem_dominator_allocation()
{

}

void mem_dominator_allocation::recursively_referred_proxies(unsigned int curr_funID, unsigned int funID, unsigned int var_index, const CallGraphConstRef cg, const CallGraphManagerConstRef CG)
{
   InEdgeIterator ei, ei_end;
   vertex cur_vertex = CG->GetVertex(curr_funID);
   for(boost::tie(ei, ei_end) = boost::in_edges(cur_vertex, *cg); ei != ei_end; ++ei)
   {
      vertex src = boost::source(*ei, *cg);
      unsigned int src_funID = CG->get_function(src);
      if(src_funID != funID)
      {
         HLSMgr->Rmem->add_referred_variable_proxy(src_funID, var_index);
         recursively_referred_proxies(src_funID, funID, var_index, cg, CG);
      }
   }
}

void mem_dominator_allocation::exec()
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Performing memory allocation...");
   const tree_managerRef TreeM = HLSMgr->get_tree_manager();

   const HLS_targetRef HLS_T = HLSMgr->get_HLS_target();
   ///TODO: to be fixed with information coming out from the target platform description
   unsigned int base_address = Param->getOption<unsigned int>(OPT_base_address);
   unsigned int max_bram = HLS_T->get_target_device()->get_parameter<unsigned int>("BRAM_bitsize_max");
   HLSMgr->base_address = base_address;
   ///information about memory allocation to be shared across the functions
   HLSMgr->Rmem = memoryRef(new memory(TreeM, base_address, max_bram));
   setup_memory_allocation();

   const CallGraphManagerConstRef CG = HLSMgr->CGetCallGraphManager();
   /// the analysis has to be performed only on the reachable functions
   const CallGraphConstRef cg = CG->CGetCallGraph();
   const unsigned int top_function = CG->GetRootFunction();
   vertex top_vertex = CG->GetVertex(top_function);
   std::unordered_set<vertex> vertex_subset;
   std::list<unsigned int>::iterator It, End = sort_list.end();
   for(It = sort_list.begin(); It != End; ++It)
      vertex_subset.insert(CG->GetVertex(*It));

   refcount<dominance<graph> > cg_dominators;
   const CallGraphConstRef subgraph = CG->CGetCallSubGraph(vertex_subset);
   /// we do not need the exit vertex since the post-dominator graph is not used
   cg_dominators = refcount<dominance<graph> >(new dominance<graph>(*subgraph, top_vertex, NULL_VERTEX, Param));
   cg_dominators->calculate_dominance_info(dominance<graph>::CDI_DOMINATORS);
   const std::unordered_map<vertex, vertex>& cg_dominator_map = cg_dominators->get_dominator_map();


   std::map<unsigned int, std::set<vertex> > var_map;
   std::map<unsigned int, std::set<unsigned int> > where_used;
   std::set<unsigned int> written_object;
   for(It = sort_list.begin(); It != End; ++It)
   {
      const FunctionBehaviorConstRef function_behavior = HLSMgr->CGetFunctionBehavior(*It);
#ifndef NDEBUG
      const BehavioralHelperConstRef BH = function_behavior->CGetBehavioralHelper();
#endif
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Analyzing function: " + BH->get_function_name());
      vertex vert_dominator, current_vertex;
      current_vertex = CG->GetVertex(*It);
      THROW_ASSERT(cg_dominator_map.find(current_vertex) != cg_dominator_map.end(), "Dominator vertex not in the dominator tree: " + HLSMgr->CGetFunctionBehavior(CG->get_function(current_vertex))->CGetBehavioralHelper()->get_function_name());
      if(boost::in_degree(current_vertex, *cg) != 1)
         vert_dominator = cg_dominator_map.find(current_vertex)->second;
      else
         vert_dominator = current_vertex;
      const std::set<unsigned int>& function_mem = function_behavior->get_function_mem();
      for(std::set<unsigned int>::const_iterator v = function_mem.begin(); v != function_mem.end(); v++)
      {
         var_map[*v].insert(vert_dominator);
         where_used[*v].insert(*It);
         if(function_behavior->get_written_objects().find(*v) != function_behavior->get_written_objects().end())
            written_object.insert(*v);
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Dominator Vertex: " + HLSMgr->CGetFunctionBehavior(CG->get_function(vert_dominator))->CGetBehavioralHelper()->get_function_name() + " - Variable to be stored: " + BH->PrintVariable(*v));
      }
      const OpGraphConstRef g = function_behavior->CGetOpGraph(FunctionBehavior::CFG);
      std::map<unsigned int, unsigned int> var_size;
      graph::vertex_iterator v, v_end;
      for (boost::tie(v, v_end) = boost::vertices(*g); v != v_end; ++v)
      {
         std::string current_op = GET_OP(g, *v);
         PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "- Processing operation for bus size: " + current_op + " - " + GET_NAME(g, *v));
         if (LOAD == current_op || STORE == current_op)
         {
            const tree_nodeRef curr_tn = TreeM->get_tree_node_const(g->CGetOpNodeInfo(*v)->node_id);
            gimple_assign* me = GetPointer<gimple_assign>(curr_tn);
            THROW_ASSERT(me, "only gimple_assign's are allowed as memory operations");
            unsigned int var = 0;
            if (GET_OP(g, *v) == STORE)
            {
               var = tree_helper::get_base_index(TreeM, GET_INDEX_NODE(me->op0));
            }
            else
            {
               var = tree_helper::get_base_index(TreeM, GET_INDEX_NODE(me->op1));
            }
            unsigned int value_bitsize;
            if (GET_OP(g, *v) == STORE)
            {
               std::vector<HLS_manager::io_binding_type> var_read = HLSMgr->get_required_values(*It, *v);
               unsigned int size_var = std::get<0>(var_read[0]);
               unsigned int size_type_index = tree_helper::get_type_index(TreeM, size_var);
               value_bitsize = tree_helper::size(TreeM, size_type_index);
            }
            else
            {
               unsigned int size_var = HLSMgr->get_produced_value(*It, *v);
               unsigned int size_type_index = tree_helper::get_type_index(TreeM, size_var);
               value_bitsize = tree_helper::size(TreeM, size_type_index);
            }
            if(var!=0)
            {
               if(var_size.find(var) == var_size.end())
               {
                  unsigned int elmt_bitsize=1;
                  unsigned int type_index = tree_helper::get_type_index(TreeM, var);
                  tree_nodeRef type_node = TreeM->get_tree_node_const(type_index);
                  tree_helper::accessed_greatest_bitsize(TreeM, type_node, type_index, elmt_bitsize);
                  if(value_bitsize != elmt_bitsize)
                     HLSMgr->Rmem->set_sds_var(var, false);
                  else
                     HLSMgr->Rmem->set_sds_var(var, true);
                  var_size[var]=value_bitsize;
               }
               else
               {
                  if(var_size.find(var)->second != value_bitsize)
                     HLSMgr->Rmem->set_sds_var(var, false);
               }
            }
         }
      }

   }

   /// compute the number of istances for each function
   std::map<vertex, unsigned int> num_instances;
   num_instances[top_vertex] = 1;
   std::list<unsigned int>::reverse_iterator rIt, rEnd = sort_list.rend();
   for(rIt = sort_list.rbegin(); rIt != rEnd; ++rIt)
   {
      vertex cur = CG->GetVertex(*rIt);
      THROW_ASSERT(num_instances.find(cur) != num_instances.end(), "missing number of instances of function " + HLSMgr->CGetFunctionBehavior(CG->get_function(cur))->CGetBehavioralHelper()->get_function_name());
      unsigned int cur_instances = num_instances.find(cur)->second;
      OutEdgeIterator eo, eo_end;
      for(boost::tie(eo, eo_end) = boost::out_edges(cur, *cg); eo != eo_end; ++eo)
      {
         vertex tgt = boost::target(*eo, *cg);
         unsigned int n_call_points = static_cast<unsigned int>(Cget_edge_info<FunctionEdgeInfo, const CallGraph>(*eo, *cg)->call_points.size());
         if(num_instances.find(tgt) == num_instances.end())
            num_instances[tgt] = cur_instances * n_call_points;
         else
            num_instances[tgt] += cur_instances * n_call_points;
      }
   }
   THROW_ASSERT(num_instances.find(top_vertex)->second == 1, "something of wrong happened");

   /// find the common dominator and decide where to allocate
   std::map<unsigned int, std::pair<unsigned int, bool> >memory_allocation_map;
   const std::map<unsigned int, std::set<vertex> >::const_iterator it_end = var_map.end();
   for(std::map<unsigned int, std::set<vertex> >::const_iterator it = var_map.begin(); it != it_end; ++it)
   {
      unsigned int funID=0;
      unsigned int var_index = it->first;
      THROW_ASSERT(var_index, "null var index unexpected");
      if(it->second.size() == 1)
      {
         vertex cur = *it->second.begin();
         /// look for a single instance function in case the object is not a ROM
         if(written_object.find(var_index) != written_object.end())
         {
            while(num_instances.find(cur)->second != 1)
               cur = cg_dominator_map.find(cur)->second;
         }
         funID = CG->get_function(cur);
      }
      else
      {
         std::set<vertex>::const_iterator vert_it_end = it->second.end();
         if(it->second.find(top_vertex) != vert_it_end)
            funID = CG->get_function(top_vertex);
         else
         {
            std::set<vertex>::const_iterator vert_it = it->second.begin();
            std::list<vertex> dominator_list1;
            vertex cur = *vert_it;
            dominator_list1.push_front(cur);
            do
            {
               cur = cg_dominator_map.find(cur)->second;
               dominator_list1.push_front(cur);
            }
            while(cur != top_vertex);
            ++vert_it;
            std::list<vertex>::const_iterator last=dominator_list1.end();
            for(; vert_it != vert_it_end; ++vert_it)
            {
               std::list<vertex> dominator_list2;
               cur = *vert_it;
               dominator_list2.push_front(cur);
               do
               {
                  cur = cg_dominator_map.find(cur)->second;
                  dominator_list2.push_front(cur);
               }
               while(cur != top_vertex);
               /// find the common dominator between two candidates
               std::list<vertex>::const_iterator dl1_it=dominator_list1.begin(), dl2_it=dominator_list2.begin(), dl2_it_end=dominator_list2.end(), cur_last=dominator_list1.begin();
               while(dl1_it!=last && dl2_it!=dl2_it_end && *dl1_it == *dl2_it && (num_instances.find(*dl1_it)->second == 1 || written_object.find(var_index) == written_object.end()))
               {
                  cur = *dl1_it;
                  ++dl1_it;
                  cur_last = dl1_it;
                  ++dl2_it;
               }
               last=cur_last;
               funID = CG->get_function(cur);
               if(cur == top_vertex)
                  break;
            }
         }
      }
      THROW_ASSERT(funID, "null function id index unexpected");

      bool is_internal = false;
      const tree_nodeRef tn = TreeM->get_tree_node_const(var_index);
      switch(policy)
      {
         case LSS:
         {
            var_decl * vd = GetPointer<var_decl>(tn);
            if (vd && (vd->static_flag || (vd->scpe && GET_NODE(vd->scpe)->get_kind() != translation_unit_decl_K)))
               is_internal = true;
            if (GetPointer<string_cst>(tn))
               is_internal = true;
            if (HLSMgr->Rmem->is_parm_decl_copied(var_index) || HLSMgr->Rmem->is_parm_decl_stored(var_index))
               is_internal = true;
            break;
         }
         case GSS:
         {
            var_decl * vd = GetPointer<var_decl>(tn);
            if (vd && (vd->static_flag || !vd->scpe || GET_NODE(vd->scpe)->get_kind() == translation_unit_decl_K))
               is_internal = true;
            if (GetPointer<string_cst>(tn))
               is_internal = true;
            if (HLSMgr->Rmem->is_parm_decl_copied(var_index) || HLSMgr->Rmem->is_parm_decl_stored(var_index))
               is_internal = true;
            break;
         }
         case ALL_BRAM:
         {
            is_internal = true;
            break;
         }
         case EXT_PIPELINED_BRAM:
         case NO_BRAM:
         {
            is_internal = false;
            break;
         }
         default:
            THROW_ERROR("not supported memory allocation policy");
      }
      memory_allocation_map[var_index] = std::pair<unsigned int, bool>(funID, is_internal);
   }

   /// change the alignment in case is requested
   if(Param->isOption(OPT_sparse_memory) && Param->getOption<bool>(OPT_sparse_memory))
   {
      /// change the internal alignment to improve the decoding logic
      unsigned int max_byte_size = HLSMgr->Rmem->get_internal_base_address_alignment();
      for(std::map<unsigned int, std::set<vertex> >::const_iterator it = var_map.begin(); it != it_end; ++it)
      {
         unsigned int var_index = it->first;
         THROW_ASSERT(var_index, "null var index unexpected");
         if(memory_allocation_map[var_index].second)
         {
            unsigned int curr_size = tree_helper::size(TreeM,  var_index) / 8;
            max_byte_size = std::max(curr_size, max_byte_size);
         }
      }
      /// Round up to the next highest power of 2
      max_byte_size--;
      max_byte_size |= max_byte_size >> 1;
      max_byte_size |= max_byte_size >> 2;
      max_byte_size |= max_byte_size >> 4;
      max_byte_size |= max_byte_size >> 8;
      max_byte_size |= max_byte_size >> 16;
      max_byte_size++;
      HLSMgr->Rmem->set_internal_base_address_alignment(max_byte_size);
   }

   /// really allocate
   for(std::map<unsigned int, std::set<vertex> >::const_iterator it = var_map.begin(); it != it_end; ++it)
   {
      unsigned int var_index = it->first;
      THROW_ASSERT(var_index, "null var index unexpected");
      unsigned int funID=memory_allocation_map[var_index].first;
      bool is_internal = memory_allocation_map[var_index].second;
      std::string var_index_string;
      bool is_dynamic_address_used = false;

      if (is_internal)
      {
         THROW_ASSERT(where_used[var_index].size() > 0, "variable not used anywhere");
         const FunctionBehaviorConstRef function_behavior = HLSMgr->CGetFunctionBehavior(*(where_used[var_index].begin()));
         const BehavioralHelperConstRef BH = function_behavior->CGetBehavioralHelper();
         var_index_string = BH->PrintVariable(var_index);
         ///check dynamic address use
         std::set<unsigned int>::const_iterator wiu_it_end = where_used[var_index].end();
         for(std::set<unsigned int>::const_iterator wiu_it = where_used[var_index].begin(); wiu_it != wiu_it_end && !is_dynamic_address_used; ++wiu_it)
         {
            const FunctionBehaviorConstRef cur_function_behavior = HLSMgr->CGetFunctionBehavior(*wiu_it);
            if(cur_function_behavior->get_dynamic_address().find(var_index) != cur_function_behavior->get_dynamic_address().end())
               is_dynamic_address_used = true;
         }
         if(is_dynamic_address_used)
            HLSMgr->Rmem->set_sds_var(var_index, false);
         if(!HLSMgr->Rmem->has_sds_var(var_index))
            HLSMgr->Rmem->set_sds_var(var_index, false);
         if(!is_dynamic_address_used &&///we never have &(var_index_object)
               written_object.find(var_index) == written_object.end() ///read only memory
               )
         {
            HLSMgr->Rmem->add_private_memory(var_index);
            for(std::set<unsigned int>::const_iterator wiu_it = where_used[var_index].begin(); wiu_it != wiu_it_end; ++wiu_it)
            {
               const FunctionBehaviorConstRef cur_function_behavior = HLSMgr->CGetFunctionBehavior(*wiu_it);
               const BehavioralHelperConstRef cur_BH = cur_function_behavior->CGetBehavioralHelper();
               PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Adding internal variable: " + STR(var_index) + " - " + var_index_string + " in function " + cur_BH->get_function_name());
               HLSMgr->Rmem->add_internal_variable(*wiu_it, var_index);
            }
         }
#ifdef WITHOUT_PROXY
         else if(it->second.size() == 1 && where_used[var_index].size() == 1)
         {
            if(!is_dynamic_address_used && ///we never have &(var_index_object)
                  (*(where_used[var_index].begin()) == funID) ///used in a single place
                   )
               HLSMgr->Rmem->add_private_memory(var_index);
            const FunctionBehaviorConstRef cur_function_behavior = HLSMgr->CGetFunctionBehavior(funID);
            const BehavioralHelperConstRef cur_BH = cur_function_behavior->CGetBehavioralHelper();
            PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Adding internal variable: " + STR(var_index) + " - " + var_index_string + " in function " + cur_BH->get_function_name());
            HLSMgr->Rmem->add_internal_variable(funID, var_index);
         }
         else
         {
            const FunctionBehaviorConstRef cur_function_behavior = HLSMgr->CGetFunctionBehavior(funID);
            const BehavioralHelperConstRef cur_BH = cur_function_behavior->CGetBehavioralHelper();
            PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Adding internal variable: " + STR(var_index) + " - " + var_index_string + " in function " + cur_BH->get_function_name());
            HLSMgr->Rmem->add_internal_variable(funID, var_index);
            /// add proxies
            for(std::set<unsigned int>::const_iterator wiu_it = where_used[var_index].begin(); wiu_it != wiu_it_end; ++wiu_it)
            {
               if(*wiu_it != funID)
               {
                  HLSMgr->Rmem->add_internal_variable_proxy(*wiu_it, var_index);
                  recursively_referred_proxies(*wiu_it, funID, var_index, cg, CG);
               }
            }
         }
#else
         else
         {
            if(!is_dynamic_address_used ///we never have &(var_index_object)
                   )
               HLSMgr->Rmem->add_private_memory(var_index);
            const FunctionBehaviorConstRef cur_function_behavior = HLSMgr->CGetFunctionBehavior(funID);
            const BehavioralHelperConstRef cur_BH = cur_function_behavior->CGetBehavioralHelper();
            PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Adding internal variable: " + STR(var_index) + " - " + var_index_string + " in function " + cur_BH->get_function_name());
            HLSMgr->Rmem->add_internal_variable(funID, var_index);
            /// add proxies
            for(std::set<unsigned int>::const_iterator wiu_it = where_used[var_index].begin(); wiu_it != wiu_it_end; ++wiu_it)
            {
               if(*wiu_it != funID)
               {
                  HLSMgr->Rmem->add_internal_variable_proxy(*wiu_it, var_index);
                  recursively_referred_proxies(*wiu_it, funID, var_index, cg, CG);
               }
            }
         }
#endif
      }
      else
      {
         const FunctionBehaviorConstRef function_behavior = HLSMgr->CGetFunctionBehavior(funID);
         const BehavioralHelperConstRef BH = function_behavior->CGetBehavioralHelper();
         var_index_string = BH->PrintVariable(var_index);
         PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Adding variable external to the top module: " + STR(var_index) + " - " + var_index_string);
         HLSMgr->Rmem->add_external_variable(var_index);
         is_dynamic_address_used = true;
         HLSMgr->Rmem->set_sds_var(var_index, false);
      }

      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Variable: " + var_index_string);
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - Id: " + STR(var_index));
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - Base Address: " + STR(HLSMgr->Rmem->get_base_address(var_index)));
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - Size: " + STR(tree_helper::size(TreeM,  var_index) / 8));
      if(HLSMgr->Rmem->is_private_memory(var_index)) PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - Is a private memory");
      if(HLSMgr->Rmem->is_a_proxied_variable(var_index))PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - Has proxied accesses");
      if(written_object.find(var_index) == written_object.end())
      {
         HLSMgr->Rmem->add_read_only_variable(var_index);
         PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - Is a Read Only Memory");
      }
      if(HLSMgr->Rmem->is_parm_decl_copied(var_index)) PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - Is a parm decl copied");
      if(HLSMgr->Rmem->is_actual_parm_loaded(var_index)) PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - Is an actual parm decl loaded");
      if(HLSMgr->Rmem->is_parm_decl_stored(var_index)) PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - Is a parm decl stored");
      if(is_dynamic_address_used) PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - Used &(object)");
      if(HLSMgr->Rmem->is_sds_var(var_index)) PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  - The variable is always accessed with the same data size");
   }

   finalize_memory_allocation();
}

std::string mem_dominator_allocation::get_kind_text() const
{
   return "Dominator-tree memory allocation";
}
