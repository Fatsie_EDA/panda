/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file memory_allocation.hpp
 * @brief Base class to allocate memories in high-level synthesis
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef _MEMORY_ALLOCATION_HPP_
#define _MEMORY_ALLOCATION_HPP_

#include "hls_step.hpp"
REF_FORWARD_DECL(memory_allocation);
#include <list>

/**
 * Allocation memory class
 */
class memory_allocation : public HLS_step
{
   public:

      /// Memory allocation algorithm
      typedef enum
      {
         DOMINATOR = 0,
         XML_SPECIFICATION
      } algorithm_t;

      /// Memory allocation policies
      typedef enum
      {
         LSS = 0,   /// all local variables, static variables and strings are allocated on BRAMs
         GSS,       /// all global variables, static variables and strings are allocated on BRAMs
         ALL_BRAM,  /// all objects that need to be stored in memory are allocated on BRAMs
         NO_BRAM,    /// all objects that need to be stored in memory are allocated on an external memory
         EXT_PIPELINED_BRAM /// all objects that need to be stored in memory are allocated on an external pipelined memory
      } policy_t;

      /// Number of Channels
      typedef enum
      {
         MEM_ACC_11 = 0, /// for each memory at maximum one direct access and one indirect access
         MEM_ACC_N1,     /// for each memory at maximum n parallel direct accesses and one indirect access
         MEM_ACC_NN      /// for each memory at maximum n parallel direct accesses and n parallel indirect accesses
      } channel_t;

   protected:

      ///this is the adopted allocation policy
      policy_t policy;

      ///ordered list of functions to be analyzed
      std::list<unsigned int> sort_list;

      /**
       * Prepares the datastructures for the memory allocation
       */
      void setup_memory_allocation();

      /**
       * Performs a final analysis of the memory allocation to finalize the datastructure
       */
      void finalize_memory_allocation();

   public:

      /**
       * Constructor
       */
      memory_allocation(policy_t algorithm, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor
       */
      ~memory_allocation();

      /**
       * Executes the memory allocation and creates the memory datastructure
       */
      virtual void exec() = 0;

      /**
       * Returns the name of the implemented algorithm
       */
      virtual std::string get_kind_text() const = 0;

      /**
       * Factory method
       */
      static
      memory_allocationRef factory(algorithm_t algorithm, policy_t policy, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Factory method from XML file
       */
      static
      memory_allocationRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

};

#endif
