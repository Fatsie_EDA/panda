/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file state_transition_graph.cpp
 * @brief File contanining the structures necessary to manage a graph that will represent a state transition graph
 *
 * This file contains the necessary data structures used to represent a state transition graph
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */

///Header include
#include "state_transition_graph.hpp"

///Behavior include
#include "behavioral_helper.hpp"
#include "op_graph.hpp"
#include "var_pp_functor.hpp"

///Parameter include
#include "Parameter.hpp"

///Utility include
#include "simple_indent.hpp"

void StateInfo::print(std::ostream& os) const
{
   const BehavioralHelperConstRef BH = op_function_graph->CGetOpGraphInfo()->BH;
   const var_pp_functorConstRef vpp = var_pp_functorConstRef(new std_var_pp_functor(BH));

   std::list<vertex>::const_iterator i, i_end;
   i_end = executing_operations.end();
   i = executing_operations.begin();

   if (i != i_end && GET_TYPE(op_function_graph, *i) == TYPE_ENTRY)
   {
      os << "START";
      return;
   }

   if (i != i_end && GET_TYPE(op_function_graph, *i) == TYPE_EXIT)
   {
      os << "END";
      return;
   }

   os << name << " | ";
   os << " { ";
   if (i != i_end)
   {
      os << GET_NAME(op_function_graph, *i) << " --\\> ";
      std::string vertex_print = BH->print_vertex(op_function_graph, *i, vpp, true);
      boost::replace_all(vertex_print, ">", "\\>");
      boost::replace_all(vertex_print, "<", "\\<");
      boost::replace_all(vertex_print, "{", "\\{");
      boost::replace_all(vertex_print, "}", "\\}");
      boost::replace_all(vertex_print, "||", "or");
      boost::replace_all(vertex_print, "|", "or");
      os << vertex_print;
      i++;
      unsigned int num = 1;
      for (; i != i_end; i++, num++)
      {
         os << GET_NAME(op_function_graph, *i) << " --\\> ";
         vertex_print = BH->print_vertex(op_function_graph, *i, vpp, true);
         boost::replace_all(vertex_print, ">", "\\>");
         boost::replace_all(vertex_print, "<", "\\<");
         boost::replace_all(vertex_print, "{", "\\{");
         boost::replace_all(vertex_print, "}", "\\}");
         boost::replace_all(vertex_print, "||", "or");
         boost::replace_all(vertex_print, "|", "or");
         os << vertex_print;
      }
   }
   else
      os << " [none] ";

   os << " | ";
   i_end = ending_operations.end();
   i = ending_operations.begin();
   if (i != i_end)
   {
      os << GET_NAME(op_function_graph, *i) << " --\\> ";
      std::string vertex_print = BH->print_vertex(op_function_graph, *i, vpp, true);
      boost::replace_all(vertex_print, ">", "\\>");
      boost::replace_all(vertex_print, "<", "\\<");
      boost::replace_all(vertex_print, "{", "\\{");
      boost::replace_all(vertex_print, "}", "\\}");
      boost::replace_all(vertex_print, "||", "or");
      boost::replace_all(vertex_print, "|", "or");
      os << vertex_print;
      i++;
      unsigned int num = 1;
      for (; i != i_end; i++, num++)
      {
            os << GET_NAME(op_function_graph, *i) << " --\\> ";
            vertex_print = BH->print_vertex(op_function_graph, *i, vpp, true);
            boost::replace_all(vertex_print, ">", "\\>");
            boost::replace_all(vertex_print, "<", "\\<");
            boost::replace_all(vertex_print, "{", "\\{");
            boost::replace_all(vertex_print, "}", "\\}");
            boost::replace_all(vertex_print, "||", "or");
            boost::replace_all(vertex_print, "|", "or");
            os << vertex_print;
      }
   }
   else
      os << " [none] ";
   os << "}";

   os << " | ";
   std::set<unsigned int>::const_iterator si, si_end;
   si_end = BB_ids.end();
   si = BB_ids.begin();
   if (si != si_end)
   {
      os << "BB_ids = " << *si;
      ++si;
      for (; si != si_end; si++)
      {
         os << ", " << *si;
      }
   }
   else
      os << " [none] ";
}

const unsigned int TransitionInfo::DONTCARE  = 1 << 0;

void TransitionInfo::print(std::ostream& os) const
{
   const BehavioralHelperConstRef BH = op_function_graph->CGetOpGraphInfo()->BH;
   for (std::set<std::pair<vertex, unsigned int> >::const_iterator it = conditions.begin(); it != conditions.end(); it++)
   {
      if (it->second == T_COND)
         os << GET_NAME(op_function_graph, it->first) << "(T)\\n";
      else if (it->second == F_COND)
         os << GET_NAME(op_function_graph, it->first) << "(F)\\n";
      else if (it->second == DONTCARE)
         os << GET_NAME(op_function_graph, it->first) << "(-)\\n";
      else if (it->second == default_COND)
         os << GET_NAME(op_function_graph, it->first) << "(default)\\n";
      else
      {
         const var_pp_functorConstRef std(new std_var_pp_functor(BH));
         os << GET_NAME(op_function_graph, it->first) << " (" << BH->print_node(it->second, it->first, std) << ")\\n";
      }
   }
}

StateTransitionGraphInfo::StateTransitionGraphInfo(const OpGraphConstRef _op_function_graph) :
   op_function_graph(_op_function_graph), is_a_dag(true), min_cycles(0), max_cycles(0)
{}

StateTransitionGraphsCollection::StateTransitionGraphsCollection(const StateTransitionGraphInfoRef state_transition_graph_info, const ParameterConstRef _parameters) :
   graphs_collection(state_transition_graph_info, _parameters)
{}

StateTransitionGraphsCollection::~StateTransitionGraphsCollection()
{}

StateTransitionGraph::StateTransitionGraph(const StateTransitionGraphsCollectionRef state_transition_graphs_collection, int _selector) :
   graph(state_transition_graphs_collection.get(), _selector)
{}

StateTransitionGraph::StateTransitionGraph(const StateTransitionGraphsCollectionRef state_transition_graphs_collection, int _selector, std::unordered_set<vertex> _sub) :
   graph(state_transition_graphs_collection.get(), _selector, _sub)
{}

StateTransitionGraph::~StateTransitionGraph()
{}

void StateTransitionGraph::WriteDot(const std::string & file_name, const int) const
{
   const std::string output_directory = collection->parameters->getOption<std::string>(OPT_dot_directory);
   if (!boost::filesystem::exists(output_directory))
      boost::filesystem::create_directories(output_directory);
   const OpGraphConstRef op_function_graph = CGetStateTransitionGraphInfo()->op_function_graph;
   const std::string function_name = op_function_graph->CGetOpGraphInfo()->BH->get_function_name();
   const std::string complete_file_name = output_directory + function_name +"/";
   if (!boost::filesystem::exists(complete_file_name))
      boost::filesystem::create_directories(complete_file_name);
   const VertexWriterConstRef state_writer(new StateWriter(this, op_function_graph));
   const EdgeWriterConstRef transition_writer(new TransitionWriter(this, op_function_graph));
   InternalWriteDot<const StateWriter, const TransitionWriter>(complete_file_name+file_name, state_writer, transition_writer);
}

StateWriter::StateWriter(const graph* _stg, const OpGraphConstRef _op_function_graph) :
   VertexWriter(_stg, 0),
   BH(_op_function_graph->CGetOpGraphInfo()->BH),
   op_function_graph(_op_function_graph),
   entry_node(GetPointer<const StateTransitionGraphInfo>(_stg->CGetGraphInfo())->entry_node),
   exit_node(GetPointer<const StateTransitionGraphInfo>(_stg->CGetGraphInfo())->exit_node)
{}

void StateWriter::operator()(std::ostream& out, const vertex& v) const
{
   const StateInfo *temp = Cget_node_info< StateInfo > (v, *printing_graph);
   out << "[";
   if (v == entry_node or v == exit_node)
      out << "color=blue,shape=Msquare,";
   else
      out << "shape=record,";
   out << "label=\"";
   temp->print(out);
   out << "\"]";
}

TransitionWriter::TransitionWriter(const graph* _stg, const OpGraphConstRef _op_function_graph) :
   EdgeWriter(_stg, 0),
   BH(_op_function_graph->CGetOpGraphInfo()->BH),
   op_function_graph(_op_function_graph)
{}

void TransitionWriter::operator()(std::ostream& out, const EdgeDescriptor& e) const
{
   const TransitionInfo *temp = Cget_edge_info<TransitionInfo>(e, *printing_graph);
   if (ST_EDGE_NORMAL_T & printing_graph->GetSelector(e))
   {
      out << "[color=red3";
   }
   else if (ST_EDGE_FEEDBACK_T & printing_graph->GetSelector(e))
   {
      out << "[color=green2";
   }
   else
   {
      THROW_UNREACHABLE("InconsistentDataStructure");
   }
   out << ",label=\"";
   temp->print(out);
   out << "\"]";
}
