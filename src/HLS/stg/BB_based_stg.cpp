/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file BB_based_stg.hpp
 * @brief
 *
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */
#include "BB_based_stg.hpp"

#include "state_transition_graph.hpp"
#include "state_transition_graph_manager.hpp"
#include "StateTransitionGraph_constructor.hpp"

#include "allocation.hpp"
#include "fu_binding.hpp"
#include "technology_manager.hpp"
#include "technology_node.hpp"
#include "time_model.hpp"

#include "hls.hpp"
#include "hls_constraints.hpp"
#include "function_behavior.hpp"
#include "loops.hpp"
#include "loop.hpp"
#include "basic_block.hpp"
#include "op_graph.hpp"
#include "tree_basic_block.hpp"

#include "schedule.hpp"

#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"
#include "tree_helper.hpp"

#include <set>
#include <unordered_map>
#include <cmath>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/incremental_components.hpp>
#include <boost/graph/depth_first_search.hpp>

BB_based_stg::BB_based_stg(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned _funId) :
   STG_creator(_Param, _HLSMgr, _funId)
{
   HLS->STG = StateTransitionGraphManagerRef(new StateTransitionGraphManager(HLS->CGetOpGraph(FunctionBehavior::FLSAODG), Param));
}

BB_based_stg::~BB_based_stg()
{

}

static
void add_in_sched_order(std::list<vertex>& statement_list, vertex stmt, const scheduleRef &sch, const OpGraphConstRef DEBUG_PARAMETER(dfg))
{
   THROW_ASSERT(sch->is_scheduled(stmt), "First vertex is not scheduled");
   std::list<vertex>::const_iterator it_end = statement_list.end();
   std::list<vertex>::iterator it;
   THROW_ASSERT(std::find(statement_list.begin(), statement_list.end(), stmt) == statement_list.end(), "Statement already ordered: "+ GET_NAME(dfg,stmt));

   for(it = statement_list.begin(); it != it_end; it++)
   {
      THROW_ASSERT(sch->is_scheduled(*it), "Second vertex is not scheduled");
      if(sch->get_cstep(*it) > sch->get_cstep(stmt))
         break;
   }
   statement_list.insert(it, stmt);

}


void BB_based_stg::exec()
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Starting creation of STG...");

   StateTransitionGraph_constructorRef STG_builder = HLS->STG->STG_builder;
   THROW_ASSERT(STG_builder, "STG constructor not properly initialized");

   double clock_period = HLS->HLS_C->get_clock_period_resource_fraction()*HLS->HLS_C->get_clock_period();
   /// first state of a basic-block
   std::unordered_map<vertex, vertex> first_state;
   /// last state of a basic-block
   std::unordered_map<vertex, vertex> last_state;

   const OpGraphConstRef dfgRef = HLS->CGetOpGraph(FunctionBehavior::DFG);

   const scheduleRef sch = HLS->Rsch;

   const BBGraphConstRef fbb = HLS->FB->CGetBBGraph(FunctionBehavior::FBB);

   /// get entry and exit basic block
   const vertex bb_entry = fbb->CGetBBGraphInfo()->entry_vertex;
   const vertex bb_exit = fbb->CGetBBGraphInfo()->exit_vertex;

   bool first_state_p;
   bool have_previous;
   vertex previous;
   std::map<vertex, std::list<vertex> > call_states;
   std::map<vertex, std::list<vertex> > call_operations;
   std::set<vertex> already_analyzed;
   VertexIterator vit, vend;
   std::set<vertex> bb_completely_merged;
   for(boost::tie(vit, vend) = boost::vertices(*fbb); vit != vend; ++vit)
   {
      if(*vit == bb_entry)
      {
         last_state[*vit] =first_state[*vit] = HLS->STG->get_entry_state();
         continue;
      }
      else if(*vit == bb_exit)
      {
         last_state[*vit] =first_state[*vit] = HLS->STG->get_exit_state();
         continue;
      }
      else if(bb_completely_merged.find(*vit) != bb_completely_merged.end())
         continue;
      const BBNodeInfoConstRef operations = fbb->CGetBBNodeInfo(*vit);
      std::list<vertex> ordered_operations;
      std::list<vertex>::const_iterator ops_it_end = operations->statements_list.end();
      for(std::list<vertex>::const_iterator ops_it = operations->statements_list.begin(); ops_it_end != ops_it; ++ops_it)
      {
         add_in_sched_order(ordered_operations,*ops_it, sch, dfgRef);
      }

      if(boost::in_degree(*vit, *fbb) == 1)
      {
         /// for basic block connected only to entry bb
         InEdgeIterator ie, iend;
         boost::tie(ie, iend) = boost::in_edges(*vit, *fbb);
         vertex bb_src = boost::source(*ie, *fbb);
         if(bb_src == bb_entry)
         {
            std::list<vertex>::const_iterator stmt_it_end = ordered_operations.end();
            for(std::list<vertex>::const_iterator stmt_it = ordered_operations.begin(); stmt_it_end != stmt_it; ++stmt_it)
               if((GET_TYPE(dfgRef, *stmt_it) & TYPE_PHI))
               {
                  /// add an empty state before the current basic block
                  std::list<vertex> exec_ops, end_ops;
                  const BBNodeInfoConstRef entry_operations = fbb->CGetBBNodeInfo(bb_src);
                  std::list<vertex>::const_iterator entry_ops_it_end = entry_operations->statements_list.end();
                  for(std::list<vertex>::const_iterator entry_ops_it = entry_operations->statements_list.begin(); entry_ops_it_end != entry_ops_it; ++entry_ops_it)
                  {
                     exec_ops.push_back(*entry_ops_it);
                     end_ops.push_back(*entry_ops_it);
                  }
                  std::set<unsigned int> BB_ids;
                  BB_ids.insert(entry_operations->get_bb_index());
                  vertex s_cur = STG_builder->create_state(exec_ops, end_ops, BB_ids);
                  STG_builder->connect_state(last_state[bb_src], s_cur, ST_EDGE_NORMAL_T);
                  last_state[bb_src] = s_cur;
                  break;
               }
         }
      }

      first_state_p = true;
      have_previous = false;
      std::map<unsigned int, std::list<vertex> > executing_ops, ending_ops;
      unsigned int max_cstep = 0;
      unsigned int min_cstep = std::numeric_limits<unsigned int>::max();
      bool has_a_controlling_vertex = false;
      std::set<vertex> bb_cur_completely_merged;
#ifndef NDEBUG
      vertex controlling_vertex;
#endif
      std::list<vertex>::const_iterator stmt_it_end = ordered_operations.end();
      for(std::list<vertex>::const_iterator stmt_it = ordered_operations.begin(); stmt_it_end != stmt_it; ++stmt_it)
      {
         vertex op = *stmt_it;
         if(GET_TYPE(dfgRef, op) & (TYPE_GOTO)) continue;
         if(GET_TYPE(dfgRef, op) & (TYPE_VPHI))
         {
            ///check if virtual phi can be removed and so its basic block
            bool can_be_removed = true;
            OutEdgeIterator oe, oend;
            for(boost::tie(oe, oend) = boost::out_edges(*vit, *fbb); oe != oend && can_be_removed; ++oe)
            {
               vertex tgt = boost::target(*oe, *fbb);
               if(tgt == *vit) continue;
               const BBNodeInfoConstRef out_bb_operations = fbb->CGetBBNodeInfo(tgt);
               std::list<vertex>::const_iterator obo_it_end = out_bb_operations->statements_list.end();
               for(std::list<vertex>::const_iterator obo_it = out_bb_operations->statements_list.begin(); obo_it_end != obo_it && can_be_removed; ++obo_it)
               {
                  if((GET_TYPE(dfgRef, *obo_it) & TYPE_PHI) != 0)
                  {
                     std::vector<HLS_manager::io_binding_type> var_read = HLSMgr->get_required_values(HLS->functionId, *obo_it);
                     unsigned int in_index = 0;
                     const std::vector<HLS_manager::io_binding_type>::const_iterator sr_it_end = var_read.end();
                     for(std::vector<HLS_manager::io_binding_type>::const_iterator sr_it = var_read.begin(); sr_it != sr_it_end && can_be_removed; ++sr_it,++in_index)
                     {
                        unsigned int tree_var = std::get<0>(*sr_it);
                        if(tree_var == 0)
                           continue;
                        const unsigned int tree_node_operation = dfgRef->CGetOpNodeInfo(*obo_it)->node_id;
                        unsigned int bb_index = tree_helper::get_bb_edge(HLSMgr->get_tree_manager(), tree_node_operation, in_index);
                        if(bb_index == operations->get_bb_index())
                           can_be_removed = false;
                     }
                  }
               }
            }
            if(can_be_removed)
               continue;
         }
         if(already_analyzed.find(op) != already_analyzed.end()) continue;
         if(GET_TYPE(dfgRef, op) & (TYPE_IF))
         {
            has_a_controlling_vertex = true;
#ifndef NDEBUG
            controlling_vertex = op;
#endif
         }
         unsigned int cstep = sch->get_cstep(op);
         unsigned int fu_name=HLS->Rfu->get_assign(op);
         unsigned int delay = HLS->ALL->op_et_to_cycles(HLS->ALL->get_execution_time(fu_name, op, dfgRef), clock_period);
         if (delay > 0) delay--;
         unsigned int end_step = cstep + delay;
         //std::cerr << "BB_FSM  op: " << GET_NAME(dfgRef, op) << " - sch=" << cstep << " end=" << end_step << " - " << GET_TYPE(dfgRef, op) << std::endl;
         max_cstep = std::max(max_cstep, end_step);
         min_cstep = std::min(min_cstep, cstep);
         executing_ops[cstep].push_back(op);
         unsigned int initiation_time =HLS->ALL->get_initiation_time(fu_name, op, dfgRef);
         if (initiation_time==0 || initiation_time>delay)
         {
            for(unsigned int c = cstep + 1; c <= end_step; c++)
            {
               executing_ops[c].push_back(op);
            }
         }
         ending_ops[end_step].push_back(op);
      }
      /// now we do a minimal speculation on operations with a null delay, bounded to a unique functional unit
      /// and scheduled in the first control step of the outgoing basic block and it comes from a split of phi nodes (e.g., assignments on induction variable)
      /// to do that we must have a controlling vertex of type TYPE_IF in the current basic block
      /// the outgoing basic blocks must have only one incoming edge to be considered for the speculation
      /// all the TYPE_IF operations of the outgoing basic blocks could not be speculated
      /// in case all the operations of an outgoing basic blocks OBB are speculated holds the following condition last_state[OBB]= last_state[*vit]
      if(has_a_controlling_vertex && 0)
      {
         //std::cerr << "BB_FSM  op: " << GET_NAME(dfgRef, controlling_vertex) << " - " << GET_TYPE(dfgRef, controlling_vertex) << std::endl;
         THROW_ASSERT(max_cstep == sch->get_cstep(controlling_vertex), "mismatch between maximum cstep and controlling vertex cstep");
         OutEdgeIterator oe, oend;
         for(boost::tie(oe, oend) = boost::out_edges(*vit, *fbb); oe != oend; ++oe)
         {
            if(fbb->CGetBBEdgeInfo(*oe)->cfg_edge_F()) continue; /// only the true branch is considered for operation speculation
            vertex bb_tgt = boost::target(*oe, *fbb);
            if(boost::in_degree(bb_tgt, *fbb) == 1)
            {
               const BBNodeInfoConstRef bb_tgt_operations = fbb->CGetBBNodeInfo(bb_tgt);
               std::list<vertex> candidate_ordered_operations;
               std::list<vertex>::const_iterator candidate_ops_it_end = bb_tgt_operations->statements_list.end();
               for(std::list<vertex>::const_iterator candidate_ops_it = bb_tgt_operations->statements_list.begin(); candidate_ops_it_end != candidate_ops_it; ++candidate_ops_it)
               {
                  add_in_sched_order(candidate_ordered_operations,*candidate_ops_it, sch, dfgRef);
               }
               bool first_candidate_operation_found = false;
               unsigned int first_candidate_cstep=std::numeric_limits<unsigned int>::max();
               bool at_least_one_survive = false;
               std::list<vertex>::const_iterator candidate_stmt_it_end = candidate_ordered_operations.end();
               for(std::list<vertex>::const_iterator candidate_stmt_it = candidate_ordered_operations.begin(); candidate_stmt_it_end != candidate_stmt_it; ++candidate_stmt_it)
               {
                  vertex op = *candidate_stmt_it;
                  if(GET_TYPE(dfgRef, op) & (TYPE_GOTO|TYPE_LABEL)) continue;
                  unsigned int cstep = sch->get_cstep(op);
                  unsigned int fu_name=HLS->Rfu->get_assign(op);
                  unsigned int delay = HLS->ALL->op_et_to_cycles(HLS->ALL->get_execution_time(fu_name, op, dfgRef), clock_period);
                  if(!first_candidate_operation_found)
                  {
                     first_candidate_operation_found = true;
                     first_candidate_cstep = cstep;
                  }
                  if(first_candidate_cstep != cstep)
                  {
                     at_least_one_survive = true;
                     break;
                  }
                  else if(delay > 0)
                     at_least_one_survive = true;
                  else if(!HLS->ALL->is_vertex_bounded(fu_name))
                     at_least_one_survive = true;
                  else if(GET_TYPE(dfgRef, op) & TYPE_IF)
                     at_least_one_survive = true;
                  else if(!(GET_TYPE(dfgRef, op) & TYPE_WAS_GIMPLE_PHI))
                     at_least_one_survive = true;
                  else // first_candidate_cstep == cstep and delay == 0 and not a TYPE_IF operation and it is vertex bounded to fu_name
                  {
                     executing_ops[max_cstep].push_back(op);
                     ending_ops[max_cstep].push_back(op);
                     already_analyzed.insert(op);
                     PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level,  std::string("Vertex: ") + GET_NAME(dfgRef, op) + " has been speculated");
                  }
               }
               if(!at_least_one_survive)
               {
                  bb_cur_completely_merged.insert(bb_tgt);
               }
            }
         }

      }
      vertex s_cur;
      for(unsigned int l = min_cstep; l <= max_cstep; l++)
      {
         std::list<vertex> exec_ops, end_ops;
         if (executing_ops.find(l) != executing_ops.end()) exec_ops = executing_ops[l];
         if (ending_ops.find(l) != ending_ops.end()) end_ops = ending_ops[l];
         std::set<unsigned int> BB_ids;
         BB_ids.insert(operations->get_bb_index());
         s_cur = STG_builder->create_state(exec_ops, end_ops, BB_ids);

         for(std::list<vertex>::iterator op = exec_ops.begin(); op != exec_ops.end(); op++)
         {
            technology_nodeRef tn = HLS->ALL->get_fu(HLS->Rfu->get_assign(*op));
            technology_nodeRef op_tn = GetPointer<functional_unit>(tn)->get_operation(tree_helper::normalized_ID(GET_OP(dfgRef, *op)));
            THROW_ASSERT(GetPointer<operation>(op_tn)->time_m, "Time model not available for operation: " + GET_NAME(dfgRef, *op));
            if (!GetPointer<operation>(op_tn)->is_bounded())
            {
               call_operations[s_cur].push_back(*op);
            }
         }
         THROW_ASSERT(call_operations.find(s_cur) == call_operations.end() || call_operations.find(s_cur)->second.size() <= 1, "currently only one unbounded operation per state is admitted");
         if (call_operations.find(s_cur) != call_operations.end() && call_operations.find(s_cur)->second.size())
         {
            THROW_ASSERT(call_operations.find(s_cur) != call_operations.end() && call_operations.find(s_cur)->second.begin() != call_operations.find(s_cur)->second.end(), "unexpected condition");
            vertex call = call_operations.find(s_cur)->second.front();
            std::list<vertex> call_ops;
            call_ops.push_back(call);
            std::set<unsigned int> call_BB_ids;
            call_BB_ids.insert(operations->get_bb_index());
            vertex s_call = STG_builder->create_state(call_ops, call_ops, call_BB_ids);
            HLS->STG->GetStg()->GetStateInfo(s_call)->is_dummy = true;
            call_states[s_cur].push_back(s_call);
         }

         if(have_previous)
         {
            if (call_states.find(previous) == call_states.end())
            {
               STG_builder->connect_state(previous, s_cur, ST_EDGE_NORMAL_T);
            }
            else
            {
               THROW_ASSERT(call_operations.find(previous) != call_operations.end() && call_operations.find(previous)->second.begin() != call_operations.find(previous)->second.end(), "unexpected condition");
               vertex call = call_operations.find(previous)->second.front();
               THROW_ASSERT(call_states.find(previous) != call_states.end(), "unexpected condition");
               for(std::list<vertex>::iterator s = call_states.find(previous)->second.begin(); s != call_states.find(previous)->second.end(); s++)
               {
                  EdgeDescriptor s_e = STG_builder->connect_state(*s, s_cur, ST_EDGE_NORMAL_T);

                  std::set<std::pair<vertex, unsigned int> > OutCondition;
                  OutCondition.insert(std::make_pair(call, T_COND));
                  STG_builder->set_condition(s_e, OutCondition);
               }
               EdgeDescriptor s_e = STG_builder->connect_state(previous, s_cur, ST_EDGE_NORMAL_T);

               std::set<std::pair<vertex, unsigned int> > OutCondition;
               OutCondition.insert(std::make_pair(call, T_COND));
               STG_builder->set_condition(s_e, OutCondition);
            }
         }
         else
            have_previous=true;
         previous = s_cur;
         if(first_state_p)
         {
            first_state[*vit] = s_cur;
            first_state_p = false;
         }
         if (call_states.find(s_cur) != call_states.end())
         {
            THROW_ASSERT(call_operations.find(s_cur) != call_operations.end() && call_operations.find(s_cur)->second.begin() != call_operations.find(s_cur)->second.end(), "unexpected condition");
            vertex call = call_operations.find(s_cur)->second.front();
            THROW_ASSERT(call_states.find(s_cur) != call_states.end() && call_states.find(s_cur)->second.begin() != call_states.find(s_cur)->second.end(), "unexpected condition");
            vertex waiting_state = call_states.find(s_cur)->second.front();
            EdgeDescriptor s_e = STG_builder->connect_state(s_cur, waiting_state, ST_EDGE_NORMAL_T);

            std::set<std::pair<vertex, unsigned int> > OutCondition;
            OutCondition.insert(std::make_pair(call, F_COND));
            STG_builder->set_condition(s_e, OutCondition);

            s_e = STG_builder->connect_state(waiting_state, waiting_state, ST_EDGE_FEEDBACK_T);
            STG_builder->set_condition(s_e, OutCondition);

         }
         last_state[*vit] = s_cur;
      }
      if(!bb_cur_completely_merged.empty())
      {
         std::set<vertex>::const_iterator bb_vert_end = bb_cur_completely_merged.end();
         for(std::set<vertex>::const_iterator bb_vert_it = bb_cur_completely_merged.begin(); bb_vert_it != bb_vert_end; ++bb_vert_it)
         {
            bb_completely_merged.insert(*bb_vert_it);
            last_state[*bb_vert_it] = s_cur;
         }
         bb_cur_completely_merged.clear();
      }
   }

   ///connect two states belonging to different basic blocks
   ///concurrently manage entry and exit state and completely merged basic blocks
   EdgeIterator e, e_end;
   for (boost::tie(e, e_end) = boost::edges(*fbb); e != e_end ; e++)
   {
      vertex bb_src = boost::source(*e, *fbb);
      if(last_state.find(bb_src) == last_state.end()) continue;
      vertex bb_tgt = boost::target(*e, *fbb);
      if(bb_completely_merged.find(bb_tgt) != bb_completely_merged.end())
         continue;
      ///removed the edge from entry to exit
      if(bb_src == bb_entry && bb_tgt == bb_exit) continue;
      vertex s_src, s_tgt;
      if (bb_src == bb_exit)
         s_src = HLS->STG->get_exit_state();
      else
      {
         THROW_ASSERT(last_state.find(bb_src) != last_state.end(), "missing a state vertex");
         s_src = last_state.find(bb_src)->second;
      }
      if(bb_tgt == bb_entry)
         s_tgt = HLS->STG->get_entry_state();
      else if (bb_tgt == bb_exit)
         s_tgt = HLS->STG->get_exit_state();
      else
      {
         while(first_state.find(bb_tgt) == first_state.end())
         {
            THROW_ASSERT(boost::out_degree(bb_tgt, *fbb) == 1, "unexpected pattern");
            OutEdgeIterator oe, oend;
            boost::tie(oe, oend) = boost::out_edges(bb_tgt, *fbb);
            bb_tgt = boost::target(*oe, *fbb);
         }
         s_tgt = first_state.find(bb_tgt)->second;
      }
      //THROW_ASSERT(s_src != s_tgt, "chaining between basic block is not expected");

      EdgeDescriptor s_e;
      if(FB_CFG_SELECTOR & fbb->GetSelector(*e))
      {
         s_e = STG_builder->connect_state(s_src, s_tgt, ST_EDGE_FEEDBACK_T);
         if(call_states.find(s_src) !=  call_states.end())
         {
            if(call_states.find(s_src)->second.begin() != call_states.find(s_src)->second.end())
            {
               std::set<std::pair<vertex, unsigned int> > OutCondition;
               OutCondition.insert(std::make_pair(call_operations[s_src].front(), T_COND));
               STG_builder->set_condition(s_e, OutCondition);
            }
            for(std::list<vertex>::iterator s = call_states.find(s_src)->second.begin(); s != call_states.find(s_src)->second.end(); s++)
            {
               EdgeDescriptor s_e1 = STG_builder->connect_state(*s, s_tgt, ST_EDGE_FEEDBACK_T);
               std::set<std::pair<vertex, unsigned int> > OutCondition;
               OutCondition.insert(std::make_pair(call_operations[s_src].front(), T_COND));
               STG_builder->set_condition(s_e1, OutCondition);
            }
         }
      }
      else
      {
         if (call_states.find(s_src) == call_states.end())
            s_e = STG_builder->connect_state(s_src, s_tgt, ST_EDGE_NORMAL_T);
         else
         {
            THROW_ASSERT(call_operations.find(s_src) != call_operations.end() && call_operations.find(s_src)->second.size() != 0, "State " + HLS->STG->get_state_name(s_src) + " does not contain any call expression");
            vertex operation =  call_operations.find(s_src)->second.front();
            //std::cerr << HLS->STG->get_state_name(s_src) << " - number of operations: " << call_operations[s_src].size() << std::endl;
            //std::cerr << "operation name : " << GET_NAME(dfg, operation) << std::endl;
            for(std::list<vertex>::iterator s = call_states.find(s_src)->second.begin(); s != call_states.find(s_src)->second.end(); s++)
            {
               EdgeDescriptor s_edge = STG_builder->connect_state(*s, s_tgt, ST_EDGE_NORMAL_T);

               std::set<std::pair<vertex, unsigned int> > OutCondition;
               OutCondition.insert(std::make_pair(operation, T_COND));
               STG_builder->set_condition(s_edge, OutCondition);
            }

            s_e = STG_builder->connect_state(s_src, s_tgt, ST_EDGE_NORMAL_T);
            std::set<std::pair<vertex, unsigned int> > OutCondition;
            OutCondition.insert(std::make_pair(operation, T_COND));
            STG_builder->set_condition(s_e, OutCondition);
         }
      }
      std::set<std::pair<vertex, unsigned int> > out_conditions;
      ///compute the controlling vertex
      const BBNodeInfoConstRef bb_node_info = fbb->CGetBBNodeInfo(bb_src);
      THROW_ASSERT(bb_node_info->statements_list.size(), "at least one operation should belong to this basic block");
      vertex last_operation = *(bb_node_info->statements_list.rbegin());
      const std::set<unsigned int> & cfg_edge_ids = fbb->CGetBBEdgeInfo(*e)->get_labels(CFG_SELECTOR);

      if(cfg_edge_ids.size())
      {
         const std::set<unsigned int>::const_iterator ei_end = cfg_edge_ids.end();
         for(std::set<unsigned int>::const_iterator ei = cfg_edge_ids.begin(); ei!= ei_end; ei++)
         {
            out_conditions.insert(std::make_pair(last_operation, *ei));
         }
         if (out_conditions.size())
            STG_builder->set_condition(s_e, out_conditions);
      }
      /// now we manage the conditions in case of merged basic blocks
      if(bb_completely_merged.find(bb_src) != bb_completely_merged.end())
      {
         InEdgeIterator ein, ein_end;
         boost::tie(ein, ein_end) = boost::in_edges(bb_src, *fbb);
         THROW_ASSERT(boost::in_degree(bb_src, *fbb)==1, "unxpected pattern");
         vertex from_bb_vertex = boost::source(*ein, *fbb);

         std::set<std::pair<vertex, unsigned int> > OutCondition;
         ///compute the controlling vertex
         const BBNodeInfoConstRef operations = fbb->CGetBBNodeInfo(from_bb_vertex);
         THROW_ASSERT(!operations->statements_list.empty(), "at least one operation should belong to this basic block");
         vertex controlling_vertex = *(operations->statements_list.rbegin());
         const std::set<unsigned int>& edge_ids = fbb->CGetBBEdgeInfo(*ein)->get_labels(CFG_SELECTOR);
         const std::set<unsigned int>::const_iterator ei_end = edge_ids.end();
         for(std::set<unsigned int>::const_iterator ei=edge_ids.begin(); ei!= ei_end; ei++)
         {
            OutCondition.insert(std::make_pair(controlling_vertex, *ei));
         }
         if (OutCondition.size())
            STG_builder->set_condition(s_e, OutCondition);
      }
   }
   HLS->STG->compute_min_max();
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "STG created!");
}

std::string BB_based_stg::get_kind_text() const
{
   return "BB-based-STG";
}
