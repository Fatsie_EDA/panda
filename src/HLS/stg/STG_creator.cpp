/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file STG_creator.cpp
 * @brief Base class for all STG creation algorithms.
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 * $Locker:  $
 * $State: Exp $
 *
*/
#include "STG_creator.hpp"

///implemented algorithms
#include "BB_based_stg.hpp"

#include "xml_helper.hpp"
#include "polixml.hpp"

#include "Parameter.hpp"
#include "refcount.hpp"
#include "utility.hpp"

STG_creator::STG_creator(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned _funId):
   HLS_step(_Param, _HLSMgr, _funId)
{

}

STG_creator::~STG_creator()
{

}

STG_creatorRef STG_creator::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string algorithm;
   if (CE_XVM(algorithm, node)) LOAD_XVM(algorithm, node);
   else algorithm = "BB_BASED";

   stg_type stg_algorithm;
   if (algorithm == "BB_BASED")
      stg_algorithm = BB_BASED;
   else
      THROW_ERROR("STG algorithm \"" + algorithm + "\" currently not supported");
   return factory(stg_algorithm, Param, HLSMgr, funId);
}

STG_creatorRef STG_creator::factory(stg_type type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   switch (type)
   {
      case BB_BASED:
      {
         return STG_creatorRef(new BB_based_stg(Param, HLSMgr, funId));
      }
      default:
         THROW_UNREACHABLE("STG creation algorithm not supported " + boost::lexical_cast<std::string>(type));
   }
   ///this point should never be reached
   return STG_creatorRef();
}
