/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file HLS_synthesis_flow.hpp
 * @brief This flow is used to generate the synthesis of the input specification
 *
 * This flow generates at least an implementation point for each module, subjected to some design constraints.
 * It can include global or local synthesis steps
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */
#ifndef HLS_SYNTHESIS_FLOW_HPP
#define HLS_SYNTHESIS_FLOW_HPP

#include "refcount.hpp"
CONSTREF_FORWARD_DECL(Parameter);
REF_FORWARD_DECL(HLS_manager);
REF_FORWARD_DECL(HLS_constraints);
REF_FORWARD_DECL(HLS_synthesis_flow);

class HLS_synthesis_flow
{
   public:

      ///implemented synthesis flow
      typedef enum
      {
         CLASSICAL_SYNTHESIS = 0,
         XML_CONFIG
      } synthesis_t;

   protected:

      ///This class contains all the parameters
      const ParameterConstRef Param;

      ///datastructure representing the input application
      const HLS_managerRef AppM;

      ///debugging level for the class
      int debug_level;

      ///verbosity level for the class
      unsigned int output_level;

   public:

      /**
       * Constructor
       */
      HLS_synthesis_flow(const ParameterConstRef Param, const HLS_managerRef AppM);

      /**
       * Destructor
       */
      virtual ~HLS_synthesis_flow();

      /**
       * Generate all the implementations for each of the functions in the behavioral specification
       */
      virtual void performSynthesis() = 0;

      /**
       * Factory method
       */
      static HLS_synthesis_flowRef createSynthesisFlow(synthesis_t type, const ParameterConstRef Param, const HLS_managerRef AppM);

};

#endif
