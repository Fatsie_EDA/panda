/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file allocation.hpp
 * @brief This package is used by all HLS packages to manage resource constraints and characteristics.
 *
 * @defgroup allocation Allocation Package
 * @ingroup HLS
 *
 * Since all the HLS packages use this module to retrieve technology information
 * (see \ref src_HLS_allocation_page for details), it is also used to
 * introduce a simple functional unit allocator (see \ref src_HLS_binding_constraints_page for details). 
 * It returns a list of functional units compatible with the graph under analysis. Each functional unit 
 * is represented by an id of unsigned int type. Given this id, all the HLS packages can retrieve:
 *  - performances (i.e., execution time, initiation time, power consumption, area of the functional unit)
 *  - constraints (number of available functional units)
 * On the other hand, the hls_flow class can control the allocation of the functional units during
 * the instantiation of the allocation object.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef ALLOCATION_HPP
#define ALLOCATION_HPP

///superclass include
#include "hls_step.hpp"
#include "application_manager.hpp"
#include "refcount.hpp"
/**
 * @name forward declarations
 */
//@{
REF_FORWARD_DECL(allocation);
REF_FORWARD_DECL(graph);
REF_FORWARD_DECL(HLS_target);
REF_FORWARD_DECL(HLS_constraints);
CONSTREF_FORWARD_DECL(OpGraph);
REF_FORWARD_DECL(technology_node);
REF_FORWARD_DECL(node_kind_prec_info);
REF_FORWARD_DECL(library_manager);
struct updatecopy_HLS_constraints_functor;
struct operation;
//@}

#include "config_HAVE_EXPERIMENTAL.hpp"
#include "graph.hpp"
#include "utility.hpp"

#include <vector>
#include <map>
#include <string>
#include <cmath>
#include <unordered_map>

#include "hls_manager.hpp"

/**
 * @class allocation
 * @ingroup allocation
 *
 * This wrapper collects all the methods used by the High-level synthesis classes to
 * retrieve information about the functional units.
 */
class allocation : public HLS_step
{
      friend struct updatecopy_HLS_constraints_functor;

      /// For each functional unit (position in the vector), tech_constraints stores the maximum number of resources.
      std::vector<unsigned int> tech_constraints;

      /// Stores the list of the functional units
      std::vector<technology_nodeRef> list_of_FU;

      /// Store the set of functional units (identifiers) uniquely bounded
      std::set<unsigned int> is_vertex_bounded_rel;

      /// Puts into relation vertices (operations) and functional units.
      /// A pair (v, fu) means that the fu is used to implement the operation associated with the vertex v.
      std::unordered_map<vertex, unsigned int> binding;

      /// for each vertex return the set of functional unit that can be used
      std::unordered_map<vertex, std::set<unsigned int> > vertex_to_fus;

      /// map between memory units and the associated variables
      std::map<unsigned int, unsigned int> memory_units;

      /// size of each memory unit in bytes
      std::map<unsigned int, unsigned int> memory_units_sizes;

      std::map<unsigned int, unsigned int> proxy_units;

      /// map between variables and associated memory_units
      std::map<unsigned int, unsigned int> vars_to_memory_units;

      /// map between the functional unit identifier and the pair (library, fu) of names for the unit
      std::unordered_map<unsigned int, std::pair<std::string, std::string> > id_to_fu_names;

      /// store the precomputed pipeline unit: given a functional unit it return the pipeline id compliant
      std::map<std::string, std::string> precomputed_pipeline_unit;

      /// map functional units with their precision
      std::map<unsigned int, unsigned int> precision_map;

      /// define the number of ports associated with the functional unit
      std::map<unsigned int, unsigned int> nports_map;

      /**
       * Returns the technology_node associated with the given operation
       * @param fu_name is the string representing the name of the unit
       */
      static
      technology_nodeRef get_fu(const std::string& fu_name, const HLS_managerRef HLSMgr);

      /**
       * Extract the node kind and precision, if available.
       * @param node is the vertex of the graph.
       * @param g is the graph used to retrieve the required values of the vertex
       * @param info is where the information retrieved for the node get stored.
       * @param constant_id is a tree node id (i.e., different from zero) when one of the operands is constant
       */
      void GET_NODE_TYPE_PREC(const vertex node, const OpGraphConstRef g, node_kind_prec_infoRef info, HLS_manager::io_binding_type &constant_id, bool is_constrained) const;

      /**
       * In case the current functional unit has pipelined operations
       * then it return an id identifying the most compliant functional unit given the current clock period
       */
      std::string get_compliant_pipelined_unit(double clock, const std::string pipe_parameter, const technology_nodeRef current_fu, const std::string curr_op, const std::string library_name, const std::string template_suffix, unsigned int module_prec);

      /**
       * set the number of ports associated with the functional unit
       * @param fu_name is the functional unit id
       * @param n_ports is the number of ports
       */
      void set_number_channels(unsigned int fu_name, unsigned int n_ports);

      technology_nodeRef extract_bambu_provided(const library_managerRef libraryManager, std::string library_name, operation* curr_op, std::string op_name, std::string bambu_provided_resource);

      double mux_time_unit(unsigned int fu_prec) const;
      double get_execution_time_dsp_modified(const unsigned int fu_name, const technology_nodeRef &node_op) const;


   public:

      /**
       * @name Constructors and destructors.
      */
      //@{
      /**
       * Constructor.
       */
      allocation(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor.
       */
      ~allocation();
      //@}

      /**
       * Performs module allocation
       */
      void exec();

      /**
       * Returns the name of the implemented module allocation algorithm
       */
      std::string get_kind_text() const {return "default-module-allocation";}

      /**
        * Factory method
        */
      static
      allocationRef factory(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Factory method from XML file
       */
      static
      allocationRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * @name Support data types.
       */
      //@{
      enum op_perfomed
      {
         min, max
      };

      enum op_target
      {
         initiation_time, power_consumption, execution_time
      };
      //@}

      /**
       * @name Query functions. Used to extract some information from the technology data structure.
       */
      //@{
      /**
       * Checks if the operation associated with a vertex can be implemented by a given functional unit.
       * @param fu_name is the id of the functional unit.
       * @param v is the vertex for which the test is performed.
       * @return true when the functional unit can implement the operation associated with the vertex, false otherwise.
       */
      bool can_implement(const unsigned int fu_id, const vertex v) const;

      /**
       * Returns the set of functional units that can be used to implement the operation associated with vertex v.
       * @param v is the vertex.
       * @param g is the graph of the vertex v.
       */
      const std::set<unsigned int> & can_implement_set(const vertex v) const;

      /**
       * Checks if the functional unit is uniquely bounded to a vertex
       * @param fu_name is the id of the functional unit.
       * @return true when the functional unit is uniquely bounded to a vertex, false otherwise.
       */
      bool is_vertex_bounded(const unsigned int fu_name) const;

      /**
       * Checks if the functional unit has to be shared
       * @param fu_name is the id of the functional unit.
       * @return true when the functional unit has to be shared, false otherwise.
       */
      bool has_to_be_shared(const unsigned int fu_name) const;

      /**
       * In case the vertex is bounded to a particular functional unit, it returns true and the functional unit.
       * @param v is the vertex.
       * @param fu_name is the eventually bounded functional unit.
      */
      bool is_vertex_bounded_with(const vertex v, unsigned int& fu_name) const;

      /**
       * Checks if the functional unit is actually an artificial functional unit like: NOP, ENTRY and EXIT
       * @param fu_name is the id of the functional unit.
       * @return true when the functional unit is artificial, false otherwise.
       */
      bool is_artificial_fu(const unsigned int fu_name) const;

      /**
       * Checks if the functional unit has to be synthetized
       * @param fu_name is the id of the functional unit.
       * @return true when the functional unit is artificial, false otherwise.
       */
      bool has_to_be_synthetized(const unsigned int fu_name) const;

      /**
       * Returns the memory units
       */
      std::map<unsigned int, unsigned int> get_memory_units() const;

      /**
       * return the set of proxy units
       * @return the set of proxy units
       */
      const std::map<unsigned int, unsigned int> &get_proxy_units() const;

      /**
       * Checks if the functional unit implements a READ_COND operation.
       * @param fu_name is the id of the functional unit.
       * @return true when the functional unit can implement a READ_COND operation, false otherwise.
       */
      bool is_read_cond(const unsigned int fu_name) const;

      /**
       * Checks if the functional unit implements an ASSIGN operation.
       * @param fu_name is the id of the functional unit.
       * @return true when the functional unit can implement a ASSIGN operation, false otherwise.
       */
      bool is_assign(const unsigned int fu_name) const;

      /**
       * Checks if the functional unit implements a builtin_wait_call.
       * @param fu_name is the id of the functional unit.
       * @return true when the functional unit can implement a builtin_wait_call, false otherwise.
       */
      bool is_builtin_wait_call(const unsigned int fu_name) const;

      /**
       * Checks if the functional unit implements a RETURN operation.
       * @param fu_name is the id of the functional unit.
       * @return true when the functional unit can implement a RETURN operation, false otherwise.
       */
      bool is_return(const unsigned int fu_name) const;

      /**
       * Returns true if the fu_name is a memory unit
       * @param fu_name is the id of the functional unit
       * @return true when the functional unit is a memory unit
       */
      bool is_memory_unit(const unsigned int fu_name) const;

      /**
       * check if a functional unit is a proxy
       * @param fu_name is the id of the functional unit
       * @return true when fu_name is a proxy unit
       */
      bool is_proxy_unit(const unsigned int fu_name) const;

      /**
       * Returns the reference to a functional unit given its id.
       * @param fu_name is the id of the functional unit.
       * @return the reference to a functional unit
       */
      technology_nodeRef get_fu(unsigned int fu_name) const;

      /**
       * Returns the number of functional unit given its id.
       * @param fu_name is the id of the functional unit.
       * @return the number of functional unit for fu_name
       */
      unsigned int get_number_fu(unsigned int fu_name) const;

      /**
       * Returns the name of the functional for debug purpose. Do not use this string as key id.
       * @param fu_name is the id of the functional unit.
       * @return the string describing the functional unit.
       */
      std::string get_string_name(unsigned int fu_name) const;

      /**
       * Returns the initiation time for a given vertex and a given functional unit.
       * @param fu_name is the id of the functional unit.
       * @param v is the vertex for which the test is performed.
       * @param g is the graph of the vertex v.
       * @return the initiation_time for (fu_name, v, g).
       */
      unsigned int get_initiation_time(const unsigned int fu_name, const vertex v, const OpGraphConstRef g) const;

      /**
       * Returns the execution time for a given vertex and a given functional unit.
       * @param fu_name is the id of the functional unit.
       * @param v is the vertex for which the test is performed.
       * @param g is the graph of the vertex v.
       * @return the execution time for (fu_name, v, g)
       */
      double get_execution_time(const unsigned int fu_name, const vertex v, const OpGraphConstRef g) const;

      /**
       * Returns the worst execution time for all the operations associated with the functional unit.
       * @param fu_name is the id of the functional unit.
       * @return the worst execution time for fu_name
       */
      double get_worst_execution_time(const unsigned int fu_name) const;

      /**
       * Return the stage period for a given vertex and a given pipelined functional unit
       * @param fu_name is the id of the functional unit.
       * @param v is the vertex for which the test is performed.
       * @param g is the graph of the vertex v.
       * @return the stage period for (fu_name, v, g)
       */
      double get_stage_period(const unsigned int fu_name, const vertex v, const OpGraphConstRef g) const;

      /**
       * @brief Return the number of cycles for given vertex and a given functional unit
       * @param fu_name is the id of the functional unit.
       * @param v is the vertex for which the test is performed.
       * @param g is the graph of the vertex v.
       * @return the number of cycles
       */
      unsigned int get_cycles(const unsigned int fu_name, const vertex v, const OpGraphConstRef g) const;

      /**
       * Returns the worst stage period for all the operations associated with the functional unit.
       * @param fu_name is the id of the functional unit.
       * @return the worst stage period for fu_name
       */
      double get_worst_stage_period(const unsigned int fu_name) const;

      /**
       * Returns the area for a given functional unit.
       * @param fu_name is the id of the functional unit.
       * @return the area for the functional unit.
       */
      double get_area(const unsigned int fu_name) const;

      /**
       * return the precision of the given functional unit
       * @param fu_name is the id of the functional unit.
       * @return the precision associated with the functional unit
       */
      unsigned int get_prec(const unsigned int fu_name) const;

      /**
       * estimate the delay of a mux that can be uses to mux the input of the given functional unit
       * @param fu_name is the functional unit id
       * @return the mux delay
       */
      double estimate_mux_time(unsigned int fu_name) const;

      /**
       * estimate the delay of the controller
       * @return the controller delay
       */
      double estimate_controller_delay();

      /**
       * @return the setup/hold time given the current technology
       */
      double get_setup_hold_time() const;

      /**
       * return a time to be subtracted to the execution time/stage period
       * @param fu is the functional unit
       * @return a correction time
       */
      double get_correction_time(unsigned int fu) const;

      /**
       *
       * @return an estimation of the delay of a call_expr
       */
      double estimate_call_delay();

      /**
       * return an estimation of the number of DSPs used by the unit
       * @param fu_name is the id of the functional unit.
       */
      double get_DSPs(const unsigned int fu_name) const;

      /**
       * return the number of channels available for the functional unit
       * @param fu_name is the functional unit id
       * @return the number of channels
       */
      unsigned int get_number_channels(unsigned int fu_name) const;

      /**
       * This method returns the min or the max value of the execution
       * time/power consumption/initiation time for a vertex.
       * @param v is the vertex for which the analysis is performed.
       * @param g is the graph of the vertex v.
       * @param minmax an enum that indicates if the desired value is
       *        the minimum or the maximum among the fu that can handle
       *        the execution of the vertex v.
       * @param target is an enum that indicates "initiation time" if the needed
       *        value is the initiation time, "power consumption" if the needed
       *        value is the power consumption, "execution time" if the
       *        needed value is the execution time.
       * @param fu_name is the id of the functional unit associated with
       *        vertex v and with the searched attribute.
       * @param flag is true when there exist a functional_unit for
       *        vertex v.
       * @param CF is the functor used to retrive information
       * @return the max or min value.
       */
      double get_attribute_of_fu_per_op(const vertex v, const OpGraphConstRef g, op_perfomed op, op_target target, unsigned int &fu_name, bool & flag, const updatecopy_HLS_constraints_functor *CF = nullptr) const;

      /**
       * This method returns the min or the max value of the execution
       * time/power consumption/initiation time for a vertex.
       * @param v is the vertex for which the analysis is performed.
       * @param g is the graph of the vertex v.
       * @param minmax an enum that indicates if the wanted value is
       *        the minimum or the maximum among the fu that can handle
       *        the execution of the vertex v.
       * @param target is an enum that indicates "initiation time" if the needed
       *        value is the initiation time, "power consumption" if the needed
       *        value is the power consumption, "execution time" if the
       *        needed value is the execution time.
       * @return the max or min value.
       */
      double get_attribute_of_fu_per_op(const vertex v, const OpGraphConstRef g, op_perfomed op, op_target target) const;

      /**
       * Returns the number of functional units types.
       * @return the number of functional units types.
       */
      unsigned int get_number_fu_types() const;

      /**
       * Computes the maximum number of resources implementing
       * the operation associated with the vertex v.
       * @param v is the vertex for which the test is performed.
       * @param g is the graph of the vertex v.
       * @return the maximum number of resources implementing the operation associated with the vertex.
       */
      unsigned int max_number_of_resources(const vertex v) const;

      /**
       * Computes the minum number of resources implementing
       * the operation associated with the vertex v.
       * @param v is the vertex for which the test is performed.
       * @param g is the graph of the vertex v.
       * @return the minum number of resources implementing the operation associated with the vertex.
       */
      unsigned int min_number_of_resources(const vertex v) const;

      /**
       * Calculates the control steps required for a specific operation.
       * @param et is the execution time of the operation.
       * @param clock_period is the clock period of the system.
       * @return the number of control steps required.
       */
      inline
      unsigned int op_et_to_cycles(double et , double clock_period) const { return static_cast<unsigned int>(ceil( et / clock_period )); }

      /**
        * Returns the base address of the functional unit
        * @param fu_name is the id of the functional unit
        * @return the base address of the functional unit
        */
      unsigned int get_memory_var(const unsigned int fu_name) const;

      /**
       * Checks library type and sets if it's ordered, complete or simple
       */
      void check_library();
      //@}

      /**
       * @name Print functions.
      */
      //@{
      /**
       * Function that prints the class allocation.
       * @param os is the output stream.
       */
      void print(std::ostream& os) const;

      /**
       * Friend definition of the << operator.
       * @param os is the output stream.
       */
      friend std::ostream& operator<<(std::ostream& os, const allocation& s)
      {
         s.print(os);
         return os ;
      }

      /**
       * Friend definition of the << operator. Pointer version.
       * @param os is the output stream.
       */
      friend std::ostream& operator<<(std::ostream& os, const allocationRef & s)
      {
         if (s)
            s->print(os);
         return os;
      }
      //@}

      /**
       * @name Helper functions.
       */
      //@{
      /**
       * Returns the name of the functional unit, associated with the name of the library, given the corresponding identifier
       */
      std::pair<std::string, std::string> get_fu_name(unsigned int id) const;

      /**
       * return true in case fu type is a memory unit with direct access channels
       * @param fu_type is functional unit id
       * @return true in case the functional unit has direct access channels
       */
      bool is_direct_access_memory_unit(unsigned int fu_type);

      /**
       * return true in case fu type is a resource unit performing an indirect access to memory
       * @param fu_type is functional unit id
       * @return true in case the functional unit access memories through the bus
       */
      bool is_indirect_access_memory_unit(unsigned int fu);


#ifndef NDEBUG
      /**
       * Prints the actual allocation.
       * @param g is the graph.
       * @param debug is the level of debugging used.
       */
      inline
      void print_allocated_resources(const OpGraphConstRef g) const;
      //@}
#endif

      /**
       * Checks if the given operation has a bounded execution time or not
       * @param g is the graph of the vertex
       * @param op is the vertex representing the operation
       * @param fu_type is the identifier of the function unit
       * @return true if the operation op has a bounded execution time when executed by the unit fu_type
       */
      bool is_operation_bounded(const OpGraphConstRef g, const vertex& op, unsigned int fu_type) const;


      /**
       * compute a number representing the "weight" of the functional unit
       * @param fu_s1 is the functional unit id
       * @param u_nterms is the number of inputs
       * @return normalized area of fu_s1
       */
      double compute_normalized_area(unsigned int fu_s1, size_t u_nterms);

      static
      std::string extract_bambu_provided_name(unsigned int prec_in, unsigned int prec_out, const HLS_managerRef HLSMgr, technology_nodeRef &current_fu);
};

/**
 * @struct updatecopy_HLS_constraints_functor
 * constraint functor used by get_attribute_of_fu_per_op
 */
struct updatecopy_HLS_constraints_functor
{

      /**
       * Required functor function used to compute the number of resources associated with the given resource
       * @param name is the name of the resource
       * @return the number of resources of name
       */
      unsigned int operator() (const unsigned int name) const;

      /**
       * Function used to update the copy of the technology constraints
       * @param name is the id of the resource
       * @param the number of delta resources
       */
      void update(const unsigned int name, int delta);

      /**
       * Constructor
       * @param ALL is the reference to allocation class where technology constraints are stored
       */
      updatecopy_HLS_constraints_functor(const allocationRef & ALL) : tech(ALL->tech_constraints) {}

      /**
       * Destructor
       */
      ~updatecopy_HLS_constraints_functor() {}

   private:

      /// copy of the technology constraints
      std::vector<unsigned int> tech;

};

/**
 * @class node_kind_prec_info
 * @ingroup allocation
 *
 * This structure collect the information of input and output
 * precisions of nodes and the node kind.
 */
struct node_kind_prec_info
{
      ///  Node kind.
      std::string node_kind;
      ///  Vector of input precisions.
      std::vector<unsigned int> input_prec;
      /// vector storing the number of elements in case the input is a vector, 0 otherwise
      std::vector<unsigned int> input_nelem;
      ///  Precision of the output.
      unsigned int output_prec;
      /// number of output elemnts in case the output is a a vector, 0 otherwise
      unsigned int output_nelem;

      node_kind_prec_info() : output_prec(0), output_nelem(0) {}
};

#endif
