/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file allocation.cpp
 * @brief Wrapper for technology used by the high-level synthesis flow.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "config_HAVE_FLOPOCO.hpp"

#include "allocation.hpp"

#include "technology_manager.hpp"
#include "library_manager.hpp"
#include "technology_node.hpp"
#include "area_model.hpp"
#include "time_model.hpp"
#include "clb_model.hpp"

#include "technology_builtin.hpp"
#include "memory.hpp"
#include "memory_allocation.hpp"
#include "hls_target.hpp"
#include "hls_constraints.hpp"
#include "hls.hpp"
#include "utility.hpp"
#include "exceptions.hpp"
#include "utility.hpp"
#include "op_graph.hpp"
#include "tree_helper.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"

#include "structural_objects.hpp"
#include "structural_manager.hpp"
#include "hls_manager.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"

#include "memory.hpp"
#include "target_device.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

#include <map>
#include <vector>

#include <iostream>

#include "moduleGenerator.hpp"

#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"

#define MAP_ON_DISTRAM 0


static bool is_a_skip_operation(std::string op_name)
{
   if(
         op_name == "mult_expr" ||
         op_name == "widen_mult_expr" ||
         op_name == "dot_prod_expr"
         )
      return true;
   else
      return false;
}
static inline std::string encode_op_type(const std::string & op_name, const std::string & fu_supported_types)
{
   return op_name + ":" + fu_supported_types;
}

static inline unsigned int resize_to_8_16_32_64_128(unsigned int value)
{
   if(value  <= 8)
      return 8;
   else if (value <= 16)
      return 16;
   else if(value <= 32)
      return 32;
   else if(value <= 64)
      return 64;
   else if(value <= 128)
      return 128;
   else
      THROW_ERROR("not expected size " + boost::lexical_cast<std::string>(value));
   return 0;
}

static inline std::string encode_op_type_prec(const std::string & op_name, const std::string & fu_supported_types, node_kind_prec_infoRef node_info)
{
   std::string op_type = encode_op_type(op_name, fu_supported_types);
   const size_t n_ins = node_info->input_prec.size();
   for (size_t ind=0; ind < n_ins; ++ind)
   {
      if(node_info->input_nelem[ind]==0)
         op_type += ":" + STR(node_info->input_prec[ind]);
      else
         op_type += ":" + STR(node_info->input_prec[ind]) + ":" + STR(node_info->input_nelem[ind]);
   }
   if(node_info->output_nelem==0)
      op_type += ":" + STR(node_info->output_prec);
   else
      op_type += ":" + STR(node_info->output_prec) + ":" + STR(node_info->output_nelem);
   return op_type;
}

allocation::allocation(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned _funId):
   HLS_step(_Param, _HLSMgr, _funId)
{
}

allocation::~allocation()
{

}


technology_nodeRef allocation::extract_bambu_provided(const library_managerRef libraryManager, std::string DEBUG_PARAMETER(library_name), operation* curr_op, std::string op_name, std::string bambu_provided_resource)
{
   THROW_ASSERT(libraryManager->is_fu(bambu_provided_resource), "functional unit not yet synthesized: " + bambu_provided_resource + "(" + library_name + ")");
   technology_nodeRef current_fu = libraryManager->get_fu(bambu_provided_resource);
   THROW_ASSERT(current_fu, "functional unit not yet synthesized: " + bambu_provided_resource + "(" + library_name + ")");
   const std::vector<technology_nodeRef> &op_vec=GetPointer<functional_unit>(current_fu)->get_operations();
   bool has_current_op = false;
   for(std::vector<technology_nodeRef>::const_iterator op_it=op_vec.begin(); op_it!=op_vec.end(); ++op_it)
      if(GetPointer<operation>(*op_it)->get_name() == op_name)
         has_current_op = true;

   if(!has_current_op)
   {
      technology_nodeRef op = technology_nodeRef(new operation());
      operation * cop = GetPointer<operation>(op);
      operation * ref_op = GetPointer<operation>(op_vec[0]);
      cop->operation_name = op_name;
      cop->time_m = ref_op->time_m;
#if HAVE_EXPERIMENTAL
      cop->power_m = ref_op->power_m;
#endif
      cop->commutative = curr_op->commutative;
      cop->bounded = curr_op->bounded;
      cop->supported_types = curr_op->supported_types;
      cop->pipe_parameters = curr_op->pipe_parameters;
      GetPointer<functional_unit>(current_fu)->add(op);
   }
   return current_fu;
}

std::string allocation::extract_bambu_provided_name(unsigned int prec_in, unsigned int prec_out, const HLS_managerRef HLSMgr, technology_nodeRef &current_fu)
{
   std::string unit_name;
   if(prec_in == 32 && prec_out == 64)
      unit_name = SF_FFDATA_CONVERTER_32_64_STD;
   else if(prec_in == 64 && prec_out == 32)
      unit_name = SF_FFDATA_CONVERTER_64_32_STD;
   else
      THROW_ERROR("not supported float to float conversion: " + STR(prec_in) + " " + STR(prec_out));
   current_fu = get_fu(unit_name, HLSMgr);
   return unit_name;
}

void allocation::exec()
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Starting allocation");
   const HLS_targetRef HLS_T = HLSMgr->get_HLS_target();
   const technology_managerRef TM = HLS_T->get_technology_manager();
   const tree_managerRef TreeM = HLSMgr->get_tree_manager();
   const FunctionBehaviorConstRef function_behavior = HLSMgr->CGetFunctionBehavior(funId);
   const HLS_constraintsRef HLS_C = HLS->HLS_C;
   double clock_period = HLS_C->get_clock_period_resource_fraction()*HLS_C->get_clock_period();

   unsigned int base_address = HLSMgr->base_address;
   bool Has_extern_allocated_data = ((HLSMgr->Rmem->get_memory_address()-base_address)>0 && static_cast<memory_allocation::policy_t>(HLS->Param->getOption<unsigned int>(OPT_memory_allocation_policy)) != memory_allocation::EXT_PIPELINED_BRAM)  ||
                                    (HLSMgr->Rmem->has_unknown_addresses() &&
                                     static_cast<memory_allocation::policy_t>(HLS->Param->getOption<unsigned int>(OPT_memory_allocation_policy)) != memory_allocation::ALL_BRAM &&
                                     static_cast<memory_allocation::policy_t>(HLS->Param->getOption<unsigned int>(OPT_memory_allocation_policy)) != memory_allocation::EXT_PIPELINED_BRAM);

   const std::map<unsigned int, memory_symbolRef>& function_vars = HLSMgr->Rmem->get_function_vars(funId);
   for(std::map<unsigned int, memory_symbolRef>::const_iterator l = function_vars.begin(); l != function_vars.end(); ++l)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, " - analyzing variable " + STR(l->first));
      unsigned int var = l->first;
      technology_nodeRef current_fu;
      unsigned int n_ports = 1;
#if MAP_ON_DISTRAM
      if((tree_helper::size(TreeM,  var)) <= 2*HLSMgr->Rmem->get_bram_bitsize())
         current_fu = get_fu(ARRAY_1D_STD_DISTRAM);
      else
#endif
      if(Param->getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_11)
      {
         if(HLSMgr->Rmem->is_private_memory(var) && HLSMgr->Rmem->is_sds_var(var))
            current_fu = get_fu(ARRAY_1D_STD_BRAM_SDS, HLSMgr);
         else
            current_fu = get_fu(ARRAY_1D_STD_BRAM, HLSMgr);
      }
      else if(Param->getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_N1)
      {
         if(HLSMgr->Rmem->is_private_memory(var) && HLSMgr->Rmem->is_sds_var(var))
            current_fu = get_fu(ARRAY_1D_STD_BRAM_N1_SDS, HLSMgr);
         else
            current_fu = get_fu(ARRAY_1D_STD_BRAM_N1, HLSMgr);
         n_ports = Param->getOption<unsigned int>(OPT_channels_number);
      }
      else if(Param->getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_NN)
      {
         if(HLSMgr->Rmem->is_private_memory(var) && HLSMgr->Rmem->is_sds_var(var))
            current_fu = get_fu(ARRAY_1D_STD_BRAM_NN_SDS, HLSMgr);
         else
            current_fu = get_fu(ARRAY_1D_STD_BRAM_NN, HLSMgr);
         n_ports = Param->getOption<unsigned int>(OPT_channels_number);
      }
      else
         THROW_ERROR("type of channel based organization not yet supported");


      unsigned int current_size = get_number_fu_types();
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, " - allocating unit " + current_fu->get_name() + " for variable " + function_behavior->CGetBehavioralHelper()->PrintVariable(l->first) + " in position " + STR(current_size));
      list_of_FU.push_back(current_fu);
      tech_constraints.push_back(n_ports);
      set_number_channels(current_size, n_ports);
      id_to_fu_names[current_size] = std::make_pair(current_fu->get_name(), TM->get_library(current_fu->get_name()));
      THROW_ASSERT(tech_constraints.size() == list_of_FU.size(), "Something of wrong happened");
      vars_to_memory_units[var] = current_size;
      memory_units[current_size] = var;
      memory_units_sizes[current_size] = tree_helper::size(TreeM,  var) / 8;
      precision_map[current_size] = 0;
      /// check clock constraints compatibility
      functional_unit * fu_br= GetPointer<functional_unit>(current_fu);
      technology_nodeRef op_store_node =fu_br->get_operation(STORE);
      operation * op_store = GetPointer<operation>(op_store_node);
      double store_delay = op_store->time_m->get_execution_time()-get_correction_time(current_size)+get_setup_hold_time();
      if(store_delay>clock_period)
         THROW_ERROR("clock constraint too tight: BRAMs for this device cannot run so fast...");

   }
   /// allocate proxies
   if(HLSMgr->Rmem->has_proxied_internal_variables(funId))
   {
      const std::set<unsigned int>& proxied_vars = HLSMgr->Rmem->get_proxied_internal_variables(funId);
      for(std::set<unsigned int>::const_iterator pv_it = proxied_vars.begin(); pv_it != proxied_vars.end(); ++pv_it)
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, " - analyzing proxied variable " + STR(*pv_it));
         unsigned int var = *pv_it;
         technology_nodeRef current_fu;
         unsigned int n_ports = 1;
         if(Param->getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_11)
            current_fu = get_fu(PROXY_CTRL, HLSMgr);
         else if(Param->getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_N1 || Param->getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_NN)
         {
            current_fu = get_fu(PROXY_CTRLN, HLSMgr);
            n_ports = Param->getOption<unsigned int>(OPT_channels_number);
         }
         else
            THROW_ERROR("type of channel based organization not yet supported");
         unsigned int current_size = get_number_fu_types();
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, " - allocating unit " + current_fu->get_name() + " for variable " + function_behavior->CGetBehavioralHelper()->PrintVariable(var) + " in position " + STR(current_size));
         list_of_FU.push_back(current_fu);
         tech_constraints.push_back(n_ports);
         set_number_channels(current_size, n_ports);
         id_to_fu_names[current_size] = std::make_pair(current_fu->get_name(), TM->get_library(current_fu->get_name()));
         THROW_ASSERT(tech_constraints.size() == list_of_FU.size(), "Something of wrong happened");
         vars_to_memory_units[var] = current_size;
         proxy_units[current_size]=var;
         precision_map[current_size] = 0;
      }
   }

   bool skip_flopoco_resources = false;
#if !HAVE_FLOPOCO
   skip_flopoco_resources = true;
#endif
   bool skip_softfloat_resources = true;
   if(Param->isOption(OPT_soft_float) && Param->getOption<bool>(OPT_soft_float))
   {
      skip_flopoco_resources = true;
      skip_softfloat_resources = false;
   }
   std::unordered_map<std::string, unsigned int> tech_vec = HLS_C->tech_constraints;
#if 0
   std::unordered_map<technology_nodeRef, unsigned int> artificial_allocation;
#endif
   const std::unordered_map<std::string, std::pair<std::string, std::pair<std::string, unsigned int> > > &binding_constraints = HLS_C->binding_constraints;
   std::unordered_map<std::string, std::map<unsigned int, unsigned int> > fu_name_to_id;
   std::set<vertex> vertex_to_analyse;
   std::set<vertex> vertex_analysed;
   OpVertexSet support_ops(function_behavior->CGetOpGraph(FunctionBehavior::CFG));
   support_ops.insert(HLS->operations.begin(), HLS->operations.end());
   const OpGraphConstRef g = function_behavior->CGetOpGraph(FunctionBehavior::CFG, support_ops);
   graph::vertex_iterator v, v_end;
   for (boost::tie(v, v_end) = boost::vertices(*g); v != v_end; ++v)
   {
      std::string current_op = tree_helper::normalized_ID(GET_OP(g, *v));
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "- Processing operation: " + current_op + " - " + GET_NAME(g, *v));
      if (LOAD == current_op || STORE == current_op)
      {
         const tree_nodeRef curr_tn = TreeM->get_tree_node_const(g->CGetOpNodeInfo(*v)->node_id);
         gimple_assign* me = GetPointer<gimple_assign>(curr_tn);
         THROW_ASSERT(me, "only gimple_assign's are allowed as memory operations");
         unsigned int var = 0;
         if (current_op == STORE)
            var = tree_helper::get_base_index(TreeM, GET_INDEX_NODE(me->op0));
         else
            var = tree_helper::get_base_index(TreeM, GET_INDEX_NODE(me->op1));
         if (var == 0 || (function_vars.find(var) == function_vars.end() && (!HLSMgr->Rmem->has_proxied_internal_variables(funId) ||
                                                                               HLSMgr->Rmem->get_proxied_internal_variables(funId).find(var) == HLSMgr->Rmem->get_proxied_internal_variables(funId).end())))
         {
            vertex_to_analyse.insert(*v);
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  * Operation " + current_op + " queued for allocation");
            continue;
         }
         THROW_ASSERT(vars_to_memory_units.find(var) != vars_to_memory_units.end(), "Not existing memory unit associated with the variable");
         binding[*v] = vars_to_memory_units[var];
         vertex_to_fus[*v].insert(vars_to_memory_units[var]);
         technology_nodeRef current_fu = list_of_FU[vars_to_memory_units[var]];
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  . Operation " + current_op + " named " + GET_NAME(g, *v) + " mapped onto " + current_fu->get_name() + ", found in library " + TM->get_library(current_op) + " in position " + STR(vars_to_memory_units[var]));
      }
      // Artificial FUs
      else if (ASSIGN == current_op || ASSERT_EXPR == current_op || ADDR_EXPR == current_op ||
               READ_COND == current_op || MULTI_READ_COND == current_op || NOP_EXPR == current_op || CONVERT_EXPR == current_op ||
               SWITCH_COND == current_op || GIMPLE_LABEL == current_op || GIMPLE_GOTO == current_op ||
               GIMPLE_PRAGMA == current_op ||
               ENTRY == current_op || EXIT == current_op || NOP == current_op ||
               GIMPLE_RETURN == current_op || GIMPLE_PHI == current_op || GIMPLE_NOP == current_op || GIMPLE_ASM == current_op ||
               VIEW_CONVERT_EXPR == current_op || BUILTIN_WAIT_CALL_STD == current_op
               )
      {
         unsigned int current_size = get_number_fu_types();
         technology_nodeRef current_fu;
         if (current_op == ASSIGN)
         {
            unsigned int modify_tree_index = g->CGetOpNodeInfo(*v)->node_id;
            tree_nodeRef modify_node = TreeM->get_tree_node_const(modify_tree_index);
            gimple_assign *gms = GetPointer<gimple_assign>(modify_node);
            unsigned int left_type_index;
            tree_nodeRef left_type_node = tree_helper::get_type_node(GET_NODE(gms->op0), left_type_index);
            if(tree_helper::is_int(TreeM, left_type_index))
               current_fu = get_fu(ASSIGN_SIGNED_STD, HLSMgr);
            else if(tree_helper::is_real(TreeM, left_type_index))
               current_fu = get_fu(ASSIGN_REAL_STD, HLSMgr);
            else
               current_fu = get_fu(ASSIGN_UNSIGNED_STD, HLSMgr);
         }
         else if (current_op == ASSERT_EXPR)
         {
            unsigned int modify_tree_index = g->CGetOpNodeInfo(*v)->node_id;
            tree_nodeRef modify_node = TreeM->get_tree_node_const(modify_tree_index);
            gimple_assign *gms = GetPointer<gimple_assign>(modify_node);
            unsigned int left_type_index;
            tree_nodeRef left_type_node = tree_helper::get_type_node(GET_NODE(gms->op0), left_type_index);
            if(tree_helper::is_int(TreeM, left_type_index))
               current_fu = get_fu(ASSERT_EXPR_SIGNED_STD, HLSMgr);
            else if(tree_helper::is_real(TreeM, left_type_index))
               current_fu = get_fu(ASSERT_EXPR_REAL_STD, HLSMgr);
            else
               current_fu = get_fu(ASSERT_EXPR_UNSIGNED_STD, HLSMgr);
         }
         else if (current_op == ADDR_EXPR)
            current_fu = get_fu(ADDR_EXPR_STD, HLSMgr);
         else if (current_op == NOP_EXPR)
         {
            unsigned int modify_tree_index = g->CGetOpNodeInfo(*v)->node_id;

            //std::cout << NOP_EXPR << "->" << modify_tree_index << std::endl;
            tree_nodeRef modify_node = TreeM->get_tree_node_const(modify_tree_index);
            gimple_assign *gms = GetPointer<gimple_assign>(modify_node);

            unsigned int left_type_index;
            tree_nodeRef left_type_node = tree_helper::get_type_node(GET_NODE(gms->op0), left_type_index);
            nop_expr * ne = GetPointer<nop_expr>(GET_NODE(gms->op1));
            unsigned int right_type_index;
            tree_nodeRef right_type_node = tree_helper::get_type_node(GET_NODE(ne->op), right_type_index);

            bool unsignedR = tree_helper::is_unsigned(TreeM, right_type_index);
            bool unsignedL = tree_helper::is_unsigned(TreeM, left_type_index);
            bool intR = tree_helper::is_int(TreeM, right_type_index);
            bool intL = tree_helper::is_int(TreeM, left_type_index);
            bool enumR = tree_helper::is_an_enum(TreeM, right_type_index);
            bool enumL = tree_helper::is_an_enum(TreeM, left_type_index);
            bool boolR = tree_helper::is_bool(TreeM, right_type_index);
            bool boolL = tree_helper::is_bool(TreeM, left_type_index);
            bool is_a_pointerR = tree_helper::is_a_pointer(TreeM, right_type_index);
            bool is_a_pointerL = tree_helper::is_a_pointer(TreeM, left_type_index);
            bool is_realR = tree_helper::is_real(TreeM, right_type_index);
            bool is_realL = tree_helper::is_real(TreeM, left_type_index);

            if((unsignedR || is_a_pointerR || boolR) && (unsignedL || is_a_pointerL || boolL))
               current_fu = get_fu(UUDATA_CONVERTER_STD, HLSMgr);
            else if((intR || enumR) && (unsignedL || is_a_pointerL || boolL))
               current_fu = get_fu(IUDATA_CONVERTER_STD, HLSMgr);
            else if((unsignedR || is_a_pointerR || boolR) && (intL|| enumL))
               current_fu = get_fu(UIDATA_CONVERTER_STD, HLSMgr);
            else if((intR || enumR) && (intL|| enumL))
               current_fu = get_fu(IIDATA_CONVERTER_STD, HLSMgr);
            else if(is_realR && is_realL)
            {
               if(!skip_flopoco_resources)
                  current_fu = get_fu(FFDATA_CONVERTER_STD, HLSMgr);
               else if(!skip_softfloat_resources)
               {
                  unsigned int prec_in = tree_helper::size(TreeM, right_type_index);
                  unsigned int prec_out = tree_helper::size(TreeM, left_type_index);
                  extract_bambu_provided_name(prec_in, prec_out, HLSMgr, current_fu);
               }
               else
                  THROW_ERROR("missing resource for floating point to floating point conversion");
            }
            else
               THROW_ERROR(std::string("Nop_Expr pattern not supported ") + boost::lexical_cast<std::string>(modify_tree_index));
         }
         else if (current_op == CONVERT_EXPR)
         {
            unsigned int modify_tree_index = g->CGetOpNodeInfo(*v)->node_id;

            //std::cout << CONVERT_EXPR << "->" << modify_tree_index << std::endl;
            tree_nodeRef modify_node = TreeM->get_tree_node_const(modify_tree_index);
            gimple_assign *gms = GetPointer<gimple_assign>(modify_node);
            unsigned int left_type_index;
            tree_nodeRef left_type_node = tree_helper::get_type_node(GET_NODE(gms->op0), left_type_index);
            convert_expr * ce = GetPointer<convert_expr>(GET_NODE(gms->op1));
            unsigned int right_type_index;
            tree_nodeRef right_type_node = tree_helper::get_type_node(GET_NODE(ce->op), right_type_index);

            bool unsignedR = tree_helper::is_unsigned(TreeM, right_type_index);
            bool unsignedL = tree_helper::is_unsigned(TreeM, left_type_index);
            bool intR = tree_helper::is_int(TreeM, right_type_index);
            bool intL = tree_helper::is_int(TreeM, left_type_index);
            bool boolR = tree_helper::is_bool(TreeM, right_type_index);
            bool boolL = tree_helper::is_bool(TreeM, left_type_index);
            bool is_a_pointerR = tree_helper::is_a_pointer(TreeM, right_type_index);
            bool is_a_pointerL = tree_helper::is_a_pointer(TreeM, left_type_index);

            if((unsignedR || is_a_pointerR || boolR) && (unsignedL || is_a_pointerL || boolL))
               current_fu = get_fu(UUCONVERTER_EXPR_STD, HLSMgr);
            else if(intR && (unsignedL || is_a_pointerL || boolL))
               current_fu = get_fu(IUCONVERTER_EXPR_STD, HLSMgr);
            else if((unsignedR || is_a_pointerR || boolR) && intL)
               current_fu = get_fu(UICONVERTER_EXPR_STD, HLSMgr);
            else if(intR && intL)
               current_fu = get_fu(IICONVERTER_EXPR_STD, HLSMgr);
            else
               THROW_ERROR(std::string("CONVERT_EXPR pattern not supported ") + boost::lexical_cast<std::string>(modify_tree_index) + ":" + STR(left_type_index) + ":" + STR(right_type_index));

         }
         else if (current_op == BUILTIN_WAIT_CALL_STD)
            current_fu = get_fu(BUILTIN_WAIT_CALL_STD, HLSMgr);
         else if (current_op == READ_COND)
            current_fu = get_fu(READ_COND_STD, HLSMgr);
         else if (current_op == MULTI_READ_COND)
            current_fu = get_fu(MULTI_READ_COND_STD, HLSMgr);
         else if (current_op == SWITCH_COND)
            current_fu = get_fu(SWITCH_COND_STD, HLSMgr);
         else if (current_op == GIMPLE_LABEL)
            current_fu = get_fu(GIMPLE_LABEL_STD, HLSMgr);
         else if (current_op == GIMPLE_GOTO)
            current_fu = get_fu(GIMPLE_GOTO_STD, HLSMgr);
         else if(current_op == GIMPLE_PRAGMA)
            current_fu = get_fu(GIMPLE_PRAGMA_STD, HLSMgr);
         else if (current_op == ENTRY)
            current_fu = get_fu(ENTRY_STD, HLSMgr);
         else if (current_op == EXIT)
            current_fu = get_fu(EXIT_STD, HLSMgr);
         else if (current_op == NOP)
            current_fu = get_fu(NOP_STD, HLSMgr);
         else if (current_op == GIMPLE_RETURN)
            current_fu = get_fu(GIMPLE_RETURN_STD, HLSMgr);
         else if (current_op == GIMPLE_PHI)
            current_fu = get_fu(GIMPLE_PHI_STD, HLSMgr);
         else if (current_op == GIMPLE_ASM)
            current_fu = get_fu(GIMPLE_ASM_STD, HLSMgr);
         else if (current_op == GIMPLE_NOP)
            current_fu = get_fu(GIMPLE_NOP_STD, HLSMgr);
         else if (current_op == VIEW_CONVERT_EXPR)
            current_fu = get_fu(VIEW_CONVERT_STD, HLSMgr);
         else
            THROW_ERROR("Unexpected operation");
         // FU must exist
         THROW_ASSERT(current_fu, std::string("Not found ") + current_op + " in library " + TM->get_library(current_op));
         unsigned int current_id;
#if 0
         if(artificial_allocation.find(current_fu) == artificial_allocation.end())
         {
            list_of_FU.push_back(current_fu);
            tech_constraints.push_back(INFINITE_UINT);
            current_id = current_size;
            artificial_allocation[current_fu] = current_id;
         }
         else
            current_id = artificial_allocation.find(current_fu)->second;
#else
         list_of_FU.push_back(current_fu);
         tech_constraints.push_back(1);
         current_id = current_size;
#endif
         THROW_ASSERT(tech_constraints.size() == list_of_FU.size(), "Something of wrong happen");
#if 1
         is_vertex_bounded_rel.insert(current_id);
         binding[*v] = current_id;
#endif
         vertex_to_fus[*v].insert(current_id);
         id_to_fu_names[current_id] = std::make_pair(current_fu->get_name(), TM->get_library(current_op));
         unsigned int out_var = HLSMgr->get_produced_value(HLS->functionId, *v);
         if(out_var)
         {
            unsigned int type_index = tree_helper::get_type_index(TreeM, out_var);
            if(tree_helper::is_a_vector(TreeM, type_index))
            {
               const unsigned int element_type = tree_helper::GetElements(TreeM, type_index);
               const unsigned int element_size = static_cast<unsigned int>(tree_helper::size(TreeM, element_type));
               precision_map[current_size] = element_size;
            }
            else
               precision_map[current_size] = tree_helper::size(TreeM, tree_helper::get_type_index(TreeM, out_var));
         }
         else
            precision_map[current_size] = 0;
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  . Operation " + current_op + " mapped onto " + current_fu->get_name() + ", found in library " + TM->get_library(current_op));
      }
      // Constrained FUs
      else if(binding_constraints.find(GET_NAME(g, *v)) != binding_constraints.end())
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  . Current node is under constraints");
         const std::pair<std::string, std::pair<std::string, unsigned int> >& defined_binding = binding_constraints.find(GET_NAME(g, *v))->second;
         std::string fu_name = defined_binding.first;
         std::string fu_library = defined_binding.second.first;
         unsigned int fu_index = defined_binding.second.second;
         std::string key = ENCODE_FU_LIB(fu_name, fu_library);
         if(fu_name_to_id.find(key) != fu_name_to_id.end())
         {
            if(fu_name_to_id[key].find(fu_index) != fu_name_to_id[key].end())
               binding[*v] = fu_name_to_id[key][fu_index];
            else
            {
               unsigned int current_size = get_number_fu_types();
               fu_name_to_id[key][fu_index] = current_size;
               technology_nodeRef current_fu = TM->get_fu(fu_name, fu_library);
               THROW_ASSERT(current_fu, std::string("Not found") + fu_name + " in library " + fu_library);
               list_of_FU.push_back(current_fu);
               tech_constraints.push_back(1);
               binding[*v] = current_size;
               vertex_to_fus[*v].insert(current_size);
               id_to_fu_names[current_size] = std::make_pair(fu_name, fu_library);
               if(tech_vec.find(key) != tech_vec.end())
                  tech_vec[key]--;
               unsigned int out_var = HLSMgr->get_produced_value(HLS->functionId, *v);
               if(out_var)
               {
                  unsigned int type_index = tree_helper::get_type_index(TreeM, out_var);
                  if(tree_helper::is_a_vector(TreeM, type_index))
                  {
                     const unsigned int element_type = tree_helper::GetElements(TreeM, type_index);
                     const unsigned int element_size = static_cast<unsigned int>(tree_helper::size(TreeM, element_type));
                     precision_map[current_size] = element_size;
                  }
                  else
                     precision_map[current_size] = tree_helper::size(TreeM, tree_helper::get_type_index(TreeM, out_var));
               }
               else
                  precision_map[current_size] = 0;
            }
         }
         else
         {
            unsigned int current_size = get_number_fu_types();
            fu_name_to_id[key][fu_index] = current_size;
            technology_nodeRef current_fu = TM->get_fu(fu_name, fu_library);
            THROW_ASSERT(current_fu, std::string("Not found") + fu_name + " in library " + fu_library);
            list_of_FU.push_back(current_fu);
            tech_constraints.push_back(1);
            binding[*v] = current_size;
            vertex_to_fus[*v].insert(current_size);
            id_to_fu_names[current_size] = std::make_pair(fu_name, fu_library);
            if(tech_vec.find(key) != tech_vec.end())
               tech_vec[key]--;
            unsigned int out_var = HLSMgr->get_produced_value(HLS->functionId, *v);
            if(out_var)
            {
               unsigned int type_index = tree_helper::get_type_index(TreeM, out_var);
               if(tree_helper::is_a_vector(TreeM, type_index))
               {
                  const unsigned int element_type = tree_helper::GetElements(TreeM, type_index);
                  const unsigned int element_size = static_cast<unsigned int>(tree_helper::size(TreeM, element_type));
                  precision_map[current_size] = element_size;
               }
               else
                  precision_map[current_size] = tree_helper::size(TreeM, tree_helper::get_type_index(TreeM, out_var));
            }
            else
               precision_map[current_size] = 0;
         }
      }
      else
      {
         vertex_to_analyse.insert(*v);
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  * Operation " + current_op + " queued for allocation");
      }
   }

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Starting allocation of operations in queued vertices");
   const std::vector<std::string>& libraries = TM->get_library_list();
   std::map<std::string,technology_nodeRef> new_fu;


#if 0
   std::unordered_map<std::string, unsigned int>::const_iterator tv_end = tech_vec.end();
   for(std::unordered_map<std::string, unsigned int>::const_iterator tv = tech_vec.begin(); tv != tv_end; ++tv)
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Resource constraint on " + tv->first + ": " + STR(tv->second));
#endif

   std::string bambu_provided_resource;
   std::map<technology_nodeRef,std::map<unsigned int, std::map<HLS_manager::io_binding_type, unsigned int> > >fu_list;

   for(unsigned int l = 0; l < libraries.size(); l++)
   {
      const library_managerRef library = TM->get_library_manager(libraries[l]);
      const library_manager::fu_map_type& list_fu = library->get_library_fu();
      //library_manager::fu_map_type::const_iterator fu_end = list_fu.end();
      for (library_manager::fu_map_type::const_iterator fu = list_fu.begin(); fu !=  list_fu.end(); fu++)
      {
         //std::cerr << "fu->first" << fu->first << std::endl;

         ///LOAD/STORE operations allocated to such units have been already allocated
         if (fu->first == ARRAY_1D_STD_BRAM || fu->first == ARRAY_1D_STD_BRAM_N1 || fu->first == ARRAY_1D_STD_BRAM_NN ||
             fu->first == ARRAY_1D_STD_BRAM_SDS || fu->first == ARRAY_1D_STD_BRAM_N1_SDS || fu->first == ARRAY_1D_STD_BRAM_NN_SDS ||
             fu->first == ARRAY_1D_STD_DISTRAM) continue;
         ///LOAD/STORE operations on proxys have been already managed
         if(fu->first == PROXY_CTRL || fu->first == PROXY_CTRLN) continue;

         if(Param->getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_11 || Param->getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_N1)
         {
            if(fu->first == MEMORY_STDN || fu->first == MEMORY_STDN_D10 || fu->first == MEMORY_STDN_D11 || fu->first == MEMORY_STDN_D21 ||
                  fu->first == BMEMORY_STDN || fu->first == BMEMORY_STDN_D10 || fu->first == BMEMORY_STDN_D11 || fu->first == BMEMORY_STDN_D21 ||
                  fu->first == STD_PRINTFN)
               continue;
         }
         else
         {
            if(fu->first == MEMORY_STD || fu->first == MEMORY_STD_D10 || fu->first == MEMORY_STD_D11 || fu->first == MEMORY_STD_D21 ||
                  fu->first == BMEMORY_STD || fu->first == BMEMORY_STD_D10 || fu->first == BMEMORY_STD_D11 || fu->first == BMEMORY_STD_D21 ||
                  fu->first == STD_PRINTF)
               continue;
         }

         if(Has_extern_allocated_data && fu->first == BMEMORY_STD) continue;
         if(Has_extern_allocated_data && fu->first == BMEMORY_STD_D10) continue;
         if(Has_extern_allocated_data && fu->first == BMEMORY_STD_D11) continue;
         if(Has_extern_allocated_data && fu->first == BMEMORY_STD_D21) continue;
         if(Has_extern_allocated_data && fu->first == BMEMORY_STDN) continue;
         if(Has_extern_allocated_data && fu->first == BMEMORY_STDN_D10) continue;
         if(Has_extern_allocated_data && fu->first == BMEMORY_STDN_D11) continue;
         if(Has_extern_allocated_data && fu->first == BMEMORY_STDN_D21) continue;
         if(!Has_extern_allocated_data && fu->first == MEMORY_STD) continue;
         if(!Has_extern_allocated_data && fu->first == MEMORY_STD_D10) continue;
         if(!Has_extern_allocated_data && fu->first == MEMORY_STD_D11) continue;
         if(!Has_extern_allocated_data && fu->first == MEMORY_STD_D21) continue;
         if(!Has_extern_allocated_data && fu->first == MEMORY_STDN) continue;
         if(!Has_extern_allocated_data && fu->first == MEMORY_STDN_D10) continue;
         if(!Has_extern_allocated_data && fu->first == MEMORY_STDN_D11) continue;
         if(!Has_extern_allocated_data && fu->first == MEMORY_STDN_D21) continue;

         if(fu->first.find(BMEMORY_STD) == 0 && fu->first.find(BMEMORY_STDN) == std::string::npos)
         {
            std::string postfix = Param->getOption<std::string>(OPT_memory_controller_type);
            if(fu->first != (BMEMORY_STD + postfix)) continue;
         }
         if(fu->first.find(BMEMORY_STDN) == 0)
         {
            std::string postfix = Param->getOption<std::string>(OPT_memory_controller_type);
            if(fu->first != (BMEMORY_STDN + postfix)) continue;
         }
         if(fu->first.find(MEMORY_STD) == 0 && fu->first.find(MEMORY_STDN) == std::string::npos)
         {
            std::string postfix = Param->getOption<std::string>(OPT_memory_controller_type);
            if(fu->first != (MEMORY_STD + postfix)) continue;
         }
         if(fu->first.find(MEMORY_STDN) == 0)
         {
            std::string postfix = Param->getOption<std::string>(OPT_memory_controller_type);
            if(fu->first != (MEMORY_STDN + postfix)) continue;
         }

         technology_nodeRef current_fu = fu->second;

         if (GetPointer<functional_unit_template>(current_fu)) continue;

         std::unordered_map<std::string, unsigned int>::const_iterator tech_constrain_it;
         if (GetPointer<functional_unit>(current_fu)->fu_template_name != "")
            tech_constrain_it = tech_vec.find(ENCODE_FU_LIB(GetPointer<functional_unit>(current_fu)->fu_template_name, libraries[l]));
         else
            tech_constrain_it = tech_vec.find(ENCODE_FU_LIB(current_fu->get_name(), libraries[l]));

         if (tech_constrain_it != tech_vec.end() && tech_constrain_it->second == 0) continue; // forced to use 0 FUs of current ones

         bambu_provided_resource = "";
         structural_managerRef structManager_obj=GetPointer<functional_unit>(current_fu)->CM;
         if(structManager_obj ) ///generated cannot be directly considered for allocation
         {
            structural_objectRef modobj=structManager_obj->get_circ();
            module * mod = GetPointer<module>(modobj);

            if(mod->get_generated())
               continue;

            const NP_functionalityRef &np = mod->get_NP_functionality();
            if(skip_flopoco_resources)
            {
               if(np && np->get_NP_functionality(NP_functionality::FLOPOCO_PROVIDED) != "")
                  continue;
            }
            if(skip_softfloat_resources)
            {
               if(np && np->get_NP_functionality(NP_functionality::BAMBU_PROVIDED) != "")
                  continue;
            }
            else if(np)
                  bambu_provided_resource = np->get_NP_functionality(NP_functionality::BAMBU_PROVIDED);
         }
         else if (GetPointer<functional_unit>(current_fu)->fu_template_name != "")
         {
            std::string tfname = GetPointer<functional_unit>(current_fu)->fu_template_name;
            technology_nodeRef tfu = get_fu(tfname, HLSMgr);
            if(!tfu || !GetPointer<functional_unit_template>(tfu) || !GetPointer<functional_unit_template>(tfu)->FU || !GetPointer<functional_unit>(GetPointer<functional_unit_template>(tfu)->FU)->CM) continue;
            structural_managerRef tcm = GetPointer<functional_unit>(GetPointer<functional_unit_template>(tfu)->FU)->CM;
            structural_objectRef tmodobj=tcm->get_circ();
            module * tmod = GetPointer<module>(tmodobj);
            const NP_functionalityRef &tnp = tmod->get_NP_functionality();
            if(tnp && skip_flopoco_resources && tnp->get_NP_functionality(NP_functionality::FLOPOCO_PROVIDED) != "")
               continue;
            if(tnp && skip_softfloat_resources && tnp->get_NP_functionality(NP_functionality::BAMBU_PROVIDED) != "")
               continue;
            else if(tnp)
               bambu_provided_resource = tnp->get_NP_functionality(NP_functionality::BAMBU_PROVIDED);
         }

         PRINT_DBG_MEX(DEBUG_LEVEL_INFINITE, debug_level, "Considering functional unit: " + current_fu->get_name());
         const functional_unit::operation_vec & Ops = GetPointer<functional_unit>(current_fu)->get_operations();
         functional_unit::operation_vec::const_iterator ops_end = Ops.end();
         bool found;
         unsigned int current_fu_id = get_number_fu_types();

         unsigned int current_id=current_fu_id;

         bool predicate_1 = library->get_library_name() != WORK_LIBRARY;
         bool predicate_2 = library->get_library_name() == WORK_LIBRARY;

         found = false;

         for (functional_unit::operation_vec::const_iterator ops = Ops.begin(); ops != ops_end; ops++)
         {
            operation* curr_op = GetPointer<operation>(*ops);
            PRINT_DBG_MEX(DEBUG_LEVEL_INFINITE, debug_level, "Considering operation: " + (*ops)->get_name());
            bool varargs_fu;
            for (std::set<vertex>::const_iterator vert = vertex_to_analyse.begin(); vert != vertex_to_analyse.end(); vert++)
            {
               if (tree_helper::normalized_ID(GET_OP(g, *vert)) != curr_op->get_name())
                  continue;
               else if((tree_helper::normalized_ID(GET_OP(g, *vert)) == LOAD || tree_helper::normalized_ID(GET_OP(g, *vert)) == STORE) &&
                       fu->first != MEMORY_STD &&  fu->first != MEMORY_STD_D10 && fu->first != MEMORY_STD_D11 && fu->first != MEMORY_STD_D21 &&
                       fu->first != MEMORY_STDN && fu->first != MEMORY_STDN_D10 && fu->first != MEMORY_STDN_D11 && fu->first != MEMORY_STDN_D21 &&
                       fu->first != BMEMORY_STD && fu->first != BMEMORY_STD_D10 && fu->first != BMEMORY_STD_D11 && fu->first != BMEMORY_STD_D21 &&
                       fu->first != BMEMORY_STDN && fu->first != BMEMORY_STDN_D10 && fu->first != BMEMORY_STDN_D11 && fu->first != BMEMORY_STDN_D21)
                  continue;
               else if(predicate_1 && TM->get_fu(tree_helper::normalized_ID(GET_OP(g, *vert)), WORK_LIBRARY) && GET_TYPE(g, *vert) != TYPE_MEMCPY) continue;
               //else if(predicate_2 && GET_TYPE(g, *vert) == TYPE_MEMCPY) continue;

               varargs_fu = false;

               std::string specialized_fuName="";

               std::string current_op;
               node_kind_prec_infoRef node_info(new node_kind_prec_info());
               HLS_manager::io_binding_type constant_id;
               PRINT_DBG_MEX(DEBUG_LEVEL_INFINITE, debug_level, "Considering vertex " + GET_NAME(g, *vert));
               bool isMemory = false;
               if (fu->first == MEMORY_STD || fu->first == MEMORY_STD_D10 || fu->first == MEMORY_STD_D11 || fu->first == MEMORY_STD_D21 ||
                   fu->first == MEMORY_STDN || fu->first == MEMORY_STDN_D10 || fu->first == MEMORY_STDN_D11 || fu->first == MEMORY_STDN_D21 ||
                   fu->first == BMEMORY_STD || fu->first == BMEMORY_STD_D10 || fu->first == BMEMORY_STD_D11 || fu->first == BMEMORY_STD_D21 ||
                   fu->first == BMEMORY_STDN || fu->first == BMEMORY_STDN_D10 || fu->first == BMEMORY_STDN_D11 || fu->first == BMEMORY_STDN_D21) isMemory = true;

               if (!isMemory)
                  GET_NODE_TYPE_PREC(*vert, g, node_info, constant_id, tech_constrain_it != tech_vec.end());
               else
                  node_info->node_kind = "VECTOR_BOOL";

               // Check for correct type
               if (node_info->node_kind != "" && !curr_op->is_type_supported(node_info->node_kind))
                  continue; // FU does not support the operation type
               // Check for correct precision
               if (!curr_op->is_type_supported(node_info->node_kind, node_info->input_prec, node_info->input_nelem))
                  continue; // FU does support the operation type, but does not support correct precision
               // in case the operation is pipelined check the clock period
               THROW_ASSERT(curr_op->time_m, "expected a time model for " + current_fu->get_name() + " for operation " + curr_op->get_name());

               if(curr_op->time_m->get_cycles() >= 1 && curr_op->time_m->get_stage_period()>clock_period)
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Warning: Functional unit " + current_fu->get_name() + " not compliant with the given clock period " + boost::lexical_cast<std::string>(clock_period));
                  //continue;
               }
               else if(curr_op->time_m->get_cycles() >= 1)
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Functional unit " + current_fu->get_name() + " compliant with the given clock period " + boost::lexical_cast<std::string>(clock_period) + " stage period " + boost::lexical_cast<std::string>(curr_op->time_m->get_stage_period()));

               if(structManager_obj && GetPointer<module>(structManager_obj->get_circ())->is_var_args())
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level,"Found a var args function");
                  varargs_fu = true;
                  moduleGeneratorRef modGen = moduleGeneratorRef(new moduleGenerator(Param, HLSMgr));
                  std::vector<HLS_manager::io_binding_type> required_variables = HLSMgr->get_required_values(funId, *vert);
                  current_op=current_fu->get_name()+modGen->get_specialized_name(required_variables, function_behavior);
                  specialized_fuName=current_op;

                  std::string fu_name=current_fu->get_name();
                  std::string check_lib = TM->get_library(specialized_fuName);
                  if(check_lib == libraries[l])
                  {
                     new_fu[specialized_fuName]=get_fu(specialized_fuName, HLSMgr);
                  }
                  else if(new_fu.find(specialized_fuName)==new_fu.end())
                  {
                     modGen->specialize_fu(fu_name, *vert,libraries[l], TM, function_behavior, specialized_fuName, new_fu, HLS->HLS_T->get_target_device()->get_type());
                  }
               }
               else if (node_info->node_kind != "" && !isMemory)
                  current_op = encode_op_type_prec((*ops)->get_name(), curr_op->get_type_supported_string(), node_info);
               else if (node_info->node_kind != "")
                  current_op = encode_op_type((*ops)->get_name(), curr_op->get_type_supported_string());
               else
                  current_op = (*ops)->get_name();

               if (GetPointer<functional_unit>(current_fu)->fu_template_name != "")
               {
                  // If the current_fu is a specialization of a template compute required parameters for the specialization.
                  std::string required_prec = "";
                  std::string template_suffix = "";
                  const size_t n_ins = node_info->input_prec.size();
                  for (size_t ind=0; ind < n_ins; ++ind)
                  {
                     if(node_info->input_nelem[ind] == 0)
                     {
                        required_prec += STR(node_info->input_prec[ind]) + " ";
                        template_suffix += STR(node_info->input_prec[ind]) + "_";
                     }
                     else
                     {
                        required_prec += STR(node_info->input_prec[ind]) + " " + STR(node_info->input_nelem[ind]) + " ";
                        template_suffix += STR(node_info->input_prec[ind]) + "_"+ STR(node_info->input_nelem[ind]) + "_";
                     }
                  }
                  if(node_info->output_nelem==0)
                  {
                     required_prec += STR(node_info->output_prec);
                     template_suffix += STR(node_info->output_prec);
                  }
                  else
                  {
                     required_prec += STR(node_info->output_prec) + " " + STR(node_info->output_nelem);
                     template_suffix += STR(node_info->output_prec) + "_" + STR(node_info->output_nelem);
                  }
                  std::string fu_template_parameters = GetPointer<functional_unit>(current_fu)->fu_template_parameters;
                  if(fu_template_parameters.find(required_prec) != 0) continue;
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "- required_prec: \"" + required_prec);
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "- template_suffix: \"" + template_suffix + "\"");
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "- fu_template_parameters: \"" + fu_template_parameters + "\"");
                  std::string pipeline_id = get_compliant_pipelined_unit(clock_period, curr_op->pipe_parameters, current_fu, curr_op->get_name(), library->get_library_name(), template_suffix, node_info->output_prec);
                  if(pipeline_id!="")
                     required_prec += " " + pipeline_id;

                  // if the computed parameters is different from what was used to build this specialization skip it.
                  if (required_prec != fu_template_parameters) continue;
               }
               found = true;
               std::string library_name = libraries[l];
               if(bambu_provided_resource != "")
                  library_name = WORK_LIBRARY;

               const library_managerRef libraryManager = TM->get_library_manager(library_name);
               if(bambu_provided_resource != "")
               {
                  current_fu = extract_bambu_provided(libraryManager, library_name, curr_op, curr_op->get_name(), bambu_provided_resource);
               }

               std::map<technology_nodeRef,std::map<unsigned int, std::map<HLS_manager::io_binding_type, unsigned int> > >::iterator techMap;

               std::string functionalUnitName="";
               unsigned int specializedId=current_id;
               unsigned int max_prec = node_info->input_prec.begin() == node_info->input_prec.end() ? 0 : *std::max_element(node_info->input_prec.begin(), node_info->input_prec.end());

               if(isMemory || predicate_2 || tech_constrain_it != tech_vec.end() || bambu_provided_resource != "")
               {
                  constant_id = HLS_manager::io_binding_type(0,0);
                  max_prec = 0;
               }
               if(varargs_fu)
               {
                  functionalUnitName=specialized_fuName;
                  techMap=fu_list.find(new_fu.find(functionalUnitName)->second);
                  PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Specialized unit: "+new_fu.find(functionalUnitName)->first);
                  if(techMap!=fu_list.end() && techMap->second.find(max_prec) != techMap->second.end() && techMap->second.find(max_prec)->second.find(constant_id) != techMap->second.find(max_prec)->second.end())
                  {
                     specializedId=techMap->second.find(max_prec)->second.find(constant_id)->second;
                  }
                  else
                  {
                     PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Insert into list of unit to add: "+new_fu.find(functionalUnitName)->first);
                     fu_list[new_fu.find(functionalUnitName)->second][max_prec][constant_id]=current_id;
                     precision_map[current_id]=max_prec;
                     current_id++;
                  }
               }
               else
               {
                  functionalUnitName=current_fu->get_name();
                  techMap=fu_list.find(libraryManager->get_fu(functionalUnitName));
                  if(techMap!=fu_list.end() && techMap->second.find(max_prec) != techMap->second.end() && techMap->second.find(max_prec)->second.find(constant_id) != techMap->second.find(max_prec)->second.end())
                  {
                     specializedId=techMap->second.find(max_prec)->second.find(constant_id)->second;
                  }
                  else
                  {
                     fu_list[libraryManager->get_fu(functionalUnitName)][max_prec][constant_id]=current_id;
                     precision_map[current_id]=max_prec;
                     current_id++;
                  }
               }

               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "    . Match found for vertex: " + GET_NAME(g, *vert));
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "      . Adding candidate FU: " + functionalUnitName + " for operation: " + current_op + " in position " + STR(specializedId));
               vertex_analysed.insert(*vert);
               vertex_to_fus[*vert].insert(specializedId);
               id_to_fu_names[specializedId] = std::make_pair(functionalUnitName, library_name);
               if(fu->first == MEMORY_STDN || fu->first == MEMORY_STDN_D10 || fu->first == MEMORY_STDN_D11 || fu->first == MEMORY_STDN_D21 ||
                     fu->first == BMEMORY_STDN || fu->first == BMEMORY_STDN_D10 || fu->first == BMEMORY_STDN_D11 || fu->first == BMEMORY_STDN_D21)
               {
                  unsigned int n_ports = Param->getOption<unsigned int>(OPT_channels_number);
                  set_number_channels(specializedId, n_ports);
               }
            }
         }
         if (found)
         {
            for(unsigned int pos=current_fu_id; pos<current_id; pos++)
            {
               for(std::map<technology_nodeRef,std::map<unsigned int, std::map<HLS_manager::io_binding_type, unsigned int> > >::iterator iter=fu_list.begin(); iter!=fu_list.end(); iter++)
               {
                  for(std::map<unsigned int, std::map<HLS_manager::io_binding_type, unsigned int> >::iterator iter2=iter->second.begin(); iter2!=iter->second.end(); iter2++)
                  {
                     for(std::map<HLS_manager::io_binding_type, unsigned int>::iterator iter3=iter2->second.begin(); iter3!=iter2->second.end(); iter3++)
                     {
                        if(iter3->second==pos)
                        {
                           PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Specialized unit: "+(iter->first->get_name())+" in position: "+STR(iter3->second));
                           list_of_FU.push_back(iter->first);
                           if(get_number_channels(iter3->second)>1)
                              tech_constraints.push_back(get_number_channels(iter3->second));
                           else if (tech_constrain_it == tech_vec.end())
                              tech_constraints.push_back(INFINITE_UINT);
                           else
                           {
                              //std::cerr << "Constrained " << pos << "=" <<iter->first->get_name() << "->"<< tech_constrain_it->second << std::endl;
                              PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Constrained "+STR(pos) + "=" + iter->first->get_name() + "->"<< STR(tech_constrain_it->second) );
                              tech_constraints.push_back(tech_constrain_it->second);
                           }
                        }
                     }
                  }
               }
            }
            THROW_ASSERT(tech_constraints.size() == list_of_FU.size(), "something of wrong happen");
         }
         PRINT_DBG_MEX(DEBUG_LEVEL_INFINITE, debug_level, "");
      }
      for(std::map<std::string,technology_nodeRef>::iterator iter_new_fu=new_fu.begin(); iter_new_fu!=new_fu.end(); iter_new_fu++)
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Adding functional unit: "+iter_new_fu->first+" in "+libraries[l]);
         TM->add(iter_new_fu->second,libraries[l]);
      }
      new_fu.clear();
   }

#ifndef NDEBUG
   // For debug purpose
   print_allocated_resources(g);
#endif

   // Check if each operation has been analysed
   for (std::set<vertex>::const_iterator ve = vertex_to_analyse.begin(); ve != vertex_to_analyse.end(); ve++)
   {
      if (vertex_analysed.find(*ve) != vertex_analysed.end()) continue;

      node_kind_prec_infoRef node_info(new node_kind_prec_info());
      HLS_manager::io_binding_type constant_id;
      GET_NODE_TYPE_PREC(*ve, g, node_info, constant_id, false);
      std::string precisions;
      const size_t n_ins = node_info->input_prec.size();
      for (size_t ind=0; ind < n_ins; ++ind)
      {
         if(node_info->input_nelem[ind] == 0)
            precisions += " " + STR(node_info->input_prec[ind]);
         else
            precisions += " " + STR(node_info->input_prec[ind]) + ":" + STR(node_info->input_nelem[ind]);
      }
      if(node_info->output_nelem==0)
         precisions += " " + STR(node_info->output_prec);
      else
         precisions += " " + STR(node_info->output_prec)+ ":" + STR(node_info->output_nelem);
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Operation for which does not exist a functional unit in the resource library: " + tree_helper::normalized_ID(GET_OP(g, *ve)) + " in vertex: " + GET_NAME(g, *ve) + " with vertex type: " + node_info->node_kind + " and vertex prec:" + precisions);
   }
   if(vertex_to_analyse.size() > vertex_analysed.size())
      THROW_ERROR("Vertices not completely allocated");

   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Function " + function_behavior->CGetBehavioralHelper()->get_function_name() + " uses " + STR(tech_constraints.size()) + " functional unit types");
}

void allocation::print(std::ostream& os) const
{
   std::vector<technology_nodeRef>::const_iterator fu_end = list_of_FU.end();
   unsigned int index = 0;
   for (std::vector<technology_nodeRef>::const_iterator fu =  list_of_FU.begin(); fu != fu_end; fu++)
   {
      os << index << " ";
      index++;
      (*fu)->print(os);
   }
   if (!vertex_to_fus.empty())
   {
      os << "Op_name relation with functional unit name and operations.\n";
      std::unordered_map<vertex, std::set<unsigned int> >::const_iterator op_end = vertex_to_fus.end();
      for (std::unordered_map<vertex, std::set<unsigned int> >::const_iterator it_op = vertex_to_fus.begin(); it_op != op_end; it_op++)
      {
         std::set<unsigned int>::const_iterator f_end = it_op->second.end();
         for (std::set<unsigned int>::const_iterator f_op = it_op->second.begin(); f_op != f_end; f_op++)
            os << "  [" << it_op->first << ", <" << list_of_FU[*f_op]->get_name() << ">]" << std::endl;
      }
   }
}

bool allocation::can_implement(const unsigned int fu_id, const vertex v) const
{
   return can_implement_set(v).find(fu_id) != can_implement_set(v).end();
}

const std::set<unsigned int> & allocation::can_implement_set(const vertex v) const
{
   std::unordered_map<vertex, std::set<unsigned int> >::const_iterator vtf_it = vertex_to_fus.find(v);
   THROW_ASSERT(vtf_it != vertex_to_fus.end(), "unmapped vertex");
   return vtf_it->second;

}

bool allocation::is_read_cond(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   return list_of_FU[fu_name]->get_name() == READ_COND_STD;
}

bool allocation::is_assign(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   return list_of_FU[fu_name]->get_name() == ASSIGN_UNSIGNED_STD || list_of_FU[fu_name]->get_name() == ASSIGN_SIGNED_STD || list_of_FU[fu_name]->get_name() == ASSIGN_REAL_STD;
}

bool allocation::is_builtin_wait_call(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   return list_of_FU[fu_name]->get_name() == BUILTIN_WAIT_CALL_STD;
}

bool allocation::is_return(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   return list_of_FU[fu_name]->get_name() == GIMPLE_RETURN_STD;
}

bool allocation::is_memory_unit(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   return memory_units.find(fu_name) != memory_units.end();
}

bool allocation::is_proxy_unit(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   return proxy_units.find(fu_name) != proxy_units.end();
}

bool allocation::is_vertex_bounded(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   return is_vertex_bounded_rel.find(fu_name) != is_vertex_bounded_rel.end();
}

bool allocation::is_vertex_bounded_with(const vertex v, unsigned int & fu_name) const
{
   if(binding.find(v) == binding.end())
      return false;
   else
      fu_name = binding.find(v)->second;
   return true;
}

bool allocation::is_artificial_fu(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   std::string fu_string_name = list_of_FU[fu_name]->get_name();
   if(fu_string_name == ASSIGN_UNSIGNED_STD || fu_string_name == ASSIGN_SIGNED_STD || fu_string_name == ASSIGN_REAL_STD || !has_to_be_synthetized(fu_name))
      return true;
   else
      return false;
}

bool allocation::has_to_be_synthetized(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   std::string fu_string_name = list_of_FU[fu_name]->get_name();
   if(fu_string_name == GIMPLE_RETURN_STD || fu_string_name == ENTRY_STD || fu_string_name == EXIT_STD || fu_string_name == NOP_STD ||
         fu_string_name == GIMPLE_PHI_STD || fu_string_name == GIMPLE_ASM_STD || fu_string_name == GIMPLE_LABEL_STD || fu_string_name == GIMPLE_GOTO_STD || fu_string_name == GIMPLE_NOP_STD ||
         fu_string_name == GIMPLE_PRAGMA_STD)
      return false;
   else
      return true;
}

unsigned int allocation::get_number_fu(unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   return tech_constraints[fu_name];
}

technology_nodeRef allocation::get_fu(unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   return list_of_FU[fu_name];
}

///ToBeCompleted
std::string allocation::get_string_name(unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   return list_of_FU[fu_name]->get_name() + "_" + STR(fu_name);
}

unsigned int allocation::get_initiation_time(const unsigned int fu_name, const vertex v, const OpGraphConstRef g) const
{
   THROW_ASSERT(can_implement_set(v).find(fu_name) != can_implement_set(v).end(), "This function (" + get_string_name(fu_name) + ") cannot implement the operation " + tree_helper::normalized_ID(GET_OP(g, v)));
   if(!has_to_be_synthetized(fu_name))
      return 0;
   technology_nodeRef node_op = GetPointer<functional_unit>(list_of_FU[fu_name])->get_operation(tree_helper::normalized_ID(GET_OP(g, v)));
   THROW_ASSERT(GetPointer<operation>(node_op)->time_m, "Timing information not specified for unit " + id_to_fu_names.find(fu_name)->second.first);
   return GetPointer<operation>(node_op)->time_m->get_initiation_time();
}

unsigned int allocation::get_cycles(const unsigned int fu_name, const vertex v, const OpGraphConstRef g) const
{
   THROW_ASSERT(can_implement_set(v).find(fu_name) != can_implement_set(v).end(), "This function (" + get_string_name(fu_name) + ") cannot implement the operation " + tree_helper::normalized_ID(GET_OP(g, v)));
   if(!has_to_be_synthetized(fu_name))
      return 0;
   technology_nodeRef node_op = GetPointer<functional_unit>(list_of_FU[fu_name])->get_operation(tree_helper::normalized_ID(GET_OP(g, v)));
   THROW_ASSERT(GetPointer<operation>(node_op)->time_m, "Timing information not specified for unit " + id_to_fu_names.find(fu_name)->second.first);
   return GetPointer<operation>(node_op)->time_m->get_cycles();
}

#define DSPs_MARGIN 1.3
double allocation::get_execution_time_dsp_modified(const unsigned int fu_name, const technology_nodeRef &node_op) const
{
   if(get_DSPs(fu_name)>0)
      return DSPs_MARGIN*GetPointer<operation>(node_op)->time_m->get_execution_time();
   else
      return GetPointer<operation>(node_op)->time_m->get_execution_time();
}

double allocation::get_execution_time(const unsigned int fu_name, const vertex v, const OpGraphConstRef g) const
{
   THROW_ASSERT(can_implement_set(v).find(fu_name) != can_implement_set(v).end(), "This function (" + get_string_name(fu_name) + ") cannot implement the operation " + tree_helper::normalized_ID(GET_OP(g, v)));
   if(!has_to_be_synthetized(fu_name))
      return 0.0;
   technology_nodeRef node_op = GetPointer<functional_unit>(list_of_FU[fu_name])->get_operation(tree_helper::normalized_ID(GET_OP(g, v)));
   THROW_ASSERT(GetPointer<operation>(node_op)->time_m, "Timing information not specified for unit " + id_to_fu_names.find(fu_name)->second.first);
   if (GetPointer<operation>(node_op)->time_m->get_cycles())
      return GetPointer<operation>(node_op)->time_m->get_cycles() * HLS->HLS_C->get_clock_period() * HLS->HLS_C->get_clock_period_resource_fraction();
   ///single cycle DSP based components are underestimated when the RTL synthesis backend converts in LUTs, so we slightly increase the execution time
   return get_execution_time_dsp_modified(fu_name, node_op);
}

double allocation::get_worst_execution_time(const unsigned int fu_name) const
{
   if(!has_to_be_synthetized(fu_name))
      return 0.0;
   const functional_unit::operation_vec node_ops = GetPointer<functional_unit>(list_of_FU[fu_name])->get_operations();
   double max_value = 0.0;
   functional_unit::operation_vec::const_iterator no_it_end = node_ops.end();
   for(functional_unit::operation_vec::const_iterator no_it = node_ops.begin(); no_it != no_it_end; ++no_it)
      max_value = std::max(max_value,  get_execution_time_dsp_modified(fu_name, *no_it));
   return max_value;
}

double allocation::get_stage_period(const unsigned int fu_name, const vertex v, const OpGraphConstRef g) const
{
   THROW_ASSERT(can_implement_set(v).find(fu_name) != can_implement_set(v).end(), "This function (" + get_string_name(fu_name) + ") cannot implement the operation " + tree_helper::normalized_ID(GET_OP(g, v)));
   if(!has_to_be_synthetized(fu_name))
      return 0.0;
   technology_nodeRef node_op = GetPointer<functional_unit>(list_of_FU[fu_name])->get_operation(tree_helper::normalized_ID(GET_OP(g, v)));
   THROW_ASSERT(GetPointer<operation>(node_op)->time_m, "Timing information not specified for unit " + id_to_fu_names.find(fu_name)->second.first);
   return GetPointer<operation>(node_op)->time_m->get_stage_period();
}

double allocation::get_worst_stage_period(const unsigned int fu_name) const
{
   if(!has_to_be_synthetized(fu_name))
      return 0.0;
   const functional_unit::operation_vec node_ops = GetPointer<functional_unit>(list_of_FU[fu_name])->get_operations();
   double max_value = 0.0;
   functional_unit::operation_vec::const_iterator no_it_end = node_ops.end();
   for(functional_unit::operation_vec::const_iterator no_it = node_ops.begin(); no_it != no_it_end; ++no_it)
      max_value = std::max(max_value,  GetPointer<operation>(*no_it)->time_m->get_stage_period());
   return max_value;
}

double allocation::get_area(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   if(!has_to_be_synthetized(fu_name))
      return 0.0;
   area_modelRef a_m = GetPointer<functional_unit>(list_of_FU[fu_name])->area_m;
   THROW_ASSERT(a_m, "Area information not specified for unit " + id_to_fu_names.find(fu_name)->second.first);
   double area = a_m->get_area_value();
   return area;
}


double allocation::get_DSPs(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   if(!has_to_be_synthetized(fu_name))
      return 0.0;
   area_modelRef a_m = GetPointer<functional_unit>(list_of_FU[fu_name])->area_m;
   THROW_ASSERT(a_m, "Area information not specified for unit " + id_to_fu_names.find(fu_name)->second.first);
   if(GetPointer<clb_model>(a_m))
      return GetPointer<clb_model>(a_m)->get_resource_value(clb_model::DSP);
   else return 0;
}

unsigned int allocation::get_number_channels(unsigned int fu_name) const
{
   if(nports_map.find(fu_name) == nports_map.end())
      return 1;
   else
      return nports_map.find(fu_name)->second;
}

void allocation::set_number_channels(unsigned int fu_name, unsigned int n_ports)
{
   nports_map[fu_name] = n_ports;
}

unsigned int allocation::get_number_fu_types() const
{
   return static_cast<unsigned int>(list_of_FU.size());
}


double allocation::get_attribute_of_fu_per_op(const vertex v, const OpGraphConstRef g, allocation::op_perfomed op, allocation::op_target target) const
{
   unsigned int fu_name;
   bool flag;
   double res =  get_attribute_of_fu_per_op(v, g, op, target, fu_name, flag);
   THROW_ASSERT(flag, "something of wrong happen");
   return res;
}


double allocation::get_attribute_of_fu_per_op(const vertex v, const OpGraphConstRef g, allocation::op_perfomed op, allocation::op_target target, unsigned int &fu_name, bool & flag, const updatecopy_HLS_constraints_functor *CF ) const
{
   const std::set<unsigned int> & fu_set = vertex_to_fus.find(v)->second;

   std::string op_name = tree_helper::normalized_ID(GET_OP(g, v));
   const std::set<unsigned int>::const_iterator f_end = fu_set.end();
   std::set<unsigned int>::const_iterator f_i = fu_set.begin();
   unsigned int int_value;
   double double_value;
   flag = false;
   while (CF && f_i != f_end && ((*CF)(*f_i) <= 0 || (binding.find(v) != binding.end()  && binding.find(v)->second != *f_i))) f_i++;
   if (f_i == f_end) return -1.0;
   flag = true;

   switch (target)
   {
      case initiation_time:
      {
         unsigned int temp;
         fu_name = *f_i;
         if(!has_to_be_synthetized(fu_name))
            return 1.0;

         THROW_ASSERT(GetPointer<operation>(GetPointer<functional_unit>(list_of_FU[fu_name])->get_operation(op_name))->time_m, "Timing information not specified for operation " + op_name + " on unit " + id_to_fu_names.find(fu_name)->second.first);
         int_value = GetPointer<operation>(GetPointer<functional_unit>(list_of_FU[fu_name])->get_operation(op_name))->time_m->get_initiation_time();

         if(binding.find(v) != binding.end() && binding.find(v)->second == fu_name)
            return static_cast<double>(int_value);
         f_i++;

         for (; f_i != f_end; f_i++)
         {
            if (CF && (*CF)(*f_i) <= 0) continue;
            switch (op)
            {
               case max:
                  THROW_ASSERT(GetPointer<operation>(GetPointer<functional_unit>(list_of_FU[*f_i])->get_operation(op_name))->time_m, "Timing information not specified for operation " + op_name + " on unit " + id_to_fu_names.find(*f_i)->second.first);
                  temp = MAX(int_value, GetPointer<operation>(GetPointer<functional_unit>(list_of_FU[*f_i])->get_operation(op_name))->time_m->get_initiation_time());
                  break;
               case min:
                  THROW_ASSERT(GetPointer<operation>(GetPointer<functional_unit>(list_of_FU[*f_i])->get_operation(op_name))->time_m, "Timing information not specified for operation " + op_name + " on unit " + id_to_fu_names.find(*f_i)->second.first);
                  temp = MIN(int_value, GetPointer<operation>(GetPointer<functional_unit>(list_of_FU[*f_i])->get_operation(op_name))->time_m->get_initiation_time());
                  break;
               default:
                  temp = 0;
                  THROW_ERROR(std::string("Not supported allocation::op_perfomed"));
                  break;
            }
            if (temp != int_value)
            {
               fu_name = *f_i;
               int_value = temp;
            }
         }
         return static_cast<double>(int_value);
         break;
      }
      case execution_time:
      {
         double temp;
         fu_name = *f_i;
         if(!has_to_be_synthetized(fu_name))
            return 0.0;
         THROW_ASSERT(GetPointer<operation>(GetPointer<functional_unit>(list_of_FU[fu_name])->get_operation(op_name))->time_m, "Timing information not specified for operation " + op_name + " on unit " + id_to_fu_names.find(fu_name)->second.first);
         double_value = get_execution_time_dsp_modified(fu_name, GetPointer<functional_unit>(list_of_FU[fu_name])->get_operation(op_name));
         if(binding.find(v) != binding.end() && binding.find(v)->second == fu_name)
            return double_value;
         f_i++;
         for (; f_i != f_end; f_i++)
         {
            if (CF && (*CF)(*f_i) <= 0) continue;
            switch (op)
            {
               case max:
                  THROW_ASSERT(GetPointer<operation>(GetPointer<functional_unit>(list_of_FU[*f_i])->get_operation(op_name))->time_m, "Timing information not specified for operation " + op_name + " on unit " + id_to_fu_names.find(*f_i)->second.first);
                  temp = MAX(double_value, get_execution_time_dsp_modified(fu_name, GetPointer<functional_unit>(list_of_FU[*f_i])->get_operation(op_name)));
                  break;
               case min:
                  THROW_ASSERT(GetPointer<operation>(GetPointer<functional_unit>(list_of_FU[*f_i])->get_operation(op_name))->time_m, "Timing information not specified for operation " + op_name + " on unit " + id_to_fu_names.find(*f_i)->second.first);
                  temp = MIN(double_value, get_execution_time_dsp_modified(fu_name, GetPointer<functional_unit>(list_of_FU[*f_i])->get_operation(op_name)));
                  break;
               default:
                  temp = 0;
                  THROW_ERROR(std::string("Not supported allocation::op_perfomed"));
                  break;
            }
            if (temp != double_value)
            {
               fu_name = *f_i;
               double_value = temp;
            }
         }
         return double_value;
         break;
      }
      case(power_consumption):
      default:
         THROW_ERROR(std::string("Not supported allocation::op_target"));
         break;
   }
   return -1.0;
}


unsigned int allocation::max_number_of_resources(const vertex v) const
{
   const std::set<unsigned int> & fu_set = vertex_to_fus.find(v)->second;

   unsigned int tot_num_res = 0, num_res;
   const std::set<unsigned int>::const_iterator f_end = fu_set.end();

   for (std::set<unsigned int>::const_iterator f_i = fu_set.begin(); f_i != f_end; f_i++)
   {
      num_res = tech_constraints[*f_i];
      THROW_ASSERT(num_res != 0, "something of wrong happen");
      if (num_res == INFINITE_UINT)
         return num_res;
      else
         tot_num_res += num_res;
   }
   return tot_num_res;
}


unsigned int allocation::min_number_of_resources(const vertex v) const
{
   const std::set<unsigned int> & fu_set = vertex_to_fus.find(v)->second;

   unsigned int min_num_res = INFINITE_UINT, num_res;
   const std::set<unsigned int>::const_iterator f_end = fu_set.end();

   for (std::set<unsigned int>::const_iterator f_i = fu_set.begin(); f_i != f_end; f_i++)
   {
      num_res = tech_constraints[*f_i];
      THROW_ASSERT(num_res != 0, "something of wrong happen");
      min_num_res = min_num_res > num_res ? num_res : min_num_res;
   }
   return min_num_res;
}

unsigned int allocation::get_memory_var(const unsigned int fu_name) const
{
   THROW_ASSERT(is_memory_unit(fu_name), "functional unit id not meaningful");
   return memory_units.find(fu_name)->second;
}

unsigned int updatecopy_HLS_constraints_functor::operator() (const unsigned int name) const
{
   return tech[name];
}

void updatecopy_HLS_constraints_functor::update(const unsigned int name, int delta)
{
   if (tech[name] == INFINITE_UINT)
      return;
   tech[name] = static_cast<unsigned int>(static_cast<int>(tech[name]) + delta);
}


std::pair<std::string, std::string> allocation::get_fu_name(unsigned int id) const
{
   THROW_ASSERT(id_to_fu_names.find(id) != id_to_fu_names.end(), "Functional unit name not stored!");
   return id_to_fu_names.find(id)->second;
}

bool allocation::is_direct_access_memory_unit(unsigned int fu_type)
{
   return get_fu_name(fu_type).first == ARRAY_1D_STD_BRAM || get_fu_name(fu_type).first == ARRAY_1D_STD_BRAM_N1 || get_fu_name(fu_type).first == ARRAY_1D_STD_BRAM_NN ||
         get_fu_name(fu_type).first == ARRAY_1D_STD_BRAM_SDS || get_fu_name(fu_type).first == ARRAY_1D_STD_BRAM_N1_SDS || get_fu_name(fu_type).first == ARRAY_1D_STD_BRAM_NN_SDS ||
         get_fu_name(fu_type).first == ARRAY_1D_STD_DISTRAM  ||
         get_fu_name(fu_type).first == PROXY_CTRL || get_fu_name(fu_type).first == PROXY_CTRLN;
}

bool allocation::is_indirect_access_memory_unit(unsigned int fu)
{
   std::string fu_name = get_fu_name(fu).first;
   return fu_name == BMEMORY_STD || fu_name == BMEMORY_STD_D10 || fu_name == BMEMORY_STD_D11 || fu_name == BMEMORY_STD_D21 ||
         fu_name == BMEMORY_STDN || fu_name == BMEMORY_STDN_D10 || fu_name == BMEMORY_STDN_D11 || fu_name == BMEMORY_STDN_D21 ||
         fu_name == MEMORY_STD || fu_name == MEMORY_STD_D10 || fu_name == MEMORY_STD_D11 || fu_name == MEMORY_STD_D21 ||
         fu_name == MEMORY_STDN || fu_name == MEMORY_STDN_D10 || fu_name == MEMORY_STDN_D11 || fu_name == MEMORY_STDN_D21;
}

void allocation::GET_NODE_TYPE_PREC(const vertex node, const OpGraphConstRef g, node_kind_prec_infoRef info, HLS_manager::io_binding_type &constant_id, bool is_constrained) const
{
   const tree_managerRef TreeM = HLSMgr->get_tree_manager();
   std::vector<HLS_manager::io_binding_type> vars_read = HLSMgr->get_required_values(funId, node);
   unsigned int first_valid_id = 0;
   unsigned int index=0;
   constant_id = HLS_manager::io_binding_type(0,0);
   if (vars_read.size() == 0) return;
   std::string current_op = tree_helper::normalized_ID(GET_OP(g, node));

   long long int vec_size = 0;
   bool is_a_pointer = false;
   bool is_a_function = false;
   unsigned int type_index = 0;
   bool is_second_constant = false;
   for (std::vector<HLS_manager::io_binding_type>::iterator itr = vars_read.begin(), end = vars_read.end(); itr != end; ++itr, ++index)
   {
      unsigned int id = std::get<0>(*itr);
      if (id && !first_valid_id) first_valid_id = id;
      if((current_op == "cond_expr" || current_op == "vec_cond_expr") && index != 0 && id) first_valid_id = id;
      if (id==0 || (tree_helper::is_constant(TreeM, id) && !is_constrained && !is_second_constant && vars_read.size() != 1))
      {
         info->input_prec.push_back(0);
         info->input_nelem.push_back(0);
         is_second_constant=true;
         constant_id = *itr;
      }
      else
      {
         type_index = tree_helper::get_type_index(TreeM, id);
         if(tree_helper::is_an_array(TreeM, id) || tree_helper::is_a_struct(TreeM, type_index) || tree_helper::is_an_union(TreeM, type_index) || tree_helper::is_a_complex(TreeM, type_index))
         {
            info->input_prec.push_back(32);
            info->input_nelem.push_back(0);
         }
         else
         {
            unsigned int size_value = tree_helper::size(TreeM, id);
            if(tree_helper::is_a_vector(TreeM, type_index))
            {
               const unsigned int element_type = tree_helper::GetElements(TreeM, type_index);
               const unsigned int element_size = static_cast<unsigned int>(tree_helper::size(TreeM, element_type));
               info->input_nelem.push_back(size_value/element_size);
               info->input_prec.push_back(element_size);
            }
            else
            {
               info->input_nelem.push_back(0);
               info->input_prec.push_back(size_value);
            }
         }
      }
   }

   THROW_ASSERT(first_valid_id, "Unexpected pattern");
   type_index = tree_helper::get_type_index(TreeM, first_valid_id, vec_size, is_a_pointer, is_a_function);

   if(is_a_pointer || tree_helper::is_an_array(TreeM, type_index) || tree_helper::is_a_struct(TreeM, type_index) || tree_helper::is_an_union(TreeM, type_index) || tree_helper::is_a_complex(TreeM, type_index))
   {
      info->node_kind = "VECTOR_BOOL";
   }
   else if (tree_helper::is_int(TreeM, type_index))
   {
      info->node_kind = "INT";
   }
   else if (tree_helper::is_real(TreeM, type_index))
   {
      info->node_kind = "REAL";
   }
   else if (tree_helper::is_unsigned(TreeM, type_index))
   {
      info->node_kind = "UINT";
   }
   else if (tree_helper::is_bool(TreeM, type_index))
   {
      info->node_kind = "VECTOR_BOOL";
   }
   else if(tree_helper::is_a_vector(TreeM, type_index))
   {
      const unsigned int element_type = tree_helper::GetElements(TreeM, type_index);
      if (tree_helper::is_int(TreeM, element_type))
         info->node_kind = "VECTOR_INT";
      else if(tree_helper::is_unsigned(TreeM, element_type))
         info->node_kind = "VECTOR_UINT";
      else if(tree_helper::is_real(TreeM, element_type))
         info->node_kind = "VECTOR_REAL";
   }
   else
   {
      THROW_ERROR("not supported type: " + STR(type_index));
   }



   /// Now we need to normalize the size to be compliant with the technology library assumptions
   unsigned int max_size_in =  resize_to_8_16_32_64_128(*std::max_element(info->input_prec.begin(), info->input_prec.end()));
   unsigned int min_n_elements = 0;
   std::vector<unsigned int>::const_iterator n_el_it_end = info->input_nelem.end();
   for(std::vector<unsigned int>::const_iterator n_el_it = info->input_nelem.begin(); n_el_it != n_el_it_end; ++n_el_it)
   {
      if(*n_el_it)
      {
         if(min_n_elements)
            min_n_elements = std::min(min_n_elements, *n_el_it);
         else
            min_n_elements = *n_el_it;
      }
   }
   for(std::vector<unsigned int>::iterator n_el_it = info->input_nelem.begin(); n_el_it != n_el_it_end; ++n_el_it)
   {
      if(*n_el_it)
         *n_el_it = min_n_elements;
   }
   //unsigned int max_io = std::max(max_size_in, max_size_out);
   if(current_op.find( "float_expr_") == 0 ||
         current_op.find("fix_trunc_expr_") == 0 ||
         current_op == "widen_mult_expr" ||
         current_op == "dot_prod_expr" ||
         current_op == "widen_sum_expr" ||
         current_op == "widen_mult_hi_expr" ||
         current_op == "widen_mult_lo_expr" ||
         current_op == "vec_unpack_hi_expr" ||
         current_op == "vec_unpack_lo_expr"
         )
   {
      /// ad hoc correction for float_expr conversion
      if(current_op.find( "float_expr_") == 0 && max_size_in < 32)
         max_size_in = 32;
      unsigned int nodeOutput_id = HLSMgr->get_produced_value(funId, node);
      if (nodeOutput_id)
      {
         type_index = tree_helper::get_type_index(TreeM, nodeOutput_id);
         if(tree_helper::is_an_array(TreeM, type_index) || tree_helper::is_a_struct(TreeM, type_index) || tree_helper::is_an_union(TreeM, type_index) || tree_helper::is_a_complex(TreeM, type_index))
            info->output_prec = 32;
         else
         {
            info->output_prec = tree_helper::size(TreeM, nodeOutput_id);
            if(tree_helper::is_a_vector(TreeM, type_index))
            {
               const unsigned int element_type = tree_helper::GetElements(TreeM, type_index);
               const unsigned int element_size = static_cast<unsigned int>(tree_helper::size(TreeM, element_type));
               info->output_nelem = info->output_prec/element_size;
            }
            else
               info->output_nelem = 0;
         }
      }
   }
   else
   {
      info->output_prec =max_size_in;
      info->output_nelem = min_n_elements;
   }
   size_t n_inputs = info->input_prec.size();
   for (unsigned int i = 0; i < n_inputs; ++i)
      if(info->input_prec[i] != 0)
         info->input_prec[i] = max_size_in;

}

std::string allocation::get_compliant_pipelined_unit(double clock, const std::string pipe_parameter, const technology_nodeRef current_fu, const std::string curr_op, const std::string library_name, const std::string template_suffix, unsigned int module_prec)
{
   if(pipe_parameter=="") return "";
   THROW_ASSERT(GetPointer<functional_unit>(current_fu), "expected a functional unit object");
   functional_unit* fu = GetPointer<functional_unit>(current_fu);

   THROW_ASSERT(fu->fu_template_name != "", "expected a template_name for a pipelined unit");
   if(precomputed_pipeline_unit.find(fu->fu_template_name+"_"+template_suffix) != precomputed_pipeline_unit.end())
   {
      std::string compliant_id = precomputed_pipeline_unit.find(fu->fu_template_name+"_"+template_suffix)->second;
      if(pipe_parameter == compliant_id)
         return pipe_parameter;
      else
         return "";
   }
   const HLS_targetRef HLS_T = HLSMgr->get_HLS_target();
   const technology_nodeRef fu_template = HLS_T->get_technology_manager()->get_fu(fu->fu_template_name, library_name);
   const functional_unit_template* fu_temp = GetPointer<functional_unit_template>(fu_template);
   THROW_ASSERT(fu_temp, "expected a template functional unit for a pipelined unit");
   bool is_flopoco_provided = false;
   structural_managerRef tcm = GetPointer<functional_unit>(fu_temp->FU)->CM;
   if(tcm)
   {
      structural_objectRef tmodobj=tcm->get_circ();
      module * tmod = GetPointer<module>(tmodobj);
      const NP_functionalityRef &np = tmod->get_NP_functionality();
      if(np->get_NP_functionality(NP_functionality::FLOPOCO_PROVIDED) != "")
         is_flopoco_provided = true;
   }
   technology_nodeRef fun_temp_operation = GetPointer<functional_unit>(fu_temp->FU)->get_operation(curr_op);
   THROW_ASSERT(fun_temp_operation, "operation not present in the template description");
   operation * template_op = GetPointer<operation>(fun_temp_operation);
   std::string temp_pipe_parameters = template_op->pipe_parameters;
   std::vector<std::string> parameters_split;
   boost::algorithm::split(parameters_split, temp_pipe_parameters, boost::algorithm::is_any_of("|"));
   THROW_ASSERT(parameters_split.size() > 0, "unexpected pipe_parameter format");
   for(size_t el_indx = 0; el_indx < parameters_split.size(); ++el_indx)
   {
      std::vector<std::string> parameters_pairs;
      boost::algorithm::split(parameters_pairs, parameters_split[el_indx], boost::algorithm::is_any_of(":"));
      if(parameters_pairs[0] == "*")
      {
         temp_pipe_parameters = parameters_pairs[1];
         break;
      }
      else if(boost::lexical_cast<unsigned int>(parameters_pairs[0]) == module_prec)
      {
         temp_pipe_parameters = parameters_pairs[1];
         break;
      }

   }
   THROW_ASSERT(temp_pipe_parameters != "", "expected some pipe_parameters for the the template operation");
   std::vector<std::string> pipe_parameters;
   std::string fastest_pipe_parameter="0";
   double fastest_stage_period = std::numeric_limits<double>::max();
   boost::algorithm::split(pipe_parameters, temp_pipe_parameters, boost::algorithm::is_any_of(","));
   const std::vector<std::string>::const_iterator st_end = pipe_parameters.end();
   std::vector<std::string>::const_iterator st_next;
   unsigned int skip_pipe_parameter=0;
   if(is_flopoco_provided)
      skip_pipe_parameter = std::max(1u,Param->getOption<unsigned int>(OPT_skip_pipe_parameter));
   else if(is_a_skip_operation(curr_op))
      skip_pipe_parameter = Param->getOption<unsigned int>(OPT_skip_pipe_parameter);
   for(std::vector<std::string>::const_iterator st = st_next = pipe_parameters.begin(); st != st_end; ++st)
   {
      ++st_next;
      const technology_nodeRef fu_cur_obj = HLS_T->get_technology_manager()->get_fu(fu->fu_template_name+"_"+template_suffix+"_"+*st, library_name);
      if(fu_cur_obj)
      {
         area_modelRef a_m = GetPointer<functional_unit>(fu_cur_obj)->area_m;
         THROW_ASSERT(a_m, "Area information not specified for unit " + fu->fu_template_name+"_"+template_suffix+"_"+*st);
         double dsp_multiplier = (GetPointer<clb_model>(a_m) &&  GetPointer<clb_model>(a_m)->get_resource_value(clb_model::DSP)>0) ? DSPs_MARGIN : 1.0;

         if(fu_cur_obj)
         {
            const functional_unit* fu_cur = GetPointer<functional_unit>(fu_cur_obj);
            operation* fu_cur_operation = GetPointer<operation>(fu_cur->get_operation(curr_op));
            if(fu_cur_operation->time_m->get_cycles() >= 1 && fu_cur_operation->time_m->get_stage_period()<fastest_stage_period)
            {
               fastest_pipe_parameter = *st;
               fastest_stage_period = fu_cur_operation->time_m->get_stage_period();
            }
            if((*st == "0" && dsp_multiplier*fu_cur_operation->time_m->get_execution_time() <clock) || (fu_cur_operation->time_m->get_cycles() >= 1 && fu_cur_operation->time_m->get_stage_period()<clock && *st != "0"))
            {
               if(skip_pipe_parameter && st_next != st_end)
                  --skip_pipe_parameter;
               else
               {
                  precomputed_pipeline_unit[fu->fu_template_name+"_"+template_suffix] = *st;
                  if(*st == pipe_parameter)
                  {
                     return pipe_parameter;
                  }
                  else
                  {
                     return "";
                  }
               }
            }
         }
      }
   }
   if(fastest_pipe_parameter == "0") /// in case no pipelined version exist it returns the one not pipelined
   {
     precomputed_pipeline_unit[fu->fu_template_name+"_"+template_suffix] = fastest_pipe_parameter; 
   }
   else
   {
      /// in case clock is not compatible with any of the pipelined version it returns the fastest pipelined version available
      THROW_WARNING("No functional unit exists for the given clock period: the fastest pipelined unit will be used ("+fu->fu_template_name +")");
      precomputed_pipeline_unit[fu->fu_template_name+"_"+template_suffix] = fastest_pipe_parameter;
   }
   if(pipe_parameter == fastest_pipe_parameter)
   {
      return fastest_pipe_parameter;
   }
   else
      return "";
}

#ifndef NDEBUG
inline void allocation::print_allocated_resources(const OpGraphConstRef g) const
{
   if (debug_level >= DEBUG_LEVEL_VERBOSE)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "\nDumping the list of all the fixed bindings FU <-> node");
      for (std::unordered_map<vertex, unsigned int>::const_iterator bind = binding.begin(); bind != binding.end(); bind++)
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "  Vertex " + GET_NAME(g, bind->first));
         PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "    Corresponding operation: " + tree_helper::normalized_ID(GET_OP(g, bind->first)) + "(" + STR(bind->second) + ")");
         functional_unit* fu = dynamic_cast<functional_unit*>(GetPointer<functional_unit>(list_of_FU[bind->second]));
         PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "    Vertex bound to: " + fu->get_name());
      }

      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Dumping the list of all the possible bindings FU <-> node");
      for (std::unordered_map<vertex, std::set<unsigned int> >::const_iterator bind = vertex_to_fus.begin(); bind != vertex_to_fus.end(); bind++)
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "  Vertex " + GET_NAME(g,bind->first) + "(" + GET_OP(g,bind->first) + ")");
         PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "    Operation can be implemented by the following FUs:");
         for (std::set<unsigned int>::const_iterator fu_id = bind->second.begin(); fu_id != bind->second.end(); fu_id++)
         {
            functional_unit* fu = dynamic_cast<functional_unit*>(GetPointer<functional_unit>(list_of_FU[*fu_id]));
            PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "      FU name: " + fu->get_name()+ "(" + STR(*fu_id) + ")");
         }
      }
   }
}
#endif

technology_nodeRef allocation::get_fu(const std::string& fu_name, const HLS_managerRef HLSMgr)
{
   const HLS_targetRef HLS_T = HLSMgr->get_HLS_target();
   std::string library_name = HLS_T->get_technology_manager()->get_library(fu_name);
   if(library_name == "") return technology_nodeRef();
   return HLS_T->get_technology_manager()->get_fu(fu_name, library_name);
}

std::map<unsigned int, unsigned int> allocation::get_memory_units() const
{
   return memory_units;
}

const std::map<unsigned int,unsigned int>& allocation::get_proxy_units() const
{
   return proxy_units;
}

bool allocation::is_operation_bounded(const OpGraphConstRef g, const vertex& op, unsigned int fu_type) const
{
   const technology_nodeRef node = get_fu(fu_type);
   std::string op_string = tree_helper::normalized_ID(GET_OP(g, op));
   const functional_unit* fu = GetPointer<functional_unit>(node);
   const technology_nodeRef op_node = fu->get_operation(op_string);
   return GetPointer<operation>(op_node)->is_bounded();
}

allocationRef allocation::factory(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   return allocationRef(new allocation(Param, HLSMgr, funId));
}

allocationRef allocation::xload_factory(const xml_element*, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   return allocationRef(new allocation(Param, HLSMgr, funId));
}

unsigned int allocation::get_prec(const unsigned int fu_name) const
{
   THROW_ASSERT(fu_name < get_number_fu_types(), "functional unit id not meaningful");
   THROW_ASSERT(precision_map.find(fu_name) != precision_map.end(), "missing the precision of " + STR(fu_name));
   return precision_map.find(fu_name)->second != 0 ? precision_map.find(fu_name)->second : 32;
}

#define MUX_MARGIN 0.7
double allocation::estimate_mux_time(unsigned int fu_name) const
{
   unsigned int fu_prec =  get_prec(fu_name);
   return mux_time_unit(fu_prec);

}

double allocation::mux_time_unit(unsigned int fu_prec) const
{
   const technology_managerRef TM = HLS->HLS_T->get_technology_manager();
   technology_nodeRef f_unit_mux = TM->get_fu(MUX_GATE_STD + STR("_1_") + STR(fu_prec) + "_" + STR(fu_prec) + "_" + STR(fu_prec), LIBRARY_STD_FU);
   functional_unit * fu_br= GetPointer<functional_unit>(f_unit_mux);
   technology_nodeRef op_mux_node =fu_br->get_operation(MUX_GATE_STD);
   operation * op_mux = GetPointer<operation>(op_mux_node);
   double mux_delay = op_mux->time_m->get_execution_time()-get_setup_hold_time();
   if(mux_delay<0.0) mux_delay = get_setup_hold_time();
//   return mux_delay*MUX_MARGIN+get_setup_hold_time();
//   return mux_delay+get_setup_hold_time()*MUX_MARGIN;
      return mux_delay;
}

double allocation::estimate_controller_delay()
{
   unsigned int fu_prec =  32;
   /*
   return 2*mux_time_unit(fu_prec);
*/
   const technology_managerRef TM = HLS->HLS_T->get_technology_manager();
   technology_nodeRef f_unit_plus = TM->get_fu(ADDER_STD + std::string("_") + STR(fu_prec) + "_" + STR(fu_prec) + "_" + STR(fu_prec), LIBRARY_STD_FU);
   functional_unit * fu_br= GetPointer<functional_unit>(f_unit_plus);
   technology_nodeRef op_plus_node =fu_br->get_operation("plus_expr");
   operation * op_plus = GetPointer<operation>(op_plus_node);
   double plus_delay = op_plus->time_m->get_execution_time()-get_setup_hold_time();
   if(plus_delay<0.0) plus_delay = 0.1;
   return plus_delay;
}

double allocation::get_setup_hold_time() const
{
   technology_nodeRef f_unit_br = HLS->HLS_T->get_technology_manager()->get_fu(ASSIGN_VECTOR_BOOL_STD, LIBRARY_STD_FU);
   functional_unit * fu_br= GetPointer<functional_unit>(f_unit_br);
   technology_nodeRef op_br_node =fu_br->get_operation(ASSIGN);
   operation * op_br_LOAD = GetPointer<operation>(op_br_node);
   return op_br_LOAD->time_m->get_execution_time();
}

double allocation::get_correction_time(unsigned int fu) const
{
   double res_value = get_setup_hold_time();
   std::string fu_name = get_fu_name(fu).first;
   if (fu_name == ARRAY_1D_STD_BRAM || fu_name == ARRAY_1D_STD_BRAM_N1 || fu_name == ARRAY_1D_STD_BRAM_NN)
   {
      unsigned var = get_memory_var(fu);
      if(!HLSMgr->Rmem->is_a_proxied_variable(var))
         res_value = res_value + estimate_mux_time(fu);
      if(HLSMgr->Rmem->is_private_memory(var))
         res_value = res_value + estimate_mux_time(fu);
      unsigned int bram_bitsize = HLSMgr->Rmem->get_bram_bitsize();
      int br_multiplier = 0;
      if(bram_bitsize==128)
         br_multiplier = -3;
      else if(bram_bitsize==64)
         br_multiplier = -2;
      else if(bram_bitsize==32)
         br_multiplier = -1;
      res_value = res_value + br_multiplier*get_setup_hold_time();
      //if(memory_units_sizes.find(fu) != memory_units_sizes.end() && memory_units_sizes.find(fu)->second <= 16) /// small memories are faster than large ones...
      //   res_value = res_value + estimate_mux_time(fu);
      //std::cerr << fu << " AR correction " << res_value << std::endl;
   }
   if (fu_name == ARRAY_1D_STD_BRAM_SDS || fu_name == ARRAY_1D_STD_BRAM_N1_SDS || fu_name == ARRAY_1D_STD_BRAM_NN_SDS)
   {
      unsigned var = get_memory_var(fu);
      if(!HLSMgr->Rmem->is_a_proxied_variable(var))
         res_value = res_value + estimate_mux_time(fu);
      const tree_managerRef TreeM = HLSMgr->get_tree_manager();
      unsigned int elmt_bitsize=1;
      unsigned int type_index = tree_helper::get_type_index(TreeM, var);
      tree_nodeRef type_node = TreeM->get_tree_node_const(type_index);
      tree_helper::accessed_greatest_bitsize(TreeM, type_node, type_index, elmt_bitsize);
      int size_multiplier = 0;
      if(elmt_bitsize==128)
         size_multiplier = -2;
      else if(elmt_bitsize==64)
         size_multiplier = -1;
      else if(elmt_bitsize==32)
         size_multiplier = 0;
      else if(elmt_bitsize==16)
         size_multiplier = +1;
      else if(elmt_bitsize==8)
         size_multiplier = +2;
      res_value = res_value + size_multiplier*get_setup_hold_time();
      //std::cerr << fu << " AR correction " << res_value << std::endl;
   }
   else if(fu_name == PROXY_CTRL || fu_name == PROXY_CTRLN)
   {
      unsigned var = proxy_units.find(fu)->second;
      if(HLSMgr->Rmem->is_private_memory(var) && HLSMgr->Rmem->is_sds_var(var))
      {
         double STORE_exec_delta;
         if(fu_name == PROXY_CTRL)
         {
            technology_nodeRef f_unit_br = HLS->HLS_T->get_technology_manager()->get_fu(ARRAY_1D_STD_BRAM, LIBRARY_STD_FU);
            functional_unit * fu_br= GetPointer<functional_unit>(f_unit_br);
            technology_nodeRef op_br_node =fu_br->get_operation(STORE);
            operation * op_br_STORE = GetPointer<operation>(op_br_node);
            double STORE_exec_time = op_br_STORE->time_m->get_execution_time();
            technology_nodeRef f_unit_sds = HLS->HLS_T->get_technology_manager()->get_fu(ARRAY_1D_STD_BRAM_SDS, LIBRARY_STD_FU);
            functional_unit * fu_sds= GetPointer<functional_unit>(f_unit_sds);
            technology_nodeRef op_sds_node =fu_sds->get_operation(STORE);
            operation * op_sds_STORE = GetPointer<operation>(op_sds_node);
            double STORE_sds_exec_time = op_sds_STORE->time_m->get_execution_time();
            STORE_exec_delta = STORE_exec_time - STORE_sds_exec_time;
         }
         else
         {
            technology_nodeRef f_unit_br = HLS->HLS_T->get_technology_manager()->get_fu(ARRAY_1D_STD_BRAM_NN, LIBRARY_STD_FU);
            functional_unit * fu_br= GetPointer<functional_unit>(f_unit_br);
            technology_nodeRef op_br_node =fu_br->get_operation(STORE);
            operation * op_br_STORE = GetPointer<operation>(op_br_node);
            double STORE_exec_time = op_br_STORE->time_m->get_execution_time();
            technology_nodeRef f_unit_sds = HLS->HLS_T->get_technology_manager()->get_fu(ARRAY_1D_STD_BRAM_NN_SDS, LIBRARY_STD_FU);
            functional_unit * fu_sds= GetPointer<functional_unit>(f_unit_sds);
            technology_nodeRef op_sds_node =fu_sds->get_operation(STORE);
            operation * op_sds_STORE = GetPointer<operation>(op_sds_node);
            double STORE_sds_exec_time = op_sds_STORE->time_m->get_execution_time();
            STORE_exec_delta = STORE_exec_time - STORE_sds_exec_time;

         }
         res_value = res_value + STORE_exec_delta;
         const tree_managerRef TreeM = HLSMgr->get_tree_manager();
         unsigned int elmt_bitsize=1;
         unsigned int type_index = tree_helper::get_type_index(TreeM, var);
         tree_nodeRef type_node = TreeM->get_tree_node_const(type_index);
         tree_helper::accessed_greatest_bitsize(TreeM, type_node, type_index, elmt_bitsize);
         int size_multiplier = 0;
         if(elmt_bitsize==128)
            size_multiplier = -2;
         else if(elmt_bitsize==64)
            size_multiplier = -1;
         else if(elmt_bitsize==32)
            size_multiplier = 0;
         else if(elmt_bitsize==16)
            size_multiplier = +1;
         else if(elmt_bitsize==8)
            size_multiplier = +2;
         res_value = res_value + size_multiplier*get_setup_hold_time();
         //std::cerr << fu << " PR correction " << res_value << std::endl;
      }
      else
      {
         if(HLSMgr->Rmem->is_private_memory(var))
            res_value = res_value + estimate_mux_time(fu);
         unsigned int bram_bitsize = HLSMgr->Rmem->get_bram_bitsize();
         int br_multiplier = 0;
         if(bram_bitsize==128)
            br_multiplier = -3;
         else if(bram_bitsize==64)
            br_multiplier = -2;
         else if(bram_bitsize==32)
            br_multiplier = -1;
         res_value = res_value + br_multiplier*get_setup_hold_time();
      }
      //std::cerr << fu << " PR correction " << res_value << std::endl;
   }
   else if(fu_name == BMEMORY_STD || fu_name == BMEMORY_STDN)
   {
      unsigned int bram_bitsize = HLSMgr->Rmem->get_bram_bitsize();
      int br_multiplier = 0;
      if(bram_bitsize==128)
         br_multiplier = -3;
      else if(bram_bitsize==64)
         br_multiplier = -2;
      else if(bram_bitsize==32)
         br_multiplier = -1;
      res_value = res_value + br_multiplier*get_setup_hold_time();
      //std::cerr << fu << " BM correction " << res_value << std::endl;
   }
   return res_value;
}
double allocation::estimate_call_delay()
{
   double call_delay = (HLS->HLS_C->get_clock_period_resource_fraction()*HLS->HLS_C->get_clock_period())-3*mux_time_unit(32);
   if(call_delay<0.0)
      call_delay = 0.1;
   return call_delay;
}


double allocation::compute_normalized_area(unsigned int fu_s1, size_t u_nterms)
{
   double resource_area = get_area(fu_s1);
   int n_terms = static_cast<int>(u_nterms);

   //double initial_area = resource_area;
   technology_nodeRef fu_obj = get_fu(fu_s1);
   if(GetPointer<functional_unit>(fu_obj) && GetPointer<functional_unit>(fu_obj)->fu_template_name != "")
   {
      const functional_unit::operation_vec & Ops = GetPointer<functional_unit>(fu_obj)->get_operations();
      unsigned int max_prec = 0;
      std::string tp = GetPointer<functional_unit>(fu_obj)->fu_template_parameters;
      std::vector<std::string> split;
      boost::algorithm::split(split, tp, boost::algorithm::is_any_of(" "));
      int has_pp = 0;

      functional_unit::operation_vec::const_iterator ops_end = Ops.end();
      for (functional_unit::operation_vec::const_iterator ops = Ops.begin(); ops != ops_end; ops++)
      {
         operation* curr_op = GetPointer<operation>(*ops);
         std::string pp = curr_op->pipe_parameters;
         if(pp != "")
         {
            has_pp = 1;
            break;
         }
      }

      n_terms = static_cast<int>(split.size()) - has_pp;
      THROW_ASSERT(n_terms>0, "unexpected condition");
      for(int index = 0; index < n_terms; ++index)
      {
         max_prec = std::max(max_prec, boost::lexical_cast<unsigned int>(split[static_cast<size_t>(index)]));

      }
      unsigned int boundary_cost = 0;
      for(int index = 0; index < n_terms; ++index)
      {
         if(split[static_cast<size_t>(index)] != "0")
            boundary_cost += boost::lexical_cast<unsigned int>(split[static_cast<size_t>(index)]);
      }
      resource_area = (resource_area/boundary_cost);
   }
   else
      resource_area = resource_area/(32*n_terms);

   return resource_area+get_DSPs(fu_s1);;
}

