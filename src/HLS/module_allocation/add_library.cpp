/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file add_library.cpp
 * @brief Implementation of the class to add the current module to the technology library
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "add_library.hpp"

#include "hls.hpp"
#include "hls_constraints.hpp"
#include "hls_target.hpp"
#include "hls_manager.hpp"
#include "allocation.hpp"
#include "state_transition_graph_manager.hpp"
#include "state_transition_graph.hpp"
#include "memory.hpp"

#include "tree_manager.hpp"
#include "tree_helper.hpp"
#include "structural_manager.hpp"
#include "structural_objects.hpp"
#include "technology_manager.hpp"
#include "time_model.hpp"
#include "area_model.hpp"

#include "Parameter.hpp"

add_library::add_library(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned _funId) :
   HLS_step(_Param, _HLSMgr, _funId)
{

}

add_library::~add_library()
{

}

void add_library::exec()
{
   /*const tree_managerRef TreeM = HLSMgr->get_tree_manager();*/
   std::string function_name = /*tree_helper::normalized_ID(tree_helper::name_function(TreeM, HLS->functionId));*/ HLS->top->get_circ()->get_typeRef()->id_type;
   const technology_managerRef TM = HLS->HLS_T->get_technology_manager();
   TM->add_resource(WORK_LIBRARY, function_name, HLS->top);
   TM->add_operation(WORK_LIBRARY, function_name, function_name);
   double clock_period_value = HLS->HLS_C->get_clock_period();
   const target_deviceRef device = HLS->HLS_T->get_target_device();
   functional_unit* fu = GetPointer<functional_unit>(TM->get_fu(function_name, WORK_LIBRARY));
   fu->set_clock_period(clock_period_value);
   fu->set_clock_period_resource_fraction(HLS->HLS_C->get_clock_period_resource_fraction());
   operation* op = GetPointer<operation>(fu->get_operation(function_name));
   op->time_m = time_model::create_model(device->get_type(), Param);
   if(HLS->STG->CGetStg()->CGetStateTransitionGraphInfo()->is_a_dag)
   {
      const structural_objectRef cir = HLS->top->get_circ();
      module *mod = GetPointer<module>(cir);
      bool is_bounded = !HLSMgr->Rmem->has_proxied_internal_variables(funId);
      for (unsigned int i = 0; i < mod->get_in_port_size() && is_bounded; i++)
      {
         const structural_objectRef& port_obj = mod->get_in_port(i);
         if(GetPointer<port_o>(port_obj)->get_is_memory())
            is_bounded = false; ///functions accessing memory are classified as unbounded
      }
      if(is_bounded)
      {
         unsigned int min_cycles = HLS->STG->CGetStg()->CGetStateTransitionGraphInfo()->min_cycles;
         unsigned int max_cycles = HLS->STG->CGetStg()->CGetStateTransitionGraphInfo()->max_cycles;
         if(max_cycles == min_cycles && min_cycles > 0 && min_cycles < 33)
         {
            op->bounded = true;
            op->time_m->set_execution_time(HLS->ALL->estimate_call_delay(), min_cycles);
            op->time_m->set_stage_period(HLS->ALL->estimate_call_delay());
         }
         else
         {
            op->bounded = false;
            op->time_m->set_execution_time(HLS->ALL->estimate_call_delay(), 0);
         }
      }
      else
      {
         op->bounded = false;
         op->time_m->set_execution_time(HLS->ALL->estimate_call_delay(), 0);
      }
   }
   else
   {
      op->bounded = false;
      op->time_m->set_execution_time(HLS->ALL->estimate_call_delay(), 0);
   }
   op->time_m->set_synthesis_dependent(true);
   fu->area_m = area_model::create_model(device->get_type(), Param);
   fu->area_m->set_area_value(2000);/// fake number to avoid sharing of functions
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Added " << function_name << " to WORK_LIBRARY");
}

add_libraryRef add_library::xload_factory(const xml_element*, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   return add_libraryRef(new add_library(Param, HLSMgr, funId));
}
