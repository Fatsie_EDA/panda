/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file objective_evaluator.cpp
 * @brief
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_MODELSIM.hpp"
#include "config_HAVE_XILINX.hpp"
#include "config_HAVE_LIBRARY_CHARACTERIZATION_BUILT.hpp"

#include "objective_evaluator.hpp"

#include "hls.hpp"
#include "hls_target.hpp"

///linear models
#if HAVE_EXPERIMENTAL
#include "area_estimation.hpp"
#include "time_estimation.hpp"
#include "clock_slack_estimation.hpp"
#endif

///exact evaluations
#if HAVE_LIBRARY_CHARACTERIZATION_BUILT
#include "BackendFlow.hpp"
#include "area_evaluation.hpp"
#include "registers_evaluation.hpp"
#include "dsps_evaluation.hpp"
#include "brams_evaluation.hpp"
#include "slack_evaluation.hpp"
#include "frequency_evaluation.hpp"
#endif
#include "time_evaluation.hpp"
#include "cycles_evaluation.hpp"
#if HAVE_EXPERIMENTAL
#include "num_AF_edges_estimation.hpp"
#include "edges_reduction_evaluation.hpp"
#endif

#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"
#include "exceptions.hpp"

objective_evaluator::objective_evaluator(const ParameterConstRef _Param, const hlsRef _HLS, const BackendFlowRef _BEflow) :
   Param(_Param),
   debug_level(_Param->getOption<int>(OPT_debug_level)),
   output_level(_Param->getOption<int>(OPT_output_level)),
   HLS(_HLS),
   BEflow(_BEflow)
{

}

objective_evaluator::~objective_evaluator()
{

}

objective_evaluatorRef objective_evaluator::create_evaluator(evaluation_objective obj, evaluation_mode mode, const ParameterConstRef Param, const hlsRef _HLS, const BackendFlowRef _BEflow)
{
   ///list of wrappers that could be shared between the different estimations
   switch(mode)
   {
      case EXACT:
      {
         switch(obj)
         {
#if HAVE_LIBRARY_CHARACTERIZATION_BUILT
            case CLOCK_SLACK:
            {
               return objective_evaluatorRef(new slack_evaluation(Param, _HLS, _BEflow));
            }
            case FREQUENCY:
            {
               return objective_evaluatorRef(new frequency_evaluation(Param, _HLS, _BEflow));
            }
            case AREA:
            {
               return objective_evaluatorRef(new area_evaluation(Param, _HLS, _BEflow));
            }
            case REGISTERS:
            {
               return objective_evaluatorRef(new registers_evaluation(Param, _HLS, _BEflow));
            }
            case DSPS:
            {
               return objective_evaluatorRef(new dsps_evaluation(Param, _HLS, _BEflow));
            }
            case BRAMS:
            {
               return objective_evaluatorRef(new brams_evaluation(Param, _HLS, _BEflow));
            }
#endif
            case TIME:
            {
               return objective_evaluatorRef(new time_evaluation(Param, _HLS, _BEflow));
            }
            case CYCLES:
            {
               return objective_evaluatorRef(new cycles_evaluation(Param, _HLS, _BEflow));
            }
#if HAVE_EXPERIMENTAL
            case NUM_AF_EDGES:
               return objective_evaluatorRef(new num_AF_edges_estimation(Param, _HLS, _BEflow));
            case EDGES_REDUCTION:
               return objective_evaluatorRef(new edges_reduction_evaluation(Param, _HLS, _BEflow));
#endif
#if ! HAVE_LIBRARY_CHARACTERIZATION_BUILT
            case AREA:
            case CLOCK_SLACK:
#endif
            default:
               THROW_ERROR("Wrong Objective in Exact Evaluation Mode!");
         }
      }
#if HAVE_EXPERIMENTAL
      case LINEAR:
      {
         switch(obj)
         {
            case AREA:
               return objective_evaluatorRef(new area_estimation(Param, _HLS, _BEflow));
            case TIME:
               return objective_evaluatorRef(new time_estimation(Param, _HLS, _BEflow));
            case CLOCK_SLACK:
               return objective_evaluatorRef(new clock_slack_estimation(Param, _HLS, _BEflow));
#if HAVE_LIBRARY_CHARACTERIZATION_BUILT
            case FREQUENCY:
            case REGISTERS:
            case BRAMS:
            case DSPS:
#endif
            case NUM_AF_EDGES:
            case EDGES_REDUCTION:
            case CYCLES:
            default:
               THROW_ERROR("Wrong Objective in Linear Evaluation Mode");
         }
      }
      case STATISTICAL:
      {
         THROW_UNREACHABLE("Statistical evaluation is not yet supported");
      }
#endif
      case NONE:
      {
         return objective_evaluatorRef();
      }
      case WEIGHTED_AVERAGE:
      default:
         THROW_UNREACHABLE("Wrong estimation mode");
   }
   return objective_evaluatorRef();
}

std::string objective_evaluator::get_evaluation_objective_text(evaluation_objective obj)
{
   switch(obj)
   {
      case AREA:
         return "AREA";
      case CLOCK_SLACK:
         return "CLOCK_SLACK";
#if HAVE_LIBRARY_CHARACTERIZATION_BUILT
      case FREQUENCY:
         return "FREQUENCY";
      case REGISTERS:
         return "REGISTERS";
      case DSPS:
         return "DSPS";
      case BRAMS:
         return "BRAMS";
#endif
      case TIME:
         return "TIME";
      case CYCLES:
         return "CYCLES";
#if HAVE_EXPERIMENTAL
      case NUM_AF_EDGES:
         return "NUM_AF_EDGES";
      case EDGES_REDUCTION:
         return "EDGES_REDUCTION";
#endif
      default:
         THROW_ERROR("Wrong Objective!");
   }
   return "";
}
