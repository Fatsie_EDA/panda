/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file objective_evaluator.hpp
 * @brief Definition of wrapper class for all objectives that can be evaluated into high-level synthesis evaluations
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef OBJECTIVE_EVALUATOR_HPP
#define OBJECTIVE_EVALUATOR_HPP

#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_LIBRARY_CHARACTERIZATION_BUILT.hpp"

#include "refcount.hpp"
#include <vector>

/**
 * @name forward declarations
 */
//@{
/// RefCount type definition of the objective_evaluator class
REF_FORWARD_DECL(objective_evaluator);
/// RefCount type definition of the hls data structure
REF_FORWARD_DECL(hls);
/// RefCount type definition of the target device datastructure
REF_FORWARD_DECL(target_device);
/// RefCount type definition of the parameter class
CONSTREF_FORWARD_DECL(Parameter);
REF_FORWARD_DECL(BackendFlow);
//@}

/**
 * @class objective_evaluator
 * @ingroup Estimations
 *
 * This class defines a wrapper for all objectives to be evaluated
 */
class objective_evaluator
{
   protected:

      /// class containing all the parameters
      const ParameterConstRef Param;

      /// debugging level of the class
      int debug_level;

      /// output level of the class
      int output_level;

      /// class containing the HLS results
      const hlsRef HLS;

      /// backend flow
      const BackendFlowRef BEflow;

   public:

      /// Definition of the objectives that can be evaluated.
      /// - TIME is the evaluation of the execution time for the behavioral description under given constraints (e.g., clock period)
      /// - AREA is the evaluation of area needed to implement the description under given technology library informations
      typedef enum
      {
         TIME = 0,
         CYCLES,
         AREA,
         CLOCK_SLACK,
#if HAVE_LIBRARY_CHARACTERIZATION_BUILT
         FREQUENCY,
         REGISTERS,
         BRAMS,
         DSPS,
#endif
#if HAVE_EXPERIMENTAL
         NUM_AF_EDGES,
         EDGES_REDUCTION
#endif
      } evaluation_objective;

      typedef enum
      {
         EXACT = 0,
#if HAVE_EXPERIMENTAL
         LINEAR,
         STATISTICAL,
#endif
         WEIGHTED_AVERAGE,
         NONE
      } evaluation_mode;

      /**
       * Constructor of the class
       */
      objective_evaluator(const ParameterConstRef _Param, const hlsRef _HLS, const BackendFlowRef _BEflow);

      /**
       * Destructor of the class
       */
      virtual ~objective_evaluator();

      /**
       * Creates the evaluators of the desired objective
       */
      static
      objective_evaluatorRef create_evaluator(evaluation_objective obj, evaluation_mode mode, const ParameterConstRef Param, const hlsRef HLS, const BackendFlowRef _BEflow);

      /**
       * Evaluates the specified objectives
       */
      virtual std::vector<double> estimate() = 0;

      /**
       * Return the evaluation_objective obj as a string
      */
      static
      std::string get_evaluation_objective_text(evaluation_objective obj);

};

/// RefCount type definition of the objective_evaluator class
typedef refcount<objective_evaluator> objective_evaluatorRef;

#endif // OBJECTIVE_EVALUATOR_HPP
