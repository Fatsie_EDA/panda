/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file evaluation.cpp
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"

#include <set>

#include "evaluation.hpp"
#include "objective_evaluator.hpp"

#include "hls.hpp"
#include "hls_target.hpp"
#include "hls_constraints.hpp"
#include "hls_manager.hpp"

#include "technology_manager.hpp"

#include "schedule.hpp"

#include "BambuParameter.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"

#include "dbgPrintHelper.hpp"
#include "polixml.hpp"
#include "xml_helper.hpp"


evaluation::evaluation(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId, objective_evaluator::evaluation_mode _mode):
      HLS_step(_Param, _HLSMgr, _funId),
      mode(_mode)
{
   std::string objective_string = Param->getOption<std::string>(OPT_evaluation_objectives);
   std::vector<std::string> objective_vector = convert_string_to_vector<std::string>(objective_string, ",");
   const BackendFlowRef BEflow = HLSMgr->get_backend_flow();
   for(unsigned int v = 0; v < objective_vector.size(); v++)
   {
      if (objective_vector[v] == "AREA")
         cost_function_list.push_back(objective_evaluator::AREA);
      else if (objective_vector[v] == "TIME")
         cost_function_list.push_back(objective_evaluator::TIME);
      else if (objective_vector[v] == "CYCLES")
         cost_function_list.push_back(objective_evaluator::CYCLES);
#if HAVE_LIBRARY_CHARACTERIZATION_BUILT
      else if (objective_vector[v] == "CLOCK_SLACK")
         cost_function_list.push_back(objective_evaluator::CLOCK_SLACK);
      else if (objective_vector[v] == "FREQUENCY")
         cost_function_list.push_back(objective_evaluator::FREQUENCY);
      else if (objective_vector[v] == "REGISTERS")
         cost_function_list.push_back(objective_evaluator::REGISTERS);
      else if (objective_vector[v] == "DSPS")
         cost_function_list.push_back(objective_evaluator::DSPS);
      else if (objective_vector[v] == "BRAMS")
         cost_function_list.push_back(objective_evaluator::BRAMS);
#endif
#if HAVE_EXPERIMENTAL
      else if (objective_vector[v] == "NUM_AF_EDGES")
         cost_function_list.push_back(objective_evaluator::NUM_AF_EDGES);
      else if (objective_vector[v] == "EDGES_REDUCTION")
         cost_function_list.push_back(objective_evaluator::EDGES_REDUCTION);
#endif
      else
         THROW_ERROR("Estimation objective not yet supported: \"" + objective_vector[v] + "\"");
      cost_functions.push_back(objective_evaluator::create_evaluator(cost_function_list.back(), mode, Param, HLS, BEflow));
   }
}

evaluation::~evaluation()
{

}

void evaluation::exec()
{
   PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, " == High-Level Synthesis Results Evaluations ==");

   evaluations.clear();
   size_t n_objectives = cost_function_list.size();
   std::string out_file_name = "bambu_results";

   unsigned int progressive = 0;
   std::string candidate_out_file_name;
   do
   {
      candidate_out_file_name = out_file_name + "_" + boost::lexical_cast<std::string>(progressive++) + ".xml";
   } while (boost::filesystem::exists(candidate_out_file_name));

   out_file_name = candidate_out_file_name;
   xml_document document;
   xml_element* nodeRoot = document.create_root_node("bambu_results");

   std::string bench_name;
   if(Param->isOption(OPT_benchmark_name))
      bench_name = Param->getOption<std::string>(OPT_benchmark_name);
   if(bench_name == "")
   {
      bench_name = HLS->FB->CGetBehavioralHelper()->get_function_name();
   }
   else
      bench_name += ":" + HLS->FB->CGetBehavioralHelper()->get_function_name();

   bench_name += "_" + boost::lexical_cast<std::string>(progressive-1);

   WRITE_XNVM2("benchmark_name", bench_name, nodeRoot);
   xml_element* child_element;

   for (unsigned int obj_index = 0; obj_index < n_objectives; ++obj_index)
   {
      std::string value;
      ///each objective can potentially return a vector of values
      std::vector<double> evaluation1 = cost_functions[obj_index]->estimate();

      value = boost::lexical_cast<std::string>(evaluation1[0]);
      evaluations.push_back(evaluation1[0]);

      for(unsigned int num = 1; num < evaluation1.size(); ++num)
      {
        evaluations.push_back(evaluation1[num]);
        value += "," + boost::lexical_cast<std::string>(evaluation1[0]);
      }
      child_element = nodeRoot->add_child_element(objective_evaluator::get_evaluation_objective_text(cost_function_list[obj_index]));
      WRITE_XNVM2("value", value, child_element);
   }
   child_element = nodeRoot->add_child_element("HLS_execution_time");
   long double exec_time = HLSMgr->HLS_execution_time;
   WRITE_XNVM2("value", STR(exec_time/1000), child_element);

   document.write_to_file_formatted(out_file_name);
}

evaluationRef evaluation::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string mode = "EXACT";
   std::string obj = "AREA,TIME,CYCLES,FREQUENCY,CLOCK_SLACK,REGISTERS,DSPS,BRAMS";
   objective_evaluator::evaluation_mode ev_mode = objective_evaluator::EXACT;

   if (CE_XVM(mode, node)) LOAD_XVM(mode, node);
   if (CE_XVM(obj, node)) LOAD_XVM(obj, node);
   const_cast<Parameter *>(Param.get())->setOption(OPT_evaluation_objectives, obj);

   if (mode == "EXACT")
      ev_mode = objective_evaluator::EXACT;
   else
      THROW_ERROR("Evaluation method \"" + mode + "\" currently not supported");
   return evaluationRef(new evaluation(Param, HLSMgr, funId, ev_mode));
}

std::string evaluation::get_kind_text() const
{
   return "evaluation";
}
