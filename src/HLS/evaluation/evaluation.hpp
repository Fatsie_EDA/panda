/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file evaluation.hpp
 * @brief Class to compute evaluations about high-level synthesis
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */

#ifndef _EVALUTION_HPP_
#define _EVALUTION_HPP_

#include "hls_step.hpp"
REF_FORWARD_DECL(evaluation);

#include "objective_evaluator.hpp"

#include <vector>

/**
 * @defgroup Estimations High_Level Synthesis evaluations
 * @ingroup HLS
 * This subproject contains informations about high level synthesis evaluation results
 * To get more details, please see \ref src_HLS_evaluation_page
 */

/**
 * @class evaluation
 * @brief Class definition for high level synthesis result evaluation
 * @ingroup Estimations
 *
 * This class is the abstract class for all kind of result evaluations. Different kinds of evaluation can be performed.
 * Real evaluations or lower-bound/upper-bound evaluations can be provided.
 */
class evaluation : public HLS_step
{
   protected:

      /// List of objectives that have to be evaluated
      std::vector<objective_evaluator::evaluation_objective> cost_function_list;

      /// Vector of the objective_evaluator
      std::vector<objective_evaluatorRef> cost_functions;

      /// evaluation  mode
      objective_evaluator::evaluation_mode mode;

      /// store the result of the evaluation
      std::vector<double> evaluations;

   public:

      /// vector representing the weights associated with each objective
      std::vector<double> weights;

      /**
       * Constructor
       */
      evaluation(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId, objective_evaluator::evaluation_mode mode);

      /**
       * Destructor
       */
      virtual ~evaluation();

      /**
       * Performs the evaluation of the solution
       */
      virtual void exec();

      /**
       * Returns the results of the evaluation
       */
      const std::vector<double> & get_evaluation () const {return evaluations;}

      /**
       * Returns the name of the step
       */
      std::string get_kind_text() const;

      /**
       * Factory method from XML
       */
      static
      evaluationRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

};
///RefCount type definition of the evaluation class
typedef refcount<evaluation> evaluationRef;

#endif
