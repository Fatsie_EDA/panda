/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file brams_evaluation.hpp
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef BRAMS_EVALUATION_HPP
#define BRAMS_EVALUATION_HPP

#include "refcount.hpp"
#include "design_evaluation.hpp"

REF_FORWARD_DECL(hls);
REF_FORWARD_DECL(BackendFlow);

/**
 * @class brams_evaluation
 * Class determining the number of BRAMs after logic synthesis.
 */
class brams_evaluation : public design_evaluation
{
   public:

      /**
       * Constructor of the class
       */
      brams_evaluation(const ParameterConstRef Param, const hlsRef HLS, const BackendFlowRef _BEflow);

      /**
       * Destructor of the class
       */
      virtual ~brams_evaluation() {}

      /**
       * Return the number of BRAMs
       */
      std::vector<double> estimate();

};

typedef refcount<brams_evaluation> brams_evaluationRef;

#endif
