/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file slack_evaluation.hpp
 * @brief .
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef SLACK_EVALUATION_HPP
#define SLACK_EVALUATION_HPP

#include "refcount.hpp"
#include "design_evaluation.hpp"

REF_FORWARD_DECL(hls);
REF_FORWARD_DECL(BackendFlow);

/**
 * @class slack_evaluation
 * Class determining exact slack after logic synthesis.
 */
class slack_evaluation : public design_evaluation
{
   public:

      /**
       * Constructor of the class
       */
      slack_evaluation(const ParameterConstRef _Param, const hlsRef _HLS, const BackendFlowRef _BEflow);

      /**
       * Destructor of the class
       */
      virtual ~slack_evaluation() {}

      /**
       * Computes the area evaluations
       */
      std::vector<double> estimate();

};

typedef refcount<slack_evaluation> slack_evaluationRef;

#endif
