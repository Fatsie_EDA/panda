/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file cycles_evaluation.hpp
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "cycles_evaluation.hpp"

#include "hls.hpp"
#include "hls_constraints.hpp"

#include "BackendFlow.hpp"

cycles_evaluation::cycles_evaluation(const ParameterConstRef _Param, const hlsRef _HLS, const BackendFlowRef _BEflow) :
   objective_evaluator(_Param, _HLS, _BEflow)
{

}


std::vector<double> cycles_evaluation::estimate()
{
#if HAVE_SIMULATION_WRAPPER_BUILT

   unsigned long long int average_execution = BEflow->ExecuteSimulation();
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Simulation(s) completed! Average execution: " + boost::lexical_cast<std::string>(average_execution) + " cycles");
   return std::vector<double>(1, static_cast<double>(average_execution));
#else
   return std::vector<double>(1, 0);
#endif
}
