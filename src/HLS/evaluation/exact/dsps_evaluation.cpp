/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file dsps_evaluation.hpp
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "dsps_evaluation.hpp"

#include "area_model.hpp"
#include "clb_model.hpp"

#include "BackendFlow.hpp"

dsps_evaluation::dsps_evaluation(const ParameterConstRef _Param, const hlsRef _HLS, const BackendFlowRef _BEflow) :
   design_evaluation(_Param, _HLS, _BEflow)
{

}


std::vector<double> dsps_evaluation::estimate()
{
   if (!BEflow->get_used_resources())
   {
      evaluate();
   }

   ///get the used resources from the wrapper
   area_modelRef area_m = BEflow->get_used_resources();
   double dsps = 0;
   if(GetPointer<clb_model>(area_m))
      dsps = GetPointer<clb_model>(area_m)->get_resource_value(clb_model::DSP);
   if (area_m) PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "DSPs: " << dsps);
   else PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "(no DSPs)");

   return std::vector<double>(1, dsps);

}

