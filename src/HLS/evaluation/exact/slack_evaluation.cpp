/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file slack_evaluation.hpp
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "slack_evaluation.hpp"

#include "hls.hpp"
#include "hls_constraints.hpp"

#include "time_model.hpp"

#include "BackendFlow.hpp"

slack_evaluation::slack_evaluation(const ParameterConstRef _Param, const hlsRef _HLS, const BackendFlowRef _BEflow) :
   design_evaluation(_Param, _HLS, _BEflow)
{

}

std::vector<double> slack_evaluation::estimate()
{
   if (!BEflow->get_timing_results())
   {
      evaluate();
   }

   ///get the timing information after the synthesis
   time_modelRef time_m = BEflow->get_timing_results();
   double minimum_period = time_m->get_execution_time();

   double clock_period = HLS->HLS_C->get_clock_period();
   double slack = clock_period - minimum_period;
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Clock period         : " + boost::lexical_cast<std::string>(clock_period));
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Design minimum period: " + boost::lexical_cast<std::string>(minimum_period));
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Design slack         : " + boost::lexical_cast<std::string>(slack));

   return std::vector<double>(1, slack);
}

