/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file axi4lite_interface.hpp
 * @brief Class to generate AXI interfaces for cores generated through HLS
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision: 10946 $
 * $Date: 2013-08-30 10:29:02 +0200 (Thu, 30 Aug 2013) $
 * Last modified by $Author: mminutoli $
 *
*/
#ifndef _AXI_INTERFACE_HPP_
#define _AXI_INTERFACE_HPP_

#include "minimal_interface.hpp"
REF_FORWARD_DECL(structural_manager);
REF_FORWARD_DECL(structural_object);

class axi4lite_interface : public minimal_interface
{
   protected:

      /**
       * Adds the ports for the slave interface
       */
      void add_slave_interface(const structural_managerRef SM_axi_interface, const structural_objectRef wrappedOb, bool only_parametersj);

      /**
       * Adds the ports for the master interface
       */
      void add_master_interface(const structural_managerRef SM_axi_interface, const structural_objectRef wrappedObj);

      /**
       * Allocates the slave interface
       */
      void create_slv_interface(const structural_managerRef SM_axi_interface, const structural_objectRef wrappedObj, bool only_parameters);

      /**
       * Allocates the master interface
       */
      void create_master_interface(const structural_managerRef SM_axi_interface, const structural_objectRef wrappedObj);

   public:

      /**
       * Constructor
       */
      axi4lite_interface(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor
       */
      virtual ~axi4lite_interface();

      /**
       * Creates the AXI interface
       */
      virtual void exec();

      /**
       * Returns the name of the implemented step
       */
      virtual std::string get_kind_text() const;

};

#endif
