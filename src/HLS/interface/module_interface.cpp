/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file module_interface.cpp
 * @brief Base class for all module interfaces
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 * $Locker:  $
 * $State: Exp $
 *
*/
///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "module_interface.hpp"

///implemented interfaces
#include "minimal_interface.hpp"
#include "WB4_interface.hpp"
#include "axi4lite_interface.hpp"
#if HAVE_EXPERIMENTAL
#include "NPI_interface.hpp"
#include "FSL_interface.hpp"
#endif

#include "function_behavior.hpp"
#include "behavioral_helper.hpp"

#include "hls.hpp"
#include "hls_manager.hpp"
#include "memory.hpp"
#include "memory_symbol.hpp"

#include "structural_manager.hpp"
#include "structural_objects.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"

#include "Parameter.hpp"
#include "constant_strings.hpp"

module_interface::module_interface(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
      HLS_step(_Param, _HLSMgr, _funId)
{

}

module_interface::~module_interface()
{
}

module_interfaceRef module_interface::factory(interface_t type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   switch(type)
   {
      case MINIMAL:
         return module_interfaceRef(new minimal_interface(Param, HLSMgr, funId));
      case WB4:
         return module_interfaceRef(new WB4_interface(Param, HLSMgr, funId));
      case AXI4LITE:
         return module_interfaceRef(new axi4lite_interface(Param, HLSMgr, funId));
#if HAVE_EXPERIMENTAL
      case FSL:
         return module_interfaceRef(new FSL_interface(Param, HLSMgr, funId));
      case NPI:
         return module_interfaceRef(new NPI_interface(Param, HLSMgr, funId));
#endif
      default:
         THROW_ERROR("Not supported interface type");
   }
   return module_interfaceRef();
}

module_interfaceRef module_interface::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string type;
   if (CE_XVM(type, node)) LOAD_XVM(type, node);
   else type = "MINIMAL";

   interface_t type_t = MINIMAL;
   if (type == "MINIMAL")
      type_t = MINIMAL;
   else if (type == "WB4")
      type_t = WB4;
   else if (type == "AXI4LITE")
      type_t = AXI4LITE;
#if HAVE_EXPERIMENTAL
   else if (type == "FSL")
      type_t = FSL;
   else if (type == "NPI")
      type_t = NPI;
#endif
   else
      THROW_ERROR("Interface type \"" + type + "\" currently not supported");
   return factory(type_t, Param, HLSMgr, funId);
}

void module_interface::allocate_parameters()
{
   const FunctionBehaviorConstRef function_behavior = HLSMgr->CGetFunctionBehavior(HLS->functionId);
   const BehavioralHelperConstRef behavioral_helper = function_behavior->CGetBehavioralHelper();

   // Allocate memory for the start register.
   unsigned int topIdx = behavioral_helper->get_function_index();
   HLSMgr->Rmem->add_parameter(topIdx, topIdx, 1);
   std::string base_address = HLSMgr->Rmem->get_symbol(topIdx)->get_symbol_name();
   memory::add_memory_parameter(HLS->top, base_address, STR(HLSMgr->Rmem->get_base_address(topIdx)));

   // Allocate every parameter on chip.
   const std::list<unsigned int>& topParams = behavioral_helper->get_parameters();
   for (std::list<unsigned int>::const_iterator itr = topParams.begin(), end = topParams.end(); itr != end; ++itr)
   {
      HLSMgr->Rmem->add_parameter(behavioral_helper->get_function_index(), *itr, 0);
      base_address = HLSMgr->Rmem->get_symbol(*itr)->get_symbol_name();
      memory::add_memory_parameter(HLS->top, base_address, STR(HLSMgr->Rmem->get_base_address(*itr)));
   }

   // Allocate the return value on chip.
   const unsigned int function_return = behavioral_helper->GetFunctionReturnType(HLS->functionId);
   if (function_return)
   {
      HLSMgr->Rmem->add_parameter(behavioral_helper->get_function_index(), function_return, 0);
      base_address = HLSMgr->Rmem->get_symbol(function_return)->get_symbol_name();
      memory::add_memory_parameter(HLS->top, base_address, STR(HLSMgr->Rmem->get_base_address(function_return)));
   }
}

void module_interface::add_sign(const structural_managerRef SM, const structural_objectRef sig1, const structural_objectRef sig2, const std::string& sig_name)
{
   structural_objectRef sig = SM->add_sign(sig_name, SM->get_circ(), sig1->get_typeRef());
   SM->add_connection(sig, sig1);
   SM->add_connection(sig, sig2);
}
