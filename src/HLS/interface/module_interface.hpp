/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file module_interface.hpp
 * @brief Base class to model interfaces for high-level synthesis
 *
 * This class is a pure virtual one, that has to be specilized in order to model a particular interface chosen
 * for connecting high-level synthesis results to microprocessors or busses
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
*/
#ifndef _MODULE_INTERFACE_HPP_
#define _MODULE_INTERFACE_HPP_

///Autoheader
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "hls_step.hpp"
REF_FORWARD_DECL(module_interface);
REF_FORWARD_DECL(structural_manager);
REF_FORWARD_DECL(structural_object);

class module_interface : public HLS_step
{
   public:

      ///different types of interfaces that have been implemented
      typedef enum
      {
         MINIMAL = 0,
         WB4,
         AXI4LITE,
#if HAVE_EXPERIMENTAL
         FSL,
         NPI
#endif
      } interface_t;

   protected:

      /**
       * Allocates the in/out parameters of the module as internal registers
       */
      void allocate_parameters();

      /**
       * Connects two ports by adding a signal (i.e., wire)
       */
      void add_sign(const structural_managerRef SM, const structural_objectRef sig1, const structural_objectRef sig2, const std::string& sig_name);

   public:

      /**
       * Constructor
       */
      module_interface(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor
       */
      virtual ~module_interface();

      /**
       * Factory method
       */
      static
      module_interfaceRef factory(interface_t type, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Factory method based on the XML configuration
       */
      static
      module_interfaceRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

};

#endif
