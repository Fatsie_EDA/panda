/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file HLS_step.cpp
 * @brief Implementation of some common methods to manage HLS_step algorithms
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///header include
#include "hls_step.hpp"

#include "hls_manager.hpp"

#include "Parameter.hpp"

HLS_step::HLS_step(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   Param(_Param),
   debug_level(_Param->getOption<int>(OPT_debug_level)),
   output_level(_Param->getOption<unsigned int>(OPT_output_level)),
   HLSMgr(_HLSMgr),
   funId(_funId),
   HLS(get_HLS())
{

}

HLS_step::~HLS_step()
{

}

hlsRef HLS_step::get_HLS() const
{
   return HLSMgr->get_HLS(funId);
}
