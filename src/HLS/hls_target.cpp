/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file hls_target.cpp
 * @brief Implementation of some methods to manage the target for the HLS
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Header include
#include "hls_target.hpp"

/// ----------- Resource Library ----------- ///
/// Resource Library Parsing
#include "parse_technology.hpp"
/// Resource Library Datastructure
#include "technology_manager.hpp"
#include "technology_builtin.hpp"
/// ----------- Target Device ----------- ///
#include "target_device.hpp"
#include "target_technology.hpp"

#include "fileIO.hpp"
#include "xml_dom_parser.hpp"
#include "xml_helper.hpp"
#include "polixml.hpp"
#include "Parameter.hpp"

HLS_target::HLS_target(const ParameterConstRef _Param, const technology_managerRef _TM, const target_deviceRef _target) :
   target_manager(_Param, _TM, _target)
{
   unsigned int debug_level = Param->getOption<unsigned int>(OPT_debug_level);
   if (Param->isOption(OPT_clock_period))
   {
      double clock_period_value = Param->getOption<double>(OPT_clock_period);
      device->set_parameter("clock_period", clock_period_value);
   }
   unsigned int output_level = Param->getOption<unsigned int>(OPT_output_level);
   PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Target technology = " + target->get_string_type());

   /// this adds predefined resources
   add_builtin_resources(TM, Param, device);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Built-in resources added");

   // if configuration file is given, it is parsed to check for technology information
   if (Param->isOption(OPT_xml_input_configuration))
   {
      std::string fn = Param->getOption<std::string>(OPT_xml_input_configuration);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "checking for technology information in the configuration file...");
      try
      {
         fileIO_istreamRef sname = fileIO_istream_open(fn);
         xml_dom_parser parser;
         parser.parse_stream(*sname);
         if (parser)
         {
            device->xload(device, fn, parser.get_document()->get_root_node());
         }
      }
      catch (const char * msg)
      {
         THROW_ERROR("Error during technology file parsing: " + std::string(msg));
      }
      catch (const std::string & msg)
      {
         THROW_ERROR("Error during technology file parsing: " + msg);
      }
      catch (const std::exception& ex)
      {
         THROW_ERROR("Error during technology file parsing: " + std::string(ex.what()));
      }
      catch ( ... )
      {
         THROW_ERROR("Error during technology file parsing");
      }
      PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, " ==== XML configuration file parsed for technology information ====");
   }

   /// load specific device information
   device->load_devices(device);

   // if technology file is given, it is parsed
   if (Param->isOption("techfile"))
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "parsing the technology file...");
      read_technology_File(Param->getOption<std::string>("techfile").c_str(), TM, Param, device);
      PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "==== Technology file parsed ====");
   }
   THROW_ASSERT(TM, "Technology datastructure not defined");
   // ---------- Save XML file ------------ //
   if (debug_level >= DEBUG_LEVEL_PEDANTIC)
      write_xml_technology_File("technology.out.XML", TM, device->get_type());
}

HLS_target::~HLS_target()
{

}

HLS_targetRef HLS_target::create_target(const ParameterRef Param)
{
   technology_managerRef TM = technology_managerRef(new technology_manager(Param));
   if (Param->isOption(OPT_xml_input_configuration))
   {
      try
      {
         std::string fn = Param->getOption<std::string>(OPT_xml_input_configuration);
         fileIO_istreamRef sname = fileIO_istream_open(fn);
         xml_dom_parser parser;
         //parser.set_validate();
         parser.parse_stream(*sname);
         if (parser)
         {
            //Walk the tree:
            const xml_element* node = parser.get_document()->get_root_node(); //deleted by DomParser.

            const xml_node::node_list list = node->get_children();
            for (xml_node::node_list::const_iterator iter = list.begin(); iter != list.end(); ++iter)
            {
               const xml_element* Enode = GetPointer<const xml_element>(*iter);
               if(!Enode) continue;
               if(Enode->get_name() == "device")
               {
                  std::string type = "FPGA";
                  if (CE_XVM(type, Enode)) LOAD_XVM(type, Enode);
                  target_device::type_t type_device = target_device::FPGA;
                  if (type == "IC") type_device = target_device::IC;
                  Param->setOption(OPT_target_device_type, type_device);

                  target_deviceRef target = target_device::create_device(type_device, Param, TM);
                  target->xload(target, fn, node);
                  return HLS_targetRef(new HLS_target(Param, TM, target));
               }
            }
         }
      }
      catch (const char * msg)
      {
         THROW_ERROR("Error during technology file parsing: " + std::string(msg));
      }
      catch (const std::string & msg)
      {
         THROW_ERROR("Error during technology file parsing: " + msg);
      }
      catch (const std::exception& ex)
      {
         THROW_ERROR("Error during technology file parsing: " + std::string(ex.what()));
      }
      catch ( ... )
      {
         THROW_ERROR("Error during technology file parsing");
      }
   }
   unsigned int type_device = Param->getOption<unsigned int>(OPT_target_device_type);
   target_deviceRef target = target_device::create_device(static_cast<target_device::type_t>(type_device), Param, TM);
   return HLS_targetRef(new HLS_target(Param, TM, target));
}
