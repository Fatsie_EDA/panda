/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file HLStestbench.cpp
 * @brief hls testbenches automatic generation
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Minutoli <mminutoli@gmail.com>
 * @author Manuel Beniani <manuel.beniani@gmail.com>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Autoheader include

#include "config_PACKAGE_BUGREPORT.hpp"
#include "config_PACKAGE_NAME.hpp"
#include "config_PACKAGE_VERSION.hpp"

#include "HLStestbench.hpp"
#include "HLStestbench-impl.hpp"

#include "tree_manager.hpp"
#include "tree_helper.hpp"
#include "tree_reindex.hpp"
#include "call_graph_manager.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"
#include "var_pp_functor.hpp"
#include "hls_manager.hpp"
#include "hls_target.hpp"

#include "memory.hpp"
#include "memory_allocation.hpp"

#include "fu_binding.hpp"
#include "module_interface.hpp"
#include "structural_objects.hpp"

#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"
#include "simple_indent.hpp"
#include "utility.hpp"
#include "fileIO.hpp"
#include "polixml.hpp"
#include "xml_dom_parser.hpp"

#include "prettyPrintVertex.hpp"
#include "boost/filesystem.hpp"
#include "xml_helper.hpp"

#include "HDL_manager.hpp"
#include "language_writer.hpp"

#include "technology_builtin.hpp"
#include "technology_wishbone.hpp"

#include "gcc_wrapper.hpp"

/// ----------- C backend ----------- ///
#include "c_backend_step_factory.hpp"
#include "hls_c_backend_information.hpp"
#include "design_flow_manager.hpp"

#include <fstream>
#include <iostream>

#define TESTBENCH_COPYING3_SHORT_NROW 15
const char* TESTBENCH_COPYING3_SHORT[TESTBENCH_COPYING3_SHORT_NROW] = {
   "************************************************************************\n",
   "This hardware description is free; you can redistribute it and/or modify\n",
   "it under the terms of the GNU General Public License as published by\n",
   "the Free Software Foundation; either version 3, or (at your option)\n",
   "any later version.\n",
   "\n",
   "This hardware description is distributed in the hope that it will be useful,\n",
   "but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY\n",
   "or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License\n",
   "for more details.\n",
   "\n",
   "You should have received a copy of the GNU General Public License\n",
   "along with the PandA framework; see the files COPYING\n",
   "If not, see <http://www.gnu.org/licenses/>.\n",
   "************************************************************************\n\n"
};


/*
 * HLStestbench Implementation.
 */
HLStestbench * HLStestbench::Create(const ParameterConstRef _Param, const HLS_managerRef _AppM)
{
   HLStestbench * TB;
   switch (_Param->getOption<int>(OPT_interface_type))
   {
      case module_interface::MINIMAL:
         TB = new MinimalInterfaceTestbench(_Param, _AppM);
         break;
      case module_interface::WB4:
         TB = new WishboneInterfaceTestbench(_Param, _AppM);
         break;
      default:
         THROW_ERROR("Missing testbench implementation for the requested interface.");
   }
   return TB;
}


HLStestbench::HLStestbench(const ParameterConstRef _Param, const HLS_managerRef _AppM) :
      debug_level(_Param->getOption<int>(OPT_debug_level)),
      Param(_Param),
      AppM(_AppM)
{
   output_directory = Param->getOption<std::string>(OPT_output_directory) + "/simulation/";
   if (!boost::filesystem::exists(output_directory)) boost::filesystem::create_directories(output_directory);
}


HLStestbench::~HLStestbench()
{

}


std::string HLStestbench::generate(const std::string & filename_bench, const structural_objectRef &cir,
                                   const double target_period) const
{
   std::string filename;
   if(Param->getOption<std::string>(OPT_simulator) == "VERILATOR")
      filename = this->verilator_testbench(filename_bench, cir, target_period);
   else
      filename = this->create_HDL_testbench(filename_bench, cir, target_period, false);
   return filename;
}


unsigned int HLStestbench::parse_test_vectors(const std::string& tb_input_xml, std::vector<std::map<std::string, std::string> >& test_vectors)
{
   const tree_managerRef TreeM = AppM->get_tree_manager();
   const unsigned int function_id = AppM->CGetCallGraphManager()->GetRootFunction();
   const BehavioralHelperConstRef behavioral_helper = AppM->CGetFunctionBehavior(function_id)->CGetBehavioralHelper();

   if (!boost::filesystem::exists(tb_input_xml))
   {
      THROW_WARNING("XML file \"" + tb_input_xml + "\" cannot be opened, creating a stub with random values");
      xml_document document;
      xml_element* nodeRoot = document.create_root_node("function");

      xml_element* node = nodeRoot->add_child_element("testbench");

      std::list<unsigned int> parameters = behavioral_helper->get_parameters();
      for(std::list<unsigned int>::iterator p = parameters.begin(); p != parameters.end(); p++)
      {
         if (behavioral_helper->is_a_pointer(*p)) continue;
         std::string param = behavioral_helper->PrintVariable(*p);

         long long int value = (rand() % 20);
         node->set_attribute(param, boost::lexical_cast<std::string>(value));
      }

      document.write_to_file_formatted(tb_input_xml);
   }
   try
   {
      fileIO_istreamRef sname = fileIO_istream_open(tb_input_xml);
      xml_dom_parser parser;
      //parser.set_validate();
      parser.parse_stream(*sname);
      if (parser)
      {
         //Walk the tree:
         const xml_element* node = parser.get_document()->get_root_node(); //deleted by DomParser.
         const xml_node::node_list list = node->get_children();
         for (xml_node::node_list::const_iterator iter = list.begin(); iter != list.end(); ++iter)
         {
            const xml_element* Enode = GetPointer<const xml_element>(*iter);

            if(!Enode || Enode->get_name() != "testbench") continue;

            std::map<std::string, std::string> test_vector;

            std::list<unsigned int> parameters = behavioral_helper->get_parameters();
            for(std::list<unsigned int>::iterator p = parameters.begin(); p != parameters.end(); p++)
            {
               std::string param = behavioral_helper->PrintVariable(*p);
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Parameter: " + param + (behavioral_helper->is_a_pointer(*p) ? " (memory access)" : " (input value)"));
               if ((Enode)->get_attribute(param))
               {
                  test_vector[param] = boost::lexical_cast<std::string>((Enode)->get_attribute(param)->get_value());
               }
               else if (!behavioral_helper->is_a_pointer(*p))
               {
                  THROW_ERROR("Missing input value for parameter: " + param);
               }
            }
            test_vectors.push_back(test_vector);
         }

         return static_cast<unsigned int>(test_vectors.size());
      }
   }
   catch (const char * msg)
   {
      std::cerr << msg << std::endl;
   }
   catch (const std::string & msg)
   {
      std::cerr << msg << std::endl;
   }
   catch (const std::exception& ex)
   {
      std::cout << "Exception caught: " << ex.what() << std::endl;
   }
   catch ( ... )
   {
      std::cerr << "unknown exception" << std::endl;
   }
   THROW_ERROR("Error during parsing of testbench file " + tb_input_xml);
   return 0;
}


std::string HLStestbench::print_var_init(const tree_managerConstRef TreeM, unsigned int var, const memoryRef mem)
{
   std::vector<std::string> init_els;
   const tree_nodeRef& tn = TreeM->get_tree_node_const(var);
   tree_nodeRef init_node;
   var_decl * vd = GetPointer<var_decl>(tn);
   if(vd && vd->init)
      init_node = GET_NODE(vd->init);

   if (init_node && (!GetPointer<constructor>(init_node) || GetPointer<constructor>(init_node)->list_of_idx_valu.size()))
   {
      fu_binding::write_init(TreeM, tn, init_node, init_els, mem);
   }
   else if(GetPointer<string_cst>(tn) || GetPointer<integer_cst>(tn) || GetPointer<real_cst>(tn))
   {
      fu_binding::write_init(TreeM, tn, tn, init_els, mem);
   }
   else if (GetPointer<gimple_call>(tn)) {}
   else
   {
      if (tree_helper::is_an_array(TreeM, var))
      {
         unsigned int size_of_data;
         unsigned int type_index;
         std::vector<unsigned int> dims;
         tree_helper::get_type_node(tn, type_index);
         tree_helper::get_array_dimensions(TreeM, type_index, dims, size_of_data);
         unsigned int num_elements=1;
         std::string value;
         for(std::vector<unsigned int>::const_iterator it = dims.begin(); it != dims.end(); ++it)
            num_elements *= *it;
         for (unsigned int l = 0; l < num_elements; l++)
         {
            value = "";
            for (unsigned int i = 0; i < size_of_data; i++)
               value += "0";
            init_els.push_back(value);
         }
      }
      else
      {
          unsigned int size_of_data = tree_helper::size(TreeM, var);
         std::string value;
         for (unsigned int i = 0; i < size_of_data; i++)
            value += "0";
         init_els.push_back(value);
      }
   }
   std::string init;
   for (unsigned int l = 0; l < init_els.size(); l++)
   {
      if (l) init += ",";
      init += init_els[l];
   }
   //std::cerr << "variable: " << behavioral_helper->PrintVariable(var) << std::endl;
   //std::cerr << "   init: " << init << std::endl;

   return init;
}

std::string HLStestbench::creating_c_testbench(std::map<std::string, std::string>& test_vector, const std::string& input_file, bool is_first_vector)
{
   const DesignFlowManagerRef design_flow_manager(new DesignFlowManager(Param));
   CBackendStepFactory c_backend_step_factory = CBackendStepFactory(design_flow_manager, AppM, Param);
   const DesignFlowStepRef design_flow_step = c_backend_step_factory.CreateCBackendStep(CBackend::CB_HLS, output_directory + "values.c", CBackendInformationConstRef(new HLSCBackendInformation(input_file, test_vector, is_first_vector)));
   design_flow_step->Exec();
   return output_directory + "values.c";
}

void HLStestbench::generate_input_file(const std::string& fileName)
{
   std::unordered_set<std::string> file_sources;
   file_sources.insert(fileName);
   const GccWrapperConstRef gcc_wrapper(new GccWrapper(Param, Param->getOption<CompilerTarget>(OPT_default_compiler), GccWrapper_OptimizationSet::O0));
   gcc_wrapper->CreateExecutable(file_sources, output_directory + "test", std::string(" -D") + SIMULATION_DEFINE + " -ffloat-store ");

   ///executing the test to generate inputs and exected outputs values
   const std::string command = output_directory + "test";
   int ret = PandaSystem(Param, command);
   if(IsError(ret))
   {
      THROW_ERROR("Error in generating the tests");
   }

}


void HLStestbench::generate_C_testbench()
{
   PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "  . Generation of the testbenches");

   std::vector<std::map<std::string, std::string> > test_vectors;

   std::string tb_input_xml = Param->getOption<std::string>("tb_input_xml");
   unsigned int vectors = parse_test_vectors(tb_input_xml, test_vectors);
   if (vectors == 0)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "  No input vectors to be tested");
      return;
   }
   else
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "  Number of input vectors: " + boost::lexical_cast<std::string>(vectors));
   }

   std::string input_file = output_directory + "values.txt";
   if (boost::filesystem::exists(input_file)) boost::filesystem::remove_all(input_file);

   for (unsigned int l = 0; l < test_vectors.size(); l++)
   {
      ///creating C file for generating the values
      std::string fileName = creating_c_testbench(test_vectors[l], input_file, l==0);

      ///executing the test file and getting the testbench values
      generate_input_file(fileName);
   }

   ///TODO: to be fixed
   const_cast<Parameter *>(Param.get())->setOption("tb_inputs", input_file);
}

std::string HLStestbench::verilator_testbench(const std::string& filename_bench, const structural_objectRef &cir,
                                              const double target_period) const
{
   if (!Param->getOption<bool>(OPT_generate_testbench) or !Param->isOption("tb_inputs"))
      return "";
   std::string simulation_values_path = Param->getOption<std::string>("tb_inputs");

   PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "  . Generation of the Verilator testbenches");

   if (!boost::filesystem::exists(simulation_values_path)) THROW_ERROR("Error in generating verilator testbenches, values file missing!");

   std::string fileName = this->write_verilator_testbench(filename_bench, cir, simulation_values_path, target_period);

   PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "  . End of the Verilator testbenches");

   return fileName;
}


std::string
HLStestbench::write_verilator_testbench(
    const std::string& filename_bench,
    const structural_objectRef &cir,
    const std::string& input_file,
    const double target_period) const
{
   // Generate the testbench
   std::string testbenchFilename = output_directory + filename_bench +"_tb.v";
   std::ofstream hdl_file(testbenchFilename.c_str(), std::ios::out);

   language_writerRef writer =
         language_writer::create_writer(language_writer::VERILOG, debug_level);
   const tree_managerRef TreeM = AppM->get_tree_manager();

   this->write_underling_testbench(hdl_file, writer, cir, target_period,
                                   input_file, false, false, TreeM);
   std::ostringstream os;
   simple_indent PP('{', '}', 3);


   // Creating output file
   std::string fileName = output_directory + filename_bench +"_main.cpp";
   std::ofstream fileOut(fileName.c_str(), std::ios::out);

   std::string top_fname = cir->get_typeRef()->id_type;
   PP(os, "#include <string>\n");
   PP(os, "#include <verilated.h>\n");
   PP(os, "#include \"V" + top_fname + "_tb.h\"\n");
   PP(os, "\n");
   PP(os, "#if VM_TRACE\n");
   PP(os, "# include <verilated_vcd_c.h>\n");
   PP(os, "#endif\n");
   PP(os, "\n");
   PP(os, "\n");
   PP(os,"#define SIMULATION_MAX "+ STR(Param->getOption<int>(OPT_max_sim_cycles)) + "\n\n");
   PP(os,"static const double CLOCK_PERIOD ="+boost::lexical_cast<std::string>(target_period)+";\n");
   PP(os,"static const double HALF_CLOCK_PERIOD = CLOCK_PERIOD/2;\n");
   PP(os, "\n");
   PP(os, "double main_time = 0;\n");
   PP(os, "\n");
   PP(os, "double sc_time_stamp ()  {return main_time;}\n");
   PP(os, "\n");
   PP(os, "int main (int argc, char **argv, char **env)\n");
   PP(os, "{\n");
   PP(os, "   V" + top_fname + "_tb *top;\n");
   PP(os, "\n");
   PP(os, "   std::string vcd_output_filename = \"" + output_directory + "test.vcd\";\n");
   PP(os, "   Verilated::commandArgs(argc, argv);\n");
   PP(os, "   Verilated::debug(0);\n");
   PP(os, "   top = new V" + top_fname + "_tb;\n");
   PP(os, "   \n");
   PP(os, "   \n");
   PP(os, "   #if VM_TRACE\n");
   PP(os, "   Verilated::traceEverOn(true);\n");
   PP(os, "   VerilatedVcdC* tfp = new VerilatedVcdC;\n");
   PP(os, "   #endif\n");
   PP(os, "   main_time=0;\n");
   PP(os, "#if VM_TRACE\n");
   PP(os, "   top->trace (tfp, 99);\n");
   PP(os, "   tfp->open (vcd_output_filename.c_str());\n");
   PP(os, "#endif\n");
   PP(os, "   int cycleCounter = 0;\n");
   PP(os, "   top->clock = 0;\n");
   PP(os, "   while (!Verilated::gotFinish() && cycleCounter < SIMULATION_MAX)\n");
   PP(os, "   {\n");
   PP(os, "     top->clock = top->clock == 0 ? 1 : 0;\n");
   PP(os, "     top->eval();\n");
   PP(os, "#if VM_TRACE\n");
   PP(os, "     if (tfp) tfp->dump (main_time);\n");
   PP(os, "#endif\n");
   PP(os, "     main_time += HALF_CLOCK_PERIOD;\n");
   PP(os, "     cycleCounter++;\n");
   PP(os, "   }\n");
   PP(os, "#if VM_TRACE\n");
   PP(os, "   if (tfp) tfp->dump (main_time);\n");
   PP(os, "#endif\n");
   PP(os, "   top->final();\n");
   PP(os, "   #if VM_TRACE\n");
   PP(os, "   tfp->close();\n");
   PP(os, "   delete tfp;\n");
   PP(os, "   #endif\n");
   PP(os, "   delete top;\n");
   PP(os, "   exit(0L);\n");
   PP(os, "}");

   fileOut << os.str() << std::endl;
   fileOut.close();

   return fileName;
}

std::string HLStestbench::create_HDL_testbench(const std::string& filename_bench, const structural_objectRef &cir, const double target_period, bool xilinx_isim) const
{
   if (!Param->getOption<bool>(OPT_generate_testbench) or !Param->isOption("tb_inputs"))
      return "";
   const tree_managerRef TreeM = AppM->get_tree_manager();

   language_writerRef writer = language_writer::create_writer(language_writer::VERILOG, debug_level);
   std::string simulation_values_path = Param->getOption<std::string>("tb_inputs");
   bool generate_vcd_output = Param->isOption(OPT_generate_vcd) && Param->getOption<bool>(OPT_generate_vcd);

   std::string file_name = output_directory + filename_bench + writer->get_extension();
   std::ofstream hdl_file(file_name.c_str());

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  writing testbench");
   writer->write_comment(hdl_file, std::string("File automatically generated by: ") + PACKAGE_NAME + " framework version=" + PACKAGE_VERSION + "\n");
   writer->write_comment(hdl_file, std::string("Send any bug to: ") + PACKAGE_BUGREPORT + "\n");
   for(unsigned int row=0; row < TESTBENCH_COPYING3_SHORT_NROW; ++row)
   {
      writer->write_comment(hdl_file, std::string(TESTBENCH_COPYING3_SHORT[row]));
   }
   //write testbech for simulation
   this->write_hdl_testbench(hdl_file, writer, cir, target_period, simulation_values_path, generate_vcd_output, xilinx_isim, TreeM);
   hdl_file.close();

   return file_name;
}


static void write_clock_process(std::ostream&, language_writerRef );
void
HLStestbench::write_hdl_testbench(
    std::ostream& hdl_file, language_writerRef writer,
    const structural_objectRef &cir, double target_period,
    std::string simulation_values_path, bool generate_vcd_output,
    bool xilinx_isim, const tree_managerConstRef TreeM) const
{
   this->write_underling_testbench(hdl_file, writer, cir, target_period, simulation_values_path,
                                   generate_vcd_output, xilinx_isim, TreeM);

   writer->write(hdl_file, "module " + cir->get_id() + "_tb_top;\n");
   writer->write(hdl_file, "reg " + STR(CLOCK_PORT_NAME) + ";\n");
   writer->write(hdl_file, "initial\nbegin\n");
   writer->write(hdl_file, STR(CLOCK_PORT_NAME) + " = 1;\n");
   writer->write(hdl_file, "end\n");

   write_clock_process(hdl_file, writer);

   writer->write(hdl_file, cir->get_id() + "_tb DUT(." + STR(CLOCK_PORT_NAME)
                 + "(" + STR(CLOCK_PORT_NAME) + "));\n");
   writer->write(hdl_file, "endmodule\n");
}


/*
 * Local helper function used to factorize common stuff between
 * Wishbone and Minimal interface. Anyway this can be used in specialization.
 * Maybe this is sympthom that a proper formalization of the testbench structure is
 * needed.
 */
static void write_clock_process(std::ostream& hdl_file, language_writerRef writer)
{
   /// write clock switching operation
   writer->write_comment(hdl_file, "Clock switching: 1 cycle every CLOCK_PERIOD seconds\n");
   writer->write(hdl_file, "always # `HALF_CLOCK_PERIOD clock = !clock;\n\n");
}


static void write_hdl_testbench_prolog(std::ostream& hdl_file, language_writerRef writer, double target_period, const ParameterConstRef Param)
{
   writer->write(hdl_file, "`timescale 1ns / 1ps\n");
   writer->write_comment(hdl_file, "CONSTANTS DECLARATION\n");
   writer->write(hdl_file, "`define EOF 32'hFFFF_FFFF\n`define NULL 0\n`define MAX_COMMENT_LENGTH 1000\n`define SIMULATION_LENGTH "+ STR(Param->getOption<int>(OPT_max_sim_cycles)) + "\n\n");
   writer->write(hdl_file, "`define HALF_CLOCK_PERIOD "+boost::lexical_cast<std::string>(target_period/2)+"\n\n");
   writer->write(hdl_file, "`define CLOCK_PERIOD (2*`HALF_CLOCK_PERIOD)\n\n");
   writer->write(hdl_file, "`define MEM_DELAY_READ 2\n\n");
   writer->write(hdl_file, "`define MEM_DELAY_WRITE 1\n\n");
}


static void write_module_begin(std::ostream& hdl_file, language_writerRef writer,
                               const structural_objectRef &cir)
{
   writer->write_comment(hdl_file, "MODULE DECLARATION\n");
   writer->write(hdl_file, "module " + cir->get_id() + "_tb("
                 + STR(CLOCK_PORT_NAME) + ");\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
}


static void write_module_end(std::ostream& hdl_file, language_writerRef writer)
{
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "endmodule\n\n");
}


static void write_module_instantiation(std::ostream &hdl_file, language_writerRef writer,
                                       const structural_objectRef &cir, bool xilinx_isim)
{
   module *mod = GetPointer<module>(cir);

   /// write module instantiation and ports binding
   writer->write_comment(hdl_file, "MODULE INSTANTIATION AND PORTS BINDING\n");
   writer->write_module_instance_begin(hdl_file, cir, !xilinx_isim);
   std::string prefix = "";
   if (mod->get_in_port_size())
   {
      for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
      {
         if(i == 0)
            prefix = ".";
         else
            prefix = ", .";

         writer->write(hdl_file, prefix + HDL_manager::convert_to_identifier(writer.get(), mod->get_in_port(i)->get_id()) + 
            "(" + HDL_manager::convert_to_identifier(writer.get(), mod->get_in_port(i)->get_id()) + ")");
      }
   }
   if (mod->get_out_port_size())
   {
      for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
      {
         writer->write(hdl_file, prefix + HDL_manager::convert_to_identifier(writer.get(), mod->get_out_port(i)->get_id()) +
            "(" + HDL_manager::convert_to_identifier(writer.get(), mod->get_out_port(i)->get_id()) + ")");
      }
   }

   writer->write_module_instance_end(hdl_file, cir);
}


static void write_input_signal_declaration(std::ostream &hdl_file, language_writerRef writer,
                                           const structural_objectRef &cir,
                                           const tree_managerConstRef TreeM, bool &with_memory)
{
   module *mod = GetPointer<module>(cir);

   /// write input signals declaration
   if (mod->get_in_port_size())
   {
      writer->write_comment(hdl_file, "INPUT SIGNALS\n");
      for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
      {
         const structural_objectRef& port_obj = mod->get_in_port(i);
         if(GetPointer<port_o>(port_obj)->get_is_memory())
         {
            if (GetPointer<port_o>(port_obj)->get_id().find("M") == 0)
               with_memory=true;
            writer->write(hdl_file, "wire ");
         }
         else if (GetPointer<port_o>(port_obj)->get_id() == CLOCK_PORT_NAME)
            writer->write(hdl_file, "input ");
         else
            writer->write(hdl_file, "reg ");

         writer->write(hdl_file, writer->type_converter(port_obj->get_typeRef()) + writer->type_converter_size(port_obj));
         writer->write(hdl_file, HDL_manager::convert_to_identifier(writer.get(), mod->get_in_port(i)->get_id()) + ";\n");
         if (port_obj->get_typeRef()->treenode > 0 && tree_helper::is_a_pointer(TreeM, port_obj->get_typeRef()->treenode))
         {
            unsigned int pt_type_index = tree_helper::get_pointed_type(TreeM, tree_helper::get_type_index(TreeM,port_obj->get_typeRef()->treenode));
            tree_nodeRef pt_node = TreeM->get_tree_node_const(pt_type_index);
            if(GetPointer<array_type>(pt_node))
            {
               while(GetPointer<array_type>(pt_node))
               {
                  pt_type_index = GET_INDEX_NODE(GetPointer<array_type>(pt_node)->elts);
                  pt_node = GET_NODE(GetPointer<array_type>(pt_node)->elts);
               }
            }
            long long int bitsize = tree_helper::size(TreeM, pt_type_index);

            writer->write(hdl_file,
               "reg [" + STR(bitsize -1)
               + ":0] ex_" + port_obj->get_id() + ";\n");
         }
      }
      writer->write(hdl_file, "\n");
   }
   writer->write(hdl_file, "reg start_next_sim;\n");
   writer->write(hdl_file, "reg start_port_next;\n");
}


static void write_output_signal_declaration(std::ostream &hdl_file, language_writerRef writer,
                                            const structural_objectRef &cir)
{
   module * mod = GetPointer<module>(cir);

   /// write output signals declaration
   if (mod->get_out_port_size())
   {
      writer->write_comment(hdl_file, "OUTPUT SIGNALS\n");
      for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
      {
         writer->write(hdl_file, "wire " + writer->type_converter(mod->get_out_port(i)->get_typeRef()) + writer->type_converter_size(mod->get_out_port(i)));
         writer->write(hdl_file, HDL_manager::convert_to_identifier(writer.get(), mod->get_out_port(i)->get_id()) + ";\n");
         if (mod->get_out_port(i)->get_id() == RETURN_PORT_NAME)
         {
            writer->write(hdl_file, "reg " + writer->type_converter(mod->get_out_port(i)->get_typeRef()) + writer->type_converter_size(mod->get_out_port(i)));
            writer->write(hdl_file, "ex_" + HDL_manager::convert_to_identifier(writer.get(), mod->get_out_port(i)->get_id()) + ";\n");
            writer->write(hdl_file, "reg " + writer->type_converter(mod->get_out_port(i)->get_typeRef()) + writer->type_converter_size(mod->get_out_port(i)));
            writer->write(hdl_file, "registered_" + HDL_manager::convert_to_identifier(writer.get(), mod->get_out_port(i)->get_id()) + ";\n");
         }
      }
      writer->write(hdl_file, "\n");
   }
}


static void write_auxiliary_signal_declaration(std::ostream &hdl_file, language_writerRef writer, const structural_objectRef cir)
{
   writer->write(hdl_file, "parameter MEMSIZE = 524288");

   module * mod = GetPointer<module>(cir);
   const NP_functionalityRef &np = mod->get_NP_functionality();
   if (np)
   {
      ///writing memory-related parameters
      if (mod->is_parameter(MEMORY_PARAMETER))
      {
         std::string memory_str = mod->get_parameter(MEMORY_PARAMETER);
         std::vector<std::string> mem_tag = convert_string_to_vector<std::string>(memory_str, ";");
         for(unsigned int i = 0; i < mem_tag.size(); i++)
         {
            std::vector<std::string> mem_add = convert_string_to_vector<std::string>(mem_tag[i], "=");
            THROW_ASSERT(mem_add.size() == 2, "malformed address");
            writer->write(hdl_file, ", ");
            std::string name = mem_add[0];
            std::string value = mem_add[1];
            if (value.find("\"\"") != std::string::npos)
            {
               boost::replace_all(value, "\"\"", "\"");
            }
            else if (value.find("\"") != std::string::npos)
            {
               boost::replace_all(value, "\"", "");
               value = boost::lexical_cast<std::string>(value.size()) + "'b" + value;
            }
            writer->write(hdl_file, name + "=" + value);
         }
      }
   }
   writer->write(hdl_file, ";\n");

   writer->write_comment(hdl_file, "AUXILIARY VARIABLES DECLARATION\n");
   writer->write(hdl_file, "time startTime, endTime, sim_time;\n");
   writer->write(hdl_file, "integer res_file, file, _r_, _n_, _i_, _addr_i_;\n");
   writer->write(hdl_file, "integer _ch_;\n");
   writer->write(hdl_file, "reg compare_outputs, successs; // Flag: True if input vector specifies expected outputs\n");
   writer->write(hdl_file, "reg [8*`MAX_COMMENT_LENGTH:0] line; // Comment line read from file\n\n");

   writer->write(hdl_file, "reg [31:0] addr, base_addr;\n");
   writer->write(hdl_file, "reg signed [7:0] mem [0:MEMSIZE-1];\n\n");
   writer->write(hdl_file, "reg [3:0] state, next_state;\n");
   writer->write(hdl_file, "reg start_results_comparison;\n");
}


static void begin_initial_block(std::ostream &hdl_file, language_writerRef writer)
{
   writer->write(hdl_file, "\n");
   writer->write_comment(hdl_file, "Operation to be executed just one time\n");
   writer->write(hdl_file, "initial\nbegin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
}


static void end_initial_block(std::ostream &hdl_file, language_writerRef writer)
{
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
}


static void open_value_file(std::ostream &hdl_file, language_writerRef writer,
                            std::string input_values_filename)
{
   writer->write_comment(hdl_file, "OPEN FILE WITH VALUES FOR SIMULATION\n");
   writer->write(hdl_file, "file = $fopen(\"" + input_values_filename + "\",\"r\");\n");
   writer->write_comment(hdl_file, "Error in file open\n");
   writer->write(hdl_file, "if (file == `NULL)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"ERROR - Error opening the file\");\n");
   writer->write(hdl_file, "$finish;");
   writer->write_comment(hdl_file, "Terminate\n");
   writer->write(hdl_file, "");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
}


static void open_result_file(std::ostream &hdl_file, language_writerRef writer,
                             std::string result_file)
{
   writer->write_comment(hdl_file, "OPEN FILE WHERE results will be written\n");
   writer->write(hdl_file, "res_file = $fopen(\"" + result_file + "\",\"w\");\n\n");
   writer->write_comment(hdl_file, "Error in file open\n");
   writer->write(hdl_file, "if (res_file == `NULL)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"ERROR - Error opening the res_file\");\n");
   writer->write(hdl_file, "$fclose(file);\n");
   writer->write(hdl_file, "$finish;");
   writer->write_comment(hdl_file, "Terminate\n");
   writer->write(hdl_file, "");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
}


static void initialize_auxiliary_variables(std::ostream &hdl_file, language_writerRef writer)
{
   writer->write_comment(hdl_file, "Variables initialization\n");
   writer->write(hdl_file, "sim_time = 0;\n");
   writer->write(hdl_file, "startTime = 0;\n");
   writer->write(hdl_file, "endTime = 0;\n");
   writer->write(hdl_file, "_ch_ = 0;\n");
   writer->write(hdl_file, "_n_ = 0;\n");
   writer->write(hdl_file, "_r_ = 0;\n");
   writer->write(hdl_file, "line = 0;\n");
   writer->write(hdl_file, "_i_ = 0;\n");
   writer->write(hdl_file, "_addr_i_ = 0;\n");
   writer->write(hdl_file, "compare_outputs = 0;\n");
   writer->write(hdl_file, "start_next_sim = 0;\n");
   writer->write(hdl_file, "state = 0;\n");
   writer->write(hdl_file, "next_state = 1;\n");
   writer->write(hdl_file, "successs = 1;\n");
}


static void initialize_input_signals(std::ostream &hdl_file, language_writerRef writer,
                                     const structural_objectRef &cir,
                                     const tree_managerConstRef TreeM)
{
   module * mod = GetPointer<module>(cir);

   if (mod->get_in_port_size())
   {
      for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
      {
         const structural_objectRef& port_obj = mod->get_in_port(i);
         if(GetPointer<port_o>(port_obj)->get_is_memory() || WB_ACKIM_PORT_NAME == port_obj->get_id()) continue;
         if(CLOCK_PORT_NAME != port_obj->get_id())
            writer->write(hdl_file, HDL_manager::convert_to_identifier(writer.get(), port_obj->get_id()) + " = 0;\n");
         if (port_obj->get_typeRef()->treenode > 0 && tree_helper::is_a_pointer(TreeM, port_obj->get_typeRef()->treenode))
         {
            writer->write(hdl_file, "ex_" + port_obj->get_id() + " = 0;\n");
         }
      }
      writer->write(hdl_file, "\n");
   }
}


static void testbench_controller_machine(std::ostream &hdl_file,
                                         language_writerRef writer,
                                         bool first_valid_input)
{
   writer->write(hdl_file, "always @(*)\n");
   writer->write(hdl_file, "  begin\n");
   writer->write(hdl_file, "     start_results_comparison = 0;\n");
   writer->write(hdl_file, "     reset = 1;\n");
   writer->write(hdl_file, "     start_port_next = 0;\n");
   writer->write(hdl_file, "     start_next_sim = 0;\n");
   writer->write(hdl_file, "     case (state)\n");
   writer->write(hdl_file, "       0:\n");
   writer->write(hdl_file, "         begin\n");
   writer->write(hdl_file, "            reset = 0;\n");
   writer->write(hdl_file, "            next_state = 1;\n");
   writer->write(hdl_file, "         end\n");
   writer->write(hdl_file, "       1:\n");
   writer->write(hdl_file, "         begin\n");
   writer->write(hdl_file, "            reset = 0;\n");
   writer->write(hdl_file, "            next_state = 2;\n");
   writer->write(hdl_file, "         end\n");
   writer->write(hdl_file, "       2:\n");
   writer->write(hdl_file, "         begin\n");
   writer->write(hdl_file, "            start_port_next = 1;\n");
   writer->write(hdl_file, "            if (done_port == 1'b1)\n");
   writer->write(hdl_file, "              begin\n");
   writer->write(hdl_file, "                next_state = 4;\n");
   writer->write(hdl_file, "              end\n");
   writer->write(hdl_file, "            else\n");
   writer->write(hdl_file, "              next_state = 3;\n");
   writer->write(hdl_file, "         end\n");
   writer->write(hdl_file, "       3:\n");
   writer->write(hdl_file, "         if (done_port == 1'b1)\n");
   writer->write(hdl_file, "           begin\n");
   writer->write(hdl_file, "              next_state = 4;\n");
   writer->write(hdl_file, "           end\n");
   writer->write(hdl_file, "         else\n");
   writer->write(hdl_file, "           next_state = 3;\n");
   writer->write(hdl_file, "       4:\n");
   writer->write(hdl_file, "         begin\n");
   writer->write(hdl_file, "            start_results_comparison = 1;\n");
   writer->write(hdl_file, "            next_state = 5;\n");
   writer->write(hdl_file, "         end\n");
   writer->write(hdl_file, "       5:\n");
   writer->write(hdl_file, "         begin\n");
   if(first_valid_input)
   {
      writer->write_comment(hdl_file, "wait a cycle (needed for a correct simulation)\n");
      writer->write(hdl_file, "            $fclose(res_file);\n");
      writer->write(hdl_file, "            $fclose(file);\n");
      writer->write(hdl_file, "            $finish;\n");
   }
   else
   {
      writer->write_comment(hdl_file, "Restart a new computation if possible\n");
      writer->write(hdl_file, "            next_state = 2;\n");
   }
   writer->write(hdl_file, "         end\n");
   writer->write(hdl_file, "     endcase // case (state)\n");
   writer->write(hdl_file, "  end // always @ (*)\n");

   writer->write(hdl_file, "always @(posedge clock)\n");
   writer->write(hdl_file, "  begin\n");
   writer->write(hdl_file, "  state = next_state;\n");
   writer->write(hdl_file, "  start_port = start_port_next;\n");
   writer->write(hdl_file, "  end\n");
}


static void memory_initialization(std::ostream &hdl_file, language_writerRef writer)
{
   writer->write(hdl_file, "for (addr = 0; addr < MEMSIZE; addr = addr + 1)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "mem[addr] = 8'b0;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
}


static void write_max_simulation_time_control(std::ostream &hdl_file, language_writerRef writer)
{
   writer->write(hdl_file, "always @(posedge clock)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "if (($time - startTime)/`CLOCK_PERIOD > `SIMULATION_LENGTH)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"Simulation not completed into %d cycles\", `SIMULATION_LENGTH);\n");
   writer->write(hdl_file, "$fwrite(res_file, \"X\\t\");\n");
   writer->write(hdl_file, "$fwrite(res_file, \"%d\\n\", `SIMULATION_LENGTH);\n");
   writer->write(hdl_file, "$fclose(res_file);\n");
   writer->write(hdl_file, "$fclose(file);\n");
   writer->write(hdl_file, "$finish;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n\n");
}


static void reading_base_memory_address_from_file(std::ostream &hdl_file, language_writerRef writer)
{
   writer->write_comment(hdl_file, "reading base address memory --------------------------------------------------------------\n");
   writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
   writer->write(hdl_file, "while (_ch_ == \"/\" || _ch_ == \"\\n\" || _ch_ == \"b\") // skip comments and empty lines\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "if (_ch_ == \"b\")\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "_r_ = $fscanf(file,\"%b\\n\", base_addr); ");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
}


static void memory_initialization_from_file(std::ostream &hdl_file, language_writerRef writer)
{
   writer->write_comment(hdl_file, "initializing memory --------------------------------------------------------------\n");
   writer->write(hdl_file, "while (_ch_ == \"/\" || _ch_ == \"\\n\" || _ch_ == \"m\") // skip comments and empty lines\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "if (_ch_ == \"m\")\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "_r_ = $fscanf(file,\"%b\\n\", mem[_addr_i_]); ");
   writer->write(hdl_file, "_addr_i_ = _addr_i_ + 1;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
}


static void begin_file_reading_operation(std::ostream &hdl_file, language_writerRef writer)
{
   writer->write(hdl_file, "\n");

   writer->write_comment(hdl_file, "Assigning values\n");
   writer->write(hdl_file, "always @ (posedge " + STR(START_PORT_NAME) + ")\nbegin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "if (" + STR(START_PORT_NAME) + " == 1'b1)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
}

static void end_file_reading_operation(std::ostream &hdl_file, language_writerRef writer)
{
   writer->write_comment(hdl_file, "Simulation start\n");
   writer->write(hdl_file, "startTime = $time;\n$display(\"Reading of vector values from input file completed. Simulation started.\");\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n\n");
}


static void read_input_value_from_file(std::ostream &hdl_file, language_writerRef writer,
                                       const std::string &input_name, bool &first_valid_input)
{
   if(input_name != CLOCK_PORT_NAME && input_name != RESET_PORT_NAME && input_name != START_PORT_NAME)
   {
      writer->write(hdl_file, "\n");
      writer->write_comment(hdl_file, "Read a value for " + input_name + " --------------------------------------------------------------\n");
      if (!first_valid_input)
         writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
      writer->write(hdl_file, "while (_ch_ == \"/\" || _ch_ == \"\\n\") ");
      writer->write_comment(hdl_file, "skip comments and empty lines\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
      writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");

      /// if it's the first input value
      if(first_valid_input)
      {
         /// write statement for new vectors' check
         writer->write_comment(hdl_file, "If no character found\n");
         writer->write(hdl_file, "if (_ch_ == -1)\n");
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write(hdl_file, "$display(\"No more values found. Simulation(s) executed: %d.\\n\", _n_);\n");
         writer->write(hdl_file, "$fclose(res_file);\n");
         writer->write(hdl_file, "$fclose(file);\n");
         writer->write(hdl_file, "$finish;\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");
         writer->write(hdl_file, "else\n");
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write_comment(hdl_file, "Vectors count\n");
         writer->write(hdl_file, "_n_ = _n_ + 1;\n");
         writer->write(hdl_file, "$display(\"Start reading vector %d's values from input file.\\n\", _n_);\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");
         first_valid_input = false;
      }

      writer->write(hdl_file, "if (_ch_ == \"p\") begin\n");
      writer->write(hdl_file, "_r_ = $fscanf(file,\"%b\\n\", " + input_name + "); ");
      writer->write_comment(hdl_file, "expected format: bbb...b (example: 00101110)\n");
      writer->write(hdl_file, "end\n");

      writer->write(hdl_file, "if (_r_ != 1) ");
      writer->write_comment(hdl_file, "error\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
      writer->write(hdl_file, "if (_ch_ == `EOF) ");
      writer->write_comment(hdl_file, "end-of-file reached\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "$display(\"ERROR - End of file reached before getting all the values for the parameters\");\n");
      writer->write(hdl_file, "$fclose(res_file);\n");
      writer->write(hdl_file, "$fclose(file);\n");
      writer->write(hdl_file, "$finish;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "else ");
      writer->write_comment(hdl_file, "generic error\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "$display(\"ERROR - Unknown error while reading the file. Character found: %s\", _ch_);\n");
      writer->write(hdl_file, "$fclose(res_file);\n");
      writer->write(hdl_file, "$fclose(file);\n");
      writer->write(hdl_file, "$finish;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "$display(\"Value found for input " + input_name + ": %b\", " + input_name + ");\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write_comment(hdl_file, "Value for " + input_name + " found ---------------------------------------------------------------\n");
   }
}

static void write_compute_ulps_functions(std::ostream& hdl_file, language_writerRef writer)
{
   writer->write(hdl_file, "\n");
   writer->write(hdl_file, "function real bits32_to_real64;\n");
   writer->write(hdl_file, "  input [31:0] in1;\n");
   writer->write(hdl_file, "  reg [7:0] exponent1;\n");
   writer->write(hdl_file, "  reg is_exp_zero;\n");
   writer->write(hdl_file, "  reg is_all_ones;\n");
   writer->write(hdl_file, "  reg [10:0] exp_tmp;\n");
   writer->write(hdl_file, "  reg [63:0] out1;\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "  exponent1 = in1[30:23];\n");
   writer->write(hdl_file, "  is_exp_zero = exponent1 == 8'd0;\n");
   writer->write(hdl_file, "  is_all_ones = exponent1 == {8{1'b1}};\n");
   writer->write(hdl_file, "  exp_tmp = {3'd0, exponent1} + 11'd896;\n");
   writer->write(hdl_file, "  out1[63] = in1[31];\n");
   writer->write(hdl_file, "  out1[62:52] = is_exp_zero ? 11'd0 : (is_all_ones ? {11{1'b1}} : exp_tmp);\n");
   writer->write(hdl_file, "  out1[51:29] = in1[22:0];\n");
   writer->write(hdl_file, "  out1[28:0] = 29'd0;\n");
   writer->write(hdl_file, "  bits32_to_real64 = $bitstoreal(out1);\n");
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "endfunction\n");

   writer->write(hdl_file, "function real compute_ulp32;\n");
   writer->write(hdl_file, "  input [31:0] computed;\n");
   writer->write(hdl_file, "  input [31:0] expected;\n");
   writer->write(hdl_file, "  real computedR;\n");
   writer->write(hdl_file, "  real expectedR;\n");
   writer->write(hdl_file, "  real diffR;\n");
   writer->write(hdl_file, "  reg [31:0] denom;\n");
   writer->write(hdl_file, "  real denomR;\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "  denom = 32'd0;\n");
   writer->write(hdl_file, "  if (expected[30:0] == 31'd0)\n");
   writer->write(hdl_file, "    denom[30:23] = 8'd104;\n");
   writer->write(hdl_file, "  else\n");
   writer->write(hdl_file, "    denom[30:23] = expected[30:23]-8'd23;\n");
   writer->write(hdl_file, "  computedR = bits32_to_real64(computed);\n");
   writer->write(hdl_file, "  expectedR = bits32_to_real64(expected);\n");
   writer->write(hdl_file, "  denomR = bits32_to_real64(denom);\n");
   writer->write(hdl_file, "  diffR = computedR - expectedR;\n");
   writer->write(hdl_file, "  if(diffR < 0.0)\n");
   writer->write(hdl_file, "    diffR = - diffR;\n");
   writer->write(hdl_file, "  compute_ulp32 = diffR / denomR;\n");
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "endfunction\n");
   writer->write(hdl_file, "\n");
   writer->write(hdl_file, "function real compute_ulp64;\n");
   writer->write(hdl_file, "  input [63:0] computed;\n");
   writer->write(hdl_file, "  input [63:0] expected;\n");
   writer->write(hdl_file, "  real computedR;\n");
   writer->write(hdl_file, "  real expectedR;\n");
   writer->write(hdl_file, "  real diffR;\n");
   writer->write(hdl_file, "  reg [63:0] denom;\n");
   writer->write(hdl_file, "  real denomR;\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "  denom = 64'd0;\n");
   writer->write(hdl_file, "  if (expected[62:0] == 63'd0)\n");
   writer->write(hdl_file, "    denom[62:52] = 11'd971;\n");
   writer->write(hdl_file, "  else\n");
   writer->write(hdl_file, "    denom[62:52] = expected[62:52]-11'd52;\n");
   writer->write(hdl_file, "  computedR = $bitstoreal(computed);\n");
   writer->write(hdl_file, "  expectedR = $bitstoreal(expected);\n");
   writer->write(hdl_file, "  denomR = $bitstoreal(denom);\n");
   writer->write(hdl_file, "  diffR = computedR - expectedR;\n");
   writer->write(hdl_file, "  if(diffR < 0.0)\n");
   writer->write(hdl_file, "    diffR = - diffR;\n");
   writer->write(hdl_file, "  compute_ulp64 = diffR / denomR;\n");
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "endfunction\n");

}



/*
 * MinimalInterfaceTestbench Implementation.
 */
void MinimalInterfaceTestbench::write_underling_testbench(std::ostream& hdl_file, language_writerRef writer,
                                                          const structural_objectRef &cir, double target_period,
                                                          std::string simulation_values_path, bool generate_vcd_output,
                                                          bool xilinx_isim, const tree_managerConstRef TreeM) const
{
   std::string input_values_filename = simulation_values_path;
   memory_allocation::policy_t policy = static_cast<memory_allocation::policy_t>(Param->getOption<unsigned int>(OPT_memory_allocation_policy));

   /// generation of VCD output
   std::string vcd_output_filename = output_directory + "test.vcd";

   module * mod = GetPointer<module>(cir);

   write_hdl_testbench_prolog(hdl_file, writer, target_period, Param);

   write_module_begin(hdl_file, writer, cir);

   write_compute_ulps_functions(hdl_file, writer);

   write_auxiliary_signal_declaration(hdl_file, writer, cir);

   bool with_memory = false;

   write_input_signal_declaration(hdl_file, writer, cir, TreeM, with_memory);

   write_output_signal_declaration(hdl_file, writer, cir);

   /// write inout signals declaration
   if (mod->get_in_out_port_size())
   {
      THROW_ERROR("Module with IO ports cannot synthesized");
   }
   if(with_memory)
   {
      structural_objectRef M_Rdata_ram_port = mod->find_member("M_Rdata_ram", port_o_K, cir);
      long long int bitsize = M_Rdata_ram_port->get_typeRef()->size * M_Rdata_ram_port->get_typeRef()->vector_size;
      unsigned int n_ports = M_Rdata_ram_port->get_kind() == port_vector_o_K ? GetPointer<port_o>(M_Rdata_ram_port)->get_ports_size() : 1;
      writer->write(hdl_file, "reg signed [31:0] reg_DataReady["+ STR(n_ports-1)+ ":0];\n");
      writer->write(hdl_file, "wire ["+ STR(bitsize*n_ports-1)+ ":0] mask;\n\n");
      if(policy==memory_allocation::ALL_BRAM || policy==memory_allocation::EXT_PIPELINED_BRAM)
         writer->write(hdl_file, "reg ["+ STR(bitsize*n_ports-1)+ ":0] M_Rdata_ram_delayed [`MEM_DELAY_READ-1:0];\n\n");
   }
   
   write_module_instantiation(hdl_file, writer, cir, xilinx_isim);

   if(xilinx_isim)
       writer->write(hdl_file, "glbl #("+ STR(target_period/2*1000) + ", 0) glbl();");

   begin_initial_block(hdl_file, writer);

   /// VCD output generation (optional)
   if(generate_vcd_output)
   {
      writer->write_comment(hdl_file, "VCD file generation\n");
      writer->write(hdl_file, "$dumpfile(\"" + vcd_output_filename + "\");\n");
      writer->write(hdl_file, "$dumpvars;\n");
   }

   /// open file with values
   open_value_file(hdl_file, writer, input_values_filename);

   /// open file with results
   std::string result_file = Param->getOption<std::string>(OPT_simulation_output);
   open_result_file(hdl_file, writer, result_file);

   /// auxiliary variables initialization
   initialize_auxiliary_variables(hdl_file, writer);

   /// write input signals initialization
   initialize_input_signals(hdl_file, writer, cir, TreeM);

   if (mod->find_member(RETURN_PORT_NAME, port_o_K, cir))
   {
      writer->write(hdl_file, "ex_" + STR(RETURN_PORT_NAME) + " = 0;\n");
      writer->write(hdl_file, "registered_" + STR(RETURN_PORT_NAME) + " = 0;\n");
      writer->write(hdl_file, "\n");
   }
   if(with_memory)
   {
      structural_objectRef M_Rdata_ram_port = mod->find_member("M_Rdata_ram", port_o_K, cir);
      THROW_ASSERT(M_Rdata_ram_port, "M_Rdata_ram port is missing");
      unsigned int M_Rdata_ram_port_n_ports = M_Rdata_ram_port->get_kind() == port_vector_o_K ? GetPointer<port_o>(M_Rdata_ram_port)->get_ports_size() : 1;
      for(unsigned int i= 0; i < M_Rdata_ram_port_n_ports; ++i)
      {
         writer->write(hdl_file, "reg_DataReady["+boost::lexical_cast<std::string>(i)+"] = 0;\n\n");
      }
   }

   memory_initialization(hdl_file, writer);

   end_initial_block(hdl_file, writer);

   /// file reading operations
   begin_file_reading_operation(hdl_file, writer);

   reading_base_memory_address_from_file(hdl_file, writer);

   memory_initialization_from_file(hdl_file, writer);

   bool first_valid_input = true;

   /// iterate over all input ports
   if (mod->get_in_port_size())
   {
      std::string input_name = "";

      /// write code for input values extraction
      for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
      {
         if(GetPointer<port_o>(mod->get_in_port(i))->get_is_memory()) continue;
         input_name = HDL_manager::convert_to_identifier(writer.get(), mod->get_in_port(i)->get_id());

         read_input_value_from_file(hdl_file, writer, input_name, first_valid_input);
      }
   }
   if (!first_valid_input) writer->write(hdl_file, "_ch_ = $fgetc(file);\n");

   end_file_reading_operation(hdl_file, writer);

   write_max_simulation_time_control(hdl_file, writer);

   if(with_memory)
   {
      structural_objectRef Mout_data_ram_size_port = mod->find_member("Mout_data_ram_size", port_o_K, cir);
      THROW_ASSERT(Mout_data_ram_size_port, "Mout_data_ram_size port is missing");
      structural_objectRef Mout_Wdata_ram_port = mod->find_member("Mout_Wdata_ram", port_o_K, cir);
      THROW_ASSERT(Mout_Wdata_ram_port, "Mout_Wdata_ram port is missing");
      long long int Mout_Wdata_ram_bitsize = Mout_Wdata_ram_port->get_typeRef()->size * Mout_Wdata_ram_port->get_typeRef()->vector_size;
      if(Mout_data_ram_size_port->get_kind() == port_vector_o_K)
      {
         unsigned int Mout_data_ram_size_n_ports = Mout_data_ram_size_port->get_kind() == port_vector_o_K ? GetPointer<port_o>(Mout_data_ram_size_port)->get_ports_size() : 1;
         long long int Mout_data_ram_size_bitsize = Mout_data_ram_size_port->get_typeRef()->size * Mout_data_ram_size_port->get_typeRef()->vector_size;
         for(unsigned int i = 0; i < Mout_data_ram_size_n_ports; ++i)
         {
            std::string mask_string = "(1 << Mout_data_ram_size[" + boost::lexical_cast<std::string>((i+1)*Mout_data_ram_size_bitsize-1)+":"+ boost::lexical_cast<std::string>((i)*Mout_data_ram_size_bitsize) + "]) -1";
            writer->write(hdl_file, "assign mask["+ boost::lexical_cast<std::string>((i+1)*Mout_Wdata_ram_bitsize-1)+":"+ boost::lexical_cast<std::string>((i)*Mout_Wdata_ram_bitsize) +"] = " + mask_string + ";\n");
         }
      }
      else
         writer->write(hdl_file, "assign mask = (1 << Mout_data_ram_size) -1;\n");

      writer->write_comment(hdl_file, "OffChip Memory write\n");
      writer->write(hdl_file, "always @(posedge clock)\n");
      writer->write(hdl_file, "begin");
      writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");

      structural_objectRef Mout_addr_ram_port = mod->find_member("Mout_addr_ram", port_o_K, cir);
      THROW_ASSERT(Mout_addr_ram_port, "Mout_addr_ram port is missing");
      long long int Mout_addr_ram_bitsize = Mout_addr_ram_port->get_typeRef()->size * Mout_addr_ram_port->get_typeRef()->vector_size;
      unsigned int Mout_Wdata_ram_n_ports = Mout_Wdata_ram_port->get_kind() == port_vector_o_K ? GetPointer<port_o>(Mout_Wdata_ram_port)->get_ports_size() : 1;
      long long int bitsize = Mout_Wdata_ram_port->get_typeRef()->size * Mout_Wdata_ram_port->get_typeRef()->vector_size;
      for(unsigned int i= 0; i < Mout_Wdata_ram_n_ports; ++i)
      {
         std::string mem_aggregate = memory_aggregate_slices(i, bitsize, Mout_addr_ram_bitsize);
         std::string post_slice;
         if(Mout_addr_ram_port->get_kind() == port_vector_o_K)
            post_slice = "["+boost::lexical_cast<std::string>(i)+"]";
         writer->write(hdl_file, "if (Mout_we_ram"+ post_slice + " === 1'b1 && base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE))\n");
         writer->write(hdl_file, "begin");
         writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
         if(Mout_Wdata_ram_port->get_kind() == port_vector_o_K)
            post_slice = "["+boost::lexical_cast<std::string>((i+1)*bitsize-1)+":"+boost::lexical_cast<std::string>(i*bitsize)+"]";
         else
            post_slice = "";
         writer->write(hdl_file, mem_aggregate + " <= (Mout_Wdata_ram"+ post_slice + " & mask"+ post_slice + ") | (" + mem_aggregate + " & ~(mask"+ post_slice + "));\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");
      }
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n\n");

      structural_objectRef Min_Wdata_ram_port = mod->find_member("Min_Wdata_ram", port_o_K, cir);
      if(Min_Wdata_ram_port)
         writer->write(hdl_file, "assign Min_Wdata_ram = 0;\n");

      structural_objectRef M_Rdata_ram_port = mod->find_member("M_Rdata_ram", port_o_K, cir);
      THROW_ASSERT(M_Rdata_ram_port, "M_Rdata_ram port is missing");
      bitsize = M_Rdata_ram_port->get_typeRef()->size * M_Rdata_ram_port->get_typeRef()->vector_size;
      unsigned int M_Rdata_ram_port_n_ports = M_Rdata_ram_port->get_kind() == port_vector_o_K ? GetPointer<port_o>(M_Rdata_ram_port)->get_ports_size() : 1;

      structural_objectRef Sout_Rdata_ram_port = mod->find_member("Sout_Rdata_ram", port_o_K, cir);
      structural_objectRef Sin_Rdata_ram_port = mod->find_member("Sin_Rdata_ram", port_o_K, cir);
      if(Sin_Rdata_ram_port)
      {
         for(unsigned int i= 0; i < M_Rdata_ram_port_n_ports; ++i)
         {
            std::string mem_aggregate = memory_aggregate_slices(i, bitsize, Mout_addr_ram_bitsize);
            std::string post_slice1;
            if(Mout_addr_ram_port->get_kind() == port_vector_o_K)
               post_slice1 = "["+boost::lexical_cast<std::string>(i)+"]";
            std::string post_slice2;
            if(M_Rdata_ram_port->get_kind() == port_vector_o_K)
               post_slice2 = "["+boost::lexical_cast<std::string>((i+1)*bitsize-1)+":"+boost::lexical_cast<std::string>(i*bitsize)+"]";
            else
               post_slice2 = "";
            writer->write(hdl_file, "assign Sin_Rdata_ram"+ post_slice2 + " = (Mout_oe_ram"+ post_slice1 + "===1'b1 && base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE)) ? " + mem_aggregate + " : " + STR(bitsize) + "'b0;\n");
         }
         THROW_ASSERT(Sout_Rdata_ram_port, "Sout_Rdata_ram port is missing");
         writer->write(hdl_file, "assign M_Rdata_ram = Sout_Rdata_ram;\n");
      }
      else if(Sout_Rdata_ram_port)
      {
         for(unsigned int i= 0; i < M_Rdata_ram_port_n_ports; ++i)
         {
            std::string mem_aggregate = memory_aggregate_slices(i, bitsize, Mout_addr_ram_bitsize);
            std::string post_slice1;
            if(Mout_addr_ram_port->get_kind() == port_vector_o_K)
               post_slice1 = "["+boost::lexical_cast<std::string>(i)+"]";
            std::string post_slice2;
            if(M_Rdata_ram_port->get_kind() == port_vector_o_K)
               post_slice2 = "["+boost::lexical_cast<std::string>((i+1)*bitsize-1)+":"+boost::lexical_cast<std::string>(i*bitsize)+"]";
            else
               post_slice2 = "";
            if(policy==memory_allocation::ALL_BRAM || policy==memory_allocation::EXT_PIPELINED_BRAM)
            {
               writer->write(hdl_file, "always @(posedge clock)\n");
               writer->write(hdl_file, "begin");
               writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
               writer->write(hdl_file, "M_Rdata_ram_delayed[`MEM_DELAY_READ-1]"+ post_slice2 + " = Sout_Rdata_ram"+ post_slice2 + " | ((Mout_oe_ram"+ post_slice1 + "===1'b1 && base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE)) ? " + mem_aggregate + " : " + STR(bitsize) + "'b0);\n");
               writer->write(hdl_file, "for (_i_=0; _i_<`MEM_DELAY_READ-1; _i_=_i_+1)");
               writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
               writer->write(hdl_file, "M_Rdata_ram_delayed[_i_]"+ post_slice2 + " <= M_Rdata_ram_delayed[_i_+1]"+ post_slice2 + ";");
               writer->write(hdl_file, STR(STD_CLOSING_CHAR));
               writer->write(hdl_file, STR(STD_CLOSING_CHAR)+"\n");
               writer->write(hdl_file, "end\n");
               writer->write(hdl_file, "assign M_Rdata_ram"+ post_slice2 + " = M_Rdata_ram_delayed[0]"+ post_slice2 + ";\n\n");
            }
            else
               writer->write(hdl_file, "assign M_Rdata_ram"+ post_slice2 + " = Sout_Rdata_ram"+ post_slice2 + " | ((Mout_oe_ram"+ post_slice1 + "===1'b1 && base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE)) ? " + mem_aggregate + " : " + STR(bitsize) + "'b0);\n");
         }
      }
      else
      {
         for(unsigned int i= 0; i < M_Rdata_ram_port_n_ports; ++i)
         {
            std::string mem_aggregate = memory_aggregate_slices(i, bitsize, Mout_addr_ram_bitsize);
            std::string post_slice1;
            if(Mout_addr_ram_port->get_kind() == port_vector_o_K)
               post_slice1 = "["+boost::lexical_cast<std::string>(i)+"]";
            std::string post_slice2;
            if(M_Rdata_ram_port->get_kind() == port_vector_o_K)
               post_slice2 = "["+boost::lexical_cast<std::string>((i+1)*bitsize-1)+":"+boost::lexical_cast<std::string>(i*bitsize)+"]";
            else
               post_slice2 = "";
            if(policy==memory_allocation::ALL_BRAM || policy==memory_allocation::EXT_PIPELINED_BRAM)
            {
               writer->write(hdl_file, "always @(posedge clock)\n");
               writer->write(hdl_file, "begin");
               writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
               writer->write(hdl_file, "M_Rdata_ram_delayed[`MEM_DELAY_READ-1]"+ post_slice2 + " = (Mout_oe_ram"+ post_slice1 + "===1'b1 && base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE)) ? " + mem_aggregate + " : " + STR(bitsize) + "'b0;\n");
               writer->write(hdl_file, "for (_i_=0; _i_<`MEM_DELAY_READ-1; _i_=_i_+1)");
               writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
               writer->write(hdl_file, "M_Rdata_ram_delayed[_i_]"+ post_slice2 + " <= M_Rdata_ram_delayed[_i_+1]"+ post_slice2 + ";");
               writer->write(hdl_file, STR(STD_CLOSING_CHAR));
               writer->write(hdl_file, STR(STD_CLOSING_CHAR)+"\n");
               writer->write(hdl_file, "end\n");
               writer->write(hdl_file, "assign M_Rdata_ram"+ post_slice2 + " = M_Rdata_ram_delayed[0]"+ post_slice2 + ";\n\n");
            }
            else
               writer->write(hdl_file, "assign M_Rdata_ram"+ post_slice2 + " = (Mout_oe_ram"+ post_slice1 + "===1'b1 && base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE)) ? " + mem_aggregate + " : " + STR(bitsize) + "'b0;\n");
         }
      }
      structural_objectRef Sin_Wdata_ram_port = mod->find_member("S_Wdata_ram", port_o_K, cir);
      if(Sin_Wdata_ram_port)
         writer->write(hdl_file, "assign S_Wdata_ram = Mout_Wdata_ram;\n");


      writer->write(hdl_file, "always @(posedge clock)\n");
      writer->write(hdl_file, "begin");
      writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
      for(unsigned int i= 0; i < M_Rdata_ram_port_n_ports; ++i)
      {
         std::string post_slice1;
         if(Mout_addr_ram_port->get_kind() == port_vector_o_K)
            post_slice1 = "["+boost::lexical_cast<std::string>(i)+"]";
         std::string post_slice2;
         if(M_Rdata_ram_port->get_kind() == port_vector_o_K)
            post_slice2 = "["+boost::lexical_cast<std::string>((i+1)*bitsize-1)+":"+boost::lexical_cast<std::string>(i*bitsize)+"]";
         else
            post_slice2 = "";

         writer->write(hdl_file, "if ((Mout_oe_ram"+ post_slice1 + "===1'b1 && base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE)))\n");
         writer->write(hdl_file, "begin");
         writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
         writer->write(hdl_file, "if (reg_DataReady["+boost::lexical_cast<std::string>(i)+"] >= 0 && reg_DataReady["+boost::lexical_cast<std::string>(i)+"] < `MEM_DELAY_READ-1)");
         writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
         writer->write(hdl_file, "reg_DataReady["+boost::lexical_cast<std::string>(i)+"] <= 1 + reg_DataReady["+boost::lexical_cast<std::string>(i)+"];");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR)+"\n");
         writer->write(hdl_file, "else");
         writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
         writer->write(hdl_file, "reg_DataReady["+boost::lexical_cast<std::string>(i)+"] <= 0;\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n\n");

         writer->write(hdl_file, "else if ((Mout_we_ram"+ post_slice1 + "===1'b1 && base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE)))\n");
         writer->write(hdl_file, "begin");
         writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
         writer->write(hdl_file, "if (reg_DataReady["+boost::lexical_cast<std::string>(i)+"] >= 0 && reg_DataReady["+boost::lexical_cast<std::string>(i)+"] < `MEM_DELAY_WRITE-1)");
         writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
         writer->write(hdl_file, "reg_DataReady["+boost::lexical_cast<std::string>(i)+"] <= 1 + reg_DataReady["+boost::lexical_cast<std::string>(i)+"];\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "else");
         writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
         writer->write(hdl_file, "reg_DataReady["+boost::lexical_cast<std::string>(i)+"] <= 0;\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n\n");

         writer->write(hdl_file, "else");
         writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
         writer->write(hdl_file, "reg_DataReady["+boost::lexical_cast<std::string>(i)+"] <= 0;");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR)+"\n");
      }
      writer->write(hdl_file, STR(STD_CLOSING_CHAR)+"\n");
      writer->write(hdl_file, "end\n\n");

      structural_objectRef Sin_DataRdy_port = mod->find_member("Sin_DataRdy", port_o_K, cir);
      structural_objectRef Sout_DataRdy_port = mod->find_member("Sout_DataRdy", port_o_K, cir);
      if(Sin_DataRdy_port)
      {
         for(unsigned int i= 0; i < M_Rdata_ram_port_n_ports; ++i)
         {
            std::string post_slice1;
            if(Mout_addr_ram_port->get_kind() == port_vector_o_K)
               post_slice1 = "["+boost::lexical_cast<std::string>(i)+"]";
            writer->write(hdl_file,"assign Sin_DataRdy"+ post_slice1 + " = (base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE)) && ((Mout_oe_ram"+ post_slice1 + "===1'b1 && (reg_DataReady["+boost::lexical_cast<std::string>(i)+"] == `MEM_DELAY_READ-1)) || (Mout_we_ram"+ post_slice1 + "===1'b1 && (reg_DataReady["+boost::lexical_cast<std::string>(i)+"] == `MEM_DELAY_WRITE-1)));\n");
         }
         THROW_ASSERT(Sout_DataRdy_port, "Sout_DataRdy port is missing");
         writer->write(hdl_file,"assign M_DataRdy = Sout_DataRdy;\n");
      }
      else if(Sout_DataRdy_port)
      {
         for(unsigned int i= 0; i < M_Rdata_ram_port_n_ports; ++i)
         {
            std::string post_slice1;
            if(Mout_addr_ram_port->get_kind() == port_vector_o_K)
               post_slice1 = "["+boost::lexical_cast<std::string>(i)+"]";
            writer->write(hdl_file,"assign M_DataRdy"+ post_slice1 + " = Sout_DataRdy"+ post_slice1 + " | ((base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE)) && ((Mout_oe_ram"+ post_slice1 + "===1'b1 && (reg_DataReady["+boost::lexical_cast<std::string>(i)+"] == `MEM_DELAY_READ-1)) || (Mout_we_ram"+ post_slice1 + "===1'b1 && (reg_DataReady["+boost::lexical_cast<std::string>(i)+"] == `MEM_DELAY_WRITE-1))));\n");
         }
      }
      else
      {
         for(unsigned int i= 0; i < M_Rdata_ram_port_n_ports; ++i)
         {
            std::string post_slice1;
            if(Mout_addr_ram_port->get_kind() == port_vector_o_K)
               post_slice1 = "["+boost::lexical_cast<std::string>(i)+"]";
            writer->write(hdl_file,"assign M_DataRdy"+ post_slice1 + " = (base_addr <= Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] && Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] < (base_addr + MEMSIZE)) && ((Mout_oe_ram"+ post_slice1 + "===1'b1 && (reg_DataReady["+boost::lexical_cast<std::string>(i)+"] == `MEM_DELAY_READ-1)) || (Mout_we_ram"+ post_slice1 + "===1'b1 && (reg_DataReady["+boost::lexical_cast<std::string>(i)+"] == `MEM_DELAY_WRITE-1)));\n");
         }
      }

      /// Master inputs set up
      structural_objectRef Min_oe_ram_port = mod->find_member("Min_oe_ram", port_o_K, cir);
      if(Min_oe_ram_port)
         writer->write(hdl_file, "assign Min_oe_ram = 0;\n");
      structural_objectRef Min_we_ram_port = mod->find_member("Min_we_ram", port_o_K, cir);
      if(Min_we_ram_port)
         writer->write(hdl_file, "assign Min_we_ram = 0;\n");
      structural_objectRef Min_addr_ram_port = mod->find_member("Min_addr_ram", port_o_K, cir);
      if(Min_addr_ram_port)
         writer->write(hdl_file, "assign Min_addr_ram = 0;\n");
      structural_objectRef Min_data_ram_size_port = mod->find_member("Min_data_ram_size", port_o_K, cir);
      if(Min_data_ram_size_port)
         writer->write(hdl_file, "assign Min_data_ram_size = 0;\n");
      /// Slave inputs connections
      structural_objectRef Mout_oe_ram_port = mod->find_member("Mout_oe_ram", port_o_K, cir);
      THROW_ASSERT(Mout_oe_ram_port, "Mout_oe_ram port is missing");
      structural_objectRef S_oe_ram_port = mod->find_member("S_oe_ram", port_o_K, cir);
      if(S_oe_ram_port)
      {
         writer->write(hdl_file, "assign S_oe_ram = Mout_oe_ram;\n");
      }
      structural_objectRef Mout_we_ram_port = mod->find_member("Mout_we_ram", port_o_K, cir);
      THROW_ASSERT(Mout_we_ram_port, "Mout_we_ram port is missing");
      structural_objectRef S_we_ram_port = mod->find_member("S_we_ram", port_o_K, cir);
      if(S_we_ram_port)
      {
         writer->write(hdl_file, "assign S_we_ram = Mout_we_ram;\n");
      }
      structural_objectRef S_addr_ram_port = mod->find_member("S_addr_ram", port_o_K, cir);
      if(S_addr_ram_port)
      {
         writer->write(hdl_file, "assign S_addr_ram = Mout_addr_ram;\n");
      }

      structural_objectRef S_data_ram_size_port = mod->find_member("S_data_ram_size", port_o_K, cir);
      if(S_data_ram_size_port)
      {
         writer->write(hdl_file, "assign S_data_ram_size = Mout_data_ram_size;\n");
      }

      writer->write(hdl_file, "always @(posedge clock)\n");
      writer->write(hdl_file, "begin");
      writer->write(hdl_file, STR(STD_OPENING_CHAR)+"\n");
      for(unsigned int i= 0; i < M_Rdata_ram_port_n_ports && i < Mout_Wdata_ram_n_ports; ++i)
      {
         std::string post_slice1;
         if(Mout_addr_ram_port->get_kind() == port_vector_o_K)
            post_slice1 = "["+boost::lexical_cast<std::string>(i)+"]";
         writer->write(hdl_file, "if (Mout_we_ram"+ post_slice1 + "===1'b1 && Mout_oe_ram"+ post_slice1 + "===1'b1)\n");
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write_comment(hdl_file, "error\n");
         writer->write(hdl_file, "$display(\"ERROR - Mout_we_ram and Mout_oe_ram both enabled\");\n");
         writer->write(hdl_file, "$fclose(res_file);\n");
         writer->write(hdl_file, "$fclose(file);\n");
         writer->write(hdl_file, "$finish;\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");
      }
      writer->write(hdl_file, STR(STD_CLOSING_CHAR)+"\n");
      writer->write(hdl_file, "end\n\n");

   }

   /// write code for simulation time computation
   writer->write_comment(hdl_file, "Check done signal\n");
   writer->write(hdl_file, "always @(negedge clock)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "if (done_port == 1)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "endTime = $time;\n\n");
   writer->write_comment(hdl_file, "Simulation time (clock cycles) = (time elapsed (seconds) / clock cycle (seconds per cycle)) (until done_port is 1)\n");
   writer->write(hdl_file, "sim_time = (endTime + `HALF_CLOCK_PERIOD - startTime)/`CLOCK_PERIOD;\n\n");

   writer->write(hdl_file, "successs = 1;\n");
   writer->write(hdl_file, "compare_outputs = 0;\n");

   if (mod->find_member(RETURN_PORT_NAME, port_o_K, cir))
   {
      writer->write(hdl_file, "registered_" + std::string(RETURN_PORT_NAME) + " = " + std::string(RETURN_PORT_NAME) + ";\n");
   }
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));

   writer->write(hdl_file, "always @(negedge clock)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "if (start_results_comparison == 1)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));

   /// write code for expected output values extraction
   for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
   {
      if (GetPointer<port_o>(mod->get_in_port(i))->get_is_memory() || !mod->get_in_port(i)->get_typeRef()->treenode || !tree_helper::is_a_pointer(TreeM, mod->get_in_port(i)->get_typeRef()->treenode)) continue;

      std::string unmangled_name = mod->get_in_port(i)->get_id();
      std::string port_name = HDL_manager::convert_to_identifier(writer.get(), unmangled_name);
      std::string output_name = "ex_" + unmangled_name;
      unsigned int pt_type_index = tree_helper::get_pointed_type(TreeM, tree_helper::get_type_index(TreeM,mod->get_in_port(i)->get_typeRef()->treenode));
      tree_nodeRef pt_node = TreeM->get_tree_node_const(pt_type_index);
      if(GetPointer<array_type>(pt_node))
      {
         while(GetPointer<array_type>(pt_node))
         {
            pt_type_index = GET_INDEX_NODE(GetPointer<array_type>(pt_node)->elts);
            pt_node = GET_NODE(GetPointer<array_type>(pt_node)->elts);
         }
      }
      long long int bitsize = tree_helper::size(TreeM, pt_type_index);
      bool is_real = tree_helper::is_real(TreeM, pt_type_index);


      writer->write(hdl_file, "\n");
      writer->write_comment(hdl_file, "OPTIONAL - Read a value for " + output_name + " --------------------------------------------------------------\n");

      writer->write(hdl_file, "_i_ = 0;\n");
      writer->write(hdl_file, "while (_ch_ == \"/\" || _ch_ == \"\\n\" || _ch_ == \"o\") ");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "if (_ch_ == \"o\")\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "compare_outputs = 1;\n");
      writer->write(hdl_file, "_r_ = $fscanf(file,\"%b\\n\", " + output_name + "); ");
      writer->write_comment(hdl_file, "expected format: bbb...b (example: 00101110)\n");
      writer->write(hdl_file, "if (_r_ != 1)\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write_comment(hdl_file, "error\n");
      writer->write(hdl_file, "$display(\"ERROR - Unknown error while reading the file. Character found: %s\", _ch_);\n");
      writer->write(hdl_file, "$fclose(res_file);\n");
      writer->write(hdl_file, "$fclose(file);\n");
      writer->write(hdl_file, "$finish;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "$display(\"Value found for output " + output_name + ": %b\", " + output_name + ");\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      if(is_real)
      {
         writer->write(hdl_file, "$display(\" res = %b " + port_name + " = %d " " mem[" + port_name  + " + %d - base_addr] = %f  expected = %f \", ");
         writer->write(hdl_file, "{");
         for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
         {
            if(bitsize_index) writer->write(hdl_file, ", ");
            writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
         }
         writer->write(hdl_file,  "} == " + output_name + ", ");
         writer->write(hdl_file,  port_name + ", _i_*" + boost::lexical_cast<std::string>(bitsize/8) + ", " + (bitsize==32?"bits32_to_real64":"$bitstoreal")+ "({");
         for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
         {
            if(bitsize_index) writer->write(hdl_file, ", ");
            writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
         }
         writer->write(hdl_file, std::string("}), ") + (bitsize==32?"bits32_to_real64":"$bitstoreal")+ "(" + output_name+ "));\n");

         if(bitsize==32 || bitsize==64)
         {
            writer->write(hdl_file, "$display(\" FP error %f \\n\", compute_ulp"+(bitsize==32?STR(32):STR(64))+"({");
            for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
            {
               if(bitsize_index) writer->write(hdl_file, ", ");
               writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
            }
            writer->write(hdl_file,  "}, " + output_name);
            writer->write(hdl_file, "));\n");
            writer->write(hdl_file, "if (compute_ulp"+(bitsize==32?STR(32):STR(64))+"({");
            for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
            {
               if(bitsize_index) writer->write(hdl_file, ", ");
               writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
            }
            writer->write(hdl_file,  "}, " + output_name);
            writer->write(hdl_file, ") > "+ STR(Param->getOption<double>(OPT_max_ulp)) +")\n");
         }
         else
            THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "floating point precision not yet supported: " + STR(bitsize));

      }
      else
      {
         writer->write(hdl_file, "$display(\" res = %b " + port_name + " = %d " " mem[" + port_name  + " + %d - base_addr] = %d  expected = %d \\n\", ");
         writer->write(hdl_file, "{");
         for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
         {
            if(bitsize_index) writer->write(hdl_file, ", ");
            writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
         }
         writer->write(hdl_file,  "} == " + output_name + ", ");
         writer->write(hdl_file,  port_name + ", _i_*" + boost::lexical_cast<std::string>(bitsize/8) + ", {");
         for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
         {
            if(bitsize_index) writer->write(hdl_file, ", ");
            writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
         }
         writer->write(hdl_file, "}, " + output_name+ ");\n");

         writer->write(hdl_file, "if ({");
         for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
         {
            if(bitsize_index) writer->write(hdl_file, ", ");
            writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
         }
         writer->write(hdl_file,  "} !== " + output_name);
         writer->write(hdl_file, ")\n");
      }
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "successs = 0;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));

      writer->write(hdl_file, "_i_ = _i_ + 1;\n");
      writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write_comment(hdl_file, "skip comments and empty lines\n");
      writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
      writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");

      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");

      writer->write(hdl_file, "if (_ch_ == \"e\")\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write_comment(hdl_file, "error\n");
      writer->write(hdl_file, "$display(\"ERROR - Unknown error while reading the file. Character found: %s\", _ch_);\n");
      writer->write(hdl_file, "$fclose(res_file);\n");
      writer->write(hdl_file, "$fclose(file);\n");
      writer->write(hdl_file, "$finish;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "_ch_ = $fgetc(file);\n");

   }

   if (mod->find_member(RETURN_PORT_NAME, port_o_K, cir))
   {
      std::string output_name = "ex_" + std::string(RETURN_PORT_NAME);
      structural_objectRef return_port = mod->find_member(RETURN_PORT_NAME, port_o_K, cir);

      writer->write(hdl_file, "_i_ = 0;\n");
      writer->write(hdl_file, "while (_ch_ == \"/\" || _ch_ == \"\\n\" || _ch_ == \"o\") ");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "if (_ch_ == \"o\")\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "compare_outputs = 1;\n");
      writer->write(hdl_file, "_r_ = $fscanf(file,\"%b\\n\", " + output_name + "); ");
      writer->write_comment(hdl_file, "expected format: bbb...b (example: 00101110)\n");
      writer->write(hdl_file, "if (_r_ != 1)\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write_comment(hdl_file, "error\n");
      writer->write(hdl_file, "$display(\"ERROR - Unknown error while reading the file. Character found: %s\", _ch_);\n");
      writer->write(hdl_file, "$fclose(res_file);\n");
      writer->write(hdl_file, "$fclose(file);\n");
      writer->write(hdl_file, "$finish;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "$display(\"Value found for output " + output_name + ": %b\", " + output_name + ");\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));

      if(return_port->get_typeRef()->type == structural_type_descriptor::REAL)
      {
         if(GET_TYPE_SIZE(return_port)==32)
         {
            writer->write(hdl_file, "$display(\" " + std::string(RETURN_PORT_NAME) + " = %f   expected = %f \", bits32_to_real64(registered_" + std::string(RETURN_PORT_NAME) + "), bits32_to_real64(ex_" + std::string(RETURN_PORT_NAME) + "));\n");
            writer->write(hdl_file, "$display(\" FP error %f \\n\", compute_ulp32(registered_" + std::string(RETURN_PORT_NAME) + ", " + output_name + "));\n");
            writer->write(hdl_file, "if (compute_ulp32(registered_" + std::string(RETURN_PORT_NAME) + ", " + output_name + ") > "+ STR(Param->getOption<double>(OPT_max_ulp)) +")\n");
         }
         else if(GET_TYPE_SIZE(return_port)==64)
         {
            writer->write(hdl_file, "$display(\" " + std::string(RETURN_PORT_NAME) + " = %f   expected = %f \", $bitstoreal(registered_" + std::string(RETURN_PORT_NAME) + "), $bitstoreal(ex_" + std::string(RETURN_PORT_NAME) + "));\n");
            writer->write(hdl_file, "$display(\" FP error %f \\n\", compute_ulp64(registered_" + std::string(RETURN_PORT_NAME) + ", " + output_name + "));\n");
            writer->write(hdl_file, "if (compute_ulp64(registered_" + std::string(RETURN_PORT_NAME) + ", " + output_name + ") > "+ STR(Param->getOption<double>(OPT_max_ulp)) +")\n");
         }
         else
            THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "floating point precision not yet supported: " + STR(GET_TYPE_SIZE(return_port)));
      }
      else
      {
         writer->write(hdl_file, "$display(\" " + std::string(RETURN_PORT_NAME) + " = %d   expected = %d \\n\", registered_" + std::string(RETURN_PORT_NAME) + ", ex_" + std::string(RETURN_PORT_NAME) + ");\n");
         writer->write(hdl_file, "if (registered_" + std::string(RETURN_PORT_NAME) + " !== " + output_name + ")\n");
      }
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "successs = 0;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));

      writer->write(hdl_file, "_i_ = _i_ + 1;\n");
      writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write_comment(hdl_file, "skip comments and empty lines\n");
      writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
      writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");

      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");

      writer->write(hdl_file, "if (_ch_ == \"e\")\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write_comment(hdl_file, "error\n");
      writer->write(hdl_file, "$display(\"ERROR - Unknown error while reading the file. Character found: %s\", _ch_);\n");
      writer->write(hdl_file, "$fclose(res_file);\n");
      writer->write(hdl_file, "$fclose(file);\n");
      writer->write(hdl_file, "$finish;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
   }
   writer->write_comment(hdl_file, "Compare output with expected output (if given)\n");
   writer->write(hdl_file, "if (compare_outputs == 1)\n");

   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"Simulation ended after %d cycles.\\n\", sim_time);\n");
   writer->write(hdl_file, "if (successs == 1)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"Simulation completed with success\\n\");\n");
   writer->write(hdl_file, "$fwrite(res_file, \"1\\t\");\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"Simulation FAILED\\n\");\n");
   writer->write(hdl_file, "$fwrite(res_file, \"0\\t\");\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");


   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"Simulation ended after %d cycles (no expected outputs specified).\\n\", sim_time);\n");
   writer->write(hdl_file, "$fwrite(res_file, \"-\\t\");\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "$fwrite(res_file, \"%d\\n\", sim_time);\n");
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));

   testbench_controller_machine(hdl_file, writer, first_valid_input);

   write_module_end(hdl_file, writer);
}


std::string MinimalInterfaceTestbench::memory_aggregate_slices(unsigned int i, long long int bitsize, long long int Mout_addr_ram_bitsize) const
{
   std::string mem_aggregate = "{";
   for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
   {
      if(bitsize_index) mem_aggregate += ", ";
      mem_aggregate += "mem[Mout_addr_ram["+boost::lexical_cast<std::string>((i+1)*Mout_addr_ram_bitsize-1)+ ":"+boost::lexical_cast<std::string>(i*Mout_addr_ram_bitsize)+"] + " + STR((bitsize - bitsize_index)/8-1) + " - base_addr]";
   }
   mem_aggregate += "}";

   return mem_aggregate;
}


/*
 * WishboneInterfaceTestbench Implementation.
 *
 * The following function are helpers for the real implementation.
 * They avoid having unnecessary private members that will cause recompilation
 * of dependent modules due to possible changes to the interface.
 */
static void write_wishbone_input_signal_declaration(std::ostream &hdl_file, language_writerRef writer,
                                                    const structural_objectRef &cir,
                                                    const tree_managerConstRef TreeM)
{
   module *mod = GetPointer<module>(cir);

   /// write input signals declaration
   if (mod->get_in_port_size())
   {
      writer->write_comment(hdl_file, "INPUT SIGNALS\n");
      for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
      {
         const structural_objectRef& port_obj = mod->get_in_port(i);
         if(CLOCK_PORT_NAME == port_obj->get_id())
            writer->write(hdl_file, "input ");
         else if(GetPointer<port_o>(port_obj)->get_is_memory() || WB_ACKIM_PORT_NAME == port_obj->get_id())
            writer->write(hdl_file, "wire ");
         else
            writer->write(hdl_file, "reg ");

         writer->write(hdl_file, writer->type_converter(port_obj->get_typeRef()) + writer->type_converter_size(port_obj));
         if (port_obj->get_kind() != port_o_K && port_obj->get_typeRef()->type != structural_type_descriptor::VECTOR_BOOL)
         {
            unsigned int lsb = GetPointer<port_o>(mod->get_in_port(i))->get_lsb();
            writer->write(hdl_file, "[" + STR(GetPointer<port_o>(mod->get_in_port(i))->get_ports_size() - 1 + lsb) + ":" + STR(lsb) + "] ");
         }
         writer->write(hdl_file, HDL_manager::convert_to_identifier(writer.get(), mod->get_in_port(i)->get_id()) + ";\n");
         if (port_obj->get_typeRef()->treenode > 0 && tree_helper::is_a_pointer(TreeM, port_obj->get_typeRef()->treenode))
         {
            unsigned int pt_type_index = tree_helper::get_pointed_type(TreeM, tree_helper::get_type_index(TreeM,port_obj->get_typeRef()->treenode));
            tree_nodeRef pt_node = TreeM->get_tree_node_const(pt_type_index);
            if(GetPointer<array_type>(pt_node))
            {
               while(GetPointer<array_type>(pt_node))
               {
                  pt_type_index = GET_INDEX_NODE(GetPointer<array_type>(pt_node)->elts);
                  pt_node = GET_NODE(GetPointer<array_type>(pt_node)->elts);
               }
            }
            long long int bitsize = tree_helper::size(TreeM, pt_type_index);
            writer->write(hdl_file,
               "reg [" + STR(bitsize-1)
               + ":0] ex_" + port_obj->get_id() + ";\n");
         }
      }
      writer->write(hdl_file, "\n");
   }
}


static void write_wishbone_callFSM_signal_declaration(std::ostream& hdl_file, language_writerRef writer)
{
   writer->write_comment(hdl_file, "State machine present and next state\n");
   writer->write(hdl_file, "reg [7:0] call_state;\n");
   writer->write(hdl_file, "reg [7:0] next_call_state;\n");

   writer->write(hdl_file, "reg cyc_fsm;\n");
   writer->write(hdl_file, "reg stb_fsm;\n");
   writer->write(hdl_file, "reg we_fsm;\n");
   writer->write(hdl_file, "reg done_port;\n");
   writer->write(hdl_file, "reg start_next_sim;\n");
   writer->write(hdl_file, "reg start_port_next;\n");
}


static void write_wishbone_callFSM(std::ostream& hdl_file, language_writerRef writer, 
                                   std::vector<std::string> &parameterNames, bool has_return)
{
   // sequential logic
   writer->write(hdl_file, "always @(posedge clock)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "if (reset == 1'b0)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "call_state <= 8'd0;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "call_state <= next_call_state;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");

   // State transition table
   writer->write(hdl_file, "always @(call_state or start_port or ack_os or irq)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "next_call_state = 8'd0;\n");
   writer->write(hdl_file, "case (call_state)\n");

   writer->write(hdl_file, "8'd0:\n");
   writer->write(hdl_file, "if (start_port == 1'b1)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "next_call_state = 8'd1;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "next_call_state = 8'd0;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   
   unsigned int state = 0;
   for (std::vector<std::string>::reverse_iterator itr = parameterNames.rbegin(),
                                                   end = parameterNames.rend(); itr != end; ++itr)
   {
      if (*itr != RETURN_PORT_NAME)
      {
         writer->write_comment(hdl_file, "Send parameter " + *itr + "\n");
         writer->write(hdl_file, "8'd" + boost::lexical_cast<std::string>(++state) + ":\n");
         writer->write(hdl_file, "if (ack_os == 1'b1)\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, "next_call_state = 8'd" + boost::lexical_cast<std::string>(state + 1) + ";\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");
         writer->write(hdl_file, "else\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, "next_call_state = 8'd" + boost::lexical_cast<std::string>(state) + ";\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");
      }
   }
   writer->write(hdl_file, "8'd" + boost::lexical_cast<std::string>(++state) + ":\n");
   writer->write(hdl_file, "if (irq == 1'b1)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "next_call_state = 8'd" + boost::lexical_cast<std::string>(state + 1) + ";\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "next_call_state = 8'd" + boost::lexical_cast<std::string>(state) + ";\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");

   if (has_return) {
      writer->write_comment(hdl_file, "Retrieve " + std::string(RETURN_PORT_NAME) + "\n");
      writer->write(hdl_file, "8'd" + boost::lexical_cast<std::string>(++state) + ":\n");
      writer->write(hdl_file, "if (ack_os == 1'b1)\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, "next_call_state = 8'd" + boost::lexical_cast<std::string>(state + 1) + ";\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, "next_call_state = 8'd" + boost::lexical_cast<std::string>(state) + ";\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
   }

   writer->write(hdl_file, "8'd" + boost::lexical_cast<std::string>(++state) + ":\n");
   writer->write(hdl_file, "if (start_port == 1'b1)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "next_call_state = 8'd0;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "next_call_state = 8'd" + boost::lexical_cast<std::string>(state) + ";\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");

   writer->write(hdl_file, "default: next_call_state = 8'd0;\n");
   writer->write(hdl_file, "endcase\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");

   // Output function
   writer->write(hdl_file, "always @(posedge clock)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "if (reset == 1'b0)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "case (call_state)\n");

   writer->write(hdl_file, "8'd0:\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "cyc_fsm = 1'b0;\n");
   writer->write(hdl_file, "stb_fsm = 1'b0;\n");
   writer->write(hdl_file, "we_fsm = 1'b0;\n");
   writer->write(hdl_file, "done_port = 1'b0;\n");
   writer->write(hdl_file, "sel_is = 8'b11111111;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");

   state = 0;
   for (std::vector<std::string>::reverse_iterator itr = parameterNames.rbegin(),
                                                   end = parameterNames.rend(); itr != end; ++itr)
   {
      if (*itr != RETURN_PORT_NAME)
      {
         writer->write_comment(hdl_file, "Send parameter " + *itr + "\n");
         writer->write(hdl_file, "8'd" + boost::lexical_cast<std::string>(++state) + ":\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, "addr_is = ADDRESS_OFFSET_" + *itr + ";\n");
         writer->write(hdl_file, "dat_is = " + HDL_manager::convert_to_identifier(writer.get(), *itr) + ";\n");
         writer->write(hdl_file, "cyc_fsm = 1'b1;\n");
         writer->write(hdl_file, "stb_fsm = 1'b1;\n");
         writer->write(hdl_file, "we_fsm = 1'b1;\n");
         writer->write(hdl_file, "sel_is = 8'b11111111;\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");
      }
   }
   writer->write(hdl_file, "8'd" + boost::lexical_cast<std::string>(++state) + ":\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "cyc_fsm = 1'b0;\n");
   writer->write(hdl_file, "stb_fsm = 1'b0;\n");
   writer->write(hdl_file, "we_fsm = 1'b0;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");

   if (has_return) {
      writer->write_comment(hdl_file, "Retrieve " + std::string(RETURN_PORT_NAME) + "\n");
      writer->write(hdl_file, "8'd" + boost::lexical_cast<std::string>(++state) + ":\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, "addr_is = ADDRESS_OFFSET_" + std::string(RETURN_PORT_NAME) + ";\n");
      writer->write(hdl_file, "cyc_fsm = 1'b1;\n");
      writer->write(hdl_file, "stb_fsm = 1'b1;\n");
      writer->write(hdl_file, "we_fsm = 1'b0;\n");
      writer->write(hdl_file, "sel_is = 8'b11111111;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
   }

   writer->write(hdl_file, "8'd" + boost::lexical_cast<std::string>(++state) + ":\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "cyc_fsm = 1'b0;\n");
   writer->write(hdl_file, "stb_fsm = 1'b0;\n");
   writer->write(hdl_file, "we_fsm = 1'b0;\n");
   writer->write(hdl_file, "done_port = 1'b1;\n");
   if (has_return) {
      writer->write(hdl_file, "registered_" + std::string(RETURN_PORT_NAME) + " = dat_os;\n");
   }
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");

   writer->write(hdl_file, "default:\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "endcase\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n\n");

   writer->write(hdl_file, "always @(posedge clock)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "we_is = we_fsm & !ack_os;\n");
   writer->write(hdl_file, "cyc_is = cyc_fsm & !ack_os;\n");
   writer->write(hdl_file, "stb_is = stb_fsm & !ack_os;\n\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
}


static void write_wishbone_memory_handler(std::ostream& hdl_file, language_writerRef writer,
                                          unsigned int dataBusBitSize)
{
   unsigned int dataBusByteSize = dataBusBitSize >> 3;

   writer->write(hdl_file, "always @(posedge clock)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "if (!reset)" + STR(STD_OPENING_CHAR) +"\n");
   writer->write(hdl_file, "ack_delayed <= 1'b0;" + STR(STD_CLOSING_CHAR) + "\n");
   writer->write(hdl_file, "else" + STR(STD_OPENING_CHAR) + "\n");
   writer->write(hdl_file, "ack_delayed <= stb_om & cyc_om & !ack_im;");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR) + "\n");
   writer->write(hdl_file, "end\n");

   writer->write(hdl_file, "assign ack_im = ack_delayed;\n");

   writer->write_comment(hdl_file, "memory read/write controller\n");
   writer->write(hdl_file, "always @(posedge clock)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "if (cyc_om & stb_om)\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "if (we_om && base_addr <= addr_om && addr_om <= (base_addr + MEMSIZE - " + STR(dataBusByteSize) + "))\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "case (sel_om)\n");
   unsigned int caseValue = 0;
   for (unsigned int j = 0; j < dataBusByteSize; ++j) {
      caseValue = (caseValue | (1U << j));
      writer->write(hdl_file, boost::lexical_cast<std::string>(dataBusByteSize) + "'d"
                    + boost::lexical_cast<std::string>(caseValue) + ":\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "begin\n");
      for (unsigned int i = 0; i <= j; ++i) {
         writer->write(hdl_file, "mem[(addr_om - base_addr) + "
                       + STR(i) + "] = dat_om["
                       + STR((i * 8) + 7) + ":"
                       + STR((i * 8)) + "];\n");
      }
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
   }
   writer->write(hdl_file, "default:\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "$display(\"ERROR - Unsupported sel value.\");\n"
                 "$fclose(res_file);\n$fclose(file);\n$finish;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "endcase\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "if (!we_om && base_addr <= addr_om && addr_om <= (base_addr + MEMSIZE - " + STR(dataBusByteSize) + "))\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "case (sel_om)\n");
   caseValue = 0;
   for (unsigned int j = 0; j < dataBusByteSize; ++j) {
      caseValue = (caseValue | (1U << j));
      writer->write(hdl_file, boost::lexical_cast<std::string>(dataBusByteSize) + "'d"
                    + boost::lexical_cast<std::string>(caseValue) + ":\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "begin\n");
      for (unsigned int i = 0; i <= j; ++i) {
         writer->write(hdl_file, "dat_im["
                       + STR((i * 8) + 7) + ":"
                       + STR((i * 8)) + "] = mem[(addr_om - base_addr) + "
                       + STR(i) + "];\n");
      }
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
   }
   writer->write(hdl_file, "default:\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, "$display(\"ERROR - Unsupported sel value.\");\n"
                 "$fclose(res_file);\n$fclose(file);\n$finish;\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "endcase\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n\n");
}


static void write_wishbone_output_signal_declaration(std::ostream &hdl_file, language_writerRef writer,
                                                     const structural_objectRef &cir, bool &withMemory)
{
   module * mod = GetPointer<module>(cir);

   /// write output signals declaration
   if (mod->get_out_port_size())
   {
      writer->write_comment(hdl_file, "OUTPUT SIGNALS\n");
      for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
      {
         writer->write(hdl_file, "wire " + writer->type_converter(mod->get_out_port(i)->get_typeRef()) + writer->type_converter_size(mod->get_out_port(i)));
         if (mod->get_out_port(i)->get_kind() != port_o_K && mod->get_out_port(i)->get_typeRef()->type != structural_type_descriptor::VECTOR_BOOL)
         {
            unsigned int lsb = GetPointer<port_o>(mod->get_out_port(i))->get_lsb();
            writer->write(hdl_file, "[" + STR(GetPointer<port_o>(mod->get_out_port(i))->get_ports_size() - 1 + lsb) + ":" + STR(lsb) + "] ");
         }
         std::string portId = mod->get_out_port(i)->get_id();
         withMemory |= portId == WB_STBOM_PORT_NAME || portId == WB_CYCOM_PORT_NAME;
         writer->write(hdl_file, HDL_manager::convert_to_identifier(writer.get(), portId) + ";\n");
      }
      writer->write(hdl_file, "\n");
   }
}


void WishboneInterfaceTestbench::write_underling_testbench(std::ostream& hdl_file, language_writerRef writer,
                                                          const structural_objectRef &cir, double target_period,
                                                          std::string simulation_values_path, bool generate_vcd_output,
                                                          bool xilinx_isim, const tree_managerConstRef TreeM) const
{
   const memoryRef mem = AppM->Rmem;
   std::string input_values_filename = simulation_values_path;

   /// generation of VCD output
   std::string vcd_output_filename = output_directory + "test.vcd";

   write_hdl_testbench_prolog(hdl_file, writer, target_period, Param);

   write_module_begin(hdl_file, writer, cir);

   write_compute_ulps_functions(hdl_file, writer);

   write_auxiliary_signal_declaration(hdl_file, writer, cir);
   write_wishbone_callFSM_signal_declaration(hdl_file, writer);

   writer->write(hdl_file, "reg " + std::string(START_PORT_NAME) + ";\n");
   writer->write(hdl_file, "reg ack_delayed;\n");
   write_wishbone_input_signal_declaration(hdl_file, writer, cir, TreeM);

   bool withMemory = false;
   write_wishbone_output_signal_declaration(hdl_file, writer, cir, withMemory);

   // parameter
   writer->write_comment(hdl_file, "Function parameters\n");

   const unsigned int topFunctionId = AppM->CGetCallGraphManager()->GetRootFunction();
   const BehavioralHelperConstRef behavioral_helper = AppM->CGetFunctionBehavior(topFunctionId)->CGetBehavioralHelper();
   const std::map<unsigned int, memory_symbolRef>& parameters = mem->get_function_parameters(topFunctionId);
   std::vector<std::string> parameterNames;
   std::string topFunctionName = behavioral_helper->PrintVariable(topFunctionId);
   for (std::map<unsigned int, memory_symbolRef>::const_iterator It = parameters.begin(), End = parameters.end(); It != End; ++It)
   {
      unsigned int var = It->first;
      std::string variableName = var == behavioral_helper->GetFunctionReturnType(topFunctionId) ? RETURN_PORT_NAME : behavioral_helper->PrintVariable(var);
      parameterNames.push_back(variableName);
      unsigned int variableType = tree_helper::get_type_index(TreeM, var);
      unsigned int variableBitSize = tree_helper::size(TreeM, variableType);
      unsigned int expectedVariableBitSize = tree_helper::size(TreeM, variableType);
      if(tree_helper::is_a_pointer(TreeM, var))
      {
         unsigned int pt_type_index = tree_helper::get_pointed_type(TreeM, tree_helper::get_type_index(TreeM,var));
         tree_nodeRef pt_node = TreeM->get_tree_node_const(pt_type_index);
         if(GetPointer<array_type>(pt_node))
         {
            while(GetPointer<array_type>(pt_node))
            {
               pt_type_index = GET_INDEX_NODE(GetPointer<array_type>(pt_node)->elts);
               pt_node = GET_NODE(GetPointer<array_type>(pt_node)->elts);
            }
         }
         expectedVariableBitSize = tree_helper::size(TreeM, pt_type_index);
      }
      if (var == behavioral_helper->GetFunctionReturnType(topFunctionId))
      {
         writer->write_comment(hdl_file, variableName + " -> " + STR(It->first) + "\n");
         writer->write(hdl_file, "reg [" + STR(expectedVariableBitSize) + "-1:0] ex_" + variableName + ";\n");
         writer->write(hdl_file, "reg [" + STR(variableBitSize) + "-1:0] registered_" + variableName + ";\n");
         writer->write(hdl_file, "parameter ADDRESS_OFFSET_" + variableName + " = "
                       + STR(mem->get_parameter_base_address(var)) + ";\n");
      }
      else
      {
         writer->write_comment(hdl_file, variableName + " -> " + STR(var) + "\n");
         writer->write(hdl_file, "reg [" + STR(variableBitSize) + "-1:0] " +
                       HDL_manager::convert_to_identifier(writer.get(), variableName) + ";\n");
         writer->write(hdl_file, "parameter ADDRESS_OFFSET_" + variableName + " = "
                       + STR(mem->get_parameter_base_address(var)) + ";\n");
         if (tree_helper::is_a_pointer(TreeM, var))
         {
            writer->write(hdl_file, "reg [" + STR(expectedVariableBitSize) + "-1:0] ex_" + variableName + ";\n");
         }
      }
   }

   /// write inout signals declaration
   module *mod = GetPointer<module>(cir);
   if (mod->get_in_out_port_size())
   {
      THROW_ERROR("Module with IO ports cannot synthesized");
   }

   write_module_instantiation(hdl_file, writer, cir, xilinx_isim);

   if(xilinx_isim)
      writer->write(hdl_file, "glbl #("+ STR(target_period/2*1000) + ", 0) glbl();");

   begin_initial_block(hdl_file, writer);

   /// VCD output generation (optional)
   if(generate_vcd_output)
   {
      writer->write_comment(hdl_file, "VCD file generation\n");
      writer->write(hdl_file, "$dumpfile(\"" + vcd_output_filename + "\");\n");
      writer->write(hdl_file, "$dumpvars;\n");
   }

   /// open file with values
   open_value_file(hdl_file, writer, input_values_filename);

   /// open file with results
   std::string result_file = Param->getOption<std::string>(OPT_simulation_output);
   open_result_file(hdl_file, writer, result_file);

   /// auxiliary variables initialization
   initialize_auxiliary_variables(hdl_file, writer);

   initialize_input_signals(hdl_file, writer, cir, TreeM);

   memory_initialization(hdl_file, writer);

   end_initial_block(hdl_file, writer);

   // file reading operations
   begin_file_reading_operation(hdl_file, writer);

   reading_base_memory_address_from_file(hdl_file, writer);

   memory_initialization_from_file(hdl_file, writer);

   bool first_valid_input = true;
   for (std::vector<std::string>::size_type i = 1; i < parameterNames.size(); ++i)
   {
      if (parameterNames[i] != topFunctionName && parameterNames[i] != RETURN_PORT_NAME) {
         std::string input_name = HDL_manager::convert_to_identifier(writer.get(), parameterNames[i]);
         read_input_value_from_file(hdl_file, writer, input_name, first_valid_input);
      }
   }
   if (!first_valid_input) writer->write(hdl_file, "_ch_ = $fgetc(file);\n");

   end_file_reading_operation(hdl_file, writer);

   // memory read write process
   if (withMemory) {
      structural_objectRef dat_om = cir->find_member(WB_DATOM_PORT_NAME, port_o_K, cir);
      unsigned int data_bus_bitsize = GET_TYPE_SIZE(dat_om);
      write_wishbone_memory_handler(hdl_file, writer, data_bus_bitsize);
   }

   // Call FSM
   write_wishbone_callFSM(hdl_file, writer, parameterNames, behavioral_helper->GetFunctionReturnType(topFunctionId));

   // Testbench FSM
   testbench_controller_machine(hdl_file, writer, first_valid_input);

   write_max_simulation_time_control(hdl_file, writer);

   writer->write_comment(hdl_file, "Check done_port signal\n");
   writer->write(hdl_file, "always @(negedge clock)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "if (done_port == 1)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "endTime = $time;\n\n");
   writer->write_comment(hdl_file, "Simulation time (clock cycles) = 1+(time elapsed (seconds) / clock cycle (seconds per cycle)) (until done is 1)\n");
   writer->write(hdl_file, "sim_time = (endTime + `HALF_CLOCK_PERIOD - startTime)/`CLOCK_PERIOD;\n\n");

   writer->write(hdl_file, "successs = 1;\n");
   writer->write(hdl_file, "compare_outputs = 0;\n");

   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));

   writer->write(hdl_file, "always @(negedge clock)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "if (start_results_comparison == 1)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));

   // output check.
   first_valid_input = true;
   for (std::map<unsigned int, memory_symbolRef>::const_iterator It = parameters.begin(),
        End = parameters.end(); It != End; ++It)
   {
      unsigned int var = It->first;
      if (tree_helper::is_a_pointer(TreeM, var) && var != behavioral_helper->GetFunctionReturnType(topFunctionId))
      {
         std::string variableName = behavioral_helper->PrintVariable(var);
         std::string port_name = HDL_manager::convert_to_identifier(writer.get(), variableName);
         std::string output_name = "ex_" + variableName;
         unsigned int pt_type_index = tree_helper::get_pointed_type(TreeM, tree_helper::get_type_index(TreeM,var));
         tree_nodeRef pt_node = TreeM->get_tree_node_const(pt_type_index);
         if(GetPointer<array_type>(pt_node))
         {
            while(GetPointer<array_type>(pt_node))
            {
               pt_type_index = GET_INDEX_NODE(GetPointer<array_type>(pt_node)->elts);
               pt_node = GET_NODE(GetPointer<array_type>(pt_node)->elts);
            }
         }
         long long int bitsize = tree_helper::size(TreeM, pt_type_index);
         bool is_real = tree_helper::is_real(TreeM, pt_type_index);

         writer->write(hdl_file, "\n");
         writer->write_comment(hdl_file, "OPTIONAL - Read a value for " + output_name + " --------------------------------------------------------------\n");

         writer->write(hdl_file, "_i_ = 0;\n");
         writer->write(hdl_file, "while (_ch_ == \"/\" || _ch_ == \"\\n\" || _ch_ == \"o\") ");
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write(hdl_file, "if (_ch_ == \"o\")\n");
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write(hdl_file, "compare_outputs = 1;\n");
         writer->write(hdl_file, "_r_ = $fscanf(file,\"%b\\n\", " + output_name + "); ");
         writer->write_comment(hdl_file, "expected format: bbb...b (example: 00101110)\n");
         writer->write(hdl_file, "if (_r_ != 1)\n");
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write_comment(hdl_file, "error\n");
         writer->write(hdl_file, "$display(\"ERROR - Unknown error while reading the file. Character found: %c\", _ch_);\n");
         writer->write(hdl_file, "$fclose(res_file);\n");
         writer->write(hdl_file, "$fclose(file);\n");
         writer->write(hdl_file, "$finish;\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");
         writer->write(hdl_file, "else\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write(hdl_file, "$display(\"Value found for output " + output_name + ": %b\", " + output_name + ");\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         if(is_real)
         {
            writer->write(hdl_file, "$display(\" res = %b " + port_name + " = %d " " mem[" + port_name  + " + %d - base_addr] = %f  expected = %f \", ");
            writer->write(hdl_file, "{");
            for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
            {
               if(bitsize_index) writer->write(hdl_file, ", ");
               writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
            }
            writer->write(hdl_file,  "} == " + output_name + ", ");
            writer->write(hdl_file,  port_name + ", _i_*" + boost::lexical_cast<std::string>(bitsize/8) + ", " + (bitsize==32?"bits32_to_real64":"$bitstoreal")+ "({");
            for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
            {
               if(bitsize_index) writer->write(hdl_file, ", ");
               writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
            }
            writer->write(hdl_file, std::string("}), ") + (bitsize==32?"bits32_to_real64":"$bitstoreal")+ "(" + output_name+ "));\n");

            if(bitsize==32 || bitsize==64)
            {
               writer->write(hdl_file, "$display(\" FP error %f \\n\", compute_ulp"+(bitsize==32?STR(32):STR(64))+"({");
               for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
               {
                  if(bitsize_index) writer->write(hdl_file, ", ");
                  writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
               }
               writer->write(hdl_file,  "}, " + output_name);
               writer->write(hdl_file, "));\n");
               writer->write(hdl_file, "if (compute_ulp"+(bitsize==32?STR(32):STR(64))+"({");
               for(unsigned int bitsize_index=0; bitsize_index < bitsize; bitsize_index = bitsize_index + 8)
               {
                  if(bitsize_index) writer->write(hdl_file, ", ");
                  writer->write(hdl_file, "mem[" + port_name + " + _i_*" + boost::lexical_cast<std::string>(bitsize/8) + " + " + boost::lexical_cast<std::string>((bitsize - bitsize_index)/8-1) + " - base_addr]");
               }
               writer->write(hdl_file,  "}, " + output_name);
               writer->write(hdl_file, ") > "+ STR(Param->getOption<double>(OPT_max_ulp)) +")\n");
            }
            else
               THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "floating point precision not yet supported: " + STR(bitsize));
         }
         else
         {
            writer->write(hdl_file, "$display(\" res = %b " + port_name + " = %d " " mem[" + port_name  + " + %d - base_addr] = %d  expected = %d \\n\", ");
            writer->write(hdl_file, "{");
            long long int bytesize = bitsize / 8;
            for(unsigned int index=0; index < bytesize; ++index)
            {
               if(index) writer->write(hdl_file, ", ");
               writer->write(hdl_file, "mem[(" + port_name + " - base_addr) + _i_*" + boost::lexical_cast<std::string>(bytesize) + " + "
                             + boost::lexical_cast<std::string>(bytesize - index - 1) +"]");
            }
            writer->write(hdl_file,  "} == " + output_name + ", ");
            writer->write(hdl_file,  port_name + ", _i_*" + boost::lexical_cast<std::string>(bitsize/8) + ", {");
            for(unsigned int index=0; index < bytesize; ++index)
            {
               if(index) writer->write(hdl_file, ", ");
               writer->write(hdl_file, "mem[(" + port_name + " - base_addr) + _i_*" + boost::lexical_cast<std::string>(bytesize) + " + "
                             + boost::lexical_cast<std::string>(bytesize - index - 1) + "]");
            }
            writer->write(hdl_file, "}, " + output_name+ ");\n");

            writer->write(hdl_file, "if ({");
            for(unsigned int index=0; index < bytesize; ++index)
            {
               if(index) writer->write(hdl_file, ", ");
               writer->write(hdl_file, "mem[(" + port_name + " - base_addr) + _i_*" + boost::lexical_cast<std::string>(bytesize) + " + "
                             + boost::lexical_cast<std::string>(bytesize - index - 1) + "]");
            }
            writer->write(hdl_file,  "} !== " + output_name);
            writer->write(hdl_file, ")\n");
         }
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write(hdl_file, "successs = 0;\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));

         writer->write(hdl_file, "_i_ = _i_ + 1;\n");
         writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");
         writer->write(hdl_file, "else\n");
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write_comment(hdl_file, "skip comments and empty lines\n");
         writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
         writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");

         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");

         writer->write(hdl_file, "if (_ch_ == \"e\")\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "else\n");
         writer->write(hdl_file, "begin\n");
         writer->write(hdl_file, STR(STD_OPENING_CHAR));
         writer->write_comment(hdl_file, "error\n");
         writer->write(hdl_file, "$display(\"ERROR - Unknown error while reading the file. Character found: %c\", _ch_);\n");
         writer->write(hdl_file, "$fclose(res_file);\n");
         writer->write(hdl_file, "$fclose(file);\n");
         writer->write(hdl_file, "$finish;\n");
         writer->write(hdl_file, STR(STD_CLOSING_CHAR));
         writer->write(hdl_file, "end\n");
      }
   }

   if (unsigned int returnIndex = behavioral_helper->GetFunctionReturnType(topFunctionId))
   {
      std::string output_name = "ex_" + std::string(RETURN_PORT_NAME);

      writer->write(hdl_file, "_i_ = 0;\n");
      writer->write(hdl_file, "while (_ch_ == \"/\" || _ch_ == \"\\n\" || _ch_ == \"o\") ");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "if (_ch_ == \"o\")\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "compare_outputs = 1;\n");
      writer->write(hdl_file, "_r_ = $fscanf(file,\"%b\\n\", " + output_name + "); ");
      writer->write_comment(hdl_file, "expected format: bbb...b (example: 00101110)\n");
      writer->write(hdl_file, "if (_r_ != 1)\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write_comment(hdl_file, "error\n");
      writer->write(hdl_file, "$display(\"ERROR - Unknown error while reading the file. Character found: %c\", _ch_);\n");
      writer->write(hdl_file, "$fclose(res_file);\n");
      writer->write(hdl_file, "$fclose(file);\n");
      writer->write(hdl_file, "$finish;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "$display(\"Value found for output " + output_name + ": %b\", " + output_name + ");\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      if(tree_helper::is_real(TreeM, returnIndex))
      {
         if(tree_helper::size(TreeM, returnIndex)==32)
         {
            writer->write(hdl_file, "$display(\" " + std::string(RETURN_PORT_NAME) + " = %f   expected = %f \", bits32_to_real64(registered_" + std::string(RETURN_PORT_NAME) + "), bits32_to_real64(ex_" + std::string(RETURN_PORT_NAME) + "));\n");
            writer->write(hdl_file, "$display(\" FP error %f \\n\", compute_ulp32(registered_" + std::string(RETURN_PORT_NAME) + ", " + output_name + "));\n");
            writer->write(hdl_file, "if (compute_ulp32(registered_" + std::string(RETURN_PORT_NAME) + ", " + output_name + ") > "+ STR(Param->getOption<double>(OPT_max_ulp)) +")\n");
         }
         else if(tree_helper::size(TreeM, returnIndex)==64)
         {
            writer->write(hdl_file, "$display(\" " + std::string(RETURN_PORT_NAME) + " = %f   expected = %f \", $bitstoreal(registered_" + std::string(RETURN_PORT_NAME) + "), $bitstoreal(ex_" + std::string(RETURN_PORT_NAME) + "));\n");
            writer->write(hdl_file, "$display(\" FP error %f \\n\", compute_ulp64(registered_" + std::string(RETURN_PORT_NAME) + ", " + output_name + "));\n");
            writer->write(hdl_file, "if (compute_ulp64(registered_" + std::string(RETURN_PORT_NAME) + ", " + output_name + ") > "+ STR(Param->getOption<double>(OPT_max_ulp)) +")\n");
         }
         else
            THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "floating point precision not yet supported: " + STR(tree_helper::size(TreeM, returnIndex)));
      }
      else
      {
         writer->write(hdl_file, "$display(\" " + std::string(RETURN_PORT_NAME) + " = %d   expected = %d \\n\", registered_" + std::string(RETURN_PORT_NAME) + ", ex_" + std::string(RETURN_PORT_NAME) + ");\n");
         writer->write(hdl_file, "if (registered_" + std::string(RETURN_PORT_NAME) + " !== " + output_name + ")\n");
      }
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "successs = 0;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));

      writer->write(hdl_file, "_i_ = _i_ + 1;\n");
      writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write_comment(hdl_file, "skip comments and empty lines\n");
      writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
      writer->write(hdl_file, "_ch_ = $fgetc(file);\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");

      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");

      writer->write(hdl_file, "if (_ch_ == \"e\")\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write(hdl_file, "_r_ = $fgets(line, file);\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "else\n");
      writer->write(hdl_file, "begin\n");
      writer->write(hdl_file, STR(STD_OPENING_CHAR));
      writer->write_comment(hdl_file, "error\n");
      writer->write(hdl_file, "$display(\"ERROR - Unknown error while reading the file. Character found: %c\", _ch_);\n");
      writer->write(hdl_file, "$fclose(res_file);\n");
      writer->write(hdl_file, "$fclose(file);\n");
      writer->write(hdl_file, "$finish;\n");
      writer->write(hdl_file, STR(STD_CLOSING_CHAR));
      writer->write(hdl_file, "end\n");
   }
   
   writer->write_comment(hdl_file, "Compare output with expected output (if given)\n");
   writer->write(hdl_file, "if (compare_outputs == 1)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"Simulation ended after %d cycles.\\n\", sim_time);\n");
   writer->write(hdl_file, "if (successs == 1)\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"Simulation completed with success\\n\");\n");
   writer->write(hdl_file, "$fwrite(res_file, \"1\\t\");\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"Simulation FAILED\\n\");\n");
   writer->write(hdl_file, "$fwrite(res_file, \"0\\t\");\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "else\n");
   writer->write(hdl_file, "begin\n");
   writer->write(hdl_file, STR(STD_OPENING_CHAR));
   writer->write(hdl_file, "$display(\"Simulation ended after %d cycles (no expected outputs specified).\\n\", sim_time);\n");
   writer->write(hdl_file, "$fwrite(res_file, \"-\\t\");\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, "$fwrite(res_file, \"%d\\n\", sim_time);\n");
   if(first_valid_input)
   {
      writer->write_comment(hdl_file, "wait a cycle (needed for a correct simulation)\n");
      writer->write(hdl_file, "$fclose(res_file);\n");
      writer->write(hdl_file, "$fclose(file);\n");
      writer->write(hdl_file, "$finish;\n");
   }
   else
   {
      writer->write_comment(hdl_file, "Restart a new computation if possible\n");
   }
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n");
   writer->write(hdl_file, STR(STD_CLOSING_CHAR));
   writer->write(hdl_file, "end\n\n");

   write_module_end(hdl_file, writer);
}
