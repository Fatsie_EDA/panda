/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef HLSTESTBENCH_IMPL_HPP
#define HLSTESTBENCH_IMPL_HPP


class MinimalInterfaceTestbench : public HLStestbench
{
   std::string memory_aggregate_slices(unsigned int i, long long int bitsize, long long int Mout_addr_ram_bitsize) const;

   void write_underling_testbench(std::ostream& hdl_file, language_writerRef writer,
                                  const structural_objectRef &cir, double target_period,
                                  std::string simulation_values_path, bool generate_vcd_output,
                                  bool xilinx_isim, const tree_managerConstRef TreeM) const;
  public:
   MinimalInterfaceTestbench(const ParameterConstRef _Param, const HLS_managerRef _AppM)
         : HLStestbench(_Param, _AppM) {}
};


class WishboneInterfaceTestbench : public HLStestbench
{
   void write_underling_testbench(std::ostream& hdl_file, language_writerRef writer,
                                  const structural_objectRef &cir, double target_period,
                                  std::string simulation_values_path, bool generate_vcd_output,
                                  bool xilinx_isim, const tree_managerConstRef TreeM) const;

  public:
   WishboneInterfaceTestbench(const ParameterConstRef _Param, const HLS_managerRef _AppM)
         : HLStestbench(_Param, _AppM) {}
};


#endif /* HLSTESTBENCH_IMPL_HPP */
