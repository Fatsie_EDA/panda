/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file HLStestbench.hpp
 * @brief Class to compute testbenches for high-level synthesis
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Minutoli <mminutoli@gmail.com>
 * @author Manuel Beniani <manuel.beniani@gmail.com>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */

#ifndef HLS_TESTBENCH_HPP
#define HLS_TESTBENCH_HPP

#define SIMULATION_DEFINE "ONLY_SIMULATION"

#include "refcount.hpp"
#include "application_manager.hpp"

/**
 * @name forward declarations
 */
//@{
CONSTREF_FORWARD_DECL(BehavioralHelper);
CONSTREF_FORWARD_DECL(Parameter);
REF_FORWARD_DECL(HLS_manager);
CONSTREF_FORWARD_DECL(tree_manager);
REF_FORWARD_DECL(technology_manager);
REF_FORWARD_DECL(HLS_constraints);
REF_FORWARD_DECL(structural_object);
REF_FORWARD_DECL(memory);
REF_FORWARD_DECL(language_writer);
//@}

#include <vector>
#include <string>
#include <map>


/**
 * HLStestbench is a Facade class that hide implementation details of the
 * testbench creation.
 */
class HLStestbench
{
      unsigned int parse_test_vectors(const std::string& tb_input_xml, std::vector<std::map<std::string, std::string> >& test_vectors);

      std::string creating_c_testbench(std::map<std::string, std::string>& test_vector, const std::string& input_file, bool is_first_vector);

      void generate_input_file(const std::string& fileName);


      /**
       * Creates the HDL testbench file associated with the given component
       */
      std::string create_HDL_testbench(const std::string & filename_bench, const structural_objectRef &cir, const double target_period, bool xilinx_isim) const;


      /**
       * Write the hdl testbench.
       *
       * This function takes care of generating the hdl testbench top component.
       *
       * @param hdl_file Output stream of the source file of the generated testbench.
       * @param writer Language writer.
       * @param cir Structural object of the design under test.
       * @param target_period Clock period of the target.
       * @param simulation_values_path Path of the values file.
       * @param generate_vcd_output Enable/Disable vcd generation.
       * @param xilinx_isim Xilinx isim?
       * @param TreeM Tree Manager of the design under test.
       */
      virtual void write_hdl_testbench(std::ostream& hdl_file, language_writerRef writer, const structural_objectRef &cir, double target_period, std::string simulation_values_path, bool generate_vcd_output, bool xilinx_isim, const tree_managerConstRef TreeM) const;

      /**
       * This MUST be specialized by subclasses in order to give a
       * proper implementation of the testbench based on the component
       * tested and the interface exposed.
       *
       * The default implementation throws an error.
       *
       * @param hdl_file Output stream of the source file of the generated testbench.
       * @param writer Language writer.
       * @param cir Structural object of the design under test.
       * @param target_period Clock period of the target.
       * @param simulation_values_path Path of the values file.
       * @param generate_vcd_output Enable/Disable vcd generation.
       * @param xilinx_isim Xilinx isim?
       * @param TreeM Tree Manager of the design under test.
       */
      virtual void write_underling_testbench(std::ostream& hdl_file, language_writerRef writer,
                                             const structural_objectRef &cir, double target_period,
                                             std::string simulation_values_path, bool generate_vcd_output,
                                             bool xilinx_isim, const tree_managerConstRef TreeM) const = 0;

      /**
       * Generates and execute the Verilator testbench file associated with the given component
       */
      std::string verilator_testbench(const std::string &filename_bench, const structural_objectRef &cir,
                                      const double target_period) const;

      /**
       * Write the verilator testbench.
       *
       * @param filename_bench Filename of the source file of the generated testbench.
       * @param cir Structural object of the design under test.
       * @param input_file Filename of the stimuli file.
       * @param target_period Target period.
       */
      virtual std::string write_verilator_testbench(const std::string& filename_bench, const structural_objectRef &cir, const std::string& input_file, const double target_period) const;

   protected:
      ///debug level of the class
      int debug_level;

      ///class containing all the parameters
      const ParameterConstRef Param;

      ///information about the target application
      const HLS_managerRef AppM;

      ///output directory
      std::string output_directory;

      /**
       * Constructor.
       *
       * Declared protected to prevent direct instantiation. Use
       * Create() factory methods instead.
       */
      HLStestbench(const ParameterConstRef Param, const HLS_managerRef AppM);

   public:
      /**
       * Destructor.
       */
      virtual ~HLStestbench();

     /**
      * HLSTestbench factory method.
      */
      static HLStestbench * Create(const ParameterConstRef Param, const HLS_managerRef AppM);


      static
      std::string print_var_init(const tree_managerConstRef TreeM, unsigned int var, const memoryRef mem);

      /**
       * Generates the C wrapper, the stimuli and the outputs expected
       */
      void generate_C_testbench();

      /**
       * Generate testbench
       */
      std::string generate(const std::string & filename_bench, const structural_objectRef &cir,
                           const double target_period) const;
};

///Refcount type definition of the class
typedef refcount<HLStestbench> HLStestbenchRef;

#endif
