/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file hls.cpp
 * @brief Data structure implementation for high-level synthesis flow.
 *
 * This file contains all the implementations used by hls class to manage the
 * high level synthesis flow
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Header include
#include "hls.hpp"

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "application_manager.hpp"
#include "call_graph.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"

#include "hls_target.hpp"
#include "allocation.hpp"
#include "technology_manager.hpp"
#include "technology_builtin.hpp"

#include "hls_constraints.hpp"

#include "schedule.hpp"
#include "fu_binding.hpp"
#include "reg_binding.hpp"
#include "conn_binding.hpp"

#include "standard_hls.hpp"
#include "virtual_hls.hpp"

#include "structural_manager.hpp"

#include "BambuParameter.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"
#include "exceptions.hpp"
#include "utility.hpp"

#include <boost/lexical_cast.hpp>

#include "op_graph.hpp"


static
void computeResources(const structural_objectRef circ, const technology_managerRef TM, std::map<std::string, unsigned int>& resources);

/*************************************************************************************************
 *                                                                                               *
 *                                           HLS methods                                         *
 *                                                                                               *
 *************************************************************************************************/

hls::hls(const ParameterConstRef _Param, const application_managerRef _AppM, unsigned int _function_id, const OpVertexSet & _operations, const HLS_targetRef _HLS_T, const HLS_constraintsRef _HLS_C) :
      functionId(_function_id),
      FB(functionId ? _AppM->CGetFunctionBehavior(functionId) : FunctionBehaviorConstRef()),
      operations(_operations),
      HLS_T(_HLS_T),
      HLS_C(_HLS_C),
      Param(_Param),
      debug_level (_Param->getOption<int>(OPT_debug_level)),
      output_level (_Param->getOption<int>(OPT_output_level)),
      HLS_execution_time(0)
{
   THROW_ASSERT(HLS_T, "HLS initalization: HLS_target not available");
   THROW_ASSERT(HLS_C, "HLS initalization: HLS_constraints not available");
   THROW_ASSERT(Param, "HLS initalization: Parameter not available");
}

hls::~hls()
{

}

/// Prints hls informations
void hls::print(std::ostream& os) const
{
   PRINT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "");
   if (Rsch)
   {
      print_scheduling(os);
   }
   if (Rreg)
   {
      print_register_binding(os);
   }
   if (Rconn)
   {
      print_connection_binding(os);
   }
}

/// Prints connection binding
void hls::print_connection_binding(std::ostream& os) const
{
   if (debug_level >= DEBUG_LEVEL_VERBOSE)
   {
      os << "Connection binding:\n";
      Rconn->print(os);
   }
   if (output_level >= OUTPUT_LEVEL_MINIMUM)
   {
      os << "Total number of multiplexers: " << Rconn->get_component_num(MUX_GATE_STD) << std::endl;
      os << "Total number of bit-level multiplexers: " << Rconn->determine_bit_level_mux() << std::endl;
   }
}

/// Prints register binding
void hls::print_register_binding(std::ostream& os) const
{
   if (debug_level >= DEBUG_LEVEL_VERBOSE)
   {
      os << "Register binding:\n";
      reg_binding::const_iterator it;
      const OpGraphConstRef g = this->CGetOpGraph(FunctionBehavior::FLSAODG);
      for (it = Rreg->begin(); it != Rreg->end(); it++)
      {
         Rreg->print_rowHead(os, g, it);
         os.width(VARIABLE_COLUMN_SIZE);
         Rreg->print_el(os, g, it);
         os << std::endl;
      }
   }
   if (output_level >= OUTPUT_LEVEL_MINIMUM)
   {
      os << "Total number of registers: " << Rreg->get_used_regs() << std::endl;
      unsigned int number_ff = 0;
      for (unsigned int r = 0; r < Rreg->get_used_regs(); r++)
      {
         number_ff += Rreg->determine_bitsize(r);
      }
      os << "Total number of flip-flop registers: " << number_ff << std::endl;

   }
}

void hls::print_scheduling(std::ostream& os) const
{
   if (debug_level >= DEBUG_LEVEL_VERBOSE)
   {
      const OpGraphConstRef g = this->CGetOpGraph(FunctionBehavior::FLSAODG);
      std::map<unsigned int, std::set<vertex> > csteps_partitions;
      VertexIterator sch_i, sch_end;
      for (boost::tie(sch_i, sch_end) = boost::vertices(*g); sch_i != sch_end; sch_i++)
      {
         csteps_partitions[Rsch->get_cstep(*sch_i)].insert(*sch_i);
      }
      for (unsigned int cstep = 0; cstep < Rsch->get_csteps(); cstep++)
      {
         if(csteps_partitions.find(cstep) == csteps_partitions.end()) continue;

         std::set<vertex> & cstep_vertices = csteps_partitions.find(cstep)->second;
         const std::set<vertex>::const_iterator cv_it_end = cstep_vertices.end();
         for (std::set<vertex>::const_iterator cv_it = cstep_vertices.begin(); cv_it != cv_it_end; ++cv_it)
         {

            os << "Operation: ";
            os.width(COLUMN_SIZE);
            os.setf(std::ios_base::left, std::ios_base::adjustfield);
            os << GET_NAME(g, *cv_it) + "(" + GET_OP(g, *cv_it) + ")";
            os.width(0);

            os << " scheduled at control step (";
            os.width(0);
            os.setf(std::ios_base::left, std::ios_base::adjustfield);
            os << Rsch->get_cstep(*cv_it);
            os << "-";
            os << Rsch->get_cstep_end(*cv_it);
            os << ")";
            os.width(3);

            os << " ";

            os.width(COLUMN_SIZE);
            os.setf(std::ios_base::left, std::ios_base::adjustfield);
            os.width(0);
            os << " on functional unit " << Rfu->get_fu_name(*cv_it) << "(";
            if (Rfu->get_index(*cv_it) != INFINITE_UINT)
               os << Rfu->get_index(*cv_it);
            else
               os << "n.a.";
            os << ")";

            os << std::endl;

         }
      }
      os << "Total number of control steps: " << Rsch->get_csteps() << std::endl;
   }
}

const OpGraphConstRef hls::CGetOpGraph(FunctionBehavior::graph_type type) const
{
   return this->FB->CGetOpGraph(type, operations);
}

void hls::xload(const xml_element* node)
{

   schedule& sch = *(this->Rsch);
   fu_binding& fu = *(this->Rfu);
   unsigned int tot_cstep = 0;

   std::map<std::string, vertex> String2Vertex;
   std::map<std::pair<std::string, std::string>, std::list<unsigned int> > String2Id;
   const OpGraphConstRef data = this->CGetOpGraph(FunctionBehavior::FDFG);

   for(OpVertexSet::const_iterator op = operations.begin(); op != operations.end(); op++)
   {
      String2Vertex[GET_NAME(data, *op)] = *op;
   }

   for(unsigned int id = 0; id < ALL->get_number_fu_types(); id++)
   {
      String2Id[ALL->get_fu_name(id)].push_back(id);
   }
   //Recurse through child nodes:
   const xml_node::node_list list = node->get_children();
   for (xml_node::node_list::const_iterator iter = list.begin(); iter != list.end(); ++iter)
   {
      const xml_element* Enode = GetPointer<const xml_element>(*iter);
      if(!Enode || Enode->get_name() != "scheduling") continue;
      const xml_node::node_list list1 = Enode->get_children();
      for (xml_node::node_list::const_iterator iter1 = list1.begin(); iter1 != list1.end(); ++iter1)
      {
         const xml_element* EnodeC = GetPointer<const xml_element>(*iter1);
         if(!EnodeC) continue;
         if(EnodeC->get_name() == "scheduling_constraints")
         {
            std::string vertex_name;
            unsigned int cstep=0;
            LOAD_XVM(vertex_name, EnodeC);
            THROW_ASSERT(vertex_name != "", "bad formed xml file: vertex_name expected in a hls specification");
            if(CE_XVM(cstep, EnodeC)) 
               LOAD_XVM(cstep, EnodeC);
            else
               THROW_ERROR("bad formed xml file: cstep expected in a hls specification for operation " + vertex_name);
            if (cstep > tot_cstep) tot_cstep = cstep;

            unsigned int fu_index;
            LOAD_XVM(fu_index, EnodeC);

            std::string fu_name;
            std::string library = LIBRARY_STD;
            LOAD_XVM(fu_name, EnodeC);
            if(CE_XVM(library, EnodeC)) LOAD_XVM(library, EnodeC);
            unsigned int fu_type;
            if (ALL->is_artificial_fu(String2Id[std::make_pair(fu_name, library)].front()) || 
                ALL->is_assign(String2Id[std::make_pair(fu_name, library)].front()))
            {
               fu_type = String2Id[std::make_pair(fu_name, library)].front();
               String2Id[std::make_pair(fu_name, library)].pop_front();
            }
            else
            {
               fu_type = String2Id[std::make_pair(fu_name, library)].front();
            }

            sch.set_execution(String2Vertex[vertex_name], cstep);
            fu.bind(String2Vertex[vertex_name], fu_type, fu_index);
         }
      }
   }
   sch.set_csteps(tot_cstep + 1);
}

void hls::xwrite(xml_element* rootnode)
{
   schedule& sch = *(this->Rsch);
   fu_binding& fu = *(this->Rfu);

   const OpGraphConstRef data = CGetOpGraph(FunctionBehavior::FDFG);

   xml_element* Enode = rootnode->add_child_element("scheduling");

   for(OpVertexSet::const_iterator op = operations.begin(); op != operations.end(); op++)
   {
      xml_element* EnodeC = Enode->add_child_element("scheduling_constraints");
      std::string vertex_name = GET_NAME(data, *op);
      unsigned int cstep = sch.get_cstep(*op);
      WRITE_XVM(vertex_name, EnodeC);
      WRITE_XVM(cstep, EnodeC);

      unsigned int fu_type = fu.get_assign(*op);
      unsigned int fu_index = fu.get_index(*op);
      std::string fu_name, library;
      boost::tie(fu_name, library) = this->ALL->get_fu_name(fu_type);

      WRITE_XVM(fu_name, EnodeC);
      WRITE_XVM(fu_index, EnodeC);
      if (library != LIBRARY_STD) WRITE_XVM(library, EnodeC);
   }

   if(datapath)
   {
      Enode = rootnode->add_child_element("resource_allocation");
      std::map<std::string, unsigned int> resources;
      const technology_managerRef TM = HLS_T->get_technology_manager();
      computeResources(datapath->get_circ(), TM, resources);
      for (std::map<std::string, unsigned int>::iterator r = resources.begin(); r != resources.end(); r++)
      {
         xml_element* EnodeC = Enode->add_child_element("resource");
         std::string name = r->first;
         unsigned int number = r->second;
         WRITE_XVM(name, EnodeC);
         WRITE_XVM(number, EnodeC);
      }
   }
}

structural_type_descriptorRef hls::get_data_type(unsigned int variable) const
{
   return structural_type_descriptorRef(new structural_type_descriptor(variable, FB->CGetBehavioralHelper()));
}


static
void computeResources(const structural_objectRef circ, const technology_managerRef TM, std::map<std::string, unsigned int>& resources)
{
   const module* mod = GetPointer<module>(circ);
   for (unsigned int l = 0; l < mod->get_internal_objects_size(); l++)
   {
      const structural_objectRef obj = mod->get_internal_object(l);
      const structural_type_descriptorRef id_type = obj->get_typeRef();
      if (obj->get_kind() != component_o_K) continue;
      computeResources(obj, TM, resources);
      if (obj->get_id() == "Controller_i" || obj->get_id() == "Datapath_i") continue;
      std::string library = TM->get_library(id_type->id_type);
      if (library == WORK_LIBRARY) continue;
      resources[id_type->id_type]++;
   }
}

void hls::print_resources(std::ostream& os) const
{
   THROW_ASSERT(datapath, "datapath not yet created!");
   std::map<std::string, unsigned int> resources;
   const technology_managerRef TM = HLS_T->get_technology_manager();
   computeResources(datapath->get_circ(), TM, resources);
   os << "Summary of resources:" << std::endl;
   for (std::map<std::string, unsigned int>::iterator r = resources.begin(); r != resources.end(); r++)
   {
      os << "- " + r->first + ": " + STR(r->second) << std::endl;
   }
}
