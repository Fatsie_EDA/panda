/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file liveness.cpp
 * @brief Class implementation of the dataflow analysis
 *
 * This class performs the dataflow analysis
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Francesca Malcotti <francy_malco@virgilio.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "liveness.hpp"

#include "Parameter.hpp"
#include "constant_strings.hpp"

liveness::liveness(const ParameterConstRef _Param) :
   Param(_Param),
   null_vertex_string("NULL_VERTEX")
{
   conflicts_already_computed = false;
}

liveness::~liveness()
{

}

bool liveness::is_defined(unsigned int var) const
{
   if(var_op_definition.find(var)!=var_op_definition.end())
      return true;

   return false;
}

void liveness::set_live_in(const vertex& v, unsigned int var)
{
   live_in[v].insert(var);
}

void liveness::set_live_in(const vertex& v, std::set<unsigned int>::const_iterator first, const std::set<unsigned int>::const_iterator last)
{
   live_in[v].insert(first, last);
}

void liveness::erase_el_live_in(const vertex& v, unsigned int var)
{
   live_in[v].erase(var);
}

const std::set<unsigned int>& liveness::get_live_in(const vertex& v) const
{
   if(live_in.find(v) != live_in.end())
      return live_in.find(v)->second;
   else
      return empty_set;
}

void liveness::set_live_out(const vertex& v, unsigned int var)
{
   live_out[v].insert(var);
}

void liveness::set_live_out(const vertex& v, const std::set<unsigned int>& vars)
{
   live_out[v] = vars;
}

void liveness::set_live_out(const vertex& v, std::set<unsigned int>::const_iterator first, const std::set<unsigned int>::const_iterator last)
{
   live_out[v].insert(first, last);
}

void liveness::erase_el_live_out(const vertex& v, unsigned int var)
{
   live_out[v].erase(var);
}

const std::set<unsigned int>& liveness::get_live_out(const vertex& v) const
{
   if(live_out.find(v) != live_out.end())
      return live_out.find(v)->second;
   else
      return empty_set;
}

#if HAVE_EXPERIMENTAL

void liveness::set_live_out_p(const vertex& state, const std::set<unsigned int>& vars)
{
   live_out_p[state] = vars;
}

void liveness::set_live_in_p(const vertex& state, const std::set<unsigned int>& vars)
{
   live_in_p[state] = vars;
}

const std::set<unsigned int>& liveness::get_live_out_p(const vertex& state) const
{
   THROW_ASSERT(live_out_p.find(state) != live_out_p.end(), "Live-out_p not computed");
   return live_out_p.find(state)->second;
}

const std::set<unsigned int>& liveness::get_live_in_p(const vertex& state) const
{
   THROW_ASSERT(live_in_p.find(state) != live_in_p.end(), "Live-in_p not computed");
   return live_in_p.find(state)->second;
}



void liveness::add_conflict (vertex op1, vertex op2)
{
   if(!are_in_conflict(op1, op2))
      is_in_conflict_with[op1].insert(op2);
   if(!are_in_conflict(op2, op1))
      is_in_conflict_with[op2].insert(op1);
}
#endif

vertex liveness::get_op_where_defined(unsigned int var) const
{
   THROW_ASSERT(var_op_definition.find(var) != var_op_definition.end(), "var never defined " + boost::lexical_cast<std::string>(var));
   return var_op_definition.find(var)->second;

}

bool liveness::has_op_where_defined(unsigned int var) const
{
   return(var_op_definition.find(var) != var_op_definition.end());
}

const std::set<vertex>& liveness::get_state_in(vertex state, vertex op, unsigned int var) const
{
   THROW_ASSERT(state_in_definitions.find(state) != state_in_definitions.end(), "state never used " + get_name(state));
   THROW_ASSERT(state_in_definitions.find(state)->second.find(op) != state_in_definitions.find(state)->second.end(), "op never used in state " + get_name(state));
   THROW_ASSERT(state_in_definitions.find(state)->second.find(op)->second.find(var) != state_in_definitions.find(state)->second.find(op)->second.end(), "var never used in the given state. Var: " + boost::lexical_cast<std::string>(var));
   return state_in_definitions.find(state)->second.find(op)->second.find(var)->second;
}

bool liveness::has_state_in(vertex state, vertex op, unsigned int var) const
{
   if(state_in_definitions.find(state) == state_in_definitions.end()) return false;
   if(state_in_definitions.find(state)->second.find(op) == state_in_definitions.find(state)->second.end()) return false;
   if(state_in_definitions.find(state)->second.find(op)->second.find(var) == state_in_definitions.find(state)->second.find(op)->second.end()) return false;
   return true;
}

void liveness::add_state_in_for_var(unsigned int var, vertex op, vertex state, vertex state_in)
{
   state_in_definitions[state][op][var].insert(state_in);
}

const std::set<vertex>& liveness::get_state_out(vertex state, vertex op, unsigned int var) const
{
   THROW_ASSERT(state_out_definitions.find(state) != state_out_definitions.end(), "state never used " + get_name(state));
   THROW_ASSERT(state_out_definitions.find(state)->second.find(op) != state_out_definitions.find(state)->second.end(), "op never used in state " + get_name(state));
   THROW_ASSERT(state_out_definitions.find(state)->second.find(op)->second.find(var) != state_out_definitions.find(state)->second.find(op)->second.end(), "var never used in the given state. Var: " + boost::lexical_cast<std::string>(var));
   return state_out_definitions.find(state)->second.find(op)->second.find(var)->second;
}

bool liveness::has_state_out(vertex state, vertex op, unsigned int var) const
{
   if(state_out_definitions.find(state) == state_out_definitions.end()) return false;
   if(state_out_definitions.find(state)->second.find(op) == state_out_definitions.find(state)->second.end()) return false;
   if(state_out_definitions.find(state)->second.find(op)->second.find(var) == state_out_definitions.find(state)->second.find(op)->second.end()) return false;
   return true;
}

void liveness::add_state_out_for_var(unsigned int var, vertex op, vertex state, vertex state_in)
{
   state_out_definitions[state][op][var].insert(state_in);
}

const std::set<vertex>& liveness::get_state_where_end(vertex op) const
{
   THROW_ASSERT(ending_operations.find(op) != ending_operations.end(), "op never ending in a state ");
   return ending_operations.find(op)->second;
}

const std::set<vertex>& liveness::get_state_where_run(vertex op) const
{
   THROW_ASSERT(running_operations.find(op) != running_operations.end(), "op never running in a state ");
   return running_operations.find(op)->second;
}

const std::string & liveness::get_name(vertex v) const
{
   if(v == NULL_VERTEX) return null_vertex_string;
   THROW_ASSERT(names.find(v) != names.end(), "state without a name");
   return names.find(v)->second;
}

bool liveness::are_in_conflict (vertex op1, vertex op2)
{
   if(!conflicts_already_computed)
   {
      /*const std::set<vertex>& op1_end = get_state_where_end(op1);
      const std::set<vertex>& op2_end = get_state_where_end(op2);
      const std::set<vertex>::const_iterator op1_end_it_end = op1_end.end();
      for(std::set<vertex>::const_iterator op1_end_it = op1_end.begin(); op1_end_it != op1_end_it_end; ++op1_end_it)
         if(op2_end.find(*op1_end_it) != op2_end.end())
            return true;
            */

      const std::set<vertex>& op1_run = get_state_where_run(op1);
      const std::set<vertex>& op2_run = get_state_where_run(op2);
      const std::set<vertex>::const_iterator op1_run_it_end = op1_run.end();
      for(std::set<vertex>::const_iterator op1_run_it = op1_run.begin(); op1_run_it != op1_run_it_end; ++op1_run_it)
         if(op2_run.find(*op1_run_it) != op2_run.end())
            return true;
      return false;
   }
#if HAVE_EXPERIMENTAL
   else
   {
      return (is_in_conflict_with[op1].find(op2)!=is_in_conflict_with[op1].end());
   }
#else
   return false;

#endif
}

vertex liveness::get_start_op(vertex state) const
{
   THROW_ASSERT(start_op.find(state) != start_op.end(), "start_op map does not have this chained vertex " + get_name(state));
   return start_op.find(state)->second;
}

void liveness::set_start_op(vertex state, vertex op)
{
   start_op[state] = op;
}

