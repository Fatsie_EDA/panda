/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file liveness_computer.hpp
 * @brief Class specification for algorithms that compute the liveness of variables
 *
 * This class is abstract and it has to be specialized in order to implement the desider algorithm
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef LIVENESS_COMPUTER_HPP
#define LIVENESS_COMPUTER_HPP

#include "config_HAVE_EXPERIMENTAL.hpp"

#include "hls_step.hpp"
REF_FORWARD_DECL(liveness_computer);

class liveness_computer : public HLS_step
{
   protected:

      /**
       * is the ssa_name allocable on a register
       */
      bool is_register_compatible(unsigned int ssa) const;

   public:

      typedef enum
      {
         FSM_NI_SSA_LIVENESS = 0,
#if HAVE_EXPERIMENTAL
         CHAINING_BASED_LIVENESS
#endif
      } liveness_algorithm;

      /**
       * Constructor
       */
      liveness_computer(const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Destructor
       */
      virtual ~liveness_computer();
    
      /**
       * Factory method.
       */
      static
      liveness_computerRef factory(liveness_algorithm algorithm, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

      /**
       * Factory method from XML node
       */
      static
      liveness_computerRef xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId);

};
//refcount definition for class
typedef refcount<liveness_computer> liveness_computerRef;

#endif
