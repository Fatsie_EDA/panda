/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file FSM_NI_SSA_liveness.cpp
 * @brief liveness analysis exploiting the SSA form of the IR
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "FSM_NI_SSA_liveness.hpp"
#include "liveness.hpp"
#include "memory.hpp"

#include "hls.hpp"
#include "hls_constraints.hpp"

#include "state_transition_graph.hpp"
#include "state_transition_graph_manager.hpp"
#include "basic_block.hpp"
#include "op_graph.hpp"

///Behavior include
#include "hls_manager.hpp"
#include "behavioral_helper.hpp"
#include "function_behavior.hpp"

#include "tree_helper.hpp"

#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"

FSM_NI_SSA_liveness::FSM_NI_SSA_liveness(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   liveness_computer(_Param, _HLSMgr, _funId)
{

}

FSM_NI_SSA_liveness::~FSM_NI_SSA_liveness()
{
}

void FSM_NI_SSA_liveness::exec()
{
   HLS->Rliv = livenessRef(new liveness(Param));
   const StateTransitionGraphConstRef astg = HLS->STG->CGetAstg();
   const StateTransitionGraphConstRef stg = HLS->STG->CGetStg();
   const BBGraphConstRef fbb = HLS->FB->CGetBBGraph(FunctionBehavior::FBB);
   const OpGraphConstRef data = HLS->CGetOpGraph(FunctionBehavior::DFG);

   /// Map between basic block node index and vertices
   std::unordered_map<unsigned int, vertex> bb_index_map;
   VertexIterator vit, vend;
   for(boost::tie(vit, vend) = boost::vertices(*fbb); vit != vend; ++vit)
   {
      bb_index_map[fbb->CGetBBNodeInfo(*vit)->get_bb_index()] = *vit;
   }

   /// vertex of the STG analyzed in reverse order
   std::deque<vertex> reverse_order_state_list;
   astg->ReverseTopologicalSort(reverse_order_state_list);
   const vertex bb_exit = fbb->CGetBBGraphInfo()->exit_vertex;
   unsigned int prev_bb_index = fbb->CGetBBNodeInfo(bb_exit)->get_bb_index();
   vertex exit_state = HLS->STG->get_exit_state();
   vertex entry_state = HLS->STG->get_entry_state();
   bool second_state_of_BB = false;
   std::deque<vertex> dummy_states;

   /// compute which operation defines a variable (add_op_definition)
   /// compute in which states an operation complete the computation (add_state_for_ending_op)
   /// compute the set of supporting states
   /// compute the set of dummy states
   std::deque<vertex>::const_iterator rosl_it_end = reverse_order_state_list.end();
   for(std::deque<vertex>::const_iterator rosl_it = reverse_order_state_list.begin(); rosl_it != rosl_it_end; ++rosl_it)
   {
      HLS->Rliv->add_name(*rosl_it, HLS->STG->get_state_name(*rosl_it));
      HLS->Rliv->add_support_state(*rosl_it);
      if(*rosl_it == exit_state)
         continue;
      const StateInfoConstRef state_info = astg->CGetStateInfo(*rosl_it);
      const std::list<vertex>& ending_operations_cur = state_info->ending_operations;
      std::list<vertex>::const_reverse_iterator eoc_it_end = ending_operations_cur.rend();
      for(std::list<vertex>::const_reverse_iterator eoc_it = ending_operations_cur.rbegin(); eoc_it != eoc_it_end; ++eoc_it)
      {
         const CustomSet<unsigned int> & scalar_defs = data->CGetOpNodeInfo(*eoc_it)->GetScalarVariables(AT_DEF);
         if(not scalar_defs.empty())
         {
            CustomSet<unsigned int>::const_iterator it_end = scalar_defs.end();
            for(CustomSet<unsigned int>::const_iterator it = scalar_defs.begin(); it != it_end; ++it)
               if(is_register_compatible(*it))
               {
                  HLS->Rliv->add_op_definition(*it, *eoc_it);
               }
         }
         HLS->Rliv->add_state_for_ending_op(*eoc_it, *rosl_it);
      }

      if(state_info->is_dummy)
      {
         dummy_states.push_back(*rosl_it);
         continue;
      }
   }

   /// compute live_out
   vertex prev_state = exit_state;
   for(std::deque<vertex>::const_iterator rosl_it = reverse_order_state_list.begin(); rosl_it != rosl_it_end; ++rosl_it)
   {
      if(*rosl_it == exit_state) continue;
      const StateInfoConstRef state_info = astg->CGetStateInfo(*rosl_it);
      if(state_info->is_dummy)
      {
         continue;
      }
      vertex last_operation;
      unsigned int bb_index;
      if(state_info->executing_operations.empty())
      {
         if(state_info->ending_operations.empty())
            bb_index = prev_bb_index; // it may happen with pipelined operations
         else
         {
            last_operation = *(state_info->ending_operations).rbegin();
            bb_index = GET_BB_INDEX(data, last_operation);
         }
      }
      else
      {
         last_operation = *(state_info->executing_operations.rbegin());
         bb_index = GET_BB_INDEX(data, last_operation);
      }

      if(prev_bb_index != bb_index)
      {
         const std::set<unsigned int> & live_out = fbb->CGetBBNodeInfo(bb_index_map[bb_index])->get_live_out();
         std::set<unsigned int>::const_iterator lo_it_end = live_out.end();
         for(std::set<unsigned int>::const_iterator lo_it = live_out.begin(); lo_it != lo_it_end; ++lo_it)
            if(is_register_compatible(*lo_it))
               HLS->Rliv->set_live_out(*rosl_it, *lo_it);
         second_state_of_BB = true;
      }
      else
      {
         if(second_state_of_BB)
         {
            second_state_of_BB = false;
            std::set<unsigned int>::const_iterator lo_it_end = HLS->Rliv->get_live_out(prev_state).end();
            for(std::set<unsigned int>::const_iterator lo_it = HLS->Rliv->get_live_out(prev_state).begin(); lo_it != lo_it_end; ++lo_it)
            {
               HLS->Rliv->set_live_out(*rosl_it, *lo_it);
            }
         }
         else
            HLS->Rliv->set_live_out(*rosl_it, HLS->Rliv->get_live_out(prev_state).begin(), HLS->Rliv->get_live_out(prev_state).end());
         const std::list<vertex>& executing_operations = stg->CGetStateInfo(prev_state)->executing_operations;
         std::list<vertex>::const_reverse_iterator exo_it_end = executing_operations.rend();
         for(std::list<vertex>::const_reverse_iterator exo_it = executing_operations.rbegin(); exo_it != exo_it_end; ++exo_it)
         {
            const CustomSet<unsigned int> & scalar_uses = data->CGetOpNodeInfo(*exo_it)->GetScalarVariables(AT_USE);
            CustomSet<unsigned int>::const_iterator scalar_use, scalar_use_end = scalar_uses.end();
            for(scalar_use = scalar_uses.begin(); scalar_use != scalar_use_end; scalar_use++)
            {
               if(is_register_compatible(*scalar_use))
                  HLS->Rliv->set_live_out(*rosl_it, *scalar_use);
            }
         }
         const std::list<vertex>& ending_operations = astg->CGetStateInfo(prev_state)->ending_operations;
         std::list<vertex>::const_reverse_iterator eo_it_end = ending_operations.rend();
         for(std::list<vertex>::const_reverse_iterator eo_it = ending_operations.rbegin(); eo_it != eo_it_end; ++eo_it)
         {
            const CustomSet<unsigned int> & scalar_defs = data->CGetOpNodeInfo(*eo_it)->GetScalarVariables(AT_DEF);
            if(not scalar_defs.empty())
            {
               CustomSet<unsigned int>::const_iterator it_end = scalar_defs.end();
               for(CustomSet<unsigned int>::const_iterator it = scalar_defs.begin(); it != it_end; ++it)
                  if(is_register_compatible(*it))
                     HLS->Rliv->erase_el_live_out(*rosl_it, *it);
            }
         }
      }
      prev_state = *rosl_it;
      prev_bb_index = bb_index;
   }

   /// compute the live in of a state by traversing the state list in topological order
   prev_state = entry_state;
   prev_bb_index = fbb->CGetBBNodeInfo(bb_exit)->get_bb_index();
   std::deque<vertex>::const_reverse_iterator osl_it_end = reverse_order_state_list.rend();
   for(std::deque<vertex>::const_reverse_iterator osl_it = reverse_order_state_list.rbegin(); osl_it != osl_it_end; ++osl_it)
   {
      const StateInfoConstRef state_info = astg->CGetStateInfo(*osl_it);
      if(state_info->is_dummy)
         continue;
      vertex last_operation;
      unsigned int bb_index;
      if(state_info->executing_operations.empty())
      {
         if(state_info->ending_operations.empty())
            bb_index = prev_bb_index; // it may happen with pipelined operations
         else
         {
            last_operation = *(state_info->ending_operations.rbegin());
            bb_index = GET_BB_INDEX(data, last_operation);
         }
      }
      else
      {
         last_operation = *(state_info->executing_operations.rbegin());
         bb_index = GET_BB_INDEX(data, last_operation);
      }

      if(prev_bb_index != bb_index)
      {
         const std::set<unsigned int> & live_in = fbb->CGetBBNodeInfo(bb_index_map[bb_index])->get_live_in();
         std::set<unsigned int>::const_iterator lo_it_end = live_in.end();
         for(std::set<unsigned int>::const_iterator lo_it = live_in.begin(); lo_it != lo_it_end; ++lo_it)
            if(is_register_compatible(*lo_it))
               HLS->Rliv->set_live_in(*osl_it, *lo_it);
      }
      else
         HLS->Rliv->set_live_in(*osl_it, HLS->Rliv->get_live_out(prev_state).begin(), HLS->Rliv->get_live_out(prev_state).end());
      prev_state = *osl_it;
      prev_bb_index = bb_index;

   }

   /// fix the live in/out of dummy states
   std::deque<vertex>::const_iterator ds_it_end = dummy_states.end();
   for(std::deque<vertex>::const_iterator ds_it = dummy_states.begin(); ds_it != ds_it_end; ++ds_it)
   {
      const StateInfoConstRef state_info = astg->CGetStateInfo(*ds_it);
      InEdgeIterator e_it, e_it_end;
      for(boost::tie(e_it, e_it_end) = boost::in_edges(*ds_it, *astg); e_it != e_it_end; ++e_it)
      {
         vertex src_state = boost::source(*e_it, *astg);
         HLS->Rliv->set_live_out(*ds_it, HLS->Rliv->get_live_out(src_state).begin(), HLS->Rliv->get_live_out(src_state).end());
         HLS->Rliv->set_live_in(*ds_it, HLS->Rliv->get_live_out(src_state).begin(), HLS->Rliv->get_live_out(src_state).end());
         /// add all the uses of *ds_it to src_state
         const std::list<vertex>& executing_operations = state_info->executing_operations;
         std::list<vertex>::const_iterator eo_it_end = executing_operations.end();
         for(std::list<vertex>::const_iterator eo_it = executing_operations.begin(); eo_it != eo_it_end; ++eo_it)
         {
            const CustomSet<unsigned int> & scalar_uses = data->CGetOpNodeInfo(*eo_it)->GetScalarVariables(AT_USE);
            CustomSet<unsigned int>::const_iterator scalar_use, scalar_use_end = scalar_uses.end();
            for(scalar_use = scalar_uses.begin(); scalar_use != scalar_use_end; scalar_use++)
            {
               if(is_register_compatible(*scalar_use))
               {
                  HLS->Rliv->set_live_out(src_state, *scalar_use);
                  HLS->Rliv->set_live_in(*ds_it, *scalar_use);
                  /// extend the lifetime of used variable to reduce the critical path
                  OutEdgeIterator oe_it, oe_it_end;
                  for(boost::tie(oe_it, oe_it_end) = boost::out_edges(*ds_it, *astg); oe_it != oe_it_end; ++oe_it)
                  {
                     vertex target_state = boost::target(*oe_it, *astg);
                     HLS->Rliv->set_live_in(target_state, *scalar_use);
                  }
               }
            }
         }
      }
   }
   /// fix live out of the states adjacent with the exit state
   InEdgeIterator es_it, es_it_end;
   for(boost::tie(es_it, es_it_end) = boost::in_edges(exit_state, *astg); es_it != es_it_end; ++es_it)
   {
      vertex src_state = boost::source(*es_it, *astg);
      HLS->Rliv->set_live_out(src_state, HLS->Rliv->get_live_in(src_state).begin(), HLS->Rliv->get_live_in(src_state).end());
   }



   /// compute in which state an operation is in execution
   /// compute state in relation: on which transition a variable is live in
   /// compute state out relation: on which transition a variable is live out
   for(std::deque<vertex>::const_iterator rosl_it = reverse_order_state_list.begin(); rosl_it != rosl_it_end; ++rosl_it)
   {
      const StateInfoConstRef state_info = astg->CGetStateInfo(*rosl_it);
      INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "-->Analyzing state " + state_info->name);
      const std::list<vertex>& ending_operations_cur = state_info->ending_operations;
      const std::list<vertex>& running_operations_cur = state_info->executing_operations;
      std::list<vertex>::const_iterator roc_it_end = running_operations_cur.end();
      for(std::list<vertex>::const_iterator roc_it = running_operations_cur.begin(); roc_it != roc_it_end; ++roc_it)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "-->Analyzing running operation " + GET_NAME(data, *roc_it));
         HLS->Rliv->add_state_for_running_op(*roc_it, *rosl_it);
         if((GET_TYPE(data, *roc_it) & TYPE_PHI) != 0)
         {
            std::vector<HLS_manager::io_binding_type> var_read = HLSMgr->get_required_values(HLS->functionId, *roc_it);
            unsigned int in_index = 0;
            const std::vector<HLS_manager::io_binding_type>::const_iterator sr_it_end = var_read.end();
            for(std::vector<HLS_manager::io_binding_type>::const_iterator sr_it = var_read.begin(); sr_it != sr_it_end; ++sr_it,++in_index)
            {
               unsigned int tree_var = std::get<0>(*sr_it);
               if(tree_var == 0)
                  continue;
               const unsigned int tree_node_operation = data->CGetOpNodeInfo(*roc_it)->node_id;
               unsigned int bb_index = tree_helper::get_bb_edge(HLSMgr->get_tree_manager(), tree_node_operation, in_index);
               /// now we look for the last state with operations belonging to basic block bb_index
#ifndef NDEBUG
               bool found_state = false;
#endif
               InEdgeIterator ie, iend;
               for(boost::tie(ie, iend) = boost::in_edges(*rosl_it, *stg); ie != iend; ie++)
               {
                  vertex src_state = boost::source(*ie, *stg);
                  const StateInfoConstRef source_state_info = stg->CGetStateInfo(src_state);
                  const std::set<unsigned int> & BB_ids = source_state_info->BB_ids;
                  if(BB_ids.find(bb_index) != BB_ids.end())
                  {
                     //THROW_ASSERT((HLS->Rliv->get_live_out(src_state).find(tree_var) != HLS->Rliv->get_live_out(src_state).end()), "unexpected live out condition");
                     THROW_ASSERT(src_state != entry_state, "a phi has to have always source state");
                     HLS->Rliv->add_state_in_for_var(tree_var, *roc_it, *rosl_it, src_state);
#ifndef NDEBUG
                     found_state = true;
#endif
                  }

               }
               THROW_ASSERT(found_state, "unexpected condition");
            }
         }
         else
         {
            OutEdgeIterator oe, oend;
            for(boost::tie(oe, oend) = boost::out_edges(*rosl_it, *stg); oe != oend; oe++)
            {
               vertex tgt_state = boost::target(*oe, *stg);
               const CustomSet<unsigned int> & scalar_defs = data->CGetOpNodeInfo(*roc_it)->GetScalarVariables(AT_DEF);
               const CustomSet<unsigned int> & cited_variables = data->CGetOpNodeInfo(*roc_it)->cited_variables;
               const CustomSet<unsigned int> all_but_scalar_defs = cited_variables - scalar_defs;
               CustomSet<unsigned int>::const_iterator all_but_scalar_def, all_but_scalar_def_end = all_but_scalar_defs.end();
               for(all_but_scalar_def = all_but_scalar_defs.begin(); all_but_scalar_def != all_but_scalar_def_end; all_but_scalar_def++)
               {
                  if(is_register_compatible(*all_but_scalar_def))
                     HLS->Rliv->add_state_in_for_var(*all_but_scalar_def, *roc_it, *rosl_it, tgt_state);
                  else if(HLSMgr->Rmem->has_base_address(*all_but_scalar_def) ||
                       (/*tree_helper::is_ssa_name(HLS->AppM->get_tree_manager(), *all_but_scalar_def) && */tree_helper::is_parameter(HLSMgr->get_tree_manager(), *all_but_scalar_def)))
                  {
                     HLS->Rliv->add_state_in_for_var(*all_but_scalar_def, *roc_it, *rosl_it, tgt_state);
                  }
                  else if(tree_helper::is_ssa_name(HLSMgr->get_tree_manager(), *all_but_scalar_def) && tree_helper::is_virtual(HLSMgr->get_tree_manager(), *all_but_scalar_def))
                  {
                     ;
                  }
                  else
                  {
                     THROW_ERROR("unexpected condition " +STR(*all_but_scalar_def));
                  }
               }
            }
         }
         INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "<--Analyzed running operation " + GET_NAME(data, *roc_it));
      }

      std::list<vertex>::const_iterator eoc_it_end = ending_operations_cur.end();
      for(std::list<vertex>::const_iterator eoc_it = ending_operations_cur.begin(); eoc_it != eoc_it_end; ++eoc_it)
      {
         if((GET_TYPE(data, *eoc_it) & TYPE_PHI) != 0) continue;
         const CustomSet<unsigned int> & scalar_defs = data->CGetOpNodeInfo(*eoc_it)->GetScalarVariables(AT_DEF);
         if(not scalar_defs.empty())
         {
            CustomSet<unsigned int>::const_iterator it_end = scalar_defs.end();
            for(CustomSet<unsigned int>::const_iterator it = scalar_defs.begin(); it != it_end; ++it)
               if(is_register_compatible(*it))
               {
                  OutEdgeIterator e_it, e_it_end;
                  for(boost::tie(e_it, e_it_end) = boost::out_edges(*rosl_it, *stg); e_it != e_it_end; ++e_it)
                  {
                     vertex tgt_state = boost::target(*e_it, *stg);
                     if(HLS->Rliv->get_live_in(tgt_state).find(*it) != HLS->Rliv->get_live_in(tgt_state).end())
                        HLS->Rliv->add_state_out_for_var(*it, *eoc_it, *rosl_it, tgt_state);
                  }
               }
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "<--Analyzed state " + state_info->name);
   }


#ifndef NDEBUG
   if(debug_level>= DEBUG_LEVEL_PEDANTIC)
   {
      /// print the analysis result
      const BehavioralHelperConstRef  BH = HLS->FB->CGetBehavioralHelper();
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "STG Liveness for function " + BH->get_function_name());
      VertexIterator vIt, vEnd;
      for(boost::tie(vIt, vEnd) = boost::vertices(*astg); vIt != vEnd; vIt++)
      {
         PRINT_DBG_STRING(DEBUG_LEVEL_PEDANTIC, debug_level, "Live In for state " + HLS->STG->get_state_name(*vIt) + ": ");
         std::set<unsigned int>::const_iterator li_it_end = HLS->Rliv->get_live_in(*vIt).end();
         for(std::set<unsigned int>::const_iterator li_it = HLS->Rliv->get_live_in(*vIt).begin(); li_it != li_it_end; ++li_it)
         {
            PRINT_DBG_STRING(DEBUG_LEVEL_PEDANTIC, debug_level,  BH->PrintVariable(*li_it) + " ");
         }
         PRINT_DBG_STRING(DEBUG_LEVEL_PEDANTIC, debug_level, "\n");
         PRINT_DBG_STRING(DEBUG_LEVEL_PEDANTIC, debug_level, "Live Out for state " + HLS->STG->get_state_name(*vIt) + ": ");
         std::set<unsigned int>::const_iterator lo_it_end = HLS->Rliv->get_live_out(*vIt).end();
         for(std::set<unsigned int>::const_iterator lo_it = HLS->Rliv->get_live_out(*vIt).begin(); lo_it != lo_it_end; ++lo_it)
         {
            PRINT_DBG_STRING(DEBUG_LEVEL_PEDANTIC, debug_level,  BH->PrintVariable(*lo_it) + " ");
         }
         PRINT_DBG_STRING(DEBUG_LEVEL_PEDANTIC, debug_level, "\n");
      }
   }
#endif

}

std::string FSM_NI_SSA_liveness::get_kind_text() const
{
   return "FSM_NI_SSA_liveness";
}
