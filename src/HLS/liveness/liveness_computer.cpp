/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file liveness_computer.cpp
 * @brief Some method for the abstract class
 *
 * This class creates the specialization to perform the computation of the liveness
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "liveness_computer.hpp"
#include "FSM_NI_SSA_liveness.hpp"
#if HAVE_EXPERIMENTAL
#include "chaining_based_liveness.hpp"
#endif

#include "memory.hpp"

#include "polixml.hpp"
#include "xml_helper.hpp"
#include "tree_helper.hpp"
#include "hls.hpp"

///Behavior include
#include "hls_manager.hpp"

liveness_computer::liveness_computer(const ParameterConstRef _Param, const HLS_managerRef _HLSMgr, unsigned int _funId) :
   HLS_step(_Param, _HLSMgr, _funId)
{

}

liveness_computer::~liveness_computer()
{

}

liveness_computerRef liveness_computer::xload_factory(const xml_element* node, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   std::string algorithm;
   if (CE_XVM(algorithm, node)) LOAD_XVM(algorithm, node);
   else algorithm = "FSM_NI_SSA_LIVENESS";

   liveness_algorithm algorithm_t;
   if (algorithm == "FSM_NI_SSA_LIVENESS")
      algorithm_t = FSM_NI_SSA_LIVENESS;
#if HAVE_EXPERIMENTAL
   else if (algorithm == "CHAINING_BASED_LIVENESS")
      algorithm_t = CHAINING_BASED_LIVENESS;
#endif
   else
      THROW_ERROR("Liveness algorithm \"" + algorithm + "\" currently not supported");
   return factory(algorithm_t, Param, HLSMgr, funId);
}

liveness_computerRef liveness_computer::factory(liveness_algorithm algorithm, const ParameterConstRef Param, const HLS_managerRef HLSMgr, unsigned int funId)
{
   switch(algorithm)
   {
      case FSM_NI_SSA_LIVENESS:
         return liveness_computerRef(new FSM_NI_SSA_liveness(Param, HLSMgr, funId));
#if HAVE_EXPERIMENTAL
      case CHAINING_BASED_LIVENESS:
         return liveness_computerRef(new chaining_based_liveness(Param, HLSMgr, funId));
#endif
      default:
         THROW_UNREACHABLE("liveness method not yet supported " + boost::lexical_cast<std::string>(algorithm));
   }
   return liveness_computerRef();
}

bool liveness_computer::is_register_compatible(unsigned int ssa) const
{
   return tree_helper::is_ssa_name(HLSMgr->get_tree_manager(), ssa) &&
         !tree_helper::is_virtual(HLSMgr->get_tree_manager(), ssa) && /// virtual ssa_name is not considered
         !HLSMgr->Rmem->has_base_address(ssa) && /// ssa_name allocated in memory
         !tree_helper::is_parameter(HLSMgr->get_tree_manager(), ssa); /// parameters have been already stored in a register by the calling function
}
