/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file design_flow_manager.cpp
 * @brief Wrapper of design_flow
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "design_flow_manager.hpp"

///Design flow include
#include "design_flow_aux_step.hpp"
#include "design_flow_graph.hpp"
#include "design_flow_step.hpp"
#include "design_flow_step_factory.hpp"

///Parameter include
#include "Parameter.hpp"

DesignFlowStepNecessitySorter::DesignFlowStepNecessitySorter(const DesignFlowGraphConstRef _design_flow_graph) :
   design_flow_graph(_design_flow_graph)
{}

bool DesignFlowStepNecessitySorter::operator()(const vertex x, const vertex y) const
{
   const DesignFlowStepInfoConstRef x_info = design_flow_graph->CGetDesignFlowStepInfo(x);
   const DesignFlowStepInfoConstRef y_info = design_flow_graph->CGetDesignFlowStepInfo(y);
   const bool x_composed = x_info->design_flow_step->IsComposed();
   const bool y_composed = y_info->design_flow_step->IsComposed();
   if(x_composed and not y_composed)
   {
      return true;
   }
   else if(not x_composed and y_composed)
   {
      return false;
   }
   else if(x_info->unnecessary and not y_info->unnecessary)
   {
      return false;
   }
   else if(not x_info->unnecessary and y_info->unnecessary)
   {
      return true;
   }
   else
   {
#if HAVE_UNORDERED
      return x < y;
#else
      return x_info->design_flow_step->GetName() < y_info->design_flow_step->GetName();
#endif
   }
}

DesignFlowManager::DesignFlowManager(const ParameterConstRef _parameters) :
   design_flow_graphs_collection(new DesignFlowGraphsCollection(_parameters)),
   design_flow_graph(new DesignFlowGraph(design_flow_graphs_collection, DesignFlowGraph::DEPENDENCE_SELECTOR | DesignFlowGraph::PRECEDENCE_SELECTOR | DesignFlowGraph::AUX_SELECTOR)),
   feedback_design_flow_graph(new DesignFlowGraph(design_flow_graphs_collection, DesignFlowGraph::DEPENDENCE_SELECTOR | DesignFlowGraph::PRECEDENCE_SELECTOR | DesignFlowGraph::AUX_SELECTOR | DesignFlowGraph::DEPENDENCE_FEEDBACK_SELECTOR)),
   possibly_ready(std::set<vertex, DesignFlowStepNecessitySorter>(DesignFlowStepNecessitySorter(design_flow_graph))),
   parameters(_parameters),
   output_level(_parameters->getOption<int>(OPT_output_level))
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this));
   const DesignFlowGraphInfoRef design_flow_graph_info = design_flow_graph->GetDesignFlowGraphInfo();
   null_deleter nullDel;
   design_flow_graph_info->entry = design_flow_graphs_collection->AddDesignFlowStep(DesignFlowStepRef(new AuxDesignFlowStep("Entry", DESIGN_FLOW_ENTRY, DesignFlowManagerConstRef(this, nullDel), parameters)), false);
#ifndef NDEBUG
   if(parameters->getOption<bool>(OPT_print_dot) or debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
   {
      step_names[design_flow_graph_info->entry] = "Entry";
   }
#endif
   const DesignFlowStepInfoRef entry_info = design_flow_graph->GetDesignFlowStepInfo(design_flow_graph_info->entry);
   entry_info->executed = true;
   design_flow_graph_info->exit = design_flow_graphs_collection->AddDesignFlowStep(DesignFlowStepRef(new AuxDesignFlowStep("Exit", DESIGN_FLOW_EXIT, DesignFlowManagerConstRef(this, nullDel), parameters)), false);
#ifndef NDEBUG
   if(parameters->getOption<bool>(OPT_print_dot) or debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
   {
      step_names[design_flow_graph_info->exit] = "Exit";
   }
#endif
}

DesignFlowManager::~DesignFlowManager()
{}

size_t DesignFlowManager::step_counter = 0;

void DesignFlowManager::AddStep(const DesignFlowStepRef step)
{
   DesignFlowStepSet steps;
   steps.insert(step);
   RecursivelyAddSteps(steps, false);
   Consolidate();
}

void DesignFlowManager::AddSteps(const DesignFlowStepSet & steps)
{
   RecursivelyAddSteps(steps, false);
   Consolidate();
}

void DesignFlowManager::RecursivelyAddSteps(const DesignFlowStepSet & steps, const bool unnecessary)
{
   static size_t temp_counter = 0;
   const DesignFlowGraphInfoRef design_flow_graph_info = design_flow_graph->GetDesignFlowGraphInfo();
   DesignFlowStepSet steps_to_be_processed = steps;
   while(steps_to_be_processed.size())
   {
      const DesignFlowStepRef design_flow_step = *(steps_to_be_processed.begin());
      steps_to_be_processed.erase(design_flow_step);
      const std::string signature = design_flow_step->GetSignature();
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Adding design flow step " + design_flow_step->GetName() + " - Signature " + signature);

      ///Get vertex from design flow graph; there are four cases
      vertex step_vertex = GetDesignFlowStep(signature);
      ///The step already exists
      if(step_vertex)
      {
         if(unnecessary)
         {
            ///The step already exists and we are trying to readd as unnecessary; both if now it is unnecessary or not, nothing has to be done
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--This step already exist");
            continue;
         }
         else
         {
            const DesignFlowStepInfoRef design_flow_step_info = design_flow_graph->GetDesignFlowStepInfo(step_vertex);
            if(design_flow_step_info->unnecessary)
            {
               ///The step already exists and it was unnecessary; now we are switching to necessary; note that computation of relationships of this node is performed to propagate the necessity
               ///If design flow step was ready I have to reinsert it into the set because of the ordering; so the setting of the unnecessary flag can not be factorized
               if(possibly_ready.find(step_vertex) != possibly_ready.end())
               {
                  possibly_ready.erase(step_vertex);
                  design_flow_step_info->unnecessary = false;
                  possibly_ready.insert(step_vertex);
               }
               else
               {
                  design_flow_step_info->unnecessary = false;
               }
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---This step already exist but was unnecessary. Now it becomes necessary");
            }
            else
            {
               ///The step already exists and it is already necessary; nothing to do
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--This step already exist");
               continue;
            }
         }
      }
      else
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---This step does not exist");
         step_vertex = design_flow_graphs_collection->AddDesignFlowStep(design_flow_step, unnecessary);
#ifndef NDEBUG
         if(parameters->getOption<bool>(OPT_print_dot) or debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
         {
            step_names[step_vertex] = design_flow_step->GetName();
         }
#endif
      }

      DesignFlowStepSet relationships;

      ///Add edges from dependencies
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Adding dependencies of " + design_flow_step->GetName());
      design_flow_step->ComputeRelationships(relationships, DesignFlowStep::DEPENDENCE_RELATIONSHIP);
      RecursivelyAddSteps(relationships, unnecessary);
      DesignFlowStepSet::const_iterator relationship, relationship_end = relationships.end();
      for(relationship = relationships.begin(); relationship != relationship_end; relationship++)
      {
         const std::string relationship_signature = (*relationship)->GetSignature();
         vertex relationship_vertex = GetDesignFlowStep(relationship_signature);
         design_flow_graphs_collection->AddDesignFlowDependence(relationship_vertex, step_vertex, DesignFlowGraph::DEPENDENCE_SELECTOR);
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Added dependencies of " + design_flow_step->GetName());

      ///Add edge from post dependencies
      relationships.clear();
      design_flow_step->ComputeRelationships(relationships, DesignFlowStep::POST_DEPENDENCE_RELATIONSHIP);
      RecursivelyAddSteps(relationships, unnecessary);
      relationship_end = relationships.end();
      for(relationship = relationships.begin(); relationship != relationship_end; relationship++)
      {
         const std::string relationship_signature = (*relationship)->GetSignature();
         vertex relationship_vertex = GetDesignFlowStep(relationship_signature);
         if(design_flow_graph->IsReachable(relationship_vertex, step_vertex))
         {
            design_flow_graphs_collection->AddDesignFlowDependence(step_vertex, relationship_vertex, DesignFlowGraph::DEPENDENCE_FEEDBACK_SELECTOR);
            DeExecute(relationship_vertex);
         }
         else
         {
            design_flow_graphs_collection->AddDesignFlowDependence(step_vertex, relationship_vertex, DesignFlowGraph::DEPENDENCE_SELECTOR);
         }
      }

      ///Add steps from precedences
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Adding precedences of " + design_flow_step->GetName());
      relationships.clear();
      design_flow_step->ComputeRelationships(relationships, DesignFlowStep::PRECEDENCE_RELATIONSHIP);
      RecursivelyAddSteps(relationships, true);
      relationship_end = relationships.end();
      for(relationship = relationships.begin(); relationship != relationship_end; relationship++)
      {
         const std::string relationship_signature = (*relationship)->GetSignature();
         vertex relationship_vertex = GetDesignFlowStep(relationship_signature);
         design_flow_graphs_collection->AddDesignFlowDependence(relationship_vertex, step_vertex, DesignFlowGraph::PRECEDENCE_SELECTOR);
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Added precedences of " + design_flow_step->GetName());

      ///Add steps from post-precedences
      relationships.clear();
      design_flow_step->ComputeRelationships(relationships, DesignFlowStep::POST_PRECEDENCE_RELATIONSHIP);
      RecursivelyAddSteps(relationships, true);
      relationship_end = relationships.end();
      for(relationship = relationships.begin(); relationship != relationship_end; relationship++)
      {
         const std::string relationship_signature = (*relationship)->GetSignature();
         vertex relationship_vertex = GetDesignFlowStep(relationship_signature);
         design_flow_graphs_collection->AddDesignFlowDependence(step_vertex, relationship_vertex, DesignFlowGraph::PRECEDENCE_SELECTOR);
         const DesignFlowStepInfoRef target_info = design_flow_graph->GetDesignFlowStepInfo(relationship_vertex);
         if(target_info->executed)
         {
            DeExecute(relationship_vertex);
         }
      }

      ///Check if the added step is already ready
      bool current_ready = true;
      InEdgeIterator ie, ie_end;
      for(boost::tie(ie, ie_end) = boost::in_edges(step_vertex, *design_flow_graph); ie != ie_end; ie++)
      {
         vertex pre_dependence_vertex = boost::source(*ie, *design_flow_graph);
         const DesignFlowStepInfoRef pre_info = design_flow_graph->GetDesignFlowStepInfo(pre_dependence_vertex);
         if(not pre_info->executed)
         {
            current_ready = false;
         }
      }
      if(current_ready)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Adding " + design_flow_step->GetName() + " to list of ready steps");
         possibly_ready.insert(step_vertex);
      }
      if(parameters->getOption<bool>(OPT_print_dot))
      {
         feedback_design_flow_graph->WriteDot("Design_Flow_" + boost::lexical_cast<std::string>(step_counter) + "_" + boost::lexical_cast<std::string>(temp_counter));
         temp_counter++;
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Added design flow step " + design_flow_step->GetName() + " - Signature " + signature);
   }
}

const DesignFlowGraphConstRef DesignFlowManager::CGetDesignFlowGraph() const
{
   return design_flow_graph;
}

void DesignFlowManager::Exec()
{
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Started execution of design flow");
   if(parameters->getOption<bool>(OPT_print_dot))
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Writing initial design flow graph");
      feedback_design_flow_graph->WriteDot("Design_Flow_" + boost::lexical_cast<std::string>(step_counter));
   }
   while(possibly_ready.size())
   {
      step_counter++;
      const vertex next = *(possibly_ready.begin());
      possibly_ready.erase(next);
      const DesignFlowStepInfoRef design_flow_step_info = design_flow_graph->GetDesignFlowStepInfo(next);
      const DesignFlowStepRef step = design_flow_step_info->design_flow_step;
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Beginning iteration number " + boost::lexical_cast<std::string>(step_counter) + " - Considering step " + step->GetName());
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Other ready steps are");
      std::set<vertex, DesignFlowStepNecessitySorter>::const_iterator ready_step, ready_step_end = possibly_ready.end();
      for(ready_step = possibly_ready.begin(); ready_step != ready_step_end; ready_step++)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---" + design_flow_graph->CGetDesignFlowStepInfo(*ready_step)->design_flow_step->GetName());
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");

      ///Now check if next is actually ready
      ///First of all check if there are new dependence to add
      DesignFlowStepSet pre_dependence_steps;
      step->ComputeRelationships(pre_dependence_steps, DesignFlowStep::DEPENDENCE_RELATIONSHIP);
      RecursivelyAddSteps(pre_dependence_steps, design_flow_step_info->unnecessary);
      bool current_ready = true;
      DesignFlowStepSet::const_iterator pre_dependence_step, pre_dependence_step_end = pre_dependence_steps.end();
      for(pre_dependence_step = pre_dependence_steps.begin(); pre_dependence_step != pre_dependence_step_end; pre_dependence_step++)
      {
         const vertex pre_dependence_vertex = design_flow_graph->GetDesignFlowStep((*pre_dependence_step)->GetSignature());
         design_flow_graphs_collection->AddDesignFlowDependence(pre_dependence_vertex, next, DesignFlowGraph::DEPENDENCE_SELECTOR);
         const DesignFlowStepInfoRef pre_info = design_flow_graph->GetDesignFlowStepInfo(pre_dependence_vertex);
         if(not pre_info->executed)
         {
            current_ready = false;
         }
      }
      ///Now iterate on ingoing precedence edge
      InEdgeIterator ie, ie_end;
      for(boost::tie(ie, ie_end) = boost::in_edges(next, *design_flow_graph); ie != ie_end; ie++)
      {
         if(design_flow_graph->GetSelector(*ie) & DesignFlowGraph::PRECEDENCE_SELECTOR)
         {
            const vertex precedence_vertex = boost::source(*ie, *design_flow_graph);
            const DesignFlowStepInfoRef pre_info = design_flow_graph->GetDesignFlowStepInfo(precedence_vertex);
            if(not pre_info->executed)
            {
               current_ready = false;
               break;
            }
         }
      }
      if(not current_ready)
      {
         Consolidate();
         if(parameters->getOption<bool>(OPT_print_dot) or debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
         {
            feedback_design_flow_graph->WriteDot("Design_Flow_" + boost::lexical_cast<std::string>(step_counter));
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Writing Design_Flow_" + boost::lexical_cast<std::string>(step_counter));
         }
#ifndef NDEBUG
         ///Save the current status of the graph in history
         VertexIterator temp_step, temp_step_end;
         for(boost::tie(temp_step, temp_step_end) = boost::vertices(*design_flow_graph); temp_step != temp_step_end; temp_step++)
         {
            const DesignFlowStepInfoConstRef temp_design_flow_step_info = design_flow_graph->CGetDesignFlowStepInfo(*temp_step);
            vertex_history[step_counter][*temp_step] = std::pair<bool, bool>(temp_design_flow_step_info->executed, temp_design_flow_step_info->unnecessary);
         }
         EdgeIterator edge, edge_end;
         for(boost::tie(edge, edge_end) = boost::edges(*design_flow_graph); edge != edge_end; edge++)
         {
            edge_history[step_counter][*edge] = design_flow_graph->GetSelector(*edge);
         }
#endif
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Ended iteration number " + boost::lexical_cast<std::string>(step_counter));
         continue;
      }
      if(design_flow_step_info->unnecessary)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Skipping execution of " + step->GetName() + " since unnecessary");
      }
      else
      {
         INDENT_OUT_MEX(OUTPUT_LEVEL_VERY_PEDANTIC, output_level, "-->Starting execution of " + step->GetName());
         step->Initialize();
         step->Exec();
         INDENT_OUT_MEX(OUTPUT_LEVEL_VERY_PEDANTIC, output_level, "<--Ended execution of " + step->GetName());
      }
      design_flow_step_info->executed = true;

      ///Add edge from post dependencies
      DesignFlowStepSet relationships;
      step->ComputeRelationships(relationships, DesignFlowStep::POST_DEPENDENCE_RELATIONSHIP);
      DesignFlowStepSet::const_iterator relationship, relationship_end = relationships.end();
      for(relationship = relationships.begin(); relationship != relationship_end; relationship++)
      {
         const std::string relationship_signature = (*relationship)->GetSignature();
         vertex relationship_vertex = GetDesignFlowStep(relationship_signature);
         if(design_flow_graph->IsReachable(relationship_vertex, next))
         {
            design_flow_graphs_collection->AddDesignFlowDependence(next, relationship_vertex, DesignFlowGraph::DEPENDENCE_FEEDBACK_SELECTOR);
            DeExecute(relationship_vertex);
         }
         else
         {
            design_flow_graphs_collection->AddDesignFlowDependence(next, relationship_vertex, DesignFlowGraph::DEPENDENCE_SELECTOR);
         }
      }
      Consolidate();
      OutEdgeIterator oe, oe_end;
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Starting checking of new ready steps");
      for(boost::tie(oe, oe_end) = boost::out_edges(next, *design_flow_graph); oe != oe_end; oe++)
      {
         const vertex target = boost::target(*oe, *design_flow_graph);
         DesignFlowStepInfoRef target_info = design_flow_graph->GetDesignFlowStepInfo(target);
         if(target_info->executed)
         {
            ///Post dependence previously required and previously executed;
            ///Now it is not more required, otherwise execution flag should just invalidated
            continue;
         }
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Examining successor " + design_flow_graph->GetDesignFlowStepInfo(target)->design_flow_step->GetName());
         bool target_ready = true;
         for(boost::tie(ie, ie_end) = boost::in_edges(target, *design_flow_graph); ie != ie_end; ie++)
         {
            const vertex source = boost::source(*ie, *design_flow_graph);
            const DesignFlowStepInfoRef source_info = design_flow_graph->GetDesignFlowStepInfo(source);
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Examining predecessor " + source_info->design_flow_step->GetName());
            if(not source_info->executed)
            {
               target_ready = false;
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not ready");
               break;
            }
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
         }
         if(target_ready)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Adding " + design_flow_graph->CGetDesignFlowStepInfo(target)->design_flow_step->GetName() + " to list of ready steps");
            possibly_ready.insert(target);
         }
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Checked new ready steps");
      if(parameters->getOption<bool>(OPT_print_dot) or debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Writing Design_Flow_" + boost::lexical_cast<std::string>(step_counter));
         feedback_design_flow_graph->WriteDot("Design_Flow_" + boost::lexical_cast<std::string>(step_counter));

#ifndef NDEBUG
         ///Save the current status of the graph in history
         VertexIterator temp_step, temp_step_end;
         for(boost::tie(temp_step, temp_step_end) = boost::vertices(*design_flow_graph); temp_step != temp_step_end; temp_step++)
         {
            const DesignFlowStepInfoConstRef temp_design_flow_step_info = design_flow_graph->CGetDesignFlowStepInfo(*temp_step);
            vertex_history[step_counter][*temp_step] = std::pair<bool, bool>(temp_design_flow_step_info->executed, temp_design_flow_step_info->unnecessary);
         }
         EdgeIterator edge, edge_end;
         for(boost::tie(edge, edge_end) = boost::edges(*design_flow_graph); edge != edge_end; edge++)
         {
            edge_history[step_counter][*edge] = design_flow_graph->GetSelector(*edge);
         }
#endif
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Ended iteration number " + boost::lexical_cast<std::string>(step_counter) + " - Step " + step->GetName());
   }
#ifndef NDEBUG
   if(parameters->getOption<bool>(OPT_print_dot) or debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
   {
      for(size_t writing_step_counter = 0; writing_step_counter < step_counter; writing_step_counter++)
      {
         if(edge_history.find(writing_step_counter) != edge_history.end())
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Writing Design_Flow_History_" + boost::lexical_cast<std::string>(writing_step_counter));
            feedback_design_flow_graph->WriteDot("Design_Flow_History_" + boost::lexical_cast<std::string>(writing_step_counter), vertex_history, edge_history, step_names, writing_step_counter);
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Written Design_Flow_History_" + boost::lexical_cast<std::string>(writing_step_counter));
         }
      }
   }
#endif
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Ended execution of design flow");
}

vertex DesignFlowManager::GetDesignFlowStep(const std::string signature) const
{
   return design_flow_graphs_collection->GetDesignFlowStep(signature);
}

const DesignFlowStepFactoryConstRef DesignFlowManager::CGetDesignFlowStepFactory(const std::string prefix) const
{
   THROW_ASSERT(design_flow_step_factories.find(prefix) != design_flow_step_factories.end(), "No factory to create steps with prefix " + prefix + " found");
   return design_flow_step_factories.find(prefix)->second;
}

void DesignFlowManager::RegisterFactory(const DesignFlowStepFactoryConstRef factory)
{
   design_flow_step_factories[factory->GetPrefix()] = factory;
}

void DesignFlowManager::Consolidate()
{
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Consolidating graph");
   const DesignFlowGraphInfoConstRef design_flow_graph_info = design_flow_graph->CGetDesignFlowGraphInfo();
   const vertex entry = design_flow_graph_info->entry;
   const vertex exit = design_flow_graph_info->exit;
   VertexIterator step, step_end;
   for(boost::tie(step, step_end) = boost::vertices(*design_flow_graph); step != step_end; step++)
   {
      if(*step != entry and *step != exit)
      {
         if(boost::in_degree(*step, *design_flow_graph) == 0)
         {
            design_flow_graphs_collection->AddDesignFlowDependence(design_flow_graph_info->entry, *step, DesignFlowGraph::AUX_SELECTOR);
         }
         if(boost::out_degree(*step, *design_flow_graph) == 0)
         {
            design_flow_graphs_collection->AddDesignFlowDependence(*step, design_flow_graph_info->exit, DesignFlowGraph::AUX_SELECTOR);
         }
      }
   }
}

void DesignFlowManager::DeExecute(const vertex starting_vertex)
{
   ///Set not executed on the starting vertex
   design_flow_graph->GetDesignFlowStepInfo(starting_vertex)->executed = false;

   ///Check if the vertex is already ready
   bool current_ready = true;
   InEdgeIterator ie, ie_end;
   for(boost::tie(ie, ie_end) = boost::in_edges(starting_vertex, *design_flow_graph); ie != ie_end; ie++)
   {
      vertex pre_dependence_vertex = boost::source(*ie, *design_flow_graph);
      const DesignFlowStepInfoRef pre_info = design_flow_graph->GetDesignFlowStepInfo(pre_dependence_vertex);
      if(not pre_info->executed)
      {
         current_ready = false;
         break;
      }
   }
   if(current_ready)
   {
      possibly_ready.insert(starting_vertex);
   }

   ///Propagating to successor
   OutEdgeIterator oe, oe_end;
   for(boost::tie(oe, oe_end) = boost::out_edges(starting_vertex, *design_flow_graph); oe != oe_end; oe++)
   {
      const vertex target = boost::target(*oe, *design_flow_graph);
      const DesignFlowStepInfoRef target_info = design_flow_graph->GetDesignFlowStepInfo(target);
      if(target_info->executed)
      {
         DeExecute(target);
      }
   }
}

