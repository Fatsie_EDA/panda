/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file HDL_manager.cpp
 * @brief Implementation of the base methods for writing HDL descriptions
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Header include
#include "HDL_manager.hpp"

///Autoheader include
#include "config_HAVE_FLOPOCO.hpp"
#include "config_PACKAGE_BUGREPORT.hpp"
#include "config_PACKAGE_NAME.hpp"
#include "config_PACKAGE_VERSION.hpp"

#include "structural_manager.hpp"
#include "structural_objects.hpp"
#include "NP_functionality.hpp"

#include "technology_manager.hpp"
#include "technology_builtin.hpp"
#include "target_device.hpp"
#include "FPGA_device.hpp"

#if HAVE_FLOPOCO
#include "flopoco_wrapper.hpp"
#endif

#include "Parameter.hpp"
#include "dbgPrintHelper.hpp"
#include "exceptions.hpp"

#include <iostream>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>

#define COPYING3_SHORT_NROW 17
const char* COPYING3_SHORT[COPYING3_SHORT_NROW] = {
   "************************************************************************\n",
   "The following text holds for all the components tagged with PANDA_GPLv3.\n",
   "\n",
   "This hardware description is free; you can redistribute it and/or modify\n",
   "it under the terms of the GNU General Public License as published by\n",
   "the Free Software Foundation; either version 3, or (at your option)\n",
   "any later version.\n",
   "\n",
   "This hardware description is distributed in the hope that it will be useful,\n",
   "but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY\n",
   "or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License\n",
   "for more details.\n",
   "\n",
   "You should have received a copy of the GNU General Public License\n",
   "along with the PandA framework; see the files COPYING\n",
   "If not, see <http://www.gnu.org/licenses/>.\n",
   "************************************************************************\n\n"
};

HDL_manager::HDL_manager(const target_deviceRef _device, const ParameterConstRef _Param) :
   device(_device),
   TM(_device->get_technology_manager()),
   Param(_Param),
   debug_level(_Param->getOption<int>(OPT_debug_level))
{
#if HAVE_FLOPOCO
   flopo_wrap = flopoco_wrapperRef(new flopoco_wrapper(debug_level, _device->get_parameter<std::string>("family")));
#endif
}

HDL_manager::~HDL_manager()
{

}

std::string HDL_manager::write_components(std::string filename, language_writer::language lan, const std::string& top, const std::list<structural_objectRef>& components, bool equation) const
{
   language_writerRef writer = language_writer::create_writer(lan, debug_level);

   structural_objectRef top_obj;
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  check if the structural components are compliant with the language writer");
   ///check if the structural components are compliant with the language writer
   bool are_compliant = true;
   std::list<structural_objectRef>::const_iterator cit_end = components.end();
   ///verify which is the proper backend writer for the component
   for (std::list<structural_objectRef>::const_iterator cit = components.begin(); are_compliant && cit != cit_end; cit++)
   {
      if (top == (*cit)->get_id()) top_obj = *cit;
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "    checking " + (*cit)->get_path());
      are_compliant = writer->check_structural_object_compliance(*cit);
      if (!are_compliant)
         THROW_WARNING("Component " + (*cit)->get_id() + " is not compliant with the " + writer->get_name() + " writer");
   }
   if (!are_compliant)
      THROW_ERROR("hdl_gen cannot continue...");

   std::string complete_filename = filename + writer->get_extension();
   std::ofstream hdl_file(complete_filename.c_str());

   writer->write_comment(hdl_file, std::string("File automatically generated by: ") + PACKAGE_NAME + " framework version=" + PACKAGE_VERSION + "\n");
   writer->write_comment(hdl_file, std::string("Send any bug to: ") + PACKAGE_BUGREPORT + "\n");

   for(unsigned int row=0; row < COPYING3_SHORT_NROW; ++row)
      writer->write_comment(hdl_file, std::string(COPYING3_SHORT[row]));

   ///write the header of the file
   writer->write_header(hdl_file);

   ///write all modules
   for (std::list<structural_objectRef>::const_iterator cit = components.begin(); cit != cit_end; cit++)
   {
      structural_objectRef obj = *cit;
      NP_functionalityRef npf = GetPointer<module>(obj)->get_NP_functionality();
      if (npf and npf->get_NP_functionality(NP_functionality::FLOPOCO_PROVIDED) != "")
      {
         write_module(hdl_file, writer, obj, equation);
         continue;
      }
      std::string library = TM->get_library((*cit)->get_typeRef()->id_type);
      ///we write the definition of the object stored in library
      if (library.size())
      {
         technology_nodeRef tn = TM->get_fu((*cit)->get_typeRef()->id_type, library);
         if (GetPointer<functional_unit>(tn))
            obj = GetPointer<functional_unit>(tn)->CM->get_circ();
         else if (GetPointer<functional_unit_template>(tn))
         {
            technology_nodeRef FU = GetPointer<functional_unit_template>(tn)->FU;
            obj = GetPointer<functional_unit>(FU   )->CM->get_circ();
         }
         else
            THROW_ERROR("unexpected condition");
      }
      write_module(hdl_file, writer, obj, equation);
   }
   ///write the tail of the file
   writer->write_tail(hdl_file, top_obj);

   return complete_filename;
}

void HDL_manager::write_components(std::string filename, const std::string& top, const std::list<structural_objectRef>& components, bool equation)
{
   ///default language
   language_writer::language lan = static_cast<language_writer::language>(Param->getOption<unsigned int>(OPT_writer_language));

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Everything seems ok. Let's start with the real job.");

   NP_functionality::NP_functionaly_type type = NP_functionality::UNKNOWN;
   if (lan == language_writer::VERILOG)
   {
      type = NP_functionality::VERILOG_PROVIDED;
   }
   else if (lan == language_writer::SYSTEM_VERILOG)
   {
      type = NP_functionality::SYSTEM_VERILOG_PROVIDED;
   }
   else if (lan == language_writer::VHDL)
   {
      type = NP_functionality::VHDL_PROVIDED;
   }
   else
      THROW_ERROR("Language not supported");

   ///determine the proper language for each component
   std::map<language_writer::language, std::list<structural_objectRef> > component_language;
   for (std::list<structural_objectRef>::const_iterator cit = components.begin(); cit != components.end(); cit++)
   {
      module* mod = GetPointer<module>(*cit);
      THROW_ASSERT(mod, "Expected a component object");

      unsigned int n_elements = mod->get_internal_objects_size();

      const NP_functionalityRef np = mod->get_NP_functionality();

      if (n_elements || (np && np->exist_NP_functionality(type)))
      {
         component_language[lan].push_back(*cit);
      }
      else
      {
         if (np && (np->exist_NP_functionality(NP_functionality::VERILOG_PROVIDED) || np->exist_NP_functionality(NP_functionality::FSM)))
            component_language[language_writer::VERILOG].push_back(*cit);
         else if (np && (np->exist_NP_functionality(NP_functionality::VHDL_PROVIDED) || np->exist_NP_functionality(NP_functionality::FLOPOCO_PROVIDED)))
            component_language[language_writer::VHDL].push_back(*cit);
         else if (np && np->exist_NP_functionality(NP_functionality::SYSTEM_VERILOG_PROVIDED))
            component_language[language_writer::SYSTEM_VERILOG].push_back(*cit);
         else
            THROW_ERROR("Language not supported! Module " + mod->get_path());
      }
   }

   ///generate the auxiliary files
   for(std::map<language_writer::language, std::list<structural_objectRef> >::iterator l = component_language.begin(); l != component_language.end(); l++)
   {
      if (lan == l->first) continue;
      std::string generated_filename = write_components(filename, l->first, top, component_language[l->first], equation);
      HDL_aux_files.push_back(generated_filename);
   }

   std::string complete_filename = write_components(filename, lan, top, component_language[lan], equation);
   ///add the generated file to the global list
   HDL_files.push_back(complete_filename);

#if HAVE_FLOPOCO
   /// as a last thing to do we wrote all the common FloPoCo components
   flopo_wrap->writeVHDLcommon();
#endif
}

void HDL_manager::hdl_gen(std::string filename, const structural_objectRef cir, bool equation)
{
   HDL_files.clear();
   HDL_aux_files.clear();

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  compute the list of components for which a structural description exists");
   ///compute the list of components for which a structural description exist.
   std::list<structural_objectRef> list_of_com;
   get_post_order_structural_components(cir, list_of_com);
   if (list_of_com.empty())
   {
      THROW_ASSERT(GetPointer<module>(cir), "Expected a component or a channel");
      THROW_ASSERT(GetPointer<module>(cir)->get_NP_functionality(), "Structural empty description received");
      return;
   }

   ///generate the HDL descriptions for all the components
   write_components(filename, cir->get_id(), list_of_com, equation);
}

void HDL_manager::hdl_gen(std::string filename, const std::string& top, const std::vector<structural_objectRef> &cirs, bool hierarchical, bool equation)
{
   HDL_files.clear();
   HDL_aux_files.clear();

   ///compute the list of components for which a structural description exists.
   std::list<structural_objectRef> list_of_com;
   structural_objectRef cir;
   for(unsigned int i = 0; i < cirs.size(); i++)
   {
      if (!hierarchical)
      {
         list_of_com.push_back(cirs[i]);
         continue;
      }

      size_t size = list_of_com.size();
      if (cirs[i]->get_id() == top)
         cir = cirs[i];
      get_post_order_structural_components(cirs[i], list_of_com);
      if (size == list_of_com.size())
      {
         THROW_ASSERT(GetPointer<module>(cirs[i]), "Expected a component or a channel");
         THROW_ASSERT(GetPointer<module>(cirs[i])->get_NP_functionality(), "Structural empty description received: " + cirs[i]->get_path());
         return;
      }
   }

   ///generate the HDL descriptions for all the components
   write_components(filename, top, list_of_com, equation);
}

/**
 * Used to test if a component has been already inserted into the component list
 */
struct find_eq_module
{
   //predicate function
   bool operator()(const structural_objectRef& el)
   {
      return el == target || HDL_manager::get_mod_typename(lan, el) == HDL_manager::get_mod_typename(lan, target);
   }
   find_eq_module(const language_writer* _lan, const structural_objectRef& _target) : lan(_lan), target(_target)
   {
      THROW_ASSERT(_target, "structural_objectRef must exist");
   }
private:
   const language_writer* lan;
   const structural_objectRef& target;
};

bool HDL_manager::is_fsm(const structural_objectRef &cir) const
{
   /// check for a fsm description
   module * mod_inst = GetPointer<module>(cir);
   THROW_ASSERT(mod_inst, "Expected a component or a channel");
   const NP_functionalityRef &np = mod_inst->get_NP_functionality();
   if (np)
   {
      return np->exist_NP_functionality(NP_functionality::FSM);
   }
   return false;
}

void HDL_manager::get_post_order_structural_components(const structural_objectRef cir, std::list<structural_objectRef> &list_of_com) const
{
   switch (cir->get_kind())
   {
      case component_o_K:
      case channel_o_K:
      {
         module * mod = GetPointer<module>(cir);
         unsigned int n_elements = mod->get_internal_objects_size();
         if(n_elements)
         {
            for (unsigned int i = 0; i < n_elements; i++)
            {
               switch (mod->get_internal_object(i)->get_kind())
               {
                  case channel_o_K:
                  case component_o_K:
                  {
                     if (!mod->get_internal_object(i)->get_black_box() and !builtin_verilog_gates::is_builtin(GET_TYPE_NAME(mod->get_internal_object(i))))
                        get_post_order_structural_components(mod->get_internal_object(i), list_of_com);
                     break;
                  }
                  case constant_o_K:
                  case signal_vector_o_K:
                  case signal_o_K:
                  case bus_connection_o_K:
                     break; ///no action for signals and bus
                  case action_o_K:
                  case data_o_K:
                  case event_o_K:
                  case port_o_K:
                  case port_vector_o_K:
                  default:
                     THROW_ERROR("Structural object not foreseen: " + std::string(mod->get_internal_object(i)->get_kind_text()));
               }
            }
         }
         NP_functionalityRef NPF = mod->get_NP_functionality();
         if (NPF and NPF->exist_NP_functionality(NP_functionality::IP_COMPONENT))
         {
            std::string ip_cores = NPF->get_NP_functionality(NP_functionality::IP_COMPONENT);
            std::vector<std::string> ip_cores_list = convert_string_to_vector<std::string>(ip_cores, ",");
            for(unsigned int ip = 0; ip < ip_cores_list.size(); ip++)
            {
               std::string ip_core = ip_cores_list[ip];
               std::vector<std::string> ip_core_vec = convert_string_to_vector<std::string>(ip_core, ":");
               if (ip_core_vec.size() < 1 or ip_core_vec.size() > 2) THROW_ERROR("Malformed IP component definition \"" + ip_core + "\"");
               std::string library, component_name;
               if (ip_core_vec.size() == 2)
               {
                  library = ip_core_vec[0];
                  component_name = ip_core_vec[1];
               }
               else
               {
                  component_name = ip_core_vec[0];
                  library = TM->get_library(component_name);
               }
               technology_nodeRef tn = TM->get_fu(component_name, library);
               structural_objectRef core_cir;
               if(tn->get_kind() == functional_unit_K)
                  core_cir = GetPointer<functional_unit>(tn)->CM->get_circ();
               else if(tn->get_kind() == functional_unit_template_K && GetPointer<functional_unit>(GetPointer<functional_unit_template>(tn)->FU))
                  core_cir = GetPointer<functional_unit>(GetPointer<functional_unit_template>(tn)->FU)->CM->get_circ();
               else
                  THROW_ERROR("Unexpected pattern");
               get_post_order_structural_components(core_cir, list_of_com);
               std::list<structural_objectRef>::iterator fo = std::find_if(list_of_com.begin(), list_of_com.end(), find_eq_module(nullptr, core_cir));
               if (fo == list_of_com.end())
               {
                  list_of_com.push_back(core_cir);
               }
            }
         }
         std::list<structural_objectRef>::iterator fo = std::find_if(list_of_com.begin(), list_of_com.end(), find_eq_module(nullptr, cir));
         if (fo == list_of_com.end())
            list_of_com.push_back(cir);
         break;
      }
      case action_o_K:
      case bus_connection_o_K:
      case constant_o_K:
      case data_o_K:
      case event_o_K:
      case port_o_K:
      case port_vector_o_K:
      case signal_o_K:
      case signal_vector_o_K:
      default:
         THROW_ERROR("Structural object not foreseen");
   }
}
void HDL_manager::io_signal_fix_ith(std::ostream& os, const language_writerRef writer, const structural_objectRef po, bool& lspf) const
{
   THROW_ASSERT(po && po->get_kind() == port_o_K, "Expected a port; got something different");
   port_o * p = GetPointer<port_o>(po);
   structural_objectRef po_owner = po->get_owner();
   if (po_owner->get_kind() == port_vector_o_K)
      po_owner = po_owner->get_owner();
   for (unsigned int j = 0; j < p->get_connections_size(); j++)
   {
      if (p->get_connection(j)->get_kind() == signal_o_K and
          (p->get_connection(j)->get_owner() == po_owner or
           (p->get_connection(j)->get_owner()->get_kind() == signal_vector_o_K and p->get_connection(j)->get_owner()->get_owner() == po_owner)
          )
         )
      {
         if (!lspf)
         {
            writer->write_comment(os, "io-signal post fix\n");
            lspf = true;
         }
         writer->write_io_signal_post_fix(os, po, p->get_connection(j));
      }
      if (p->get_connection(j)->get_kind() == constant_o_K and p->get_connection(j)->get_owner() == po_owner)
      {
         if (!lspf)
         {
            writer->write_comment(os, "io-signal post fix\n");
            lspf = true;
         }
         writer->write_io_signal_post_fix(os, po, p->get_connection(j));
      }
   }
}

void HDL_manager::write_module(std::ostream& os, const language_writerRef writer, const structural_objectRef cir, bool equation) const
{
   const module * mod = GetPointer<module>(cir);
   THROW_ASSERT(mod, "Expected a module got something of different");

   const NP_functionalityRef &np = mod->get_NP_functionality();
   if (np && np->get_NP_functionality(NP_functionality::FLOPOCO_PROVIDED) != "")
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_PARANOIC, this->debug_level, "FloPoCo compliant module: " + mod->get_id());
      this->write_flopoco_module(cir);
      return;
   }

   ///write module declaration
   writer->write_comment(os, mod->get_description() + "\n");
   writer->write_comment(os, mod->get_copyright() + "\n");
   writer->write_comment(os, "Author(s): " + mod->get_authors() + "\n");
   writer->write_comment(os, "License: " + mod->get_license() + "\n");

   ///write library declaration component
   writer->write_library_declaration(os, cir);

   writer->write_module_declaration(os, cir);

   writer->write_module_parametrization_decl(os,cir);

   writer->write_port_decl_header(os);

   /// write IO port declarations
   if (mod->get_in_port_size())
   {
      writer->write_comment(os, "IN\n");
      for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
      {
         if (i == mod->get_in_port_size()-1 && !mod->get_out_port_size() && !mod->get_in_out_port_size() && !mod->get_gen_port_size())
            writer->write_port_declaration(os, mod->get_in_port(i), true);
         else
            writer->write_port_declaration(os, mod->get_in_port(i), false);
      }
   }
   if (mod->get_out_port_size())
   {
      writer->write_comment(os, "OUT\n");
      for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
      {
         if (i == mod->get_out_port_size()-1 && !mod->get_in_out_port_size() && !mod->get_gen_port_size())
            writer->write_port_declaration(os, mod->get_out_port(i), true);
         else
            writer->write_port_declaration(os, mod->get_out_port(i), false);
      }
   }
   if (mod->get_in_out_port_size())
   {
      writer->write_comment(os, "INOUT\n");
      for (unsigned int i = 0; i < mod->get_in_out_port_size(); i++)
      {
         if (i == mod->get_in_out_port_size()-1 && !mod->get_gen_port_size())
            writer->write_port_declaration(os, mod->get_in_out_port(i), true);
         else
            writer->write_port_declaration(os, mod->get_in_out_port(i), false);
      }
   }

   if (mod->get_gen_port_size())
   {
      writer->write_comment(os, "Ports\n");
      for (unsigned int i = 0; i < mod->get_gen_port_size(); i++)
      {
         if (i == mod->get_gen_port_size()-1)
            writer->write_port_declaration(os, mod->get_gen_port(i), true);
         else
            writer->write_port_declaration(os, mod->get_gen_port(i), false);
      }
   }

   writer->write_port_decl_tail(os);

   ///close the interface declaration and start the implementation
   writer->write_module_internal_declaration(os, cir);

   ///specify the timing annotations to the components
   if (Param->getOption<bool>(OPT_timing_simulation))
   {
      ///specify timing characterization
      writer->write_timing_specification(os, TM, cir);
   }

   if(equation)
   {
      std::string behav = np->get_NP_functionality(NP_functionality::EQUATION);
      write_behavioral(os, writer, cir, behav);
   }
   else
   {
      /// write components declarations
      /// write signal declarations
      unsigned int n_elements = mod->get_internal_objects_size();
      if(n_elements)
      {
         writer->write_comment(os, "Component and signal declarations\n");

         std::list<std::pair<std::string, structural_objectRef> > cs;
         for (unsigned int i = 0; i < n_elements; i++)
         {
            switch (mod->get_internal_object(i)->get_kind())
            {
            case channel_o_K:
            case component_o_K:
               {
                  break;
               }
            case signal_vector_o_K:
            case signal_o_K:
               {
                  cs.push_back(std::make_pair(mod->get_internal_object(i)->get_id(), mod->get_internal_object(i)));
                  continue;
               }
            case bus_connection_o_K:
               THROW_ERROR("Bus connection not yes supported.");
            case action_o_K:
            case constant_o_K:
            case data_o_K:
            case event_o_K:
            case port_o_K:
            case port_vector_o_K:
            default:
               ;//do nothing
            }
            writer->write_component_declaration(os, mod->get_internal_object(i));
         }
         cs.sort();

         for(std::list<std::pair<std::string, structural_objectRef> >::iterator c = cs.begin(); c != cs.end(); c++)
         {
            writer->write_signal_declaration(os, c->second);
         }

         ///write module_instantiation begin
         writer->write_module_definition_begin(os, cir);

         cs.clear();
         /// write module instantiation & connection binding
         for (unsigned int i = 0; i < n_elements; i++)
         {
            switch (mod->get_internal_object(i)->get_kind())
            {
               case channel_o_K:
               case component_o_K:
                  {
                     cs.push_back(std::make_pair(mod->get_internal_object(i)->get_id(), mod->get_internal_object(i)));
                     break;
                  }
               case action_o_K:
               case bus_connection_o_K:
               case constant_o_K:
               case data_o_K:
               case event_o_K:
               case port_o_K:
               case port_vector_o_K:
               case signal_o_K:
               case signal_vector_o_K:
               default:
                  ;//do nothing
            }
         }
         cs.sort();

         for(std::list<std::pair<std::string, structural_objectRef> >::iterator c = cs.begin(); c != cs.end(); c++)
         {
            structural_objectRef obj = c->second;
            writer->write_module_instance_begin(os, obj, true);
            /// write IO ports binding
            module * mod_inst = GetPointer<module>(obj);
            bool first_port_analyzed = false;
            /// First output and then input. Some backend could have benefits from this ordering.
            /// Some customizations are possible, like direct translation of gates into built-in statements.
            if (writer->has_output_prefix())
            {
               if (mod_inst->get_out_port_size())
               {
                  //writer->write_comment(os, "OUT binding\n");
                  for (unsigned int i = 0; i < mod_inst->get_out_port_size(); i++)
                  {
                     if (mod_inst->get_out_port(i)->get_kind() == port_o_K)
                     {
                        const structural_objectRef object_bounded = GetPointer<port_o>(mod_inst->get_out_port(i))->find_bounded_object(cir);
                        if (!object_bounded) continue;
                        writer->write_port_binding(os, mod_inst->get_out_port(i), object_bounded, first_port_analyzed);
                     }
                     else
                     {
                        writer->write_vector_port_binding(os, mod_inst->get_out_port(i), first_port_analyzed);
                     }
                     first_port_analyzed = true;
                  }
               }
            }
            if (mod_inst->get_in_port_size())
            {
               //writer->write_comment(os, "IN binding\n");
               for (unsigned int i = 0; i < mod_inst->get_in_port_size(); i++)
               {
                  if (mod_inst->get_in_port(i)->get_kind() == port_o_K)
                  {
                     const structural_objectRef object_bounded = GetPointer<port_o>(mod_inst->get_in_port(i))->find_bounded_object(cir);
                     writer->write_port_binding(os, mod_inst->get_in_port(i), object_bounded, first_port_analyzed);
                  }
                  else
                  {
                     writer->write_vector_port_binding(os, mod_inst->get_in_port(i), first_port_analyzed);
                  }
                  first_port_analyzed = true;
               }
            }
            if (mod_inst->get_in_out_port_size())
            {
               //writer->write_comment(os, "INOUT binding\n");
               for (unsigned int i = 0; i < mod_inst->get_in_out_port_size(); i++)
               {
                  if (mod_inst->get_in_out_port(i)->get_kind() == port_o_K)
                  {
                     const structural_objectRef object_bounded = GetPointer<port_o>(mod_inst->get_in_out_port(i))->find_bounded_object();
                     if (!object_bounded) continue;
                     writer->write_port_binding(os, mod_inst->get_in_out_port(i), object_bounded, first_port_analyzed);
                  }
                  else
                  {
                     writer->write_vector_port_binding(os, mod_inst->get_in_out_port(i), first_port_analyzed);
                  }
                  first_port_analyzed = true;
               }
            }
            if (mod_inst->get_gen_port_size())
            {
               //writer->write_comment(os, "Ports binding\n");
               for (unsigned int i = 0; i < mod_inst->get_gen_port_size(); i++)
               {
                  if (mod_inst->get_gen_port(i)->get_kind() == port_o_K)
                     writer->write_port_binding(os, mod_inst->get_gen_port(i), GetPointer<port_o>(mod_inst->get_gen_port(i))->find_bounded_object(), first_port_analyzed);
                  else
                  {
                     writer->write_vector_port_binding(os, mod_inst->get_gen_port(i), first_port_analyzed);
                  }
                  first_port_analyzed = true;
               }
            }
            if (!writer->has_output_prefix())
            {
               if (mod_inst->get_out_port_size())
               {
                  //writer->write_comment(os, "OUT binding\n");
                  for (unsigned int i = 0; i < mod_inst->get_out_port_size(); i++)
                  {
                     if (mod_inst->get_out_port(i)->get_kind() == port_o_K)
                     {
                        const structural_objectRef object_bounded = GetPointer<port_o>(mod_inst->get_out_port(i))->find_bounded_object();
                        if (!object_bounded) continue;
                        writer->write_port_binding(os, mod_inst->get_out_port(i), object_bounded, first_port_analyzed);
                     }
                     else
                     {
                        writer->write_vector_port_binding(os, mod_inst->get_out_port(i), first_port_analyzed);
                     }
                     first_port_analyzed = true;
                  }
               }
            }

            writer->write_module_instance_end(os, obj);
         }


         /// write loop signal post fix
         bool lspf = false;
         if (mod->get_in_port_size())
         {
            for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
            {
               if (mod->get_in_port(i)->get_kind() == port_o_K)
                  io_signal_fix_ith(os, writer, mod->get_in_port(i), lspf);
               else
               {
                  port_o * pv = GetPointer<port_o>(mod->get_in_port(i));
                  for (unsigned int k = 0; k < pv->get_ports_size(); k++)
                     io_signal_fix_ith(os, writer, pv->get_port(k), lspf);
               }
            }
         }
         if (mod->get_out_port_size())
         {
            for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
            {
               if (mod->get_out_port(i)->get_kind() == port_o_K)
                  io_signal_fix_ith(os, writer, mod->get_out_port(i), lspf);
               else
               {
                  port_o * pv = GetPointer<port_o>(mod->get_out_port(i));
                  for (unsigned int k = 0; k < pv->get_ports_size(); k++)
                     io_signal_fix_ith(os, writer, pv->get_port(k), lspf);
               }
            }
         }
         if (mod->get_in_out_port_size())
         {
            for (unsigned int i = 0; i < mod->get_in_out_port_size(); i++)
            {
               if (mod->get_in_out_port(i)->get_kind() == port_o_K)
                  io_signal_fix_ith(os, writer, mod->get_in_out_port(i), lspf);
               else
               {
                  port_o * pv = GetPointer<port_o>(mod->get_in_out_port(i));
                  for (unsigned int k = 0; k < pv->get_ports_size(); k++)
                     io_signal_fix_ith(os, writer, pv->get_port(k), lspf);
               }
            }
         }
         ///for generic ports the post fix is not required. A generic port is never attached to a signal.

      }
      /// check if there is some behavior attached to the module
      else if(is_fsm(cir))
      {
         THROW_ASSERT(np, "Behavior not expected: " + HDL_manager::convert_to_identifier(writer.get(), GET_TYPE_NAME(cir)));
         std::string fsm_desc = np->get_NP_functionality(NP_functionality::FSM);
         THROW_ASSERT(fsm_desc != "", "Behavior not expected: "+HDL_manager::convert_to_identifier(writer.get(), GET_TYPE_NAME(cir)));
         write_fsm(os, writer, cir, fsm_desc);
      }
      else if(np)
         writer->write_NP_functionalities(os,cir);
      else
      {
         THROW_ASSERT(!cir->get_black_box(), "black box componet has to be managed in a different way");
      }
   }

   ///write module_instantiation end
   writer->write_module_definition_end(os, cir);
}

void HDL_manager::write_flopoco_module(const structural_objectRef &cir) const
{
   module * mod_inst = GetPointer<module>(cir);
   long long int mod_size_in = 0, mod_size_out=0;
   for (unsigned int i = 0; i < mod_inst->get_in_port_size(); i++)
   {
      // Size of module is size of the largest output
      if (mod_size_in < STD_GET_SIZE(mod_inst->get_in_port(i)->get_typeRef()))
         mod_size_in = STD_GET_SIZE(mod_inst->get_in_port(i)->get_typeRef());
   }
   for (unsigned int i = 0; i < mod_inst->get_out_port_size(); i++)
   {
      // Size of module is size of the largest output
      if (mod_size_out < STD_GET_SIZE(mod_inst->get_out_port(i)->get_typeRef()))
         mod_size_out = STD_GET_SIZE(mod_inst->get_out_port(i)->get_typeRef());
   }
#if HAVE_FLOPOCO
   language_writerRef lan;
   std::string mod_type = mod_inst->get_NP_functionality()->get_NP_functionality(NP_functionality::FLOPOCO_PROVIDED);
   std::string mod_name = convert_to_identifier(lan.get(), GET_TYPE_NAME(cir));
   // Create the module
   PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, this->debug_level, "Creating " + mod_type + ", named " + mod_name + " whose size is " + STR(mod_size_in) + "|" + STR(mod_size_out));
   std::string pipe_parameter;
   if(mod_inst->is_parameter(PIPE_PARAMETER))
      pipe_parameter = mod_inst->get_parameter(PIPE_PARAMETER);
   flopo_wrap->add_FU(mod_type, static_cast<unsigned int>(mod_size_in), static_cast<unsigned int>(mod_size_out), mod_name, pipe_parameter);
   flopo_wrap->writeVHDL(mod_name, static_cast<unsigned int>(mod_size_in), static_cast<unsigned int>(mod_size_out), pipe_parameter);
#else
   THROW_ERROR("Floating point based HLS requires --enable-flopoco at configuration time");
#endif
}

void HDL_manager::write_behavioral(std::ostream& os, const language_writerRef writer, const structural_objectRef &, const std::string &behav) const
{
   std::vector<std::string> SplitVec;
   boost::algorithm::split( SplitVec, behav, boost::algorithm::is_any_of(";"));
   THROW_ASSERT(SplitVec.size(), "Expected at least one behavioral description");

   for(unsigned int i = 0; i < SplitVec.size(); i++)
   {
      std::vector<std::string> SplitVec2;
      boost::algorithm::split( SplitVec2, SplitVec[i], boost::algorithm::is_any_of("="));
      THROW_ASSERT(SplitVec2.size() == 2, "Expected two operands");
      writer->write_assign(os, SplitVec2[0], SplitVec2[1]);
   }
}

void HDL_manager::write_fsm(std::ostream& os, const language_writerRef writer, const structural_objectRef &cir, const std::string &fsm_desc_i) const
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Start writing the FSM...");

   std::string fsm_desc=fsm_desc_i;
   boost::algorithm::erase_all(fsm_desc, "\n");

   //std::cout << fsm_desc_i << std::endl;

   std::vector<std::string> SplitVec;
   boost::algorithm::split( SplitVec, fsm_desc, boost::algorithm::is_any_of(";") );
   THROW_ASSERT(SplitVec.size()>1, "Expected more than one ';' in the fsm specification (the first is the reset)");
   //std::cerr << " size = " << SplitVec.size() << std::endl;

   typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
   boost::char_separator<char> sep(" ", nullptr);
   //compute the list of states
   std::list<std::string> list_of_states;
   std::vector<std::string>::const_iterator it_end = SplitVec.end();
   std::vector<std::string>::const_iterator it = SplitVec.begin();
   tokenizer first_line_tokens(*it, sep);
   tokenizer::iterator tok_iter = first_line_tokens.begin();
   std::string reset_state = convert_to_identifier(writer.get(), *tok_iter);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Reset state: '" << reset_state << "'");
   tok_iter++;
   THROW_ASSERT(tok_iter != first_line_tokens.end(), "Wrong fsm description: expected the reset port name");
   std::string reset_port = convert_to_identifier(writer.get(), *tok_iter);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Reset port: '" << reset_port << "'");
   tok_iter++;
   THROW_ASSERT(tok_iter != first_line_tokens.end(), "Wrong fsm description: expected the start port name");
   std::string start_port = convert_to_identifier(writer.get(), *tok_iter);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Start port: '" << start_port << "'");
   tok_iter++;
   THROW_ASSERT(tok_iter != first_line_tokens.end(), "Wrong fsm description: expected the clock port name");
   std::string clock_port = convert_to_identifier(writer.get(), *tok_iter);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Clock port: '" << clock_port << "'");
   tok_iter++;
   THROW_ASSERT(tok_iter == first_line_tokens.end(), "Wrong fsm description: unexpetcted tokens"+*tok_iter);

   it++;
   std::vector<std::string>::const_iterator first = it;
   for(; it+1 != it_end; it++)
   {
      tokenizer tokens(*it, sep);
      list_of_states.push_back(convert_to_identifier(writer.get(), *tokens.begin()));
   }
   std::vector<std::string>::const_iterator end = it;
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Number of states: " << list_of_states.size());
   //std::cout << list_of_states.size() << " " << bitnumber(list_of_states.size()-1) << std::endl;
   THROW_ASSERT(reset_state==*(list_of_states.begin()), "reset state and first state has to be the same");

   ///write state declaration.
   writer->write_state_declaration(os, cir, list_of_states, reset_port, reset_state);

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "write module_instantiation begin");
   //write module_instantiation begin
   writer->write_module_definition_begin(os, cir);

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "write the present_state update");
   ///write the present_state update
   writer->write_present_state_update(os, reset_state, reset_port, clock_port, Param->getOption<std::string>(OPT_sync_reset));

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "write transition and output functions");
   ///write transition and output functions
   writer->write_transition_output_functions(os, cir, reset_state, reset_port, start_port, clock_port, first, end);

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "FSM writing completed!");
}

std::string HDL_manager::convert_to_identifier(const language_writer* writer, const std::string &id)
{
   if ((id[0] >= '0' && id[0] <= '9') || (id.find(".") != std::string::npos) || (id.find("[") != std::string::npos) || (id.find("]") != std::string::npos))
      return "\\" + id + " ";
   else if(writer and writer->check_keyword(id))
      return "\\" + id + " ";
   else if(id == "array")
      return id+"_S";
   else
      return id;
}

std::string HDL_manager::convert_to_identifier(const std::string & id)
{
   const language_writer* lan = nullptr;
   return convert_to_identifier(lan, id);
}

std::string HDL_manager::get_HDL_files() const
{
   std::string res = "";
   for(std::list<std::string>::const_iterator f = HDL_files.begin(); f != HDL_files.end(); f++)
   {
      if (res.size())
         res += ";";
      res += *f;
   }
   return res;
}

std::string HDL_manager::get_aux_files() const
{
   std::string res = "";
#if HAVE_FLOPOCO
   std::unordered_set<std::string> flopoco_files = flopo_wrap->get_files_written();
   std::unordered_set<std::string>::const_iterator it_end = flopoco_files.end();
   std::unordered_set<std::string>::const_iterator it = flopoco_files.begin();
   if(it != it_end)
   {
      res += *it;
      ++it;
      while(it != it_end)
      {
        res += ";"+*it;
        ++it;
      }
   }
#endif
   for(std::list<std::string>::const_iterator f = HDL_aux_files.begin(); f != HDL_aux_files.end(); f++)
   {
      if (res.size())
         res += ";";
      res += *f;
   }
   return res;
}

std::string HDL_manager::get_mod_typename(const language_writer* lan, const structural_objectRef& cir)
{
   std::string res = GET_TYPE_NAME(cir);
   module * mod = GetPointer<module>(cir);
   const NP_functionalityRef &np = mod->get_NP_functionality();
   if (np && np->get_NP_functionality(NP_functionality::FLOPOCO_PROVIDED) != "")
   {
      long long int mod_size_in = 0;
      for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
      {
         // Size of module is size of the largest output
         if (mod_size_in < STD_GET_SIZE(mod->get_in_port(i)->get_typeRef()))
            mod_size_in = STD_GET_SIZE(mod->get_in_port(i)->get_typeRef());
      }
      res = res + "_" + STR(mod_size_in);
      long long int mod_size_out = 0;
      for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
      {
         // Size of module is size of the largest output
         if (mod_size_out < STD_GET_SIZE(mod->get_out_port(i)->get_typeRef()))
            mod_size_out = STD_GET_SIZE(mod->get_out_port(i)->get_typeRef());
      }
      res = res + "_" + STR(mod_size_out);
      if(mod->is_parameter(PIPE_PARAMETER) && mod->get_parameter(PIPE_PARAMETER) != "")
         res = res + "_" + mod->get_parameter(PIPE_PARAMETER);
   }
   return convert_to_identifier(lan, res);
}
