/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file language_writer.cpp
 * @brief This classes starting from a structural representation write different HDL based descriptions (VHDL, Verilog, SystemC).
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"

#include "language_writer.hpp"

#include "sv_writer.hpp"
#include "verilog_writer.hpp"
#include "VHDL_writer.hpp"
#if HAVE_EXPERIMENTAL
#include "SystemC_writer.hpp"
#include "blif_writer.hpp"
#include "edif_writer.hpp"
#endif

#include "exceptions.hpp"

language_writer::language_writer(char open_char, char close_char, int _debug_level) :
   PP(open_char, close_char, 2),
   debug_level(_debug_level)
{

}

language_writer::~language_writer()
{

}

unsigned int language_writer::bitnumber(unsigned int n)
{
   unsigned int count=0;
   while (n)
   {
      count++;
      n >>= 1;
   }
   if (count == 0) return 1;
   return count;
}

language_writerRef language_writer::create_writer(language language_w, int debug_level)
{
   switch (language_w)
   {
      case VERILOG:
         return language_writerRef(new verilog_writer(debug_level));
         break;
      case SYSTEM_VERILOG:
         return language_writerRef(new system_verilog_writer(debug_level));
         break;
      case VHDL:
         return language_writerRef(new VHDL_writer(debug_level));
         break;
#if HAVE_EXPERIMENTAL
      case SYSTEMC:
         return language_writerRef(new SystemC_writer(debug_level));
         break;
      case BLIF:
         return language_writerRef(new blif_writer(debug_level));
         break;
      case EDIF:
         return language_writerRef(new edif_writer(debug_level));
         break;
#endif
      default:
         THROW_ERROR("HDL backend language not supported");
   }
   return language_writerRef();
}

void language_writer::write(std::ostream& os, const std::string &rawString)
{
   PP(os, rawString);
}

void language_writer::write_header (std::ostream&)
{

}

void language_writer::write_timing_specification(std::ostream&, const technology_managerConstRef, const structural_objectRef&)
{

}

bool language_writer::check_structural_object_compliance(const structural_objectRef &) const
{
   ///to be completed
   return true;
}

