/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file VHDL_writer.cpp
 * @brief This class implements the methods to write VHDL descriptions.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_FROM_C_BUILT.hpp"

#include "VHDL_writer.hpp"

#include "HDL_manager.hpp"

#include "structural_objects.hpp"
#include "exceptions.hpp"
#include "dbgPrintHelper.hpp"
#include "NP_functionality.hpp"
#include "technology_builtin.hpp"

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <functional>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>

VHDL_writer::VHDL_writer(int _debug_level) :
   language_writer(STD_OPENING_CHAR, STD_OPENING_CHAR, _debug_level)
{

}

VHDL_writer::~VHDL_writer()
{

}

void VHDL_writer::write_comment(std::ostream& os, const std::string &comment_string)
{
   PP(os, "-- " + comment_string);
}

std::string VHDL_writer::type_converter(structural_type_descriptorRef Type)
{
   switch (Type->type)
   {
      case structural_type_descriptor::BOOL:
         {
            return "std_logic";
            break;
         }
      case structural_type_descriptor::INT:
         {
            return "signed";
            break;
         }
      case structural_type_descriptor::UINT:
         {
            return "std_logic_vector";
            break;
         }
      case structural_type_descriptor::REAL:
         {
            return "std_logic_vector";
            break;
         }
      case structural_type_descriptor::USER:
         {
            THROW_ERROR("USER type not yet supported");
            break;
         }
      case structural_type_descriptor::VECTOR_BOOL:
         {
            return "std_logic_vector";
            break;
         }
      case structural_type_descriptor::VECTOR_INT:
         {
            return "signed";
            break;
         }
      case structural_type_descriptor::VECTOR_UINT:
         {
            return "unsigned";
            break;
         }
      case structural_type_descriptor::VECTOR_REAL:
         {
            return "real";
            break;
         }
      case structural_type_descriptor::VECTOR_USER:
         {
            THROW_ERROR("VECTOR_USER type not yet supported");
            break;
         }
      case structural_type_descriptor::OTHER:
         {
            return Type->id_type;
            break;
         }
      case structural_type_descriptor::UNKNOWN:
      default:
         THROW_UNREACHABLE("");
   }
   return "";
}

std::string VHDL_writer::type_converter_size(const structural_objectRef &cir)
{
   structural_type_descriptorRef Type = cir->get_typeRef();
   const structural_objectRef Owner = cir->get_owner();
   module * mod = GetPointer<module>(Owner);
   std::string port_name = cir->get_id();
   bool specialization_string=false;
   //std::cerr << "cir: " << cir->get_id() << " " << GetPointer<port_o>(cir)->size_parameter << std::endl;
   if(mod)
   {
      const NP_functionalityRef &np = mod->get_NP_functionality();
      if (np)
      {
         std::vector<std::pair<std::string, structural_objectRef> > parameters;
         mod->get_NP_library_parameters(Owner, parameters);
         std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it_end = parameters.end();
         std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it = parameters.begin();
         for(;it!= it_end; ++it)
            if(port_name==it->first)
               specialization_string=true;
      }
   }
   switch (Type->type)
   {
      case structural_type_descriptor::BOOL:
         {
            return "";
         }
      case structural_type_descriptor::INT:
      case structural_type_descriptor::UINT:
      case structural_type_descriptor::REAL:
         {
            if (!GetPointer<port_o>(cir) or !GetPointer<port_o>(cir)->get_reverse())
            {
               if (GetPointer<port_o>(cir) && GetPointer<port_o>(cir)->size_parameter.size())
                  return " (" + (GetPointer<port_o>(cir)->size_parameter) + "-1 downto 0)";
               else if(specialization_string)
                  return " (" + (BITSIZE_PREFIX+port_name) + "-1 downto 0)";
               else
                  return " (" + boost::lexical_cast<std::string>(Type->size - 1) + " downto 0)";
            }
            else
            {
               if (GetPointer<port_o>(cir) && GetPointer<port_o>(cir)->size_parameter.size())
                  return " (0 to " + (GetPointer<port_o>(cir)->size_parameter) + "-1)";
               else if(specialization_string)
                  return " (0 to " + (BITSIZE_PREFIX+port_name) + "-1)";
               else
                  return " (0 to " + boost::lexical_cast<std::string>(Type->size - 1) + ")";
            }
         }
      case structural_type_descriptor::USER:
         {
            THROW_ERROR("USER type not yet supported");
            break;
         }
      case structural_type_descriptor::VECTOR_BOOL:
      case structural_type_descriptor::VECTOR_INT:
      case structural_type_descriptor::VECTOR_UINT:
      case structural_type_descriptor::VECTOR_REAL:
         {
            if (!GetPointer<port_o>(cir) or !GetPointer<port_o>(cir)->get_reverse())
            {
               if (GetPointer<port_o>(cir) && GetPointer<port_o>(cir)->size_parameter.size())
                  return " (" + (GetPointer<port_o>(cir)->size_parameter) + "-1 downto 0)";
               else if(specialization_string)
                  return " (" + (BITSIZE_PREFIX+port_name) + "-1 downto 0)";
               else
                  return " (" + boost::lexical_cast<std::string>(Type->vector_size - 1) + " downto 0)";
            }
            else
            {
               if (GetPointer<port_o>(cir) && GetPointer<port_o>(cir)->size_parameter.size())
                  return " (0 to " + (GetPointer<port_o>(cir)->size_parameter) + "-1)";
               else if(specialization_string)
                  return " (0 to " + (BITSIZE_PREFIX+port_name) + "-1)";
               else
                  return " (0 to " + boost::lexical_cast<std::string>(Type->vector_size - 1) + ")";
            }
         }
      case structural_type_descriptor::VECTOR_USER:
         {
            THROW_ERROR("VECTOR_USER type not yet supported");
            break;
         }
      case structural_type_descriptor::OTHER:
         {
            return Type->id_type;
            break;
         }
      case structural_type_descriptor::UNKNOWN:
      default:
         THROW_ERROR("Not initialized type");
   }
   return "";
}

void VHDL_writer::write_library_declaration(std::ostream& os, const structural_objectRef & cir)
{
   THROW_ASSERT(cir->get_kind() == component_o_K || cir->get_kind() == channel_o_K, "Expected a component or a channel got something of different");
   NP_functionalityRef NPF = GetPointer<module>(cir)->get_NP_functionality();
   if (!NPF or !NPF->exist_NP_functionality(NP_functionality::IP_LIBRARY))
   {
      PP(os, "library IEEE;\n");
      PP(os, "use IEEE.std_logic_1164.all;\n");
      PP(os, "use IEEE.numeric_std.all;\n");
      return;
   }
   std::string library = NPF->get_NP_functionality(NP_functionality::IP_LIBRARY);
   std::vector<std::string> library_list = convert_string_to_vector<std::string>(library, ";");
   for(unsigned int l = 0; l < library_list.size(); l++)
   {
      PP(os, library_list[l]+";\n");
   }
}

void VHDL_writer::write_module_declaration(std::ostream& os, const structural_objectRef &cir)
{
   THROW_ASSERT(cir->get_kind() == component_o_K || cir->get_kind() == channel_o_K, "Expected a component or a channel got something of different");
   PP(os, "entity " + HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)) + " is \n");
   list_of_comp_already_def.clear();
}

void VHDL_writer::write_module_internal_declaration(std::ostream& os, const structural_objectRef &cir)
{
   THROW_ASSERT(cir->get_kind() == component_o_K || cir->get_kind() == channel_o_K, "Expected a component or a channel got something of different");
   PP(os, "end " + HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)) + ";\n");
   PP(os, "\narchitecture " + HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)) + "_arch of ");
   PP(os, HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)) + " is\n");
   PP.indent();
}

void VHDL_writer::write_port_declaration(std::ostream& os, const structural_objectRef &cir, bool last_port_to_analyze)
{
   THROW_ASSERT(cir->get_kind() == port_o_K || cir->get_kind() == port_vector_o_K , "Expected a port got something of different " + cir->get_id());
   port_o::port_direction dir;
   dir = GetPointer<port_o>(cir)->get_port_direction();
      
   PP(os, HDL_manager::convert_to_identifier(this, cir->get_id()) + " : ");
   switch (dir)
   {
      case port_o::IN:
         {
            PP(os, "in");
            break;
         }
      case port_o::OUT:
         {
            PP(os, "out");
            break;
         }
      case port_o::IO:
         {
            PP(os, "inout");
            break;
         }
      case port_o::GEN:
      case port_o::TLM_IN:
      case port_o::TLM_INOUT:
      case port_o::TLM_OUT:
      case port_o::UNKNOWN:
      default:
         THROW_ERROR("Something went wrong!");
   }
   PP(os, " " + type_converter(cir->get_typeRef()) + type_converter_size(cir));
   if (GET_TYPE_SIZE(cir) > 1)
      PP(os, " := (others => '0')");
   else
      PP(os, " := '0'");


   if (cir->get_kind() != port_o_K && cir->get_typeRef()->type != structural_type_descriptor::VECTOR_BOOL)
      PP(os, "(" + boost::lexical_cast<std::string>(GetPointer<port_o>(cir)->get_ports_size() - 1) + " downto 0)");
   if (!last_port_to_analyze)
      PP(os, ";\n");
}

void VHDL_writer::write_component_declaration(std::ostream& os, const structural_objectRef &cir)
{
   module * mod = GetPointer<module>(cir);
   THROW_ASSERT(mod, "Expected a module got something of different");

   const NP_functionalityRef &np = mod->get_NP_functionality();
   std::string comp;
   if (np && np->get_NP_functionality(NP_functionality::FLOPOCO_PROVIDED) != "")
   {
      long long int mod_size = 0;
      for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
      {
         // Size of module is size of the largest output
         if (mod_size < STD_GET_SIZE(mod->get_out_port(i)->get_typeRef()))
            mod_size = STD_GET_SIZE(mod->get_out_port(i)->get_typeRef());
      }
      comp = HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)) + "_" + STR(mod_size);
   }
   else  
      comp = HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir));
   
   if (list_of_comp_already_def.find(comp) == list_of_comp_already_def.end())
      list_of_comp_already_def.insert(comp);
   else
      return ;
   
   PP(os, "\ncomponent " + comp + "\n");
   write_module_parametrization_decl(os, cir);
   PP(os,"port (\n");
   
   PP.indent();
   if (mod->get_in_port_size())
   {
      write_comment(os, "IN\n");
      for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
      {
         if (i == mod->get_in_port_size()-1 && !mod->get_out_port_size() && !mod->get_in_out_port_size() && !mod->get_gen_port_size())
            write_port_declaration(os, mod->get_in_port(i), true);
         else
            write_port_declaration(os, mod->get_in_port(i), false);
      }
   }
   if (mod->get_out_port_size())
   {
      write_comment(os, "OUT\n");
      for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
      {
         if (i == mod->get_out_port_size()-1 && !mod->get_in_out_port_size() && !mod->get_gen_port_size())
            write_port_declaration(os, mod->get_out_port(i), true);
         else
            write_port_declaration(os, mod->get_out_port(i), false);
      }
   }
   if (mod->get_in_out_port_size())
   {
      write_comment(os, "INOUT\n");
      for (unsigned int i = 0; i < mod->get_in_out_port_size(); i++)
      {
         if (i == mod->get_in_out_port_size()-1 && !mod->get_gen_port_size())
            write_port_declaration(os, mod->get_in_out_port(i), true);
         else
            write_port_declaration(os, mod->get_in_out_port(i), false);
      }
   }
   if (mod->get_gen_port_size())
   {
      write_comment(os, "Ports\n");
      for (unsigned int i = 0; i < mod->get_gen_port_size(); i++)
      {
         if (i == mod->get_gen_port_size()-1)
            write_port_declaration(os, mod->get_gen_port(i), true);
         else
            write_port_declaration(os, mod->get_gen_port(i), false);
      }
   }
   PP.deindent();

   PP(os, "\n);\nend component;\n");
}

void VHDL_writer::write_signal_declaration(std::ostream& os, const structural_objectRef &cir)
{
   THROW_ASSERT(cir->get_kind() == signal_o_K, "Expected a component or a channel got something of different");
   PP(os, "signal " + HDL_manager::convert_to_identifier(this, cir->get_id()) + " : " +  type_converter(cir->get_typeRef()) + type_converter_size(cir) + ";\n");
}

void VHDL_writer::write_module_definition_begin(std::ostream& os, const structural_objectRef &)
{
   PP.deindent();
   PP(os, "begin\n");
   PP.indent();
}


void VHDL_writer::write_module_instance_begin(std::ostream& os, const structural_objectRef &cir, bool write_parametrization)
{
   THROW_ASSERT(cir->get_kind() == component_o_K || cir->get_kind() == channel_o_K, "Expected a component or a channel got something of different");
   module * mod = GetPointer<module>(cir);
   const NP_functionalityRef &np = mod->get_NP_functionality();
   if (np && np->get_NP_functionality(NP_functionality::FLOPOCO_PROVIDED) != "")
   {
      long long int mod_size = 0;
      for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
      {
         // Size of module is size of the largest output
         if (mod_size < STD_GET_SIZE(mod->get_out_port(i)->get_typeRef()))
            mod_size = STD_GET_SIZE(mod->get_out_port(i)->get_typeRef());
      }
      PP(os, HDL_manager::convert_to_identifier(this, cir->get_id()) + " : " + HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)) + "_" + STR(mod_size));
   }
   else  
      PP(os, HDL_manager::convert_to_identifier(this, cir->get_id()) + " : " + HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)));
   //check possible module parametrization
   if(write_parametrization)
      write_module_parametrization(os, cir);
   PP(os, " port map (");
}

void VHDL_writer::write_module_instance_end(std::ostream& os, const structural_objectRef &)
{
   PP(os, ");\n");
}

void VHDL_writer::write_module_definition_end(std::ostream& os, const structural_objectRef &cir)
{
   THROW_ASSERT(cir->get_kind() == component_o_K || cir->get_kind() == channel_o_K, "Expected a component or a channel got something of different");
   PP.deindent();
   PP(os, "\nend " + HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)) + "_arch;\n\n");
}

void VHDL_writer::write_vector_port_binding(std::ostream& os, const structural_objectRef &port, bool& first_port_analyzed)
{
   port_o * pv = GetPointer<port_o>(port);
   unsigned int n_ports = pv->get_ports_size();
   unsigned int index;
   for (unsigned int j =  0; j < n_ports; ++j)
   {
      index = n_ports - j - 1;
      structural_objectRef object_bounded = GetPointer<port_o>(pv->get_port(index))->find_bounded_object();
      if (!object_bounded) continue;

      write_port_binding(os, pv->get_port(index), object_bounded, first_port_analyzed);
   }
}

void VHDL_writer::write_port_binding(std::ostream& os, const structural_objectRef &port, const structural_objectRef &object_bounded, bool& first_port_analyzed)
{
   THROW_ASSERT(port, "NULL object_bounded received");
   THROW_ASSERT(port->get_kind() == port_o_K, "Expected a port got something of different");
   THROW_ASSERT(port->get_owner(), "The port has to have an owner");
   if (first_port_analyzed)
      PP(os, ", ");
   if (port->get_owner()->get_kind() == port_vector_o_K)
      PP(os, HDL_manager::convert_to_identifier(this, port->get_owner()->get_id()) + "(" + port->get_id() + ") => ");
   else
      PP(os, HDL_manager::convert_to_identifier(this, port->get_id()) + " => ");

   first_port_analyzed = true;
   if (!object_bounded and GetPointer<port_o>(port)->get_port_direction() == port_o::IN)
   {
      long long int size = GET_TYPE_SIZE(port);
      if (port->get_typeRef()->type == structural_type_descriptor::BOOL)
      {
         PP(os, "'0'");
      }
      else
      {
         std::string null;
         for(unsigned int s = 0; s < size; s++) null += "0";
         PP(os, "\"" + null + "\"");
      }
      return;
   }
   THROW_ASSERT(object_bounded, "NULL object_bounded received");
   THROW_ASSERT(object_bounded->get_kind() != port_o_K || object_bounded->get_owner(), "A port has to have always an owner");
   if (object_bounded->get_kind() == port_o_K && object_bounded->get_owner()->get_kind() == port_vector_o_K)
      PP(os, HDL_manager::convert_to_identifier(this, object_bounded->get_owner()->get_id()) + "(" + object_bounded->get_id() + ")");
   else
      PP(os, HDL_manager::convert_to_identifier(this, object_bounded->get_id()));
   first_port_analyzed = true;
}

void VHDL_writer::write_io_signal_post_fix(std::ostream& os, const structural_objectRef &port, const structural_objectRef &sig)
{
   THROW_ASSERT(port && port->get_kind() == port_o_K, "Expected a port got something of different");
   THROW_ASSERT(port->get_owner(), "Expected a port with an owner");
   THROW_ASSERT(sig && sig->get_kind() == signal_o_K, "Expected a signal got something of different");
   std::string port_string, signal_string = HDL_manager::convert_to_identifier(this, sig->get_id());
   if (port->get_owner()->get_kind() == port_vector_o_K)
      port_string = HDL_manager::convert_to_identifier(this, port->get_owner()->get_id()) + "(" + port->get_id() + ")";
   else
      port_string = HDL_manager::convert_to_identifier(this, port->get_id());
   if (GetPointer<port_o>(port)->get_port_direction() == port_o::IN)
      std::swap(port_string, signal_string);
   PP(os, port_string + " <= " + signal_string + ";\n");
}

void VHDL_writer::write_module_parametrization(std::ostream& os, const structural_objectRef &cir)
{
   THROW_ASSERT(cir->get_kind() == component_o_K || cir->get_kind() == channel_o_K, "Expected a component or a channel got something of different");
   module * mod = GetPointer<module>(cir);
   const NP_functionalityRef &np = mod->get_NP_functionality();
   if (np)
   {
      std::vector<std::pair<std::string, structural_objectRef> > parameters;
      mod->get_NP_library_parameters(cir, parameters);
      std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it_end = parameters.end();
      std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it = parameters.begin();
      bool first_it = true;
      if(it!=it_end)
         PP(os, " generic map(");

      for(;it!= it_end; ++it)
      {
         if(first_it)
         {
            first_it = false;
         }
         else
            PP(os, ", ");
         const std::string &name = it->first;
         structural_objectRef obj = it->second;
         if(obj)
            PP(os, BITSIZE_PREFIX+name + "=>" + boost::lexical_cast<std::string>(GET_TYPE_SIZE(obj)));
         else
            PP(os, name + "=>" + mod->get_parameter(name));
      }
      if(!first_it)
         PP(os, ")");
   }
}

void VHDL_writer::write_tail(std::ostream&, const structural_objectRef &)
{

}

void VHDL_writer::write_state_declaration(std::ostream& os, const structural_objectRef &, const std::list<std::string> &list_of_states, const std::string &, const std::string &)
{
   std::list<std::string>::const_iterator it_end = list_of_states.end();
   size_t n_states = list_of_states.size();
   unsigned int count = 0;

   write_comment(os, "define the states of FSM model\n");
   PP(os, "type state_type is (");
   for(std::list<std::string>::const_iterator it = list_of_states.begin(); it != it_end; it++)
   {
      PP(os, *it);
      count++;
      if(count != n_states)
         PP(os, ", ");
   }
   PP(os, ");\n");
   PP(os, "signal present_state, next_state : state_type;\n");
}

void VHDL_writer::write_present_state_update(std::ostream& os, const std::string &reset_state, const std::string &reset_port, const std::string &clock_port, const std::string &reset_type)
{
   write_comment(os, "concurrent process#1: state registers\n");
   if(reset_type == "no" || reset_type == "sync")
   {
      PP(os, "state_reg: process(" + clock_port + ", " + reset_port + ")\n");
      PP(os, "begin\n");
      PP.indent();
      PP(os,   "if (" + clock_port + "'event and " + clock_port + "='1') then\n");
      PP.indent();
      PP(os,   "if (" + reset_port + "='1') then\n");
      PP.indent();
      PP(os,     "present_state <= "+reset_state+";\n");
      PP.deindent();
      PP(os,   "else\n");
      PP.indent();
      PP(os,     "present_state <= next_state;\n");
      PP.deindent();
      PP(os,   "end if;\n");
   }
   else
   {
      PP(os, "state_reg: process(" + clock_port + ", " + reset_port + ")\n");
      PP(os, "begin\n");
      PP.indent();
      PP(os,   "if (" + reset_port + "='1') then\n");
      PP.indent();
      PP(os,     "present_state <= "+reset_state+";\n");
      PP.deindent();
      PP(os,   "elsif (" + clock_port + "'event and " + clock_port + "='1') then\n");
      PP.indent();
      PP(os,     "present_state <= next_state;\n");
   }
   PP.deindent();
   PP(os,   "end if;\n");
   PP.deindent();
   PP(os, "end process;\n");
}

void VHDL_writer::write_transition_output_functions(std::ostream& os, const structural_objectRef &cir, const std::string &reset_state, const std::string &reset_port, const std::string &, const std::string &clock_port, std::vector<std::string>::const_iterator &first, std::vector<std::string>::const_iterator &end)
{
   module * mod = GetPointer<module>(cir);
   boost::char_separator<char> state_sep(":", nullptr);
   boost::char_separator<char> sep(" ", nullptr);
   typedef boost::tokenizer<boost::char_separator<char> > tokenizer;


   ///compute the default output
   tokenizer tokens_first(*first, sep);
   tokenizer::const_iterator it = tokens_first.begin();
   ///get the default output of the reset state
   it++;
   std::string default_output = *it;

   write_comment(os, "concurrent process#2: combinational logic\n");
   PP(os, "comb_logic: process(present_state");
   for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
   {
      std::string port_name = HDL_manager::convert_to_identifier(this, mod->get_in_port(i)->get_id());
      if (port_name != clock_port and port_name != reset_port)
      {
         PP(os, ", " +port_name);
      }
   }
   PP(os, ")\n");

   PP(os, "begin\n");
   PP.indent();

   /// set the defaults
   for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
   {
      std::string port_name = HDL_manager::convert_to_identifier(this, mod->get_out_port(i)->get_id());
      if(default_output[i] != '-')
         PP(os, port_name + " <= '"+default_output[i]+"';\n");
   }

   PP(os, "next_state <= " + reset_state + ";\n");

   PP(os,   "case present_state is\n");
   PP.indent();

   for(std::vector<std::string>::const_iterator first_it = first; first_it != end; first_it++)
   {
      tokenizer state_tokens_first(*first_it, state_sep);

      tokenizer::const_iterator its = state_tokens_first.begin();

      std::string state_description = *its;
      its++;

      std::vector<std::string> state_transitions;
      for(; its != state_tokens_first.end(); its++)
         state_transitions.push_back(*its);

      tokenizer tokens_curr(state_description, sep);
      ///get the present state
      its = tokens_curr.begin();
      std::string present_state = HDL_manager::convert_to_identifier(this, *its);
      ///get the current output
      its++;
      std::string current_output = *its;

      PP(os, "when " + present_state + " =>\n");

      //PP(os, "begin");
      PP.indent();
      for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
      {
         std::string port_name = HDL_manager::convert_to_identifier(this, mod->get_out_port(i)->get_id());
         if(default_output[i] != current_output[i] and current_output[i] != '-')
            PP(os, port_name + " <= '"+current_output[i]+"';\n");
      }

      bool unique_transition = (state_transitions.size() == 1);

      for(unsigned int i = 0; i < state_transitions.size(); i++)
      {
         tokenizer transition_tokens(state_transitions[i], sep);
         tokenizer::const_iterator itt = transition_tokens.begin();

         std::string current_input;
         if(mod->get_in_port_size() - 3) // clock and reset are always present
         {
            current_input = *itt;
            itt++;
         }
         else
            current_input = "";
         std::string next_state = *itt;
         ++itt;
         std::string transition_outputs = *itt;
         ++itt;
         THROW_ASSERT(itt == transition_tokens.end(), "Bad transition format");

         if (!unique_transition)
         {
            if (i == 0)
            {
               PP(os, "if (");
            }
            else if ((i + 1) == state_transitions.size())
            {
               PP(os, "else\n");
            }
            else
            {
               PP(os, "elsif (");
            }
            if ((i + 1) < state_transitions.size())
            {
               unsigned int position1 = 0;
               bool first_test = true;
               for (unsigned int i1 = 0; i1 < mod->get_in_port_size(); i1++)
               {
                  std::string port_name = HDL_manager::convert_to_identifier(this, mod->get_in_port(i1)->get_id());
                  if(port_name != reset_port && port_name != clock_port)
                  {
                     if(current_input[position1] != '-')
                     {
                        if(!first_test)
                           PP(os, " and ");
                        else
                           first_test=false;
                        PP(os, port_name);
                        if(current_input[position1] == '1')
                           PP(os, "='1'");
                        else
                           PP(os, "='0'");
                     }
                     position1++;
                  }
               }
               PP(os, ") then\n");
            }
            PP.indent();
         }
         PP(os, "next_state <= " + next_state + ";\n");
         for (unsigned int i2 = 0; i2 < mod->get_out_port_size(); i2++)
         {
            if (transition_outputs[i2] != '-')
            {
               std::string port_name = HDL_manager::convert_to_identifier(this, mod->get_out_port(i2)->get_id());
               PP(os, port_name + " <= '" + transition_outputs[i2] + "';\n");
            }
         }
         PP.deindent();
      }
      if (!unique_transition)
      {
         PP(os, "end if;\n");
         PP.deindent();
      }
   }

   PP.deindent();
   PP(os,   "end case;\n");
   PP.deindent();
   PP(os, "end process;\n");
}


void VHDL_writer::write_assign(std::ostream&, const std::string&, const std::string&)
{
   THROW_ERROR("Not yet implemented");
}

void VHDL_writer::write_NP_functionalities(std::ostream& os, const structural_objectRef &cir)
{
   module * mod = GetPointer<module>(cir);
   THROW_ASSERT(mod, "Expected a component object");
   const NP_functionalityRef &np = mod->get_NP_functionality();
   THROW_ASSERT(np, "NP Behavioral description is missing for module: "+HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)));
   std::string beh_desc = np->get_NP_functionality(NP_functionality::VHDL_PROVIDED);
   THROW_ASSERT(beh_desc != "", "VHDL behavioral description is missing for module: " + HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)));
   remove_escaped(beh_desc);
   PP(os, beh_desc);
}

void VHDL_writer::write_port_decl_header(std::ostream& os)
{
   PP(os, "port (\n");
   PP.indent();
}

void VHDL_writer::write_port_decl_tail(std::ostream& os)
{
   PP.deindent();
   PP(os, "\n);\n");
}

void VHDL_writer::write_module_parametrization_decl(std::ostream& os, const structural_objectRef &cir)
{
   THROW_ASSERT(cir->get_kind() == component_o_K || cir->get_kind() == channel_o_K, "Expected a component or a channel got something of different");
   module * mod = GetPointer<module>(cir);
   const NP_functionalityRef &np = mod->get_NP_functionality();
   if (np)
   {
      std::vector<std::pair<std::string, structural_objectRef> > parameters;
      mod->get_NP_library_parameters(cir, parameters);
      std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it_end = parameters.end();
      std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it = parameters.begin();
      bool first_it = true;
      if(it!=it_end)
         PP(os, "generic(");

      for(;it!= it_end; ++it)
      {
         if(first_it)
         {
            first_it = false;
         }
         else
            PP(os, ";");
         PP(os, "\n");
         const std::string &name = it->first;
         structural_objectRef obj = it->second;
         if(obj)
            PP(os, std::string(" ") + BITSIZE_PREFIX+name + ": integer := " + boost::lexical_cast<std::string>(GET_TYPE_SIZE(obj)));
         else
         {
            std::string par_value = mod->get_parameter(name);
            if(par_value[0] =='\"')
            {
               PP(os, name + " : std_logic_vector(" + boost::lexical_cast<std::string>(par_value.length()-3) + " downto 0) := "+par_value);
            }
            else
               PP(os, name + " : integer := "+par_value);
         }
      }
      ///in case first_it is false at least one parameter has used.
      if(!first_it)
         PP(os, ");\n");
   }
}
