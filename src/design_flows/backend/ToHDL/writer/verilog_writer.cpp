/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file verilog_writer.cpp
 * @brief This class implements the methods to write Verilog descriptions.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_FROM_C_BUILT.hpp"

#include "verilog_writer.hpp"

#include "HDL_manager.hpp"

#include "technology_manager.hpp"
#include "time_model.hpp"

#include "structural_objects.hpp"
#include "exceptions.hpp"
#include "dbgPrintHelper.hpp"
#include "NP_functionality.hpp"
#include "technology_builtin.hpp"
#include "tree_helper.hpp"
#include "structural_objects.hpp"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <functional>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>

#include "state_transition_graph_manager.hpp"

void verilog_writer::write_comment(std::ostream& os, const std::string &comment_string)
{
   PP(os, "// " + comment_string);
}

std::string verilog_writer::type_converter(structural_type_descriptorRef Type)
{
   switch (Type->type)
   {
      case structural_type_descriptor::BOOL:
      {
         return "";
      }
      case structural_type_descriptor::INT:
      {
         return "signed ";
      }
      case structural_type_descriptor::UINT:
      {
         return "";
      }
      case structural_type_descriptor::USER:
      {
         THROW_ERROR("USER type not yet supported");
         break;
      }
      case structural_type_descriptor::REAL:
      {
         return "";
      }
      case structural_type_descriptor::VECTOR_BOOL:
      {
         return "";
         break;
      }
      case structural_type_descriptor::VECTOR_INT:
      {
         return "signed ";
         break;
      }
      case structural_type_descriptor::VECTOR_UINT:
      {
         return "";
         break;
      }
      case structural_type_descriptor::VECTOR_REAL:
      {
         break;
      }
      case structural_type_descriptor::VECTOR_USER:
      {
         THROW_ERROR("VECTOR_USER type not yet supported");
         break;
      }
      case structural_type_descriptor::OTHER:
      {
         return Type->id_type;
         break;
      }
      case structural_type_descriptor::UNKNOWN:
      default:
         THROW_ERROR("Not initialized type");
   }
   return "";
}

std::string verilog_writer::type_converter_size(const structural_objectRef &cir)
{
   structural_type_descriptorRef Type = cir->get_typeRef();
   const structural_objectRef Owner = cir->get_owner();
   module * mod = GetPointer<module>(Owner);
   std::string port_name = cir->get_id();
   bool specialization_string=false;
   if(mod)
   {
      const NP_functionalityRef &np = mod->get_NP_functionality();
      if (np)
      {
         std::vector<std::pair<std::string, structural_objectRef> > parameters;
         mod->get_NP_library_parameters(Owner, parameters);
         std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it_end = parameters.end();
         std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it = parameters.begin();
         for(;it!= it_end; ++it)
            if(port_name==it->first)
               specialization_string=true;
      }
   }
   switch (Type->type)
   {
      case structural_type_descriptor::BOOL:
      {
         if (cir->get_kind() == port_vector_o_K)
         {
            if(specialization_string)
               return "[" + (PORTSIZE_PREFIX+port_name) + "-1:0] ";
            else
               return "[" + boost::lexical_cast<std::string>(GetPointer<port_o>(cir)->get_ports_size() - 1) + ":0] ";
         }
         else
            return "";
      }
      case structural_type_descriptor::USER:
      {
         Type->print(std::cerr);
         THROW_ERROR("USER type not yet supported");
         break;
      }
      case structural_type_descriptor::INT:
      case structural_type_descriptor::UINT:
      case structural_type_descriptor::REAL:
      case structural_type_descriptor::VECTOR_BOOL:
      {
         if(specialization_string)
         {
            if (cir->get_kind() == port_vector_o_K)
            {
               unsigned int lsb = GetPointer<port_o>(cir)->get_lsb();
               return "[(" + (PORTSIZE_PREFIX+port_name) +"*" + (BITSIZE_PREFIX+port_name) + ")+(" + boost::lexical_cast<std::string>(static_cast<int>(lsb)-1) + "):" + boost::lexical_cast<std::string>(lsb) + "] ";
            }
            else
               return "[" + (BITSIZE_PREFIX+port_name) + "-1:0] ";
         }
         else
         {
            if (cir->get_kind() == signal_vector_o_K)
            {
               const structural_objectRef first_sig = GetPointer<signal_o>(cir)->get_signal(0);
               structural_type_descriptorRef Type_fs = first_sig->get_typeRef();
               unsigned int n_sign = GetPointer<signal_o>(cir)->get_signals_size();
               unsigned int size_fs = Type_fs->vector_size>0 ? Type_fs->size*Type_fs->vector_size : Type_fs->size;
               unsigned int lsb = GetPointer<signal_o>(cir)->get_lsb();
               unsigned int msb = size_fs*n_sign+lsb;

               return "[" + boost::lexical_cast<std::string>(static_cast<int>(msb) - 1 ) + ":" + boost::lexical_cast<std::string>(lsb) + "] ";
            }
            else if (cir->get_kind() == port_vector_o_K)
            {
               unsigned int lsb = GetPointer<port_o>(cir)->get_lsb();
               unsigned int n_ports = GetPointer<port_o>(cir)->get_ports_size();
               const structural_objectRef first_port = GetPointer<port_o>(cir)->get_port(0);
               structural_type_descriptorRef Type_fp = first_port->get_typeRef();
               unsigned int size_fp = Type_fp->vector_size>0 ? Type_fp->size*Type_fp->vector_size : Type_fp->size;
               unsigned int msb = size_fp * n_ports+lsb;
               return "[" + boost::lexical_cast<std::string>(static_cast<int>(msb) - 1 ) + ":" + boost::lexical_cast<std::string>(lsb) + "] ";
            }
            if (Type->vector_size > 1 && Type->size == 1)
               return "[" + boost::lexical_cast<std::string>(static_cast<int>(Type->vector_size) - 1) + ":0] ";
            else if (Type->vector_size == 1 && Type->size == 1)
               return "";
            else if (Type->vector_size ==  0 && Type->size != 0)
               return "[" + boost::lexical_cast<std::string>(static_cast<int>(Type->size) - 1) + ":0] ";
            else
               THROW_ERROR("Not completely specified: "+port_name);
         }
         break;
      }
      case structural_type_descriptor::VECTOR_UINT:
      case structural_type_descriptor::VECTOR_INT:
      case structural_type_descriptor::VECTOR_REAL:
      {
         if(specialization_string)
            return "[" + (BITSIZE_PREFIX+port_name) +"*"+(NUM_ELEM_PREFIX+port_name) + "-1:0] ";
         else if (Type->vector_size*Type->size > 1)
            return "[" + boost::lexical_cast<std::string>(Type->vector_size*Type->size - 1) + ":0] ";
         else
            return "";
      }
      case structural_type_descriptor::VECTOR_USER:
      {
         THROW_ERROR("VECTOR_USER type not yet supported");
         break;
      }
      case structural_type_descriptor::OTHER:
      {
         return Type->id_type;
         break;
      }
      case structural_type_descriptor::UNKNOWN:
      default:
         THROW_ERROR("Not initialized type");
   }
   return "";
}

std::string verilog_writer::may_slice_string(const structural_objectRef &cir)
{
   structural_type_descriptorRef Type = cir->get_typeRef();
   const structural_objectRef Owner = cir->get_owner();
   module * mod = GetPointer<module>(Owner);
   std::string port_name = cir->get_id();
   bool specialization_string=false;
   if(mod)
   {
      const NP_functionalityRef &np = mod->get_NP_functionality();
      if (np)
      {
         std::vector<std::pair<std::string, structural_objectRef> > parameters;
         mod->get_NP_library_parameters(Owner, parameters);
         std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it_end = parameters.end();
         std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it = parameters.begin();
         for(;it!= it_end; ++it)
            if(port_name==it->first)
               specialization_string=true;
      }
   }
   switch (Type->type)
   {
      case structural_type_descriptor::BOOL:
      {
         if(Owner->get_kind() == port_vector_o_K)
         {
               return "[" + GetPointer<port_o>(cir)->get_id() + "]";
         }
         else
            return "";
      }
      case structural_type_descriptor::USER:
      {
         Type->print(std::cerr);
         THROW_ERROR("USER type not yet supported");
         break;
      }
      case structural_type_descriptor::INT:
      case structural_type_descriptor::UINT:
      case structural_type_descriptor::REAL:
      case structural_type_descriptor::VECTOR_BOOL:
      {
         if(specialization_string)
         {
            if(Owner->get_kind() == port_vector_o_K)
            {
               unsigned int lsb = GetPointer<port_o>(Owner)->get_lsb();
               return "[((" + boost::lexical_cast<std::string>(GetPointer<port_o>(cir)->get_id()) +"+1)*" + (BITSIZE_PREFIX+port_name) + ")+(" + boost::lexical_cast<std::string>(static_cast<int>(lsb)-1) + "):("+ boost::lexical_cast<std::string>(GetPointer<port_o>(cir)->get_id()) +"*" + (BITSIZE_PREFIX+port_name)+")+" + boost::lexical_cast<std::string>(lsb) + "]";
            }
            else
               return "";
         }
         else
         {
            if (Owner->get_kind() == port_vector_o_K)
            {
               structural_type_descriptorRef Type_fp = cir->get_typeRef();
               unsigned int size_fp = Type_fp->vector_size>0 ? Type_fp->size*Type_fp->vector_size : Type_fp->size;
               unsigned int lsb = GetPointer<port_o>(Owner)->get_lsb();
               return "[" + boost::lexical_cast<std::string>((1+boost::lexical_cast<int>(GetPointer<port_o>(cir)->get_id()))*static_cast<int>(size_fp) + static_cast<int>(lsb) - 1) + ":" + boost::lexical_cast<std::string>((boost::lexical_cast<int>(GetPointer<port_o>(cir)->get_id()))*static_cast<int>(size_fp) + static_cast<int>(lsb)) + "]";
            }
            else
               return "";
         }
         break;
      }
      case structural_type_descriptor::VECTOR_UINT:
      case structural_type_descriptor::VECTOR_INT:
      case structural_type_descriptor::VECTOR_REAL:
      {
         if(specialization_string)
         {
            if(Owner->get_kind() == port_vector_o_K)
            {
               unsigned int lsb = GetPointer<port_o>(Owner)->get_lsb();
               return "[((" + boost::lexical_cast<std::string>(GetPointer<port_o>(cir)->get_id()) +"+1)*" + (BITSIZE_PREFIX+port_name) + "*" + (NUM_ELEM_PREFIX+port_name) + ")+(" + boost::lexical_cast<std::string>(static_cast<int>(lsb)-1) + "):("+ boost::lexical_cast<std::string>(GetPointer<port_o>(cir)->get_id()) +"*" + (BITSIZE_PREFIX+port_name)  + "*" + (NUM_ELEM_PREFIX+port_name) +")+" + boost::lexical_cast<std::string>(lsb) + "]";
            }
            else
               return "";
         }
         else
         {
            if (Owner->get_kind() == port_vector_o_K)
            {
               structural_type_descriptorRef Type_fp = cir->get_typeRef();
               unsigned int size_fp = Type_fp->vector_size>0 ? Type_fp->size*Type_fp->vector_size : Type_fp->size;
               unsigned int lsb = GetPointer<port_o>(Owner)->get_lsb();
               return "[" + boost::lexical_cast<std::string>((1+boost::lexical_cast<int>(GetPointer<port_o>(cir)->get_id()))*static_cast<int>(size_fp) + static_cast<int>(lsb) - 1) + ":" + boost::lexical_cast<std::string>((boost::lexical_cast<int>(GetPointer<port_o>(cir)->get_id()))*static_cast<int>(size_fp) + static_cast<int>(lsb)) + "]";
            }
            else
               return "";
         }
         break;
      }
      case structural_type_descriptor::OTHER:
      case structural_type_descriptor::VECTOR_USER:
      {
         THROW_ERROR("VECTOR_USER type not yet supported");
         break;
      }
      case structural_type_descriptor::UNKNOWN:
      default:
         THROW_ERROR("Not initialized type");
   }
   return "";
}

void verilog_writer::write_library_declaration(std::ostream& , const structural_objectRef &)
{
}

void verilog_writer::write_module_declaration(std::ostream& os, const structural_objectRef &cir)
{
   module * mod = GetPointer<module>(cir);
   THROW_ASSERT(mod, "Expected a module got something of different");
   PP(os, "`timescale 1ns / 1ps\n");
   PP(os, "module " + HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)) + "(");
   bool first_obj = false;
   /// write IO port declarations respecting the position
   for(unsigned int i = 0; i < mod->get_num_ports(); i++)
   {
      if (first_obj)
         os << ", ";
      else
         first_obj = true;
      PP(os, HDL_manager::convert_to_identifier(this, mod->get_positional_port(i)->get_id()));
   }
   PP(os, ");\n");
   PP.indent();
}

void verilog_writer::write_module_internal_declaration(std::ostream&, const structural_objectRef &)
{

}

void verilog_writer::write_assign(std::ostream& os, const std::string& op0, const std::string& op1)
{
   if (op0 != op1) PP(os, "  assign " + op0 + " = " + op1 + ";\n");
}


void verilog_writer::write_port_declaration(std::ostream& os, const structural_objectRef &cir, bool)
{
   THROW_ASSERT(cir->get_kind() == port_o_K || cir->get_kind() == port_vector_o_K , "Expected a port got something of different " + cir->get_id());
   port_o::port_direction dir;
   dir = GetPointer<port_o>(cir)->get_port_direction();
   switch (dir)
   {
      case port_o::IN:
      {
         PP(os, "input ");
         break;
      }
      case port_o::OUT:
      {
         PP(os, "output ");
         break;
      }
      case port_o::IO:
      {
         PP(os, "inout ");
         break;
      }
      case port_o::GEN:
         {
            THROW_ERROR("Generic port not yet supported!");
            break;
         }
      case port_o::TLM_IN:
      case port_o::TLM_INOUT:
      case port_o::TLM_OUT:
      case port_o::UNKNOWN:
      default:
         THROW_ERROR("Something went wrong!");
   }

   PP(os, type_converter(cir->get_typeRef()) + type_converter_size(cir));
   PP(os, HDL_manager::convert_to_identifier(this, cir->get_id()) + ";\n");
}

void verilog_writer::write_component_declaration(std::ostream&, const structural_objectRef&)
{

}

void verilog_writer::write_signal_declaration(std::ostream& os, const structural_objectRef &cir)
{
   THROW_ASSERT(cir->get_kind() == signal_o_K || cir->get_kind() == signal_vector_o_K, "Expected a signal or a signal vector got something of different");
   PP(os, "wire " + type_converter(cir->get_typeRef()) + type_converter_size(cir));
   if (cir->get_kind() == signal_vector_o_K and cir->get_typeRef()->type == structural_type_descriptor::BOOL)
   {
      const structural_objectRef first_sig = GetPointer<signal_o>(cir)->get_signal(0);
      structural_type_descriptorRef Type_fs = first_sig->get_typeRef();
      unsigned int n_sign = GetPointer<signal_o>(cir)->get_signals_size();
      unsigned int size_fs = Type_fs->vector_size>0 ? Type_fs->size*Type_fs->vector_size : Type_fs->size;
      unsigned int lsb = GetPointer<signal_o>(cir)->get_lsb();
      unsigned int msb = size_fs*n_sign+lsb;
      PP(os, "[" + boost::lexical_cast<std::string>(msb-1) + ":"+ boost::lexical_cast<std::string>(lsb)+"] ");
   }
   PP(os, HDL_manager::convert_to_identifier(this, cir->get_id()) + ";\n");
}

void verilog_writer::write_module_definition_begin(std::ostream& os, const structural_objectRef &)
{
   PP(os, "\n");
}


void verilog_writer::write_module_instance_begin(std::ostream& os, const structural_objectRef &cir, bool write_parametrization)
{
   THROW_ASSERT(cir->get_kind() == component_o_K || cir->get_kind() == channel_o_K, "Expected a component or a channel got something of different");
   if (builtin_verilog_gates::is_builtin(GET_TYPE_NAME(cir)))
      PP(os, builtin_verilog_gates::get_verilog_builtin(GET_TYPE_NAME(cir)));
   else
      PP(os, HDL_manager::get_mod_typename(this, cir));

   ///check possible module parametrization
   if (!builtin_verilog_gates::is_builtin(GET_TYPE_NAME(cir)) && write_parametrization)
      write_module_parametrization(os, cir);
   PP(os, " " + HDL_manager::convert_to_identifier(this, cir->get_id()) + " (");
}
void verilog_writer::write_module_instance_end(std::ostream& os, const structural_objectRef &)
{
   PP(os, ");\n");
}

void verilog_writer::write_module_definition_end(std::ostream& os, const structural_objectRef &)
{
   PP.deindent();
   PP(os, "\nendmodule\n\n");
}

void verilog_writer::write_vector_port_binding(std::ostream& os, const structural_objectRef &port, bool& first_port_analyzed)
{
   THROW_ASSERT(port, "NULL object_bounded received");
   THROW_ASSERT(port->get_kind() == port_vector_o_K, "Expected a port vector, got something of different");
   bool is_gate_builtin = builtin_verilog_gates::is_builtin(GET_TYPE_NAME(port->get_owner()));
   const structural_objectRef p_object_bounded = GetPointer<port_o>(port)->find_bounded_object(port->get_owner());
   if (p_object_bounded)
   {
      if (first_port_analyzed)
         PP(os, ", ");
      PP(os, ".");
      PP(os, HDL_manager::convert_to_identifier(this, port->get_id()));
      PP(os, "(");
      if (p_object_bounded->get_kind() == port_vector_o_K)
      {
         PP(os, HDL_manager::convert_to_identifier(this, p_object_bounded->get_id()));
      }
      else if (p_object_bounded->get_kind() == signal_vector_o_K)
      {
         PP(os, HDL_manager::convert_to_identifier(this, p_object_bounded->get_id()));
      }
      else
         THROW_ERROR("not expected case");
      PP(os, ")");
   }
   else
   {
      std::string port_binding;
      port_o * pv = GetPointer<port_o>(port);
      bool local_first_port_analyzed = false;
      unsigned int msb, lsb;
      msb = std::numeric_limits<unsigned int>::max();
      lsb = std::numeric_limits<unsigned int>::max();
      structural_objectRef slice;
      structural_objectRef null_object;
      unsigned int n_ports = pv->get_ports_size();
      unsigned int index;
      for (unsigned int j =  0; j < n_ports; ++j)
      {
         index = n_ports - j - 1;
         structural_objectRef object_bounded = GetPointer<port_o>(pv->get_port(index))->find_bounded_object();
         if (!object_bounded) THROW_ERROR("not bounded: " + pv->get_port(index)->get_path());

         if (object_bounded->get_owner()->get_kind() == port_vector_o_K || object_bounded->get_owner()->get_kind() == signal_vector_o_K)
         {
            unsigned int bit = boost::lexical_cast<unsigned int>(object_bounded->get_id());
            if (slice and slice->get_id() != object_bounded->get_owner()->get_id())
            {
               if (local_first_port_analyzed)
                  port_binding += ", ";
               port_binding += HDL_manager::convert_to_identifier(this, slice->get_id());
               unsigned int max=0;
               port_o *pvs = GetPointer<port_o>(slice);
               if (pvs) max = pvs->get_ports_size();
               else
               {
                  signal_o *sv = GetPointer<signal_o>(slice);
                  if (sv) max = sv->get_signals_size();
               }
               if (lsb != 0 || msb != (max - 1))
               {
                  port_binding += "[" + boost::lexical_cast<std::string>(msb);
                  if (msb != lsb)
                     port_binding += ":" + boost::lexical_cast<std::string>(lsb);
                  port_binding += "]";
               }
               local_first_port_analyzed = true;
               slice = null_object;
               msb = std::numeric_limits<unsigned int>::max();
            }
            if (!slice || (slice->get_id() == object_bounded->get_owner()->get_id() and bit == lsb - 1))
            {
               slice = object_bounded->get_owner();
               if (msb == std::numeric_limits<unsigned int>::max())
                  msb = bit;
               lsb = bit;
               continue;
            }
         }
         else if (object_bounded->get_kind() == constant_o_K)
         {
            if (local_first_port_analyzed)
               port_binding += ", ";
            if (slice)
            {
               port_binding += HDL_manager::convert_to_identifier(this, slice->get_id());
               unsigned int max=0;
               port_o *pvs = GetPointer<port_o>(slice);
               if (pvs) max = pvs->get_ports_size();
               else
               {
                  signal_o *sv = GetPointer<signal_o>(slice);
                  if (sv) max = sv->get_signals_size();
               }
               if (lsb != 0 || msb != (max - 1))
               {
                  port_binding += "[" + boost::lexical_cast<std::string>(msb);
                  if (msb != lsb)
                     port_binding += ":" + boost::lexical_cast<std::string>(lsb);
                  port_binding += "], ";
               }
               else if (local_first_port_analyzed)
                  port_binding += ", ";
               local_first_port_analyzed = true;
               slice = null_object;
               msb = std::numeric_limits<unsigned int>::max();
            }
            constant_o *con = GetPointer<constant_o>(object_bounded);
            std::string trimmed_value = "";
            unsigned long long int long_value = boost::lexical_cast<unsigned long long int>(con->get_value());
            for(unsigned int ind = 0; ind < GET_TYPE_SIZE(con); ind++)
               trimmed_value = trimmed_value + (((1LLU << (GET_TYPE_SIZE(con)-ind-1)) & long_value) ? '1' : '0');
            port_binding += STR(GET_TYPE_SIZE(con)) + "'b" + trimmed_value;
         }
         else
         {
            if (local_first_port_analyzed)
               port_binding += ", ";
            if (slice)
            {
               port_binding += HDL_manager::convert_to_identifier(this, slice->get_id());
               unsigned int max=0;
               port_o *pvs = GetPointer<port_o>(slice);
               if (pvs) max = pvs->get_ports_size();
               else
               {
                  signal_o *sv = GetPointer<signal_o>(slice);
                  if (sv) max = sv->get_signals_size();
               }
               if (lsb != 0 || msb != (max - 1))
               {
                  port_binding += "[" + boost::lexical_cast<std::string>(msb);
                  if (msb != lsb)
                     port_binding += ":" + boost::lexical_cast<std::string>(lsb);
                  port_binding += "], ";
               }
               else if (local_first_port_analyzed)
                  port_binding += ", ";
               slice = null_object;
               msb = std::numeric_limits<unsigned int>::max();
            }
            port_binding += HDL_manager::convert_to_identifier(this, object_bounded->get_id());
         }
         local_first_port_analyzed = true;
      }
      if (slice)
      {
         if (local_first_port_analyzed)
            port_binding += ", ";
         port_binding += HDL_manager::convert_to_identifier(this, slice->get_id());
         unsigned int max=0;
         port_o *pvs = GetPointer<port_o>(slice);
         if (pvs) max = pvs->get_ports_size();
         else
         {
            signal_o *sv = GetPointer<signal_o>(slice);
            if (sv) max = sv->get_signals_size();
         }
         if (lsb != 0 || msb != (max - 1))
         {
            port_binding += "[" + boost::lexical_cast<std::string>(msb);
            if (msb != lsb)
               port_binding += ":" + boost::lexical_cast<std::string>(lsb);
            port_binding += "]";
         }
      }

      if (port_binding.size() == 0) return;

      if (first_port_analyzed)
         PP(os, ", ");
      first_port_analyzed = true;
      if (!is_gate_builtin)
      {
         PP(os, ".");
         PP(os, HDL_manager::convert_to_identifier(this, port->get_id()));
      }
      if (!is_gate_builtin)
         PP(os, "({");
      PP(os, port_binding);
      if (!is_gate_builtin)
         PP(os, "})");
   }
}

void verilog_writer::write_port_binding(std::ostream& os, const structural_objectRef &port, const structural_objectRef &object_bounded, bool& first_port_analyzed)
{
   if (!object_bounded) return;
   THROW_ASSERT(port, "NULL object_bounded received");
   THROW_ASSERT(port->get_kind() == port_o_K, "Expected a port got something of different");
   THROW_ASSERT(port->get_owner(), "The port has to have an owner");
   THROW_ASSERT(object_bounded, "NULL object_bounded received for port: " + port->get_path());
   THROW_ASSERT(object_bounded->get_kind() != port_o_K || object_bounded->get_owner(), "A port has to have always an owner");
   bool is_gate_builtin = false;
   if (first_port_analyzed)
      PP(os, ", ");
   if (port->get_owner()->get_kind() == port_vector_o_K)
   {
      if (!builtin_verilog_gates::is_builtin(GET_TYPE_NAME(port->get_owner()->get_owner())))
      {
         PP(os, ".");
         PP(os, HDL_manager::convert_to_identifier(this, port->get_owner()->get_id()) + "[" + port->get_id() + "]");
      }
      else
         is_gate_builtin = true;
   }
   else
   {
      if (!builtin_verilog_gates::is_builtin(GET_TYPE_NAME(port->get_owner())))
      {
         PP(os, ".");
         PP(os, HDL_manager::convert_to_identifier(this, port->get_id()));
      }
      else
         is_gate_builtin = true;
   }
   if (!is_gate_builtin)
      PP(os, "(");
   if (object_bounded->get_kind() == port_o_K && object_bounded->get_owner()->get_kind() == port_vector_o_K)
   {
      PP(os, HDL_manager::convert_to_identifier(this, object_bounded->get_owner()->get_id()) + type_converter_size(object_bounded));
   }
   else if (object_bounded->get_kind() == signal_o_K && object_bounded->get_owner()->get_kind() == signal_vector_o_K)
   {
      PP(os, HDL_manager::convert_to_identifier(this, object_bounded->get_owner()->get_id()) + type_converter_size(object_bounded));
   }
   else if (object_bounded->get_kind() == constant_o_K)
   {
      constant_o *con = GetPointer<constant_o>(object_bounded);
      std::string trimmed_value = "";
      unsigned long long int long_value = boost::lexical_cast<unsigned long long int>(con->get_value());
      for(unsigned int ind = 0; ind < GET_TYPE_SIZE(con); ind++)
         trimmed_value = trimmed_value + (((1LLU << (GET_TYPE_SIZE(con)-ind-1)) & long_value) ? '1' : '0');
      PP(os, STR(GET_TYPE_SIZE(con)) + "'b" + trimmed_value);
   }
   else
      PP(os, HDL_manager::convert_to_identifier(this, object_bounded->get_id()));
   if (!is_gate_builtin)
      PP(os, ")");
   first_port_analyzed = true;
}

void verilog_writer::write_io_signal_post_fix(std::ostream& os, const structural_objectRef &port, const structural_objectRef &sig)
{
   THROW_ASSERT(port && port->get_kind() == port_o_K, "Expected a port got something of different");
   THROW_ASSERT(port->get_owner(), "Expected a port with an owner");
   THROW_ASSERT(sig && (sig->get_kind() == signal_o_K || sig->get_kind() == constant_o_K), "Expected a signal or a constant, got something of different");
   std::string port_string;
   std::string signal_string;
   if (sig->get_kind() == constant_o_K)
   {
      constant_o *con = GetPointer<constant_o>(sig);
      std::string trimmed_value = "";
      unsigned long long int long_value = boost::lexical_cast<unsigned long long int>(con->get_value());
      for(unsigned int ind = 0; ind < GET_TYPE_SIZE(con); ind++)
         trimmed_value = trimmed_value + (((1LLU << (GET_TYPE_SIZE(con)-ind-1)) & long_value) ? '1' : '0');
      signal_string = STR(GET_TYPE_SIZE(con)) + "'b" + trimmed_value;
   }
   else if (sig->get_kind() == signal_o_K)
   {
      if (sig->get_owner()->get_kind() == signal_vector_o_K)
         signal_string = HDL_manager::convert_to_identifier(this, sig->get_owner()->get_id()) + may_slice_string(port);
      else
         signal_string = HDL_manager::convert_to_identifier(this, sig->get_id());
   }
   if (port->get_owner()->get_kind() == port_vector_o_K)
      port_string = HDL_manager::convert_to_identifier(this, port->get_owner()->get_id()) + may_slice_string(port);
   else
      port_string = HDL_manager::convert_to_identifier(this, port->get_id());
   if (GetPointer<port_o>(port)->get_port_direction() == port_o::IN)
      std::swap(port_string, signal_string);
   if (port_string != signal_string) PP(os, "assign " + port_string + " = " + signal_string + ";\n");
}

void verilog_writer::write_module_parametrization(std::ostream& os, const structural_objectRef &cir)
{
   THROW_ASSERT(cir->get_kind() == component_o_K || cir->get_kind() == channel_o_K, "Expected a component or a channel got something of different");
   module * mod = GetPointer<module>(cir);
   const NP_functionalityRef &np = mod->get_NP_functionality();
   if (np)
   {
      bool first_it = true;

      ///writing memory-related parameters
      if (mod->is_parameter(MEMORY_PARAMETER))
      {
         std::string memory_str = mod->get_parameter(MEMORY_PARAMETER);
         std::vector<std::string> mem_tag = convert_string_to_vector<std::string>(memory_str, ";");
         for(unsigned int i = 0; i < mem_tag.size(); i++)
         {
            std::vector<std::string> mem_add = convert_string_to_vector<std::string>(mem_tag[i], "=");
            THROW_ASSERT(mem_add.size() == 2, "malformed address");
            if(first_it)
            {
               PP(os, " #(");
               first_it = false;
            }
            else
            {
               PP(os, ", ");
            }
            std::string name = mem_add[0];
            std::string value = mem_add[1];
            if (value.find("\"\"") != std::string::npos)
            {
               boost::replace_all(value, "\"\"", "\"");
            }
            else if (value.find("\"") != std::string::npos)
            {
               boost::replace_all(value, "\"", "");
               value = boost::lexical_cast<std::string>(value.size()) + "'b" + value;
            }
            PP(os, "." + name + "(" + name + ")");
         }
      }

      std::vector<std::pair<std::string, structural_objectRef> > parameters;
      mod->get_NP_library_parameters(cir, parameters);
      std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it_end = parameters.end();
      std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it = parameters.begin();
      for(;it!= it_end; ++it)
      {
         if(first_it)
         {
            PP(os, " #(");
            first_it = false;
         }
         else
            PP(os, ", ");
         const std::string &name = it->first;
         structural_objectRef obj = it->second;

         if (!mod->is_parameter(std::string(BITSIZE_PREFIX)+name) && obj)
         {
            structural_type_descriptor::s_type type=obj->get_typeRef()->type;
            if((type == structural_type_descriptor::VECTOR_INT ||
                type == structural_type_descriptor::VECTOR_UINT ||
                type == structural_type_descriptor::VECTOR_REAL))
            {
               PP(os, "." + std::string(BITSIZE_PREFIX+name) + "(" + boost::lexical_cast<std::string>(obj->get_typeRef()->size) + ")");
               PP(os, ", ");
               PP(os, "." + std::string(NUM_ELEM_PREFIX+name) + "(" + boost::lexical_cast<std::string>(obj->get_typeRef()->vector_size) + ")");
            }
            else
            {
               PP(os, "." + std::string(BITSIZE_PREFIX)+name + "(" + boost::lexical_cast<std::string>(GET_TYPE_SIZE(obj)) + ")");
               if (obj->get_kind() == port_vector_o_K)
               {
                  PP(os, ", ");
                  unsigned int ports_size = GetPointer<port_o>(obj)->get_ports_size();
                  PP(os, "." + std::string(PORTSIZE_PREFIX)+name + "(" + boost::lexical_cast<std::string>(ports_size) + ")");
               }
            }
         }
         else
         {
            std::string param_value, param_name;
            if (mod->is_parameter(name))
            {
               param_value = mod->get_parameter(name);
               param_name = name;
            }
            else if (mod->is_parameter(std::string(BITSIZE_PREFIX)+name))
            {
               param_value = mod->get_parameter(std::string(BITSIZE_PREFIX)+name);
               param_name = std::string(BITSIZE_PREFIX)+name;
            }
            else
            {
               cir->print(std::cerr);
               THROW_ERROR("Parameter is missing for port " + name + " in module " + cir->get_path());

            }
            if (param_value.find("\"\"") != std::string::npos)
            {
               boost::replace_all(param_value, "\"\"", "\"");
            }
            else if (param_value.find("\"") != std::string::npos)
            {
               boost::replace_all(param_value, "\"", "");
               param_value = boost::lexical_cast<std::string>(param_value.size()) + "'b" + param_value;
            }
            PP(os, "." + param_name + "(" + param_value + ")");
         }
      }

      if(!first_it)
         PP(os, ")");
   }
}

void verilog_writer::write_tail(std::ostream& , const structural_objectRef &)
{
}

void verilog_writer::write_state_declaration(std::ostream& os, const structural_objectRef &cir, const std::list<std::string> &list_of_states, const std::string &, const std::string &/*reset_state*/)
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Starting state declaration...");

   std::list<std::string>::const_iterator it_end = list_of_states.end();
   unsigned int n_states = static_cast<unsigned int>(list_of_states.size());
   unsigned int count = 0;

   PP(os, "parameter ["+boost::lexical_cast<std::string>(language_writer::bitnumber(n_states-1)-1)+":0] ");
   for(std::list<std::string>::const_iterator it = list_of_states.begin(); it != it_end; it++)
   {
      PP(os, *it+" = "+boost::lexical_cast<std::string>(language_writer::bitnumber(n_states-1))+"'d"+boost::lexical_cast<std::string>(it->substr(strlen(STATE_NAME_PREFIX))));
      count++;
      if (count == n_states)
         ;
      else
      {
         if (count == 1)
            PP.indent();
         PP(os, ",\n");
      }
   }
   if(count > 1)
      PP.deindent();
   PP(os, ";\n");
   //PP(os, "// synthesis attribute init of _present_state is " + reset_state + ";\n");
   //PP(os, "// synthesis attribute use_sync_reset of _present_state is no;\n");
   PP(os, "reg ["+boost::lexical_cast<std::string>(language_writer::bitnumber(n_states-1)-1)+":0] _present_state, _next_state;\n");
   module * mod = GetPointer<module>(cir);
   THROW_ASSERT(mod, "Expected a component object");
   THROW_ASSERT(mod->get_out_port_size(), "Expected a FSM with at least one output");

   std::string port_name;

   /// enable buffers
   for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
   {
      port_name = HDL_manager::convert_to_identifier(this, mod->get_out_port(i)->get_id());
      PP(os, "reg " + port_name + ";\n");
   }

   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Completed state declaration");
}

void verilog_writer::write_present_state_update(std::ostream& os, const std::string &reset_state, const std::string &reset_port, const std::string &clock_port, const std::string &reset_type)
{
   if(reset_type == "no" || reset_type == "sync")
      PP(os, "always @(posedge "+clock_port+")\n");
   else
      PP(os, "always @(posedge "+clock_port+" or negedge " + reset_port + ")\n");
   PP.indent();
   ///reset is needed even in case of reset_type == "no"
   PP(os, "if (!" + reset_port + ") _present_state <= "+reset_state+";\n");
   PP(os, "else _present_state <= _next_state;\n");
   PP.deindent();
}

void verilog_writer::write_transition_output_functions(std::ostream& os, const structural_objectRef &cir, const std::string &reset_state, const std::string &reset_port, const std::string &start_port, const std::string &clock_port, std::vector<std::string>::const_iterator &first, std::vector<std::string>::const_iterator &end)
{
   std::vector<std::string> input_ports;

   typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
   const char soc[3] = {STD_OPENING_CHAR, '\n', '\0'};
   const char scc[3] = {STD_CLOSING_CHAR, '\n', '\0'};
   const char soc1[2] = {STD_OPENING_CHAR, '\0'};
   const char scc1[2] = {STD_CLOSING_CHAR, '\0'};

   boost::char_separator<char> state_sep(":", nullptr);
   boost::char_separator<char> sep(" ", nullptr);

   module * mod = GetPointer<module>(cir);
   THROW_ASSERT(mod, "Expected a component object");
   THROW_ASSERT(mod->get_out_port_size(), "Expected a FSM with at least one output");

   std::string port_name;

   for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
   {
      port_name = HDL_manager::convert_to_identifier(this, mod->get_in_port(i)->get_id());
      if (port_name != clock_port and port_name != reset_port and port_name != START_PORT_NAME)
      {
         input_ports.push_back(port_name);
      }
   }
   //std::cerr << "Number of input ports: " << input_ports.size() << std::endl;

   /// state transitions description
   PP(os, "\nalways @(_present_state");
   if (mod->get_in_port_size())
   {
      for (unsigned int i = 0; i < mod->get_in_port_size(); i++)
      {
         port_name = HDL_manager::convert_to_identifier(this, mod->get_in_port(i)->get_id());
         if(port_name != reset_port && port_name != clock_port)
            PP(os, " or "+ port_name);
      }
   }
   PP(os, ")\nbegin");
   PP(os, soc);

   ///compute the default output
   PP(os, "_next_state = " + reset_state + ";\n");
   std::string default_output;
   //std::cerr << "Number of output ports: " << mod->get_out_port_size() << std::endl;
   for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
   {
      port_name = HDL_manager::convert_to_identifier(this, mod->get_out_port(i)->get_id());
      default_output += "0";
      PP(os, port_name + " = 1'b0;\n");
   }

   PP(os, "case (_present_state)");
   PP(os, soc);

   for(std::vector<std::string>::const_iterator first_it = first; first_it != end; first_it++)
   {
      //std::cerr << "writing '" << *first_it << std::endl;
      tokenizer state_tokens_first(*first_it, state_sep);

      tokenizer::const_iterator it = state_tokens_first.begin();

      std::string state_description = *it;
      //std::cerr << "  state: '" << state_description << "'" << std::endl;
      it++;

      std::vector<std::string> state_transitions;
      for(; it != state_tokens_first.end(); it++)
         state_transitions.push_back(*it);
      //std::cerr << "    number of transitions: '" << state_transitions.size() << "'" << std::endl;

      tokenizer tokens_curr(state_description, sep);

      ///get the present state
      it = tokens_curr.begin();
      //std::cerr << "    present state: '" << *it << "'" << std::endl;
      std::string present_state = HDL_manager::convert_to_identifier(this, *it);
      ///get the current output
      it++;
      std::string current_output = *it;

      PP(os, soc1);
      PP(os, present_state + " :\n");

      if(reset_state == present_state)
      {
         PP(os, "if(~"+start_port+")\n");
         PP(os, "begin");
         PP(os, soc);
         PP(os, "_next_state = " + present_state + ";");
         PP(os, scc);
         PP(os, "end\n");
         PP(os, "else\n");
      }
      PP(os, "begin");
      PP(os, soc);
      if(current_output != default_output)
      {
         for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
         {
            port_name = HDL_manager::convert_to_identifier(this, mod->get_out_port(i)->get_id());
            //std::cerr << "setting output of port '" << port_name << "'" << std::endl;
            if(default_output[i] != current_output[i] and current_output[i] != '-')
               PP(os, port_name + " = 1'b"+current_output[i]+";\n");
         }
      }

      bool unique_transition = (state_transitions.size() == 1);

      //std::cerr << "start writing transitions..." << std::endl;

      for(unsigned int i = 0; i < state_transitions.size(); i++)
      {
         //std::cerr << " transition '" << state_transitions[i] << "'" << std::endl;
         tokenizer transition_tokens(state_transitions[i], sep);
         tokenizer::const_iterator itt = transition_tokens.begin();

         //std::string current_input;
         tokenizer::const_iterator current_input_it;
         std::string input_string=*itt;
         if(mod->get_in_port_size() - 3) // clock and reset are always present
         {
            boost::char_separator<char> comma_sep(",", nullptr);
            tokenizer current_input_tokens(input_string, comma_sep);
            current_input_it = current_input_tokens.begin();
            itt++;
         }
         std::string next_state = *itt;
         ++itt;
         std::string transition_outputs = *itt;
         ++itt;
         THROW_ASSERT(itt == transition_tokens.end(), "Bad transition format");

         if (!unique_transition)
         {
            if (i == 0)
            {
               PP(os, "if (");
            }
            else if ((i + 1) == state_transitions.size())
            {
               PP(os, "else");
            }
            else
            {
               PP(os, "else if (");
            }
            if ((i + 1) < state_transitions.size())
            {
               bool first_test = true;
               for (unsigned int ind = 0; ind < mod->get_in_port_size(); ind++)
               {
                  port_name = HDL_manager::convert_to_identifier(this, mod->get_in_port(ind)->get_id());
                  unsigned int port_size = mod->get_in_port(ind)->get_typeRef()->size;
                  unsigned int vec_size =mod->get_in_port(ind)->get_typeRef()->vector_size;
                  if(port_name != reset_port && port_name != clock_port && port_name != start_port)
                  {
                     std::string in_or_conditions = *current_input_it;
                     boost::char_separator<char> pipe_sep("|", nullptr);
                     tokenizer in_or_conditions_tokens(in_or_conditions, pipe_sep);

                     if((*in_or_conditions_tokens.begin()) != "-")
                     {
                        if(!first_test)
                           PP(os, " && ");
                        else
                           first_test=false;
                        bool first_test_or = true;
                        bool need_parenthesis = false;
                        std::string res_or_conditions;
                        for(tokenizer::const_iterator in_or_conditions_tokens_it = in_or_conditions_tokens.begin(); in_or_conditions_tokens_it != in_or_conditions_tokens.end(); ++in_or_conditions_tokens_it)
                        {
                           THROW_ASSERT((*in_or_conditions_tokens_it) != "-", "wrong conditions structure");
                           if(!first_test_or)
                           {
                              res_or_conditions += " || ";
                              need_parenthesis = true;
                           }
                           else
                              first_test_or=false;

                           res_or_conditions += port_name;
                           res_or_conditions += std::string(" == ") + ((*in_or_conditions_tokens_it)[0] == '-' ? "-" : "") + (vec_size==0 ? boost::lexical_cast<std::string>(port_size) : boost::lexical_cast<std::string>(vec_size));
                           if(port_size > 1 || (port_size == 1 && vec_size > 0))
                              res_or_conditions += "'d"+(((*in_or_conditions_tokens_it)[0] == '-') ? ((*in_or_conditions_tokens_it).substr(1)) : *in_or_conditions_tokens_it);
                           else
                              res_or_conditions += "'b"+*in_or_conditions_tokens_it;
                        }
                        if(need_parenthesis)
                           res_or_conditions = "(" + res_or_conditions + ")";
                        PP(os, res_or_conditions);
                     }
                     current_input_it++;
                  }
               }
               PP(os, ")");
            }
            PP(os, soc);
            PP(os, "begin");
            PP(os, soc);
         }
         PP(os, "_next_state = " + next_state + ";\n");
         for (unsigned int ind = 0; ind < mod->get_out_port_size(); ind++)
         {
            if (transition_outputs[ind] != '-')
            {
               port_name = HDL_manager::convert_to_identifier(this, mod->get_out_port(ind)->get_id());
               PP(os, port_name + " = 1'b" + transition_outputs[ind] + ";\n");
            }
         }
         if (!unique_transition)
         {
            PP(os, scc1);
            PP(os, "end");
            PP(os, scc);
         }
      }

      PP(os, scc1);
      PP(os, "end");
      PP(os, scc);

      //std::cerr << "completed '" << *first_it << "'" << std::endl;
   }

   PP(os, soc1);
   PP(os, "default :\n");
   PP(os, "begin");
   PP(os, soc);
   for (unsigned int i = 0; i < mod->get_out_port_size(); i++)
   {
      port_name = HDL_manager::convert_to_identifier(this, mod->get_out_port(i)->get_id());
      PP(os, port_name + " = 1'b0;\n");
   }
   PP(os, scc1);
   PP(os, "end");
   PP(os, scc);

   PP(os, scc1);
   PP(os, "endcase\n");
   PP(os, scc1);
   PP(os, "end");
}

void verilog_writer::write_NP_functionalities(std::ostream& os, const structural_objectRef &cir)
{
   module * mod = GetPointer<module>(cir);
   THROW_ASSERT(mod, "Expected a component object");
   const NP_functionalityRef &np = mod->get_NP_functionality();
   THROW_ASSERT(np, "NP Behavioral description is missing for module: "+HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)));
   std::string beh_desc = np->get_NP_functionality(NP_functionality::VERILOG_PROVIDED);
   THROW_ASSERT(beh_desc != "", "VERILOG behavioral description is missing for module: "+HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)));
   remove_escaped(beh_desc);
   PP(os, beh_desc);
}

void verilog_writer::write_port_decl_header(std::ostream&)
{
   /// do nothing
}

void verilog_writer::write_port_decl_tail(std::ostream&)
{
   /// do nothing
}

void verilog_writer::write_module_parametrization_decl(std::ostream& os, const structural_objectRef &cir)
{
   THROW_ASSERT(cir->get_kind() == component_o_K || cir->get_kind() == channel_o_K, "Expected a component or a channel got something of different");
   module * mod = GetPointer<module>(cir);
   const NP_functionalityRef &np = mod->get_NP_functionality();
   if (np)
   {
      bool first_it = true;

      ///writing memory-related parameters
      if (mod->is_parameter(MEMORY_PARAMETER))
      {
         std::string memory_str = mod->get_parameter(MEMORY_PARAMETER);
         std::vector<std::string> mem_tag = convert_string_to_vector<std::string>(memory_str, ";");
         for(unsigned int i = 0; i < mem_tag.size(); i++)
         {
            std::vector<std::string> mem_add = convert_string_to_vector<std::string>(mem_tag[i], "=");
            THROW_ASSERT(mem_add.size() == 2, "malformed address");
            if(first_it)
            {
               PP(os, "parameter ");
               first_it = false;
            }
            else
            {
               PP(os, ", ");
            }
            std::string name = mem_add[0];
            std::string value = mem_add[1];
            if (value.find("\"\"") != std::string::npos)
            {
               boost::replace_all(value, "\"\"", "\"");
            }
            else if (value.find("\"") != std::string::npos)
            {
               boost::replace_all(value, "\"", "");
               value = boost::lexical_cast<std::string>(value.size()) + "'b" + value;
            }
            PP(os, name + "=" + value);
         }
      }

      ///writing other library parameters
      std::vector<std::pair<std::string, structural_objectRef> > parameters;
      mod->get_NP_library_parameters(cir, parameters);
      std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it_end = parameters.end();
      std::vector<std::pair<std::string, structural_objectRef> >::const_iterator it = parameters.begin();
      for(;it!= it_end; ++it)
      {
         if(first_it)
         {
            PP(os, "parameter ");
            first_it = false;
         }
         else
            PP(os, ", ");
         const std::string &name = it->first;
         structural_objectRef obj = it->second;
         if(obj)
         {
            structural_type_descriptor::s_type type=obj->get_typeRef()->type;
            if((type == structural_type_descriptor::VECTOR_INT ||
                type == structural_type_descriptor::VECTOR_UINT ||
                type == structural_type_descriptor::VECTOR_REAL))
            {
               PP(os, BITSIZE_PREFIX+name + "=" + boost::lexical_cast<std::string>(obj->get_typeRef()->size));
               PP(os, ", ");
               PP(os, NUM_ELEM_PREFIX+name + "=" + boost::lexical_cast<std::string>(obj->get_typeRef()->vector_size));
            }
            else
            {
               PP(os, BITSIZE_PREFIX+name + "=" + boost::lexical_cast<std::string>(GET_TYPE_SIZE(obj)));
               if (obj->get_kind() == port_vector_o_K)
               {
                  PP(os, ", ");
                  unsigned int ports_size = GetPointer<port_o>(obj)->get_ports_size();
                  if(ports_size==0)
                     ports_size = 2;
                  PP(os, PORTSIZE_PREFIX+name + "=" + boost::lexical_cast<std::string>(ports_size));
               }
            }
         }
         else
         {
            std::string param = mod->get_parameter(name);
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  parameter = #" << name << "#; value = #" << param << "#");
            if (param.find("\"\"") != std::string::npos)
            {
               boost::replace_all(param, "\"\"", "\"");
            }
            else if (param.find("\"") != std::string::npos)
            {
               boost::replace_all(param, "\"", "");
               param = boost::lexical_cast<std::string>(param.size()) + "'b" + param;
            }
            PP(os, name + "=" + param);

         }
      }

      ///if at least one parameter is used, first_it is true.
      if(!first_it) PP(os, ";\n");
   }
}

const char* verilog_writer::tokenNames[]=
{
   "abs",
   "abstol",
   "access",
   "acos",
   "acosh",
   "always",
   "always_comb",
   "always_ff",
   "always_latch",
   "analog",
   "and",
   "asin",
   "asinh",
   "assert",
   "assign",
   "atan",
   "atan2",
   "atanh",
   "automatic",
   "begin",
   "bool",
   "buf",
   "bufif0",
   "bufif1",
   "case",
   "casex",
   "casez",
   "ceil",
   "cell",
   "cmos",
   "config",
   "continuous",
   "cos",
   "cosh",
   "ddt_nature",
   "deassign",
   "default",
   "defparam",
   "design",
   "disable",
   "discipline",
   "discrete",
   "domain",
   "edge",
   "else",
   "end",
   "endcase",
   "endconfig",
   "enddiscipline",
   "endfunction",
   "endgenerate",
   "endmodule",
   "endnature",
   "endprimitive",
   "endspecify",
   "endtable",
   "endtask",
   "event",
   "exclude",
   "exp",
   "floor",
   "flow",
   "for",
   "force",
   "forever",
   "fork",
   "from",
   "function",
   "generate",
   "genvar",
   "ground",
   "highz0",
   "highz1",
   "hypot",
   "idt_nature",
   "if",
   "ifnone",
   "incdir",
   "include",
   "inf",
   "initial",
   "inout",
   "input",
   "instance",
   "integer",
   "join",
   "large",
   "liblist",
   "library",
   "ln",
   "localparam",
   "log",
   "logic",
   "macromodule",
   "max",
   "medium",
   "min",
   "module",
   "nand",
   "nature",
   "negedge",
   "new",
   "nmos",
   "nor",
   "noshowcancelled",
   "not",
   "notif0",
   "notif1",
   "or",
   "output",
   "parameter",
   "pmos",
   "posedge",
   "potential",
   "pow",
   "primitive",
   "pull0",
   "pull1",
   "pulldown",
   "pullup",
   "pulsestyle_onevent",
   "pulsestyle_ondetect",
   "rcmos",
   "real",
   "realtime",
   "reg",
   "release",
   "repeat",
   "rnmos",
   "rpmos",
   "rtran",
   "rtranif0",
   "rtranif1",
   "scalared",
   "showcancelled",
   "signed",
   "sin",
   "sinh",
   "small",
   "specify",
   "specparam",
   "sqrt",
   "string",
   "strong0",
   "strong1",
   "supply0",
   "supply1",
   "table",
   "tan",
   "tanh",
   "task",
   "time",
   "tran",
   "tranif0",
   "tranif1",
   "tri",
   "tri0",
   "tri1",
   "triand",
   "trior",
   "trireg",
   "units",
   "unsigned",
   "use",
   "uwire",
   "vectored",
   "wait",
   "wand",
   "weak0",
   "weak1",
   "while",
   "wire",
   "wone",
   "wor",
   "xnor",
   "xor",
   /// some Verilog 2005 keywords
   "type"
};

verilog_writer::verilog_writer(int _debug_level) :
   language_writer(STD_OPENING_CHAR, STD_CLOSING_CHAR, _debug_level)
{
   for(unsigned int i = 0; i < sizeof(tokenNames)/sizeof(char*); ++i)
      keywords.insert(tokenNames[i]);
}

verilog_writer::~verilog_writer()
{

}

bool verilog_writer::check_keyword(std::string id) const
{
   return keywords.find(id) != keywords.end();
}

void verilog_writer::write_timing_specification(std::ostream& os, const technology_managerConstRef TM, const structural_objectRef& circ)
{
   module* mod_inst = GetPointer<module>(circ);
   if (mod_inst->get_internal_objects_size() > 0) return;
   const NP_functionalityRef &np = mod_inst->get_NP_functionality();
   if (np && np->exist_NP_functionality(NP_functionality::FSM)) return;

   std::string library = TM->get_library(circ->get_typeRef()->id_type);
   const technology_nodeRef& fu = TM->get_fu(circ->get_typeRef()->id_type, library);

   if (!GetPointer<functional_unit>(fu)) return;

   PP(os, "\n");
   write_comment(os, "Timing annotations\n");
   PP(os, "specify\n");
   PP.indent();
   const functional_unit::operation_vec& ops = GetPointer<functional_unit>(fu)->get_operations();
   for(unsigned int i = 0; i < ops.size(); i++)
   {
      std::map<std::string, std::map<std::string, double> > delays = GetPointer<operation>(ops[i])->get_pin_to_pin_delay();
      if (delays.size() == 0)
      {
         for(unsigned int out = 0; out < mod_inst->get_out_port_size(); out++)
         {
            for(unsigned int in = 0; in < mod_inst->get_in_port_size(); in++)
            {
               if (ops.size() > 1) PP(os, "if (sel_" + GetPointer<operation>(ops[i])->operation_name + " == 1'b1) ");
               THROW_ASSERT(GetPointer<operation>(ops[i])->time_m, "the operation has not any timing information associated with");
               PP(os, "(" + mod_inst->get_in_port(in)->get_id() + " *> " + mod_inst->get_out_port(out)->get_id() + ") = " + STR(GetPointer<operation>(ops[i])->time_m->get_execution_time()) + ";\n");
            }
         }
      }
      else
      {
         for(std::map<std::string, std::map<std::string, double> >::iterator d = delays.begin(); d != delays.end(); d++)
         {
            for(std::map<std::string, double>::iterator o = d->second.begin(); o != d->second.end(); o++)
            {
               if (ops.size() > 1) PP(os, "if (sel_" + GetPointer<operation>(ops[i])->operation_name + " == 1'b1) ");
               PP(os, "(" + d->first + " *> " + o->first + ") = " + STR(o->second) + ";\n");
            }
         }
      }
   }
   PP.deindent();
   PP(os, "endspecify\n\n");
}
