/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file verilog_writer.cpp
 * @brief Write system verilog provided descriptions.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#include "sv_writer.hpp"

#include "HDL_manager.hpp"

#include "technology_manager.hpp"

#include "structural_objects.hpp"
#include "exceptions.hpp"
#include "dbgPrintHelper.hpp"
#include "NP_functionality.hpp"
#include "structural_objects.hpp"




void system_verilog_writer::write_NP_functionalities(std::ostream& os, const structural_objectRef &cir)
{
   module * mod = GetPointer<module>(cir);
   THROW_ASSERT(mod, "Expected a component object");
   const NP_functionalityRef &np = mod->get_NP_functionality();
   THROW_ASSERT(np, "NP Behavioral description is missing for module: "+HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)));
   std::string beh_desc = np->get_NP_functionality(NP_functionality::SYSTEM_VERILOG_PROVIDED);
   THROW_ASSERT(beh_desc != "", "SYSTEM VERILOG behavioral description is missing for module: "+HDL_manager::convert_to_identifier(this, GET_TYPE_NAME(cir)));
   remove_escaped(beh_desc);
   PP(os, beh_desc);
}

system_verilog_writer::system_verilog_writer(int _debug_level) :
   verilog_writer(_debug_level)
{
}

system_verilog_writer::~system_verilog_writer()
{

}

