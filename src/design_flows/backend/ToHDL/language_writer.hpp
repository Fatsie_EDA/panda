/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file language_writer.hpp
 * @brief This class writes different HDL based descriptions (VHDL, Verilog, SystemC) starting from a structural representation.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef LANGUAGE_WRITER_HPP
#define LANGUAGE_WRITER_HPP

///Autoheader include
#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_FROM_C_BUILT.hpp"

#include <string>
#include <vector>
#include <set>
#include <list>

#include "dbgPrintHelper.hpp"
#include "simple_indent.hpp"
#include "refcount.hpp"

/**
 * @name Forward declarations.
*/
//@{
REF_FORWARD_DECL(language_writer);
REF_FORWARD_DECL(structural_type_descriptor);
REF_FORWARD_DECL(structural_object);
CONSTREF_FORWARD_DECL(technology_manager);
//@}

#define MEM_PREFIX "MEM_"
#define BITSIZE_PREFIX "BITSIZE_"
#define PORTSIZE_PREFIX "PORTSIZE_"
#define NUM_ELEM_PREFIX "NUM_ELEM_"
/**
 * HDL writer base class used to specify the interface of the different language writers
*/
class language_writer
{
   protected:

      ///pretty print functor object used by all print members to indent the generated code.
      simple_indent PP;

      ///list of library imported (e.g., includes).
      std::set<std::string> list_of_lib;

      ///list of customized gates
      std::set<std::string> list_of_customized_gates;

      ///debugging level of the class
      int debug_level;

   public:

      ///supported languages
      enum language
      {
         VERILOG = 0,
         SYSTEM_VERILOG,
#if HAVE_EXPERIMENTAL
         SYSTEMC,
         BLIF,
         EDIF,
#endif
         VHDL
      };

      /**
       * Constructor
       */
      language_writer(char open_char, char close_char, int _debug_level);

      /**
       * Destructor
       */
      virtual ~language_writer();

      /**
       * Creates the specialization of the writer based on the desired language
       * @param language_w is the desired language
       * @param debug_level is the debugging lever for the writer
       */
      static
      language_writerRef create_writer(language language_w, int debug_level);

      /**
       * Returns the name of the language writer.
       */
      virtual std::string get_name() const = 0;

      /**
       * Returns the filename extension associated with the specification.
       */
      virtual std::string get_extension() const = 0;

      /**
       * Returns true if the writer is able to correctly manage the given structural object, false otherwise.
       * @param cir is the structural description under analysis. The analysis does not consider the inner objects but just one level of the hierarchy.
       */
      virtual bool check_structural_object_compliance(const structural_objectRef &cir) const;

      /**
       * Writes a raw string into the stream.
       * @param os is the output stream.
       * @param rawString is the string to be written.
       */
      virtual void write(std::ostream& os, const std::string &rawString);

      /**
       * Writes the header part of the file. Write some lines of comments and possibly global libraries.
       * @param os is the output stream.
       */
      virtual void write_header (std::ostream& os);

      /**
       * Prints a comment.
       * @param os is the output stream.
       * @param comment_string is the string to be printed as a comment.
      */
      virtual void write_comment(std::ostream& os, const std::string &comment_string) = 0;

      /**
       * Return a language based type string given a structural_type_descriptor.
       * @param Type is the structural_type_descriptor.
      */
      virtual std::string type_converter(structural_type_descriptorRef Type) = 0;

      /**
       * Return a language based size string given the object
       * @param cir is the object
      */
      virtual std::string type_converter_size(const structural_objectRef &cir) = 0;

      /**
       * Write the declaration of the library.
       * @param os is the output stream.
       * @param cir is the component for which the library declarations are written.
      */
      virtual void write_library_declaration (std::ostream& os, const structural_objectRef &cir) = 0;
      /**
       * Write the declaration of the module.
       * @param os is the output stream.
       * @param cir is the module to be written.
      */
      virtual void write_module_declaration(std::ostream& os, const structural_objectRef &cir) = 0;
      /**
       * Write the declaration of internal objects of the module.
       * @param os is the output stream.
       * @param cir is the module to be written.
      */
      virtual void write_module_internal_declaration(std::ostream& os, const structural_objectRef &cir) = 0;
      /**
       * Write the port declaration starting from a port object.
       * @param os is the output stream
       * @param cir is the port to be written.
      */
      virtual void write_port_declaration(std::ostream& os, const structural_objectRef &cir, bool first_port_analyzed) = 0;
      /**
       * Write the declaration of componets
       * @param os is the output stream.
       * @param cir is the component to be declared.
      */
      virtual void write_component_declaration(std::ostream& os, const structural_objectRef &cir) = 0;
      /**
       * Write the declaration of a signal
       * @param os is the output stream.
       * @param cir is the signal to be declared.
      */
      virtual void write_signal_declaration(std::ostream& os, const structural_objectRef &cir) = 0;
      /**
       * Write the begin part in a module declaration.
       * @param os is the output stream.
       * @param cir is the top component to be declared.
      */
      virtual void write_module_definition_begin(std::ostream& os, const structural_objectRef &cir) = 0;
      /**
       * Write the initial part of the instance of a module.
       * @param os is the output stream.
       * @param cir is the module to be instanced.
      */
      virtual void write_module_instance_begin(std::ostream& os, const structural_objectRef &cir, bool write_parametrization) = 0;
      /**
       * Write the ending part of the instance of a module.
       * @param os is the output stream.
       * @param cir is the module to be instanced.
      */
      virtual void write_module_instance_end(std::ostream& os, const structural_objectRef &cir) = 0;
      /**
       * Write the binding of a port. It follows the name binding style.
       * @param os is the output stream.
       * @param port is the port to be bounded.
       * @param top is the component owner of the component that has the port to be bounded.
      */
      virtual void write_port_binding(std::ostream& os, const structural_objectRef &port, const structural_objectRef &top, bool& first_port_analyzed) = 0;

      virtual void write_vector_port_binding(std::ostream& os, const structural_objectRef &port, bool& first_port_analyzed) = 0;
      /**
       * Write the end part in a module declaration.
       * @param os is the output stream.
       * @param cir is the top component to be declared.
      */
      virtual void write_module_definition_end(std::ostream& os, const structural_objectRef &cir) = 0;
      /**
       * Write some code managing primary ports to signals connections.
       * Loop signals are present for example in this bench circuit:
       * INPUT(X)
       * OUTPUT(Z)
       * Y = DFF(Z)
       * Z = AND(X,Y)
       * The circuit builder adds an internal signal Z_sign allowing the write and read of the Z values.
       * @param os is the output stream.
       * @param port is the primary port for which this problem happens.
       * @param sig is the attached signal.
      */
      virtual void write_io_signal_post_fix(std::ostream& os, const structural_objectRef &port, const structural_objectRef &sig) = 0;
      /**
       * Module can be parametrized with respect different features. Port vectors are parametrized with the number of port associated,
       * while ports are parametrized in case the type is a integer with the number of bits. The id of the module is modified
       * by adding the parameters at its end. For example an AND_GATE with a port_vector of 2 will be declared as: AND_GATE_2.
       * Moreover, a multiplier with the first input of four bits, the second input with eight bits and an output of twelve bits will be
       * declared as: MULT_4_8_12.
       * Note that parametrization has a meaning only in case the functionality come from the STD technology library.
       * @param os is the output stream.
       * @param cir is the component to be declared.
      */
      virtual void write_module_parametrization(std::ostream& os, const structural_objectRef &cir) = 0;
      /**
       * Write the tail part of the file. Write some lines of comments and some debugging code.
       * @param os is the output stream.
       * @param cir is the top component.
      */
      virtual void write_tail(std::ostream& os, const structural_objectRef &cir) = 0;
      /**
       * write the declaration of all the states of the finite state machine.
       * @param os is the output stream.
       * @param list_of_states is the list of all the states.
       */
      virtual void write_state_declaration(std::ostream& os, const structural_objectRef &cir, const std::list<std::string> &list_of_states, const std::string &reset_port, const std::string &reset_state) = 0;
      /**
       * write the present_state update process
       * @param os is the output stream.
       * @param reset_state is the reset state.
       * @param reset_port is the reset port.
       * @param clock_port is the clock port.
       * @param synch_reset when true the FSM will have an synchronous reset
       */
      virtual void write_present_state_update(std::ostream& os, const std::string &reset_state, const std::string &reset_port, const std::string &clock_port, const std::string & reset_type) = 0;
      /**
       * Write the transition and output functions.
       * @param os is the output stream.
       * @param cir is the component.
       * @param reset_port is the reset port.
       * @param clock_port is the clock port.
       * @param first if the first iterator of the state table.
       * @param end if the end iterator of the state table.
       * @param n_states is the number of states.
      */
      virtual void write_transition_output_functions(std::ostream& os, const structural_objectRef &cir, const std::string &reset_state, const std::string &reset_port, const std::string &start_port, const std::string &clock_port, std::vector<std::string>::const_iterator &first, std::vector<std::string>::const_iterator &end) = 0;

      /**
       * Write in the proper language the behavioral description of the module described in "Not Parsed" form.
       * @param os is the output stream.
       * @param cir is the component.
      */
      virtual void write_NP_functionalities(std::ostream& os, const structural_objectRef &cir) = 0;

      /**
       * Write the header for generics
       * @param os is the output stream.
      */
      virtual void write_port_decl_header(std::ostream& os) = 0;

      /**
       * Write the tail for generics
       * @param os is the output stream.
       */
      virtual void write_port_decl_tail(std::ostream& os) = 0;

      /**
       * Write the declaration of the module parameters
       */
      virtual void write_module_parametrization_decl(std::ostream& os, const structural_objectRef &cir) = 0;

      virtual void write_assign(std::ostream& os, const std::string& op0, const std::string& op1) = 0;

      virtual bool has_output_prefix() const = 0;

      /**
       * Counts the number of bits in an unsigned int.
       * @param n is the number.
       */
      static
      unsigned int bitnumber (unsigned int n);

      virtual bool check_keyword(std::string id) const = 0;

      virtual void write_timing_specification(std::ostream& os, const technology_managerConstRef TM, const structural_objectRef& cir);

};
///RefCount definition of the class
typedef refcount<language_writer> language_writerRef;

#endif
