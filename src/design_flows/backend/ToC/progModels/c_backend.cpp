/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file c_backend.cpp
 * @brief Simple class used to drive the backend in order to be able to print c source code
 *
 * @author Luca Fossati <fossati@elet.polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Header include
#include  "c_backend.hpp"

///design_flows include
#include "design_flow_manager.hpp"

///design_flows/backend/ToC
#include "c_backend_step_factory.hpp"

///design_flows/backend/ToC/source_code_writers include
#include "c_writer.hpp"


///Behavior include
#include "application_manager.hpp"
#include "behavioral_helper.hpp"
#include "call_graph.hpp"
#include "function_behavior.hpp"
#include "op_graph.hpp"
#include "prettyPrintVertex.hpp"

///Graph include
#include "graph.hpp"

///Paramter include
#include "Parameter.hpp"

///STD include
#include <fstream>
#include <iostream>
#include <ostream>
#include <sstream>
#include <string>

///STL include
#include <deque>
#include <list>
#include <map>
#include <set>
#include <utility>
#include <vector>

///tree includes
#include "tree_helper.hpp"
#include "var_pp_functor.hpp"

///Utility include
#include <boost/config.hpp>
#include <boost/lexical_cast.hpp>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graph_utility.hpp>
#include "indented_output_stream.hpp"
#include "refcount.hpp"

CBackend::CBackend(const Type _type, const CBackendInformationConstRef c_backend_information, const DesignFlowManagerConstRef _design_flow_manager, const application_managerConstRef _AppM, const std::string _file_name, const ParameterConstRef _parameters) :
   DesignFlowStep(_design_flow_manager, _parameters),
   indented_output_stream(new IndentedOutputStream()),
   writer(CWriter::CreateCWriter(_type, c_backend_information, _AppM, indented_output_stream, _parameters, _parameters->getOption<int>(OPT_debug_level) >= DEBUG_LEVEL_VERBOSE)),
   file_name(_file_name),
   AppM(_AppM),
   TM(_AppM->get_tree_manager()),
   Param(_parameters),
   debug_level(_parameters->get_class_debug_level(GET_CLASS(*this)))
{
}

CBackend::~CBackend()
{
}

const CWriterRef CBackend::GetCWriter() const
{
   return writer;
}

void CBackend::Exec()
{
   INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "-->CBackend: writing " + file_name);
   WriteHeader();

   writeIncludes();

   WriteGlobalDeclarations();

   writeImplementations();
   writer->WriteFile(file_name);
   INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "<--Cbackend: writing " + file_name);
}

void CBackend::Initialize()
{
   writer->Initialize();
}

void CBackend::WriteHeader()
{
   INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "-->C backend: writing header");
   writer->WriteHeader();
   INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "<--C backend: written header");
}

void CBackend::WriteGlobalDeclarations()
{
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->CBackend: Writing global declarations");
   writer->WriteGlobalDeclarations();
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Writing function prototypes");
   std::list<unsigned int> extFunctions;
   this->AppM->get_functions_without_body(extFunctions);
   std::list<unsigned int>::iterator extBeg, extEnd;

   for (extBeg = extFunctions.begin(), extEnd = extFunctions.end(); extBeg != extEnd; extBeg++)
   {
      const BehavioralHelperConstRef BH = this->AppM->CGetFunctionBehavior(*extBeg)->CGetBehavioralHelper();
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Writing external function prototype: " + BH->get_function_name());
      bool is_system;
      std::string decl = std::get<0>(BH->get_definition(*extBeg, is_system));
      if (has_to_be_printed(is_system, decl, BH))
      {
         writer->DeclareFunctionTypes(*extBeg);
         writer->WriteFunctionDeclaration(*extBeg);
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Written external function prototype: " + BH->get_function_name());
   }

   std::list<unsigned int> functions;
   AppM->get_functions_with_body(functions);
   THROW_ASSERT(functions.size() > 0, "at least one function is expected");
   std::list<unsigned int>::const_iterator it, it_end = functions.end();
   for(it = functions.begin(); it != it_end; it++)
   {
      const BehavioralHelperConstRef BH = this->AppM->CGetFunctionBehavior(*it)->CGetBehavioralHelper();
      bool is_system;
      std::string decl = std::get<0>(BH->get_definition(*it, is_system));
      if (has_to_be_printed(is_system, decl, BH))
      {
         writer->DeclareFunctionTypes(*it);
         writer->WriteFunctionDeclaration(*it);
      }
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Written function prototypes");
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--CBackend: Written global declarations");
}

bool CBackend::has_to_be_printed(bool is_system, std::string decl, const BehavioralHelperConstRef BH) const
{
   return !is_system &&
         decl != "<built-in>" &&
                 BH->get_function_name().find("__builtin_") != 0 &&
                 BH->get_function_name().find("__hide_") != 0 &&
                 BH->get_function_name().find("__is") != 0;
}
void CBackend::writeImplementations()
{
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Writing implementations");
   //First of all I declare the functions and then the tasks
   std::list<unsigned int> functions;
   AppM->get_functions_with_body(functions);
   THROW_ASSERT(functions.size() > 0, "at least one function is expected");
   std::list<unsigned int>::const_iterator it, it_end = functions.end();
   for(it = functions.begin(); it != it_end; it++)
   {
      const BehavioralHelperConstRef BH = this->AppM->CGetFunctionBehavior(*it)->CGetBehavioralHelper();
      bool is_system;
      std::string decl = std::get<0>(BH->get_definition(*it, is_system));
      if (has_to_be_printed(is_system, decl, BH))
         writer->WriteFunctionImplementation(*it);
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Written implementations");
}

void CBackend::writeIncludes()
{
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->CBackend: writing includes");
   ///Set of decl_nodes to be examinated
   std::unordered_set<unsigned int> decl_nodes;

   std::set<std::string> writtenIncludes;

   std::list<unsigned int> extFunctions;
   this->AppM->get_functions_without_body(extFunctions);
   std::list<unsigned int>::iterator extBeg, extEnd;
   for (extBeg = extFunctions.begin(), extEnd = extFunctions.end(); extBeg != extEnd; extBeg++)
   {
      const BehavioralHelperConstRef BH = this->AppM->CGetFunctionBehavior(*extBeg)->CGetBehavioralHelper();
      AnalyzeInclude(*extBeg, BH, writtenIncludes);
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Written includes for external functions");

   std::list<unsigned int> Functions;
   AppM->get_functions_with_body(Functions);
   for (extBeg = Functions.begin(), extEnd = Functions.end(); extBeg != extEnd; extBeg++)
   {
      const BehavioralHelperConstRef BH = AppM->CGetFunctionBehavior(*extBeg)->CGetBehavioralHelper();
      const CustomSet<unsigned int> & tmp_vars = writer->GetLocalVariables(*extBeg);
      decl_nodes.insert(tmp_vars.begin(), tmp_vars.end());
      const std::list<unsigned int>& funParams = BH->get_parameters();
      decl_nodes.insert(funParams.begin(), funParams.end());
      bool is_system;
      std::string decl = std::get<0>(BH->get_definition(*extBeg, is_system));
      if (!has_to_be_printed(is_system, decl, BH))
         AnalyzeInclude(*extBeg, BH, writtenIncludes);
   }

   const BehavioralHelperConstRef  BH = AppM->CGetFunctionBehavior(*(Functions.begin()))->CGetBehavioralHelper();
   CustomSet<unsigned int> vars = AppM->get_global_variables();
   decl_nodes.insert(vars.begin(), vars.end());
   for (std::unordered_set<unsigned int>::iterator v = decl_nodes.begin(); v != decl_nodes.end(); v++)
   {
      const unsigned int variable_type = BH->get_type(*v);
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Analyzing " + BH->PrintVariable(*v) + " of type " + boost::lexical_cast<std::string>(variable_type));
      AnalyzeInclude(variable_type, BH, writtenIncludes);
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
   }
   for(std::set<std::string>::iterator s = writtenIncludes.begin(); s != writtenIncludes.end(); s++)
   {
      writer->writeInclude(*s);
   }
   std::vector<std::string> addHeaders = writer->getIncludes();
   for(std::vector<std::string>::iterator s = addHeaders.begin(); s != addHeaders.end(); s++)
   {
      if (writtenIncludes.find(*s) == writtenIncludes.end())
         writer->writeInclude(*s);
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Written includes for variables");
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--CBackend: written includes");
}

void CBackend::AnalyzeInclude(unsigned int index, const BehavioralHelperConstRef BH, std::set<std::string>& writtenIncludes)
{
   if(already_visited.find(index) != already_visited.end())
   {
      return;
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Computing include for " + boost::lexical_cast<std::string>(index));
   already_visited.insert(index);
   bool is_system;
   const std::string decl = std::get<0>(BH->get_definition(index, is_system));
   if(not decl.empty() and decl != "<built-in>" and is_system)
   {
      writtenIncludes.insert(decl);
   }
   else
   {
      const unsigned int type = tree_helper::get_type_index(TM, index);
      const std::unordered_set<unsigned int> types_to_be_declared = tree_helper::GetTypesToBeDeclared(TM, type, Param->getOption<bool>(OPT_without_transformation));
      std::unordered_set<unsigned int>::const_iterator type_to_be_declared, type_to_be_declared_end = types_to_be_declared.end();
      for(type_to_be_declared = types_to_be_declared.begin(); type_to_be_declared != type_to_be_declared_end; type_to_be_declared++)
      {
         AnalyzeInclude(*type_to_be_declared, BH, writtenIncludes);
      }
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Computed");
}

void CBackend::compute_variables(const OpGraphConstRef inGraph, const std::unordered_set<unsigned int> &gblVariables, std::list<unsigned int> &funParams, std::unordered_set<unsigned int> & vars)
{
   //I simply have to go over all the vertices and get the used variables;
   //the variables which have to be declared are all those variables but
   //the globals ones
   VertexIterator v, vEnd;
   for(boost::tie(v, vEnd) = boost::vertices(*inGraph); v != vEnd; v++)
   {
      const CustomSet<unsigned int> vars_temp = inGraph->CGetOpNodeInfo(*v)->cited_variables;
      vars.insert(vars_temp.begin(), vars_temp.end());
   }

   //I have to take out the variables global to the whole program and the function parameters
   std::set<unsigned int> varsTemp;
   std::unordered_set<unsigned int>::const_iterator it, it_end = gblVariables.end();
   for(it = gblVariables.begin(); it != it_end; it++)
   {
      vars.erase(*it);
   }
   std::list<unsigned int>::const_iterator it2, it2_end = funParams.end();
   for(it2 = funParams.begin(); it2 != it2_end; it2++)
   {
      vars.erase(*it2);
   }
}

const DesignFlowStepFactoryConstRef CBackend::CGetDesignFlowStepFactory() const
{
   return design_flow_manager.lock()->CGetDesignFlowStepFactory("CBackend");
}

const std::string CBackend::GetSignature() const
{
   return "CBackend::Sequential";
}

const std::string CBackend::GetName() const
{
   return GetSignature();
}

void CBackend::ComputeRelationships(DesignFlowStepSet &, const DesignFlowStep::RelationshipType)
{
}
