/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEI
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file hls_c_writer.hpp
 *
 * @author Marco Lattuada <marco.lattuada@polimi.it>
 * $Revision: $
 * $Date: $
 * Last modified by $Author: $
 *
*/

#ifndef HLS_C_WRITER_HPP
#define HLS_C_WRITER_HPP

///Superclass include
#include "c_writer.hpp"

///utility include
#include "refcount.hpp"

CONSTREF_FORWARD_DECL(HLSCBackendInformation);

class HLSCWriter : public CWriter
{
   protected:
      ///Backend information
      const HLSCBackendInformationConstRef hls_c_backend_information;

      /// store the address value of the parameters
      std::map<unsigned int, unsigned int> param_address;

      /**
       * Print the binary representation of a number
       */
      std::string convert_in_binary(const BehavioralHelperConstRef behavioral_helper, unsigned int base_type, const std::string &C_value, unsigned int precision);

      bool is_all_8zeros(std::string & str);

   public:
      /**
       * Constructor of the class
       * @param hls_c_backend_information is the information about backend
       * @param AppM is the manager of the application
       * @param instruction_writer is the instruction writer to use to print the single instruction
       * @param indented_output_stream is the stream where code has to be printed
       * @param Param is the set of parameters
       * @param verbose tells if annotations
       */
      HLSCWriter(const HLSCBackendInformationConstRef hls_c_backend_information, const application_managerConstRef _AppM, const InstructionWriterRef instruction_writer, const IndentedOutputStreamRef indented_output_stream, const ParameterConstRef Param, bool verbose = true);

      /**
       * Destructor
       */
      virtual ~HLSCWriter();

      /**
       * Writes the final C file
       * @param file_name is the name of the file to be generated
       */
      virtual void WriteFile(const std::string & file_name);

      /**
       * Writes the header of the file
       */
      virtual void WriteHeader();
};
#endif
