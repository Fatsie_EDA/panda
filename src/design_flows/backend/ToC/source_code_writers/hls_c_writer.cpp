/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEI
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file hls_c_writer.cpp
 *
 * @author Marco Lattuada <marco.lattuada@polimi.it>
 * $Revision: $
 * $Date: $
 * Last modified by $Author: $
 *
*/

///Header include
#include "hls_c_writer.hpp"

///behavior include
#include "behavioral_helper.hpp"
#include "call_graph_manager.hpp"
#include "function_behavior.hpp"
#include "var_pp_functor.hpp"

///design_flows/backend/ToC includes
#include "hls_c_backend_information.hpp"

///HLS include
#include "hls_manager.hpp"

///HLS/memory include
#include "memory.hpp"

///HLS/simulation include
#include "HLStestbench.hpp"

///technology/physical_library include
#include "technology_builtin.hpp"

///tree include
#include "tree_helper.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"

///utility include
#include "indented_output_stream.hpp"

REF_FORWARD_DECL(memory_symbol);

HLSCWriter::HLSCWriter(const HLSCBackendInformationConstRef _hls_c_backend_information, const application_managerConstRef _AppM, const InstructionWriterRef _instruction_writer, const IndentedOutputStreamRef _indented_output_stream, const ParameterConstRef _parameters, bool _verbose) :
   CWriter(_AppM, _instruction_writer, _indented_output_stream, _parameters, _verbose),
   hls_c_backend_information(_hls_c_backend_information)
{}

HLSCWriter::~HLSCWriter()
{}

void HLSCWriter::WriteHeader()
{
   indented_output_stream->Append("//Original pretty printed C description\n\n");
   CWriter::WriteHeader();
}

void HLSCWriter::WriteFile(const std::string & file_name)
{
   indented_output_stream->Append("//Testbench part for High-Level Synthesis\n\n");
   indented_output_stream->Append("#include <stdio.h>\n\n");

   indented_output_stream->Append("//global variable used to store the output file\n");
   indented_output_stream->Append("FILE * global_testvector_file;\n");
   indented_output_stream->Append("//variable used to detect a standard end of the main (exit has not been called)\n");
   indented_output_stream->Append("unsigned int __standard_exit;\n");
   indented_output_stream->Append("//definition of __exit function\n\n");
   indented_output_stream->Append("void __exit(void)\n");
   indented_output_stream->Append("{\n");
   indented_output_stream->Append("if(!__standard_exit)\n");
   indented_output_stream->Append("{\n");
   indented_output_stream->Append("fprintf(global_testvector_file, \"//expected value for return value\\n\");\n");
   indented_output_stream->Append("fprintf(global_testvector_file, \"o00000000000000000000000000000000\\n\");\n");
   indented_output_stream->Append("fprintf(global_testvector_file, \"e\\n\");\n");
   indented_output_stream->Append("}\n");
   indented_output_stream->Append("}\n\n");

   indented_output_stream->Append("void _Dec2Bin_(FILE * testvector_file, long long int num, unsigned int precision)\n");
   indented_output_stream->Append("{\n");
   indented_output_stream->Append("int i;\n");
   indented_output_stream->Append("unsigned long long int ull_value = (unsigned long long int) num;\n");
   indented_output_stream->Append("for(i=0;i<precision;++i)\n");
   indented_output_stream->Append("fprintf(testvector_file, \"%c\", (((1LLU << (precision-i-1)) & ull_value) ? '1' : '0'));\n");
   indented_output_stream->Append("}\n\n");

   indented_output_stream->Append("void _Ptd2Bin_(FILE * testvector_file, unsigned char * num, unsigned int precision)\n");
   indented_output_stream->Append("{\n");
   indented_output_stream->Append("int i,j;\n");
   indented_output_stream->Append("char value;\n");
   indented_output_stream->Append("for(i=0; i<precision; i=i+8)\n");
   indented_output_stream->Append("{\n");
   indented_output_stream->Append("value = *(num + precision/8-i/8-1);\n");
   indented_output_stream->Append("for(j=0;j<8;++j)\n");
   indented_output_stream->Append("fprintf(testvector_file, \"%c\", (((1LLU << (8-j-1)) & value) ? '1' : '0'));\n");
   indented_output_stream->Append("}\n\n");
   indented_output_stream->Append("}\n\n");

   indented_output_stream->Append("int main()\n{\n\n");
   indented_output_stream->Append("FILE * testvector_file;\n");
   indented_output_stream->Append("unsigned int __testbench_index,__testbench_index0;\n");
   indented_output_stream->Append("atexit(__exit);\n");
   indented_output_stream->Append("__standard_exit=0;\n");
   indented_output_stream->Append("global_testvector_file=testvector_file=fopen(\"" +  hls_c_backend_information->input_file + "\", \"a\");\n");
   unsigned int base_address = GetPointer<const HLS_manager>(AppM)->base_address;
   indented_output_stream->Append("fprintf(testvector_file, \"//base address " + STR(base_address) + "\\n\");\n");
   std::string trimmed_value;
   for(unsigned int ind = 0; ind < 32; ind++)
      trimmed_value = trimmed_value + (((1LLU << (31-ind)) & base_address) ? '1' : '0');
   indented_output_stream->Append("fprintf(testvector_file, \"b" + trimmed_value + "\\n\");\n");
   const tree_managerRef TreeM = AppM->get_tree_manager();
   const unsigned int function_id = AppM->CGetCallGraphManager()->GetRootFunction();
   const BehavioralHelperConstRef behavioral_helper = AppM->CGetFunctionBehavior(function_id)->CGetBehavioralHelper();
   const std::map<unsigned int, memory_symbolRef>& mem_vars = GetPointer<const HLS_manager>(AppM)->Rmem->get_ext_memory_variables();
   std::map<unsigned int, unsigned int> address;
   for(std::map<unsigned int, memory_symbolRef>::const_iterator m = mem_vars.begin(); m != mem_vars.end(); m++)
   {
      address[GetPointer<const HLS_manager>(AppM)->Rmem->get_external_base_address(m->first)] = m->first;
   }

   std::list<unsigned int> mem;
   for(std::map<unsigned int, unsigned int>::const_iterator ma = address.begin(); ma != address.end(); ma++)
   {
      //std::cerr << "address = " << ma->first << std::endl;
      //std::cerr << "  variable = " << ma->second << std::endl;
      //std::cerr << "  variable = " << behavioral_helper->PrintVariable(ma->second) << std::endl;
      //std::cerr << "  size = " << tree_helper::size(TreeM, ma->second) << std::endl;
      //std::cerr << "  elements = " << tree_helper::size(TreeM, ma->second)/8 << std::endl;
      //std::cerr << "  end_address = " << ma->first+(tree_helper::size(TreeM, ma->second)/8) << std::endl;
      mem.push_back(ma->second);
   }
   const std::list<unsigned int>& parameters = behavioral_helper->get_parameters();
   for(std::list<unsigned int>::const_iterator p = parameters.begin(); p != parameters.end(); p++)
   {
      if (behavioral_helper->is_a_pointer(*p) && mem_vars.find(*p) == mem_vars.end())
      {
         mem.push_back(*p);
      }
   }
   size_t byte_allocated, actual_byte;
   for (std::list<unsigned int>::iterator l = mem.begin(); l != mem.end(); l++)
   {
      std::string param = behavioral_helper->PrintVariable(*l);
      if(param[0] == '"')
         param = "@"+STR(*l);

      std::vector<std::string> splitted;
      std::string test_v;
      bool is_memory = false;
      unsigned int base_type = tree_helper::get_type_index(TreeM, *l);
      byte_allocated = 0;
      actual_byte = tree_helper::size(TreeM, *l)/8;
      if (mem_vars.find(*l) != mem_vars.end() && std::find(parameters.begin(), parameters.end(), *l) == parameters.end())
      {
         is_memory = true;
         test_v = HLStestbench::print_var_init(TreeM, *l, GetPointer<const HLS_manager>(AppM)->Rmem);
         indented_output_stream->Append("fprintf(testvector_file, \"//memory initialization for variable " + param + "\\n\");\n");
      }
      else if(hls_c_backend_information->test_vector.find(param) != hls_c_backend_information->test_vector.end())
         test_v = hls_c_backend_information->test_vector.find(param)->second;
      else
         test_v ="0";
      if(!hls_c_backend_information->is_first_vector && is_memory)
         continue;//memory has been already initialized
      unsigned int base_type_size=0;
      boost::algorithm::split(splitted, test_v , boost::algorithm::is_any_of(","));
      if (tree_helper::is_a_pointer(TreeM, *l) && !is_memory)
      {
         tree_nodeRef pt_node = TreeM->get_tree_node_const(base_type);
         THROW_ASSERT(pt_node->get_kind() == pointer_type_K, "A pointer type is expected");
         unsigned int ptd_base_type = GET_INDEX_NODE(GetPointer<pointer_type>(pt_node)->ptd);
         if(behavioral_helper->is_an_array(ptd_base_type))
         {
            std::vector<unsigned int> dims;
            tree_helper::get_array_dimensions(TM, ptd_base_type, dims, base_type_size);
            base_type_size = base_type_size/8;
         }
         else
            base_type_size = tree_helper::size(TreeM, ptd_base_type)/8;
         unsigned int reserve_space = (static_cast<unsigned int>(splitted.size()))*base_type_size;
         actual_byte = reserve_space;
         /// FIXME in case the following tests require more space nothing work
         if(param_address.find(*l) == param_address.end())
         {
            param_address[*l] = GetPointer<const HLS_manager>(AppM)->Rmem->get_memory_address();
            GetPointer<const HLS_manager>(AppM)->Rmem->reserve_space(reserve_space);
         }
      }
      else if(!is_memory)
            THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "not yet supported case");
      else
         base_type_size = static_cast<unsigned int>(splitted[0].size()/8);
      /// check for regularity
      bool all_equal = splitted.size()>1;
      for(unsigned int i = 1; i < splitted.size(); i++)
         if(splitted[i] != splitted[0])
            all_equal = false;
      if(all_equal)
      {
         indented_output_stream->Append("for(__testbench_index0=0; __testbench_index0 < " + STR(splitted.size()) + "; ++__testbench_index0)\n{\n");
         byte_allocated += (base_type_size)*(splitted.size()-1);
      }
      for(unsigned int i = 0; i < splitted.size() && (!all_equal || i==0); i++)
      {
         THROW_ASSERT(splitted[i] != "", "Not well formed test vector: " + test_v);

         std::string initial_string = splitted[i];
         std::string binary_string;
         unsigned int size_of_data = static_cast<unsigned int>(initial_string.size());
         if (!is_memory)
         {
            std::string init_value_copy = initial_string;
            boost::replace_all(init_value_copy, "\\", "\\\\");
            indented_output_stream->Append("fprintf(testvector_file, \"//memory initialization for variable " + param + " value: "  + init_value_copy + "\\n\");\n");
            tree_nodeRef pt_node = TreeM->get_tree_node_const(base_type);
            THROW_ASSERT(pt_node->get_kind() == pointer_type_K, "A pointer type is expected");
            unsigned int ptd_base_type = GET_INDEX_NODE(GetPointer<pointer_type>(pt_node)->ptd);
            size_of_data = tree_helper::size(TreeM, ptd_base_type);
            if(behavioral_helper->is_a_struct(ptd_base_type))
            {
               std::vector<std::string> splitted_fields;
               boost::algorithm::split(splitted_fields, initial_string, boost::algorithm::is_any_of("|"));
               const std::list<tree_nodeConstRef> fields = tree_helper::CGetFieldTypes(TreeM->CGetTreeNode(ptd_base_type));
               size_t n_values = splitted_fields.size();
               unsigned int index=0;
               unsigned int field_size;
               for(std::list<tree_nodeConstRef>::const_iterator it=fields.begin(); it != fields.end(); ++it, ++index)
               {
                  const tree_nodeConstRef field_type = *it;
                  field_size = tree_helper::Size(field_type);
                  if(index < n_values)
                     binary_string = convert_in_binary(behavioral_helper, field_type->index, splitted_fields[index], field_size);
                  else
                     binary_string = convert_in_binary(behavioral_helper, field_type->index, "0", field_size);
                  if(is_all_8zeros(binary_string))
                  {
                     indented_output_stream->Append("for(__testbench_index=0; __testbench_index < " + STR(binary_string.size()/8) + "; ++__testbench_index)\n   fprintf(testvector_file, \"m00000000\\n\");\n");
                     byte_allocated += binary_string.size()/8;
                  }
                  else
                  {
                     for(unsigned int base_index = 0; base_index < field_size; base_index = base_index + 8)
                     {
                        indented_output_stream->Append("fprintf(testvector_file, \"m" + binary_string.substr(field_size-8-base_index, 8) + "\\n\");\n");
                        ++byte_allocated;
                     }
                  }
               }
            }
            else if(behavioral_helper->is_an_union(ptd_base_type))
            {
               unsigned int max_bitsize_field=0;
               tree_helper::accessed_greatest_bitsize(TreeM, GET_NODE(GetPointer<pointer_type>(pt_node)->ptd), ptd_base_type, max_bitsize_field);
               binary_string = convert_in_binary(behavioral_helper, 0, "0", max_bitsize_field);
               if(is_all_8zeros(binary_string))
               {
                  indented_output_stream->Append("for(__testbench_index=0; __testbench_index < " + STR(binary_string.size()/8) + "; ++__testbench_index)\n   fprintf(testvector_file, \"m00000000\\n\");\n");
                  byte_allocated += binary_string.size()/8;
               }
               else
               {
                  for(unsigned int base_index = 0; base_index < max_bitsize_field; base_index = base_index + 8)
                  {
                     indented_output_stream->Append("fprintf(testvector_file, \"m" + binary_string.substr(max_bitsize_field-8-base_index, 8) + "\\n\");\n");
                     ++byte_allocated;
                  }
               }
            }
            else if(behavioral_helper->is_an_array(ptd_base_type))
            {
               unsigned int elmts_type = behavioral_helper->GetElements(ptd_base_type);
               while(behavioral_helper->is_an_array(elmts_type))
                  elmts_type = behavioral_helper->GetElements(elmts_type);
               std::vector<unsigned int> dims;
               tree_helper::get_array_dimensions(TM, ptd_base_type, dims, size_of_data);
               unsigned int num_elements=1;
               if(splitted.size() == 1)
               {
                  for(std::vector<unsigned int>::const_iterator it = dims.begin(); it != dims.end(); ++it)
                     num_elements *= *it;
               }

               indented_output_stream->Append("for(__testbench_index0=0; __testbench_index0 < " + STR(num_elements) + "; ++__testbench_index0)\n{\n");
               binary_string =  convert_in_binary(behavioral_helper, elmts_type, initial_string, size_of_data);
               if(is_all_8zeros(binary_string))
               {
                  indented_output_stream->Append("for(__testbench_index=0; __testbench_index < " + STR(binary_string.size()/8) + "; ++__testbench_index)\n   fprintf(testvector_file, \"m00000000\\n\");\n");
                  byte_allocated += binary_string.size()/8;
               }
               else
               {
                  for(unsigned int base_index = 0; base_index < size_of_data; base_index = base_index + 8)
                  {
                     indented_output_stream->Append("fprintf(testvector_file, \"m" + binary_string.substr(size_of_data-8-base_index, 8) + "\\n\");\n");
                     ++byte_allocated;
                  }
               }
               indented_output_stream->Append("}\n");
            }
            else
            {
               binary_string =  convert_in_binary(behavioral_helper, ptd_base_type, initial_string, size_of_data);
               if(is_all_8zeros(binary_string))
               {
                  indented_output_stream->Append("for(__testbench_index=0; __testbench_index < " + STR(binary_string.size()/8) + "; ++__testbench_index)\n   fprintf(testvector_file, \"m00000000\\n\");\n");
                  byte_allocated += binary_string.size()/8;
               }
               else
               {
                  for(unsigned int base_index = 0; base_index < size_of_data; base_index = base_index + 8)
                  {
                     indented_output_stream->Append("fprintf(testvector_file, \"m" + binary_string.substr(size_of_data-8-base_index, 8) + "\\n\");\n");
                     ++byte_allocated;
                  }
               }
            }
         }
         else
         {
            if(is_all_8zeros(initial_string))
            {
               indented_output_stream->Append("for(__testbench_index=0; __testbench_index < " + STR(initial_string.size()/8) + "; ++__testbench_index)\n   fprintf(testvector_file, \"m00000000\\n\");\n");
               byte_allocated += initial_string.size()/8;
            }
            else
            {
               for(unsigned int base_index = 0; base_index < size_of_data; base_index = base_index + 8)
               {
                  indented_output_stream->Append("fprintf(testvector_file, \"m" + initial_string.substr(size_of_data-8-base_index, 8) + "\\n\");\n");
                  ++byte_allocated;
               }
            }
         }
      }
      if(all_equal)
         indented_output_stream->Append("}\n");

      if(byte_allocated < actual_byte)
      {
          indented_output_stream->Append("for(__testbench_index=0; __testbench_index < " + STR(actual_byte-byte_allocated) + "; ++__testbench_index)\n   fprintf(testvector_file, \"m00000000\\n\");\n");
          byte_allocated = actual_byte;
      }

      if(byte_allocated != actual_byte)
         THROW_ERROR("mismatch between allocated bytes and object size: All(" + STR(byte_allocated) +"), ObjSize(" + STR(actual_byte) + ") for " + STR(*l));
      std::list<unsigned int>::const_iterator l_next;
      l_next = l;
      ++l_next;
      unsigned int object_offset = 0;
      /// check if next is in memory or is a parameter
      if(l_next != mem.end() && mem_vars.find(*l_next) != mem_vars.end() && mem_vars.find(*l) != mem_vars.end())
         object_offset = GetPointer<const HLS_manager>(AppM)->Rmem->get_base_address(*l_next)-GetPointer<const HLS_manager>(AppM)->Rmem->get_base_address(*l);
      else if(mem_vars.find(*l) != mem_vars.end() && (l_next == mem.end() || param_address.find(*l_next) == param_address.end()))
         object_offset = GetPointer<const HLS_manager>(AppM)->Rmem->get_memory_address()-GetPointer<const HLS_manager>(AppM)->Rmem->get_base_address(*l);
      else if(l_next != mem.end() && mem_vars.find(*l) != mem_vars.end() && param_address.find(*l_next) != param_address.end())
         object_offset = param_address.find(*l_next)->second-GetPointer<const HLS_manager>(AppM)->Rmem->get_base_address(*l);
      else if(l_next != mem.end() && param_address.find(*l) != param_address.end() && param_address.find(*l_next) != param_address.end())
         object_offset = param_address.find(*l_next)->second-param_address.find(*l)->second;
      else if(param_address.find(*l) != param_address.end())
         object_offset = GetPointer<const HLS_manager>(AppM)->Rmem->get_memory_address()-param_address.find(*l)->second;
      else
         THROW_ERROR("unexpected pattern");
      
      THROW_ASSERT(object_offset >= actual_byte, "more allocated memory than expected");
      if(object_offset > actual_byte)
      {
         indented_output_stream->Append("for(__testbench_index=0; __testbench_index < " + STR(object_offset-byte_allocated) + "; ++__testbench_index)\n   fprintf(testvector_file, \"m00000000\\n\");\n");
      }
   }
   //std::cerr << "starting declaration of variables" << std::endl;
   indented_output_stream->Append("//base address: " + STR(base_address) + "\n");
   indented_output_stream->Append("//declaration of variables\n");
   for(std::list<unsigned int>::const_iterator p = parameters.begin(); p != parameters.end(); p++)
   {
      unsigned int type_id = behavioral_helper->get_type(*p);
      std::string type = behavioral_helper->print_type(type_id);
      std::string param = behavioral_helper->PrintVariable(*p);
      if (behavioral_helper->is_a_pointer(*p))
      {
         unsigned int base_type = tree_helper::get_type_index(TreeM, *p);
         tree_nodeRef pt_node = TreeM->get_tree_node_const(base_type);
         THROW_ASSERT(pt_node->get_kind() == pointer_type_K, "A pointer type is expected");
         base_type = GET_INDEX_NODE(GetPointer<pointer_type>(pt_node)->ptd);
         unsigned int base_type_size= tree_helper::size(TreeM, base_type)/8;

         std::vector<std::string> splitted;
         std::string test_v;
         if(hls_c_backend_information->test_vector.find(param) != hls_c_backend_information->test_vector.end())
            test_v = hls_c_backend_information->test_vector.find(param)->second;
         else
            test_v ="0";
         boost::algorithm::split(splitted, test_v , boost::algorithm::is_any_of(","));

         var_pp_functorRef var_functor = var_pp_functorRef(new std_var_pp_functor(behavioral_helper));
         indented_output_stream->Append(tree_helper::print_type(TM, type_id, false, false,  false,  *p, var_functor) + "=("+ type +")malloc(" + boost::lexical_cast<std::string>(base_type_size*splitted.size()) + ");\n");

         for(unsigned int i = 0; i < splitted.size(); i++)
         {
            if(behavioral_helper->is_a_struct(base_type) || behavioral_helper->is_an_union(base_type))
            {
               std::vector<std::string> splitted_fields;
               std::string field_value = splitted[i];
               boost::algorithm::split(splitted_fields, field_value, boost::algorithm::is_any_of("|"));
               const std::list<tree_nodeConstRef> fields = tree_helper::CGetFieldTypes(TreeM->CGetTreeNode(base_type));
               size_t n_values = splitted_fields.size();
               unsigned int index=0;
               for(std::list<tree_nodeConstRef>::const_iterator it=fields.begin(); it != fields.end(); ++it, ++index)
               {
                  if(index < n_values)
                     indented_output_stream->Append(param + "["+boost::lexical_cast<std::string>(i)+ "]." + behavioral_helper->PrintVariable(tree_helper::get_field_idx(TreeM, base_type, index))+" = "+splitted_fields[index]+";\n");
                  else
                     indented_output_stream->Append(param + "["+boost::lexical_cast<std::string>(i)+ "]." + behavioral_helper->PrintVariable(tree_helper::get_field_idx(TreeM, base_type, index))+" = 0;\n");
               }
            }
            else if(behavioral_helper->is_an_array(base_type))
            {
               unsigned int size_of_data;
               std::vector<unsigned int> dims;
               tree_helper::get_array_dimensions(TM, base_type, dims, size_of_data);
               unsigned int num_elements=1;
               for(std::vector<unsigned int>::const_iterator it = dims.begin(); it != dims.end(); ++it)
                  num_elements *= *it;
               if(splitted.size() == 1)
               {
                  for (unsigned int l = 0; l < num_elements; l++)
                  {
                     indented_output_stream->Append("(*" +param + ")["+boost::lexical_cast<std::string>(l)+ "] = " + splitted[i] + ";\n");
                  }
               }
               else
               {
                  unsigned int elmts_type = behavioral_helper->GetElements(base_type);
                  while(behavioral_helper->is_an_array(elmts_type))
                     elmts_type = behavioral_helper->GetElements(elmts_type);
                  indented_output_stream->Append("(*(((" + behavioral_helper->print_type(elmts_type) + "*)" +param + ")+ "+STR(i)+")) = " + splitted[i] + ";\n");
               }
            }
            else
               indented_output_stream->Append(param + "["+boost::lexical_cast<std::string>(i)+ "] = " + splitted[i] + ";\n");
         }
         std::string memory_addr;
         THROW_ASSERT(param_address.find(*p) != param_address.end(), "parameter does not have an address");
         memory_addr = boost::lexical_cast<std::string>(param_address[*p]);
         indented_output_stream->Append("fprintf(testvector_file, \"//parameter: " + param + " value: "  + memory_addr + "\\n\");\n");
         indented_output_stream->Append("fprintf(testvector_file, \"p" + convert_in_binary(behavioral_helper, 0, memory_addr, 32) + "\\n\");\n");
      }
      else
      {
         if(hls_c_backend_information->test_vector.find(param) == hls_c_backend_information->test_vector.end())
            THROW_ERROR("Value of " + param + " is missing in test vector");
         indented_output_stream->Append(type + " " + param + " = " + hls_c_backend_information->test_vector.find(param)->second + ";\n");
         indented_output_stream->Append("fprintf(testvector_file, \"//parameter: " + param + " value: "  + hls_c_backend_information->test_vector.find(param)->second + "\\n\");\n");
         indented_output_stream->Append("fprintf(testvector_file, \"p" + convert_in_binary(behavioral_helper, type_id, hls_c_backend_information->test_vector.find(param)->second, tree_helper::size(TreeM, type_id)) + "\\n\");\n");
      }
   }

   indented_output_stream->Append("//function call\n");
   const unsigned int return_type_index = behavioral_helper->GetFunctionReturnType(behavioral_helper->get_function_index());
   if(return_type_index)
   {
      std::string ret_type = behavioral_helper->print_type(return_type_index);
      indented_output_stream->Append(ret_type + " " + std::string(RETURN_PORT_NAME) + " = ");
   }
   std::string function_name = behavioral_helper->get_function_name();
   if(function_name == "main")
      function_name = "_" + function_name;
   indented_output_stream->Append(function_name + "(");
   bool is_first = true;
   for(std::list<unsigned int>::const_iterator p = parameters.begin(); p != parameters.end(); p++)
   {
      if (!is_first) indented_output_stream->Append(", ");
      else is_first = false;
      std::string param = behavioral_helper->PrintVariable(*p);
      indented_output_stream->Append(param);
   }
   indented_output_stream->Append(");\n");
   for(std::list<unsigned int>::const_iterator p = parameters.begin(); p != parameters.end(); p++)
   {
      std::string param = behavioral_helper->PrintVariable(*p);
      if (behavioral_helper->is_a_pointer(*p))
      {
         std::vector<std::string> splitted;
         std::string test_v;
         if(hls_c_backend_information->test_vector.find(param) != hls_c_backend_information->test_vector.end())
            test_v = hls_c_backend_information->test_vector.find(param)->second;
         else
            test_v ="0";
         boost::algorithm::split(splitted, test_v , boost::algorithm::is_any_of(","));
         unsigned int base_type = tree_helper::get_type_index(TreeM, *p);
         tree_nodeRef pt_node = TreeM->get_tree_node_const(base_type);
         THROW_ASSERT(pt_node->get_kind() == pointer_type_K, "A pointer type is expected");
         base_type = GET_INDEX_NODE(GetPointer<pointer_type>(pt_node)->ptd);
         unsigned int base_type_bitsize= tree_helper::size(TreeM, base_type);
         for(unsigned int i = 0; i < splitted.size(); i++)
         {
            if(behavioral_helper->is_real(base_type))
            {
               indented_output_stream->Append("fprintf(testvector_file, \"//expected value for output " + param + "[" + boost::lexical_cast<std::string>(i) + "]: %" + (behavioral_helper->is_real(base_type) ? std::string("g") : std::string("d")) + "\\n\", " + param + "[" + boost::lexical_cast<std::string>(i) + "]);\n");
               indented_output_stream->Append("fprintf(testvector_file, \"o\");\n");
               indented_output_stream->Append("_Ptd2Bin_(testvector_file, (unsigned char*)&" + param + "[" + boost::lexical_cast<std::string>(i) + "], "+ boost::lexical_cast<std::string>(base_type_bitsize) + ");\n");
            }
            else if(behavioral_helper->is_a_struct(base_type) || behavioral_helper->is_an_union(base_type))
            {
               indented_output_stream->Append("fprintf(testvector_file, \"//expected value for output " + param + "[" + boost::lexical_cast<std::string>(i) + "]: %" + (behavioral_helper->is_real(base_type) ? std::string("g") : std::string("d")) + "\\n\", " + param + "[" + boost::lexical_cast<std::string>(i) + "]);\n");
               indented_output_stream->Append("fprintf(testvector_file, \"o\");\n");
               indented_output_stream->Append("_Ptd2Bin_(testvector_file, (unsigned char*)&" + param + "[" + boost::lexical_cast<std::string>(i) + "], "+ boost::lexical_cast<std::string>(base_type_bitsize) + ");\n");
            }
            else if(behavioral_helper->is_an_array(base_type))
            {
               unsigned int size_of_data;
               std::vector<unsigned int> dims;
               tree_helper::get_array_dimensions(TM, base_type, dims, size_of_data);
               unsigned int num_elements=1;
               for(std::vector<unsigned int>::const_iterator it = dims.begin(); it != dims.end(); ++it)
                  num_elements *= *it;
               unsigned int elmts_type = behavioral_helper->GetElements(base_type);
               while(behavioral_helper->is_an_array(elmts_type))
                  elmts_type = behavioral_helper->GetElements(elmts_type);
               indented_output_stream->Append("fprintf(testvector_file, \"//expected value for output (*(((" + behavioral_helper->print_type(elmts_type) + "*)" +param + ")+ "+STR(i)+")): %" + (behavioral_helper->is_real(elmts_type) ? std::string("g") : std::string("d")) + "\\n\", (*(((" + behavioral_helper->print_type(elmts_type) + "*)" +param + ")+ "+STR(i)+")));\n");
               indented_output_stream->Append("fprintf(testvector_file, \"o\");\n");
               if(splitted.size() == 1)
               {
                  for (unsigned int l = 0; l < num_elements; l++)
                  {
                     if(behavioral_helper->is_real(elmts_type))
                        indented_output_stream->Append("_Ptd2Bin_(testvector_file, (unsigned char*)&(*" +param + ")["+boost::lexical_cast<std::string>(l)+ "], "+ boost::lexical_cast<std::string>(size_of_data) + ");\n");
                     else
                        indented_output_stream->Append("_Dec2Bin_(testvector_file, (*" +param + ")["+boost::lexical_cast<std::string>(l)+ "], "+ boost::lexical_cast<std::string>(size_of_data) + ");\n");
                  }
               }
               else
               {
                  if(behavioral_helper->is_real(elmts_type))
                     indented_output_stream->Append("_Ptd2Bin_(testvector_file, (unsigned char*)&(*(((" + behavioral_helper->print_type(elmts_type) + "*)" +param + ")+ "+STR(i)+")), "+ boost::lexical_cast<std::string>(size_of_data) + ");\n");
                  else
                     indented_output_stream->Append("_Dec2Bin_(testvector_file, (*(((" + behavioral_helper->print_type(elmts_type) + "*)" +param + ")+ "+STR(i)+")), "+ boost::lexical_cast<std::string>(size_of_data) + ");\n");
               }
            }
            else
            {
               indented_output_stream->Append("fprintf(testvector_file, \"//expected value for output " + param + "[" + boost::lexical_cast<std::string>(i) + "]: %" + (behavioral_helper->is_real(base_type) ? std::string("g") : std::string("d")) + "\\n\", " + param + "[" + boost::lexical_cast<std::string>(i) + "]);\n");
               indented_output_stream->Append("fprintf(testvector_file, \"o\");\n");
               indented_output_stream->Append("_Dec2Bin_(testvector_file, " + param + "[" + boost::lexical_cast<std::string>(i) + "], "+ boost::lexical_cast<std::string>(base_type_bitsize) + ");\n");
            }
            indented_output_stream->Append("fprintf(testvector_file, \"\\n\");\n");
         }
         indented_output_stream->Append("fprintf(testvector_file, \"e\\n\");\n");
      }
   }
   if (return_type_index)
   {
      indented_output_stream->Append("fprintf(testvector_file, \"//expected value for return value\\n\");\n");
      indented_output_stream->Append("fprintf(testvector_file, \"o\");\n");
      unsigned int base_type_bitsize= tree_helper::size(TreeM, return_type_index);
      if(behavioral_helper->is_real(return_type_index))
      {
         indented_output_stream->Append("_Ptd2Bin_(testvector_file, (unsigned char*)&" + std::string(RETURN_PORT_NAME) + ", "+ boost::lexical_cast<std::string>(base_type_bitsize) + ");\n");
      }
      else if(behavioral_helper->is_a_struct(return_type_index) || behavioral_helper->is_an_union(return_type_index))
      {
         indented_output_stream->Append("_Ptd2Bin_(testvector_file, (unsigned char*)&" + std::string(RETURN_PORT_NAME) + ", "+ boost::lexical_cast<std::string>(base_type_bitsize) + ");\n");
      }
      else
         indented_output_stream->Append("_Dec2Bin_(testvector_file, " + std::string(RETURN_PORT_NAME) + "," + boost::lexical_cast<std::string>(base_type_bitsize) + ");\n");
      indented_output_stream->Append("fprintf(testvector_file, \"\\n\");\n");
      indented_output_stream->Append("fprintf(testvector_file, \"e\\n\");\n");
   }
   indented_output_stream->Append("__standard_exit=1;\n");
   indented_output_stream->Append("exit(0);\n");
   indented_output_stream->Append("}\n");

   CWriter::WriteFile(file_name);
}

std::string HLSCWriter::convert_in_binary(const BehavioralHelperConstRef behavioral_helper, unsigned int base_type, const std::string &C_value, unsigned int precision)
{
   std::string trimmed_value;
   if(base_type && behavioral_helper->is_real(base_type))
   {
      trimmed_value = convert_fp_to_string(C_value, precision);
   }
   else
   {
      long long int ll_value;
      if(C_value[0] == '\'')
      {
         trimmed_value = C_value.substr(1);
         trimmed_value = trimmed_value.substr(0, trimmed_value.find('\''));
         //std::cout << "-" << trimmed_value << "-" << std::endl;
         if(trimmed_value[0] == '\\')
            ll_value = boost::lexical_cast<long long int>(trimmed_value.substr(1));
         else
            ll_value = boost::lexical_cast<char>(trimmed_value);
      }
      else
         ll_value = boost::lexical_cast<long long int>(C_value);
      unsigned long long int ull_value = static_cast<unsigned long long int>(ll_value);
      trimmed_value = "";
      for(unsigned int ind = 0; ind < precision; ind++)
         trimmed_value = trimmed_value + (((1LLU << (precision-ind-1)) & ull_value) ? '1' : '0');
   }
   return trimmed_value;
}

bool HLSCWriter::is_all_8zeros(std::string & str)
{
   size_t size = str.size();
   if(size%8 != 0 || size == 8) return false;
   for(size_t i = 0; i < size; ++i)
      if(str.at(i) != '0')
         return false;
   return true;
}

