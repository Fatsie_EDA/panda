/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file hls_c_backend_information.hpp
 * @brief Class to pass information to the hls backend
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision: $
 * $Date: $
 * Last modified by $Author: $
 *
*/

///Super class include
#include "c_backend_information.hpp"

///STL include
#include <map>

class HLSCBackendInformation : public CBackendInformation
{
   public:
      ///The file containing input
      const std::string input_file;

      ///The test vector
      const std::map<std::string, std::string> & test_vector;

      /// backend for the first vector or not
      const bool is_first_vector;

      /**
       * Constructor
       * @param input_file is the file containing input files
       * @param test_vector is the test vector
       */
      HLSCBackendInformation(const std::string input_file, const std::map<std::string, std::string> & test_vector, bool is_first_vector);

      /**
       * Destructor
       */
      ~HLSCBackendInformation();
};

