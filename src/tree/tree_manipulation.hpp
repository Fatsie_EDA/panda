/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file tree_manipulation.hpp
 * @brief Class defining some useful functions to create tree nodes and to manipulate the tree manager.
 *
 * This class defines some useful functions to create tree nodes and to manipulate the tree manager.
 *
 * @author ste <stefano.viazzi@gmail.com>
 * @warning This file is still in progress state.
 * @warning Do not use yet.
 *
*/
#ifndef TREE_MANIPULATION_HPP
#define TREE_MANIPULATION_HPP

///Utility include
#include "refcount.hpp"
#include "tree_common.hpp"

#include <map>
#include <vector>
#include <unordered_set>
#include <deque>

/**
 * @name forward declarations
 */
//@{
REF_FORWARD_DECL(tree_node);
REF_FORWARD_DECL(tree_manager);
REF_FORWARD_DECL(tree_manipulation);
REF_FORWARD_DECL(bloc);
//@}


#define ALGN_BIT_SIZE 64
#define ALGN_UNSIGNED_INT 32
#define ALGN_INT 32
#define ALGN_LONG_LONG_INT 64
#define ALGN_VOID 8
#define ALGN_BOOLEAN 8
#define ALGN_POINTER 32

#define PREC_BIT_SIZE 64
#define PREC_UNSIGNED_INT 32
#define PREC_INT 32
#define PREC_LONG_LONG_INT 64

#define SIZE_VALUE_BIT_SIZE 64
#define SIZE_VALUE_UNSIGNED_INT 32
#define SIZE_VALUE_LONG_LONG_INT 64
#define SIZE_VALUE_INT 32
#define SIZE_VALUE_BOOL 8
#define SIZE_VALUE_POINTER 32

#define MIN_VALUE_BIT_SIZE 0
#define MIN_VALUE_UNSIGNED_INT 0
#define MIN_VALUE_INT -2147483648LL
#define MIN_VALUE_LONG_LONG_INT 0

#define MAX_VALUE_BIT_SIZE -1
#define MAX_VALUE_UNSIGNED_INT -1
#define MAX_VALUE_INT 2147483647
#define MAX_VALUE_LONG_LONG_INT -1

/**
 * This class creates a layer to add nodes and to manipulate the tree_nodes manager.
*/
class tree_manipulation
{

   private:

      /// Tree Manager
      tree_managerRef TreeM;

      /// debug level.
      const int debug_level;

      /// store a unique id used during the creation of the label_decl associated with a gimple_goto.
      static unsigned int goto_label_unique_id;

      /// Set of already examined addr_expr used to avoid circular recursion.
      std::unordered_set<tree_nodeRef> already_visited;

      /// Set of already examined basic block used to avoid circular recursion.
      std::unordered_set<blocRef> bb_already_visited;

      /// List of examining block during update_block
      std::deque<blocRef> stack_block;

      /// List of examining statements during update_ssa
      std::deque<tree_nodeRef> stack_node;

      /// Map between enum kind and treeVocabularyTokenTypes
      std::map<kind,unsigned int> enum2tok;

   public:

      /**
       * This is the constructor of the tree_manipulation.
       * @param TreeM is the tree manager.
       * @param debug_level is the debug level
      */
      tree_manipulation(tree_managerRef & TreeM, int debug_level);

      /**
       * This is the destructor of the tree_manipulation.
      */
      ~tree_manipulation();



      ///EXPRESSION_TREE_NODES

      /**
       * Function used to create an unary expression.
       * @param  type is the type of the expression (tree_reindex).
       * @param  op is the operand of the unary expression (tree_reindex).
       * @param  srcp is the definition of the source position.
       * @param  operation_kind is the kind of unary expression to create (bit_not_expr_K, nop_expr_K,...).
       * @return the tree_reindex node of the operation created.
       */
      tree_nodeRef create_unary_operation(const tree_nodeRef & type, const tree_nodeRef & op, std::string srcp, kind operation_kind);

      /**
       * Function used to create a binary expression.
       * @param  type is the type of the expression (tree_reindex).
       * @param  op0 is the first operand of the binary expression (tree_reindex).
       * @param  op1 is the second operand of the binary expression (tree_reindex).
       * @param  srcp is the definition of the source position.
       * @param  operation_kind is the kind of binary expression to create (bit_and_expr_K, plus_expr_K,...).
       * @return the tree_reindex node of the operation created.
       */
      tree_nodeRef create_binary_operation(const tree_nodeRef & type, const tree_nodeRef & op0, const tree_nodeRef & op1, std::string srcp, kind operation_kind);

      /**
       * Function used to create a ternary expression.
       * @param  type is the type of the expression (tree_reindex).
       * @param  op0 is the first operand of the ternary expression (tree_reindex).
       * @param  op1 is the second operand of the ternary expression (tree_reindex).
       * @param  op2 is the third operand of the ternary expression (tree_reindex).
       * @param  srcp is the definition of the source position.
       * @param  operation_kind is the kind of ternary expression to create (component_ref_K, gimple_switch_K,...).
       * @return the tree_reindex node of the operation created.
       */
      tree_nodeRef create_ternary_operation(const tree_nodeRef & type, const tree_nodeRef & op0, const tree_nodeRef & op1, const tree_nodeRef & op2, std::string srcp, kind operation_kind);

      /**
       * Function used to create a quaternary expression.
       * @param  type is the type of the expression (tree_reindex).
       * @param  op0 is the first operand of the quaternary expression (tree_reindex).
       * @param  op1 is the second operand of the quaternary expression (tree_reindex).
       * @param  op2 is the third operand of the quaternary expression (tree_reindex).
       * @param  op3 is the fourth operand of the quaternary expression (tree_reindex).
       * @param  srcp is the definition of the source position.
       * @param  operation_kind is the kind of quaternary expression to create (array_range_ref_K, array_ref_K).
       * @return the tree_reindex node of the operation created.
       */
      tree_nodeRef create_quaternary_operation(const tree_nodeRef & type, const tree_nodeRef & op0, const tree_nodeRef & op1, const tree_nodeRef & op2, const tree_nodeRef & op3, std::string srcp, kind operation_kind);

      ///CONST_OBJ_TREE_NODES
      /** Function used to create a integer_cst node.
       * @param  type is the type of the node.
       * @param value is the value of the constant
       * @param  integer_cst_nid is the index node of the object to be created
       * @return the tree_reindex node of the integer_cst created.
       */
      tree_nodeRef CreateIntegerCst(const tree_nodeRef type, const long long int value, const unsigned int integer_cst_nid);

      ///IDENTIFIER_TREE_NODE
      /**
       * Function used to create an identifier node if it is not already present, otherwise it returns the one that is already in the tree manager.
       * @param  strg is the identifier string associated with the identifier_node.
       * @return the tree_reindex node of the identifier_node created.
       */
      tree_nodeRef create_identifier_node(const std::string strg);


      ///DECL_NODES

      /**
       * Function used to create a parm_decl.
       * @param  name is the name field containing an identifier_node used to represent a name.
       * @param  type is the type.
       * @param  scpe is the scope declaration.
       * @param  size is the size field holding the size of datum, in bits.
       * @param  argt is the type in which the argument is actually passed.
       * @param  smt_ann is the symbol_memory_tag annotation.
       * @param  init is the init field holding the value to initialize a variable to.
       * @param  srcp is the definition of the source position.
       * @param  algn is the field holding the alignment required for the datum, in bits.
       * @param  used is nonzero if the name is used in its scope
       * @param  register_flag means declared 'register'
       * @return the tree_reindex node of the parm_decl created.
       */
      tree_nodeRef create_parm_decl(const tree_nodeRef & name, const tree_nodeRef & type, const tree_nodeRef & scpe, const tree_nodeRef & size, const tree_nodeRef & argt,
               const tree_nodeRef & smt_ann, const tree_nodeRef & init, std::string srcp, unsigned int algn,int used, bool register_flag = false);

      /**
       * Function used to create a result_decl.
       * @param  name is the name field containing an identifier_node used to represent a name.
       * @param  type is the type.
       * @param  scpe is the scope declaration.
       * @param  size is the size field holding the size of datum, in bits.
       * @param  smt_ann is the symbol_memory_tag annotation (default empty).
       * @param  init is the init field holding the value to initialize a variable to.
       * @param  srcp is the definition of the source position.
       * @param  algn is the field holding the alignment required for the datum, in bits.
       * @param  artificial_flag used to indicate that this decl_node represents a compiler-generated entity (default false).
       * @return the tree_reindex node of the result_decl created.
       */
      tree_nodeRef create_result_decl(const tree_nodeRef & name, const tree_nodeRef & type, const tree_nodeRef & scpe, const tree_nodeRef & size, const tree_nodeRef &  smt_ann,
               const tree_nodeRef & init, std::string srcp,  unsigned int algn, bool artificial_flag = false);


      /**
       * Function used to create a var_decl.
       * @param  name is the name field containing an identifier_node used to represent a name.
       * @param  type is the type.
       * @param  scpe is the scope declaration.
       * @param  size is the size field holding the size of datum, in bits.
       * @param  smt_ann is the symbol_memory_tag annotation.
       * @param  init is the init field holding the value to initialize a variable to.
       * @param  srcp is the definition of the source position.
       * @param  algn is the field holding the alignment required for the datum, in bits.
       * @param  used is nonzero if the name is used in its scope
       * @param  artificial_flag used to indicate that this decl_node represents a compiler-generated entity (default false).
       * @param  use_tmpl indicates whether or not (and how) a template was expanded for this VAR_DECL (default -1).
       * @param  static_static_flag to manage C++ code with static member (default false).
       * @param  static_flag to manage standard static attribute (default false).
       * @param  extern_flag a variable can be extern (default false).
       * @param  init field holds the value to initialize a variable to (default false).
       * @return the tree_reindex node of the var_decl created.
       */
      tree_nodeRef create_var_decl(const tree_nodeRef & name, const tree_nodeRef & type, const tree_nodeRef & scpe, const tree_nodeRef & size, const tree_nodeRef & smt_ann,
               const tree_nodeRef & init, std::string srcp, unsigned int algn, int used, bool artificial_flag = false, int use_tmpl = -1, bool static_static_flag = false,
               bool extern_flag = false, bool static_flag = false, bool register_flag = false);

      ///TYPE_OBJ

      /**
       * Function that creates a void type if it is not already present, otherwise it returns the one that is already in the tree manager.
       * @return the tree_reindex node of the void type.
       */
      tree_nodeRef create_void_type();

      /**
       * Function that creates a boolean type if it is not already present, otherwise it returns the one that is already in the tree manager.
       * @return the tree_reindex node of the boolean type.
       */
      tree_nodeRef create_boolean_type();

      /**
       * Function that creates a integer type if it is not already present, otherwise it returns the one that is already in the tree manager.
       * @return the tree_reindex node of the integer type.
       */
      tree_nodeRef create_default_integer_type();

      /**
       * Function that creates a unsigned integer type if it is not already present, otherwise it returns the one that is already in the tree manager.
       * @return the tree_reindex node of the unsigned integer type.
       */
      tree_nodeRef create_default_unsigned_integer_type();

      /**
       * Function that creates a bit_size type if it is not already present, otherwise it returns the one that is already in the tree manager.
       * @return the tree_reindex node of the bit_size type.
       */
      tree_nodeRef create_bit_size_type();

      /**/
      tree_nodeRef create_size_type();

      /**
       * Function that creates a pointer type if it is not already present, otherwise it returns the one that is already in the tree manager.
       * @param  ptd type pointed by the pointer type (tree_reindex).
       * @return the tree_reindex node of the pointer type.
       */
      tree_nodeRef create_pointer_type(const tree_nodeRef & ptd);



      /// MISCELLANEOUS_OBJ_TREE_NODES


      ///SSA_NAME

      /**
       * Function used to create a ssa_name node.
       * @param  var is the variable being referenced.
       * @param  type is the type of the ssa_node
       * @param  volatile_flag flag set to true in case a ssa_name is never defined (default false).
       * @param  virtual_flag flag for virtual phi (default false).
       * @return the tree_reindex node of the ssa_name.
       *
       */
      tree_nodeRef create_ssa_name(const tree_nodeRef & var, const tree_nodeRef &type, bool volatile_flag = false, bool virtual_flag = false);

      /**
       * Function used to create a ssa_name node.
       * @param  var is the variable being referenced.
       * @param  ptr_info is the pointer info node.
       * @param  volatile_flag flag set to true in case a ssa_name is never defined (default false).
       * @param  virtual_flag flag for virtual phi (default false).
       * @return the tree_reindex node of the ssa_name.
       */
      tree_nodeRef create_ssa_name(const tree_nodeRef & var, const tree_nodeRef &type, const tree_nodeRef & ptr_info, bool volatile_flag = false, bool virtual_flag = false);


      ///GIMPLE_PHI

      /**
       * Function used to create a gimple_phi.
       * @param  ssa_res is the new ssa_name node created by the phi node.
       * @param  list_of_def_edge vector where each tuple contains the incoming reaching definition (ssa_name node) and the edge via which that definition is coming through.
       * @param  bb_index is the basic block index associated with the gimple phi
       * @param  virtual_flag flag to set if it is a virtual gimple_phi (default false).
       * @return the tree_reindex node of the gimple_phi.
       */
      tree_nodeRef create_phi_node(tree_nodeRef & ssa_res, const std::vector<std::pair<tree_nodeRef, unsigned int> > & list_of_def_edge, unsigned int bb_index, bool virtual_flag = false);


      ///GIMPLE_ASSIGN

      /**
       * Function used to create a gimple_assign.
       * @param  op0 is the first operand.
       * @param  op1 is the second operand.
       * @param  srcp is the definition of the source position.
       * @param  bb_index is the basic block index associated with the gimple statement
       * @return the tree_reindex node of the gimple_assign.
       */
      tree_nodeRef create_gimple_modify_stmt(tree_nodeRef & op0, tree_nodeRef & op1, std::string srcp, unsigned int bb_index);


      ///GIMPLE_COND

      /**
       * Function used to create a new gimple_cond with only one operand and of void type.
       * @param  expr is the condition and can be of any type.
       * @param  srcp is the definition of the source position.
       * @return the tree_reindex node of the gimple_cond created.
       * @param  bb_index is the basic block index associated with the gimple statement
       */
      tree_nodeRef create_gimple_cond(const tree_nodeRef & expr, std::string srcp, unsigned int bb_index);


      ///GIMPLE_RETURN

      /**
       * Function used to create a new gimple_return.
       * @param  decl is the result_decl node.
       * @param  op_gimple is the value to return.
       * @param  srcp is the definition of the source position.
       * @param  bb_index is the basic block index associated with the gimple statement
       * @return the tree_reindex node of the gimple_return created.
       */
      tree_nodeRef create_return_expr(const tree_nodeRef & decl, tree_nodeRef & op_gimple, std::string srcp, unsigned int bb_index);

      /**
       * create a label expression in a header of a loop.
       * @param block is the basic block where the label expression has to be inserted.
       * @param function_decl_nid is the node id of the function where the label expression is defined.
       */
      void create_label(const blocRef block, unsigned int function_decl_nid);

      /**
       * create a goto expression.
       * @param block is the basic block where the goto expression has to be inserted.
       * @param function_decl_nid is the node id of the function where the goto expression is defined.
       * @param label_expr_nid is the node id of the label expression associated with the goto expression.
       */
      void create_goto(const blocRef block, unsigned int function_decl_nid, unsigned int label_expr_nid);

      ///BASIC BLOCK

      /**
       * Function used to create a new basic block.
       * @param  list_of_bloc is the list of basic blocks.
       * @param  predecessors is the list of predecessors of the basic block to be created (indexes of the blocks).
       * @param  successors is the list of successors of the basic block to be created (indexes of the blocks).
       * @param  stmt is the list of the stmt node to be inserted in the basic block (tree_reindex nodes).
       * @param  true_edge is the index of the basic block it goes if there is a jump expression and the condition is satisfied (default=0).
       * @param  false_edge is the index of the basic block it goes if there is a jump expression and the condition is not satisfied (default=0).
       * @param  phi is the list of the gimple_phi to be inserted (default empty).
       * @return basic block created.
       */
      blocRef create_basic_block(std::map<unsigned int, blocRef> & list_of_bloc, std::vector<unsigned int> predecessors, std::vector<unsigned int> successors, std::vector<tree_nodeRef> stmt,
               unsigned int number, unsigned int true_edge = 0, unsigned int false_edge = 0, std::vector<tree_nodeRef> phi = std::vector<tree_nodeRef>());

      /**
       * Function that adds in basic block bb the phi_nodes listed in vector gimple_phi.
       * @param bb is the basic block.
       * @param gimple_phi is the vector of phi nodes.
       */
      void bb_add_phi_nodes(blocRef & bb, const std::vector<tree_nodeRef> & phi_nodes);

      /**
       * Function that adds in basic block bb the gimple_phi tn.
       * @param bb is the basic block.
       * @param tn is the gimple_phi.
       */
      void bb_add_phi_node(blocRef & bb, const tree_nodeRef & tn);

      /**
       * Function that adds in basic block bb the statements listed in vector stmt.
       * @param bb is the basic block.
       * @param stmt is the vector of statements.
       */
      void bb_add_stmts(blocRef & bb, const std::vector<tree_nodeRef> & stmt);

      /**
       * Function that adds in basic block bb the statement stmt.
       * @param bb is the basic block.
       * @param stmt is the statement.
       */
      void bb_add_stmt(blocRef & bb, const tree_nodeRef & stmt);

      /**
       * Function that adds a list of successors in basic block bb.
       * @param bb is the basic block.
       * @param successors is the vector of successors.
       */
      void bb_add_successors(blocRef & bb, const std::vector<unsigned int> & successors);

      /**
       * Function that adds a successor in basic block bb.
       * @param bb is the basic block.
       * @param successor is the successor.
       */
      void bb_add_successor(blocRef & bb, const unsigned int & successor);

      /**
       * Function that adds a list of predecessors in basic block bb.
       * @param bb is the basic block.
       * @param predecessors is the vector of predecessors.
       */
      void bb_add_predecessors(blocRef & bb, const std::vector<unsigned int> & predecessors);

      /**
       * Function that adds a predecessor in basic block bb.
       * @param bb is the basic block.
       * @param predecessor is the predecessor.
       */
      void bb_add_predecessor(blocRef & bb, const unsigned int & predecessor);

      /**
       * Function that removes in basic block bb the phi_nodes listed in vector phi_nodes.
       * @param bb is the basic block.
       * @param phi_nodes is vector of the phi_nodes.
       */
      void bb_remove_phi_nodes(blocRef & bb, const std::vector<tree_nodeRef> & phi_nodes);

      /**
       * Function that removes in basic block bb the gimple_phi tn.
       * @param bb is the basic block.
       * @param tn is the phi node.
       */
      void bb_remove_phi_node(blocRef & bb, const tree_nodeRef & tn);

      /**
       * Function that removes in basic block bb the statements listed in vector stmt.
       * @param bb is the basic block.
       * @param stmt is the vector of statements.
       */
      void bb_remove_stmts(blocRef & bb, const std::vector<tree_nodeRef> & stmt);

      /**
        * Function that removes in basic block bb the statement stmt.
        * @param bb is the basic block.
        * @param stmt is the statement.
        */
       void bb_remove_stmt(blocRef & bb, const tree_nodeRef & stmt);

      /**
       * Function that removes a list of successors in basic block bb.
       * @param bb is the basic block.
       * @param successors is the vector of successors.
       */
      void bb_remove_successors(blocRef & bb, const std::vector<unsigned int> & successors);

      /**
       * Function that removes a successor in basic block bb.
       * @param bb is the basic block.
       * @param successor is the successor.
       */
      void bb_remove_successor(blocRef & bb, const unsigned int & successor);

      /**
       * Function that removes a list of predecessors in basic block bb.
       * @param bb is the basic block.
       * @param predecessors is the vector of predecessors.
       */
      void bb_remove_predecessors(blocRef & bb, const std::vector<unsigned int> & predecessors);

      /**
       * Function that removes a predecessor in basic block bb.
       * @param bb is the basic block.
       * @param predecessor is the predecessor.
       */
      void bb_remove_predecessor(blocRef & bb, const unsigned int & predecessor);

      /**
       * Function that removes all the phi nodes of basic block bb.
       * @param bb is the basic block.
       */
      void bb_remove_all_phi_nodes(blocRef & bb);

      /**
       * Function that removes all the statements of basic block bb.
       * @param bb is the basic block.
       */
      void bb_remove_all_stmts(blocRef & bb);

      /*
       * Function that removes all the predecessors of basic block bb.
       * @param bb is the basic block.
       */
      void bb_remove_all_predecessors(blocRef & bb);

      /*
       * Function that removes all the successors of basic block bb.
       * @param bb is the basic block.
       */
      void bb_remove_all_successors(blocRef & bb);

      /**
       * Function that sets in basic block bb the index of the then basic block in case the last statement is a gimple_cond.
       * @param bb is the basic block.
       * @param false_edge_index is the index of the then basic block.
       */
      void bb_set_false_edge(blocRef & bb, const unsigned int & false_edge_index);

      /**
       * Function that sets in basic block bb the index of the if basic block in case the last statement is a gimple_cond.
       * @param bb is the basic block.
       * @param true_edge_index is the index of the if basic block.
       */
      void bb_set_true_edge(blocRef & bb, const unsigned int & true_edge_index);

      /**
       * Function that sets the number of basic block bb.
       * @param bb is the basic block.
       * @param number is the basic block number to set.
       */
      void bb_set_number(blocRef & bb, const unsigned int & number);


      ///UTILITY

      /**
       * Replace the occurrences of tree node 'old_node' with 'new_node' in statement identified by tn.
       * It operates recursively.
       * @param  tn is the tree node to be analyzed.
       * @param  old_node is the old ssa_name value.
       * @param  new_node is the new ssa_name value.
       * @return true if at least one node has been updated.
       */
      bool update_ssa(tree_nodeRef & tn, tree_nodeRef old_node, tree_nodeRef new_node);

      /**
       * Replace the occurrences of tree node 'old_node' with 'new_node' in all the statements and phi nodes of basic block bb and in the ones of its successors.
       * It operates recursively.
       * @param  list_of_bloc is the list of basic blocks.
       * @param  bb is the basic block from which the recursion starts.
       * @param  old_node is the old ssa_name value.
       * @param  new_node is the new ssa_name value.
       * @param  update_phi is a flag that is set if the phi_nodes of bb in the first call of recursion have to be considered (default true).
       * @return true if at least one node has been updated.
       */
      bool update_block(std::map<unsigned int, blocRef> & list_of_bloc, blocRef & bb, tree_nodeRef old_node, tree_nodeRef new_node, bool update_phi = true);

};

typedef refcount<tree_manipulation> tree_manipulationRef;

#endif /* TREE_MANIPULATION_HPP */
