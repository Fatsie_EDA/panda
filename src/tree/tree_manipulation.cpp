/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file tree_manipulation.cpp
 * @brief Class implementing some useful functions to create tree nodes and to manipulate the tree manager.
 *
 * This class implements some useful functions to create tree nodes and to manipulate the tree manager.
 *
 * @author ste <stefano.viazzi@gmail.com>
 * @warning This file is still in progress state.
 * @warning Do not use yet.
 *
*/
///Header include
#include "tree_manipulation.hpp"

///tree include
#include "ext_tree_node.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_basic_block.hpp"
#include "tree_reindex.hpp"

#include "token_interface.hpp"

#include <unordered_set>

unsigned int tree_manipulation::goto_label_unique_id = 0;

#define TOSTRING(id) boost::lexical_cast<std::string>(id)

#define TREE_NOT_YET_IMPLEMENTED(token) \
   THROW_ERROR(std::string("field not yet supported ") + STOK(token))

///Constructor
tree_manipulation::tree_manipulation(tree_managerRef & TM, int _debug_level) :
   TreeM(TM), debug_level(_debug_level)
{
   ///Remove from here
   enum2tok[function_decl_K] = TOK(TOK_FUNCTION_DECL);
   enum2tok[identifier_node_K] = TOK(TOK_IDENTIFIER_NODE);
   enum2tok[void_type_K] = TOK(TOK_VOID_TYPE);
   enum2tok[boolean_type_K] = TOK(TOK_BOOLEAN_TYPE);
   enum2tok[lang_type_K] = TOK(TOK_LANG_TYPE);
   enum2tok[complex_type_K] = TOK(TOK_COMPLEX_TYPE);
   enum2tok[vector_type_K] = TOK(TOK_VECTOR_TYPE);
   enum2tok[CharType_K] = TOK(TOK_CHAR_TYPE);
   enum2tok[offset_type_K] = TOK(TOK_OFFSET_TYPE);
   enum2tok[set_type_K] = TOK(TOK_SET_TYPE);
   enum2tok[qual_union_type_K] = TOK(TOK_QUAL_UNION_TYPE);
   enum2tok[real_type_K] = TOK(TOK_REAL_TYPE);
   enum2tok[integer_type_K] = TOK(TOK_INTEGER_TYPE);
   enum2tok[enumeral_type_K] = TOK(TOK_ENUMERAL_TYPE);
   enum2tok[function_type_K] = TOK(TOK_FUNCTION_TYPE);
   enum2tok[pointer_type_K] = TOK(TOK_POINTER_TYPE);
   enum2tok[reference_type_K] = TOK(TOK_REFERENCE_TYPE);
   enum2tok[array_type_K] = TOK(TOK_ARRAY_TYPE);
   enum2tok[record_type_K] = TOK(TOK_RECORD_TYPE);
   enum2tok[union_type_K] = TOK(TOK_UNION_TYPE);
   enum2tok[method_type_K] = TOK(TOK_METHOD_TYPE);
   enum2tok[type_decl_K] = TOK(TOK_TYPE_DECL);
   enum2tok[translation_unit_decl_K] = TOK(TOK_TRANSLATION_UNIT_DECL);
   enum2tok[label_decl_K] = TOK(TOK_LABEL_DECL);
   enum2tok[const_decl_K] = TOK(TOK_CONST_DECL);
   enum2tok[namespace_decl_K] = TOK(TOK_NAMESPACE_DECL);
   enum2tok[result_decl_K] = TOK(TOK_RESULT_DECL);
   enum2tok[field_decl_K] = TOK(TOK_FIELD_DECL);
   enum2tok[parm_decl_K] = TOK(TOK_PARM_DECL);
   enum2tok[var_decl_K] = TOK(TOK_VAR_DECL);
   enum2tok[gimple_return_K] = TOK(TOK_GIMPLE_RETURN);
   enum2tok[gimple_goto_K] = TOK(TOK_GIMPLE_GOTO);
   enum2tok[gimple_label_K] = TOK(TOK_GIMPLE_LABEL);
   enum2tok[unsave_expr_K] = TOK(TOK_UNSAVE_EXPR);
   enum2tok[gimple_resx_K] = TOK(TOK_GIMPLE_RESX);
   enum2tok[va_arg_expr_K] = TOK(TOK_VA_ARG_EXPR);
   enum2tok[exit_expr_K] = TOK(TOK_EXIT_EXPR);
   enum2tok[loop_expr_K] = TOK(TOK_LOOP_EXPR);
   enum2tok[nop_expr_K] = TOK(TOK_NOP_EXPR);
   enum2tok[negate_expr_K] = TOK(TOK_NEGATE_EXPR);
   enum2tok[truth_not_expr_K] = TOK(TOK_TRUTH_NOT_EXPR);
   enum2tok[convert_expr_K] = TOK(TOK_CONVERT_EXPR);
   enum2tok[addr_expr_K] = TOK(TOK_ADDR_EXPR);
   enum2tok[reference_expr_K] = TOK(TOK_REFERENCE_EXPR);
   enum2tok[cleanup_point_expr_K] = TOK(TOK_CLEANUP_POINT_EXPR);
   enum2tok[fix_trunc_expr_K] = TOK(TOK_FIX_TRUNC_EXPR);
   enum2tok[fix_ceil_expr_K] = TOK(TOK_FIX_CEIL_EXPR);
   enum2tok[fix_floor_expr_K] = TOK(TOK_FIX_FLOOR_EXPR);
   enum2tok[fix_round_expr_K] = TOK(TOK_FIX_ROUND_EXPR);
   enum2tok[float_expr_K] = TOK(TOK_FLOAT_EXPR);
   enum2tok[abs_expr_K] = TOK(TOK_ABS_EXPR);
   enum2tok[conj_expr_K] = TOK(TOK_CONJ_EXPR);
   enum2tok[realpart_expr_K] = TOK(TOK_REALPART_EXPR);
   enum2tok[imagpart_expr_K] = TOK(TOK_IMAGPART_EXPR);
   enum2tok[card_expr_K] = TOK(TOK_CARD_EXPR);
   enum2tok[non_lvalue_expr_K] = TOK(TOK_NON_LVALUE_EXPR);
   enum2tok[view_convert_expr_K] = TOK(TOK_VIEW_CONVERT_EXPR);
   enum2tok[indirect_ref_K] = TOK(TOK_INDIRECT_REF);
   enum2tok[misaligned_indirect_ref_K] = TOK(TOK_MISALIGNED_INDIRECT_REF);
   enum2tok[modify_expr_K] = TOK(TOK_MODIFY_EXPR);
   enum2tok[gimple_assign_K] = TOK(TOK_GIMPLE_ASSIGN);
   enum2tok[gt_expr_K] = TOK(TOK_GT_EXPR);
   enum2tok[plus_expr_K] = TOK(TOK_PLUS_EXPR);
   enum2tok[minus_expr_K] = TOK(TOK_MINUS_EXPR);
   enum2tok[mult_expr_K] = TOK(TOK_MULT_EXPR);
   enum2tok[trunc_div_expr_K] = TOK(TOK_TRUNC_DIV_EXPR);
   enum2tok[trunc_mod_expr_K] = TOK(TOK_TRUNC_MOD_EXPR);
   enum2tok[le_expr_K] = TOK(TOK_LE_EXPR);
   enum2tok[truth_or_expr_K] = TOK(TOK_TRUTH_OR_EXPR);
   enum2tok[ne_expr_K] = TOK(TOK_NE_EXPR);
   enum2tok[eq_expr_K] = TOK(TOK_EQ_EXPR);
   enum2tok[compound_expr_K] = TOK(TOK_COMPOUND_EXPR);
   enum2tok[init_expr_K] = TOK(TOK_INIT_EXPR);
   enum2tok[ceil_div_expr_K] = TOK(TOK_CEIL_DIV_EXPR);
   enum2tok[floor_div_expr_K] = TOK(TOK_FLOOR_DIV_EXPR);
   enum2tok[round_div_expr_K] = TOK(TOK_ROUND_DIV_EXPR);
   enum2tok[ceil_mod_expr_K] = TOK(TOK_CEIL_MOD_EXPR);
   enum2tok[floor_mod_expr_K] = TOK(TOK_FLOOR_MOD_EXPR);
   enum2tok[round_mod_expr_K] = TOK(TOK_ROUND_MOD_EXPR);
   enum2tok[rdiv_expr_K] = TOK(TOK_RDIV_EXPR);
   enum2tok[exact_div_expr_K] = TOK(TOK_EXACT_DIV_EXPR);
   enum2tok[min_expr_K] = TOK(TOK_MIN_EXPR);
   enum2tok[max_expr_K] = TOK(TOK_MAX_EXPR);
   enum2tok[lshift_expr_K] = TOK(TOK_LSHIFT_EXPR);
   enum2tok[rshift_expr_K] = TOK(TOK_RSHIFT_EXPR);
   enum2tok[lrotate_expr_K] = TOK(TOK_LROTATE_EXPR);
   enum2tok[rrotate_expr_K] = TOK(TOK_RROTATE_EXPR);
   enum2tok[bit_ior_expr_K] = TOK(TOK_BIT_IOR_EXPR);
   enum2tok[bit_xor_expr_K] = TOK(TOK_BIT_XOR_EXPR);
   enum2tok[bit_and_expr_K] = TOK(TOK_BIT_AND_EXPR);
   enum2tok[bit_not_expr_K] = TOK(TOK_BIT_NOT_EXPR);
   enum2tok[truth_andif_expr_K] = TOK(TOK_TRUTH_ANDIF_EXPR);
   enum2tok[truth_orif_expr_K] = TOK(TOK_TRUTH_ORIF_EXPR);
   enum2tok[truth_and_expr_K] = TOK(TOK_TRUTH_AND_EXPR);
   enum2tok[truth_xor_expr_K] = TOK(TOK_TRUTH_XOR_EXPR);
   enum2tok[lt_expr_K] = TOK(TOK_LT_EXPR);
   enum2tok[ge_expr_K] = TOK(TOK_GE_EXPR);
   enum2tok[unordered_expr_K] = TOK(TOK_UNORDERED_EXPR);
   enum2tok[ordered_expr_K] = TOK(TOK_ORDERED_EXPR);
   enum2tok[unlt_expr_K] = TOK(TOK_UNLT_EXPR);
   enum2tok[unle_expr_K] = TOK(TOK_UNLE_EXPR);
   enum2tok[ungt_expr_K] = TOK(TOK_UNGT_EXPR);
   enum2tok[unge_expr_K] = TOK(TOK_UNGE_EXPR);
   enum2tok[uneq_expr_K] = TOK(TOK_UNEQ_EXPR);
   enum2tok[ltgt_expr_K] = TOK(TOK_LTGT_EXPR);
   enum2tok[in_expr_K] = TOK(TOK_IN_EXPR);
   enum2tok[set_le_expr_K] = TOK(TOK_SET_LE_EXPR);
   enum2tok[range_expr_K] = TOK(TOK_RANGE_EXPR);
   enum2tok[complex_expr_K] = TOK(TOK_COMPLEX_EXPR);
   enum2tok[predecrement_expr_K] = TOK(TOK_PREDECREMENT_EXPR);
   enum2tok[preincrement_expr_K] = TOK(TOK_PREINCREMENT_EXPR);
   enum2tok[postdecrement_expr_K] = TOK(TOK_POSTDECREMENT_EXPR);
   enum2tok[postincrement_expr_K] = TOK(TOK_POSTINCREMENT_EXPR);
   enum2tok[component_ref_K] = TOK(TOK_COMPONENT_REF);
   enum2tok[array_ref_K] = TOK(TOK_ARRAY_REF);
   enum2tok[fdesc_expr_K] = TOK(TOK_FDESC_EXPR);
   enum2tok[try_catch_expr_K] = TOK(TOK_TRY_CATCH_EXPR);
   enum2tok[try_finally_K] = TOK(TOK_TRY_FINALLY);
   enum2tok[catch_expr_K] = TOK(TOK_CATCH_EXPR);
   enum2tok[eh_filter_expr_K] = TOK(TOK_EH_FILTER_EXPR);
   enum2tok[gimple_switch_K] = TOK(TOK_GIMPLE_SWITCH);
   enum2tok[save_expr_K] = TOK(TOK_SAVE_EXPR);
   enum2tok[cond_expr_K] = TOK(TOK_COND_EXPR);
   enum2tok[gimple_cond_K] = TOK(TOK_GIMPLE_COND);
   enum2tok[bit_field_ref_K] = TOK(TOK_BIT_FIELD_REF);
   enum2tok[vtable_ref_K] = TOK(TOK_VTABLE_REF);
   enum2tok[with_cleanup_expr_K] = TOK(TOK_WITH_CLEANUP_EXPR);
   enum2tok[placeholder_expr_K] = TOK(TOK_PLACEHOLDER_EXPR);
   enum2tok[goto_subroutine_K] = TOK(TOK_GOTO_SUBROUTINE);
   enum2tok[buffer_ref_K] = TOK(TOK_BUFFER_REF);
   enum2tok[array_range_ref_K] = TOK(TOK_ARRAY_RANGE_REF);
   enum2tok[case_label_expr_K] = TOK(TOK_CASE_LABEL_EXPR);
   enum2tok[gimple_bind_K] = TOK(TOK_GIMPLE_BIND);
   enum2tok[call_expr_K] = TOK(TOK_CALL_EXPR);
   enum2tok[gimple_call_K] = TOK(TOK_GIMPLE_CALL);
   enum2tok[target_expr_K] = TOK(TOK_TARGET_EXPR);
   enum2tok[gimple_asm_K] = TOK(TOK_GIMPLE_ASM);
   enum2tok[vector_cst_K] = TOK(TOK_VECTOR_CST);
   enum2tok[string_cst_K] = TOK(TOK_STRING_CST);
   enum2tok[complex_cst_K] = TOK(TOK_COMPLEX_CST);
   enum2tok[integer_cst_K] = TOK(TOK_INTEGER_CST);
   enum2tok[real_cst_K] = TOK(TOK_REAL_CST);
   enum2tok[tree_list_K] = TOK(TOK_TREE_LIST);
   enum2tok[statement_list_K] = TOK(TOK_STATEMENT_LIST);
   enum2tok[ssa_name_K] = TOK(TOK_SSA_NAME);
   enum2tok[tree_vec_K] = TOK(TOK_TREE_VEC);
   enum2tok[block_K] = TOK(TOK_BLOCK);
   enum2tok[constructor_K] = TOK(TOK_CONSTRUCTOR);
   enum2tok[gimple_phi_K] = TOK(TOK_GIMPLE_PHI);
   enum2tok[binfo_K] = TOK(TOK_BINFO);
   enum2tok[template_decl_K] = TOK(TOK_TEMPLATE_DECL);
   enum2tok[template_type_parm_K] = TOK(TOK_TEMPLATE_TYPE_PARM);
   enum2tok[typename_type_K] = TOK(TOK_TYPENAME_TYPE);
   enum2tok[cast_expr_K] = TOK(TOK_CAST_EXPR);
   enum2tok[static_cast_expr_K] = TOK(TOK_STATIC_CAST_EXPR);
   enum2tok[sizeof_expr_K] = TOK(TOK_SIZEOF_EXPR);
   enum2tok[scope_ref_K] = TOK(TOK_SCOPE_REF);
   enum2tok[ctor_initializer_K] = TOK(TOK_CTOR_INITIALIZER);
   enum2tok[arrow_expr_K] = TOK(TOK_ARROW_EXPR);
   enum2tok[return_stmt_K] = TOK(TOK_RETURN_STMT);
   enum2tok[modop_expr_K] = TOK(TOK_MODOP_EXPR);
   enum2tok[new_expr_K] = TOK(TOK_NEW_EXPR);
   enum2tok[vec_new_expr_K] = TOK(TOK_VEC_NEW_EXPR);
   enum2tok[vec_interleavehigh_expr_K] = TOK(TOC_VEC_INTERLEAVEHIGH_EXPR);
   enum2tok[vec_interleavelow_expr_K] = TOK(TOC_VEC_INTERLEAVELOW_EXPR);
   enum2tok[overload_K] = TOK(TOK_OVERLOAD);
   enum2tok[reinterpret_cast_expr_K] = TOK(TOK_REINTERPRET_CAST_EXPR);
   enum2tok[template_id_expr_K] = TOK(TOK_TEMPLATE_ID_EXPR);
   enum2tok[throw_expr_K] = TOK(TOK_THROW_EXPR);
   enum2tok[trait_expr_K] = TOK(TOK_TRAIT_EXPR);
   enum2tok[try_block_K] = TOK(TOK_TRY_BLOCK);
   enum2tok[handler_K] = TOK(TOK_HANDLER);
   enum2tok[baselink_K] = TOK(TOK_BASELINK);
   enum2tok[template_parm_index_K] = TOK(TOK_TEMPLATE_PARM_INDEX);
   enum2tok[with_size_expr_K] = TOK(TOK_WITH_SIZE_EXPR);
   enum2tok[obj_type_ref_K] = TOK(TOK_OBJ_TYPE_REF);
   enum2tok[pointer_plus_expr_K] = TOK(TOK_POINTER_PLUS_EXPR);
   enum2tok[target_mem_ref_K] = TOK(TOK_TARGET_MEM_REF);
   enum2tok[target_mem_ref461_K] = TOK(TOK_TARGET_MEM_REF461);
   enum2tok[mem_ref_K] = TOK(TOK_MEM_REF);
   enum2tok[widen_sum_expr_K] = TOK(TOK_WIDEN_SUM_EXPR);
   enum2tok[widen_mult_expr_K] = TOK(TOK_WIDEN_MULT_EXPR);
   enum2tok[gimple_predict_K] = TOK(TOK_GIMPLE_PREDICT);

}

///Destructor
tree_manipulation::~tree_manipulation()
{

}



///EXPRESSION_TREE_NODES

///TODO weight_node to fix in tree_node_factory.cpp
///Create an unary operation
tree_nodeRef tree_manipulation::create_unary_operation(const tree_nodeRef & type,const tree_nodeRef & op, std::string srcp, kind operation_kind)
{
   ///Check if the tree_node given are tree_reindex
   THROW_ASSERT(type->get_kind() == tree_reindex_K, "Type node is not a tree reindex");
   THROW_ASSERT(op->get_kind() == tree_reindex_K,"Operator node is not a tree reindex");
   THROW_ASSERT(!srcp.empty(), "It requires a non empty string");

   ///Check if the tree_node type is a unary expression
   switch (operation_kind)
   {
      case CASE_UNARY_EXPRESSION:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case call_expr_K:
      case case_label_expr_K:
      case constructor_K:
      case identifier_node_K:
      case ssa_name_K:
      case statement_list_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_BINARY_EXPRESSION:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_FAKE_NODES:
      case CASE_GIMPLE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_QUATERNARY_EXPRESSION:
      case CASE_TERNARY_EXPRESSION:
      case CASE_TYPE_NODES:
      default:
         THROW_ERROR("The operation given is not a unary expression");
   }

   ///Check if it is a correct node type
   switch (GET_NODE(type)->get_kind())
   {
      case array_type_K:
      case boolean_type_K:
      case CharType_K:
      case complex_type_K:
      case enumeral_type_K:
      case function_type_K:
      case integer_type_K:
      case lang_type_K:
      case method_type_K:
      case offset_type_K:
      case pointer_type_K:
      case qual_union_type_K:
      case real_type_K:
      case record_type_K:
      case reference_type_K:
      case set_type_K:
      case template_type_parm_K:
      case typename_type_K:
      case union_type_K:
      case vector_type_K:
      case void_type_K:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case call_expr_K:
      case case_label_expr_K:
      case constructor_K:
      case identifier_node_K:
      case ssa_name_K:
      case statement_list_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_BINARY_EXPRESSION:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_FAKE_NODES:
      case CASE_GIMPLE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_QUATERNARY_EXPRESSION:
      case CASE_TERNARY_EXPRESSION:
      case CASE_UNARY_EXPRESSION:
      default:
         THROW_ERROR(std::string("Type node not supported (") + TOSTRING(GET_INDEX_NODE(type)) + std::string("): ") + GET_NODE(type)->get_kind_text());
   }

   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   unsigned int node_nid = this->TreeM->new_tree_node_id();

   IR_schema[TOK(TOK_TYPE)] = TOSTRING(GET_INDEX_NODE(type));
   IR_schema[TOK(TOK_OP)]   = TOSTRING(GET_INDEX_NODE(op));
   IR_schema[TOK(TOK_SRCP)] = srcp;

   this->TreeM->create_tree_node(node_nid, operation_kind, IR_schema);
   tree_nodeRef return_tree_reindex = TreeM->GetTreeReindex(node_nid);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(return_tree_reindex)) + " (" + GET_NODE(return_tree_reindex)->get_kind_text() + ")" );

   return return_tree_reindex;
}

///TODO weight_node to fix in tree_node_factory.cpp
///Create a binary expression
tree_nodeRef tree_manipulation::create_binary_operation(const tree_nodeRef & type, const tree_nodeRef & op0, const tree_nodeRef & op1, std::string srcp, kind operation_kind)
{
   ///Check if the tree_node given are tree_reindex
   THROW_ASSERT(type->get_kind() == tree_reindex_K, "Type node is not a tree reindex");
   THROW_ASSERT(op0->get_kind() == tree_reindex_K, "Operator 0 node is not a tree reindex");
   THROW_ASSERT(op1->get_kind() == tree_reindex_K, "Operator 1 node is not a tree reindex");
   THROW_ASSERT(!srcp.empty(), "It requires a non empty string");

   ///Check if the tree_node type is a binary expression
   switch (operation_kind)
   {
      case CASE_BINARY_EXPRESSION:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case call_expr_K:
      case case_label_expr_K:
      case constructor_K:
      case identifier_node_K:
      case ssa_name_K:
      case statement_list_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_FAKE_NODES:
      case CASE_GIMPLE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_QUATERNARY_EXPRESSION:
      case CASE_TERNARY_EXPRESSION:
      case CASE_TYPE_NODES:
      case CASE_UNARY_EXPRESSION:
      default:
         THROW_ERROR("The operation given is not a binary expression");
   }

   ///Check if it is a correct node type
   switch (GET_NODE(type)->get_kind())
   {
      case array_type_K:
      case boolean_type_K:
      case CharType_K:
      case complex_type_K:
      case enumeral_type_K:
      case function_type_K:
      case integer_type_K:
      case lang_type_K:
      case method_type_K:
      case offset_type_K:
      case pointer_type_K:
      case qual_union_type_K:
      case real_type_K:
      case record_type_K:
      case reference_type_K:
      case set_type_K:
      case template_type_parm_K:
      case typename_type_K:
      case union_type_K:
      case vector_type_K:
      case void_type_K:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case call_expr_K:
      case case_label_expr_K:
      case constructor_K:
      case identifier_node_K:
      case ssa_name_K:
      case statement_list_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_BINARY_EXPRESSION:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_FAKE_NODES:
      case CASE_GIMPLE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_QUATERNARY_EXPRESSION:
      case CASE_TERNARY_EXPRESSION:
      case CASE_UNARY_EXPRESSION:
      default:
         THROW_ERROR(std::string("Type node not supported (") + TOSTRING(GET_INDEX_NODE(type)) + std::string("): ") + GET_NODE(type)->get_kind_text());
   }

   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   unsigned int node_nid = this->TreeM->new_tree_node_id();

   IR_schema[TOK(TOK_TYPE)] = TOSTRING(GET_INDEX_NODE(type));
   IR_schema[TOK(TOK_OP0)]  = TOSTRING(GET_INDEX_NODE(op0));
   IR_schema[TOK(TOK_OP1)]  = TOSTRING(GET_INDEX_NODE(op1));
   IR_schema[TOK(TOK_SRCP)] = srcp;

   this->TreeM->create_tree_node(node_nid, operation_kind, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(node_nid);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   return node_ref;
}

///TODO weight_node to fix in tree_node_factory.cpp
///Create a ternary expression
tree_nodeRef tree_manipulation::create_ternary_operation(const tree_nodeRef & type, const tree_nodeRef & op0, const tree_nodeRef & op1, const tree_nodeRef & op2, std::string srcp, kind operation_kind)
{
   ///Check if the tree_node given are tree_reindex
   THROW_ASSERT(type->get_kind() == tree_reindex_K, "Type is not a tree reindex");
   THROW_ASSERT(op0->get_kind() == tree_reindex_K, "Operand 0 is not a tree reindex");
   THROW_ASSERT(op1->get_kind() == tree_reindex_K, "Operand 1 is not a tree reindex");
   THROW_ASSERT(op2->get_kind() == tree_reindex_K, "Operand 3 is not a tree reindex");
   THROW_ASSERT(!srcp.empty(), "It requires a non empty string");

   ///Check if the tree_node type is a ternary expression
   switch (operation_kind)
   {
      case CASE_TERNARY_EXPRESSION:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case call_expr_K:
      case case_label_expr_K:
      case constructor_K:
      case identifier_node_K:
      case ssa_name_K:
      case statement_list_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_BINARY_EXPRESSION:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_FAKE_NODES:
      case CASE_GIMPLE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_QUATERNARY_EXPRESSION:
      case CASE_TYPE_NODES:
      case CASE_UNARY_EXPRESSION:
      default:
         THROW_ERROR("The operation given is not a ternary expression");
   }

   ///Check if it is a correct node type
   switch (GET_NODE(type)->get_kind())
   {
      case array_type_K:
      case boolean_type_K:
      case CharType_K:
      case complex_type_K:
      case enumeral_type_K:
      case function_type_K:
      case integer_type_K:
      case lang_type_K:
      case method_type_K:
      case offset_type_K:
      case pointer_type_K:
      case qual_union_type_K:
      case real_type_K:
      case record_type_K:
      case reference_type_K:
      case set_type_K:
      case template_type_parm_K:
      case typename_type_K:
      case union_type_K:
      case vector_type_K:
      case void_type_K:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case call_expr_K:
      case case_label_expr_K:
      case constructor_K:
      case identifier_node_K:
      case ssa_name_K:
      case statement_list_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_BINARY_EXPRESSION:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_FAKE_NODES:
      case CASE_GIMPLE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_QUATERNARY_EXPRESSION:
      case CASE_TERNARY_EXPRESSION:
      case CASE_UNARY_EXPRESSION:
      default:
         THROW_ERROR(std::string("Type node not supported (") + TOSTRING(GET_INDEX_NODE(type)) + std::string("): ") + GET_NODE(type)->get_kind_text());
   }


   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   unsigned int node_nid = this->TreeM->new_tree_node_id();

   IR_schema[TOK(TOK_TYPE)] = TOSTRING(GET_INDEX_NODE(type));
   IR_schema[TOK(TOK_OP0)]  = TOSTRING(GET_INDEX_NODE(op0));
   IR_schema[TOK(TOK_OP1)]  = TOSTRING(GET_INDEX_NODE(op1));
   IR_schema[TOK(TOK_OP2)]  = TOSTRING(GET_INDEX_NODE(op2));
   IR_schema[TOK(TOK_SRCP)] = srcp;

   this->TreeM->create_tree_node(node_nid, operation_kind, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(node_nid);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   return node_ref;
}

///TODO weight_node to fix in tree_node_factory.cpp
///Create a quaternary expression
tree_nodeRef tree_manipulation::create_quaternary_operation(const tree_nodeRef & type, const tree_nodeRef & op0,const tree_nodeRef & op1,const tree_nodeRef & op2,const tree_nodeRef & op3, std::string srcp, kind operation_kind)
{
   ///Check if the tree_node given are tree_reindex
   THROW_ASSERT(type->get_kind() == tree_reindex_K, "Type node is not a tree reindex");
   THROW_ASSERT(op0->get_kind() == tree_reindex_K, "Operand 0 node is not a tree reindex");
   THROW_ASSERT(op1->get_kind() == tree_reindex_K, "Operand 1 node is not a tree reindex");
   THROW_ASSERT(op2->get_kind() == tree_reindex_K, "Operand 2 node is not a tree reindex");
   THROW_ASSERT(op3->get_kind() == tree_reindex_K, "Operand 3 node is not a tree reindex");
   THROW_ASSERT(!srcp.empty(), "It requires a non empty string");

   ///Check if the tree_node type is a quaternary expression
   switch (operation_kind)
   {
      case CASE_QUATERNARY_EXPRESSION:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case call_expr_K:
      case case_label_expr_K:
      case constructor_K:
      case identifier_node_K:
      case ssa_name_K:
      case statement_list_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_BINARY_EXPRESSION:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_FAKE_NODES:
      case CASE_GIMPLE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_TERNARY_EXPRESSION:
      case CASE_TYPE_NODES:
      case CASE_UNARY_EXPRESSION:
      default:
         THROW_ERROR("The operation given is not a quaternary expression");
   }

   switch (GET_NODE(type)->get_kind())
   {
      case array_type_K:
      case boolean_type_K:
      case CharType_K:
      case complex_type_K:
      case enumeral_type_K:
      case function_type_K:
      case integer_type_K:
      case lang_type_K:
      case method_type_K:
      case offset_type_K:
      case pointer_type_K:
      case qual_union_type_K:
      case real_type_K:
      case record_type_K:
      case reference_type_K:
      case set_type_K:
      case template_type_parm_K:
      case typename_type_K:
      case union_type_K:
      case vector_type_K:
      case void_type_K:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case call_expr_K:
      case case_label_expr_K:
      case constructor_K:
      case identifier_node_K:
      case ssa_name_K:
      case statement_list_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_BINARY_EXPRESSION:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_FAKE_NODES:
      case CASE_GIMPLE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_QUATERNARY_EXPRESSION:
      case CASE_TERNARY_EXPRESSION:
      case CASE_UNARY_EXPRESSION:
      default:
         THROW_ERROR(std::string("Type node not supported (") + TOSTRING(GET_INDEX_NODE(type)) + std::string("): ") + GET_NODE(type)->get_kind_text());
   }

   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   unsigned int node_nid = this->TreeM->new_tree_node_id();

   IR_schema[TOK(TOK_TYPE)] = TOSTRING(GET_INDEX_NODE(type));
   IR_schema[TOK(TOK_OP0)]  = TOSTRING(GET_INDEX_NODE(op0));
   IR_schema[TOK(TOK_OP1)]  = TOSTRING(GET_INDEX_NODE(op1));
   IR_schema[TOK(TOK_OP2)]  = TOSTRING(GET_INDEX_NODE(op2));
   IR_schema[TOK(TOK_OP3)]  = TOSTRING(GET_INDEX_NODE(op3));
   IR_schema[TOK(TOK_SRCP)] = srcp;

   this->TreeM->create_tree_node(node_nid, operation_kind, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(node_nid);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   return node_ref;

}



///CONST_OBJ_TREE_NODES

///Create an integer_cst node
tree_nodeRef tree_manipulation::CreateIntegerCst(const tree_nodeRef type, const long long int value, const unsigned int integer_cst_nid)
{
   THROW_ASSERT(type->get_kind() == tree_reindex_K, "Type node is not a tree reindex");

   unsigned int type_node_nid = GET_INDEX_NODE(type);
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   IR_schema[TOK(TOK_TYPE)] = TOSTRING(type_node_nid);
   IR_schema[TOK(TOK_VALUE)] = TOSTRING(value);

   this->TreeM->create_tree_node(integer_cst_nid, integer_cst_K, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(integer_cst_nid);


   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   return node_ref;

}



///IDENTIFIER_TREE_NODE

///Create an identifier node
tree_nodeRef tree_manipulation::create_identifier_node(const std::string strg)
{
   THROW_ASSERT(!strg.empty(), "It requires a non empty string");

   ///@37     identifier_node  strg: "int" lngt: 3
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   tree_nodeRef node_ref;

   IR_schema[TOK(TOK_STRG)] = strg;
   unsigned int node_nid = this->TreeM->find(identifier_node_K, IR_schema);

   if (!node_nid)
   {
      node_nid = this->TreeM->new_tree_node_id();
      this->TreeM->create_tree_node(node_nid, identifier_node_K, IR_schema);
      node_ref = TreeM->GetTreeReindex(node_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + " "+ strg + ")" );
   }
   else
   {
      node_ref = TreeM->GetTreeReindex(node_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Found   node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + " "+ strg + ")" );
   }
   return node_ref;
}


///DECL_NODES

///Create a var_decl
tree_nodeRef tree_manipulation::create_var_decl(const tree_nodeRef & name, const tree_nodeRef & type, const tree_nodeRef & scpe, const tree_nodeRef & size,
         const tree_nodeRef & smt_ann, const tree_nodeRef & init, std::string srcp, unsigned int algn, int used, bool artificial_flag, int use_tmpl, bool static_static_flag,
         bool extern_flag, bool static_flag, bool register_flag)
{
   ///Check if the tree_node given are tree_reindex
   THROW_ASSERT(type->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(scpe->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(size->get_kind() == tree_reindex_K, "Node is not a tree reindex");

   THROW_ASSERT(!srcp.empty(), "It requires a non empty string");

   unsigned int node_nid = this->TreeM->new_tree_node_id();

   unsigned int type_node_nid = GET_INDEX_NODE(type);
   unsigned int size_node_nid = GET_INDEX_NODE(size);
   unsigned int scpe_node_nid = GET_INDEX_NODE(scpe);

   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;

   if(name)
   {
      THROW_ASSERT(name->get_kind() == tree_reindex_K, "Node is not a tree reindex");
      unsigned int name_node_nid = GET_INDEX_NODE(name);
      IR_schema[TOK(TOK_NAME)] = TOSTRING(name_node_nid);
   }

   if(smt_ann)
   {
      THROW_ASSERT(smt_ann->get_kind() == tree_reindex_K, "Node is not a tree reindex");
      unsigned int smt_node_nid = GET_INDEX_NODE(smt_ann);
      IR_schema[TOK(TOK_SMT_ANN)] = TOSTRING(smt_node_nid);
   }

   if(init)
   {
      THROW_ASSERT(init->get_kind() == tree_reindex_K, "Node is not a tree reindex");
      unsigned int init_nid = GET_INDEX_NODE(init);
      IR_schema[TOK(TOK_INIT)] = TOSTRING(init_nid);
   }

   IR_schema[TOK(TOK_TYPE)] = TOSTRING(type_node_nid);
   IR_schema[TOK(TOK_SCPE)] = TOSTRING(scpe_node_nid);
   IR_schema[TOK(TOK_SIZE)] = TOSTRING(size_node_nid);
   IR_schema[TOK(TOK_ALGN)] = TOSTRING(algn);
   IR_schema[TOK(TOK_USED)] = TOSTRING(used);
   IR_schema[TOK(TOK_SRCP)] = srcp;
   IR_schema[TOK(TOK_USE_TMPL)] = TOSTRING(use_tmpl);
   IR_schema[TOK(TOK_STATIC_STATIC)] = TOSTRING(static_static_flag);
   IR_schema[TOK(TOK_EXTERN)] = TOSTRING(extern_flag);
   IR_schema[TOK(TOK_STATIC)] = TOSTRING(static_flag);
   IR_schema[TOK(TOK_REGISTER)] = TOSTRING(register_flag);
   IR_schema[TOK(TOK_ARTIFICIAL)] = TOSTRING(artificial_flag);

   this->TreeM->create_tree_node(node_nid, var_decl_K, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(node_nid);

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   return node_ref;

}

///Create result_decl
tree_nodeRef tree_manipulation::create_result_decl(const tree_nodeRef & name, const tree_nodeRef & type, const tree_nodeRef & scpe, const tree_nodeRef & size,  const tree_nodeRef &  smt_ann, const tree_nodeRef & init, std::string srcp,  unsigned int algn, bool artificial_flag)
{

   ///Check if the tree_node given are tree_reindex
   THROW_ASSERT(type->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(scpe->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(size->get_kind() == tree_reindex_K, "Node is not a tree reindex");

   THROW_ASSERT(!srcp.empty(), "It requires a non empty string");

   unsigned int node_nid = this->TreeM->new_tree_node_id();

   unsigned int type_node_nid = GET_INDEX_NODE(type);
   unsigned int size_node_nid = GET_INDEX_NODE(size);
   unsigned int scpe_node_nid = GET_INDEX_NODE(scpe);

   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;

   IR_schema[TOK(TOK_TYPE)] = TOSTRING(type_node_nid);
   IR_schema[TOK(TOK_SCPE)] = TOSTRING(scpe_node_nid);
   IR_schema[TOK(TOK_SIZE)] = TOSTRING(size_node_nid);
   IR_schema[TOK(TOK_ALGN)] = TOSTRING(algn);
   IR_schema[TOK(TOK_SRCP)] = srcp;
   IR_schema[TOK(TOK_ARTIFICIAL)] = TOSTRING(artificial_flag);

   if(smt_ann)
   {
      THROW_ASSERT(smt_ann->get_kind() == tree_reindex_K, "Node is not a tree reindex");
      unsigned int smt_node_nid = GET_INDEX_NODE(smt_ann);
      IR_schema[TOK(TOK_SMT_ANN)] = TOSTRING(smt_node_nid);
   }

   if(name)
   {
      THROW_ASSERT(name->get_kind() == tree_reindex_K, "Node is not a tree reindex");
      unsigned int name_node_nid = GET_INDEX_NODE(name);
      IR_schema[TOK(TOK_NAME)] = TOSTRING(name_node_nid);
   }

   if(init)
   {
      THROW_ASSERT(init->get_kind() == tree_reindex_K, "Node is not a tree reindex");
      unsigned int init_nid = GET_INDEX_NODE(init);
      IR_schema[TOK(TOK_INIT)] = TOSTRING(init_nid);
   }

   this->TreeM->create_tree_node(node_nid, result_decl_K, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(node_nid);

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   return node_ref;

}

///Create parm_decl
tree_nodeRef tree_manipulation::create_parm_decl(const tree_nodeRef & name, const tree_nodeRef & type, const tree_nodeRef & scpe, const tree_nodeRef & size, const tree_nodeRef & argt,
         const tree_nodeRef & smt_ann, const tree_nodeRef & init, std::string srcp, unsigned int algn,int used,  bool register_flag)
{
   ///Check if the tree_node given are tree_reindex
   THROW_ASSERT(type->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(scpe->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(size->get_kind() == tree_reindex_K, "Node is not a tree reindex");

   THROW_ASSERT(!srcp.empty(), "It requires a non empty string");

   unsigned int node_nid = this->TreeM->new_tree_node_id();

   unsigned int type_node_nid = GET_INDEX_NODE(type);
   unsigned int size_node_nid = GET_INDEX_NODE(size);
   unsigned int scpe_node_nid = GET_INDEX_NODE(scpe);
   unsigned int argt_node_nid = GET_INDEX_NODE(argt);

   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   IR_schema[TOK(TOK_TYPE)] = TOSTRING(type_node_nid);
   IR_schema[TOK(TOK_SCPE)] = TOSTRING(scpe_node_nid);
   IR_schema[TOK(TOK_SIZE)] = TOSTRING(size_node_nid);
   IR_schema[TOK(TOK_ARGT)] = TOSTRING(argt_node_nid);
   IR_schema[TOK(TOK_ALGN)] = TOSTRING(algn);
   IR_schema[TOK(TOK_USED)] = TOSTRING(used);
   IR_schema[TOK(TOK_SRCP)] = srcp;
   IR_schema[TOK(TOK_REGISTER)] = TOSTRING(register_flag);

   if(smt_ann)
   {
      THROW_ASSERT(smt_ann->get_kind() == tree_reindex_K, "Node is not a tree reindex");
      unsigned int smt_node_nid = GET_INDEX_NODE(smt_ann);
      IR_schema[TOK(TOK_SMT_ANN)] = TOSTRING(smt_node_nid);
   }

   if(name)
   {
      THROW_ASSERT(name->get_kind() == tree_reindex_K, "Node is not a tree reindex");
      unsigned int name_node_nid = GET_INDEX_NODE(name);
      IR_schema[TOK(TOK_NAME)] = TOSTRING(name_node_nid);
   }

   if(init)
   {
      THROW_ASSERT(init->get_kind() == tree_reindex_K, "Node is not a tree reindex");
      unsigned int init_nid = GET_INDEX_NODE(init);
      IR_schema[TOK(TOK_INIT)] = TOSTRING(init_nid);
   }

   this->TreeM->create_tree_node(node_nid, parm_decl_K, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(node_nid);

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   return node_ref;

}


///TYPE_OBJ

///Create a void type
tree_nodeRef tree_manipulation::create_void_type()
{
   ///@41     void_type        name: @58      algn: 8
   ///@58     type_decl        name: @63      type: @41      srcp: "<built-in>:0:0"
   ///@63     identifier_node  strg: "void" lngt: 4
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   tree_nodeRef void_node;

   IR_schema[TOK(TOK_ALGN)] = TOSTRING(ALGN_VOID);
   //If a void type already exists, it is not created a new one
   unsigned int void_type_nid = this->TreeM->find(void_type_K, IR_schema);

   if (!void_type_nid)
   {
      void_type_nid = this->TreeM->new_tree_node_id();
      unsigned int type_decl_nid = this->TreeM->new_tree_node_id();
      unsigned int void_identifier_nid = GET_INDEX_NODE(create_identifier_node("void")); //@63

      ///Create the void_type
      ///@41  void_type        name: @58      algn: 8
      IR_schema[TOK(TOK_NAME)] = TOSTRING(type_decl_nid);
      this->TreeM->create_tree_node(void_type_nid, void_type_K, IR_schema);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(void_type_nid) + " (void_type)" );

      ///Create the type_decl
      ///@58 type_decl name: @63 type: @41 srcp: "<built-in>:0:0"
      IR_schema.clear();
      IR_schema[TOK(TOK_NAME)] = TOSTRING(void_identifier_nid);
      IR_schema[TOK(TOK_TYPE)] = TOSTRING(void_type_nid);
      IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
      this->TreeM->create_tree_node(type_decl_nid, type_decl_K, IR_schema);
      void_node = TreeM->GetTreeReindex(void_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(void_node)) + " (" + GET_NODE(void_node)->get_kind_text() + " void)" );
   }
   else
   {
      void_node = TreeM->GetTreeReindex(void_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Found   node " + TOSTRING(GET_INDEX_NODE(void_node)) + " (" + GET_NODE(void_node)->get_kind_text() + " void)" );
   }

   return void_node;
}

///Create a bit_size type
tree_nodeRef tree_manipulation::create_bit_size_type()
{
   ///@32    identifier_node  strg: "bitsizetype"  lngt: 13
   ///@18    integer_type   name: @32   size: @33   algn: 64    prec: 64   unsigned   min : @34   max : @35
   ///@33    integer_cst      type: @18      low : 64
   ///@34    integer_cst      type: @18      low : 0
   ///@35    integer_cst      type: @18      low : -1
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   tree_nodeRef bit_size_node;

   ///@32    identifier_node  strg: "bitsizetype"  lngt: 13
   tree_nodeRef bit_size_identifier_node = create_identifier_node("bitsizetype");
   unsigned int bit_size_identifier_nid = GET_INDEX_NODE(bit_size_identifier_node);

   ///@18    integer_type   name: @32   size: @33   algn: 64    prec: 64   unsigned   min : @34   max : @35
   IR_schema[TOK(TOK_NAME)] = TOSTRING(bit_size_identifier_nid); ///@32
   unsigned int bit_size_type_nid = this->TreeM->find(integer_type_K, IR_schema);

   ///not_found decl
   if (!bit_size_type_nid)
   {
      bit_size_type_nid = this->TreeM->new_tree_node_id();
      unsigned int size_node_nid = this->TreeM->new_tree_node_id();
      unsigned int min_node_nid = this->TreeM->new_tree_node_id();
      unsigned int max_node_nid = this->TreeM->new_tree_node_id();
      ///@18    integer_type   name: @32   size: @33   algn: 64    prec: 64   unsigned   min : @34   max : @35
      IR_schema[TOK(TOK_SIZE)] = TOSTRING(size_node_nid);
      IR_schema[TOK(TOK_ALGN)] = TOSTRING(ALGN_BIT_SIZE);
      IR_schema[TOK(TOK_PREC)] = TOSTRING(PREC_BIT_SIZE);
      IR_schema[TOK(TOK_UNSIGNED)] = TOSTRING(true);
      IR_schema[TOK(TOK_MIN)] = TOSTRING(min_node_nid);
      IR_schema[TOK(TOK_MAX)] = TOSTRING(max_node_nid);
      this->TreeM->create_tree_node(bit_size_type_nid, integer_type_K, IR_schema);
      bit_size_node = TreeM->GetTreeReindex(bit_size_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(bit_size_node)) + " (" + GET_NODE(bit_size_node)->get_kind_text() + " bit_size)" );

      ///@33    integer_cst      type: @18      low : 64
      CreateIntegerCst(bit_size_node, SIZE_VALUE_BIT_SIZE, size_node_nid);

      ///@34    integer_cst      type: @18      low : 0
      CreateIntegerCst(bit_size_node, MIN_VALUE_BIT_SIZE, min_node_nid);

      ///@35    integer_cst      type: @18      low : -1
      CreateIntegerCst(bit_size_node, MAX_VALUE_BIT_SIZE, max_node_nid);
   }
   else
   {
      bit_size_node = TreeM->GetTreeReindex(bit_size_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Found   node " + TOSTRING(GET_INDEX_NODE(bit_size_node)) + " (" + GET_NODE(bit_size_node)->get_kind_text() + " bit_size)" );
   }
   return bit_size_node;
}

///Create a size type
tree_nodeRef tree_manipulation::create_size_type()
{
   //@124    identifier_node  strg: "sizetype"             lngt: 8
   //@96     integer_type     name: @124     size: @15      algn: 32
   //                         prec: 32       unsigned       min : @125
   //                         max : @126

   ///@32    identifier_node  strg: "sizetype"  lngt: 13
   ///@18    integer_type   name: @32   size: @33   algn: 64    prec: 64   unsigned   min : @34   max : @35
   ///@33    integer_cst      type: @18      low : 64
   ///@34    integer_cst      type: @18      low : 0
   ///@35    integer_cst      type: @18      low : -1
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   tree_nodeRef size_node;

   //@124    identifier_node  strg: "sizetype"             lngt: 8
   /// sizetype will translated in unsigned long
   tree_nodeRef size_identifier_node = create_identifier_node("unsigned long ");
   unsigned int size_identifier_nid = GET_INDEX_NODE(size_identifier_node);

   //@96     integer_type name: @124 size: @15 algn: 32 prec: 32 unsigned min : @125 max : @126
   IR_schema[TOK(TOK_NAME)] = TOSTRING(size_identifier_nid); ///@124
   unsigned int size_type_nid = this->TreeM->find(integer_type_K,IR_schema);

   ///not_found decl
   if (!size_type_nid)
   {
      size_type_nid = this->TreeM->new_tree_node_id();
      unsigned int size_node_nid = this->TreeM->new_tree_node_id();
      unsigned int min_node_nid = this->TreeM->new_tree_node_id();
      unsigned int max_node_nid = this->TreeM->new_tree_node_id();
      ///@18    integer_type   name: @32   size: @33   algn: 64    prec: 64   unsigned   min : @34   max : @35
      IR_schema[TOK(TOK_SIZE)] = TOSTRING(size_node_nid);
      IR_schema[TOK(TOK_ALGN)] = TOSTRING(ALGN_BIT_SIZE);
      IR_schema[TOK(TOK_PREC)] = TOSTRING(PREC_BIT_SIZE);
      IR_schema[TOK(TOK_UNSIGNED)] = TOSTRING(true);
      IR_schema[TOK(TOK_MIN)] = TOSTRING(min_node_nid);
      IR_schema[TOK(TOK_MAX)] = TOSTRING(max_node_nid);
      this->TreeM->create_tree_node(size_type_nid, integer_type_K, IR_schema);
      size_node = TreeM->GetTreeReindex(size_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(size_node)) + " (" + GET_NODE(size_node)->get_kind_text() + " bit_size)" );

      ///@33    integer_cst      type: @18      low : 64
      CreateIntegerCst(size_node, SIZE_VALUE_BIT_SIZE, size_node_nid);

      ///@34    integer_cst      type: @18      low : 0
      CreateIntegerCst(size_node, MIN_VALUE_BIT_SIZE, min_node_nid);

      ///@35    integer_cst      type: @18      low : -1
      CreateIntegerCst(size_node, MAX_VALUE_BIT_SIZE, max_node_nid);
   }
   else
   {
      size_node = TreeM->GetTreeReindex(size_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Found   node " + TOSTRING(GET_INDEX_NODE(size_node)) + " (" + GET_NODE(size_node)->get_kind_text() + " bit_size)" );
   }
   return size_node;
}

///Create a boolean type
tree_nodeRef tree_manipulation::create_boolean_type()
{
   ///@48 boolean_type name: @55 size: @7 algn: 8
   ///@55 type_decl name: @58 type: @48 srcp: "<built-in>:0:0"
   ///@58 identifier_node strg: "_Bool" lngt: 5
   ///@7 integer_cst type: @21 low: 8       @21 is bit_size

   ///@58 identifier_node strg: "_Bool" lngt: 5
   tree_nodeRef boolean_type_node;

   tree_nodeRef boolean_identifier_node = create_identifier_node("_Bool");
   unsigned int boolean_identifier_nid = GET_INDEX_NODE(boolean_identifier_node); ///@58

   tree_nodeRef bit_size_node = create_bit_size_type(); //@21

   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;

   ///@55 type_decl name: @58 type: @48 srcp: "<built-in>:0:0"
   IR_schema[TOK(TOK_NAME)] = TOSTRING(boolean_identifier_nid); //@58
   unsigned int type_decl_nid = this->TreeM->find(type_decl_K,IR_schema);

   ///not_found decl
   if (!type_decl_nid)
   {
      type_decl_nid = this->TreeM->new_tree_node_id();
      unsigned int boolean_type_nid = this->TreeM->new_tree_node_id();
      unsigned int size_node_nid = this->TreeM->new_tree_node_id();

      ///@55 type_decl name: @58 type: @48 srcp: "<built-in>:0:0"
      IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
      this->TreeM->create_tree_node(type_decl_nid, type_decl_K, IR_schema);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(type_decl_nid) + " (type_decl boolean)" );

      ///@48 boolean_type name: @55 size: @7 algn: 8
      IR_schema.clear();
      IR_schema[TOK(TOK_NAME)] = TOSTRING(type_decl_nid); //@55
      IR_schema[TOK(TOK_SIZE)] = TOSTRING(size_node_nid); //@7
      IR_schema[TOK(TOK_ALGN)] = TOSTRING(ALGN_BOOLEAN);
      this->TreeM->create_tree_node(boolean_type_nid, boolean_type_K, IR_schema);
      boolean_type_node = TreeM->GetTreeReindex(boolean_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(boolean_type_node)) + " (" + GET_NODE(boolean_type_node)->get_kind_text() + " boolean)" );

      ///@7 integer_cst type: @21 low: 8       @21 is bit_size
      CreateIntegerCst(bit_size_node, SIZE_VALUE_BOOL, size_node_nid);
   }
   else
   {
      IR_schema.clear();
      IR_schema[TOK(TOK_NAME)] = TOSTRING(type_decl_nid);
      IR_schema[TOK(TOK_ALGN)] = TOSTRING(ALGN_BOOLEAN);

      unsigned int boolean_type_nid = this->TreeM->find(boolean_type_K,IR_schema);

      if (!boolean_type_nid)
         THROW_ERROR("Something wrong happened!");

      boolean_type_node = TreeM->GetTreeReindex(boolean_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Found   node " + TOSTRING(GET_INDEX_NODE(boolean_type_node)) + " (" + GET_NODE(boolean_type_node)->get_kind_text() + " boolean)" );
   }
   return boolean_type_node;
}

///Create an unsigned integer type
tree_nodeRef tree_manipulation::create_default_unsigned_integer_type()
{
   ///@41     identifier_node  strg: "unsigned int"         lngt: 12
   ///@8      integer_type     name: @20      size: @12      algn: 32      prec: 32       unsigned       min : @21    max : @22
   ///@20     type_decl        name: @41      type: @8       srcp: "<built-in>:0:0"
   ///@12     integer_cst      type: @18      low : 32
   ///@21     integer_cst      type: @8       low : 0
   ///@22     integer_cst      type: @8       low : -1
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   tree_nodeRef integer_type_node;

   ///@41     identifier_node  strg: "unsigned int"         lngt: 12
   tree_nodeRef unsigned_int_identifier_node = create_identifier_node("unsigned int");
   unsigned int unsigned_int_identifier_nid = GET_INDEX_NODE(unsigned_int_identifier_node); ///@41

   tree_nodeRef bit_size_node = create_bit_size_type();

   ///@20     type_decl        name: @41      type: @8       srcp: "<built-in>:0:0"
   IR_schema.clear();
   IR_schema[TOK(TOK_NAME)] = TOSTRING(unsigned_int_identifier_nid);
   unsigned int type_decl_nid = this->TreeM->find(type_decl_K,IR_schema);

   ///not_found decl
   if (!type_decl_nid)
   {
      type_decl_nid = this->TreeM->new_tree_node_id();
      unsigned int integer_type_nid = this->TreeM->new_tree_node_id();
      unsigned int size_node_nid = this->TreeM->new_tree_node_id();
      unsigned int min_node_nid = this->TreeM->new_tree_node_id();
      unsigned int max_node_nid = this->TreeM->new_tree_node_id();

      ///@20     type_decl        name: @41      type: @8       srcp: "<built-in>:0:0"
      IR_schema[TOK(TOK_TYPE)] = TOSTRING(integer_type_nid); //@8
      IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
      this->TreeM->create_tree_node(type_decl_nid, type_decl_K, IR_schema);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(type_decl_nid) + " (type_decl unsigned_int)" );

      ///@8      integer_type     name: @20      size: @12      algn: 32      prec: 32       unsigned       min : @21    max : @22
      IR_schema.clear();
      IR_schema[TOK(TOK_NAME)] = TOSTRING(type_decl_nid);
      IR_schema[TOK(TOK_SIZE)] = TOSTRING(size_node_nid);
      IR_schema[TOK(TOK_ALGN)] = TOSTRING(ALGN_UNSIGNED_INT);
      IR_schema[TOK(TOK_PREC)] = TOSTRING(PREC_UNSIGNED_INT);
      IR_schema[TOK(TOK_UNSIGNED)] = TOSTRING(true);
      IR_schema[TOK(TOK_MIN)] = TOSTRING(min_node_nid);
      IR_schema[TOK(TOK_MAX)] = TOSTRING(max_node_nid);
      this->TreeM->create_tree_node(integer_type_nid, integer_type_K, IR_schema);
      integer_type_node = TreeM->GetTreeReindex(integer_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(integer_type_node)) + " (" + GET_NODE(integer_type_node)->get_kind_text() + " unsigned_int)" );

      ///@12     integer_cst      type: @18      low : 32
      CreateIntegerCst(bit_size_node, SIZE_VALUE_UNSIGNED_INT, size_node_nid);
      ///@21     integer_cst      type: @8       low : 0
      CreateIntegerCst(integer_type_node, MIN_VALUE_UNSIGNED_INT, min_node_nid);
      ///@22     integer_cst      type: @8       low : -1
      CreateIntegerCst(integer_type_node, MAX_VALUE_UNSIGNED_INT, max_node_nid);
   }
   else
   {
      IR_schema.clear();
      IR_schema[TOK(TOK_NAME)] = TOSTRING(type_decl_nid);
      IR_schema[TOK(TOK_ALGN)] = TOSTRING(ALGN_UNSIGNED_INT);
      IR_schema[TOK(TOK_PREC)] = TOSTRING(PREC_UNSIGNED_INT);
      IR_schema[TOK(TOK_UNSIGNED)] = TOSTRING(true);
      unsigned int integer_type_nid = this->TreeM->find(integer_type_K,IR_schema);

      if (!integer_type_nid)
         THROW_ERROR("Something wrong happened!");

      integer_type_node = TreeM->GetTreeReindex(integer_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Found   node " + TOSTRING(GET_INDEX_NODE(integer_type_node)) + " (" + GET_NODE(integer_type_node)->get_kind_text() + " unsigned int)" );
   }
   return integer_type_node;
}


///Create an integer type
tree_nodeRef tree_manipulation::create_default_integer_type()
{
   ///@36     identifier_node  strg: "int" lngt: 3
   ///@19     type_decl        name: @36      type: @8       srcp: "<built-in>:0:0"
   ///@8      integer_type     name: @19      size: @11   algn: 32   prec: 32       min : @20      max : @21
   ///@11     integer_cst      type: @18      low : 32
   ///@20     integer_cst      type: @8       high: -1  low : -2147483648
   ///@21     integer_cst      type: @8       low : 2147483647

   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   tree_nodeRef integer_type_node;
   tree_nodeRef bit_size_node = create_bit_size_type();

   ///@36     identifier_node  strg: "int" lngt: 3
   tree_nodeRef int_identifier_node = create_identifier_node("int");
   unsigned int int_identifier_nid = GET_INDEX_NODE(int_identifier_node); ///@36

   ///@19     type_decl        name: @36      type: @8       srcp: "<built-in>:0:0"
   IR_schema.clear();
   IR_schema[TOK(TOK_NAME)] = TOSTRING(int_identifier_nid);
   unsigned int type_decl_nid = this->TreeM->find(type_decl_K,IR_schema);

   ///not_found decl
   if (!type_decl_nid)
   {
      type_decl_nid = this->TreeM->new_tree_node_id();
      unsigned int integer_type_nid = this->TreeM->new_tree_node_id();
      unsigned int size_node_nid = this->TreeM->new_tree_node_id();
      unsigned int min_node_nid = this->TreeM->new_tree_node_id();
      unsigned int max_node_nid = this->TreeM->new_tree_node_id();

      ///@20     type_decl        name: @41      type: @8       srcp: "<built-in>:0:0"
      IR_schema[TOK(TOK_TYPE)] = TOSTRING(integer_type_nid); //@8
      IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
      this->TreeM->create_tree_node(type_decl_nid, type_decl_K, IR_schema);

      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(type_decl_nid) + " (type_decl int)" );

      ///@8      integer_type     name: @19      size: @11   algn: 32   prec: 32       min : @20      max : @21
      IR_schema.clear();
      IR_schema[TOK(TOK_NAME)] = TOSTRING(type_decl_nid);
      IR_schema[TOK(TOK_SIZE)] = TOSTRING(size_node_nid);
      IR_schema[TOK(TOK_ALGN)] = TOSTRING(ALGN_INT);
      IR_schema[TOK(TOK_PREC)] = TOSTRING(PREC_INT);
      IR_schema[TOK(TOK_UNSIGNED)] = TOSTRING(false);
      IR_schema[TOK(TOK_MIN)] = TOSTRING(min_node_nid);
      IR_schema[TOK(TOK_MAX)] = TOSTRING(max_node_nid);
      this->TreeM->create_tree_node(integer_type_nid, integer_type_K, IR_schema);
      integer_type_node = TreeM->GetTreeReindex(integer_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(integer_type_node)) + " (" + GET_NODE(integer_type_node)->get_kind_text() + " int)" );

      ///@11     integer_cst      type: @18      low : 32
      CreateIntegerCst(bit_size_node, SIZE_VALUE_INT, size_node_nid);

      ///@20     integer_cst      type: @8       high: -1  low : -2147483648
      CreateIntegerCst(integer_type_node, MIN_VALUE_INT, min_node_nid);

      ///@21     integer_cst      type: @8       low : 2147483647
      CreateIntegerCst(integer_type_node, MAX_VALUE_INT, max_node_nid);
   }
   else
   {
      IR_schema.clear();
      IR_schema[TOK(TOK_NAME)] = TOSTRING(type_decl_nid);
      IR_schema[TOK(TOK_ALGN)] = TOSTRING(ALGN_INT);
      IR_schema[TOK(TOK_PREC)] = TOSTRING(PREC_INT);
      IR_schema[TOK(TOK_UNSIGNED)] = TOSTRING(false);

      unsigned int integer_type_nid = this->TreeM->find(integer_type_K,IR_schema);

      if (!integer_type_nid)
         THROW_ERROR("Something wrong happened!");

      integer_type_node = TreeM->GetTreeReindex(integer_type_nid);

      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Found   node " + TOSTRING(GET_INDEX_NODE(integer_type_node)) + " (" + GET_NODE(integer_type_node)->get_kind_text() + " int)" );
   }
   return integer_type_node;
}

///Create a pointer type
tree_nodeRef tree_manipulation::create_pointer_type(const tree_nodeRef & ptd)
{
   ///Check if the tree_node given are tree_reindex
   THROW_ASSERT(ptd->get_kind() == tree_reindex_K, "Node is not a tree reindex");

   ///@15     pointer_type     size: @12      algn: 32       ptd : @9     @9 type of the pointer
   ///@12     integer_cst      type: @26      low : 32       @26 is bit_size_type
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   IR_schema[TOK(TOK_PTD)] = TOSTRING(GET_INDEX_NODE(ptd));
   IR_schema[TOK(TOK_ALGN)] = TOSTRING(ALGN_POINTER);

   tree_nodeRef pointer_type_node;

   unsigned int pointer_type_nid = this->TreeM->find(pointer_type_K,IR_schema);

   if (!pointer_type_nid)
   {
      pointer_type_nid = this->TreeM->new_tree_node_id();
      unsigned int size_node_nid = this->TreeM->new_tree_node_id();

      ///@12     integer_cst      type: @26      low : 32       @26 is bit_size_type
      IR_schema[TOK(TOK_SIZE)] = TOSTRING(size_node_nid);
      this->TreeM->create_tree_node(pointer_type_nid, pointer_type_K, IR_schema);
      pointer_type_node = TreeM->GetTreeReindex(pointer_type_nid);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(pointer_type_nid) + " (pointer_type)" );

      tree_nodeRef bit_size_node = create_bit_size_type();

      ///@12     integer_cst      type: @26      low : 32       @26 is bit_size_type
      CreateIntegerCst(bit_size_node, SIZE_VALUE_POINTER, size_node_nid);

   }
   else
   {
      pointer_type_node = TreeM->GetTreeReindex(pointer_type_nid);
   }
   return pointer_type_node;
}



/// MISCELLANEOUS_OBJ_TREE_NODES


///SSA_NAME

///Create a ssa_name node
tree_nodeRef tree_manipulation::create_ssa_name(const tree_nodeRef & var, const tree_nodeRef & type, bool volatile_flag, bool virtual_flag)
{
   THROW_ASSERT(!var || var->get_kind() == tree_reindex_K, "Var node is not a tree reindex");

   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   unsigned int vers = this->TreeM->get_next_vers();
   unsigned int node_nid = this->TreeM->new_tree_node_id();

   if(type) IR_schema[TOK(TOK_TYPE)] = TOSTRING(GET_INDEX_NODE(type));
   if(var) IR_schema[TOK(TOK_VAR)] = TOSTRING(GET_INDEX_NODE(var));
   IR_schema[TOK(TOK_VERS)] = TOSTRING(vers);
   IR_schema[TOK(TOK_VOLATILE)] = TOSTRING(volatile_flag);
   IR_schema[TOK(TOK_VIRTUAL)] = TOSTRING(virtual_flag);


   this->TreeM->create_tree_node(node_nid, ssa_name_K, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(node_nid);

   const tree_nodeRef curr_node = GET_NODE(node_ref);
   ssa_name * sn = GetPointer<ssa_name>(curr_node);
   sn->virtual_flag = virtual_flag;
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   return node_ref;

}

///Create a ssa_name node
tree_nodeRef tree_manipulation::create_ssa_name(const tree_nodeRef & var, const tree_nodeRef & type, const tree_nodeRef & DEBUG_PARAMETER(ptr_info), bool volatile_flag, bool virtual_flag)
{
   THROW_ASSERT(!var || var->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(ptr_info->get_kind() == tree_reindex_K, "Node is not a tree reindex");


   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   unsigned int vers = this->TreeM->get_next_vers();
   unsigned int node_nid = this->TreeM->new_tree_node_id();

   if(type)IR_schema[TOK(TOK_TYPE)] = TOSTRING(GET_INDEX_NODE(type));
   if(var)IR_schema[TOK(TOK_VAR)] = TOSTRING(GET_INDEX_NODE(var));
   //IR_schema[TOK(TOK_PTR_INFO)] = TOSTRING(GET_INDEX_NODE(use_set));
   IR_schema[TOK(TOK_VERS)] = TOSTRING(vers);
   IR_schema[TOK(TOK_VOLATILE)] = TOSTRING(volatile_flag);
   IR_schema[TOK(TOK_VIRTUAL)] = TOSTRING(virtual_flag);

   this->TreeM->create_tree_node(node_nid, ssa_name_K, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(node_nid);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   return node_ref;
}


///GIMPLE_ASSIGN

///Create a gimple_assign
tree_nodeRef tree_manipulation::create_gimple_modify_stmt( tree_nodeRef & op0,  tree_nodeRef & op1, std::string srcp, unsigned int bb_index)
{
   THROW_ASSERT(op0->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(op1->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(!srcp.empty(), "It requires a non empty string");

   unsigned int node_nid = this->TreeM->new_tree_node_id();
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   IR_schema[TOK(TOK_SRCP)] = srcp;
   IR_schema[TOK(TOK_OP0)] = TOSTRING(GET_INDEX_NODE(op0));
   IR_schema[TOK(TOK_OP1)] = TOSTRING(GET_INDEX_NODE(op1));
   this->TreeM->create_tree_node(node_nid, gimple_assign_K, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(node_nid);
   GetPointer<gimple_node>(GET_NODE(node_ref))->bb_index = bb_index;
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   ///Add definition in ssa_name
   tree_nodeRef op0_node = GET_NODE(op0);
   if (op0_node->get_kind() == ssa_name_K)
   {
      ssa_name * sn = GetPointer<ssa_name> (op0_node);
      sn->add_def(node_ref);
   }

   return node_ref;
}


///GIMPLE_COND

///Create a gimple_cond with one operand (type void)
tree_nodeRef tree_manipulation::create_gimple_cond(const tree_nodeRef & expr, std::string srcp, unsigned int bb_index)
{
   THROW_ASSERT(expr->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(!srcp.empty(), "It requires a non empty string");

   ///schema used to create the nodes
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;

   unsigned int void_type_nid = GET_INDEX_NODE(create_void_type());
   unsigned int expr_nid = GET_INDEX_NODE(expr);
   unsigned int gimple_cond_name_nid = this->TreeM->new_tree_node_id();

   ///@116 gimple_cond type: @62 srcp: "array.c":1 op: @115
   IR_schema.clear();
   IR_schema[TOK(TOK_TYPE)] = TOSTRING(void_type_nid);
   IR_schema[TOK(TOK_OP0)] = TOSTRING(expr_nid);
   IR_schema[TOK(TOK_SRCP)] = srcp;
   this->TreeM->create_tree_node(gimple_cond_name_nid, gimple_cond_K, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(gimple_cond_name_nid);
   GetPointer<gimple_node>(GET_NODE(node_ref))->bb_index = bb_index;
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   return node_ref;
}

///Create gimple_return
tree_nodeRef tree_manipulation::create_return_expr(const tree_nodeRef & decl, tree_nodeRef & op_gimple, std::string srcp, unsigned int bb_index)
{

   THROW_ASSERT(decl->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(op_gimple->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   THROW_ASSERT(!srcp.empty(), "It requires a non empty string");

   unsigned int node_nid = this->TreeM->new_tree_node_id();

   tree_nodeRef void_type = create_void_type();

   tree_nodeRef ssa_new = create_ssa_name(decl, tree_nodeRef());

   tree_nodeRef g_node = create_gimple_modify_stmt(ssa_new,op_gimple,srcp, bb_index);

   unsigned int op_node_nid = GET_INDEX_NODE(g_node);
   unsigned int void_type_nid = GET_INDEX_NODE(void_type);

   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   IR_schema[TOK(TOK_OP)] = TOSTRING(op_node_nid);
   IR_schema[TOK(TOK_TYPE)] = TOSTRING(void_type_nid);
   IR_schema[TOK(TOK_SRCP)] = srcp;

   this->TreeM->create_tree_node(node_nid, gimple_return_K, IR_schema);
   tree_nodeRef node_ref = TreeM->GetTreeReindex(node_nid);
   GetPointer<gimple_node>(GET_NODE(node_ref))->bb_index = bb_index;
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(node_ref)) + " (" + GET_NODE(node_ref)->get_kind_text() + ")" );

   ///Add definition in ssa_name
   tree_nodeRef ssa = GET_NODE(ssa_new);
   ssa_name * sn = GetPointer<ssa_name> (ssa);
   sn->add_def(node_ref);

   return node_ref;
}

///GIMPLE_PHI

///Create a gimple_phi
tree_nodeRef tree_manipulation::create_phi_node(tree_nodeRef & ssa_res, const std::vector<std::pair<tree_nodeRef, unsigned int> > & list_of_def_edge, unsigned int bb_index, bool virtual_flag)
{
   ///Check if the tree_node given are tree_reindex
   ///THROW_ASSERT(ssa_res->get_kind() == tree_reindex_K, "ssa_name res is not a tree_reindex node");

   ///res is the new SSA_NAME node created by the PHI node.
   ///tree_nodeRef res_node = GET_NODE(ssa_res);
   ///THROW_ASSERT(res_node->get_kind() == ssa_name_K, "ssa_name res is not a ssa_name node");
   THROW_ASSERT(list_of_def_edge.size()>1, "Too few edge information");

   std::vector<std::pair<tree_nodeRef, unsigned int> >::const_iterator iterator=list_of_def_edge.begin();
   tree_nodeRef ssa_ref = iterator->first;
   ssa_name * sn_ref = GetPointer<ssa_name>(GET_NODE(ssa_ref));
   THROW_ASSERT(ssa_ref->get_kind() == tree_reindex_K, "ssa_name res is not a tree_reindex node");
   THROW_ASSERT(GET_NODE(ssa_ref)->get_kind() == ssa_name_K, "ssa_name res is not a ssa_name node");
   for (iterator++; iterator != list_of_def_edge.end(); iterator++)
   {
      tree_nodeRef tn=iterator->first;
#ifndef NDEBUG
      THROW_ASSERT(tn->get_kind() == tree_reindex_K, "ssa_name res is not a tree_reindex node");
      THROW_ASSERT(GET_NODE(tn)->get_kind() == ssa_name_K, "ssa_name res is not a ssa_name node");
      ssa_name * sn_temp = GetPointer<ssa_name>(GET_NODE(tn));
      THROW_ASSERT(GET_INDEX_NODE(sn_temp->var) == GET_INDEX_NODE(sn_ref->var), "Phi_node must be on the same variable");
#endif
   }

   ssa_res = create_ssa_name(sn_ref->var,sn_ref->type, sn_ref->volatile_flag,sn_ref->virtual_flag);

   unsigned int phi_node_nid = this->TreeM->new_tree_node_id();

   //Create the gimple_phi
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   IR_schema[TOK(TOK_RES)] = TOSTRING(GET_INDEX_NODE(ssa_res));
   this->TreeM->create_tree_node(phi_node_nid, gimple_phi_K, IR_schema);
   tree_nodeRef phi_stmt = TreeM->GetTreeReindex(phi_node_nid);
   GetPointer<gimple_node>(GET_NODE(phi_stmt))->bb_index = bb_index;

   gimple_phi * pn = GetPointer<gimple_phi> (GET_NODE(phi_stmt));
   pn->virtual_flag = virtual_flag;

   for (iterator = list_of_def_edge.begin(); iterator != list_of_def_edge.end(); iterator++)
   {
      pn->add_def_edge(iterator->first, iterator->second);
   }

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created node " + TOSTRING(GET_INDEX_NODE(phi_stmt)) + " (" + GET_NODE(phi_stmt)->get_kind_text() + ")" );

   ssa_name * ssa_new = GetPointer<ssa_name> (GET_NODE(ssa_res));
   ssa_new->add_def(phi_stmt);

   return phi_stmt;

}

///BASIC_BLOCK

///Create a basic block
blocRef tree_manipulation::create_basic_block(std::map<unsigned int, blocRef> & list_of_bloc, std::vector<unsigned int> predecessors, std::vector<unsigned int> successors,
         std::vector<tree_nodeRef> stmt, unsigned int number, unsigned int true_edge, unsigned int false_edge, std::vector<tree_nodeRef> phi)
{
   ///Create new basic block
   blocRef new_bb = blocRef(new bloc());
   ///Set the number of the block
   new_bb->number = number;
   //Add in the list of block the new bb
   list_of_bloc[new_bb->number] = new_bb;

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created BB " + TOSTRING(new_bb->number));

   ///Set the true_edge
   new_bb->true_edge = true_edge;
   ///Set the false_edge
   new_bb->false_edge = false_edge;

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "   True Edge: " + TOSTRING(new_bb->true_edge) +"  False Edge: " + TOSTRING(new_bb->false_edge));

   ///Insert the list of predecessors
   for (auto pred : predecessors)
   {
      new_bb->list_of_pred.push_back(pred);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "   Added predecessor " + TOSTRING(pred));
   }
   ///Insert the list of successors
   for (auto suc: successors)
   {
      new_bb->list_of_succ.push_back(suc);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "   Added successor   " + TOSTRING(suc));
   }
   ///Insert the list of statement
   for (auto tn : stmt)
   {
      new_bb->add_stmt(tn);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "   Added statement   " + TOSTRING(GET_INDEX_NODE(tn)) + " (" + GET_NODE(tn)->get_kind_text() + ")." );
   }
   ///Insert the list of phi node
   for (auto tn: phi)
   {
      new_bb->add_phi(tn);
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "   Added statement   " + TOSTRING(GET_INDEX_NODE(tn)) + " (" + GET_NODE(tn)->get_kind_text() + ")." );
   }

   return new_bb;
}

///Add a list of phi_nodes in basic block bb
void tree_manipulation::bb_add_phi_nodes(blocRef & bb, const std::vector<tree_nodeRef> & phi_nodes)
{
   ///Insert the list of statement
   for(auto tn : phi_nodes)
   {
      bb_add_phi_node(bb,tn);
   }
}

///Add a gimple_phi in basic block bb
void tree_manipulation::bb_add_phi_node(blocRef & bb, const tree_nodeRef & tn)
{
   bb->add_phi(tn);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "   Added phi node   " + TOSTRING(GET_INDEX_NODE(tn)) + " (" + GET_NODE(tn)->get_kind_text() + ")." );
}

///Add a list of statement in basic block bb
void tree_manipulation::bb_add_stmts(blocRef & bb, const std::vector<tree_nodeRef> & stmts)
{
   ///Insert the list of statement
   for(auto tn : stmts)
   {
      bb_add_stmt(bb, tn);
   }
}

///Add a statement in basic block
void tree_manipulation::bb_add_stmt(blocRef & bb, const tree_nodeRef & stmt)
{
   ///Insert the list of statement
   bb->add_stmt(stmt);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "   Added statement   " + TOSTRING(GET_INDEX_NODE(stmt)) + " (" + GET_NODE(stmt)->get_kind_text() + ")." );
}

///Add a list of successors in basic block bb
void tree_manipulation::bb_add_successors(blocRef & bb, const std::vector<unsigned int> & successors)
{
   for(auto successor : successors)
   {
      bb_add_successor(bb,successor);
   }
}

///Add a successor in basic block bb
void tree_manipulation::bb_add_successor(blocRef & bb, const unsigned int & successor)
{
   bb->add_succ(successor);
}

///Add a list of predecessors in basic block bb
void tree_manipulation::bb_add_predecessors(blocRef & bb, const std::vector<unsigned int> &  predecessors)
{
   for(auto predecessor : predecessors)
   {
      bb_add_predecessor(bb,predecessor);
   }
}

///Add a predecessor in basic block bb
void tree_manipulation::bb_add_predecessor(blocRef & bb, const unsigned int & predecessor)
{
   bb->add_pred(predecessor);
}

///Remove a list of phi_nodes from basic block bb
void tree_manipulation::bb_remove_phi_nodes(blocRef & bb, const std::vector<tree_nodeRef> & phi_nodes)
{
   ///Remove each operation defined in stmts_to_remove
   for(auto tn : phi_nodes)
   {
      bb_remove_phi_node(bb,tn);
   }
}

///Remove a gimple_phi from basic block bb
void tree_manipulation::bb_remove_phi_node(blocRef & bb, const tree_nodeRef & tn)
{
   std::vector<tree_nodeRef> & list_of_phi = bb->list_of_phi;
   std::vector<tree_nodeRef>::iterator iterator_stmt = std::find(list_of_phi.begin(),list_of_phi.end(),tn);
   THROW_ASSERT(iterator_stmt!=list_of_phi.end(),"phi node not found!");
   list_of_phi.erase(iterator_stmt);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "   Node " + TOSTRING(GET_INDEX_NODE(tn)) + " (" + GET_NODE(tn)->get_kind_text() + ") is removed");
}

///Remove a list of statement from basic block bb
void tree_manipulation::bb_remove_stmts(blocRef & bb, const std::vector<tree_nodeRef> & stmt)
{
   ///Remove each operation defined in stmts_to_remove
   for(auto tn : stmt)
   {
      bb_remove_stmt(bb,tn);
   }
}

///Remove a statement from bb
void tree_manipulation::bb_remove_stmt(blocRef & bb, const tree_nodeRef & stmt)
{
   ///Retrieve the list of statement
   std::list<tree_nodeRef> & list_of_stmt = bb->list_of_stmt;
   std::list<tree_nodeRef>::iterator iterator_stmt = std::find(list_of_stmt.begin(),list_of_stmt.end(),stmt);
   THROW_ASSERT(iterator_stmt!=list_of_stmt.end(),"Statement not found!");
   list_of_stmt.erase(iterator_stmt);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "   Node " + TOSTRING(GET_INDEX_NODE(stmt)) + " (" + GET_NODE(stmt)->get_kind_text() + ") is removed");
}

///Remove a list of successors from basic block bb
void tree_manipulation::bb_remove_successors(blocRef & bb, const std::vector<unsigned int> & successors)
{
   for(auto successor : successors)
   {
      bb_remove_successor(bb,successor);
   }
}

///Remove a successor from basic block bb
void tree_manipulation::bb_remove_successor(blocRef & bb, const unsigned int & successor)
{
   std::vector<unsigned int> & list_of_succ = bb->list_of_succ;
   std::vector<unsigned int>::iterator iterator_stmt = std::find(list_of_succ.begin(), list_of_succ.end(), successor);
   THROW_ASSERT(iterator_stmt!=list_of_succ.end(),"Successor not found!");
   list_of_succ.erase(iterator_stmt);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Successor " + TOSTRING(*iterator_stmt) + " is removed");
}

///Remove a list of predecessors in basic block bb.
void tree_manipulation::bb_remove_predecessors(blocRef & bb, const std::vector<unsigned int> & predecessors)
{
   for(auto predecessor : predecessors)
   {
      bb_remove_predecessor(bb,predecessor);
   }
}

///Remove a predecessors in basic block bb.
void tree_manipulation::bb_remove_predecessor(blocRef & bb, const unsigned int & predecessor)
{
   std::vector<unsigned int> & list_of_pred = bb->list_of_pred;
   std::vector<unsigned int>::iterator iterator_stmt = std::find(list_of_pred.begin(), list_of_pred.end(), predecessor);
   THROW_ASSERT(iterator_stmt!=list_of_pred.end(),"Successor not found!");
   list_of_pred.erase(iterator_stmt);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Successor " + TOSTRING(*iterator_stmt) + " is removed");
}

///Remove all the phi_nodes from basic block bb
void tree_manipulation::bb_remove_all_phi_nodes(blocRef & bb)
{
   bb->list_of_phi.erase(bb->list_of_phi.begin(), bb->list_of_phi.end());
}

///Remove all the statements from basic block bb
void tree_manipulation::bb_remove_all_stmts(blocRef & bb)
{
   std::list<tree_nodeRef> & list_of_stmt = bb->list_of_stmt;
   list_of_stmt.erase(list_of_stmt.begin(),list_of_stmt.end());
}

///Remove all the predecessors from basic block bb
void tree_manipulation::bb_remove_all_predecessors(blocRef & bb)
{
   bb->list_of_pred.erase(bb->list_of_pred.begin(), bb->list_of_pred.end());
}

///Remove all the successors from basic block bb
void tree_manipulation::bb_remove_all_successors(blocRef & bb)
{
   bb->list_of_succ.erase(bb->list_of_succ.begin(), bb->list_of_succ.end());
}

///Set the false edge of basic block bb
void tree_manipulation::bb_set_false_edge(blocRef & bb,const unsigned int & false_edge_index)
{
   bb->false_edge = false_edge_index;
}

///Set the true edge of basic block bb
void tree_manipulation::bb_set_true_edge(blocRef & bb, const unsigned int & true_edge_index)
{
   bb->true_edge = true_edge_index;
}

///Set number of basic block bb
void tree_manipulation::bb_set_number(blocRef & bb, const unsigned int & number)
{
   bb->number = number;
}


///UTILITY

///Replace the occurrences of tree node old_node with new_node in statement identified by tn.
bool tree_manipulation::update_ssa(tree_nodeRef & tn, tree_nodeRef old_node, tree_nodeRef new_node)
{
   THROW_ASSERT(tn->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   ///Variable used to check if a ssa_name has been updated
   bool updated = false;
   tree_nodeRef curr_tn = GET_NODE(tn);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "      Expanding node " + TOSTRING(GET_INDEX_NODE(tn)) + " (" + curr_tn->get_kind_text() + "). " );
   ///If the tree_node is a ssa_name check if the ssa_name needs to be updated or not.
   ///Instead if it is another operation call recursively this function
   switch (curr_tn->get_kind())
   {
      case gimple_assign_K:
      {
         gimple_assign * gm = GetPointer<gimple_assign> (curr_tn);
         updated = (update_ssa(gm->op1, old_node, new_node) || updated);
         ///If it is a store or load operation
         if (gm->vuse)
            update_ssa(gm->vuse, old_node, new_node);
         if (gm->vdef)
            update_ssa(gm->vdef, old_node, new_node);
         if (gm->redef_vuse)
            update_ssa(gm->redef_vuse, old_node, new_node);
         std::vector<tree_nodeRef> & uses = gm->use_set->variables;
         std::vector<tree_nodeRef>::iterator use, use_end = uses.end();
         for(use = uses.begin(); use != use_end; use++)
         {
            updated = (update_ssa(*use, old_node, new_node) or updated);
         }
         std::vector<tree_nodeRef> & clbs = gm->clobbered_set->variables;
         std::vector<tree_nodeRef>::iterator clb, clb_end = clbs.end();
         for(clb = clbs.begin(); clb != clb_end; clb++)
         {
            updated = (update_ssa(*clb, old_node, new_node) or updated);
         }
         break;
      }
      case gimple_cond_K:
      {
         gimple_cond* gc = GetPointer<gimple_cond>(curr_tn);
         updated = (update_ssa(gc->op0, old_node, new_node) || updated);
         break;
      }
      case gimple_multi_way_if_K:
      {
         gimple_multi_way_if * gmwi = GetPointer<gimple_multi_way_if>(curr_tn);
         for(auto cond : gmwi->list_of_cond)
         {
            if(cond.first)
               updated = (update_ssa(cond.first, old_node, new_node)) or updated;
         }
         break;
      }
      case CASE_UNARY_EXPRESSION:
      {
         if(curr_tn->get_kind() == addr_expr_K)
         {
            if(already_visited.find(curr_tn) != already_visited.end())
            {
               break;
            }
            already_visited.insert(curr_tn);
         }
         unary_expr * ue = GetPointer<unary_expr>(curr_tn);
         updated = (update_ssa(ue->op, old_node, new_node) || updated);
         break;
      }
      case CASE_BINARY_EXPRESSION:
      {
         binary_expr* be = GetPointer<binary_expr>(curr_tn);
         updated = (update_ssa(be->op0, old_node, new_node) || updated);
         updated = (update_ssa(be->op1, old_node, new_node) || updated);
         break;
      }
      case gimple_switch_K:
      {
         gimple_switch* se = GetPointer<gimple_switch>(curr_tn);
         updated = (update_ssa(se->op0, old_node, new_node) || updated);
         break;
      }
      case CASE_TERNARY_EXPRESSION:
      {
         ternary_expr * te = GetPointer<ternary_expr>(curr_tn);
         updated = (update_ssa(te->op0, old_node, new_node) || updated);
         updated = (update_ssa(te->op1, old_node, new_node) || updated);
         if(te->op2)
         updated=(update_ssa(te->op2, old_node, new_node) || updated);
         break;
      }
      case CASE_QUATERNARY_EXPRESSION:
      {
         quaternary_expr * qe = GetPointer<quaternary_expr>(curr_tn);
         updated = (update_ssa(qe->op0, old_node, new_node) || updated);
         updated = (update_ssa(qe->op1, old_node, new_node) || updated);
         if(qe->op2)
         updated=(update_ssa(qe->op2, old_node, new_node) || updated);
         if(qe->op3)
         updated=(update_ssa(qe->op3, old_node, new_node) || updated);
         break;
      }
      case gimple_phi_K:
      {
         gimple_phi * pn = GetPointer<gimple_phi>(curr_tn);
         for(unsigned int i=0; i < pn->list_of_def_edge.size();i++)
         updated = (update_ssa(pn->list_of_def_edge[i].first,old_node,new_node) || updated);
         break;
      }
      case ssa_name_K:
      {
         if(GET_INDEX_NODE(tn) == GET_INDEX_NODE(old_node))
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "      Updating ssa_name.  " + boost::lexical_cast<std::string>(GET_INDEX_NODE(tn)) + " --> " + boost::lexical_cast<std::string>(GET_INDEX_NODE(new_node)));
            updated = true;
            tn = new_node;
         }
         break;
      }
      case call_expr_K:
      {
         call_expr* ce = GetPointer<call_expr>(curr_tn);
         std::vector<tree_nodeRef> & args = ce->args;
         std::vector<tree_nodeRef>::iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            updated = (update_ssa(*arg, old_node, new_node) || updated);
         }
         break;
      }
      case gimple_call_K:
      {
         gimple_call* ce = GetPointer<gimple_call>(curr_tn);
         std::vector<tree_nodeRef> & args = ce->args;
         std::vector<tree_nodeRef>::iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            updated = (update_ssa(*arg, old_node, new_node) || updated);
         }
         std::vector<tree_nodeRef> & uses = ce->use_set->variables;
         std::vector<tree_nodeRef>::iterator use, use_end = uses.end();
         for(use = uses.begin(); use != use_end; use++)
         {
            updated = (update_ssa(*use, old_node, new_node) or updated);
         }
         std::vector<tree_nodeRef> & clbs = ce->clobbered_set->variables;
         std::vector<tree_nodeRef>::iterator clb, clb_end = clbs.end();
         for(clb = clbs.begin(); clb != clb_end; clb++)
         {
            updated = (update_ssa(*clb, old_node, new_node) or updated);
         }
         break;
      }
      case gimple_return_K:
      {
         gimple_return *re = GetPointer<gimple_return>(curr_tn);
         updated = (update_ssa(re->op, old_node, new_node) || updated);
         break;
      }
      case var_decl_K:
      case result_decl_K:
      case parm_decl_K:
      case integer_cst_K:
      case real_cst_K:
      case string_cst_K:
      case vector_cst_K:
      case complex_cst_K:
      case field_decl_K:
      case label_decl_K:
      case gimple_asm_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case function_decl_K:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case case_label_expr_K:
      case const_decl_K:
      case constructor_K:
      case gimple_bind_K:
      case gimple_for_K:
      case gimple_goto_K:
      case gimple_label_K:
      case gimple_nop_K:
      case gimple_pragma_K:
      case gimple_predict_K:
      case gimple_resx_K:
      case gimple_while_K:
      case identifier_node_K:
      case namespace_decl_K:
      case statement_list_K:
      case translation_unit_decl_K:
      case tree_list_K:
      case tree_vec_K:
      case type_decl_K:
      case CASE_CPP_NODES:
      case CASE_FAKE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_TYPE_NODES:
         THROW_UNREACHABLE(std::string("Node not supported (") + TOSTRING(GET_INDEX_NODE(tn)) + std::string("): ") + curr_tn->get_kind_text());
      default:
         {
            THROW_UNREACHABLE("");
         }
   }
   return updated;
}

///update ssa_name in basic_block bb and its successors
bool tree_manipulation::update_block(std::map<unsigned int, blocRef> & list_of_bloc, blocRef & bb, tree_nodeRef old_node, tree_nodeRef new_node, bool update_phi)
{
   stack_block.push_front(bb);
   ///Variable used to check if a ssa_name has been updated
   bool updated = false;
   ///If all the basic_blocks have been visited finish the recursion (used to avoid endless loops)
   if (bb_already_visited.find(bb) != bb_already_visited.end())
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "BB " +TOSTRING(bb->number) +" was visited");
      return updated;
   }
   ///Otherwise insert in bb_already_visited the node that is in analysis
   bb_already_visited.insert(bb);

   PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, " Checking BB  " + TOSTRING(bb->number));

   ///If the caller of the function is not the root of iteration (convergence block), than update the phi_nodes
   if (update_phi)
   {
      for(auto phi : bb->list_of_phi)
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "   Searching in " + TOSTRING(GET_INDEX_NODE(phi)) + " (ssa_name) with " + TOSTRING(GET_INDEX_NODE(phi)) + " (ssa_name)");
         updated = (update_ssa(phi,old_node,new_node)|| updated);
      }
   }
   ///Update the tree_node in list_of_stmt
   for(auto stmt : bb->list_of_stmt)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "   Searching in " + TOSTRING(GET_INDEX_NODE(stmt)) + " (ssa_name) with " + TOSTRING(GET_INDEX_NODE(stmt)) + " (ssa_name)");
      updated= (update_ssa(stmt,old_node,new_node)||updated);
   }

   ///Calculate the successors of basick block bb
   for(unsigned int succ=0; succ < bb->list_of_succ.size(); succ++)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "BB " + TOSTRING(bb->list_of_succ[succ]) + " is a successors of BB  " + TOSTRING(bb->number));
      ///If the successors of basick block bb are either ENTRY_BLOCK or EXIT_BLOCK skip the recursion
      if(bb->list_of_succ[succ]==bloc::ENTRY_BLOCK_ID || bb->list_of_succ[succ]==bloc::EXIT_BLOCK_ID)
      continue;
      blocRef succ_bb = list_of_bloc[bb->list_of_succ[succ]];
      ///Call this function recursively on the successor succ_bb
      updated= (update_block(list_of_bloc, succ_bb ,old_node,new_node) || updated);
   }
   stack_block.pop_front();

   if (stack_block.empty())
   {
      bb_already_visited.erase( bb_already_visited.begin(), bb_already_visited.end());
   }

   return updated;
}

void tree_manipulation::create_label(const blocRef block, const unsigned int function_decl_nid)
{
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   ///check if the first statement is a gimple_label but with a unnamed label_decl.
   if (!block->list_of_stmt.empty())
   {
      tree_nodeRef first_stmt = block->list_of_stmt.front();
      gimple_label * le = GetPointer<gimple_label>(GET_NODE(first_stmt));
      if (le)
      {
         THROW_ASSERT(le->op && GET_NODE(le->op)->get_kind() == label_decl_K, "label expression pattern not yet supported");
         label_decl * ld = GetPointer<label_decl>(GET_NODE(le->op));
         THROW_ASSERT(!ld->name, "gimple_label already added");
         /// addd the name to the label_decl
         //@436     identifier_node  strg: "GOTOLABEL0"   lngt: 10
         unsigned int label_decl_name_nid_test;
         do
         {
            IR_schema[TOK(TOK_STRG)] = "GOTOLABEL" + boost::lexical_cast<std::string>(goto_label_unique_id++);
            label_decl_name_nid_test = TreeM->find(identifier_node_K, IR_schema);
         }
         while (label_decl_name_nid_test);
         unsigned int label_decl_name_nid = TreeM->new_tree_node_id();//436
         TreeM->create_tree_node(label_decl_name_nid, identifier_node_K, IR_schema);
         IR_schema.clear();
         tree_nodeRef tr_new_label_name = TreeM->GetTreeReindex(label_decl_name_nid);
         ld->name = tr_new_label_name;
         ld->artificial_flag = false;
         return;
      }
   }

   //@27     identifier_node  strg: "void"   lngt: 4
   IR_schema[TOK(TOK_STRG)] = "void";
   unsigned int type_decl_name_nid = TreeM->find(identifier_node_K, IR_schema);
   if (!type_decl_name_nid)
   {
      type_decl_name_nid = TreeM->new_tree_node_id();//27
      TreeM->create_tree_node(type_decl_name_nid, identifier_node_K, IR_schema);
   }
   IR_schema.clear();

   //@436     identifier_node  strg: "GOTOLABEL0"   lngt: 10
   unsigned int label_decl_name_nid_test;
   do
   {
      IR_schema[TOK(TOK_STRG)] = "GOTOLABEL" + boost::lexical_cast<std::string>(goto_label_unique_id++);
      label_decl_name_nid_test = TreeM->find(identifier_node_K, IR_schema);
   }
   while (label_decl_name_nid_test);
   unsigned int label_decl_name_nid = TreeM->new_tree_node_id();//436
   TreeM->create_tree_node(label_decl_name_nid, identifier_node_K, IR_schema);
   IR_schema.clear();

   //@23     type_decl        name: @27      type: @15      srcp: "<built-in>":0
   IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(type_decl_name_nid);
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   unsigned int label_void_type_name_nid = TreeM->find(type_decl_K, IR_schema);
   unsigned int label_type_nid;
   if (!label_void_type_name_nid)
   {
      label_void_type_name_nid = TreeM->new_tree_node_id();//23
      label_type_nid = TreeM->new_tree_node_id();//15
      IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(label_type_nid);
      TreeM->create_tree_node(label_void_type_name_nid, type_decl_K, IR_schema);
      IR_schema.clear();
      //@15     void_type        name: @23      algn: 8
      IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(label_void_type_name_nid);
      IR_schema[TOK(TOK_ALGN)] = boost::lexical_cast<std::string>(8);
      TreeM->create_tree_node(label_type_nid, void_type_K, IR_schema);
   }
   else
   {
      IR_schema.clear();
      //@15     void_type        name: @23      algn: 8
      IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(label_void_type_name_nid);
      IR_schema[TOK(TOK_ALGN)] = boost::lexical_cast<std::string>(8);
      label_type_nid = TreeM->find(void_type_K, IR_schema);
      if (!label_type_nid)
      {
         label_type_nid = TreeM->new_tree_node_id();//15
         label_void_type_name_nid = TreeM->new_tree_node_id();//23
         IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(label_void_type_name_nid);
         TreeM->create_tree_node(label_type_nid, void_type_K, IR_schema);
         IR_schema.clear();
         IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(type_decl_name_nid);
         IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(label_type_nid);
         IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
         TreeM->create_tree_node(label_void_type_name_nid, type_decl_K, IR_schema);
      }
      else
      {
         THROW_ASSERT(label_void_type_name_nid == GET_INDEX_NODE(GetPointer<void_type>(TreeM->get_tree_node_const(label_type_nid))->name), "somenthing of wrong happen");
      }
   }
   IR_schema.clear();

   //@77     gimple_label       type: @15      srcp: "<built-in>":0     op  : @242
   unsigned int label_expr_nid = TreeM->new_tree_node_id();//77

   unsigned int label_decl_nid = TreeM->new_tree_node_id();//242
   IR_schema[TOK(TOK_OP)] = boost::lexical_cast<std::string>(label_decl_nid);
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   TreeM->create_tree_node(label_expr_nid, gimple_label_K, IR_schema);
   IR_schema.clear();

   //@242    label_decl       name: @436     type: @15      scpe: @32     srcp: "<built-in>":0                UID : 5
   IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(label_decl_name_nid);
   IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(label_type_nid);
   IR_schema[TOK(TOK_SCPE)] = boost::lexical_cast<std::string>(function_decl_nid);
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   TreeM->create_tree_node(label_decl_nid, label_decl_K, IR_schema);
   IR_schema.clear();

   tree_nodeRef tr_new_stmt = TreeM->GetTreeReindex(label_expr_nid);
   GetPointer<gimple_node>(GET_NODE(tr_new_stmt))->bb_index = block->number;
   block->add_stmt_in_front(tr_new_stmt);
#if 0
   static int nid = 0;
   std::string raw_file_name = boost::lexical_cast<std::string>(nid++) + "create_label.raw";
   std::ofstream raw_file(raw_file_name.c_str());
   raw_file << TreeM;
   raw_file.close();
#endif
}

void tree_manipulation::create_goto(const blocRef block, const unsigned int, const unsigned int label_expr_nid)
{
   TreeM->add_goto();
   ///compute the label_decl_nid
   gimple_label * le = GetPointer<gimple_label>(TreeM->get_tree_node_const(label_expr_nid));
   THROW_ASSERT(le, "expected a label expression");
   unsigned int label_decl_nid = GET_INDEX_NODE(le->op);
   ///create the gimple_goto tree node
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;

   //@27     identifier_node  strg: "void"   lngt: 4
   IR_schema[TOK(TOK_STRG)] = "void";
   unsigned int type_decl_name_nid = TreeM->find(identifier_node_K, IR_schema);
   if (!type_decl_name_nid)
   {
      type_decl_name_nid = TreeM->new_tree_node_id();//27
      TreeM->create_tree_node(type_decl_name_nid, identifier_node_K, IR_schema);
   }
   IR_schema.clear();

   //@23     type_decl        name: @27      type: @15      srcp: "<built-in>":0
   IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(type_decl_name_nid);
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   unsigned int label_void_type_name_nid = TreeM->find(type_decl_K, IR_schema);
   unsigned int label_type_nid;
   if (!label_void_type_name_nid)
   {
      label_void_type_name_nid = TreeM->new_tree_node_id();//23
      label_type_nid = TreeM->new_tree_node_id();//15
      IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(label_type_nid);
      TreeM->create_tree_node(label_void_type_name_nid, type_decl_K, IR_schema);
      IR_schema.clear();
      //@15     void_type        name: @23      algn: 8
      IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(label_void_type_name_nid);
      IR_schema[TOK(TOK_ALGN)] = boost::lexical_cast<std::string>(8);
      TreeM->create_tree_node(label_type_nid, void_type_K, IR_schema);
   }
   else
   {
      IR_schema.clear();
      //@15     void_type        name: @23      algn: 8
      IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(label_void_type_name_nid);
      IR_schema[TOK(TOK_ALGN)] = boost::lexical_cast<std::string>(8);
      label_type_nid = TreeM->find(void_type_K, IR_schema);
      if (!label_type_nid)
      {
         label_type_nid = TreeM->new_tree_node_id();//15
         label_void_type_name_nid = TreeM->new_tree_node_id();//23
         IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(label_void_type_name_nid);
         TreeM->create_tree_node(label_type_nid, void_type_K, IR_schema);
         IR_schema.clear();
         IR_schema[TOK(TOK_NAME)] = boost::lexical_cast<std::string>(type_decl_name_nid);
         IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(label_type_nid);
         IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
         TreeM->create_tree_node(label_void_type_name_nid, type_decl_K, IR_schema);
      }
      else
      {
         THROW_ASSERT(label_void_type_name_nid == GET_INDEX_NODE(GetPointer<void_type>(TreeM->get_tree_node_const(label_type_nid))->name), "somenthing of wrong happen");
      }
   }
   IR_schema.clear();

   //@60     gimple_goto        type: @15      srcp: "<built-in>":6      op  : @242
   unsigned int goto_expr_nid = TreeM->new_tree_node_id();//60
   IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(label_type_nid);
   IR_schema[TOK(TOK_OP)] = boost::lexical_cast<std::string>(label_decl_nid);
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   TreeM->create_tree_node(goto_expr_nid, gimple_goto_K, IR_schema);
   IR_schema.clear();

   tree_nodeRef tr_new_stmt = TreeM->GetTreeReindex(goto_expr_nid);
   GetPointer<gimple_node>(GET_NODE(tr_new_stmt))->bb_index = block->number;
   block->add_stmt(tr_new_stmt);
}
