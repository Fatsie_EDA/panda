/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file Parameter.hpp
 * @brief this class is used to manage the command-line or XML options. It has to be specialized with respect to the tool
 *
 *
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef PARAMETER_HPP
#define PARAMETER_HPP

///Autoheader include
#include "config_HAVE_DESIGN_ANALYSIS_BUILT.hpp"
#include "config_HAVE_DIOPSIS.hpp"
#include "config_HAVE_FROM_C_BUILT.hpp"
#include "config_HAVE_TO_C_BUILT.hpp"
#include "config_RELEASE.hpp"

///STL include
#include <map>
#include <unordered_set>
#include <vector>

///Utility include
#include <boost/algorithm/string.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <climits>
#include "exceptions.hpp"
#include "global_enums.hpp"
#include "refcount.hpp"

/// forward decl of xml Element
class xml_element;

/// An integer value to return if parameters have been right parsed
#define PARAMETER_PARSED INT_MIN
#define PARAMETER_NOTPARSED INT_MAX

#define BAMBU_OPTIONS \
(chaining) \
(chaining_algorithm) \
(controller_architecture) \
(datapath_architecture) \
(evaluation) \
(evaluation_method) \
(evaluation_objectives) \
(export_core) \
(export_core_mode) \
(fu_binding_algorithm) \
(generate_testbench) \
(generate_vcd) \
(hls_flow) \
(interface) \
(interface_type) \
(data_bus_bitsize) \
(addr_bus_bitsize) \
(liveness_algorithm) \
(scheduling_priority) \
(scheduling_algorithm) \
(simulate) \
(simulator) \
(simulation_output) \
(speculative) \
(storage_value_insertion_algorithm) \
(stg) \
(stg_algorithm) \
(register_allocation_algorithm) \
(register_grouping) \
(resp_model) \
(datapath_interconnection_algorithm) \
(insert_memory_profile) \
(timing_simulation) \
(top_file) \
(assert_debug) \
(memory_allocation_algorithm)\
(memory_allocation_policy)\
(xml_memory_allocation) \
(base_address)\
(sync_reset)\
(clock_period_resource_fraction)\
(channels_type)\
(channels_number)\
(memory_controller_type)\
(soft_float)\
(max_sim_cycles)\
(sparse_memory)\
(max_ulp)\
(skip_pipe_parameter)\
(gcc_single_write)

#define FRAMEWORK_OPTIONS \
(architecture) \
(benchmark_name) \
(cat_args) \
(compatible_compilers) \
(compute_size_of) \
(debug_level)\
(default_compiler) \
(dot_directory) \
(dump_profiling_data) \
(file_costs) \
(host_compiler) \
(ilp_max_time) \
(ilp_solver) \
(input_file) \
(input_format) \
(model_costs) \
(no_clean) \
(no_parse_files) \
(output_file) \
(output_level) \
(output_temporary_directory) \
(output_directory) \
(parse_pragma) \
(pretty_print) \
(print_dot) \
(profiling_file) \
(profiling_method) \
(program_name) \
(read_parameter_xml) \
(revision) \
(seed) \
(task_threshold) \
(top_function_name) \
(use_raw) \
(use_rtl) \
(xml_input_configuration) \
(xml_output_configuration) \
(write_parameter_xml)

#define GCC_OPTIONS \
(gcc_config) \
(gcc_costs) \
(gcc_defines) \
(gcc_extra_options) \
(gcc_include_sysdir) \
(gcc_includes) \
(gcc_libraries) \
(gcc_library_directories) \
(gcc_opt_level) \
(gcc_optimizations) \
(gcc_optimization_set) \
(gcc_parameters) \
(gcc_plugindir) \
(gcc_read_xml) \
(gcc_standard) \
(gcc_undefines) \
(gcc_warnings) \
(gcc_c) \
(gcc_E) \
(gcc_S) \
(gcc_write_xml)

#define GECCO_OPTIONS \
(algorithms) \
(analyses)

#define KOALA_OPTIONS \
(aig_analysis) \
(aig_analysis_algorithm) \
(apply_reduction_to_standard_library) \
(characterization_with_DC) \
(circuit_debug_level) \
(complete_library_post_covering) \
(complete_library_pre_covering) \
(covering) \
(csv_file) \
(design_compiler_effort) \
(drive_strength_values) \
(equation) \
(evolutionary_reduction) \
(explore_cell_variants) \
(extract_features) \
(generated_library_name) \
(group_glue) \
(has_complete_characterization) \
(hdl_backend) \
(icarus_debug_level) \
(input_libraries)\
(library) \
(library_optimization) \
(library_optimization_algorithm) \
(lib_output_format) \
(max_area) \
(max_delay) \
(output_libraries) \
(output_name) \
(regularity_abstraction_level) \
(regularity_algorithm) \
(regularity_coloring_type) \
(regularity_covering) \
(regularity_extraction) \
(regularity_fast) \
(regularity_forward) \
(regularity_hierarchical) \
(regularity_include_sequential) \
(regularity_max_inputs) \
(regularity_min_frequency) \
(regularity_min_size) \
(regularity_window_size) \
(reordering) \
(perform_resynthesis) \
(print_templates) \
(reimplement_standard_cells) \
(separate_templates) \
(set_constraint) \
(set_optimization_goal) \
(skew_values) \
(split_roots) \
(store_library_creator_db) \
(synthesis_tool_xml) \
(template_file) \
(xml_library_cells) \
(xml_library_statistics)

#define SYNTHESIS_OPTIONS \
(clock_period) \
(design_analysis_steps) \
(design_compiler_compile_log) \
(design_compiler_split_log) \
(design_parameters) \
(design_hierarchy) \
(device_string) \
(dump_genlib) \
(estimate_library) \
(estimation_output) \
(export_ip_core) \
(import_ip_core) \
(input_liberty_library_file) \
(ip_xact_architecture_template) \
(ip_xact_parameters) \
(is_structural) \
(lib2xml) \
(min_metric) \
(parse_edif) \
(perform_synthesis) \
(rtl) \
(synthesis_flow) \
(structural_HDL) \
(target_device) \
(target_library) \
(target_library_source) \
(target_technology) \
(target_technology_file) \
(target_device_file) \
(target_device_script) \
(target_device_type) \
(top_component) \
(uniquify) \
(writer_language)

#define SPIDER_OPTIONS \
(accuracy) \
(aggregated_features) \
(cross_validation) \
(interval_level) \
(latex_format_file) \
(max_bound) \
(maximum_error) \
(min_bound) \
(minimum_significance) \
(normalization_file) \
(normalization_sequences) \
(output_format) \
(precision) \
(processing_element_type) \
(skip_rows) \
(surviving_benchmarks)

#define EUCALIPTUS_OPTIONS \
(component_name)

#define TREE_PANDA_GCC_OPTIONS \
(archive_files) \
(obj_files)\
(compress_archive)

#define ZEBU_OPTIONS \
(alternative_metrics) \
(analysis_level) \
(blackbox) \
(cache_analysis) \
(compare_model_max_iterations) \
(compare_measure_regions) \
(compare_models) \
(cpus_number) \
(cuda_optimization) \
(examined_model) \
(default_fork_cost) \
(diopsis_instrumentation) \
(driving_component_type) \
(driving_metric) \
(dump_schedule) \
(evaluate_pointed_size) \
(exec_argv) \
(frontend_statistics) \
(golden_model) \
(file_input_data) \
(fork_join_backend) \
(hand_mapping) \
(ignore_mapping) \
(ignore_parallelism) \
(mapping) \
(measure_profile_overhead) \
(no_sequential) \
(normalize_models) \
(partitioning) \
(partitioning_algorithm) \
(partitioning_functions) \
(path) \
(performance_estimation) \
(platform_base_dir) \
(platform_generation) \
(prof_resolution) \
(profile_loop_max_iterations) \
(runs_number) \
(simit_fork_cost) \
(source_code_statistics) \
(resolution) \
(run) \
(sequence_length) \
(shorter_sequence) \
(symbolic_simulation) \
(trace_buffer_size) \
(tsim_instrumentation) \
(tollerance) \
(without_operating_system) \
(without_transformation)

#define OPTIONS_ENUM(r, data, elem) BOOST_PP_CAT(OPT_, elem),

///Possible options
enum enum_option {
   BOOST_PP_SEQ_FOR_EACH(OPTIONS_ENUM, BOOST_PP_EMPTY, BAMBU_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTIONS_ENUM, BOOST_PP_EMPTY, EUCALIPTUS_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTIONS_ENUM, BOOST_PP_EMPTY, FRAMEWORK_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTIONS_ENUM, BOOST_PP_EMPTY, GCC_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTIONS_ENUM, BOOST_PP_EMPTY, GECCO_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTIONS_ENUM, BOOST_PP_EMPTY, KOALA_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTIONS_ENUM, BOOST_PP_EMPTY, SPIDER_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTIONS_ENUM, BOOST_PP_EMPTY, SYNTHESIS_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTIONS_ENUM, BOOST_PP_EMPTY, TREE_PANDA_GCC_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTIONS_ENUM, BOOST_PP_EMPTY, ZEBU_OPTIONS)
};

class OptionMap : public std::map<std::string, std::string>
{
   public:
      /**
       * Constructor
       */
      OptionMap() { }

      /**
       * Destructor
       */
      ~OptionMap() { }
};

#define DEFAULT_OPT_BASE 512
#define OPT_READ_PARAMETERS_XML            DEFAULT_OPT_BASE
#define OPT_WRITE_PARAMETERS_XML           DEFAULT_OPT_BASE+1
#define OPT_DEBUG_CLASSES                  DEFAULT_OPT_BASE+2
#define OPT_BENCHMARK_NAME                 DEFAULT_OPT_BASE+3
#define OPT_BENCHMARK_FAKE_PARAMETERS      DEFAULT_OPT_BASE+4
#define INPUT_OPT_ERROR_ON_WARNING         DEFAULT_OPT_BASE+5
#define OPT_OUTPUT_TEMPORARY_DIRECTORY     DEFAULT_OPT_BASE+6
#define INPUT_OPT_PRINT_DOT                DEFAULT_OPT_BASE+7
#define INPUT_OPT_SEED                     DEFAULT_OPT_BASE+8
#define INPUT_OPT_NO_CLEAN                 DEFAULT_OPT_BASE+9

///define the default tool short option string
#define COMMON_SHORT_OPTIONS_STRING "hVv:d:"

///define default TOOL long options
#define COMMON_LONG_OPTIONS \
   { "help", no_argument, nullptr, 'h'},\
   { "verbosity", required_argument, nullptr, 'v'},\
   { "version", no_argument, nullptr, 'V'},\
   { "read-parameters-XML", required_argument, nullptr, OPT_READ_PARAMETERS_XML},\
   { "write-parameters-XML", required_argument, nullptr, OPT_WRITE_PARAMETERS_XML}, \
   { "debug", required_argument, nullptr, 'd'}, \
   { "debug-classes", required_argument, nullptr, OPT_DEBUG_CLASSES}, \
   { "no-clean", no_argument, nullptr, INPUT_OPT_NO_CLEAN}, \
   { "benchmark-name", required_argument, nullptr, OPT_BENCHMARK_NAME}, \
   { "benchmark-fake-parameters", required_argument, nullptr, OPT_BENCHMARK_FAKE_PARAMETERS}, \
   { "output-temporary-directory", required_argument, nullptr, OPT_OUTPUT_TEMPORARY_DIRECTORY}, \
   { "error-on-warning", no_argument, nullptr, INPUT_OPT_ERROR_ON_WARNING}, \
   { "print-dot", no_argument, nullptr, INPUT_OPT_PRINT_DOT}, \
   { "seed", required_argument, nullptr, INPUT_OPT_SEED}



#define INPUT_OPT_CUSTOM_OPTIONS 1024
#define INPUT_OPT_COMPUTE_SIZEOF 1+INPUT_OPT_CUSTOM_OPTIONS
#define INPUT_OPT_COMPILER 1+INPUT_OPT_COMPUTE_SIZEOF
#define INPUT_OPT_GCC_CONFIG     1+INPUT_OPT_COMPILER
#define INPUT_OPT_INCLUDE_SYSDIR 1+INPUT_OPT_GCC_CONFIG
#define INPUT_OPT_PARAM          1+INPUT_OPT_INCLUDE_SYSDIR
#define INPUT_OPT_READ_GCC_XML   1+INPUT_OPT_PARAM
#define INPUT_OPT_STD            1+INPUT_OPT_READ_GCC_XML
#define INPUT_OPT_USE_RAW        1+INPUT_OPT_STD
#define INPUT_OPT_WRITE_GCC_XML  1+INPUT_OPT_USE_RAW
#define LAST_GCC_OPT             INPUT_OPT_WRITE_GCC_XML

///define the GCC short option string
#define GCC_SHORT_OPTIONS_STRING "cf:I:D:O::l:L:W:E"

#if !RELEASE
#define GCC_LONG_OPTIONS_RAW_XML\
   { "use-raw", no_argument, nullptr, INPUT_OPT_USE_RAW},\
   { "read-GCC-XML", required_argument, nullptr, INPUT_OPT_READ_GCC_XML},\
   { "write-GCC-XML", required_argument, nullptr, INPUT_OPT_WRITE_GCC_XML},
#else
#define GCC_LONG_OPTIONS_RAW_XML
#endif
#define GCC_LONG_OPTIONS_COMPILER \
   { "compiler", required_argument, nullptr, INPUT_OPT_COMPILER},


#define GCC_LONG_OPTIONS \
   GCC_LONG_OPTIONS_COMPILER \
   { "std", required_argument, nullptr, INPUT_OPT_STD},\
   GCC_LONG_OPTIONS_RAW_XML \
   { "param", required_argument, nullptr, INPUT_OPT_PARAM},\
   { "Include-sysdir", no_argument, nullptr, INPUT_OPT_INCLUDE_SYSDIR},\
   { "gcc-config", no_argument, nullptr, INPUT_OPT_GCC_CONFIG},\
   { "compute-sizeof", no_argument, nullptr, INPUT_OPT_COMPUTE_SIZEOF},\
   { "extra-gcc-options", required_argument, nullptr, INPUT_OPT_CUSTOM_OPTIONS}

class Parameter
{
   protected:
      /// Map between the name of the option and the related string-form value
      OptionMap Options;

      /// Map between an enum option and the related string-form value
      std::map<enum enum_option, std::string> enum_options;

      ///Name of the enum options
      std::map<enum enum_option, std::string> option_name;

      /// Classes to be debugged
      std::unordered_set<std::string> debug_classes;

      ///debug level
      int debug_level;

      /**
       * Loads an XML configuration file (recursive method)
       * @param node is the starting node for the analysis
       */
      void load_xml_configuration_file_rec(const xml_element* node);

      /**
       * Manage default options (common to all tools)
       * @param next_option is the index of the option to be analyzed
       * @param optarg_param is the optional argument of the option
       * @param exit_success is where the exit value is stored
       * @return true if the option has been recognized
       */
      bool ManageDefaultOptions(int next_option, char * optarg_param, bool &exit_success);

#if HAVE_FROM_C_BUILT
      /**
       * Manage Gcc options(common to zebu and bambu)
       * @param next_option is the index of the option to be analyzed
       * @param optarg_param is the optional argument of the option
       * @return true if the option has been recognized
       */
      bool ManageGccOptions(int next_option, char * optarg_param);
#endif

      /**
       * Print the usage of the general common options
       * @param os is the stream where to print
       */
      void PrintGeneralOptionsUsage(std::ostream & os) const;

      /**
       * Print the usage of the output common options
       * @param os is the stream
       */
      void PrintOutputOptionsUsage(std::ostream & os) const;

#if HAVE_FROM_C_BUILT
      /**
       * Print the gcc options usage
       * @param os is the stream where to print
       */
      void PrintGccOptionsUsage(std::ostream & os) const;
#endif

      /**
       * Sets the default values with respect to the tool
       */
      virtual void SetDefaults() = 0;

      /**
       * Sets the default values common to all tools
       */
      void SetCommonDefaults();

      /**
       * Print the name of the program to be included in the header
       * @param os is the stream on which the program name has to be printed
       */
      virtual void PrintProgramName(std::ostream & os) const = 0;

      /**
       * Print the help
       * @param os is the stream on which the help has to be printed
       */
      virtual void PrintHelp(std::ostream & os) const = 0;

   public:
      /**
       * Constructor
       * @param program_name is the name of the executable
       * @param debug_level is the debug level
       */
      Parameter(const std::string program_name, int debug_level = 0);

      /**
       * Copy Constructor
       * @param other is copy element
       */
      Parameter(const Parameter & other);

      /**
       * Destructor
       */
      virtual ~Parameter();

      /**
       * Loads an XML configuration file
       * @param filename is the configuration file name to be loaded
       */
      void load_xml_configuration_file(const std::string& filename);

      /**
       * Write an XML configuration file with the parameters actually stored
       * @param filename is the configuration file name where parameters have to be written
       */
      void write_xml_configuration_file(const std::string& filename);

      /**
       * Execute parameter parsing. It has to be specialized
       * @param argc is the number of arguments
       * @param argv is the array of arguments passed to program.
       */
      virtual int exec(int argc, char *argv[]) = 0;

      /**
       * Checks the compatibility among the different parameters
       * and determines the implications
       */
      virtual void CheckParameters() = 0;

      /**
       * Friend definition of the << operator.
       */
      friend std::ostream& operator<<(std::ostream& os, const Parameter& s)
      {
         s.print(os);
         return os;
      }

      /**
       * Returns the value of an option
       * @param name is the name of the option
       * @param variable is the variable where the value of the option will be saved
       */
      template<typename G>
      void getOption(const std::string& name, G& variable) const
      {
        THROW_ASSERT(Options.find(name) != Options.end(), "Option \"" + name + "\" not stored");
        variable = boost::lexical_cast<G>(Options.find(name)->second);
      }

      /**
       * Returns the value of an option
       * @param name is the name of the option
       * @param variable is the variable where the value of the option will be saved
       */
      template<typename G>
      void getOption(const char* name, G& variable) const
      {
         getOption(std::string(name), variable);
      }

      /**
       * Returns the value of an option
       * @param name is the name of the option
       * @param return the value of the option
       */
      template<typename G>
      G getOption(const std::string& name) const
      {
         THROW_ASSERT(Options.find(name) != Options.end(), "Option \"" + name + "\" not stored");
         return boost::lexical_cast<G>(Options.find(name)->second);
      }

      /**
       * Returns the value of an option
       * @param name is the name of the option
       * @param return the value of the option
       */
      template<typename G>
      G getOption(const char* name) const
      {
         return getOption<G>(std::string(name));
      }

      /**
       * Returns the value of an option
       * @param name is the name of the option
       * @param return the value of the option
      */
      template<typename G>
      G getOption(const enum enum_option name) const
      {
         THROW_ASSERT(enum_options.find(name) != enum_options.end(), "Option \"" + (option_name.find(name))->second + "\" not stored");
         return boost::lexical_cast<G>(enum_options.find(name)->second);
      }

      /**
       * Sets the value of an option
       * @param name is the name of the option
       * @param value is the value of the option to be saved
       */
      template<typename G>
      void setOption(const std::string name, const G value)
      {
         Options[name] = boost::lexical_cast<std::string>(value);
      }

      /**
       * Sets the value of an option
       * @param name is the name of the option
       * @param value is the value of the option to be saved
       */
      template<typename G>
      void setOption(const char* name, const G value)
      {
         Options[std::string(name)] = boost::lexical_cast<std::string>(value);
      }

      /**
       * Sets the value of an option
       * @param name is the name of the option
       * @param value is the value of the option to be saved
       */
      template<typename G>
      void setOption(const enum enum_option name, const G value)
      {
         enum_options[name] = boost::lexical_cast<std::string>(value);
      }

      /**
       * Tests if an option has been stored
       * @param name is the name of the option
       * @return true if the option is in the map, false otherwise
       */
      bool isOption(const std::string& name) const
      {
         return Options.find(name) != Options.end();
      }

      /**
       * Tests if an option has been stored
       * @param name is the name of the option
       * @return true if the option is in the map, false otherwise
       */
      bool isOption(const char* name) const
      {
         return isOption(std::string(name));
      }

      /**
       * Tests if an option has been stored
       * @param name is the name of the option
       * @return true if the option is in the map, false otherwise
       */
      bool isOption(const enum enum_option name) const
      {
         return enum_options.find(name) != enum_options.end();
      }

      /**
       * Remove an option
       * @param name is the name of the option
       * @return true if the option has been eliminated, false otherwise
       */
      bool removeOption(const enum enum_option name)
      {
         if (!isOption(name)) return false;
         enum_options.erase(name);
         return true;
      }

      /**
       * Remove an option
       * @param name is the name of the option
       * @return true if the option has been eliminated, false otherwise
       */
      bool removeOption(const char* name)
      {
         return removeOption(std::string(name));
      }

      /**
       * Remove an option
       * @param name is the name of the option
       * @return true if the option has been eliminated, false otherwise
       */
      bool removeOption(const std::string& name)
      {
         if (!isOption(name)) return false;
         Options.erase(name);
         return true;
      }

      /**
       * Return the debug level for a specific class
       * @param name is the name the class
       * @return the corresponding level
       */
      int get_class_debug_level(const std::string class_name, int debug_level = -1) const;

      void print(std::ostream& os) const;

      /**
       * Add a class to be debugged
       */
      void add_debug_class(const std::string & class_name);

      /**
       * Print the usage of this tool = PrintHeader() + PrintHelp()
       * @param os is the stream where the message has to be printed
       */
      void PrintUsage(std::ostream &os) const;

      /**
       * This function prints the version of the tool
       */
      std::string PrintVersion() const;

      /**
       * This function prints the header of the tool = PrintProgramName() + PrintVersion()
       */
      virtual void PrintFullHeader(std::ostream & os) const;

      /**
       * Print the bug report request
       * @param os is the stream where the message has to be printed
       */
      void PrintBugReport(std::ostream &os) const;

      /**
       * Return the file format given the file name or the extension
       * @param file is the file name or the extension
       * @param check_cml_root_node tells xml file has to be analyzed
       * @return the type of the file format
       */
      FileFormat GetFileFormat(const std::string & file, bool check_cml_root_node = false) const;
};

template<>
const std::unordered_set<std::string> Parameter::getOption(const enum enum_option name) const;

template<>
const std::list<std::string> Parameter::getOption(const enum enum_option name) const;

template<>
AnalysisLevel Parameter::getOption(const enum enum_option name) const;

template<>
ProfilingMethod Parameter::getOption(const enum enum_option name) const;

template<>
ProfilingArchitectureKind Parameter::getOption(const enum enum_option name) const;

template<>
FileFormat Parameter::getOption(const enum enum_option name) const;

template<>
CompilerTarget Parameter::getOption(const enum enum_option name) const;

template<>
std::unordered_set<PerformanceEstimationAlgorithm> Parameter::getOption(const enum enum_option name) const;

template<>
PerformanceEstimationAlgorithm Parameter::getOption(const enum enum_option name) const;

#if HAVE_DIOPSIS
template<>
DiopsisInstrumentation Parameter::getOption(const enum enum_option name) const;
#endif

#if HAVE_DESIGN_ANALYSIS_BUILT
template<>
DesignAnalysisStep Parameter::getOption(const enum enum_option name) const;
#endif

#if HAVE_FROM_C_BUILT
enum class GccWrapper_OptimizationSet;
template<>
GccWrapper_OptimizationSet Parameter::getOption(const enum enum_option name) const;
template<>
void Parameter::setOption(const enum enum_option name, const GccWrapper_OptimizationSet value);
#endif

#if HAVE_TO_C_BUILT
template<>
CThreadBackendType Parameter::getOption(const enum enum_option name) const;
#endif

typedef refcount<Parameter> ParameterRef;
typedef refcount<const Parameter> ParameterConstRef;

#endif
