/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file bambu.cpp
 * @brief High level Synthesis tool.
 *
 * Main file used to perform high-level synthesis starting from a C-based specification.
 * See \ref src_bambu for further informations
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_PACKAGE_VERSION.hpp"
#include "config_HAVE_PRAGMA_BUILT.hpp"
#include "config_INSIDE_GIT.hpp"
#include "config_NPROFILE.hpp"

#include <iostream>
#include <cstdlib>

/// ----------- Intermediate Representation ----------- ///
/// Intermediate Representation Parsing
#include "parse_tree.hpp"
/// Intermediate Representation Datastructure
#include "tree_manager.hpp"
/// Intermediate Representation Management
#include "call_graph.hpp"
#include "call_graph_manager.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"
#include "op_graph.hpp"
#include "tree_helper.hpp"
#include "tree_node.hpp"
#include "hls_manager.hpp"
#include "hls_target.hpp"

/// ----------- C backend ----------- ///
#include "c_backend_step_factory.hpp"

/// ----------- Frontend Flow Library ----------- ///
#include "design_flow_manager.hpp"
#include "frontend_flow_step.hpp"
#include "frontend_flow_step_factory.hpp"

/// ----------- Resource Library ----------- ///
/// Resource Library Parsing
#include "parse_technology.hpp"
/// Resource Library Datastructure
#include "technology_manager.hpp"
#include "technology_builtin.hpp"

/// ----------- Pragma stuff ----------- ///
#if HAVE_PRAGMA_BUILT
#include "pragma_manager.hpp"
#include "actor_graph_flow_step_factory.hpp"
#endif

/// ----------- High-Level Synthesis flow ----------- ///
#include "hls_synthesis_flow.hpp"
#include "controller_creator.hpp"

/// ----------- General PandA includes ----------- ///
#include "dbgPrintHelper.hpp"
#include "utility.hpp"
#include "exceptions.hpp"
#include "cpu_time.hpp"

/// ----------- Parameters Management ----------- ///
#include "BambuParameter.hpp"
#include "constant_strings.hpp"

///GCC wrapper
#include "gcc_wrapper.hpp"

#include "global_variables.hpp"

void createCallGraph(const ParameterRef parameters, const application_managerRef AppM)
{
#ifndef NDEBUG
   int debug_level = parameters->getOption<int>(OPT_debug_level);
#endif
   unsigned int output_level = parameters->getOption<unsigned int>(OPT_output_level);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERBOSE, debug_level, " ==== Starting Initialization of Behavioral Specification ====");
   std::unordered_set<FrontendFlowStepType> transformations;
   transformations.insert(SSA_DATA_FLOW_ANALYSIS);
   transformations.insert(CONTROL_DEPENDENCE_COMPUTATION);
   transformations.insert(ADD_OP_FLOW_EDGES);
   transformations.insert(CHECK_SYSTEM_TYPE);
   transformations.insert(IR_LOWERING);

   transformations.insert(BLOCK_FIX);
   transformations.insert(SWITCH_FIX);
#if HAVE_EXPERIMENTAL
   if (parameters->getOption<bool>(OPT_speculative))
      transformations.insert(SPECULATION_EDGES_COMPUTATION);
   if (parameters->getOption<int>(OPT_controller_architecture) == controller_creator::PARALLEL_CONTROLLER)
   {
      // Silvia: this is the transformation to disable for testing
      transformations.insert(VIRTUAL_PHI_NODES_SPLIT);
      if(parameters->isOption(OPT_chaining) and parameters->getOption<bool>(OPT_chaining))
         transformations.insert(PARALLEL_REGIONS_GRAPH_COMPUTATION);
      transformations.insert(EXTENDED_PDG_COMPUTATION);
      if(parameters->isOption("pdg-reduction") and parameters->getOption<bool>("pdg-reduction"))
         transformations.insert(REDUCED_PDG_COMPUTATION);
      transformations.insert(SIMPLE_CODE_MOTION);
      transformations.insert(SHORT_CIRCUIT_TAF);
      transformations.insert(PHI_OPT);
   }
   else
   {
      //transformations.insert(ARRAY_REF_FIX);
      transformations.insert(REMOVE_CLOBBER_GA);
      transformations.insert(MULTI_WAY_IF);
      transformations.insert(SIMPLE_CODE_MOTION);
      transformations.insert(SHORT_CIRCUIT_TAF);
      transformations.insert(PHI_OPT);
   }
#else
   transformations.insert(REMOVE_CLOBBER_GA);
   transformations.insert(MULTI_WAY_IF);
   transformations.insert(SIMPLE_CODE_MOTION);
   transformations.insert(SHORT_CIRCUIT_TAF);
   transformations.insert(PHI_OPT);
#endif
   transformations.insert(DETERMINE_MEMORY_ACCESSES);
   transformations.insert(NI_SSA_LIVENESS);
   if(parameters->isOption(OPT_soft_float) && parameters->getOption<bool>(OPT_soft_float))
      transformations.insert(SOFT_FLOAT_CG_EXT);

#if HAVE_PRAGMA_BUILT
   if (parameters->getOption<bool>(OPT_parse_pragma))
   {
      transformations.insert(ORDER_COMPUTATION);
   }
   if(parameters->isOption(OPT_parse_pragma) && parameters->getOption<bool>(OPT_parse_pragma))
   {
      transformations.insert(PRAGMA_SUBSTITUTION);
      transformations.insert(PRAGMA_ANALYSIS);
   }
#endif
   const DesignFlowManagerRef design_flow_manager(new DesignFlowManager(parameters));
   const DesignFlowStepFactoryConstRef design_flow_step_factory(new FrontendFlowStepFactory(AppM, design_flow_manager, parameters));
   design_flow_manager->RegisterFactory(design_flow_step_factory);
   const DesignFlowStepSet design_flow_steps = GetPointer<const FrontendFlowStepFactory>(design_flow_step_factory)->GenerateFrontendSteps(transformations);
   design_flow_manager->AddSteps(design_flow_steps);
   design_flow_manager->Exec();

   /// check if the root function is a empty function: compiler optimizations may kill everything
   const unsigned int function_id = AppM->CGetCallGraphManager()->GetRootFunction();
   if(!function_id)
      THROW_ERROR("BadParameters: specified file(s) does not include function <" + parameters->getOption<std::string>(OPT_top_function_name) + ">");
   THROW_ASSERT(AppM->get_tree_manager()->get_tree_node_const(function_id) && GetPointer<function_decl>(AppM->get_tree_manager()->get_tree_node_const(function_id)), "expected a function decl");
   if(tree_helper::is_a_nop_function_decl(GetPointer<function_decl>(AppM->get_tree_manager()->get_tree_node_const(function_id))))
   {
      THROW_ERROR("the top function is empty or the compiler killed all the statements");
   }

   ///pretty printing
   if (parameters->isOption(OPT_pretty_print))
   {
      long cpu_time;
      START_TIME(cpu_time);
      std::string outFileName = parameters->getOption<std::string>(OPT_pretty_print);
      const DesignFlowStepFactoryConstRef c_backend_step_factory(new CBackendStepFactory(design_flow_manager, AppM, parameters));
      const DesignFlowStepRef c_backend = GetPointer<const CBackendStepFactory>(c_backend_step_factory)->CreateCBackendStep(CBackend::CB_SEQUENTIAL, outFileName, CBackendInformationConstRef());
      c_backend->Exec();
      STOP_TIME(cpu_time);
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Pretty print completed in " + print_cpu_time(cpu_time) + " seconds\n");
   }
}

/**
 * Main file used to perform high-level synthesis starting from a C specification.
 * @anchor MainBambu
 * @param argc is the number of arguments
 * @param argv is the array of arguments passed to the program.
 */
int main(int argc, char *argv[])
{
   srand(static_cast<unsigned int>(time(nullptr)));

   // General options register
   ParameterRef parameters;

   try
   {
      // ---------- Initialization ------------ //

      // Synthesis cpu time
      long total_time;
      START_TIME(total_time);

      // ---------- Parameter parsing ------------ //
      long cpu_time;
      START_TIME(cpu_time);
      parameters = ParameterRef(new BambuParameter(argv[0]));

      switch(parameters->exec(argc, argv))
      {
         case PARAMETER_NOTPARSED:
         {
            exit_code = PARAMETER_NOTPARSED;
            THROW_ERROR("Bad Parameters format");
         }
         case EXIT_SUCCESS:
         {
            if(not (parameters->getOption<bool>(OPT_no_clean)))
            {
               boost::filesystem::remove_all(parameters->getOption<std::string>(OPT_output_temporary_directory));
            }
            return EXIT_SUCCESS;
         }
         case PARAMETER_PARSED:
         {
            exit_code = EXIT_FAILURE;
            break;
         }
         default:
         {
            THROW_ERROR("Bad Parameters parsing");
         }
      }

      int output_level = parameters->getOption<int>(OPT_output_level);
      if(output_level >= OUTPUT_LEVEL_MINIMUM)
         parameters->PrintFullHeader(std::cerr);

      //Include sysdir
      if(parameters->getOption<bool>(OPT_gcc_include_sysdir))
      {
         const GccWrapperRef gcc_wrapper(new GccWrapper(parameters, CT_NO_GCC, GccWrapper_OptimizationSet::O0));
         std::vector<std::string> system_includes;
         gcc_wrapper->GetSystemIncludes(system_includes);
         std::vector<std::string>::const_iterator system_include, system_include_end = system_includes.end();
         for(system_include = system_includes.begin(); system_include != system_include_end; system_include++)
         {
            INDENT_OUT_MEX(0, 0, *system_include);
         }
         if(not (parameters->getOption<bool>(OPT_no_clean)))
         {
            boost::filesystem::remove_all(parameters->getOption<std::string>(OPT_output_temporary_directory));
         }
         return EXIT_SUCCESS;
      }

      if(parameters->getOption<bool>(OPT_gcc_config))
      {
         const GccWrapperRef gcc_wrapper(new GccWrapper(parameters, CT_NO_GCC, GccWrapper_OptimizationSet::O0));
         gcc_wrapper->GetGccConfig();
         if(not (parameters->getOption<bool>(OPT_no_clean)))
         {
            boost::filesystem::remove_all(parameters->getOption<std::string>(OPT_output_temporary_directory));
         }
         return EXIT_SUCCESS;
      }
      if(!parameters->isOption(OPT_input_file))
      {
         PRINT_OUT_MEX(OUTPUT_LEVEL_NONE, output_level, "no input files\n");
         if(not (parameters->getOption<bool>(OPT_no_clean)))
         {
            boost::filesystem::remove_all(parameters->getOption<std::string>(OPT_output_temporary_directory));
         }
         return EXIT_SUCCESS;
      }
      STOP_TIME(cpu_time);
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Parameters parsed in " + print_cpu_time(cpu_time) + " seconds\n");

      // up to now all parameters have been parsed and datastructures created, so synthesis can start

      /// ==== Loading resource library ==== ///
      START_TIME(cpu_time);
      /// ==== Creating target for the synthesis ==== ///
      HLS_targetRef HLS_T = HLS_target::create_target(parameters);
      STOP_TIME(cpu_time);
      PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Resource library loaded in " + print_cpu_time(cpu_time) + " seconds\n");

      /// hack for Virtex-5 or Virtex-4 memory models
      if(HLS_T->get_target_device()->has_parameter("is_single_write_memory") && HLS_T->get_target_device()->get_parameter<std::string>("is_single_write_memory") == "1")
         parameters->setOption(OPT_gcc_single_write, true);

      /// ==== Creating intermediate representation ==== ///
      START_TIME(cpu_time);
      /// ==== Creating behavioral specification ==== ///
      HLS_managerRef AppM = HLS_managerRef(new HLS_manager(parameters, HLS_T));
      // create the datastructures (inside application_manager) where the problem specification is contained
      createCallGraph(parameters, AppM);

#if HAVE_PRAGMA_BUILT
      const pragma_managerRef PM = AppM->get_pragma_manager();
      if(parameters->isOption(OPT_parse_pragma) && parameters->getOption<bool>(OPT_parse_pragma))
      {
         long int task_graph_creation_time = 0;
         START_TIME(task_graph_creation_time);
         if(output_level >= OUTPUT_LEVEL_MINIMUM)
         {
            if(output_level >= OUTPUT_LEVEL_VERBOSE)
            {
               INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
               INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
               INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
               INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
               INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*                        Creating Initial Actor Graph                         *");
               INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
            }
            else
            {
               INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "");
               INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, " ======================= Creating Initial Actor Graph ========================");
            }
         }
         PM->initialize();

         const DesignFlowManagerRef design_flow_manager(new DesignFlowManager(parameters));
         const DesignFlowStepFactoryConstRef ag_design_flow_step_factory(new ActorGraphFlowStepFactory(AppM, design_flow_manager, parameters));
         design_flow_manager->RegisterFactory(ag_design_flow_step_factory);
         DesignFlowStepSet design_flow_steps;
         std::list<unsigned int> input_functions;
         AppM->get_functions_with_body(input_functions);
         std::list<unsigned int>::const_iterator input_function, input_function_end = input_functions.end();
         for(input_function = input_functions.begin(); input_function != input_function_end; input_function++)
         {
            const DesignFlowStepRef design_flow_step = GetPointer<const ActorGraphFlowStepFactory>(ag_design_flow_step_factory)->CreateActorGraphStep(ACTOR_GRAPHS_CREATOR, *input_function);
            design_flow_steps.insert(design_flow_step);
         }
         design_flow_manager->AddSteps(design_flow_steps);
         design_flow_manager->Exec();

#if !NPROFILE
         /// ---------- printing information ------------ ///
         const tree_managerRef TM = AppM->get_tree_manager();
         INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "Number of potential parallel loops: " + boost::lexical_cast<std::string>(TM->get_n_pl()));
#endif
         STOP_TIME(task_graph_creation_time);
#ifndef NDEBUG
         if(output_level >= OUTPUT_LEVEL_MINIMUM)
         {
            dump_exec_time("Task Graph Creation time", task_graph_creation_time);
         }
#endif
      }

#endif

      STOP_TIME(cpu_time);
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Intermediate representation created in " + print_cpu_time(cpu_time) + " seconds\n");

      /// ==== High-Level Synthesis ==== ///
      START_TIME(cpu_time);
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, " ==== Starting High-Level Synthesis ====\n");
      HLS_synthesis_flow::synthesis_t hls_type = static_cast<HLS_synthesis_flow::synthesis_t>(parameters->getOption<unsigned int>(OPT_synthesis_flow));
      HLS_synthesis_flowRef HLSflow = HLS_synthesis_flow::createSynthesisFlow(hls_type, parameters, AppM);
      HLSflow->performSynthesis();
      STOP_TIME(cpu_time);
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, " ==== High-Level Synthesis completed in " + print_cpu_time(cpu_time) + " seconds ==== \n");

      STOP_TIME(total_time);
      PRINT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "Total Execution Time: " + print_cpu_time(total_time) + " seconds\n");

      if(not (parameters->getOption<bool>(OPT_no_clean)))
      {
         boost::filesystem::remove_all(parameters->getOption<std::string>(OPT_output_temporary_directory));
      }
      return EXIT_SUCCESS; // Bambu tool has completed execution without errors
   }

   // exception catching
   catch (const char * str)
   {
      if(EXIT_SUCCESS == exit_code)
         exit_code = EXIT_FAILURE;
      std::cerr << str << std::endl;
   }
   catch (const std::string& str)
   {
      if(EXIT_SUCCESS == exit_code)
         exit_code = EXIT_FAILURE;
      std::cerr << str << std::endl;
   }
   catch (std::exception &e)
   {
      std::cerr << e.what() << std::endl;
   }
   catch (...)
   {
      if(EXIT_SUCCESS == exit_code)
         exit_code = EXIT_FAILURE;
      std::cerr << "Unknown error type" << std::endl;
   }

   switch(exit_code)
   {
      case PARAMETER_NOTPARSED:
      {
         parameters->PrintUsage(std::cout);
         break;
      }
      case EXIT_FAILURE:
      {
         if(parameters)
            parameters->PrintBugReport(std::cout);
         break;
      }
      default:
      {

      }
   }
   if(parameters && not (parameters->getOption<bool>(OPT_no_clean)))
   {
      boost::filesystem::remove_all(parameters->getOption<std::string>(OPT_output_temporary_directory));
   }
   return exit_code;

}

