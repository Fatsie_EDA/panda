/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

/**
 * @file spider.cpp
 * @brief Parser for deep profiling information
 *
 * @author Daniele Loiacono <loiacono@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */

///Autoheader include
#include "config_HAVE_CMOS_BUILT.hpp"
#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_FROM_CSV_BUILT.hpp"
#include "config_HAVE_FROM_LIBERTY.hpp"
#include "config_HAVE_FROM_PROFILING_ANALYIS_BUILT.hpp"
#include "config_HAVE_R.hpp"

///Backend include
#if HAVE_EXPERIMENTAL
#include "xml_generator.hpp"
#endif
#include "translator.hpp"

///Constants include
#if HAVE_FROM_LIBERTY
#include "physical_library_models_constants.hpp"
#endif

///Global variables include
#include "global_variables.hpp"

///STD include
#include <fstream>
#include <string>

///Paramter include
#include "SpiderParameter.hpp"

///Parser include
#include "data_xml_parser.hpp"
#if HAVE_FROM_CSV_BUILT
#include "parse_csv.hpp"
#endif
#if HAVE_FROM_PROFILING_ANALYIS_BUILT
#include "parse_rapid_miner.hpp"
#endif

///Regressor include
#if HAVE_REGRESSORS_BUILT
#include "cell_selection.hpp"
#if HAVE_FROM_LIBERTY
#include "cell_area_preprocessing.hpp"
#endif
#include "cross_validation.hpp"
#if HAVE_R
#include "linear_regression.hpp"
#endif
#include "performance_estimation_preprocessing.hpp"
#include "regressor.hpp"
#if HAVE_R
#include "significance_preprocessing.hpp"
#endif
#endif

#if HAVE_RTL_BUILT
///RTL include
#include "rtl_node.hpp"
#endif

///STL Map
#include <map>
#include <set>
#include <unordered_map>

#if HAVE_FROM_LIBERTY
///Technology include
#include "features_extractor.hpp"
#include "parse_technology.hpp"
#include "target_technology.hpp"
#include "technology_manager.hpp"
#endif

///Utility include
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/split.hpp>
#include "exceptions.hpp"
#include <iostream>

#define MAX_LENGTH 10000

#define INIT(x,y) x.push_back(std::string(y))

int main(int argc, char *argv[])
{
   ParameterRef Param;
   try
   {
      // ---------- Parameter parsing ------------ //
      Param = ParameterRef(new SpiderParameter(argv[0]));
      switch (Param->exec(argc, argv))
      {
         case PARAMETER_NOTPARSED:
         {
            exit_code = PARAMETER_NOTPARSED;
            throw "Bad Parameters format";
         }
         case EXIT_SUCCESS:
         {
            if(not (Param->getOption<bool>(OPT_no_clean)))
            {
               boost::filesystem::remove_all(Param->getOption<std::string>(OPT_output_temporary_directory));
            }
            return EXIT_SUCCESS;
         }
         case PARAMETER_PARSED:
         {
            exit_code = EXIT_FAILURE;
            break;
         }
         default:
         {
            if(not (Param->getOption<bool>(OPT_no_clean)))
            {
               boost::filesystem::remove_all(Param->getOption<std::string>(OPT_output_temporary_directory));
            }
            THROW_ERROR("Bad Parameters parsing");
         }
      }
#ifndef NDEBUG
      int debug_level = Param->getOption<int>(OPT_debug_level);
#endif
      FileFormat input_format = Param->getOption<FileFormat>(OPT_input_format);
      FileFormat output_format = Param->getOption<FileFormat>(OPT_output_format);
      switch(input_format)
      {
#if HAVE_FROM_CSV_BUILT
         case (FF_CSV) :
         {
            TranslatorConstRef tr(new Translator(Param));
            std::string csv_file;
            const std::unordered_set<std::string> input_files = Param->getOption<const std::unordered_set<std::string> >(OPT_input_file);
            std::unordered_set<std::string>::const_iterator input_file, input_file_end = input_files.end();
            for(input_file = input_files.begin(); input_file != input_file_end; input_file++)
            {
               if(Param->GetFileFormat(*input_file, false) == FF_CSV)
               {
                  csv_file = *input_file;
                  break;
               }
            }
            std::map<std::string, std::unordered_map<std::string, std::string> > results;
            ParseCsvFile(results, csv_file, Param);
            switch(output_format)
            {
               case (FF_TEX) :
               {
                  ///Read data
                  tr->write_to_latex(results, FF_CSV, Param->getOption<std::string>(OPT_output_file));
                  break;
               }
               case(FF_CSV):
               case(FF_CSV_RTL):
               case(FF_CSV_TRE):
#if HAVE_FROM_LIBERTY
               case(FF_LIB):
#endif
               case(FF_LOG):
               case(FF_PA):
               case(FF_TGFF):
               case(FF_VERILOG):
               case(FF_VHDL):
               case(FF_XML):
               case(FF_XML_AGG):
               case(FF_XML_BAMBU_RESULTS):
#if HAVE_FROM_LIBERTY
               case(FF_XML_CELLS):
#endif
               case(FF_XML_IP_XACT_COMPONENT):
               case(FF_XML_IP_XACT_CONFIG):
               case(FF_XML_IP_XACT_DESIGN):
               case(FF_XML_IP_XACT_GENERATOR):
               case(FF_XML_SKIP_ROW):
               case(FF_XML_STAT):
               case(FF_XML_SYM_SIM):
               case(FF_XML_TEX_TABLE):
               case(FF_XML_WGT_GM):
               case(FF_XML_WGT_SYM):
               case(FF_UNKNOWN):
               default:
                  THROW_ERROR("Not support combination input file - output file types");
            }
            break;
         }
#endif
         case (FF_XML) :
         {
            switch(output_format)
            {
               case(FF_TEX):
               {
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Input: XML - Output: TEX");
                  const std::unordered_set<std::string> input_files = Param->getOption<const std::unordered_set<std::string> >(OPT_input_file);
                  std::map<std::string, std::unordered_map<std::string, std::string> > results;
                  const DataXmlParserConstRef data_xml_parser(new DataXmlParser(Param));
                  data_xml_parser->Parse(input_files, results);
                  TranslatorConstRef tr(new Translator(Param));
                  tr->write_to_latex(results, FF_XML, Param->getOption<std::string>(OPT_output_file));
                  break;
               }
#if HAVE_EXPERIMENTAL
               case(FF_XML):
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Input: XML - Output: XML");
                  std::unordered_map<std::string, long double> results;
                  std::map<enum rtl_kind, std::map<enum mode_kind, long double> > output;
                  const std::unordered_set<std::string> input_files = Param->getOption<const std::unordered_set<std::string> >(OPT_input_file);
                  if(input_files.size() != 1)
                     THROW_ERROR("Only one rapid miner output can be analyzed at a time");
                  parse_rapid_miner(*(input_files.begin()), results, debug_level);
                  TranslatorConstRef tr(new Translator(Param));
                  tr->Translate(results, output);
                  tr->write_to_xml(output, Param->getOption<std::string>(OPT_output_file));
                  break;
               }
#else
               case(FF_XML):
#endif
#if HAVE_FROM_CSV_BUILT
               case(FF_CSV):
               case(FF_CSV_RTL):
               case(FF_CSV_TRE):
#endif
#if HAVE_FROM_LIBERTY
               case(FF_LIB):
#endif
#if HAVE_EXPERIMENTAL
               case(FF_LOG):
               case(FF_PA):
#endif
               case(FF_TGFF):
               case(FF_VERILOG):
               case(FF_VHDL):
#if HAVE_REGRESSORS_BUILT
               case(FF_XML_AGG):
#endif
               case(FF_XML_BAMBU_RESULTS):
#if HAVE_FROM_LIBERTY
               case(FF_XML_CELLS):
#endif
               case(FF_XML_IP_XACT_COMPONENT):
               case(FF_XML_IP_XACT_CONFIG):
               case(FF_XML_IP_XACT_DESIGN):
               case(FF_XML_IP_XACT_GENERATOR):
               case(FF_XML_SKIP_ROW):
#if HAVE_SOURCE_CODE_STATISTICS_XML
               case(FF_XML_STAT):
#endif
               case(FF_XML_SYM_SIM):
               case(FF_XML_TEX_TABLE):
               case(FF_XML_WGT_GM):
               case(FF_XML_WGT_SYM):
               case(FF_UNKNOWN):
               default:
                  THROW_ERROR("Not support combination input file - output file types");
            }
            break;
         }
#if HAVE_FROM_LIBERTY && HAVE_R && HAVE_CMOS_BUILT
         case (FF_LIB) :
         case (FF_XML_CELLS) :
         {
            if(output_format != FF_XML)
               THROW_ERROR("Not supported combination input file - output file types");
            int output_level = Param->getOption<int>(OPT_output_level);
            if(output_level >= OUTPUT_LEVEL_MINIMUM)
            {
               if(output_level >= OUTPUT_LEVEL_VERBOSE)
               {
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*                        Computing cell area and time models                  *");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
               }
               else
               {
                  INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, " ======================= Computing cell area and time models ================= ");
               }
            }
            const technology_managerRef TM = technology_managerRef(new technology_manager(Param));

            ///read the technology library
            read_technology_library(TM, Param, target_device::create_device(target_device::IC, Param, TM));

            ///Generate features
            std::map<std::string, std::map<std::string, long double> > data, preprocessed_data;
            FeaturesExtractorRef features_extractor(new FeaturesExtractor(Param));
            std::set<std::string> column_names;
            features_extractor->ExtractAreaFeatures(TM, data, column_names);
            /*
               const CellAreaPreprocessingConstRef preprocessing(new CellAreaPreprocessing(Param));
               preprocessing->Exec(data, preprocessed_data, column_names);
               data = preprocessed_data;*/
            const PreprocessingConstRef cell_selection(new CellSelection(Param));
            cell_selection->Exec(data, preprocessed_data, column_names, STR_CST_physical_library_models_area);
            const RegressorConstRef linear_regression = RegressorConstRef(new LinearRegression(Param));
            if(Param->getOption<int>(OPT_cross_validation) > 1)
            {
               const RegressorConstRef cross_validation_regression(new CrossValidation(linear_regression, Param));
               const RegressionResultsRef results = cross_validation_regression->Exec(column_names, STR_CST_physical_library_models_area, preprocessed_data);
               if(output_level >= OUTPUT_LEVEL_MINIMUM)
               {
                  if(output_level >= OUTPUT_LEVEL_VERBOSE)
                  {
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*                             Cross validation results                        *");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
                  }
                  else
                  {
                     INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, " ============================ Cross validation results ======================= ");
                  }
                  results->Print(std::cerr);
               }
            }
            else
            {
               const RegressionResultsRef results = linear_regression->Exec(column_names, STR_CST_physical_library_models_area, preprocessed_data);
               if(output_level >= OUTPUT_LEVEL_MINIMUM)
               {
                  if(output_level >= OUTPUT_LEVEL_VERBOSE)
                  {
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*                                Regression results                           *");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
                  }
                  else
                  {
                     INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "");
                     INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, " =============================== Regression results ========================== ");
                  }
                  results->Print(std::cerr);
               }
            }
            break;
         }
#endif
         case(FF_XML_BAMBU_RESULTS) :
#if HAVE_SOURCE_CODE_STATISTICS_XML
         case(FF_XML_STAT) :
#endif
         {
            const std::unordered_set<std::string> input_files = Param->getOption<const std::unordered_set<std::string> >(OPT_input_file);
            std::map<std::string, std::unordered_map<std::string, std::string> > results;
            const DataXmlParserConstRef data_xml_parser(new DataXmlParser(Param));
            data_xml_parser->Parse(input_files, results);
            TranslatorConstRef tr(new Translator(Param));
            switch(output_format)
            {
               case(FF_TEX) :
               {
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Input: xml - Output: tex");
                  tr->write_to_latex(results, input_format, Param->getOption<std::string>(OPT_output_file));
                  break;

               }
#if HAVE_FROM_CSV_BUILT
               case(FF_CSV):
               case(FF_CSV_RTL):
               case(FF_CSV_TRE):
#endif
#if HAVE_FROM_LIBERTY
               case(FF_LIB):
#endif
#if HAVE_EXPERIMENTAL
               case(FF_LOG):
               case(FF_PA):
#endif
               case(FF_TGFF):
               case(FF_VERILOG):
               case(FF_VHDL):
               case(FF_XML):
#if HAVE_REGRESSORS_BUILT
               case(FF_XML_AGG):
#endif
               case(FF_XML_BAMBU_RESULTS):
#if HAVE_FROM_LIBERTY
               case(FF_XML_CELLS):
#endif
               case(FF_XML_IP_XACT_COMPONENT):
               case(FF_XML_IP_XACT_CONFIG):
               case(FF_XML_IP_XACT_DESIGN):
               case(FF_XML_IP_XACT_GENERATOR):
               case(FF_XML_SKIP_ROW):
#if HAVE_SOURCE_CODE_STATISTICS_XML
               case(FF_XML_STAT):
#endif
               case(FF_XML_SYM_SIM):
               case(FF_XML_TEX_TABLE):
               case(FF_XML_WGT_GM):
               case(FF_XML_WGT_SYM):
               case(FF_UNKNOWN):
               default:
               {
                  THROW_ERROR("Not supported combination input file - output file types");
               }
            }
            break;
         }
#if HAVE_R
         case(FF_XML_SYM_SIM) :
         {
            int output_level = Param->getOption<int>(OPT_output_level);
            if(output_level >= OUTPUT_LEVEL_MINIMUM)
            {
               if(output_level >= OUTPUT_LEVEL_VERBOSE)
               {
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*       Computing probability distribution of operation execution time        *");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
               }
               else
               {
                  INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "");
                  INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, " ====== Computing probability distribution of operation execution time =======");
               }
            }
            INDENT_DBG_MEX(DEBUG_LEVEL_MINIMUM, debug_level, "Input xml is Symbolic Distribution: going to estimate assembly variances");
            if(output_format != FF_XML)
               THROW_ERROR("Not supported combination input file - output file types");
            const std::string sequences = Param->getOption<std::string>(OPT_normalization_sequences);
            std::vector<std::string> sequences_splitted;
            boost::split(sequences_splitted, sequences, boost::is_any_of("-"));
            for(size_t sequence_number = 0; sequence_number < sequences_splitted.size(); sequence_number++)
            {
               const std::unordered_set<std::string> input_files = Param->getOption<const std::unordered_set<std::string> >(OPT_input_file);
               const PreprocessingConstRef performance_estimation_preprocessing(new PerformanceEstimationPreprocessing(sequences_splitted[sequence_number], Param));
               std::map<std::string, std::map<std::string, long double> > data, filtered_data, preprocessed_data;
               std::set<std::string> column_names;
               performance_estimation_preprocessing->ReadData(input_files, data, column_names);
               const PreprocessingConstRef cell_selection(new CellSelection(Param));
               cell_selection->Exec(data, filtered_data, column_names, STR_CST_cycles);
               performance_estimation_preprocessing->Exec(filtered_data, preprocessed_data, column_names, STR_CST_cycles);

               long double error = INFINITE_LONG_DOUBLE;
               while(true)
               {
                  RegressorConstRef regression = RegressorConstRef(new LinearRegression(Param));
                  if(Param->getOption<int>(OPT_cross_validation) > 1)
                  {
                     regression = RegressorConstRef(new CrossValidation(regression, Param));
                  }
                  const RegressionResultsRef results = regression->Exec(column_names, STR_CST_cycles, preprocessed_data);
                  error = results->average_training_error;
                  if(error < Param->getOption<long double>(OPT_maximum_error))
                  {
                     break;
                  }
                  long double max_error = 0.0;
                  std::string benchmark_to_be_removed;
                  const std::unordered_map<std::string, long double> & training_errors = results->training_errors;
                  std::unordered_map<std::string, long double>::const_iterator training_error, training_error_end = training_errors.end();
                  for(training_error = training_errors.begin(); training_error != training_error_end; training_error++)
                  {
                     long double current_training_error = training_error->second;
                     if(current_training_error < 0.0)
                     {
                        current_training_error = -current_training_error;
                     }
                     if(current_training_error > max_error)
                     {
                        benchmark_to_be_removed = training_error->first;
                        max_error = current_training_error;
                     }
                  }
                  THROW_ASSERT(preprocessed_data.find(benchmark_to_be_removed) != preprocessed_data.end(), benchmark_to_be_removed + " is not in the map");
                  preprocessed_data.erase(preprocessed_data.find(benchmark_to_be_removed));
                  INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, debug_level, "Removed " + benchmark_to_be_removed + " with error " + boost::lexical_cast<std::string>(max_error) + ": current average error is " + boost::lexical_cast<std::string>(error));
               }
               long double current_minimum_significance = 0.0;
               while(true)
               {
                  RegressorConstRef regression = RegressorConstRef(new LinearRegression(Param));
                  if(Param->getOption<int>(OPT_cross_validation) > 1)
                  {
                     regression = RegressorConstRef(new CrossValidation(regression, Param));
                  }
                  const RegressionResultsRef results = regression->Exec(column_names, STR_CST_cycles, preprocessed_data);
                  results->Print(std::cerr);
                  current_minimum_significance = GetPointer<LinearRegressionResults>(results)->regressor_minimum_significance;
                  if(current_minimum_significance >= Param->getOption<long double>(OPT_minimum_significance))
                  {
                     XMLGeneratorConstRef generator(new XMLGenerator(Param));
                     generator->GenerateRtlSequenceWeightModel(results->model, Param->getOption<std::string>(OPT_output_file));
                     break;
                  }
                  data = preprocessed_data;
                  const PreprocessingConstRef significance_preprocessing(new SignificancePreprocessing(GetPointer<LinearRegressionResults>(results)->regressor_significances, GetPointer<LinearRegressionResults>(results)->regressor_minimum_significance, Param));
                  significance_preprocessing->Exec(data, preprocessed_data, column_names, STR_CST_cycles);
               }
            }
            break;
         }
#endif
#if HAVE_EXPERIMENTAL
         case(FF_XML_WGT_SYM):
         {
            switch(output_format)
            {
               case(FF_XML) :
               {
                  int output_level = Param->getOption<int>(OPT_output_level);
                  if(output_level >= OUTPUT_LEVEL_MINIMUM)
                  {
                     if(output_level >= OUTPUT_LEVEL_VERBOSE)
                     {
                        INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                        INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                        INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
                        INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
                        INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*                   Genereting symbolic sequence weight model                 *");
                        INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*******************************************************************************");
                     }
                     else
                     {
                        INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "");
                        INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, " ================== Generating symbolic sequence weight models =============== ");
                     }
                  }
                  XMLGeneratorConstRef generator(new XMLGenerator(Param));
                  const std::unordered_set<std::string> input_files = Param->getOption<const std::unordered_set<std::string> >(OPT_input_file);
                  if(input_files.size() > 1)
                     THROW_ERROR("Symbolic sequence weight models can be built only starting from a single file");
                  const std::string output_file = Param->getOption<std::string>(OPT_output_file);
                  generator->GenerateSymbolicSequenceWeightModel(*(input_files.begin()), output_file);
                  break;
               }
               case(FF_CSV):
               case(FF_CSV_RTL):
               case(FF_CSV_TRE):
#if HAVE_FROM_LIBERTY
               case(FF_LIB):
#endif
               case(FF_LOG):
               case(FF_PA):
               case(FF_TEX):
               case(FF_VERILOG):
               case(FF_VHDL):
               case(FF_XML_AGG):
               case(FF_XML_BAMBU_RESULTS):
#if HAVE_FROM_LIBERTY
               case(FF_XML_CELLS):
#endif
               case(FF_XML_IP_XACT_COMPONENT):
               case(FF_XML_IP_XACT_CONFIG):
               case(FF_XML_IP_XACT_DESIGN):
               case(FF_XML_IP_XACT_GENERATOR):
               case(FF_XML_SKIP_ROW):
               case(FF_XML_STAT):
               case(FF_XML_SYM_SIM):
               case(FF_XML_TEX_TABLE):
               case(FF_TGFF):
               case(FF_XML_WGT_GM):
               case(FF_XML_WGT_SYM):
               case(FF_UNKNOWN):
               default:
               {
                  THROW_ERROR("Not supported combination input file - output file types");
               }
            }
            break;
         }
#else
         case(FF_XML_WGT_SYM):
#endif
#if HAVE_FROM_CSV_BUILT
         case(FF_CSV_RTL):
         case(FF_CSV_TRE):
#endif
#if HAVE_FROM_LIBERTY
         case(FF_LIB):
#endif
#if HAVE_EXPERIMENTAL
         case(FF_LOG):
         case(FF_PA):
#endif
         case(FF_TEX):
         case(FF_TGFF):
         case(FF_VERILOG):
         case(FF_VHDL):
#if HAVE_REGRESSORS_BUILT
         case(FF_XML_AGG):
#endif
#if HAVE_FROM_LIBERTY
         case(FF_XML_CELLS):
#endif
         case(FF_XML_IP_XACT_COMPONENT):
         case(FF_XML_IP_XACT_CONFIG):
         case(FF_XML_IP_XACT_DESIGN):
         case(FF_XML_IP_XACT_GENERATOR):
         case(FF_XML_SKIP_ROW):
#if ! HAVE_R
         case(FF_XML_SYM_SIM):
#endif
         case(FF_XML_TEX_TABLE):
         case(FF_XML_WGT_GM):
         case(FF_UNKNOWN):
         default:
            THROW_ERROR("Not supported input file type " + boost::lexical_cast<std::string>(input_format));
      }
   }
   catch (const char * str)
   {
      std::cerr << str << std::endl;
   }
   catch (const std::string& str)
   {
      std::cerr << str << std::endl;
   }
   catch (std::exception& inException)
   {
      PRINT_OUT_MEX(OUTPUT_LEVEL_NONE, 0, inException.what());
   }
   catch (...)
   {
      std::cerr << "Unknown error type" << std::endl;
   }
   if(not (Param->getOption<bool>(OPT_no_clean)))
   {
      boost::filesystem::remove_all(Param->getOption<std::string>(OPT_output_temporary_directory));
   }

   return exit_code;
}
