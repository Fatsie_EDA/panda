/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file Parameter.cpp
 * @brief
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_ARM_GCC_VERSION.hpp"
#include "config_HAVE_ARM_COMPILER.hpp"
#include "config_HAVE_FROM_ARCH_BUILT.hpp"
#include "config_HAVE_FROM_C_BUILT.hpp"
#include "config_HAVE_FROM_CSV_BUILT.hpp"
#include "config_HAVE_FROM_IPXACT_BUILT.hpp"
#include "config_HAVE_FROM_PSPLIB_BUILT.hpp"
#include "config_HAVE_FROM_SDF3_BUILT.hpp"
#include "config_HAVE_I386_GCC45_COMPILER.hpp"
#include "config_HAVE_I386_GCC46_COMPILER.hpp"
#include "config_HAVE_I386_GCC47_COMPILER.hpp"
#include "config_HAVE_I386_GCC48_COMPILER.hpp"
#include "config_HAVE_I386_GCC49_COMPILER.hpp"
#include "config_HAVE_IPXACT_BUILT.hpp"
#include "config_HAVE_PERFORMANCE_METRICS_XML.hpp"
#include "config_HAVE_REGRESSORS_BUILT.hpp"
#include "config_HAVE_SOURCE_CODE_STATISTICS_XML.hpp"
#include "config_HAVE_SPARC_COMPILER.hpp"
#include "config_HAVE_TO_DATAFILE_BUILT.hpp"
#include "config_HAVE_WEIGHT_MODELS_XML.hpp"
#include "config_I386_GCC45_VERSION.hpp"
#include "config_I386_GCC46_VERSION.hpp"
#include "config_I386_GCC47_VERSION.hpp"
#include "config_I386_GCC48_VERSION.hpp"
#include "config_I386_GCC49_VERSION.hpp"
#include "config_INSIDE_GIT.hpp"
#include "config_PACKAGE_BUGREPORT.hpp"
#include "config_PACKAGE_STRING.hpp"
#include "config_SPARC_GCC_VERSION.hpp"

///Header include
#include "Parameter.hpp"

#if HAVE_FROM_C_BUILT
///wrapper/treegcc
#include "gcc_wrapper.hpp"
#endif

///Boost include
#include <boost/lexical_cast.hpp>

///Constants include
#if HAVE_REGRESSORS_BUILT
#include "aggregated_features_xml.hpp"
#include "skip_rows_xml.hpp"
#endif
#if HAVE_FROM_ARCH_BUILT
#include "architecture_xml.hpp"
#endif
#include "constants.hpp"
#include "constant_strings.hpp"
#if HAVE_TO_DATAFILE_BUILT
#include "latex_table_xml.hpp"
#endif
#if HAVE_PERFORMANCE_METRICS_XML
#include "metrics_xml.hpp"
#endif
#if HAVE_SOURCE_CODE_STATISTICS_XML
#include "source_code_statistics_xml.hpp"
#endif
#if HAVE_WEIGHT_MODELS_XML && HAVE_EXPERIMENTAL
#include "probability_distribution_xml.hpp"
#include "weights_xml.hpp"
#endif
#if HAVE_FROM_SDF3_BUILT
#include "sdf3_xml.hpp"
#endif

///STD include
#include <iostream>
#include <stdlib.h>

///Utility include
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include "dbgPrintHelper.hpp"
#include "fileIO.hpp"
#include "refcount.hpp"
#include "xml_helper.hpp"

///XML include
#include "polixml.hpp"
#include "xml_dom_parser.hpp"
#if HAVE_FROM_IPXACT_BUILT
#include "ip_xact_xml.hpp"
///Constants include
#include "design_analysis_xml.hpp"
#endif

const std::string branch_name =
{
#include "branch_name.hpp"
};

const std::string revision_hash =
{
#include "revision_hash.hpp"
};


#define OPTION_NAME(r, data, elem) option_name[BOOST_PP_CAT(OPT_, elem)] = #elem;

Parameter::Parameter(const std::string program_name, int _debug_level) :
   debug_level(_debug_level)
{
   setOption(OPT_program_name, program_name);
   BOOST_PP_SEQ_FOR_EACH(OPTION_NAME, BOOST_PP_EMPTY, BAMBU_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTION_NAME, BOOST_PP_EMPTY, EUCALIPTUS_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTION_NAME, BOOST_PP_EMPTY, FRAMEWORK_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTION_NAME, BOOST_PP_EMPTY, GCC_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTION_NAME, BOOST_PP_EMPTY, GECCO_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTION_NAME, BOOST_PP_EMPTY, KOALA_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTION_NAME, BOOST_PP_EMPTY, SPIDER_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTION_NAME, BOOST_PP_EMPTY, SYNTHESIS_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTION_NAME, BOOST_PP_EMPTY, TREE_PANDA_GCC_OPTIONS)
   BOOST_PP_SEQ_FOR_EACH(OPTION_NAME, BOOST_PP_EMPTY, ZEBU_OPTIONS)
   //This part has been added since boost macro does not expand correctly
   std::map<enum enum_option, std::string>::iterator it, it_end = option_name.end();
   for(it = option_name.begin(); it != it_end; it++)
   {
      it->second = "OPT_" + it->second.substr(19);
      it->second = it->second.substr(0, it->second.find(')'));
   }
   SetCommonDefaults();
}

Parameter::Parameter(const Parameter & other) :
   Options(other.Options),
   enum_options(other.enum_options),
   option_name(other.option_name),
   debug_classes(other.debug_classes)
{
}

void Parameter::CheckParameters()
{
   const std::string temporary_directory = getOption<std::string>(OPT_output_temporary_directory);
   if(boost::filesystem::exists(temporary_directory))
   {
      boost::filesystem::remove_all(temporary_directory);
   }
   boost::filesystem::create_directory(temporary_directory);
   ///Output directory is not removed since it can be the current one
   const std::string output_directory = getOption<std::string>(OPT_output_directory);
   if(!boost::filesystem::exists(output_directory))
   {
      boost::filesystem::create_directory(output_directory);
   }
   if(getOption<bool>(OPT_print_dot))
   {
      const std::string dot_directory = getOption<std::string>(OPT_dot_directory);
      if(boost::filesystem::exists(dot_directory))
      {
         boost::filesystem::remove_all(dot_directory);
      }
      boost::filesystem::create_directory(dot_directory);
   }
}

Parameter::~Parameter()
{
}

void Parameter::load_xml_configuration_file_rec(const xml_element* node)
{
   //Recurse through child nodes:
   const xml_node::node_list list = node->get_children();
   for (xml_node::node_list::const_iterator iter = list.begin(); iter != list.end(); ++iter)
   {
      const xml_element* EnodeC = GetPointer<const xml_element>(*iter);
      if (!EnodeC) continue;
      /// general options
      if (CE_XVM(value, EnodeC))
         Options[GET_NODE_NAME(EnodeC)] = GET_STRING_VALUE(EnodeC);
      if (EnodeC->get_children().size())
         load_xml_configuration_file_rec(EnodeC);
   }
}

void Parameter::load_xml_configuration_file(const std::string& filename)
{
   try
   {
      fileIO_istreamRef sname = fileIO_istream_open(filename);
      xml_dom_parser parser;
      parser.parse_stream(*sname);
      if (parser)
      {
         //Walk the tree:
         const xml_element* node = parser.get_document()->get_root_node(); //deleted by DomParser.

         //Recurse through child nodes:
         const xml_node::node_list list = node->get_children();
         for (xml_node::node_list::const_iterator iter = list.begin(); iter != list.end(); ++iter)
         {
            const xml_element* EnodeC = GetPointer<const xml_element>(*iter);
            if (!EnodeC) continue;
            /// general options
            if (CE_XVM(value, EnodeC))
               Options[GET_NODE_NAME(EnodeC)] = GET_STRING_VALUE(EnodeC);
            if (EnodeC->get_children().size())
               load_xml_configuration_file_rec(EnodeC);
         }
      }
   }
   catch (const char * msg)
   {
      std::cerr << msg << std::endl;
   }
   catch (const std::string & msg)
   {
      std::cerr << msg << std::endl;
   }
   catch (const std::exception& ex)
   {
      std::cout << "Exception caught: " << ex.what() << std::endl;
   }
   catch ( ... )
   {
      std::cerr << "unknown exception" << std::endl;
   }

}

void Parameter::write_xml_configuration_file(const std::string& filename)
{
   xml_document document;

   xml_element* parameters = document.create_root_node("parameters");

   for (OptionMap::const_iterator Op = Options.begin(); Op != Options.end(); Op++)
   {
      xml_element* node = parameters->add_child_element(Op->first);
      WRITE_XNVM2("value", Op->second, node);
   }

   document.write_to_file_formatted(filename);
}

void Parameter::SetCommonDefaults()
{
   setOption(STR_OPT_benchmark_fake_parameters, "<none>");
   std::string current_dir = GetCurrentPath();
   std::string temporary_directory = current_dir + "/" +std::string(STR_CST_temporary_directory);

   setOption(OPT_dot_directory, current_dir + "/dot/");
   setOption(OPT_output_temporary_directory, temporary_directory + "/");
   setOption(OPT_print_dot, false);

   setOption(OPT_no_clean, false);
   if(revision_hash == "")
      setOption(OPT_revision, "unknown-trunk");
   else
      setOption(OPT_revision, revision_hash + "-" + branch_name);
   setOption(OPT_seed, 0);
}

void Parameter::print(std::ostream& os) const
{
   os << "List of parameters: " << std::endl;
   for(OptionMap::const_iterator i = Options.begin(); i != Options.end(); i++)
   {
      os << i->first << ": " << i->second << std::endl;
   }
   std::map<enum enum_option, std::string>::const_iterator option, option_end = enum_options.end();
   for(option = enum_options.begin(); option != option_end; option++)
   {
      os << option_name.find(option->first)->second << ": " << option->second << std::endl;
   }
   os << " === " << std::endl;
}

int Parameter::get_class_debug_level(const std::string class_name, int _debug_level) const
{
   if(debug_classes.find(class_name) != debug_classes.end() or debug_classes.find(STR_CST_debug_all) != debug_classes.end())
      return DEBUG_LEVEL_VERY_PEDANTIC;
   else if (_debug_level < 0)
      return getOption<int>(OPT_debug_level);
   else
      return _debug_level;
}

void Parameter::add_debug_class(const std::string & class_name)
{
   debug_classes.insert(class_name);
}

void Parameter::PrintFullHeader(std::ostream & os) const
{
   PrintProgramName(os);
   os << "                         Politecnico di Milano - DEIB" << std::endl;
   os << "                          System Architectures Group" << std::endl;
   os << "********************************************************************************" << std::endl;
   os << "                Copyright (c) 2004-2014 Politecnico di Milano" << std::endl;
   std::string version = PrintVersion();
   if(version.size() < 80)
      os << std::string(40 - (version.size()/2), ' ') << version << std::endl;
   else
      os << version << std::endl;
   os << std::endl;
}

std::string Parameter::PrintVersion() const
{
   return std::string("Version: ") + PACKAGE_STRING
#if INSIDE_GIT
   + " - Revision " + getOption<std::string>(OPT_revision)
#endif
   ;
}

void Parameter::PrintUsage(std::ostream & os) const
{
   PrintFullHeader(os);
   PrintHelp(os);
}

bool Parameter::ManageDefaultOptions(int next_option, char * optarg_param, bool &exit_success)
{
   exit_success= false;
   switch (next_option)
   {
      case INPUT_OPT_NO_CLEAN:
         setOption(OPT_no_clean, true);
         break;
      case 'h': // print help message and exit
         PrintUsage(std::cout);
         exit_success= true;
         break;
      case 'V':
         PrintFullHeader(std::cout);
         exit_success= true;
         break;
#if !RELEASE
      case OPT_READ_PARAMETERS_XML:
      {
         setOption(OPT_read_parameter_xml, optarg_param);
         load_xml_configuration_file(getOption<std::string>(OPT_read_parameter_xml));
         break;
      }
      case OPT_WRITE_PARAMETERS_XML:
      {
         setOption(OPT_write_parameter_xml, optarg_param);
         break;
      }
#endif
      case 'v':
      {
         setOption(OPT_output_level, optarg_param);
         break;
      }
      case OPT_BENCHMARK_NAME:
      {
         setOption(OPT_benchmark_name, optarg_param);
         break;
      }
      case OPT_BENCHMARK_FAKE_PARAMETERS:
      {
         setOption(STR_OPT_benchmark_fake_parameters, optarg_param);
         break;
      }
      case 'd':
      {
#if HAVE_FROM_C_BUILT
         if(std::string(optarg_param) == "umpversion")
         {
            CompilerTarget preferred_compiler;
            preferred_compiler = getOption<CompilerTarget>(OPT_default_compiler);
#if HAVE_I386_GCC45_COMPILER
            if(preferred_compiler & CT_I386_GCC45)
            {
               PRINT_OUT_MEX(OUTPUT_LEVEL_NONE, 0, I386_GCC45_VERSION);
            }
#endif
#if HAVE_I386_GCC46_COMPILER
            if(preferred_compiler & CT_I386_GCC46)
            {
               PRINT_OUT_MEX(OUTPUT_LEVEL_NONE, 0, I386_GCC46_VERSION);
            }
#endif
#if HAVE_I386_GCC47_COMPILER
            if(preferred_compiler & CT_I386_GCC47)
            {
               PRINT_OUT_MEX(OUTPUT_LEVEL_NONE, 0, I386_GCC47_VERSION);
            }
#endif
#if HAVE_I386_GCC48_COMPILER
            if(preferred_compiler & CT_I386_GCC48)
            {
               PRINT_OUT_MEX(OUTPUT_LEVEL_NONE, 0, I386_GCC48_VERSION);
            }
#endif
#if HAVE_I386_GCC49_COMPILER
            if(preferred_compiler & CT_I386_GCC49)
            {
               PRINT_OUT_MEX(OUTPUT_LEVEL_NONE, 0, I386_GCC49_VERSION);
            }
#endif
#if HAVE_SPARC_COMPILER
            if(preferred_compiler & (CT_SPARC_GCC | CT_SPARC_ELF_GCC))
            {
               PRINT_OUT_MEX(OUTPUT_LEVEL_NONE, 0, SPARC_GCC_VERSION);
            }
#endif
#if HAVE_ARM_COMPILER
            if(preferred_compiler & CT_ARM_GCC)
            {
               PRINT_OUT_MEX(OUTPUT_LEVEL_NONE, 0, ARM_GCC_VERSION);
            }
            exit_success=true;
            break;
#endif
         }
#endif
         if(std::string(optarg_param) == "N")
         {
            std::string gcc_extra_options = "-dN";
            if(isOption(OPT_gcc_extra_options))
               gcc_extra_options = getOption<std::string>(OPT_gcc_extra_options) + " " + gcc_extra_options;
            setOption(OPT_gcc_extra_options, gcc_extra_options);
         }
         else
         {
            debug_level = boost::lexical_cast<int>(optarg_param);
            setOption(OPT_debug_level, optarg_param);
         }
         break;
      }
      case OPT_DEBUG_CLASSES:
      {
         std::vector<std::string> Splitted;
         boost::algorithm::split(Splitted, optarg_param, boost::algorithm::is_any_of(","));
         for (unsigned int i = 0; i < Splitted.size(); i++)
         {
            add_debug_class(Splitted[i]);
         }
         break;
      }
      case INPUT_OPT_ERROR_ON_WARNING:
      {
         error_on_warning = true;
         break;
      }
      case INPUT_OPT_PRINT_DOT:
      {
         setOption(OPT_print_dot, true);
         break;
      }
      case INPUT_OPT_SEED:
      {
         setOption(OPT_seed, optarg_param);
         break;
      }
      case OPT_OUTPUT_TEMPORARY_DIRECTORY:
      {
         ///If the path is not absolute, make it into absolute
         std::string path(optarg_param);
         std::string temporary_directory_pattern;
         if(path == "/tmp")
            temporary_directory_pattern = path+"/"+std::string(STR_CST_temporary_directory);
         else if(path == "/tmp/")
            temporary_directory_pattern = path+std::string(STR_CST_temporary_directory);
         else
            temporary_directory_pattern = path;
         //The Xs are required by the mkdtemp function
         temporary_directory_pattern = temporary_directory_pattern + "-XXXXXX";

         const char * cc_temporary_directory_pattern = temporary_directory_pattern.c_str();
         char * c_temporary_directory_pattern = new char [temporary_directory_pattern.size() +1];
         strcpy(c_temporary_directory_pattern, cc_temporary_directory_pattern);
         const char * temp = mkdtemp(c_temporary_directory_pattern);
         if(!temp)
            THROW_ERROR("Error in getting temporary directory starting from " + std::string(STR_CST_temporary_directory));
         path = std::string(temp);
         delete[] c_temporary_directory_pattern;

         if(path.at(0) != '/')
            path = boost::filesystem::current_path().string() + "/" + path;
         path = path + "/";
         setOption(OPT_output_temporary_directory, path);
         setOption(OPT_no_clean, true);
         break;
      }
      default:
      {
         ///next_option is not a Tool parameter
         return true;
      }
   }
   return false;
}

#if HAVE_FROM_C_BUILT
bool Parameter::ManageGccOptions(int next_option, char * optarg_param)
{
   switch (next_option)
   {
      case 'c':
         {
            setOption(OPT_gcc_c, true);
            break;
         }
      case 'D':
      {
         std::string defines;
         if(isOption(OPT_gcc_defines))
            defines = getOption<std::string>(OPT_gcc_defines) + STR_CST_string_separator;
         if(std::string(optarg_param).find('=') != std::string::npos)
            defines +=  "\'" + std::string(optarg_param) + "\'";
         else
            defines += std::string(optarg_param);
         setOption(OPT_gcc_defines, defines);
         break;
      }
      case 'f':
      {
         std::string optimizations;
         if(isOption(OPT_gcc_optimizations))
            optimizations = getOption<std::string>(OPT_gcc_optimizations) + STR_CST_string_separator;
         setOption(OPT_gcc_optimizations, optimizations + optarg_param);
         break;
      }
      case 'W':
      {
         std::string gcc_warnings;
         if(isOption(OPT_gcc_warnings))
            gcc_warnings = getOption<std::string>(OPT_gcc_warnings) + STR_CST_string_separator;
         setOption(OPT_gcc_warnings, gcc_warnings + optarg_param);
         break;
      }
      case 'E':
      {
         setOption(OPT_gcc_E, true);
         break;
      }
      case 'I':
      {
         std::string includes= "-I " + std::string(optarg);
         if(isOption(OPT_gcc_includes))
            includes = getOption<std::string>(OPT_gcc_includes) + " " + includes;
         setOption(OPT_gcc_includes, includes);
         break;
      }
      case 'l':
      {
         std::string libraries;
         if(isOption(OPT_gcc_libraries))
            libraries = getOption<std::string>(OPT_gcc_libraries) + STR_CST_string_separator;
         setOption(OPT_gcc_libraries, libraries + optarg_param);
         break;
      }
      case 'L':
      {
         std::string library_directories;
         if(isOption(OPT_gcc_library_directories))
            library_directories = getOption<std::string>(OPT_gcc_library_directories) + STR_CST_string_separator;
         setOption(OPT_gcc_library_directories, library_directories + optarg_param);
         break;
      }
      case 'O':
      {
         if (optarg_param)
         {
            const std::string opt_level = std::string(optarg_param);
            if(opt_level == "0")
            {
               setOption(OPT_gcc_opt_level, GccWrapper_OptimizationSet::O0);
            }
            else if(opt_level == "1")
            {
               setOption(OPT_gcc_opt_level, GccWrapper_OptimizationSet::O1);
            }
            else if(opt_level == "2")
            {
               setOption(OPT_gcc_opt_level, GccWrapper_OptimizationSet::O2);
            }
            else if(opt_level == "3")
            {
               setOption(OPT_gcc_opt_level, GccWrapper_OptimizationSet::O3);
            }
            else if(opt_level == "4")
            {
               setOption(OPT_gcc_opt_level, GccWrapper_OptimizationSet::O4);
            }
            else if(opt_level == "5")
            {
               setOption(OPT_gcc_opt_level, GccWrapper_OptimizationSet::O5);
            }
            else if(opt_level == "g")
            {
               setOption(OPT_gcc_opt_level, GccWrapper_OptimizationSet::Og);
            }
            else if(opt_level == "s")
            {
               setOption(OPT_gcc_opt_level, GccWrapper_OptimizationSet::Os);
            }
            else
            {
               THROW_ERROR("Unknown optimization level: " + opt_level);
            }
         }
         else
            setOption(OPT_gcc_opt_level, GccWrapper_OptimizationSet::O1);
         break;
      }
      case 'U':
      {
         std::string undefines;
         if(isOption(OPT_gcc_undefines))
            undefines = getOption<std::string>(OPT_gcc_undefines) + STR_CST_string_separator;
         setOption(OPT_gcc_undefines, undefines + optarg_param);
         break;
      }
      case INPUT_OPT_CUSTOM_OPTIONS:
      {
         setOption(OPT_gcc_extra_options, optarg);
         break;
      }
#if !RELEASE
      case INPUT_OPT_COMPUTE_SIZEOF:
      {
         setOption(OPT_compute_size_of, true);
         break;
      }
#endif
      case INPUT_OPT_COMPILER:
      {
#if HAVE_ARM_COMPILER
         if(std::string(optarg_param) == "ARM")
         {
            setOption(OPT_default_compiler, CT_ARM_GCC);
            break;
         }
#endif
#if HAVE_SPARC_COMPILER
         if(std::string(optarg_param) == "SPARC")
         {
            setOption(OPT_default_compiler, CT_SPARC_GCC);
            break;
         }
#endif
#if HAVE_I386_GCC45_COMPILER
         if(std::string(optarg_param) == "I386_GCC45")
         {
            setOption(OPT_default_compiler, CT_I386_GCC45);
            break;
         }
#endif
#if HAVE_I386_GCC46_COMPILER
         if(std::string(optarg_param) == "I386_GCC46")
         {
            setOption(OPT_default_compiler, CT_I386_GCC46);
            break;
         }
#endif
#if HAVE_I386_GCC47_COMPILER
         if(std::string(optarg_param) == "I386_GCC47")
         {
            setOption(OPT_default_compiler, CT_I386_GCC47);
            break;
         }
#endif
#if HAVE_I386_GCC48_COMPILER
         if(std::string(optarg_param) == "I386_GCC48")
         {
            setOption(OPT_default_compiler, CT_I386_GCC48);
            break;
         }
#endif
#if HAVE_I386_GCC49_COMPILER
         if(std::string(optarg_param) == "I386_GCC49")
         {
            setOption(OPT_default_compiler, CT_I386_GCC49);
            break;
         }
#endif
         THROW_ERROR("Unknown compiler " + std::string(optarg_param));
         break;
      }
      case INPUT_OPT_GCC_CONFIG:
      {
         setOption(OPT_gcc_config, true);
         break;
      }
      case INPUT_OPT_INCLUDE_SYSDIR:
      {
         setOption(OPT_gcc_include_sysdir, true);
         break;
      }
      case INPUT_OPT_PARAM:
      {
         std::string parameters;
         if(isOption(OPT_gcc_parameters))
            parameters = getOption<std::string>(OPT_gcc_parameters) + STR_CST_string_separator;
         setOption(OPT_gcc_parameters, parameters + optarg_param);
         break;
      }
      case INPUT_OPT_READ_GCC_XML:
      {
         setOption(OPT_gcc_read_xml, optarg);
         break;
      }
      case INPUT_OPT_STD:
      {
         setOption(OPT_gcc_standard, optarg_param);
         break;
      }
#if !RELEASE
      case INPUT_OPT_USE_RAW:
      {
         setOption(OPT_use_raw, true);
         break;
      }
#endif
      case INPUT_OPT_WRITE_GCC_XML:
      {
         setOption(OPT_gcc_write_xml, optarg);
         break;
      }
      default:
      {
         ///next_option is not a GCC parameter
         return true;
      }
   }
   return false;
}
#endif

FileFormat Parameter::GetFileFormat(const std::string & file_name, const bool check_xml_root_node) const
{
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Getting file format of file " + file_name);
   std::string extension = GetExtension(file_name);
   std::string base_name = GetLeafFileName(file_name);
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Extension is " + extension);
#if HAVE_FROM_CSV_BUILT
   if(extension == "csv")
   {
      if(base_name.find('.') != std::string::npos)
      {
         std::string local_extension = GetExtension(base_name);
         if(local_extension == "rtl")
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--CSV of RTL operations");
            return FF_CSV_RTL;
         }
         else if(local_extension == "tree")
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--CSV of TREE operations");
            return FF_CSV_TRE;
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--generic CSV");
      return FF_CSV;
   }
#endif
#if HAVE_FROM_LIBERTY
   if(extension == "lib")
   {
      return FF_LIB;
   }
#endif
#if HAVE_EXPERIMENTAL
   if(extension == "log")
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--log file");
      return FF_LOG;
   }
#endif
#if HAVE_FROM_PSPLIB_BUILT
   if(extension == "mm")
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Multi mode project scheduling problem");
      return FF_PSPLIB_MM;
   }
   if(extension == "sm")
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Single-mode project scheduling problem");
      return FF_PSPLIB_SM;
   }
#endif
   if(extension == "tex")
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Latex table");
      return FF_TEX;
   }
   if(extension == "v")
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--verilog");
      return FF_VERILOG;
   }
   if(extension == "vhd" or extension == "vhdl")
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--vhdl");
      return FF_VHDL;
   }
   if(extension == "xml")
   {
      if(check_xml_root_node)
      {
         xml_dom_parser parser;
         fileIO_istreamRef input_stream = fileIO_istream_open(file_name);
         parser.parse_stream(*input_stream);
         THROW_ASSERT(parser, "Impossible to parse xml file " + file_name);

#if HAVE_DESIGN_ANALYSIS_BUILT || HAVE_SOURCE_CODE_STATISTICS_XML || HAVE_FROM_IPXACT_BUILT || HAVE_FROM_SDF3_BUILT || HAVE_TO_DATAFILE_BUILT || HAVE_PERFORMANCE_METRICS_XML || HAVE_WEIGHT_MODELS_XML
         const xml_element* root = parser.get_document()->get_root_node();
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Root node is " + root->get_name());
#if HAVE_REGRESSORS_BUILT
         if(root->get_name() == STR_XML_aggregated_features_root)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Aggregated features data");
            return FF_XML_AGG;
         }
#endif
#if HAVE_FROM_ARCH_BUILT
         if(root->get_name() == STR_XML_architecture_root)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Architecture description");
            return FF_XML_ARCHITECTURE;
         }
#endif
#if HAVE_BAMBU_RESULTS_XML
         if(root->get_name() == "bambu_results")
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Bambu results");
            return FF_XML_BAMBU_RESULTS;
         }
#endif
#if HAVE_DESIGN_ANALYSIS_BUILT
         if(root->get_name() == STR_XML_design_analysis_hierarchy)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Design hierarchy");
            return FF_XML_DESIGN_HIERARCHY;
         }
#endif
#if HAVE_SOURCE_CODE_STATISTICS_XML
         if(root->get_name() == STR_XML_source_code_statistics_root)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Source code statistics");
            return FF_XML_STAT;
         }
#endif
#if HAVE_FROM_IPXACT_BUILT
         if(root->get_name() == STR_XML_ip_xact_component)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--IP-XACT component");
            return FF_XML_IP_XACT_COMPONENT;
         }
         if(root->get_name() == STR_XML_ip_xact_design)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--IP-XACT design");
            return FF_XML_IP_XACT_DESIGN;
         }
         if(root->get_name() == STR_XML_ip_xact_generator_chain)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--IP-XACT generator chain");
            return FF_XML_IP_XACT_GENERATOR;
         }
         if(root->get_name() == STR_XML_ip_xact_design_configuration)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--IP-XACT design configuration");
            return FF_XML_IP_XACT_CONFIG;
         }
#endif
#if HAVE_TO_DATAFILE_BUILT
         if(root->get_name() == STR_XML_latex_table_root)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Latex table format");
            return FF_XML_TEX_TABLE;
         }
#endif
#if HAVE_PERFORMANCE_METRICS_XML
         if(root->get_name() == STR_XML_Metrics)
         {
            const xml_node::node_list list = root->get_children();
            for (xml_node::node_list::const_iterator iter = list.begin(); iter != list.end(); ++iter)
            {
               const xml_element* static_or_dynamic = GetPointer<const xml_element>(*iter);
               if (!static_or_dynamic)
                  continue;
               if (static_or_dynamic->get_name() == STR_XML_Metrics_Static)
               {
                  const xml_node::node_list static_children = static_or_dynamic->get_children();
                  for (xml_node::node_list::const_iterator static_child = static_children.begin(); static_child != static_children.end(); static_child++)
                  {
                     const xml_element * static_child_xml = GetPointer<const xml_element>(*static_child);
                     if(!static_child_xml)
                        continue;
                     if(static_child_xml->get_name() == STR_XML_Metrics_Sequential_Estimation)
                     {
                        const xml_node::node_list sequential_children = static_child_xml->get_children();
                        for (xml_node::node_list::const_iterator sequential_child = sequential_children.begin(); sequential_child != sequential_children.end(); sequential_child++)
                        {
                           const xml_element * sequential_child_xml = GetPointer<const xml_element>(*sequential_child);
                           if(sequential_child_xml and sequential_child_xml->get_attribute(STR_XML_Metrics_Distribution))
                           {
                              const std::string distribution = sequential_child_xml->get_attribute(STR_XML_Metrics_Distribution)->get_value();
                              if(distribution == STR_XML_probability_distribution_stochastic)
                              {
                                 INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Symbolic distribution");
                                 return FF_XML_SYM_SIM;
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
#endif
#if HAVE_FROM_SDF3_BUILT
         if(root->get_name() == STR_XML_sdf3_root)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Sdf3 format");
            return FF_XML_SDF3;
         }
#endif
#if HAVE_REGRESSORS_BUILT
         if(root->get_name() == STR_XML_skip_rows_root)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Skip rows data");
            return FF_XML_SKIP_ROW;
         }
#endif
#if HAVE_WEIGHT_MODELS_XML && HAVE_EXPERIMENTAL
         if(root->get_name() == STR_XML_weights_root)
         {
            const xml_node::node_list list = root->get_children();
            for (xml_node::node_list::const_iterator iter = list.begin(); iter != list.end(); ++iter)
            {
               const xml_element* model = GetPointer<const xml_element>(*iter);
               if (not model)
                  continue;
               if (model->get_attribute(STR_XML_weights_distribution) and model->get_attribute(STR_XML_weights_distribution)->get_value() == STR_XML_probability_distribution_symbolic)
               {
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Symbolic distribution model");
                  return FF_XML_WGT_SYM;
               }
            }
         }
#endif
#endif
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Generic XML");
      return FF_XML;
   }
   THROW_ERROR("Unrecognized extension: " + extension);
   return FF_UNKNOWN;
}

void Parameter::PrintBugReport(std::ostream &os) const
{
   os << "Please report bugs to <" << PACKAGE_BUGREPORT << ">\n" << std::endl;
}

void Parameter::PrintGeneralOptionsUsage(std::ostream & os) const
{
   os
   << "  General options:\n"
   << "    --help, -h                      Display this usage information.\n"
   << "    --version, -V                   Display the version of the program.\n";
#if !RELEASE
   os
   << "    --read-parameters-XML=<file>    Read tool options from a XML file.\n"
   << "    --write-parameters-XML=<file>   Dump the tool options into a XML file.\n";
#endif
   os << std::endl;
}

void Parameter::PrintOutputOptionsUsage(std::ostream & os) const
{
   os
   << "  Framework options:\n"
   << "    --verbosity, -v <level>         Output verbosity -- 0 NONE, 1 MINIMUM"
#if !RELEASE
   << ", 2 VERBOSE, 3 PEDANTIC, 4 VERY PEDANTIC (default=1).\n"
#else
   << " (default=0).\n"
#endif
#if !RELEASE
   << "    --debug, -d <level>             Debugging verbosity -- 0 NONE, 1 MINIMUM, 2 VERBOSE, 3 PEDANTIC, 4 VERY PEDANTIC (default=1).\n" \
   << "    --debug-classes=<classes_list>  Set maximum debug level for classes in <classes_list>\n"
#endif
   << "    --no-clean                      Not removing temporary files.\n"
   << "    --benchmark-name=<name>         Set the name of the current benchmark for data collection.\n"
   << "    --benchmark-fake-parameters     Set the parameters string for data collection. The parameters in the string are not actually used.\n"
   << "    --output-temporary-directory    Set the directory where temporary files are saved.\n"
   << "    --print-dot                     Print dot graphs.\n"
   << "    --seed=<number>                 Set the seed of the random number generator (default=0).\n"
#if !RELEASE
   << "    --error-on-warning             Throw error on warning.\n"
#endif
   ;
   os << std::endl;
}

#if HAVE_FROM_C_BUILT
void Parameter::PrintGccOptionsUsage(std::ostream & os) const
{
   os
   << "  GCC options: \n"
   << "    --compiler=<processor>    Specify which compiler is used.\n"
#if HAVE_ARM_COMPILER
   << "                                      ARM (default);\n"
#endif
#if HAVE_SPARC_COMPILER
   << "                                      SPARC\n"
#endif
#if HAVE_I386_GCC45_COMPILER
   << "                                      I386_GCC45\n"
#endif
#if HAVE_I386_GCC46_COMPILER
   << "                                      I386_GCC46\n"
#endif
#if HAVE_I386_GCC47_COMPILER
   << "                                      I386_GCC47\n"
#endif
#if HAVE_I386_GCC48_COMPILER
   << "                                      I386_GCC48\n"
#endif
#if HAVE_I386_GCC49_COMPILER
      << "                                      I386_GCC49\n"
#endif
   << "    -O<level>                       Enable a specific optimization level (default=-O0, possible values are: -O0,-O1,-O2,-O3,-Os,-O4,-O5).\n"
   << "    -f<option>                      Enable or disable a GCC optimization option (any of the -f or -fno- GCC option is supported).\n"
   << "                                      In particular, -ftree-vectorize option triggers the high-level synthesis of vectorized operations.\n"
   << "    -I<path>                        Specify a path where headers are stored.\n"
   << "    -W=<warning>                    Specify a warning passed to GCC.\n"
   << "    -E                              Enable preprocessing mode of GCC.\n"
   << "    --std=<standard>                Assume that the input sources are for <standard> (default=gnu89).\n"
   << "    -D<name>                        Predefine name as a macro, with definition 1.\n"
   << "    -U<name>                        Remove existing definition for macro <name>.\n"
   << "    --param <name>=<value>          Set the amount <value> for the parameter <name> that could be used for some optimizations.\n"\
   << "    -l<library>                     Search the library named <library> when linking.\n"
   << "    -L<dir>                         Add directory <dir> to the list of directories to be searched for -l.\n"
#if !RELEASE
   << "    --use-raw                       Specify that input file is already a raw file and not source files.\n"
   << "    --read-GCC-XML=<file>           Read GCC options from a XML file.\n"
   << "    --write-GCC-XML=<file>          Dump the GCC compiler options into a XML file.\n"
#endif
   << "    --Include-sysdir                Return the system include directory used by the embedded GCC compiler.\n"
   << "    --gcc-config                    Return the GCC configuration.\n"
   #if !RELEASE
   << "    --compute-sizeof                Replace sizeof with the computed valued for the considered target architecture.\n"
   #endif
   << "    --extra-gcc-options             Specify custom extra options to the compiler.\n";
   os << std::endl;
}
#endif

template<>
const std::unordered_set<std::string> Parameter::getOption(const enum enum_option name) const
{
   std::unordered_set<std::string> ret;
   std::vector<std::string> splitted;
   const std::string to_be_splitted = getOption<std::string>(name);
   boost::algorithm::split(splitted, to_be_splitted, boost::algorithm::is_any_of(STR_CST_string_separator));
   size_t i_end = splitted.size();
   for (size_t i = 0; i < i_end; i++)
   {
      ret.insert(splitted[i]);
   }
   return ret;
}

template<>
const std::list<std::string> Parameter::getOption(const enum enum_option name) const
{
   std::list<std::string> ret;
   std::vector<std::string> splitted;
   const std::string to_be_splitted = getOption<std::string>(name);
   boost::algorithm::split(splitted, to_be_splitted, boost::algorithm::is_any_of(STR_CST_string_separator));
   size_t i_end = splitted.size();
   for (size_t i = 0; i < i_end; i++)
   {
      ret.push_back(splitted[i]);
   }
   return ret;
}

#if HAVE_FROM_C_BUILT
template<>
ProfilingMethod Parameter::getOption(const enum enum_option name) const
{
   return static_cast<ProfilingMethod>(getOption<int>(name));
}

template<>
AnalysisLevel Parameter::getOption(const enum enum_option name) const
{
   return static_cast<AnalysisLevel>(getOption<int>(name));
}

template<>
ProfilingArchitectureKind Parameter::getOption(const enum enum_option name) const
{
   return static_cast<ProfilingArchitectureKind>(getOption<int>(name));
}

template<>
CompilerTarget Parameter::getOption(const enum enum_option name) const
{
   return static_cast<CompilerTarget>(getOption<int>(name));
}
#endif

template<>
FileFormat Parameter::getOption(const enum enum_option name) const
{
   return static_cast<FileFormat>(getOption<int>(name));
}

template<>
std::unordered_set<PerformanceEstimationAlgorithm> Parameter::getOption(const enum enum_option name) const
{
   std::unordered_set<PerformanceEstimationAlgorithm> return_value;
   const std::string temp = getOption<std::string>(name);

   std::vector<std::string> splitted;
   boost::algorithm::split(splitted, temp, boost::algorithm::is_any_of(","));
   size_t i_end = splitted.size();
   for (size_t i = 0; i < i_end; i++)
   {
      if (splitted[i] == "")
         continue;
      if (splitted[i] == STR_CST_path_based)
         return_value.insert(PE_PATH_BASED);
      else if (splitted[i] == STR_CST_worst_case)
         return_value.insert(PE_WORST_CASE);
      else if (splitted[i] == STR_CST_average_case)
         return_value.insert(PE_AVERAGE_CASE);
      else
         THROW_ERROR("Unrecognized performance estimation algorithm: " + splitted[i]);
   }
   return return_value;
}

template<>
PerformanceEstimationAlgorithm Parameter::getOption(const enum enum_option name) const
{
   return static_cast<PerformanceEstimationAlgorithm>(getOption<int>(name));
}

#if HAVE_DIOPSIS
template<>
DiopsisInstrumentation Parameter::getOption(const enum enum_option name) const
{
   return static_cast<DiopsisInstrumentation>(getOption<int>(name));
}
#endif

#if HAVE_DESIGN_ANALYSIS_BUILT
template<>
DesignAnalysisStep Parameter::getOption(const enum enum_option name) const
{
   return static_cast<DesignAnalysisStep>(getOption<int>(name));
}
#endif

#if HAVE_FROM_C_BUILT
template<>
GccWrapper_OptimizationSet Parameter::getOption(const enum enum_option name) const
{
   return static_cast<GccWrapper_OptimizationSet>(getOption<int>(name));
}
template<>
void Parameter::setOption(const enum enum_option name, const GccWrapper_OptimizationSet value)
{
   enum_options[name] = boost::lexical_cast<std::string>(static_cast<int>(value));
}
#endif

#if HAVE_TO_C_BUILT
template<>
CThreadBackendType Parameter::getOption(const enum enum_option name) const
{
   return static_cast<CThreadBackendType>(getOption<int>(name));
}
#endif
