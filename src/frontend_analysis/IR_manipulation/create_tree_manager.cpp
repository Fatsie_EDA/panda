/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file create_tree_manager.cpp
 * @brief Implementation of the class for creating the tree_manager starting from the source code files
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_FROM_PRAGMA_BUILT.hpp"
#include "config_NPROFILE.hpp"

///Header include
#include "create_tree_manager.hpp"

///behavior include
#include "application_manager.hpp"

/// Intermediate Representation at raw level
#include "parse_tree.hpp"
#include "tree_manager.hpp"

///Wrapper include
#include "gcc_wrapper.hpp"

///Utility include
#include "cpu_time.hpp"
#include "global_enums.hpp"
#include "Parameter.hpp"
#include "fileIO.hpp"

create_tree_manager::create_tree_manager(const ParameterConstRef _parameters, const application_managerRef _AppM, const DesignFlowManagerConstRef _design_flow_manager) :
   ApplicationFrontendFlowStep(_AppM, CREATE_TREE_MANAGER, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

create_tree_manager::~create_tree_manager()
{

}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > create_tree_manager::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(POST_PRECEDENCE_RELATIONSHIP) :
         {
            break;
         }
      case(PRECEDENCE_RELATIONSHIP) :
         {
#if HAVE_FROM_PRAGMA_BUILT
            relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(PRAGMA_SUBSTITUTION, WHOLE_APPLICATION));
#endif
#if HAVE_ZEBU_BUILT
            relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SIZEOF_SUBSTITUTION, WHOLE_APPLICATION));
#endif
            break;
         }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void create_tree_manager::Exec()
{
   const tree_managerRef TreeM = AppM->get_tree_manager();
   long cpu_time;
   START_TIME(cpu_time);

   if(!parameters->isOption(OPT_input_file))
      THROW_ERROR("At least one source file has to be passed to the tool");

   /// parsing of archive files
   if(parameters->isOption(OPT_archive_files))
   {
      const std::unordered_set<std::string> archive_files = parameters->getOption<const std::unordered_set<std::string> >(OPT_archive_files);
      std::unordered_set<std::string>::const_iterator archive_file, archive_file_end = archive_files.end();
      for(archive_file = archive_files.begin(); archive_file != archive_file_end; archive_file++)
      {
         if(!boost::filesystem::exists(boost::filesystem::path(*archive_file)))
         {
            THROW_ERROR("File " + *archive_file + " does not exist");
         }
         tree_managerRef TM_new;
         std::string temporary_file = "temp.o";
         std::string local_archive_file = *archive_file;
         if(local_archive_file[0] != '/')
            local_archive_file = "../" + local_archive_file;
         std::string command = "cd " + parameters->getOption<std::string>(OPT_output_temporary_directory) + "; ar p " + local_archive_file + "> " + temporary_file + "; echo """;
         //std::cout << command << std::endl;
         int ret = PandaSystem(parameters, command);
         if(IsError(ret))
         {
            THROW_ERROR("ar returns an error during archive extraction ");
         }
         TM_new = ParseTreeFile(parameters, (boost::filesystem::path(parameters->getOption<std::string>(OPT_output_temporary_directory)) / temporary_file).string());
         TreeM->merge_tree_managers(TM_new);
         std::remove((boost::filesystem::path(parameters->getOption<std::string>(OPT_output_temporary_directory)) / boost::filesystem::path(temporary_file)).string().c_str());
      }
   }


   if (parameters->getOption<bool>(OPT_use_raw))
   {
#if !NPROFILE
      long int tree_time = 0;
      START_TIME(tree_time);
#endif
      if(output_level >= OUTPUT_LEVEL_MINIMUM)
      {
         if(output_level >= OUTPUT_LEVEL_VERBOSE)
         {
            INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
            INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
            INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "");
            INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*********************************************************************************");
            INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*               Building internal representation from raw files                 *");
            INDENT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "*********************************************************************************");
         }
         else
         {
            INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, "");
            INDENT_OUT_MEX(OUTPUT_LEVEL_MINIMUM, output_level, " =============== Building internal representation from raw files ===============");
         }
      }
      const std::unordered_set<std::string> raw_files = parameters->getOption<const std::unordered_set<std::string> >(OPT_input_file);
      std::unordered_set<std::string>::const_iterator raw_file, raw_file_end = raw_files.end();
      for(raw_file = raw_files.begin(); raw_file != raw_file_end; raw_file++)
      {
         if(!boost::filesystem::exists(boost::filesystem::path(*raw_file)))
         {
            THROW_ERROR("File " + *raw_file + " does not exist");
         }
         tree_managerRef TreeM_tmp = ParseTreeFile(parameters, *raw_file);
         TreeM->merge_tree_managers(TreeM_tmp);
      }
#if !NPROFILE
      STOP_TIME(tree_time);
      if (output_level >= OUTPUT_LEVEL_MINIMUM)
      {
         dump_exec_time("Tree analysis time", tree_time);
      }
#endif
   }
   else
   {
#if !NPROFILE
      long int wrapping_time = 0;
      START_TIME(wrapping_time);
#endif
      const CompilerTarget compiler_target = parameters->getOption<CompilerTarget>(OPT_default_compiler);
      const GccWrapper_OptimizationSet optimization_set = parameters->getOption<GccWrapper_OptimizationSet>(OPT_gcc_optimization_set);

      const GccWrapperRef gcc_wrapper = GccWrapperRef(new GccWrapper(parameters, compiler_target, optimization_set));

#if !RELEASE
      // if a XML configuration file has been specified for the GCC parameters
      if (parameters->isOption(OPT_gcc_read_xml))
         gcc_wrapper->ReadXml(parameters->getOption<std::string>(OPT_gcc_read_xml));
#endif
      gcc_wrapper->FillTreeManager(TreeM, AppM->input_files);

#if !RELEASE
      if (parameters->isOption(OPT_gcc_write_xml))
         gcc_wrapper->WriteXml(parameters->getOption<std::string>(OPT_gcc_write_xml));
#endif


   if (debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      std::string raw_file_name = parameters->getOption<std::string>(OPT_output_temporary_directory) + "after_raw_merge.raw";
      std::ofstream raw_file(raw_file_name.c_str());
      INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Tree-Manager dumped for debug purpose");
      raw_file << TreeM;
      raw_file.close();
   }


#if !NPROFILE
      STOP_TIME(wrapping_time);
      if (debug_level >= DEBUG_LEVEL_MINIMUM)
      {
         dump_exec_time("Gcc wrapping time", wrapping_time);
      }
#endif
   }
   STOP_TIME(cpu_time);
   PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, "  IR created in " + print_cpu_time(cpu_time) + " seconds\n");

}
