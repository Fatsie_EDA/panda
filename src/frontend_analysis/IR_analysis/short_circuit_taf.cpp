/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file short_circuit_taf.cpp
 * @brief Analysis step rebuilding a short circuit in a single gimple_cond with the condition in three address form.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "short_circuit_taf.hpp"

#include "phi_opt.hpp"

///Behavior include
#include "application_manager.hpp"
#include "call_graph.hpp"
#include "call_graph_manager.hpp"

///Parameter include
#include "Parameter.hpp"


///STD include
#include <fstream>

///STL include
#include <unordered_set>

///tree includes
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"
#include "tree_basic_block.hpp"
#include "tree_helper.hpp"

short_circuit_taf::short_circuit_taf(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, SHORT_CIRCUIT_TAF, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

short_circuit_taf::~short_circuit_taf()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > short_circuit_taf::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BLOCK_FIX, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SWITCH_FIX, SAME_FUNCTION));
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SIMPLE_CODE_MOTION, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(PHI_OPT, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}


bool short_circuit_taf::check_phis(unsigned int curr_bb, std::map<unsigned int, blocRef> & list_of_bloc)
{
   std::vector<tree_nodeRef> & list_of_phi_curr_bb = list_of_bloc[curr_bb]->list_of_phi;
   const size_t n_curr_bb_phis = list_of_phi_curr_bb.size();
   for(size_t curr_bb_phi_index = 0; curr_bb_phi_index < n_curr_bb_phis; ++curr_bb_phi_index)
   {
      gimple_phi *cb_phi = GetPointer<gimple_phi>(GET_NODE(list_of_phi_curr_bb[curr_bb_phi_index]));
      if(cb_phi->virtual_flag)
         return false;
   }
   return true;
}

void short_circuit_taf::Exec()
{
   PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "START short circuit conversion");
   const tree_managerRef TM = AppM->get_tree_manager();
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }
   tree_nodeRef temp = TM->get_tree_node_const(function_id);
   function_decl * fd = GetPointer<function_decl>(temp);
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));

   std::map<unsigned int, blocRef> & list_of_bloc = sl->list_of_bloc;
   std::map<unsigned int, blocRef>::iterator it, it_end = list_of_bloc.end();

   ///compute merging candidates
   std::unordered_set<unsigned int> merging_candidates;
   for(it = list_of_bloc.begin(); it != it_end; it++)
   {
      if(it->first == bloc::ENTRY_BLOCK_ID || it->first == bloc::EXIT_BLOCK_ID)
         continue;
      unsigned int n_pred_bb = 0;
      if(it->second->list_of_pred.size() <= 1)
         continue;
      std::vector<unsigned int>::const_iterator it_pred_end = it->second->list_of_pred.end();
      for(std::vector<unsigned int>::const_iterator it_pred = it->second->list_of_pred.begin(); it_pred_end != it_pred; it_pred++)
      {
         if(
               !(*it_pred == bloc::ENTRY_BLOCK_ID ||
                it->first == *it_pred ||
                list_of_bloc[*it_pred]->list_of_stmt.empty() ||
                GET_NODE(*(list_of_bloc[*it_pred]->list_of_stmt.rbegin()))->get_kind() != gimple_cond_K
                )
               )
         {
            ++n_pred_bb;
         }
      }
      if(n_pred_bb > 1 && check_phis(it->first, list_of_bloc))
         merging_candidates.insert(it->first);
   }
   if(!merging_candidates.empty())
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Merging candidate number " + boost::lexical_cast<std::string>(merging_candidates.size()));
   else
      return;

   ///find the first to merge
   unsigned int bb1, bb2, merging_candidate;
   bool bb1_true = false;
   bool bb2_true = false;
   bool mergeable_pair_found;
   do
   {
      mergeable_pair_found = false;
      std::unordered_set<unsigned int>::const_iterator it_mc_end = merging_candidates.end();
      for(std::unordered_set<unsigned int>::const_iterator it_mc = merging_candidates.begin(); !mergeable_pair_found && it_mc != it_mc_end; it_mc++)
      {
         merging_candidate = *it_mc;
         mergeable_pair_found = check_merging_candidate(bb1, bb2, merging_candidate, bb1_true, bb2_true, list_of_bloc);
      }
      if(mergeable_pair_found)
      {
         if(create_gimple_cond(bb1, bb2, bb1_true, list_of_bloc, bb2_true, merging_candidate))
         {
            restructure_CFG(bb1, bb2, merging_candidate, list_of_bloc);
            if(merging_candidates.find(bb1) != merging_candidates.end())
               merging_candidates.erase(bb1);
            if(!check_merging_candidate(bb1, bb2, merging_candidate, bb1_true, bb2_true, list_of_bloc))
               merging_candidates.erase(merging_candidate);
         }
         else ///cond expr not completely collapsed
            merging_candidates.erase(merging_candidate);
      }
   } while(mergeable_pair_found);
   if(debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
   {
      PrintTreeManager(false);
   }
}

bool short_circuit_taf::check_merging_candidate(unsigned int &bb1, unsigned int &bb2, unsigned int merging_candidate, bool &bb1_true, bool &bb2_true, std::map<unsigned int, blocRef> & list_of_bloc)
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Checking merging candidate " + boost::lexical_cast<std::string>(merging_candidate));
   bool mergeable_pair_found = false;
   ///let bb1 the upper if
   THROW_ASSERT(list_of_bloc.find(merging_candidate) != list_of_bloc.end(), "merging_candidate is not included in list_of_bloc");
   const std::vector<unsigned int>::const_iterator it_pred_end = list_of_bloc[merging_candidate]->list_of_pred.end();
   for(std::vector<unsigned int>::const_iterator it_bb1_pred = list_of_bloc[merging_candidate]->list_of_pred.begin(); !mergeable_pair_found && it_pred_end != it_bb1_pred; ++it_bb1_pred)
   {
      bb1 = *it_bb1_pred;
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Examining merging candidate predecessor " + boost::lexical_cast<std::string>(bb1));
      if(bb1 == bloc::ENTRY_BLOCK_ID || bb1 == merging_candidate)
         continue;
      if(list_of_bloc[bb1]->list_of_stmt.empty()) continue;
      if(GET_NODE(*(list_of_bloc[bb1]->list_of_stmt.rbegin()))->get_kind() != gimple_cond_K) continue;
      THROW_ASSERT(list_of_bloc[bb1]->true_edge > 0, "bb1 has to be an if statement "+boost::lexical_cast<std::string>(bb1) + " " + boost::lexical_cast<std::string>(merging_candidate));
      if(list_of_bloc[bb1]->true_edge == merging_candidate)
         bb1_true = true;
      else
         bb1_true = false;
      ///let search bb2, the lower if
      for(std::vector<unsigned int>::const_iterator it_bb2_pred = list_of_bloc[merging_candidate]->list_of_pred.begin(); !mergeable_pair_found && it_pred_end != it_bb2_pred; ++it_bb2_pred)
      {
         bb2 = *it_bb2_pred;
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Examining merging candidate nested predecessor " + boost::lexical_cast<std::string>(bb2));
         if(bb2 == bloc::ENTRY_BLOCK_ID || bb2 == merging_candidate) 
            continue;
         if(list_of_bloc[bb2]->list_of_pred.size() > 1)
            continue;
         if(list_of_bloc[bb2]->list_of_stmt.size() != 1) continue;
         if(list_of_bloc[bb2]->list_of_phi.size() != 0) continue;
         if(GET_NODE(*(list_of_bloc[bb2]->list_of_stmt.rbegin()))->get_kind() != gimple_cond_K) continue;
         THROW_ASSERT(list_of_bloc[bb2]->true_edge > 0, "bb2 has to be an if statement "+boost::lexical_cast<std::string>(bb2) + " " + boost::lexical_cast<std::string>(merging_candidate));
         //This check is needed for empty while loop with short circuit (e. g. 20000314-1.c)
         if(list_of_bloc[bb2]->true_edge == bb1 || list_of_bloc[bb2]->false_edge == bb1)
            continue;
         if(bb1_true && list_of_bloc[bb1]->false_edge == bb2)
         {
            if(list_of_bloc[bb2]->true_edge == merging_candidate)
               bb2_true = true;
            else
               bb2_true = false;
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Pair found: " << boost::lexical_cast<std::string>(bb1) << " and " << boost::lexical_cast<std::string>(bb2));
            mergeable_pair_found = true;
            if(bb2_true)
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "bb1 T " + boost::lexical_cast<std::string>(bb1) + " bb2 T " + boost::lexical_cast<std::string>(bb2) + " MC " + boost::lexical_cast<std::string>(merging_candidate));
            else
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "bb1 T " + boost::lexical_cast<std::string>(bb1) + " bb2 F " + boost::lexical_cast<std::string>(bb2) + " MC " + boost::lexical_cast<std::string>(merging_candidate));
         }
         else if(!bb1_true && list_of_bloc[bb1]->true_edge == bb2)
         {
            if(list_of_bloc[bb2]->true_edge == merging_candidate)
               bb2_true = true;
            else
               bb2_true = false;
            mergeable_pair_found = true;
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Pair found: " << boost::lexical_cast<std::string>(bb1) << " and " << boost::lexical_cast<std::string>(bb2));
            if(bb2_true)
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "bb1 F " + boost::lexical_cast<std::string>(bb1) + " bb2 T " + boost::lexical_cast<std::string>(bb2) + " MC " + boost::lexical_cast<std::string>(merging_candidate));
            else
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "bb1 F " + boost::lexical_cast<std::string>(bb1) + " bb2 F " + boost::lexical_cast<std::string>(bb2) + " MC " + boost::lexical_cast<std::string>(merging_candidate));
         }
      }
   }
   return mergeable_pair_found;
}

bool short_circuit_taf::create_gimple_cond(unsigned int bb1, unsigned int bb2, bool bb1_true, std::map<unsigned int, blocRef> & list_of_bloc, bool or_type, unsigned int merging_candidate)
{
   const tree_managerRef TM = AppM->get_tree_manager();

   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Creating new cond expr: " + boost::lexical_cast<std::string>(bb1) + " is the first basic block, " + boost::lexical_cast<std::string>(bb2) + " is the second basic block");
   /// If there are more than one statements in the basic block containing cond2, then do not merge conditions (in case speculation step should manage the code motion)
   if(list_of_bloc[bb2]->list_of_stmt.size() != 1) return false;
   std::list<tree_nodeRef> & list_of_stmt_cond2 = list_of_bloc[bb2]->list_of_stmt;

   ///identify the first gimple_cond
   std::list<tree_nodeRef> & list_of_stmt_cond1 = list_of_bloc[bb1]->list_of_stmt;
   THROW_ASSERT(GET_NODE(list_of_stmt_cond1.back())->get_kind() == gimple_cond_K, "a gimple_cond is expected");
   tree_nodeRef cond_statement = list_of_stmt_cond1.back();
   list_of_stmt_cond1.erase(std::find(list_of_stmt_cond1.begin(), list_of_stmt_cond1.end(), cond_statement));

   gimple_cond* ce1 = GetPointer<gimple_cond>(GET_NODE(cond_statement));
   unsigned int cond1_index = GET_INDEX_NODE(ce1->op0);
   unsigned int type_index;
   tree_helper::get_type_node(GET_NODE(ce1->op0), type_index);
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;

   /// create the ssa_var representing the condition for bb1
   unsigned int ssa1_vers = TM->get_next_vers();
   unsigned int ssa1_node_nid = TM->new_tree_node_id();
   IR_schema[TOK(TOK_TYPE)] = STR(type_index);
   IR_schema[TOK(TOK_VERS)] = STR(ssa1_vers);
   IR_schema[TOK(TOK_VOLATILE)] = STR(false);
   IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
   TM->create_tree_node(ssa1_node_nid, ssa_name_K, IR_schema);
   IR_schema.clear();
   tree_nodeRef ssa1_cond_node = TM->GetTreeReindex(ssa1_node_nid);

   ///create the assignment between condition for bb1 and the new ssa var
   unsigned int cond1_gimple_stmt_id = TM->new_tree_node_id();
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa1_node_nid);
   IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(cond1_index);
   TM->create_tree_node(cond1_gimple_stmt_id, gimple_assign_K, IR_schema);
   IR_schema.clear();
   tree_nodeRef cond1_created_stmt = TM->GetTreeReindex(cond1_gimple_stmt_id);
   GetPointer<ssa_name>(GET_NODE(ssa1_cond_node))->add_def(cond1_created_stmt);
   /// and then add to the bb1 statement list
   GetPointer<gimple_node>(GET_NODE(cond1_created_stmt))->bb_index = bb1;
   list_of_stmt_cond1.push_back(cond1_created_stmt);
   cond1_index = ssa1_node_nid;

   /// fix merging_candidate phis
   if(list_of_bloc[merging_candidate]->list_of_phi.size())
   {
      std::vector<tree_nodeRef> & list_of_phi_mc = list_of_bloc[merging_candidate]->list_of_phi;
      const size_t n_mc_phis = list_of_phi_mc.size();
      for(size_t mc_phi_index = 0; mc_phi_index < n_mc_phis; ++mc_phi_index)
      {
         gimple_phi *mc_phi = GetPointer<gimple_phi>(GET_NODE(list_of_phi_mc[mc_phi_index]));
         const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_mc_it_end = mc_phi->list_of_def_edge.end();
         std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator phi_to_be_removed=lode_mc_it_end;
         std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator phi_to_be_updated=lode_mc_it_end;
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_mc_it = mc_phi->list_of_def_edge.begin(); lode_mc_it_end != lode_mc_it; ++lode_mc_it)
         {
            if(lode_mc_it->second == bb1)
               phi_to_be_removed = lode_mc_it;
            else if(lode_mc_it->second == bb2)
               phi_to_be_updated = lode_mc_it;
         }
         THROW_ASSERT(phi_to_be_removed != lode_mc_it_end, "unexpected condition");
         THROW_ASSERT(phi_to_be_updated != lode_mc_it_end, "unexpected condition");
         unsigned int op1 = GET_INDEX_NODE(phi_to_be_removed->first);
         unsigned int op2 = GET_INDEX_NODE(phi_to_be_updated->first);
         if(!bb1_true)
            std::swap(op1, op2);

         unsigned int res_type_index = tree_helper::get_type_index(TM, GET_INDEX_NODE(mc_phi->res));

         /// create the ssa_var representing the result of the cond_expr
         unsigned int ssa_vers = TM->get_next_vers();
         unsigned int ssa_node_nid = TM->new_tree_node_id();
         IR_schema[TOK(TOK_TYPE)] = STR(res_type_index);
         IR_schema[TOK(TOK_VERS)] = STR(ssa_vers);
         IR_schema[TOK(TOK_VOLATILE)] = STR(false);
         IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
         TM->create_tree_node(ssa_node_nid, ssa_name_K, IR_schema);
         IR_schema.clear();
         tree_nodeRef ssa_cond_node = TM->GetTreeReindex(ssa_node_nid);
         unsigned int cond_expr_id = TM->new_tree_node_id();
         IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
         IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(res_type_index);
         IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(cond1_index);
         IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(op1);
         IR_schema[TOK(TOK_OP2)] = boost::lexical_cast<std::string>(op2);
         TM->create_tree_node(cond_expr_id, cond_expr_K, IR_schema);
         IR_schema.clear();
         /// second, create the gimple assignment
         unsigned int gimple_stmt_id = TM->new_tree_node_id();
         IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
         IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa_node_nid);
         IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(cond_expr_id);
         IR_schema[TOK(TOK_ORIG)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(list_of_phi_mc[mc_phi_index]));
         TM->create_tree_node(gimple_stmt_id, gimple_assign_K, IR_schema);
         IR_schema.clear();
         tree_nodeRef created_stmt = TM->GetTreeReindex(gimple_stmt_id);
         GetPointer<ssa_name>(GET_NODE(ssa_cond_node))->add_def(created_stmt);

         /// and then add to the statement list
         GetPointer<gimple_node>(GET_NODE(created_stmt))->bb_index = bb1;
         list_of_stmt_cond1.push_back(created_stmt);
         phi_to_be_updated->first = ssa_cond_node;
         mc_phi->list_of_def_edge.erase(phi_to_be_removed);
      }

   }

   if((bb1_true && !or_type) || (!bb1_true && or_type))
   {      
      ///cond1 has to be negate
      /// create the ssa_var representing the negated condition
      unsigned int ncond_ssa_vers = TM->get_next_vers();
      unsigned int ncond_ssa_node_nid = TM->new_tree_node_id();
      IR_schema[TOK(TOK_TYPE)] = STR(type_index);
      IR_schema[TOK(TOK_VERS)] = STR(ncond_ssa_vers);
      IR_schema[TOK(TOK_VOLATILE)] = STR(false);
      IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
      TM->create_tree_node(ncond_ssa_node_nid, ssa_name_K, IR_schema);
      IR_schema.clear();
      tree_nodeRef ncond_ssa_cond_node = TM->GetTreeReindex(ncond_ssa_node_nid);

      ///create !cond1
      IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(type_index);
      IR_schema[TOK(TOK_OP)] = boost::lexical_cast<std::string>(cond1_index);
      IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
      cond1_index = TM->new_tree_node_id();
      TM->create_tree_node(cond1_index, truth_not_expr_K, IR_schema);
      IR_schema.clear();

      unsigned int ncond_gimple_stmt_id = TM->new_tree_node_id();
      IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
      IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ncond_ssa_node_nid);
      IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(cond1_index);
      TM->create_tree_node(ncond_gimple_stmt_id, gimple_assign_K, IR_schema);
      IR_schema.clear();
      tree_nodeRef created_stmt = TM->GetTreeReindex(ncond_gimple_stmt_id);
      GetPointer<ssa_name>(GET_NODE(ncond_ssa_cond_node))->def_stmts.clear();
      GetPointer<ssa_name>(GET_NODE(ncond_ssa_cond_node))->add_def(created_stmt);
      /// and then add to the bb1 statement list
      GetPointer<gimple_node>(GET_NODE(created_stmt))->bb_index = bb1;
      list_of_stmt_cond1.push_back(created_stmt);
      cond1_index = ncond_ssa_node_nid;


   }
   ///identify the second gimple_cond
   THROW_ASSERT(list_of_bloc[bb2]->list_of_phi.size() == 0, "not expected phi nodes");

   THROW_ASSERT(GET_NODE(list_of_stmt_cond2.front())->get_kind() == gimple_cond_K, "a gimple_cond is expected");

   gimple_cond* ce2 = GetPointer<gimple_cond>(GET_NODE(list_of_stmt_cond2.front()));

   unsigned int cond2_index = GET_INDEX_NODE(ce2->op0);
   unsigned int type_index2;
   tree_helper::get_type_node(GET_NODE(ce2->op0), type_index2);
   THROW_ASSERT(type_index == type_index2, "something of unexpected is happened");
   /// create the ssa_var representing the condition for bb2
   unsigned int ssa2_vers = TM->get_next_vers();
   unsigned int ssa2_node_nid = TM->new_tree_node_id();
   IR_schema[TOK(TOK_TYPE)] = STR(type_index);
   IR_schema[TOK(TOK_VERS)] = STR(ssa2_vers);
   IR_schema[TOK(TOK_VOLATILE)] = STR(false);
   IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
   TM->create_tree_node(ssa2_node_nid, ssa_name_K, IR_schema);
   IR_schema.clear();
   tree_nodeRef ssa2_cond_node = TM->GetTreeReindex(ssa2_node_nid);

   ///create the assignment between condition for bb2 and the new ssa var
   unsigned int cond2_gimple_stmt_id = TM->new_tree_node_id();
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa2_node_nid);
   IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(cond2_index);
   TM->create_tree_node(cond2_gimple_stmt_id, gimple_assign_K, IR_schema);
   IR_schema.clear();
   tree_nodeRef cond2_created_stmt = TM->GetTreeReindex(cond2_gimple_stmt_id);
   GetPointer<ssa_name>(GET_NODE(ssa2_cond_node))->add_def(cond2_created_stmt);
   /// and then add to the bb1 statement list
   GetPointer<gimple_node>(GET_NODE(cond2_created_stmt))->bb_index = bb1;
   list_of_stmt_cond1.push_back(cond2_created_stmt);
   cond2_index = ssa2_node_nid;

   ///create (!)cond1 or cond2
   IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(type_index);
   IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(cond1_index);
   IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(cond2_index);
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   unsigned int expr_index = TM->new_tree_node_id();
   if(or_type)
      TM->create_tree_node(expr_index, truth_or_expr_K, IR_schema);
   else
      TM->create_tree_node(expr_index, truth_and_expr_K, IR_schema);
   IR_schema.clear();
   /// The expression contained in ce2 must now be the newly created expression,
   /// identified by expr_index
   ce2->op0 = TM->GetTreeReindex(expr_index);

   ///add the statements of bb1 to bb2
   std::list<tree_nodeRef>::const_reverse_iterator losc_it_end = list_of_stmt_cond1.rend();
   std::list<tree_nodeRef>::const_reverse_iterator losc_it = list_of_stmt_cond1.rbegin();
   for(; losc_it != losc_it_end; ++losc_it)
   {
      list_of_stmt_cond2.insert(list_of_stmt_cond2.begin(), *losc_it);
      GetPointer<gimple_node>(GET_NODE(*losc_it))->bb_index = bb2;
   }
   if(list_of_bloc[bb1]->list_of_phi.size())
      list_of_bloc[bb2]->list_of_phi = list_of_bloc[bb1]->list_of_phi;
   std::vector<tree_nodeRef>::iterator lop_it_end = list_of_bloc[bb2]->list_of_phi.end();
   for(std::vector<tree_nodeRef>::iterator lop_it = list_of_bloc[bb2]->list_of_phi.begin(); lop_it != lop_it_end; ++lop_it)
   {
      GetPointer<gimple_node>(GET_NODE(*lop_it))->bb_index = bb2;
   }

   PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "GIMPLE_COND built");
   return true;
}


void short_circuit_taf::restructure_CFG(unsigned int bb1, unsigned int bb2, unsigned int merging_candidate, std::map<unsigned int, blocRef> & list_of_bloc)
{
   ///fix bb2 predecessor
   std::vector<unsigned int>::iterator pos;
   std::vector<unsigned int>::const_iterator it_bb1_pred_end = list_of_bloc[bb1]->list_of_pred.end();
   for(std::vector<unsigned int>::const_iterator it_bb1_pred = list_of_bloc[bb1]->list_of_pred.begin(); it_bb1_pred_end != it_bb1_pred; it_bb1_pred++)
   {
      list_of_bloc[bb2]->list_of_pred.push_back(*it_bb1_pred);
      pos = std::find(list_of_bloc[*it_bb1_pred]->list_of_succ.begin(), list_of_bloc[*it_bb1_pred]->list_of_succ.end(), bb1);
      *pos = bb2;
      if(list_of_bloc[*it_bb1_pred]->true_edge == bb1)
         list_of_bloc[*it_bb1_pred]->true_edge = bb2;
      else if(list_of_bloc[*it_bb1_pred]->false_edge == bb1)
         list_of_bloc[*it_bb1_pred]->false_edge = bb2;
   }
   pos = std::find(list_of_bloc[bb2]->list_of_pred.begin(), list_of_bloc[bb2]->list_of_pred.end(), bb1);
   list_of_bloc[bb2]->list_of_pred.erase(pos);
   ///fix bb1 empty block
   pos = std::find(list_of_bloc[merging_candidate]->list_of_pred.begin(), list_of_bloc[merging_candidate]->list_of_pred.end(), bb1);
   list_of_bloc[merging_candidate]->list_of_pred.erase(pos);
   PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Removed BB " + boost::lexical_cast<std::string>(bb1));
   /// check for BB with gimple_multi_way_if
   phi_opt::fix_multi_way_if(bb1, list_of_bloc, bb2);
   list_of_bloc.erase(bb1);
}


