/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file operations_cfg_computation.cpp
 * @brief Analysis step creating the control flow graph of the operations
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_BAMBU_BUILT.hpp"

///Header include
#include "operations_cfg_computation.hpp"

///Behavior include
#include "basic_block.hpp"
#include "basic_blocks_graph_constructor.hpp"
#include "behavioral_helper.hpp"
#include "call_graph_manager.hpp"
#include "application_manager.hpp"
#include "function_behavior.hpp"
#include "op_graph.hpp"
#include "operations_graph_constructor.hpp"

///Graph include
#include "graph.hpp"

///Parameter include
#include "Parameter.hpp"

///Tree include
#include "ext_tree_node.hpp"
#include "tree_basic_block.hpp"
#include "tree_helper.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"

operations_cfg_computation::operations_cfg_computation(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, OPERATIONS_CFG_COMPUTATION, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}


operations_cfg_computation::~operations_cfg_computation()
{

}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > operations_cfg_computation::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BB_FEEDBACK_EDGES_IDENTIFICATION, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      {
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      case(PRECEDENCE_RELATIONSHIP) :
      {
#if HAVE_BAMBU_BUILT
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(DETERMINE_MEMORY_ACCESSES, SAME_FUNCTION));
#endif
#if HAVE_ZEBU_BUILT
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(LOOPS_ANALYSIS, SAME_FUNCTION));
#endif
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void operations_cfg_computation::Exec()
{
   const tree_managerRef TM = AppM->get_tree_manager();
   const BBGraphRef fbb = function_behavior->GetBBGraph(FunctionBehavior::FBB);
   const BehavioralHelperConstRef helper = function_behavior->CGetBehavioralHelper();
   const operations_graph_constructorRef & ogc = function_behavior->ogc;
   const BasicBlocksGraphConstructorRef bbgc = function_behavior->bbgc;
   const std::unordered_set<unsigned int> root_functions = AppM->CGetCallGraphManager()->GetRootFunctions();

   VertexIterator v_iter, v_iter_end;
   std::string res;
   std::string f_name = helper->get_function_name() + "_" + boost::lexical_cast<std::string>(function_id);

   /// entry and exit computation
   clean_start_nodes();
   ogc->add_operation(ENTRY, ENTRY, BB_ENTRY);
   bbgc->add_operation_to_bb(ogc->getIndex(ENTRY), BB_ENTRY);
   ogc->add_type(ENTRY, TYPE_ENTRY);

   ogc->add_operation(EXIT, EXIT, BB_EXIT);
   bbgc->add_operation_to_bb(ogc->getIndex(EXIT), BB_EXIT);
   ogc->add_type(EXIT, TYPE_EXIT);

   /// first cycle to compute the label_decl_map and the first_statement maps.
   for(boost::tie(v_iter, v_iter_end) = boost::vertices(*fbb); v_iter != v_iter_end; ++v_iter)
   {
      if(/**v_iter == fbb->CGetBBGraphInfo()->entry_vertex || */*v_iter == fbb->CGetBBGraphInfo()->exit_vertex)
         continue;
      const BBNodeInfoConstRef bb_node_info = fbb->CGetBBNodeInfo(*v_iter);
      const blocRef & b = bb_node_info->block;
      if(!b->list_of_stmt.empty())
      {
         tree_nodeRef front = b->list_of_stmt.front();
         if(GET_NODE(front)->get_kind() == gimple_label_K)
         {
            gimple_label* le = GetPointer<gimple_label>(GET_NODE(front));
            THROW_ASSERT(le->op, "Label expr without object");
            res = get_first_node(front, f_name);
            label_decl_map[GET_INDEX_NODE(le->op)] = res;
         }
         else if(b->list_of_phi.empty())
            res = get_first_node(front, f_name);
         else
            res = get_first_node(*b->list_of_phi.begin(), f_name);
      }
      else if(!b->list_of_phi.empty())
         res = get_first_node(*b->list_of_phi.begin(), f_name);
      else if(*v_iter == fbb->CGetBBGraphInfo()->entry_vertex)
      {
         res = ENTRY;
         first_statement[b->number] = res;
         continue;
      }
      else/*empty basic block*/
      {
         res = f_name + "_empty_" + boost::lexical_cast<std::string>(b->number);
         ogc->add_operation(res, NOP, b->number);
         bbgc->add_operation_to_bb(ogc->getIndex(res), b->number);
         ogc->add_type(res, TYPE_NOP);
      }
      first_statement[b->number] = res;
   }
   /// second cycle doing the real job
   tree_nodeRef last_instruction;
   for(boost::tie(v_iter, v_iter_end) = boost::vertices(*fbb); v_iter != v_iter_end; ++v_iter)
   {
      if(/* *v_iter == fbb->CGetBBGraphInfo()->entry_vertex || */*v_iter == fbb->CGetBBGraphInfo()->exit_vertex)
         continue;
      const BBNodeInfoConstRef bb_node_info = fbb->CGetBBNodeInfo(*v_iter);
      const blocRef & b = bb_node_info->block;
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Building operation of basic block BB" + boost::lexical_cast<std::string>(b->number));
      bool skip_first_stmt = false;
      if(!b->list_of_stmt.empty())
      {
         tree_nodeRef front = b->list_of_stmt.front();
         if(GET_NODE(front)->get_kind() == gimple_label_K)
         {
            actual_name = get_first_node(front, f_name);
            THROW_ASSERT(actual_name == first_statement[b->number], "the name of the first vertice has to be the same of the label expression vertex");
            if(!empty_start_nodes())
               connect_start_nodes(ogc, actual_name);
            init_start_nodes(actual_name);
            build_operation_recursive(TM, ogc, front, f_name, b->number);
            ogc->SetNodeId(actual_name, GET_INDEX_NODE(front));
            bbgc->add_operation_to_bb(ogc->getIndex(actual_name), b->number);
            skip_first_stmt = true;
         }
      }
      std::vector<tree_nodeRef>::iterator p_end = b->list_of_phi.end();
      for(std::vector<tree_nodeRef>::iterator p = b->list_of_phi.begin(); p != p_end; p++)
      {
         actual_name = get_first_node(*p, f_name);
         if(!empty_start_nodes())
            connect_start_nodes(ogc, actual_name);
         init_start_nodes(actual_name);
         build_operation_recursive(TM, ogc, *p, f_name, b->number);
         ogc->SetNodeId(actual_name, GET_INDEX_NODE(*p));
         bbgc->add_operation_to_bb(ogc->getIndex(actual_name), b->number);
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---List of operations size " + boost::lexical_cast<std::string>(b->list_of_stmt.size()));
      std::list<tree_nodeRef>::iterator s_end = b->list_of_stmt.end();
      std::list<tree_nodeRef>::iterator s = b->list_of_stmt.begin();
      if(skip_first_stmt)
         s++;
      for(; s != s_end; s++)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Analyzing operation " + boost::lexical_cast<std::string>(GET_INDEX_NODE(*s)));
         actual_name = get_first_node(*s, f_name);
         if(!empty_start_nodes())
            connect_start_nodes(ogc, actual_name);
         init_start_nodes(actual_name);
         build_operation_recursive(TM, ogc, *s, f_name, b->number);
         ogc->SetNodeId(actual_name, GET_INDEX_NODE(*s));
         bbgc->add_operation_to_bb(ogc->getIndex(actual_name), b->number);
      }
      clean_start_nodes();
      if(b->list_of_stmt.empty() )
      {
         if(!b->list_of_phi.empty())
            last_instruction = b->list_of_phi.back();
         else
            last_instruction = tree_nodeRef();
      }
      else if(b->list_of_stmt.size() == 1 && skip_first_stmt)
      {
         if(!b->list_of_phi.empty())
            last_instruction = b->list_of_phi.back();
         else
            last_instruction = b->list_of_stmt.back();
      }
      else
      {
         last_instruction = b->list_of_stmt.back();
      }
      if(last_instruction && GET_NODE(last_instruction)->get_kind() != gimple_switch_K)
      {
         init_start_nodes(get_first_node(last_instruction, f_name));
      }
      else if(!last_instruction)
      {
         init_start_nodes(first_statement[b->number]);
      }
      if(b->list_of_succ.size() == 0 and root_functions.find(function_id) != root_functions.end())
      {
         std::list<std::string>::iterator operation, operation_end = start_nodes.end();
         for(operation = start_nodes.begin(); operation != operation_end; operation++)
         {
            ogc->add_type(*operation, TYPE_LAST_OP);
         }
      }
      else
      {
         std::vector<unsigned int>::iterator su_end = b->list_of_succ.end();
         for(std::vector<unsigned int>::iterator su = b->list_of_succ.begin(); su != su_end; su++)
         {
            if((*su)== bloc::EXIT_BLOCK_ID)
            {
               connect_start_nodes(ogc, EXIT);
               if(root_functions.find(function_id) != root_functions.end())
               {
                  std::list<std::string>::iterator operation, operation_end = start_nodes.end();
                  for(operation = start_nodes.begin(); operation != operation_end; operation++)
                  {
                     ogc->add_type(*operation, TYPE_LAST_OP);
                  }
               }
            }
            else
            {
               if(*su == b->true_edge)
                  connect_start_nodes(ogc, first_statement[*su], true, false);
               else if(*su == b->false_edge)
                  connect_start_nodes(ogc, first_statement[*su], false, true);
               else if(last_instruction && GetPointer<gimple_goto>(GET_NODE(last_instruction)) && b->list_of_succ.size() > 1)
                  connect_start_nodes(ogc, first_statement[*su], true, true, *su);
               else
                  connect_start_nodes(ogc, first_statement[*su]);
            }
         }
      }
      clean_start_nodes();
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
   }
   label_decl_map.clear();
   first_statement.clear();

   if(parameters->getOption<bool>(OPT_print_dot))
   {
      function_behavior->CGetOpGraph(FunctionBehavior::CFG)->WriteDot("OP_CFG_Cleaned.dot");
   }
}


std::string operations_cfg_computation::get_first_node(const tree_nodeRef &tn, const std::string & f_name) const
{
   tree_nodeRef curr_tn;
   if (tn->get_kind() == tree_reindex_K)
   {
      curr_tn = GET_NODE(tn);
   }
   else
   {
      curr_tn = tn;
   }
   unsigned int ind = GET_INDEX_NODE(tn);
   std::string src, res = "";
   src = f_name + "_" + boost::lexical_cast<std::string>(ind);

   switch (curr_tn->get_kind())
   {
      case CASE_GIMPLE_NODES:
         return src;
      case case_label_expr_K:
      {
         case_label_expr* cle = GetPointer<case_label_expr>(curr_tn);
         ind = GET_INDEX_NODE(cle->got);
         THROW_ASSERT(label_decl_map.find(ind) != label_decl_map.end(), "Label " + boost::lexical_cast<std::string>(ind) + " doesn't exist");
         return label_decl_map.find(ind)->second;
      }
      case binfo_K:
      case block_K:
      case constructor_K:
      case call_expr_K:
      case identifier_node_K:
      case ssa_name_K:
      case statement_list_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_BINARY_EXPRESSION:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_FAKE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_QUATERNARY_EXPRESSION:
      case CASE_TERNARY_EXPRESSION:
      case CASE_TYPE_NODES:
      case CASE_UNARY_EXPRESSION:
      default:
         THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, std::string("Node not supported (") + boost::lexical_cast<std::string>(ind) + std::string("): ") + curr_tn->get_kind_text());
   }
   return "";
}


void operations_cfg_computation::insert_start_node(const std::string &start_node)
{
   start_nodes.push_back(start_node);
}

void operations_cfg_computation::clean_start_nodes()
{
   start_nodes.clear();
}

bool operations_cfg_computation::empty_start_nodes() const
{
   return start_nodes.empty();
}

void operations_cfg_computation::init_start_nodes(const std::string &start_node)
{
   start_nodes.clear();
   start_nodes.push_back(start_node);
}

void operations_cfg_computation::connect_start_nodes(const operations_graph_constructorRef ogc, const std::string & next, bool true_edge, bool false_edge, unsigned int nodeid)
{
   const std::unordered_set<unsigned int> root_functions = AppM->CGetCallGraphManager()->GetRootFunctions();
   std::string Start_node;
   std::list<std::string>::iterator s_end = start_nodes.end();
   for(std::list<std::string>::iterator s = start_nodes.begin(); s != s_end; s++)
   {
      Start_node = *s;
      ///Mark first operation of the application
      if(root_functions.find(function_id) != root_functions.end() and Start_node == ENTRY)
      {
         ogc->add_type(next, TYPE_FIRST_OP);
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Adding edge from " + Start_node + " to " + next);
      ogc->AddEdge(ogc->getIndex(Start_node), ogc->getIndex(next), CFG_SELECTOR);
      if(true_edge && false_edge)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Adding label " + boost::lexical_cast<std::string>(nodeid));
         ogc->add_edge_info(ogc->getIndex(Start_node), ogc->getIndex(next), CFG_SELECTOR, nodeid);
      }
      if(true_edge && !false_edge)
         ogc->add_edge_info(ogc->getIndex(Start_node), ogc->getIndex(next), CFG_SELECTOR, T_COND);
      if(false_edge && !true_edge)
         ogc->add_edge_info(ogc->getIndex(Start_node), ogc->getIndex(next), CFG_SELECTOR, F_COND);
   }
}

void operations_cfg_computation::build_operation_recursive(const tree_managerRef TM, const operations_graph_constructorRef ogc, const tree_nodeRef tn, const std::string &f_name, unsigned int bb_index)
{
   const tree_nodeRef &curr_tn = GET_NODE(tn);
   unsigned int ind = GET_INDEX_NODE(tn);
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Building CFG of node " + boost::lexical_cast<std::string>(ind) + " of type " + curr_tn->get_kind_text());
   switch (curr_tn->get_kind())
   {
      case gimple_return_K:
      {
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         ogc->add_type(actual_name, TYPE_RET);
         break;
      }
      case gimple_assign_K:
      {
         gimple_assign* me = GetPointer<gimple_assign>(curr_tn);

         tree_nodeRef op0 = GET_NODE(me->op0);
         tree_nodeRef op1 = GET_NODE(me->op1);
         tree_nodeRef op0_type = tree_helper::get_type_node(op0);
         tree_nodeRef op1_type = tree_helper::get_type_node(op1);
         const std::set<unsigned int>& fun_mem_data = function_behavior->get_function_mem();
         bool is_a_vector_bitfield = false;
         /// check for bit field ref of vector type
         if(op1->get_kind() == bit_field_ref_K)
         {
            bit_field_ref* bfr = GetPointer<bit_field_ref>(op1);
            if(tree_helper::is_a_vector(TM, GET_INDEX_NODE(bfr->op0)))
            is_a_vector_bitfield = true;
         }

         bool load_candidate = (op1->get_kind() == bit_field_ref_K && !is_a_vector_bitfield) || op1->get_kind() == component_ref_K || op1->get_kind() == indirect_ref_K || op1->get_kind() == misaligned_indirect_ref_K || op1->get_kind() == mem_ref_K || op1->get_kind() == array_ref_K || op1->get_kind() == target_mem_ref_K || op1->get_kind() == target_mem_ref461_K || op1->get_kind() == realpart_expr_K || op1->get_kind() == imagpart_expr_K;
         bool store_candidate = op0->get_kind() == bit_field_ref_K || op0->get_kind() == component_ref_K || op0->get_kind() == indirect_ref_K || op0->get_kind() == misaligned_indirect_ref_K || op0->get_kind() == mem_ref_K || op0->get_kind() == array_ref_K || op0->get_kind() == target_mem_ref_K || op0->get_kind() == target_mem_ref461_K || op0->get_kind() == realpart_expr_K || op0->get_kind() == imagpart_expr_K;


         if(op1->get_kind() == view_convert_expr_K)
         {
            view_convert_expr* vc = GetPointer<view_convert_expr>(op1);
            tree_nodeRef vc_op_type = tree_helper::get_type_node(GET_NODE(vc->op));
            if(op0_type->get_kind() == record_type_K || op0_type->get_kind() == union_type_K)
               store_candidate = true;
            if(vc_op_type->get_kind() == record_type_K || vc_op_type->get_kind() == union_type_K)
               load_candidate = true;

            if(vc_op_type->get_kind() == array_type_K && op0_type->get_kind() == vector_type_K)
               load_candidate = true;
            if(vc_op_type->get_kind() == vector_type_K && op0_type->get_kind() == array_type_K)
               store_candidate = true;
            /*if (GetPointer<ssa_name>(GET_NODE(vc->op)))
            {
               ssa_name* sn = GetPointer<ssa_name>(GET_NODE(vc->op));
               parm_decl *pd = GetPointer<parm_decl>(GET_NODE(sn->var));
               if(0 && pd)
               {
                  load_candidate |= fun_mem_data.find(GET_INDEX_NODE(sn->var)) != fun_mem_data.end();
               }
               else if(op0_type->get_kind()== record_type_K || op0_type->get_kind()== union_type_K)
               {
                  store_candidate = true;
               }
            }
            else if(GetPointer<var_decl>(GET_NODE(vc->op)))
            {
               load_candidate |= fun_mem_data.find(GET_INDEX_NODE(vc->op)) != fun_mem_data.end();
            }
            else
               THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "build_operation_recursive view_convert_expr currently not supported: " + GET_NODE(vc->op)->get_kind_text()+" @"+STR(ind));*/
         }

         if(not me->clobber and not tree_helper::is_a_vector(TM, op0->index) and ((((tree_helper::is_an_array(TM, op0->index) and not tree_helper::is_a_pointer(TM, op0->index))) or op1->get_kind() == constructor_K)))
         {
            if(not tree_helper::is_an_array(TM, op0->index) or (((op1->get_kind() == constructor_K or (op1->get_kind() == var_decl_K and GetPointer<const var_decl>(op1)->init) or op1->get_kind() == string_cst_K)) and (GetPointer<const decl_node>(op0) or op0->get_kind() == ssa_name_K)))
            {
               function_behavior->GetBehavioralHelper()->add_initialization(op0->index, op1->index);
               ogc->add_type(actual_name, TYPE_INIT);
            }
         }

         if(me->init_assignment || me->clobber)
         {
            ogc->add_operation(actual_name, NOP, bb_index);
            ogc->add_type(actual_name, TYPE_NOP);
         }
         else if(op1->get_kind() == float_expr_K)
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, " - set as float_expr_xx_to_xxx operation");
            float_expr* fe = GetPointer<float_expr>(op1);
            unsigned int size_dest = tree_helper::size(TM, GET_INDEX_NODE(fe->type));
            unsigned int size_from = tree_helper::size(TM, GET_INDEX_NODE(fe->op));
            if(size_from < 32)
               size_from = 32;
            ogc->add_operation(actual_name, FLOAT_EXPR + STR("_") + STR(size_from) + "_to_" + STR(size_dest), bb_index);
            ogc->add_type(actual_name, TYPE_GENERIC);
         }
         else if(op1->get_kind() == fix_trunc_expr_K)
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, " - set as fix_trunc_expr_xx_to_xxx operation");
            fix_trunc_expr* fte = GetPointer<fix_trunc_expr>(op1);
            unsigned int size_dest = tree_helper::size(TM, GET_INDEX_NODE(fte->type));
            bool is_unsigned = tree_helper::is_unsigned(TM, GET_INDEX_NODE(fte->type));
            unsigned int size_from = tree_helper::size(TM, GET_INDEX_NODE(fte->op));
            ogc->add_operation(actual_name, FIX_TRUNC_EXPR + STR("_") + STR(size_from) + "_to_" + (is_unsigned ? "u" : "") +STR(size_dest), bb_index);
            ogc->add_type(actual_name, TYPE_GENERIC);
         }
         else if(tree_helper::is_a_vector(TM, GET_INDEX_NODE(me->op0)) && op1->get_kind() == constructor_K)
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, " - set as VECTOR CONCATENATION operation");
            constructor* co = GetPointer<constructor>(op1);
            unsigned int size_obj = tree_helper::size(TM, GET_INDEX_NODE(co->type));
            unsigned int n_byte =size_obj/8;
            ogc->add_operation(actual_name, VECT_CONCATENATION + STR("_") + STR(n_byte) + "_" + STR(co->list_of_idx_valu.size()), bb_index);
            ogc->add_type(actual_name, TYPE_GENERIC);
         }
         else if(op1->get_kind() == complex_expr_K)
         {
            ogc->add_operation(actual_name, op1->get_kind_text(), bb_index);
            ogc->add_type(actual_name, TYPE_GENERIC);
         }
         else if(op0_type && op1_type &&
                ((op0_type->get_kind()== record_type_K && op1_type->get_kind()== record_type_K && op1->get_kind() != view_convert_expr_K) ||
                (op0_type->get_kind()== union_type_K && op1_type->get_kind()== union_type_K && op1->get_kind() != view_convert_expr_K) ||
                (op0_type->get_kind() == array_type_K) ||
                (fun_mem_data.find(GET_INDEX_NODE(me->op0)) != fun_mem_data.end() && fun_mem_data.find(GET_INDEX_NODE(me->op1)) != fun_mem_data.end()) ||
                (fun_mem_data.find(GET_INDEX_NODE(me->op0)) != fun_mem_data.end() && load_candidate) ||
                (store_candidate && fun_mem_data.find(GET_INDEX_NODE(me->op1)) != fun_mem_data.end())
                )
               )
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---set as MEMCPY operation");
            ogc->add_operation(actual_name, MEMCPY, bb_index);
            ogc->add_type(actual_name, TYPE_MEMCPY);
         }
         else if (store_candidate || fun_mem_data.find(GET_INDEX_NODE(me->op0)) != fun_mem_data.end())
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---set as STORE operation");
            ogc->add_operation(actual_name, STORE, bb_index);
            ogc->add_type(actual_name, TYPE_STORE);
         }
         else if (load_candidate || fun_mem_data.find(GET_INDEX_NODE(me->op1)) != fun_mem_data.end())
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---set as LOAD operation");
            ogc->add_operation(actual_name, LOAD, bb_index);
            ogc->add_type(actual_name, TYPE_LOAD);
         }
         else
         {
            ogc->add_operation(actual_name, ASSIGN, bb_index);
            ogc->add_type(actual_name, TYPE_ASSIGN);
            if(me->orig)
               ogc->add_type(actual_name, TYPE_WAS_GIMPLE_PHI);
            build_operation_recursive(TM, ogc, me->op1, f_name, bb_index);
         }
         break;
      }
      case gimple_nop_K:
      {
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         ogc->add_type(actual_name, TYPE_GENERIC);
         break;
      }
      case call_expr_K:
      {
         call_expr* ce = GetPointer<call_expr>(curr_tn);
         tree_nodeRef temp_node = GET_NODE(ce->fn);
         function_decl * fd= nullptr;

         if(temp_node->get_kind() == addr_expr_K)
         {
            unary_expr* ue = GetPointer<unary_expr>(temp_node);
            temp_node = ue->op;
            fd = GetPointer<function_decl>(GET_NODE(temp_node));
         }
         else if(temp_node->get_kind() == obj_type_ref_K)
         {
            temp_node = tree_helper::find_obj_type_ref_function(ce->fn);
            fd = GetPointer<function_decl>(GET_NODE(temp_node));
         }
         if(fd)
         {
            std::string fun_name = tree_helper::print_function_name(TM, fd);
            fun_name = tree_helper::normalized_ID(fun_name);
            //const std::string builtin_prefix("__builtin_");
            //if(fun_name.find(builtin_prefix) == 0)
            //   fun_name = fun_name.substr(builtin_prefix.size());
            const unsigned int call_id = GET_INDEX_NODE(temp_node);
            //Creating node of call
            ogc->add_operation(actual_name, fun_name, bb_index);
            ogc->add_type(actual_name, TYPE_EXTERNAL);
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---set as TYPE_EXTERNAL operation");
            if(fun_name == "exit" || fun_name == "abort" || fun_name == "__builtin_exit" || fun_name == "__builtin_abort")
               ogc->add_type(actual_name, TYPE_LAST_OP);
            ogc->add_called_function(actual_name, call_id);
         }
         else
         {
            //Call of not an usual function decl (for example array_ref of an array of function pointer)
            //This call is needed to set the basic block index of this operation. Otherwise
            //vertex is created but basic block index is not setted
            ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         }
         break;
      }
      case gimple_call_K:
      {
         gimple_call* ce = GetPointer<gimple_call>(curr_tn);
         tree_nodeRef temp_node = GET_NODE(ce->fn);
         function_decl * fd= nullptr;

         if(temp_node->get_kind() == addr_expr_K)
         {
            unary_expr* ue = GetPointer<unary_expr>(temp_node);
            temp_node = ue->op;
            fd = GetPointer<function_decl>(GET_NODE(temp_node));
         }
         else if(temp_node->get_kind() == obj_type_ref_K)
         {
            temp_node = tree_helper::find_obj_type_ref_function(ce->fn);
            fd = GetPointer<function_decl>(GET_NODE(temp_node));
         }
         if(fd)
         {
            if(tree_helper::is_a_nop_function_decl(fd))
            {
               ogc->add_operation(actual_name, NOP, bb_index);
               ogc->add_type(actual_name, TYPE_NOP);
            }
            else
            {
               std::string fun_name = tree_helper::print_function_name(TM, fd);
               fun_name = tree_helper::normalized_ID(fun_name);
               //const std::string builtin_prefix("__builtin_");
               //if(fun_name.find(builtin_prefix) == 0)
               //   fun_name = fun_name.substr(builtin_prefix.size());
               const unsigned int call_id = GET_INDEX_NODE(temp_node);
               //Creating node of call
               ogc->add_operation(actual_name, fun_name, bb_index);
               ogc->add_type(actual_name, TYPE_EXTERNAL);
               if(fun_name == "exit" || fun_name == "abort" || fun_name == "__builtin_exit" || fun_name == "__builtin_abort")
                  ogc->add_type(actual_name, TYPE_LAST_OP);
               ogc->add_called_function(actual_name, call_id);
            }
         }
         else
         {
            //Call of not an usual function decl (for example array_ref of an array of function pointer)
            //This call is needed to set the basic block index of this operation. Otherwise
            //vertex is created but basic block index is not setted
            ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         }
         break;
      }
      case gimple_goto_K:
      {
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         ogc->add_type(actual_name, TYPE_GOTO);
         break;
      }
      case gimple_label_K:
      {
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         gimple_label *le = GetPointer<gimple_label>(curr_tn);
         THROW_ASSERT(le->op, "expected a gimple_label operand");
         label_decl * ld = GetPointer<label_decl>(GET_NODE(le->op));
         if(ld->artificial_flag)
            ogc->add_type(actual_name, TYPE_NOP);
         else
            ogc->add_type(actual_name, TYPE_LABEL);
         break;
      }
      case gimple_cond_K:
      {
         gimple_cond* gc = GetPointer<gimple_cond>(curr_tn);
         ogc->add_operation(actual_name, READ_COND, bb_index);
         build_operation_recursive(TM, ogc, gc->op0, f_name, bb_index);
         ogc->add_type(actual_name, TYPE_IF);
         break;
      }
      case gimple_multi_way_if_K:
      {
         ogc->add_operation(actual_name, MULTI_READ_COND, bb_index);
         ogc->add_type(actual_name, TYPE_MULTIIF);
         break;
      }
      case gimple_while_K:
      {
         gimple_while * we = GetPointer<gimple_while>(curr_tn);
         ogc->add_operation(actual_name, READ_COND, bb_index);
         build_operation_recursive(TM, ogc, we->op0, f_name, bb_index);
         ogc->add_type(actual_name, TYPE_WHILE);
         break;
      }
      case gimple_for_K:
      {
         ogc->add_operation(actual_name, READ_COND, bb_index);
         ogc->add_type(actual_name, TYPE_FOR);
         break;
      }
      case gimple_switch_K:
      {
         gimple_switch* se = GetPointer<gimple_switch>(curr_tn);

         ogc->add_operation(actual_name, SWITCH_COND, bb_index);
         build_operation_recursive(TM, ogc, se->op0, actual_name, bb_index);
         ogc->add_type(actual_name, TYPE_SWITCH);

         tree_nodeRef case_label_exprs;
         if(se->op1)
            case_label_exprs = GET_NODE(se->op1);
         else
            THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, std::string("op2 in gimple_switch not yet supported (") + boost::lexical_cast<std::string>(ind) + std::string(")"));
         if(case_label_exprs->get_kind() == tree_vec_K)
         {
            tree_vec* tv = GetPointer<tree_vec>(case_label_exprs);
            std::vector<tree_nodeRef>::iterator end = tv->list_of_op.end();
            for(std::vector<tree_nodeRef>::iterator i = tv->list_of_op.begin(); i != end; i++)
            {
               std::string res = get_first_node(*i, f_name);
               THROW_ASSERT(res != "", "Impossible to find first operation of case " + boost::lexical_cast<std::string>(GET_INDEX_NODE(*i)));
               if(GET_NODE(*i)->get_kind() == case_label_expr_K)
               {
                  case_label_expr * cl = GetPointer<case_label_expr>(GET_NODE(*i));
                  if(cl->default_flag)
                     connect_start_nodes(ogc, res, true, true, default_COND);
                  else
                     connect_start_nodes(ogc, res, true, true, GET_INDEX_NODE(*i));
               }
               else
                  connect_start_nodes(ogc, res, true, true, GET_INDEX_NODE(*i));
            }
         }
         else
            THROW_ERROR(std::string("expected tree_vec in op1 in gimple_switch (") + boost::lexical_cast<std::string>(ind) + std::string(")"));
         break;
      }
      case CASE_UNARY_EXPRESSION:
      {
         const unary_expr * ue = GetPointer<unary_expr>(curr_tn);
         build_operation_recursive(TM, ogc, ue->op, f_name, bb_index);
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         ogc->add_type(actual_name, TYPE_GENERIC);
         break;
      }
      case CASE_BINARY_EXPRESSION:
      {
         const binary_expr * be = GetPointer<binary_expr>(curr_tn);
         build_operation_recursive(TM, ogc, be->op0, f_name, bb_index);
         build_operation_recursive(TM, ogc, be->op1, f_name, bb_index);
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         ogc->add_type(actual_name, TYPE_GENERIC);
         break;
      }
      case CASE_TERNARY_EXPRESSION:
      {
         const ternary_expr * te = GetPointer<ternary_expr>(curr_tn);
         build_operation_recursive(TM, ogc, te->op0, f_name, bb_index);
         if(te->op1)
            build_operation_recursive(TM, ogc, te->op1, f_name, bb_index);
         if(te->op2)
            build_operation_recursive(TM, ogc, te->op2, f_name, bb_index);
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         ogc->add_type(actual_name, TYPE_GENERIC);
         break;
      }
      case CASE_QUATERNARY_EXPRESSION:
      {
         const quaternary_expr * qe = GetPointer<quaternary_expr>(curr_tn);
         build_operation_recursive(TM, ogc, qe->op0, f_name, bb_index);
         if(qe->op1)
            build_operation_recursive(TM, ogc, qe->op1, f_name, bb_index);
         if(qe->op2)
            build_operation_recursive(TM, ogc, qe->op2, f_name, bb_index);
         if(qe->op3)
            build_operation_recursive(TM, ogc, qe->op3, f_name, bb_index);
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         ogc->add_type(actual_name, TYPE_GENERIC);
         break;
      }
      case ssa_name_K:
      case integer_cst_K:
      case real_cst_K:
      case string_cst_K:
      case vector_cst_K:
      case complex_cst_K:
      case constructor_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case CASE_DECL_NODES:
      case gimple_pragma_K:
      {
         break;
      }
      case gimple_asm_K:
      {
         gimple_asm* ae = GetPointer<gimple_asm>(curr_tn);
         if(!ae->volatile_flag && (ae->in && ae->out))
            ogc->add_type(actual_name, TYPE_GENERIC);
         else
            ogc->add_type(actual_name, TYPE_GENERIC|TYPE_OPAQUE);
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         break;
      }
      case gimple_phi_K:
      {
         gimple_phi * phi = GetPointer<gimple_phi>(curr_tn);
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         ogc->add_type(actual_name, phi->virtual_flag ? TYPE_VPHI : TYPE_PHI);
         break;
      }
      case CASE_CPP_NODES:

      {
         ogc->add_operation(actual_name, curr_tn->get_kind_text(), bb_index);
         ogc->add_type(actual_name, TYPE_GENERIC);
         break;
      }
      case binfo_K:
      case block_K:
      case case_label_expr_K:
      case gimple_bind_K:
      case gimple_predict_K:
      case gimple_resx_K:
      case identifier_node_K:
      case statement_list_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_FAKE_NODES:
      case CASE_PRAGMA_NODES:
      case CASE_TYPE_NODES:
      default:
         {
            THROW_UNREACHABLE("");
         }
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Built CFG of node " + boost::lexical_cast<std::string>(ind));
}

