/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file multi_way_if.cpp
 * @brief Analysis step rebuilding multi-way if.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "multi_way_if.hpp"

///Behavior include
#include "application_manager.hpp"
#include "call_graph.hpp"
#include "call_graph_manager.hpp"

///Parameter include
#include "Parameter.hpp"


///STD include
#include <fstream>

///STL include
#include <unordered_set>

///tree includes
#include "tree_manager.hpp"
#include "ext_tree_node.hpp"
#include "tree_reindex.hpp"
#include "tree_basic_block.hpp"
#include "tree_helper.hpp"

multi_way_if::multi_way_if(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, MULTI_WAY_IF, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

multi_way_if::~multi_way_if()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > multi_way_if::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SWITCH_FIX, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BLOCK_FIX, SAME_FUNCTION));
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(CHECK_SYSTEM_TYPE, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      {
         break;
      }
      case(PRECEDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(PHI_OPT, SAME_FUNCTION));
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void multi_way_if::update_cfg(unsigned int succ, unsigned int curr_bb, unsigned int pred, std::map<unsigned int, blocRef>& list_of_bloc)
{
   if(list_of_bloc[succ]->list_of_pred.begin() != list_of_bloc[succ]->list_of_pred.end())
   {
      while(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), curr_bb) != list_of_bloc[succ]->list_of_pred.end())
         list_of_bloc[succ]->list_of_pred.erase(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), curr_bb));
   }
   if(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), pred) == list_of_bloc[succ]->list_of_pred.end())
      list_of_bloc[succ]->list_of_pred.push_back(pred);
   if(std::find(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end(), succ) == list_of_bloc[pred]->list_of_succ.end())
      list_of_bloc[pred]->list_of_succ.push_back(succ);
   std::vector<tree_nodeRef> & list_of_phi_succ = list_of_bloc[succ]->list_of_phi;
   std::vector<tree_nodeRef>::iterator phi, phi_end = list_of_phi_succ.end();
   for(phi = list_of_phi_succ.begin(); phi != phi_end; phi++)
   {
      gimple_phi * current_phi = GetPointer<gimple_phi>(GET_NODE(*phi));
      std::vector<std::pair< tree_nodeRef, unsigned int> > & list_of_def_edge = current_phi->list_of_def_edge;
      std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator definition, definition_end = list_of_def_edge.end();
      for(definition = list_of_def_edge.begin(); definition != definition_end; definition++)
      {
         if(definition->second == curr_bb)
            definition->second = pred;
      }
   }
}



void multi_way_if::Exec()
{
   PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "START multi-way if rebuilding");
   const tree_managerRef TM = AppM->get_tree_manager();
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }
   tree_nodeRef temp = TM->get_tree_node_const(function_id);
   function_decl * fd = GetPointer<function_decl>(temp);
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));

   std::map<unsigned int, blocRef> & list_of_bloc = sl->list_of_bloc;
   std::map<unsigned int, blocRef>::iterator it, it_end = list_of_bloc.end();
   std::set<unsigned int> bb_to_be_removed;

   for(it = list_of_bloc.begin(); it != it_end; it++)
   {
      unsigned int curr_bb = it->first;
      if(curr_bb == bloc::ENTRY_BLOCK_ID || curr_bb == bloc::EXIT_BLOCK_ID)
         continue;
      if(it->second->list_of_pred.size() > 1)
         continue;
      unsigned int pred = it->second->list_of_pred.front();
      if(pred == bloc::ENTRY_BLOCK_ID) continue;
      if(list_of_bloc[pred]->list_of_stmt.empty()) continue;
      tree_nodeRef last_pred_stmt = GET_NODE(*(list_of_bloc[pred]->list_of_stmt.rbegin()));
      if(last_pred_stmt->get_kind() != gimple_cond_K /*&& last_pred_stmt->get_kind() != gimple_multi_way_if_K*/) continue;
      if(list_of_bloc[curr_bb]->list_of_stmt.size()!= 1) continue;
      tree_nodeRef last_curr_stmt = GET_NODE(*(list_of_bloc[curr_bb]->list_of_stmt.rbegin()));
      if(last_curr_stmt->get_kind() != gimple_cond_K /*&& last_curr_stmt->get_kind() != gimple_multi_way_if_K*/) continue;
      if(list_of_bloc[curr_bb]->list_of_phi.size()!= 0) continue;
      ///check for short circuit conditions
#if 1
      std::set<unsigned int> succ_set;
      succ_set.insert(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end());
      succ_set.insert(list_of_bloc[curr_bb]->list_of_succ.begin(), list_of_bloc[curr_bb]->list_of_succ.end());
      if(succ_set.size() != (list_of_bloc[pred]->list_of_succ.size()+list_of_bloc[curr_bb]->list_of_succ.size())) continue;
#endif
      /// now the merging starts
      create_gimple_multi_way_if(pred, curr_bb, list_of_bloc);
      list_of_bloc[pred]->false_edge = 0;
      list_of_bloc[pred]->true_edge = 0;
      ///cfg fixing
      if(std::find(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end(), curr_bb) != list_of_bloc[pred]->list_of_succ.end())
         list_of_bloc[pred]->list_of_succ.erase(std::find(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end(), curr_bb));
      /// update list of succ of pred and list of pred of succ
      std::vector<unsigned int>::const_iterator los_it_end = list_of_bloc[curr_bb]->list_of_succ.end();
      for(std::vector<unsigned int>::const_iterator los_it = list_of_bloc[curr_bb]->list_of_succ.begin(); los_it != los_it_end; ++los_it)
      {
         update_cfg(*los_it, curr_bb, pred, list_of_bloc);
      }
      bb_to_be_removed.insert(curr_bb);

   }
   std::set<unsigned int>::iterator it_tbr, it_tbr_end = bb_to_be_removed.end();
   for(it_tbr=bb_to_be_removed.begin(); it_tbr != it_tbr_end; ++it_tbr)
   {
      list_of_bloc.erase(*it_tbr);
   }
   bb_to_be_removed.clear();

   if(debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
   {
      PrintTreeManager(false);
   }
}

void multi_way_if::extract_condition(const tree_managerRef TM, unsigned int &ssa1_node_nid, std::list<tree_nodeRef>& list_of_stmt_cond1, gimple_cond* ce1, unsigned int bb1, unsigned int type_index)
{
   if(GET_NODE(ce1->op0)->get_kind() == ssa_name_K)
      ssa1_node_nid = GET_INDEX_NODE(ce1->op0);
   else
   {
      /// create the ssa_var representing the condition for bb1
      unsigned int ssa1_vers = TM->get_next_vers();
      ssa1_node_nid = TM->new_tree_node_id();
      std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
      IR_schema[TOK(TOK_TYPE)] = STR(type_index);
      IR_schema[TOK(TOK_VERS)] = STR(ssa1_vers);
      IR_schema[TOK(TOK_VOLATILE)] = STR(false);
      IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
      TM->create_tree_node(ssa1_node_nid, ssa_name_K, IR_schema);
      IR_schema.clear();
      tree_nodeRef ssa1_cond_node = TM->GetTreeReindex(ssa1_node_nid);

      ///create the assignment between condition for bb1 and the new ssa var
      unsigned int cond1_index = GET_INDEX_NODE(ce1->op0);
      unsigned int cond1_gimple_stmt_id = TM->new_tree_node_id();
      IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
      IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa1_node_nid);
      IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(cond1_index);
      TM->create_tree_node(cond1_gimple_stmt_id, gimple_assign_K, IR_schema);
      IR_schema.clear();
      tree_nodeRef cond1_created_stmt = TM->GetTreeReindex(cond1_gimple_stmt_id);
      GetPointer<ssa_name>(GET_NODE(ssa1_cond_node))->add_def(cond1_created_stmt);
      /// and then add to the bb1 statement list
      GetPointer<gimple_node>(GET_NODE(cond1_created_stmt))->bb_index = bb1;
      list_of_stmt_cond1.push_back(cond1_created_stmt);
   }
}

void multi_way_if::compute_not(unsigned int &res_ssa_id, unsigned int type_index, unsigned int ssa1_node_nid, const tree_managerRef TM, std::list<tree_nodeRef>& list_of_stmt, unsigned int bb_index)
{
   unsigned int neg_ssa_vers = TM->get_next_vers();
   res_ssa_id = TM->new_tree_node_id();
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   IR_schema[TOK(TOK_TYPE)] = STR(type_index);
   IR_schema[TOK(TOK_VERS)] = STR(neg_ssa_vers);
   IR_schema[TOK(TOK_VOLATILE)] = STR(false);
   IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
   TM->create_tree_node(res_ssa_id, ssa_name_K, IR_schema);
   IR_schema.clear();
   tree_nodeRef neg_ssa_node = TM->GetTreeReindex(res_ssa_id);

   IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(type_index);
   IR_schema[TOK(TOK_OP)] = boost::lexical_cast<std::string>(ssa1_node_nid);
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   unsigned int neg_expr_index = TM->new_tree_node_id();
   TM->create_tree_node(neg_expr_index, truth_not_expr_K, IR_schema);

   unsigned int neg_stmt_id = TM->new_tree_node_id();
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(res_ssa_id);
   IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(neg_expr_index);
   TM->create_tree_node(neg_stmt_id, gimple_assign_K, IR_schema);
   IR_schema.clear();
   tree_nodeRef neg_stmt = TM->GetTreeReindex(neg_stmt_id);
   GetPointer<ssa_name>(GET_NODE(neg_ssa_node))->add_def(neg_stmt);
   /// and then add to the bb1 statement list
   GetPointer<gimple_node>(GET_NODE(neg_stmt))->bb_index = bb_index;
   list_of_stmt.push_back(neg_stmt);
}
void multi_way_if::compute_and(unsigned int &ssa_res_node_nid, unsigned int type_index, unsigned int ssa1_node_nid, unsigned int ssa2_node_nid, const tree_managerRef TM, std::list<tree_nodeRef>& list_of_stmt, unsigned int bb_index, bool second_negated)
{
   /// create the ssa_var representing the and between conditions
   unsigned int ssa_vers = TM->get_next_vers();
   ssa_res_node_nid = TM->new_tree_node_id();
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   IR_schema[TOK(TOK_TYPE)] = STR(type_index);
   IR_schema[TOK(TOK_VERS)] = STR(ssa_vers);
   IR_schema[TOK(TOK_VOLATILE)] = STR(false);
   IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
   TM->create_tree_node(ssa_res_node_nid, ssa_name_K, IR_schema);
   IR_schema.clear();
   tree_nodeRef ssa_res_cond_node = TM->GetTreeReindex(ssa_res_node_nid);

   unsigned int second_operand = ssa2_node_nid;
   if(second_negated)
   {
      unsigned int neg_ssa_vers = TM->get_next_vers();
      second_operand = TM->new_tree_node_id();
      IR_schema[TOK(TOK_TYPE)] = STR(type_index);
      IR_schema[TOK(TOK_VERS)] = STR(neg_ssa_vers);
      IR_schema[TOK(TOK_VOLATILE)] = STR(false);
      IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
      TM->create_tree_node(second_operand, ssa_name_K, IR_schema);
      IR_schema.clear();
      tree_nodeRef neg_ssa_node = TM->GetTreeReindex(second_operand);

      IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(type_index);
      IR_schema[TOK(TOK_OP)] = boost::lexical_cast<std::string>(ssa2_node_nid);
      IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
      unsigned int neg_expr_index = TM->new_tree_node_id();
      TM->create_tree_node(neg_expr_index, truth_not_expr_K, IR_schema);

      unsigned int neg_stmt_id = TM->new_tree_node_id();
      IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
      IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(second_operand);
      IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(neg_expr_index);
      TM->create_tree_node(neg_stmt_id, gimple_assign_K, IR_schema);
      IR_schema.clear();
      tree_nodeRef neg_stmt = TM->GetTreeReindex(neg_stmt_id);
      GetPointer<ssa_name>(GET_NODE(neg_ssa_node))->add_def(neg_stmt);
      /// and then add to the bb1 statement list
      GetPointer<gimple_node>(GET_NODE(neg_stmt))->bb_index = bb_index;
      list_of_stmt.push_back(neg_stmt);
   }

   IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(type_index);
   IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa1_node_nid);
   IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(second_operand);
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   unsigned int expr_index = TM->new_tree_node_id();
   TM->create_tree_node(expr_index, truth_and_expr_K, IR_schema);

   ///create the assignment between condition for bb1 and the new ssa var
   unsigned int cond_gimple_stmt_id = TM->new_tree_node_id();
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa_res_node_nid);
   IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(expr_index);
   TM->create_tree_node(cond_gimple_stmt_id, gimple_assign_K, IR_schema);
   IR_schema.clear();
   tree_nodeRef cond_created_stmt = TM->GetTreeReindex(cond_gimple_stmt_id);
   GetPointer<ssa_name>(GET_NODE(ssa_res_cond_node))->add_def(cond_created_stmt);
   /// and then add to the bb1 statement list
   GetPointer<gimple_node>(GET_NODE(cond_created_stmt))->bb_index = bb_index;
   list_of_stmt.push_back(cond_created_stmt);
}

void multi_way_if::create_gimple_multi_way_if(unsigned int pred, unsigned int curr_bb, std::map<unsigned int, blocRef> & list_of_bloc)
{
   const tree_managerRef TM = AppM->get_tree_manager();
   ///identify the first gimple_cond
   std::list<tree_nodeRef> & list_of_stmt_cond1 = list_of_bloc[pred]->list_of_stmt;
   THROW_ASSERT(GET_NODE(list_of_stmt_cond1.back())->get_kind() == gimple_cond_K, "a gimple_cond is expected");
   tree_nodeRef cond1_statement = list_of_stmt_cond1.back();
   list_of_stmt_cond1.erase(std::find(list_of_stmt_cond1.begin(), list_of_stmt_cond1.end(), cond1_statement));
   gimple_cond* ce1 = GetPointer<gimple_cond>(GET_NODE(cond1_statement));
   unsigned int ssa1_node_nid;
   unsigned int type_index;
   tree_helper::get_type_node(GET_NODE(ce1->op0), type_index);
   extract_condition(TM, ssa1_node_nid, list_of_stmt_cond1, ce1, pred, type_index);

   ///identify the second gimple_cond
   std::list<tree_nodeRef> & list_of_stmt_cond2 = list_of_bloc[curr_bb]->list_of_stmt;
   THROW_ASSERT(GET_NODE(list_of_stmt_cond2.back())->get_kind() == gimple_cond_K, "a gimple_cond is expected");
   tree_nodeRef cond2_statement = list_of_stmt_cond2.back();
   list_of_stmt_cond2.erase(std::find(list_of_stmt_cond2.begin(), list_of_stmt_cond2.end(), cond2_statement));
   gimple_cond* ce2 = GetPointer<gimple_cond>(GET_NODE(cond2_statement));
   unsigned int ssa2_node_nid;
   extract_condition(TM, ssa2_node_nid, list_of_stmt_cond1, ce2, pred, type_index);

   /// create the gimple_multi_way_if node
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
   unsigned int gimple_multi_way_if_id = TM->new_tree_node_id();
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   TM->create_tree_node(gimple_multi_way_if_id, gimple_multi_way_if_K, IR_schema);
   IR_schema.clear();
   tree_nodeRef gimple_multi_way_if_stmt = TM->GetTreeReindex(gimple_multi_way_if_id);
   GetPointer<gimple_node>(GET_NODE(gimple_multi_way_if_stmt))->bb_index = pred;
   gimple_multi_way_if* gmwi = GetPointer<gimple_multi_way_if>(GET_NODE(gimple_multi_way_if_stmt));
   if(list_of_bloc[pred]->false_edge == curr_bb)
   {
      std::pair< tree_nodeRef, unsigned int> cond1(TM->GetTreeReindex(ssa1_node_nid), list_of_bloc[pred]->true_edge);
      gmwi->list_of_cond.push_back(cond1);
      unsigned int res_and;
      compute_and(res_and, type_index, ssa2_node_nid, ssa1_node_nid, TM, list_of_stmt_cond1, pred, true);
      std::pair< tree_nodeRef, unsigned int> cond2(TM->GetTreeReindex(res_and), list_of_bloc[curr_bb]->true_edge);
      gmwi->list_of_cond.push_back(cond2);
      std::pair< tree_nodeRef, unsigned int> cond3(tree_nodeRef(), list_of_bloc[curr_bb]->false_edge);
      gmwi->list_of_cond.push_back(cond3);
   }
   else
   {
      unsigned int res_not;
      compute_not(res_not, type_index, ssa1_node_nid, TM, list_of_stmt_cond1, pred);
      std::pair< tree_nodeRef, unsigned int> cond1(TM->GetTreeReindex(res_not), list_of_bloc[pred]->false_edge);
      gmwi->list_of_cond.push_back(cond1);
      unsigned int res_and2;
      compute_and(res_and2, type_index, ssa1_node_nid, ssa2_node_nid, TM, list_of_stmt_cond1, pred, false);
      std::pair< tree_nodeRef, unsigned int> cond2(TM->GetTreeReindex(res_and2), list_of_bloc[curr_bb]->true_edge);
      gmwi->list_of_cond.push_back(cond2);

      std::pair< tree_nodeRef, unsigned int> cond3(tree_nodeRef(), list_of_bloc[curr_bb]->false_edge);
      gmwi->list_of_cond.push_back(cond3);
   }
   list_of_stmt_cond1.push_back(gimple_multi_way_if_stmt);
}
