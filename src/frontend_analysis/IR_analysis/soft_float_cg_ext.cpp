/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file soft_float_cg_ext.cpp
 * @brief Step that extends the call graph with the soft-float calls where appropriate.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 *
*/

///Header include
#include "soft_float_cg_ext.hpp"

///design_flows include
#include "design_flow_graph.hpp"
#include "design_flow_manager.hpp"
#include "design_flow_step.hpp"

///frontend_analysis
#include "symbolic_application_frontend_flow_step.hpp"

///Behavior include
#include "application_manager.hpp"
#include "call_graph.hpp"
#include "call_graph_manager.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"

///Graph include
#include "basic_block.hpp"
#include "basic_blocks_graph_constructor.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <fstream>

///STL include
#include <map>
#include <string>

///Tree include
#include "ext_tree_node.hpp"
#include "tree_basic_block.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"
#include "tree_helper.hpp"
#include "tree_manipulation.hpp"

///Utility include
#include "dbgPrintHelper.hpp"
#include "exceptions.hpp"

bool soft_float_cg_ext::tree_dumped = false;

static tree_nodeRef
create_ga(tree_manipulationRef &IRman, tree_nodeRef &Scpe, tree_nodeRef &type, tree_nodeRef& op, unsigned int bb_index)
{
   function_decl * fd = GetPointer<function_decl>(GET_NODE(Scpe));
   std::string srcp_default = fd->include_name +":0:0";
   tree_nodeRef ssa_vd = IRman->create_ssa_name(tree_nodeRef(), type);
   return IRman->create_gimple_modify_stmt(ssa_vd, op, srcp_default, bb_index);
}

soft_float_cg_ext::soft_float_cg_ext(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, SOFT_FLOAT_CG_EXT, _design_flow_manager, _parameters),
   modified_call_graph(false)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

soft_float_cg_ext::~soft_float_cg_ext()
{}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > soft_float_cg_ext::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   return relationships;
}

void soft_float_cg_ext::Exec()
{
   modified_call_graph = false;
   tree_managerRef TM = AppM->get_tree_manager();
   if(debug_level >= DEBUG_LEVEL_PEDANTIC && !tree_dumped)
   {
      tree_dumped = true;
      std::string raw_file_name = parameters->getOption<std::string>(OPT_output_temporary_directory) + "before_soft_float_cg_ext.raw";
      std::ofstream raw_file(raw_file_name.c_str());
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Tree-Manager dumped for debug purpose");
      raw_file << TM;
      raw_file.close();
   }

   const tree_nodeRef curr_tn = TM->GetTreeNode(function_id);
   tree_nodeRef Scpe = TM->GetTreeReindex(function_id);
   function_decl * fd = GetPointer<function_decl>(curr_tn);
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));

   std::map<unsigned int, blocRef> &blocks = sl->list_of_bloc;
   std::map<unsigned int, blocRef>::iterator it, it_end;

   it_end = blocks.end();
   for(it = blocks.begin(); it != it_end; it++)
   {
      blocRef B = it->second;
      unsigned int B_id = B->number;
      std::list<tree_nodeRef> & list_of_stmt = B->list_of_stmt;
      /// manage capacity
      std::list<tree_nodeRef>::iterator it_los, it_los_end = list_of_stmt.end();
      for(it_los = list_of_stmt.begin(); it_los != it_los_end; ++it_los)
      {
         if(GET_NODE(*it_los)->get_kind() == gimple_cond_K)
         {
            gimple_cond * gc =  GetPointer<gimple_cond>(GET_NODE(*it_los));
            enum kind code0 = GET_NODE(gc->op0)->get_kind();
            if(code0 != ssa_name_K)
            {
               tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));
               unsigned int type_index = tree_helper::get_type_index(TM, GET_INDEX_NODE(gc->op0));
               tree_nodeRef type = TM->GetTreeReindex(type_index);
               tree_nodeRef new_ga = create_ga(IRman, Scpe, type, gc->op0, B_id);
               tree_nodeRef ssa_vd = GetPointer<gimple_assign>(GET_NODE(new_ga))->op0;

               gc->op0 = ssa_vd;
               list_of_stmt.insert(it_los, new_ga);
               it_los = list_of_stmt.begin();
               it_los_end = list_of_stmt.end();
            }
         }
      }
   }

   for(it = blocks.begin(); it != it_end; it++)
   {
      std::list<tree_nodeRef> & list_of_stmt = it->second->list_of_stmt;
      std::list<tree_nodeRef>::iterator it1, it2, it2_end = list_of_stmt.end();
      it1 = list_of_stmt.begin();
      for(it2 = list_of_stmt.begin(); it2 != it2_end; it2 = it1)
      {
         it1++;
         recursive_examinate(*it2);
      }
   }
   AppM->GetCallGraphManager()->ComputeReachedFunctions();

   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      std::string raw_file_name = parameters->getOption<std::string>(OPT_output_temporary_directory) + "after_soft_float_cg_ext.raw";
      std::ofstream raw_file(raw_file_name.c_str());
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Tree-Manager dumped for debug purpose");
      raw_file << TM;
      raw_file.close();
   }
}

void soft_float_cg_ext::recursive_examinate(const tree_nodeRef & tn)
{
   THROW_ASSERT(tn->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Update recursively " + boost::lexical_cast<std::string>(GET_INDEX_NODE(tn)));
   const tree_managerRef TM = AppM->get_tree_manager();
   const tree_nodeRef curr_tn = GET_NODE(tn);
   switch(curr_tn->get_kind())
   {
      case call_expr_K:
      {
         const call_expr * ce = GetPointer<call_expr>(curr_tn);
         const std::vector<tree_nodeRef> & args = ce->args;
         std::vector<tree_nodeRef>::const_iterator arg, arg_end = args.end();
         unsigned int parm_index = 0;
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            recursive_examinate(*arg);
            ++parm_index;
         }
         break;
      }
      case gimple_call_K:
      {
         const gimple_call * ce = GetPointer<gimple_call>(curr_tn);
         const std::vector<tree_nodeRef> & args = ce->args;
         std::vector<tree_nodeRef>::const_iterator arg, arg_end = args.end();
         unsigned int parm_index = 0;
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            recursive_examinate(*arg);
            ++parm_index;
         }
         break;
      }
      case gimple_assign_K:
      {
         gimple_assign * gm = GetPointer<gimple_assign>(curr_tn);
         recursive_examinate(gm->op0);
         recursive_examinate(gm->op1);
         break;
      }
      case gimple_nop_K:
      {
         break;
      }
      case var_decl_K:
      case parm_decl_K:
      case ssa_name_K:
      {
         break;
      }
      case tree_list_K:
      {
         tree_nodeRef current = tn;
         while(current)
         {
            recursive_examinate(GetPointer<tree_list>(GET_NODE(current))->valu);
            current = GetPointer<tree_list>(GET_NODE(current))->chan;
         }
         break;
      }
      case CASE_UNARY_EXPRESSION :
      {
         const unary_expr * ue = GetPointer<unary_expr>(curr_tn);
         recursive_examinate(ue->op);
         tree_nodeRef expr_type = GET_NODE(ue->type);
         unsigned int op_expr_type_index;
         tree_nodeRef op_expr_type = tree_helper::get_type_node(GET_NODE(ue->op), op_expr_type_index);
         if(expr_type->get_kind() == real_type_K)
         {
            switch(curr_tn->get_kind())
            {
               case float_expr_K:
               {
                  unsigned int bitsize_in = tree_helper::size(TM, op_expr_type_index);
                  unsigned int bitsize_out = tree_helper::size(TM, GET_INDEX_NODE(ue->type));
                  std::string bitsize_str_in = bitsize_in == 96 ? "x80" : STR(bitsize_in);
                  std::string bitsize_str_out = bitsize_out == 96 ? "x80" : STR(bitsize_out);
                  std::string fu_name;
                  if(op_expr_type->get_kind() != real_type_K)
                  {
                     if(tree_helper::is_unsigned(TM, op_expr_type_index))
                        fu_name = "uint"+bitsize_str_in+"_to_float"+bitsize_str_out+"if";
                     else
                        fu_name = "int"+bitsize_str_in+"_to_float"+bitsize_str_out+"if";
                     unsigned int called_function_id = TM->function_index(fu_name);
                     THROW_ASSERT(called_function_id, "The library miss this function " + fu_name);
                     THROW_ASSERT(AppM->GetFunctionBehavior(called_function_id)->GetBehavioralHelper()->has_implementation(), "inconsistent behavioral helper");
                     AppM->add_function(function_id, called_function_id, AppM->GetFunctionBehavior(called_function_id)->GetBehavioralHelper(), 0);
                     modified_call_graph = true;
                  }
                  break;
               }
               case nop_expr_K:
               case abs_expr_K:
               case negate_expr_K:
               case view_convert_expr_K:
               case indirect_ref_K:
                  break;
               case addr_expr_K:
               case arrow_expr_K:
               case bit_not_expr_K:
               case buffer_ref_K:
               case card_expr_K:
               case cast_expr_K:
               case cleanup_point_expr_K:
               case conj_expr_K:
               case convert_expr_K:
               case exit_expr_K:
               case fix_ceil_expr_K:
               case fix_floor_expr_K:
               case fix_round_expr_K:
               case fix_trunc_expr_K:
               case imagpart_expr_K:
               case misaligned_indirect_ref_K:
               case loop_expr_K:
               case non_lvalue_expr_K:
               case realpart_expr_K:
               case reference_expr_K:
               case reinterpret_cast_expr_K:
               case sizeof_expr_K:
               case static_cast_expr_K:
               case throw_expr_K:
               case truth_not_expr_K:
               case unsave_expr_K:
               case va_arg_expr_K:
               case reduc_max_expr_K:
               case reduc_min_expr_K:
               case reduc_plus_expr_K:
               case vec_unpack_hi_expr_K:
               case vec_unpack_lo_expr_K:
               case vec_unpack_float_hi_expr_K:
               case vec_unpack_float_lo_expr_K:
               case binfo_K:
               case block_K:
               case call_expr_K:
               case case_label_expr_K:
               case constructor_K:
               case identifier_node_K:
               case ssa_name_K:
               case statement_list_K:
               case target_mem_ref_K:
               case target_mem_ref461_K:
               case tree_list_K:
               case tree_vec_K:
               case CASE_BINARY_EXPRESSION:
               case CASE_CPP_NODES:
               case CASE_CST_NODES:
               case CASE_DECL_NODES:
               case CASE_FAKE_NODES:
               case CASE_GIMPLE_NODES:
               case CASE_PRAGMA_NODES:
               case CASE_QUATERNARY_EXPRESSION:
               case CASE_TERNARY_EXPRESSION:
               case CASE_TYPE_NODES:
               {
                  THROW_ERROR("not yet supported soft float function: " +curr_tn->get_kind_text());
               }
               default:
               {
                  THROW_UNREACHABLE("");
               }

            }
         }
         if(op_expr_type->get_kind() == real_type_K)
         {
            switch(curr_tn->get_kind())
            {
               case fix_trunc_expr_K:
               {
                  unsigned int bitsize_in = tree_helper::size(TM, op_expr_type_index);
                  unsigned int bitsize_out = tree_helper::size(TM, GET_INDEX_NODE(ue->type));
                  bool is_unsigned = tree_helper::is_unsigned(TM, GET_INDEX_NODE(ue->type));
                  std::string bitsize_str_in = bitsize_in == 96 ? "x80" : STR(bitsize_in);
                  std::string bitsize_str_out = bitsize_out == 96 ? "x80" : STR(bitsize_out);
                  std::string fu_name = "float"+bitsize_str_in+"_to_"+ (is_unsigned ? "u" : "") +"int"+bitsize_str_out+"_round_to_zeroif";
                  unsigned int called_function_id = TM->function_index(fu_name);
                  THROW_ASSERT(called_function_id, "The library miss this function " + fu_name);
                  THROW_ASSERT(AppM->GetFunctionBehavior(called_function_id)->GetBehavioralHelper()->has_implementation(), "inconsistent behavioral helper");
                  AppM->add_function(function_id, called_function_id, AppM->GetFunctionBehavior(called_function_id)->GetBehavioralHelper(), 0);
                  modified_call_graph = true;
                  break;
               }
               case binfo_K:
               case block_K:
               case call_expr_K:
               case case_label_expr_K:
               case constructor_K:
               case identifier_node_K:
               case ssa_name_K:
               case statement_list_K:
               case target_mem_ref_K:
               case target_mem_ref461_K:
               case tree_list_K:
               case tree_vec_K:
               case CASE_BINARY_EXPRESSION:
               case CASE_CPP_NODES:
               case CASE_CST_NODES:
               case CASE_DECL_NODES:
               case CASE_FAKE_NODES:
               case CASE_GIMPLE_NODES:
               case CASE_PRAGMA_NODES:
               case CASE_QUATERNARY_EXPRESSION:
               case CASE_TERNARY_EXPRESSION:
               case CASE_TYPE_NODES:
               case abs_expr_K:
               case addr_expr_K:
               case arrow_expr_K:
               case bit_not_expr_K:
               case buffer_ref_K:
               case card_expr_K:
               case cast_expr_K:
               case cleanup_point_expr_K:
               case conj_expr_K:
               case convert_expr_K:
               case exit_expr_K:
               case fix_ceil_expr_K:
               case fix_floor_expr_K:
               case fix_round_expr_K:
               case float_expr_K:
               case imagpart_expr_K:
               case indirect_ref_K:
               case misaligned_indirect_ref_K:
               case loop_expr_K:
               case negate_expr_K:
               case non_lvalue_expr_K:
               case nop_expr_K:
               case realpart_expr_K:
               case reference_expr_K:
               case reinterpret_cast_expr_K:
               case sizeof_expr_K:
               case static_cast_expr_K:
               case throw_expr_K:
               case truth_not_expr_K:
               case unsave_expr_K:
               case va_arg_expr_K:
               case view_convert_expr_K:
               case reduc_max_expr_K:
               case reduc_min_expr_K:
               case reduc_plus_expr_K:
               case vec_unpack_hi_expr_K:
               case vec_unpack_lo_expr_K:
               case vec_unpack_float_hi_expr_K:
               case vec_unpack_float_lo_expr_K:
                  break;
               default:
                  {
                     THROW_UNREACHABLE("");
                  }
            }
         }
         break;
      }
      case CASE_BINARY_EXPRESSION :
      {
         const binary_expr * be = GetPointer<binary_expr>(curr_tn);
         recursive_examinate(be->op0);
         recursive_examinate(be->op1);
         unsigned int expr_type_index;
         tree_nodeRef expr_type = tree_helper::get_type_node(GET_NODE(be->op0), expr_type_index);
         if(expr_type->get_kind() == real_type_K)
         {
            bool add_call = true;
            std::string fu_suffix;
            switch(curr_tn->get_kind())
            {
               case mult_expr_K:
               {
                  fu_suffix = "mul";
                  break;
               }
               case plus_expr_K:
               {
                  fu_suffix = "add";
                  break;
               }
               case minus_expr_K:
               {
                  fu_suffix = "sub";
                  break;
               }
               case rdiv_expr_K:
               {
                  fu_suffix = "div";
                  break;
               }
               case gt_expr_K:
               {
                  fu_suffix = "gt";
                  break;
               }
               case ge_expr_K:
               {
                  fu_suffix = "ge";
                  break;
               }
               case lt_expr_K:
               {
                  fu_suffix = "lt";
                  break;
               }
               case le_expr_K:
               {
                  fu_suffix = "le";
                  break;
               }
               case assert_expr_K:
               case bit_and_expr_K:
               case bit_ior_expr_K:
               case bit_xor_expr_K:
               case catch_expr_K:
               case ceil_div_expr_K:
               case ceil_mod_expr_K:
               case complex_expr_K:
               case compound_expr_K:
               case eh_filter_expr_K:
               case eq_expr_K:
               case exact_div_expr_K:
               case fdesc_expr_K:
               case floor_div_expr_K:
               case floor_mod_expr_K:
               case goto_subroutine_K:
               case in_expr_K:
               case init_expr_K:
               case lrotate_expr_K:
               case lshift_expr_K:
               case max_expr_K:
               case mem_ref_K:
               case min_expr_K:
               case modify_expr_K:
               case ne_expr_K:
               case ordered_expr_K:
               case pointer_plus_expr_K:
               case postdecrement_expr_K:
               case postincrement_expr_K:
               case predecrement_expr_K:
               case preincrement_expr_K:
               case range_expr_K:
               case round_div_expr_K:
               case round_mod_expr_K:
               case rrotate_expr_K:
               case rshift_expr_K:
               case set_le_expr_K:
               case trunc_div_expr_K:
               case trunc_mod_expr_K:
               case truth_and_expr_K:
               case truth_andif_expr_K:
               case truth_or_expr_K:
               case truth_orif_expr_K:
               case truth_xor_expr_K:
               case try_catch_expr_K:
               case try_finally_K:
               case uneq_expr_K:
               case ltgt_expr_K:
               case unge_expr_K:
               case ungt_expr_K:
               case unle_expr_K:
               case unlt_expr_K:
               case unordered_expr_K:
               case widen_sum_expr_K:
               case widen_mult_expr_K:
               case with_size_expr_K:
               case vec_lshift_expr_K:
               case vec_rshift_expr_K:
               case widen_mult_hi_expr_K:
               case widen_mult_lo_expr_K:
               case vec_pack_trunc_expr_K:
               case vec_pack_sat_expr_K:
               case vec_pack_fix_trunc_expr_K:
               case vec_extracteven_expr_K:
               case vec_extractodd_expr_K:
               case vec_interleavehigh_expr_K:
               case vec_interleavelow_expr_K:
               {
                  add_call = false;
                  break;
               }
               case binfo_K:
               case block_K:
               case call_expr_K:
               case case_label_expr_K:
               case constructor_K:
               case identifier_node_K:
               case ssa_name_K:
               case statement_list_K:
               case target_mem_ref_K:
               case target_mem_ref461_K:
               case tree_list_K:
               case tree_vec_K:
               case CASE_CPP_NODES:
               case CASE_CST_NODES:
               case CASE_DECL_NODES:
               case CASE_FAKE_NODES:
               case CASE_GIMPLE_NODES:
               case CASE_PRAGMA_NODES:
               case CASE_QUATERNARY_EXPRESSION:
               case CASE_TERNARY_EXPRESSION:
               case CASE_TYPE_NODES:
               case CASE_UNARY_EXPRESSION:
                  {
                     break;
                  }
               default:
                  {
                     THROW_UNREACHABLE("");
                  }
            }
            if(add_call)
            {
               unsigned int bitsize = tree_helper::size(TM, expr_type_index);
               std::string bitsize_str = bitsize == 96 ? "x80" : STR(bitsize);
               std::string fu_name = "float"+bitsize_str+"_"+fu_suffix+"if";
               unsigned int called_function_id = TM->function_index(fu_name);
               THROW_ASSERT(called_function_id, "The library miss this function " + fu_name);
               THROW_ASSERT(AppM->GetFunctionBehavior(called_function_id)->GetBehavioralHelper()->has_implementation(), "inconsistent behavioral helper");
               AppM->add_function(function_id, called_function_id, AppM->GetFunctionBehavior(called_function_id)->GetBehavioralHelper(), 0);
               modified_call_graph = true;
            }
         }
         break;
      }
      case CASE_TERNARY_EXPRESSION :
      {
         const ternary_expr * te = GetPointer<ternary_expr>(curr_tn);
         recursive_examinate(te->op0);
         if(te->op1)
            recursive_examinate(te->op1);
         if(te->op2)
            recursive_examinate(te->op2);
         break;
      }
      case CASE_QUATERNARY_EXPRESSION :
      {
         const quaternary_expr * qe = GetPointer<quaternary_expr>(curr_tn);
         recursive_examinate(qe->op0);
         if(qe->op1)
            recursive_examinate(qe->op1);
         if(qe->op2)
            recursive_examinate(qe->op2);
         if(qe->op3)
            recursive_examinate(qe->op3);
         break;
      }
      case constructor_K:
      {
         const constructor * co = GetPointer<constructor>(curr_tn);
         const std::vector<std::pair< tree_nodeRef, tree_nodeRef> > & list_of_idx_valu = co->list_of_idx_valu;
         std::vector<std::pair< tree_nodeRef, tree_nodeRef> >::const_iterator it, it_end = list_of_idx_valu.end();
         for(it = list_of_idx_valu.begin(); it != it_end; it++)
         {
            recursive_examinate(it->second);
         }
         break;
      }
      case gimple_cond_K:
      {
         const gimple_cond * gc = GetPointer<gimple_cond>(curr_tn);
         recursive_examinate(gc->op0);
         break;
      }
      case gimple_switch_K:
      {
         const gimple_switch * se = GetPointer<gimple_switch>(curr_tn);
         recursive_examinate(se->op0);
         break;
      }
      case gimple_multi_way_if_K:
      {
         gimple_multi_way_if* gmwi=GetPointer<gimple_multi_way_if>(curr_tn);
         const std::vector<std::pair< tree_nodeRef, unsigned int> >::const_iterator gmwi_it_end = gmwi->list_of_cond.end();
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::const_iterator gmwi_it = gmwi->list_of_cond.begin(); gmwi_it != gmwi_it_end; ++gmwi_it)
            if((*gmwi_it).first)
               recursive_examinate((*gmwi_it).first);
         break;
      }
      case gimple_return_K:
      {
         const gimple_return * re = GetPointer<gimple_return>(curr_tn);
         if(re->op)
            recursive_examinate(re->op);
         break;
      }
      case gimple_for_K:
      {
         const gimple_for * gf = GetPointer<const gimple_for>(curr_tn);
         recursive_examinate(gf->op0);
         recursive_examinate(gf->op1);
         recursive_examinate(gf->op2);
         break;
      }
      case gimple_while_K:
      {
         const gimple_while * gw = GetPointer<gimple_while>(curr_tn);
         recursive_examinate(gw->op0);
         break;
      }
      case CASE_TYPE_NODES:
      case type_decl_K:
      {
         break;
      }
      case target_mem_ref_K:
      {
         const target_mem_ref * tmr = GetPointer<target_mem_ref>(curr_tn);
         if(tmr->symbol)
            recursive_examinate(tmr->symbol);
         if(tmr->base)
            recursive_examinate(tmr->base);
         if(tmr->idx)
            recursive_examinate(tmr->idx);
         break;
      }
      case target_mem_ref461_K:
      {
         const target_mem_ref461 * tmr = GetPointer<target_mem_ref461>(curr_tn);
         if(tmr->base)
            recursive_examinate(tmr->base);
         if(tmr->idx)
            recursive_examinate(tmr->idx);
         if(tmr->idx2)
            recursive_examinate(tmr->idx2);
         break;
      }
      case real_cst_K:
      case complex_cst_K:
      case string_cst_K:
      case integer_cst_K:
      case field_decl_K:
      case function_decl_K:
      case label_decl_K:
      case result_decl_K:
      case vector_cst_K:
      case tree_vec_K:
      case case_label_expr_K:
      case gimple_label_K:
      case gimple_asm_K:
      case gimple_goto_K:
      case CASE_PRAGMA_NODES:
         break;
      case binfo_K:
      case block_K:
      case const_decl_K:
      case CASE_CPP_NODES:
      case gimple_bind_K:
      case gimple_phi_K:
      case gimple_pragma_K:
      case gimple_predict_K:
      case gimple_resx_K:
      case identifier_node_K:
      case last_tree_K:
      case namespace_decl_K:
      case none_K:
      case placeholder_expr_K:
      case statement_list_K:
      case translation_unit_decl_K:
      case tree_reindex_K:
      {
         THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "Not supported node: " + std::string(curr_tn->get_kind_text()));
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Updated recursively " + boost::lexical_cast<std::string>(GET_INDEX_NODE(tn)));
   return;
}

