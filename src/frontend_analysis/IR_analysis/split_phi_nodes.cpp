/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file split_phi_nodes.cpp
 * @brief
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "split_phi_nodes.hpp"

///Behavior include
#include "application_manager.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <fstream>

///Tree include
#include "ext_tree_node.hpp"
#include "tree_basic_block.hpp"
#include "tree_manager.hpp"
#include "tree_reindex.hpp"

split_phi_nodes::split_phi_nodes(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, SPLIT_PHINODES, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}


split_phi_nodes::~split_phi_nodes()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > split_phi_nodes::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(VAR_DECL_FIX, SAME_FUNCTION));
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(PRECEDENCE_RELATIONSHIP):
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void split_phi_nodes::Exec()
{
   const tree_managerRef TM = AppM->get_tree_manager();
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }
   tree_nodeRef temp = TM->get_tree_node_const(function_id);
   function_decl * fd = GetPointer<function_decl>(temp);
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));
   std::map<unsigned int, blocRef> & list_of_bloc = sl->list_of_bloc;

   std::map<unsigned int, blocRef>::iterator iit, iit_end = list_of_bloc.end();
   for(iit = list_of_bloc.begin(); iit != iit_end ; iit++)
   {
      blocRef& bb_block = iit->second;
      if (bb_block->list_of_phi.size())
      {
         //std::cout << "There are phi in block " << bb_block->number << std::endl;
         std::vector<tree_nodeRef>& phi_list = bb_block->list_of_phi;
         for(std::vector<tree_nodeRef>::iterator index = phi_list.begin(); index != phi_list.end(); )
         {
            std::vector<tree_nodeRef>::iterator curr_index = index;
            tree_nodeRef phi = *curr_index;
            bool removed = split_phi(phi, bb_block, list_of_bloc, TM);
            if (removed)
            {
               bb_block->removed_phi++;
               phi_list.erase(curr_index);
            }
            else
               ++index;
         }
      }
   }
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(false);
   }
}

bool split_phi_nodes::split_phi(tree_nodeRef tree_phi, blocRef& bb_block, std::map<unsigned int, blocRef>& list_of_bloc, const tree_managerRef TM)
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Splitting phi node " + boost::lexical_cast<std::string>(GET_INDEX_NODE(tree_phi)));
   gimple_phi *phi = GetPointer<gimple_phi>(GET_NODE(tree_phi));
   THROW_ASSERT(phi, "A not-phinode is stored in phi_list");
   //std::cout << "Analyzing phi-node: @" << GET_INDEX_NODE(tree_phi) << std::endl;
   tree_nodeRef res = phi->res;
   std::vector<std::pair< tree_nodeRef, unsigned int> >& list_of_def_edge = phi->list_of_def_edge;
   if (phi->virtual_flag)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Phi node is virtual so not splitting");
      return false;
   }
   for(unsigned int defId = 0; defId < list_of_def_edge.size(); defId++)
   {
      tree_nodeRef def = list_of_def_edge[defId].first;
      unsigned int bb_source = list_of_def_edge[defId].second;
      blocRef source_bb;
      std::map<unsigned int, blocRef>::iterator it, it_end = list_of_bloc.end();
      for(it = list_of_bloc.begin(); it != it_end ; it++)
      {
         source_bb = it->second;
         if (source_bb->number != bb_source) continue;
         if(replace.find(std::pair<unsigned int, unsigned int>(bb_source, bb_block->number)) != replace.end())
         {
            bb_source = replace[std::pair<unsigned int, unsigned int>(bb_source, bb_block->number)];
            source_bb = list_of_bloc.find(bb_source)->second;
         }
         if (bb_source == bloc::ENTRY_BLOCK_ID)
         {
            //New block has not been created
            if(!next_entry)
            {
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Predecessor is entry: creating new basic block");
               THROW_ASSERT(source_bb->list_of_succ.size() == 1, "ENTRY BB has more than one successor");
               unsigned int this_bb_index = source_bb->list_of_succ.front();
               //Creating new basic block
               blocRef new_bb = blocRef(new bloc());
               new_bb->number = list_of_bloc.rbegin()->first + 1;
               new_bb->list_of_pred.push_back(bloc::ENTRY_BLOCK_ID);
               new_bb->list_of_succ.push_back(this_bb_index);
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Adding edge from " + boost::lexical_cast<std::string>(bloc::ENTRY_BLOCK_ID) + " to " + boost::lexical_cast<std::string>(this_bb_index));

               //Updating entry
               source_bb->list_of_succ.clear();
               source_bb->list_of_succ.push_back(new_bb->number);

               //Updating this
               blocRef this_bb;
               unsigned int index;
               for(index = 0; index < list_of_bloc.size(); index++)
               {
                  if(list_of_bloc[index]->number == this_bb_index)
                  {
                     this_bb = list_of_bloc[index];
                     break;
                  }
               }
               THROW_ASSERT(index < list_of_bloc.size(), "Current basic block not found");
               for(index = 0; index < this_bb->list_of_pred.size(); index++)
               {
                  if(this_bb->list_of_pred[index] == bloc::ENTRY_BLOCK_ID)
                  {
                     PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Adding edge from " + boost::lexical_cast<std::string>(new_bb->number) + " to " + boost::lexical_cast<std::string>(this_bb->list_of_pred[index]));
                     this_bb->list_of_pred[index] = new_bb->number;
                     break;
                  }
               }
               THROW_ASSERT(index < this_bb->list_of_pred.size(), "Entry not found in predecessors of current basic block");

               list_of_bloc[new_bb->number] = new_bb;
               source_bb = new_bb;
               next_entry = new_bb;
            }
            else
            {
               source_bb = next_entry;
            }
         }
         std::list<tree_nodeRef>& list_of_stmt = source_bb->list_of_stmt;

         unsigned int gimple_stmt_id = TM->new_tree_node_id();
         std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
         IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
         IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(res));
         IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(def));
         IR_schema[TOK(TOK_ORIG)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(tree_phi));
         TM->create_tree_node(gimple_stmt_id, gimple_assign_K, IR_schema);
         tree_nodeRef created_stmt = TM->GetTreeReindex(gimple_stmt_id);
         GetPointer<ssa_name>(GET_NODE(res))->add_def(created_stmt);

         if(
               list_of_stmt.size()
               and
               (
                  GetPointer<gimple_goto>(GET_NODE(list_of_stmt.back())) ||
                  GetPointer<gimple_while>(GET_NODE(list_of_stmt.back())) ||
                  GetPointer<gimple_for>(GET_NODE(list_of_stmt.back())) ||
                  GetPointer<gimple_switch>(GET_NODE(list_of_stmt.back()))
               )
           )
         {
            tree_nodeRef back = list_of_stmt.back();
            list_of_stmt.erase(std::find(list_of_stmt.begin(), list_of_stmt.end(), back));
            list_of_stmt.push_back(created_stmt);
            list_of_stmt.push_back(back);
         }
         else if(list_of_stmt.size() && GetPointer<gimple_cond>(GET_NODE(list_of_stmt.back())))
         {
            if(bb_block->list_of_pred.size() > 1)
            {
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "One of the predecessors ends with an if");
               bool true_case = source_bb->true_edge == bb_block->number;
               blocRef new_bb = blocRef(new bloc());
               if(true_case)
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "This is true edge");
                  //Creating new basic block
                  new_bb->number = list_of_bloc.rbegin()->first + 1;
                  new_bb->list_of_pred.push_back(source_bb->true_edge);
                  new_bb->list_of_succ.push_back(bb_block->number);
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created new basic block " + boost::lexical_cast<std::string>(new_bb->number));

                  //Updating predecessor
                  source_bb->list_of_succ.erase(std::find(source_bb->list_of_succ.begin(), source_bb->list_of_succ.end(), bb_block->number));
                  source_bb->list_of_succ.push_back(new_bb->number);
                  source_bb->true_edge = new_bb->number;

                  //Updating this
                  bb_block->list_of_pred.erase(std::find(bb_block->list_of_pred.begin(), bb_block->list_of_pred.end(), source_bb->number));
                  bb_block->list_of_pred.push_back(new_bb->number);
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Adding edge from " + boost::lexical_cast<std::string>(source_bb->number) + " to " + boost::lexical_cast<std::string>(new_bb->number));
               }
               else
               {
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "This is false edge");
                  //Creating new basic block
                  new_bb->number = list_of_bloc.rbegin()->first + 1;
                  new_bb->list_of_pred.push_back(source_bb->false_edge);
                  new_bb->list_of_succ.push_back(bb_block->number);
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Created new basic block " + boost::lexical_cast<std::string>(new_bb->number));

                  //Updating predecessor
                  source_bb->list_of_succ.erase(std::find(source_bb->list_of_succ.begin(), source_bb->list_of_succ.end(), bb_block->number));
                  source_bb->list_of_succ.push_back(new_bb->number);
                  source_bb->false_edge = new_bb->number;
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Updated predecessor");

                  //Updating this
                  bb_block->list_of_pred.erase(std::find(bb_block->list_of_pred.begin(), bb_block->list_of_pred.end(), source_bb->number));
                  bb_block->list_of_pred.push_back(new_bb->number);
                  PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Adding edge from " + boost::lexical_cast<std::string>(source_bb->number) + " to " + boost::lexical_cast<std::string>(new_bb->number));

               }
               list_of_bloc[new_bb->number] = new_bb;
               replace[std::pair<unsigned int, unsigned int>(source_bb->number, bb_block->number)] = new_bb->number;

               //Inserting phi
               std::list<tree_nodeRef>& ilist_of_stmt = new_bb->list_of_stmt;
               ilist_of_stmt.push_back(created_stmt);
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Inserted new gimple " + boost::lexical_cast<std::string>(gimple_stmt_id));
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Finished creation of new basic block");
            }
            else
            {
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Inserted new gimple " + boost::lexical_cast<std::string>(gimple_stmt_id));
               bb_block->list_of_stmt.insert(bb_block->list_of_stmt.begin(), created_stmt);
            }
         }
         else
         {
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Inserted new gimple " + boost::lexical_cast<std::string>(gimple_stmt_id));
            list_of_stmt.push_back(created_stmt);
         }
         break;
      }
   }
   return true;
}
