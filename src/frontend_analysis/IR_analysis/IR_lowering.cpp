/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file IR_lowering.cpp
 * @brief Decompose some complex gimple statements into set of simple operations.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Header include
#include "IR_lowering.hpp"

///Behavior include
#include "application_manager.hpp"
#include "behavioral_helper.hpp"
#include "function_behavior.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <fstream>

///Tree include
#include "tree_basic_block.hpp"
#include "tree_manager.hpp"
#include "tree_reindex.hpp"
#include "tree_manipulation.hpp"
#include "tree_helper.hpp"

static tree_nodeRef
create_ga(tree_manipulationRef &IRman, tree_nodeRef &Scpe, tree_nodeRef &type, tree_nodeRef& op, unsigned int bb_index)
{
   function_decl * fd = GetPointer<function_decl>(GET_NODE(Scpe));
   std::string srcp_default = fd->include_name +":0:0";
   tree_nodeRef ssa_vd = IRman->create_ssa_name(tree_nodeRef(), type);
   return IRman->create_gimple_modify_stmt(ssa_vd, op, srcp_default, bb_index);
}


/// the code for lowering of div, mult and rem comes from GCC sources (expmed.c)

/// Test whether a value is zero of a power of two.
#define EXACT_POWER_OF_2_OR_ZERO_P(x) (((x) & ((x) - 1)) == 0)

/// Given X, an unsigned number, return the largest int Y such that 2**Y <= X. If X is 0, return -1.
static int
floor_log2 (unsigned long long int x)
{
  int t = 0;

  if (x == 0)
    return -1;

  if (x >= (static_cast<unsigned long long int>(1)) << (t + 32))
      t += 32;
  if (x >= (static_cast<unsigned long long int>(1)) << (t + 16))
    t += 16;
  if (x >= (static_cast<unsigned long long int>(1)) << (t + 8))
    t += 8;
  if (x >= (static_cast<unsigned long long int>(1)) << (t + 4))
    t += 4;
  if (x >= (static_cast<unsigned long long int>(1)) << (t + 2))
    t += 2;
  if (x >= (static_cast<unsigned long long int>(1)) << (t + 1))
    t += 1;

  return t;
}
/** Return the logarithm of X, base 2, considering X unsigned,
   if X is a power of 2.  Otherwise, returns -1.  */
static int
exact_log2 (unsigned long long int x)
{
  if (x != (x & -x))
    return -1;
  return floor_log2 (x);
}


/// Compute the inverse of X mod 2**n, i.e., find Y such that X * Y is
/// congruent to 1 (mod 2**N).
static unsigned long long int
invert_mod2n (unsigned long long int x, unsigned int n)
{
  /* Solve x*y == 1 (mod 2^n), where x is odd.  Return y.  */

  /* The algorithm notes that the choice y = x satisfies
     x*y == 1 mod 2^3, since x is assumed odd.
     Each iteration doubles the number of bits of significance in y.  */

  unsigned long long int mask;
  unsigned long long int y = x;
  unsigned int nbit = 3;

  mask = (n == sizeof(long long int)*8
      ? ~static_cast<unsigned long long int>(0)
      : (static_cast<unsigned long long int>(1) << n) - 1);

  while (nbit < n)
    {
      y = y * (2 - x*y) & mask;		/* Modulo 2^N */
      nbit *= 2;
    }
  return y;
}

enum alg_code {
  alg_unknown,
  alg_zero,
  alg_m, alg_shift,
  alg_add_t_m2,
  alg_sub_t_m2,
  alg_add_factor,
  alg_sub_factor,
  alg_add_t2_m,
  alg_sub_t2_m,
  alg_impossible
};

/** This structure holds the "cost" of a multiply sequence.  The
   "cost" field holds the total cost of every operator in the
   synthetic multiplication sequence, hence cost(a op b) is defined
   as cost(op) + cost(a) + cost(b), where cost(leaf) is zero.
   The "latency" field holds the minimum possible latency of the
   synthetic multiply, on a hypothetical infinitely parallel CPU.
   This is the critical path, or the maximum height, of the expression
   tree which is the sum of costs on the most expensive path from
   any leaf to the root.  Hence latency(a op b) is defined as zero for
   leaves and cost(op) + max(latency(a), latency(b)) otherwise.  */
struct mult_cost {
  short cost;     /** Total cost of the multiplication sequence.  */
  short latency;  /** The latency of the multiplication sequence.  */
};


/** This structure records a sequence of operations.
   `ops' is the number of operations recorded.
   `cost' is their total cost.
   The operations are stored in `op' and the corresponding
   logarithms of the integer coefficients in `log'.

   These are the operations:
   alg_zero		total := 0;
   alg_m		total := multiplicand;
   alg_shift		total := total * coeff
   alg_add_t_m2		total := total + multiplicand * coeff;
   alg_sub_t_m2		total := total - multiplicand * coeff;
   alg_add_factor	total := total * coeff + total;
   alg_sub_factor	total := total * coeff - total;
   alg_add_t2_m		total := total * coeff + multiplicand;
   alg_sub_t2_m		total := total * coeff - multiplicand;

   The first operand must be either alg_zero or alg_m.  */
struct algorithm
{
  struct mult_cost cost;
  short ops;
  /* The size of the OP and LOG fields are not directly related to the
     word size, but the worst-case algorithms will be if we have few
     consecutive ones or zeros, i.e., a multiplicand like 10101010101...
     In that case we will generate shift-by-2, add, shift-by-2, add,...,
     in total wordsize operations.  */
  enum alg_code op[64];
  char log[64];
};

/** Indicates the type of fixup needed after a constant multiplication.
   BASIC_VARIANT means no fixup is needed, NEGATE_VARIANT means that
   the result should be negated, and ADD_VARIANT means that the
   multiplicand should be added to the result.  */
enum mult_variant {basic_variant, negate_variant, add_variant};

/** This macro is used to compare a pointer to a mult_cost against an
   single integer "cost" value.  This is equivalent to the macro
   CHEAPER_MULT_COST(X,Z) where Z = {Y,Y}.  */
#define MULT_COST_LESS(X,Y) ((X).cost < (Y)	\
                 || ((X).cost == (Y) && (X).latency < (Y)))

/** This macro is used to compare two mult_costs against
   each other.  The macro returns true if X is cheaper than Y.
   Currently, the cheaper of two mult_costs is the one with the
   lower "cost".  If "cost"s are tied, the lower latency is cheaper.  */
#define CHEAPER_MULT_COST(X,Y)  ((X).cost < (Y).cost		\
                 || ((X).cost == (Y).cost	\
                     && (X).latency < (Y).latency))


static std::unordered_map<std::pair<unsigned int,unsigned long long>, std::pair<enum alg_code, struct mult_cost> > alg_hash;

/** Compute and return the best algorithm for multiplying by T.
   The algorithm must cost less than cost_limit
   If retval.cost >= COST_LIMIT, no algorithm was found and all
   other field of the returned struct are undefined.
   MODE is the machine mode of the multiplication.  */
static void
synth_mult (struct algorithm &alg_out, unsigned long long t,
        const struct mult_cost &cost_limit, tree_nodeRef &type, tree_managerRef &TM)
{
   int m;
   struct algorithm alg_in, best_alg;
   struct mult_cost best_cost;
   struct mult_cost new_limit;
   short op_cost, op_latency;
   unsigned long long int orig_t = t;
   unsigned long long int q;
   unsigned data_bitsize = tree_helper::size(TM, tree_helper::get_type_index(TM,GET_INDEX_NODE(type)));
   int maxm = static_cast<int>(std::min (32u, data_bitsize));
   bool cache_hit = false;
   enum alg_code cache_alg = alg_zero;

   /** Indicate that no algorithm is yet found.  If no algorithm
      is found, this value will be returned and indicate failure.  */
   alg_out.cost.cost = static_cast<short>(cost_limit.cost + 1);
   alg_out.cost.latency = static_cast<short>(cost_limit.latency + 1);

   if (cost_limit.cost < 0
       || (cost_limit.cost == 0 && cost_limit.latency <= 0))
      return;

   /** t == 1 can be done in zero cost.  */
   if (t == 1)
   {
      alg_out.ops = 1;
      alg_out.cost.cost = 0;
      alg_out.cost.latency = 0;
      alg_out.op[0] = alg_m;
      return;
   }

   /** t == 0 sometimes has a cost.  If it does and it exceeds our limit,
      fail now.  */
   if (t == 0)
   {
      alg_out.ops = 1;
      alg_out.cost.cost = 0/*zero_cost[speed]*/;
      alg_out.cost.latency = 0/*zero_cost[speed]*/;
      alg_out.op[0] = alg_zero;
      return;
   }
   best_cost = cost_limit;
   if(alg_hash.find(std::make_pair(data_bitsize,t)) != alg_hash.end())
   {
      std::pair<enum alg_code, struct mult_cost> res = alg_hash.find(std::make_pair(data_bitsize,t))->second;
      cache_alg = res.first;
      if (cache_alg == alg_impossible)
      {
         /** The cache tells us that it's impossible to synthesize
               multiplication by T within alg_hash[hash_index].cost.  */
         if (!CHEAPER_MULT_COST (res.second, cost_limit))
            /** COST_LIMIT is at least as restrictive as the one
                 recorded in the hash table, in which case we have no
                 hope of synthesizing a multiplication.  Just
                 return.  */
            return;

         /** If we get here, COST_LIMIT is less restrictive than the
               one recorded in the hash table, so we may be able to
               synthesize a multiplication.  Proceed as if we didn't
               have the cache entry.  */
      }
      else
      {
         if (CHEAPER_MULT_COST (cost_limit, res.second))
            /** The cached algorithm shows that this multiplication
                 requires more cost than COST_LIMIT.  Just return.  This
                 way, we don't clobber this cache entry with
                 alg_impossible but retain useful information.  */
            return;
         cache_hit = true;

         switch (cache_alg)
         {
            case alg_shift:
               goto do_alg_shift;

            case alg_add_t_m2:
            case alg_sub_t_m2:
               goto do_alg_addsub_t_m2;

            case alg_add_factor:
            case alg_sub_factor:
               goto do_alg_addsub_factor;

            case alg_add_t2_m:
               goto do_alg_add_t2_m;

            case alg_sub_t2_m:
               goto do_alg_sub_t2_m;
            case alg_impossible:
            case alg_m:
            case alg_unknown:
            case alg_zero:
            default:
               THROW_ERROR ("condition not expected");
         }
      }
   }

   /** If we have a group of zero bits at the low-order part of T, try
      multiplying by the remaining bits and then doing a shift.  */

   if ((t & 1) == 0)
   {
      do_alg_shift:
      m = floor_log2 (t & -t);	/* m = number of low zero bits */
      if (m < maxm)
      {
         q = t >> m;
         /* The function expand_shift will choose between a shift and
                  a sequence of additions, so the observed cost is given as
                  MIN (m * add_cost[speed][mode], shift_cost[speed][mode][m]).  */
         op_cost = static_cast<short>(m * 1/*add_cost[speed][mode]*/);
         if (0/*shift_cost[speed][mode][m]*/ < op_cost)
            op_cost = 0/*shift_cost[speed][mode][m]*/;
         new_limit.cost = static_cast<short>(best_cost.cost - op_cost);
         new_limit.latency = static_cast<short>(best_cost.latency - op_cost);
         synth_mult (alg_in, q, new_limit, type, TM);

         alg_in.cost.cost = static_cast<short>(alg_in.cost.cost + op_cost);
         alg_in.cost.latency = static_cast<short>(alg_in.cost.latency + op_cost);
         if (CHEAPER_MULT_COST (alg_in.cost, best_cost))
         {
            best_cost = alg_in.cost;
            std::swap(alg_in, best_alg);
            best_alg.log[best_alg.ops] = static_cast<char>(m);
            best_alg.op[best_alg.ops] = alg_shift;
         }

         /** See if treating ORIG_T as a signed number yields a better
          sequence.  Try this sequence only for a negative ORIG_T
          as it would be useless for a non-negative ORIG_T.  */
         if (static_cast<long long int>(orig_t) < 0)
         {
            /** Shift ORIG_T as follows because a right shift of a
          negative-valued signed type is implementation
          defined.  */
            q = ~(~orig_t >> m);
            /** The function expand_shift will choose between a shift
          and a sequence of additions, so the observed cost is
          given as MIN (m * add_cost[speed][mode], shift_cost[speed][mode][m]).  */
            op_cost = static_cast<short>(m * 1)/*add_cost[speed][mode]*/;
            if (0 /*shift_cost[speed][mode][m]*/ < op_cost)
               op_cost = 0/*shift_cost[speed][mode][m]*/;
            new_limit.cost = static_cast<short>(best_cost.cost - op_cost);
            new_limit.latency = static_cast<short>(best_cost.latency - op_cost);
            synth_mult (alg_in, q, new_limit, type, TM);

            alg_in.cost.cost = static_cast<short>(alg_in.cost.cost + op_cost);
            alg_in.cost.latency = static_cast<short>(alg_in.cost.latency + op_cost);
            if (CHEAPER_MULT_COST (alg_in.cost, best_cost))
            {
               best_cost = alg_in.cost;
               std::swap(alg_in, best_alg);
               best_alg.log[best_alg.ops] = static_cast<char>(m);
               best_alg.op[best_alg.ops] = alg_shift;
            }
         }
      }
      if (cache_hit)
         goto done;
   }

   /** If we have an odd number, add or subtract one.  */
   if ((t & 1) != 0)
   {
      unsigned long long int w;

      do_alg_addsub_t_m2:
      for (w = 1; (w & t) != 0; w <<= 1)
         ;
      /** If T was -1, then W will be zero after the loop.  This is another
      case where T ends with ...111.  Handling this with (T + 1) and
      subtract 1 produces slightly better code and results in algorithm
      selection much faster than treating it like the ...0111 case
      below.  */
      if (w == 0
          || (w > 2
              /** Reject the case where t is 3.
                  Thus we prefer addition in that case.  */
              && t != 3))
      {
         /** T ends with ...111.  Multiply by (T + 1) and subtract 1.  */

         op_cost = 1/*add_cost[speed][mode]*/;
         new_limit.cost = static_cast<short>(best_cost.cost - op_cost);
         new_limit.latency = static_cast<short>(best_cost.latency - op_cost);
         synth_mult (alg_in, t + 1, new_limit, type, TM);

         alg_in.cost.cost = static_cast<short>(alg_in.cost.cost + op_cost);
         alg_in.cost.latency = static_cast<short>(alg_in.cost.latency + op_cost);
         if (CHEAPER_MULT_COST (alg_in.cost, best_cost))
         {
            best_cost = alg_in.cost;
            std::swap(alg_in, best_alg);
            best_alg.log[best_alg.ops] = 0;
            best_alg.op[best_alg.ops] = alg_sub_t_m2;
         }
      }
      else
      {
         /** T ends with ...01 or ...011.  Multiply by (T - 1) and add 1.  */

         op_cost = 1/*add_cost[speed][mode]*/;
         new_limit.cost = static_cast<short>(best_cost.cost - op_cost);
         new_limit.latency = static_cast<short>(best_cost.latency - op_cost);
         synth_mult (alg_in, t - 1, new_limit, type, TM);

         alg_in.cost.cost = static_cast<short>(alg_in.cost.cost + op_cost);
         alg_in.cost.latency = static_cast<short>(alg_in.cost.latency + op_cost);
         if (CHEAPER_MULT_COST (alg_in.cost, best_cost))
         {
            best_cost = alg_in.cost;
            std::swap(alg_in, best_alg);
            best_alg.log[best_alg.ops] = 0;
            best_alg.op[best_alg.ops] = alg_add_t_m2;
         }
      }

      /** We may be able to calculate a * -7, a * -15, a * -31, etc
      quickly with a - a * n for some appropriate constant n.  */
      m = exact_log2 (-orig_t + 1);
      if (m >= 0 && m < maxm)
      {
         op_cost = 1/*shiftsub1_cost[speed][mode][m]*/;
         new_limit.cost = static_cast<short>(best_cost.cost - op_cost);
         new_limit.latency = static_cast<short>(best_cost.latency - op_cost);
         synth_mult (alg_in, static_cast<unsigned long long int>(-orig_t + 1) >> m, new_limit, type, TM);

         alg_in.cost.cost = static_cast<short>(alg_in.cost.cost + op_cost);
         alg_in.cost.latency = static_cast<short>(alg_in.cost.latency + op_cost);
         if (CHEAPER_MULT_COST (alg_in.cost, best_cost))
         {
            best_cost = alg_in.cost;
            std::swap(alg_in, best_alg);
            best_alg.log[best_alg.ops] = static_cast<char>(m);
            best_alg.op[best_alg.ops] = alg_sub_t_m2;
         }
      }

      if (cache_hit)
         goto done;
   }

   /** Look for factors of t of the form
      t = q(2**m +- 1), 2 <= m <= floor(log2(t - 1)).
      If we find such a factor, we can multiply by t using an algorithm that
      multiplies by q, shift the result by m and add/subtract it to itself.

      We search for large factors first and loop down, even if large factors
      are less probable than small; if we find a large factor we will find a
      good sequence quickly, and therefore be able to prune (by decreasing
      COST_LIMIT) the search.  */

   do_alg_addsub_factor:
   for (m = floor_log2 (t - 1); m >= 2; m--)
   {
      unsigned long long int d;

      d = (static_cast<unsigned long long int>(1) << m) + 1;
      if (t % d == 0 && t > d && m < maxm
          && (!cache_hit || cache_alg == alg_add_factor))
      {
         /** If the target has a cheap shift-and-add instruction use
          that in preference to a shift insn followed by an add insn.
          Assume that the shift-and-add is "atomic" with a latency
          equal to its cost, otherwise assume that on superscalar
          hardware the shift may be executed concurrently with the
          earlier steps in the algorithm.  */
         op_cost = 1/*add_cost[speed][mode]*/ + 0/*shift_cost[speed][mode][m]*/;
         if (1/*shiftadd_cost[speed][mode][m]*/ < op_cost)
         {
            op_cost = 1/*shiftadd_cost[speed][mode][m]*/;
            op_latency = op_cost;
         }
         else
            op_latency = 1/*add_cost[speed][mode]*/;

         new_limit.cost = static_cast<short>(best_cost.cost - op_cost);
         new_limit.latency = static_cast<short>(best_cost.latency - op_latency);
         synth_mult (alg_in, t / d, new_limit, type, TM);

         alg_in.cost.cost = static_cast<short>(alg_in.cost.cost + op_cost);
         alg_in.cost.latency = static_cast<short>(alg_in.cost.latency + op_latency);
         if (alg_in.cost.latency < op_cost)
            alg_in.cost.latency = op_cost;
         if (CHEAPER_MULT_COST (alg_in.cost, best_cost))
         {
            best_cost = alg_in.cost;
            std::swap(alg_in, best_alg);
            best_alg.log[best_alg.ops] = static_cast<char>(m);
            best_alg.op[best_alg.ops] = alg_add_factor;
         }
         /** Other factors will have been taken care of in the recursion.  */
         break;
      }

      d = (static_cast<unsigned long long int>(1) << m) - 1;
      if (t % d == 0 && t > d && m < maxm
          && (!cache_hit || cache_alg == alg_sub_factor))
      {
         /** If the target has a cheap shift-and-subtract insn use
          that in preference to a shift insn followed by a sub insn.
          Assume that the shift-and-sub is "atomic" with a latency
          equal to it's cost, otherwise assume that on superscalar
          hardware the shift may be executed concurrently with the
          earlier steps in the algorithm.  */
         op_cost = 1/*add_cost[speed][mode]*/ + 0/*shift_cost[speed][mode][m]*/;
         if (1/*shiftsub0_cost[speed][mode][m]*/ < op_cost)
         {
            op_cost = 1/*shiftsub0_cost[speed][mode][m]*/;
            op_latency = op_cost;
         }
         else
            op_latency = 1/*add_cost[speed][mode]*/;

         new_limit.cost = static_cast<short>(best_cost.cost - op_cost);
         new_limit.latency = static_cast<short>(best_cost.latency - op_latency);
         synth_mult (alg_in, t / d, new_limit, type, TM);

         alg_in.cost.cost = static_cast<short>(alg_in.cost.cost + op_cost);
         alg_in.cost.latency = static_cast<short>(alg_in.cost.latency + op_latency);
         if (alg_in.cost.latency < op_cost)
            alg_in.cost.latency = op_cost;
         if (CHEAPER_MULT_COST (alg_in.cost, best_cost))
         {
            best_cost = alg_in.cost;
            std::swap(alg_in, best_alg);
            best_alg.log[best_alg.ops] = static_cast<char>(m);
            best_alg.op[best_alg.ops] = alg_sub_factor;
         }
         break;
      }
   }
   if (cache_hit)
      goto done;

   /** Try shift-and-add (load effective address) instructions,
      i.e. do a*3, a*5, a*9.  */
   if ((t & 1) != 0)
   {
   do_alg_add_t2_m:
      q = t - 1;
      q = q & -q;
      m = exact_log2 (q);
      if (m >= 0 && m < maxm)
      {
         op_cost = 1/*shiftadd_cost[speed][mode][m]*/;
         new_limit.cost = static_cast<short>(best_cost.cost - op_cost);
         new_limit.latency = static_cast<short>(best_cost.latency - op_cost);
         synth_mult (alg_in, (t - 1) >> m, new_limit, type, TM);

         alg_in.cost.cost = static_cast<short>(alg_in.cost.cost + op_cost);
         alg_in.cost.latency = static_cast<short>(alg_in.cost.latency + op_cost);
         if (CHEAPER_MULT_COST (alg_in.cost, best_cost))
         {
            best_cost = alg_in.cost;
            std::swap(alg_in, best_alg);
            best_alg.log[best_alg.ops] = static_cast<char>(m);
            best_alg.op[best_alg.ops] = alg_add_t2_m;
         }
      }
      if (cache_hit)
         goto done;

      do_alg_sub_t2_m:
      q = t + 1;
      q = q & -q;
      m = exact_log2 (q);
      if (m >= 0 && m < maxm)
      {
         op_cost = 1/*shiftsub0_cost[speed][mode][m]*/;
         new_limit.cost = static_cast<short>(best_cost.cost - op_cost);
         new_limit.latency = static_cast<short>(best_cost.latency - op_cost);
         synth_mult (alg_in, (t + 1) >> m, new_limit, type, TM);

         alg_in.cost.cost = static_cast<short>(alg_in.cost.cost + op_cost);
         alg_in.cost.latency = static_cast<short>(alg_in.cost.latency + op_cost);
         if (CHEAPER_MULT_COST (alg_in.cost, best_cost))
         {
            best_cost = alg_in.cost;
            std::swap(alg_in, best_alg);
            best_alg.log[best_alg.ops] = static_cast<char>(m);
            best_alg.op[best_alg.ops] = alg_sub_t2_m;
         }
      }
      if (cache_hit)
         goto done;
   }

   done:
   /** If best_cost has not decreased, we have not found any algorithm.  */
   if (!CHEAPER_MULT_COST (best_cost, cost_limit))
   {
      /** We failed to find an algorithm.  Record alg_impossible for
      this case (that is, <T, MODE, COST_LIMIT>) so that next time
      we are asked to find an algorithm for T within the same or
      lower COST_LIMIT, we can immediately return to the
      caller.  */
      alg_hash[std::make_pair(data_bitsize, t)] = std::make_pair(alg_impossible, cost_limit);
      return;
   }

   /** Cache the result.  */
   if (!cache_hit)
   {
      mult_cost last_limit = {best_cost.cost,best_cost.latency};
      alg_hash[std::make_pair(data_bitsize, t)] = std::make_pair(best_alg.op[best_alg.ops], last_limit);
   }

   /** If we are getting a too long sequence for `struct algorithm'
      to record, make this search fail.  */
   if (best_alg.ops == 64)
      return;

   /** Copy the algorithm from temporary space to the space at alg_out.
      We avoid using structure assignment because the majority of
      best_alg is normally undefined, and this is a critical function.  */
   alg_out = best_alg;
   alg_out.ops = static_cast<short>(best_alg.ops + 1);

}

/** Find the cheapest way of multiplying a value of mode MODE by VAL.
   Try three variations:
       - a shift/add sequence based on VAL itself
       - a shift/add sequence based on -VAL, followed by a negation
       - a shift/add sequence based on VAL - 1, followed by an addition.

   Return true if the cheapest of these cost less than MULT_COST,
   describing the algorithm in *ALG and final fixup in *VARIANT.  */
static bool
choose_mult_variant (tree_nodeRef &type, long long int val,
             struct algorithm &alg, enum mult_variant &variant, short int Mult_cost, tree_managerRef &TM)
{
   unsigned int data_bitsize = tree_helper::size(TM, tree_helper::get_type_index(TM,GET_INDEX_NODE(type)));
   struct algorithm alg2;
   struct mult_cost limit;
   short int op_cost;

   /** Fail quickly for impossible bounds.  */
   if (Mult_cost < 0)
     return false;

   /** Ensure that Mult_cost provides a reasonable upper bound.
      Any constant multiplication can be performed with less
      than 2 * bits additions.  */
   op_cost = static_cast<short>(2 * data_bitsize * 1/*add_cost[speed][mode]*/);
   if (Mult_cost > op_cost)
     Mult_cost = op_cost;

   variant = basic_variant;
   limit.cost = Mult_cost;
   limit.latency = Mult_cost;
   synth_mult (alg, static_cast<unsigned long long int>(val), limit, type, TM);

   /* This works only if the inverted value actually fits in an
      `unsigned int' */
   if (8*sizeof(int) >= data_bitsize)
     {
       op_cost = 1/*neg_cost[speed][mode]*/;
       if (MULT_COST_LESS (alg.cost, Mult_cost))
     {
       limit.cost = static_cast<short>(alg.cost.cost - op_cost);
       limit.latency = static_cast<short>(alg.cost.latency - op_cost);
     }
       else
     {
       limit.cost = static_cast<short>(Mult_cost - op_cost);
       limit.latency = static_cast<short>(Mult_cost - op_cost);
     }

       synth_mult (alg2, static_cast<unsigned long long int>(-val), limit, type, TM);
       alg2.cost.cost = static_cast<short>(alg2.cost.cost + op_cost);
       alg2.cost.latency = static_cast<short>(alg2.cost.latency + op_cost);
       if (CHEAPER_MULT_COST (alg2.cost, alg.cost))
          alg = alg2, variant = negate_variant;
     }

   /* This proves very useful for division-by-constant.  */
   op_cost = 1/*add_cost[speed][mode]*/;
   if (MULT_COST_LESS (alg.cost, Mult_cost))
     {
       limit.cost = static_cast<short>(alg.cost.cost - op_cost);
       limit.latency = static_cast<short>(alg.cost.latency - op_cost);
     }
   else
     {
       limit.cost = static_cast<short>(Mult_cost - op_cost);
       limit.latency = static_cast<short>(Mult_cost - op_cost);
     }

   synth_mult (alg2, static_cast<unsigned long long int>(val - 1), limit, type, TM);
   alg2.cost.cost = static_cast<short>(alg2.cost.cost + op_cost);
   alg2.cost.latency = static_cast<short>(alg2.cost.latency + op_cost);
   if (CHEAPER_MULT_COST (alg2.cost, alg.cost))
     alg = alg2, variant = add_variant;

   return MULT_COST_LESS (alg.cost, Mult_cost);
 }

 /** A subroutine of expand_mult, used for constant multiplications.
    Multiply OP0 by VAL in mode MODE, storing the result in TARGET if
    convenient.  Use the shift/add sequence described by ALG and apply
    the final fixup specified by VARIANT.  */

 static tree_nodeRef
 expand_mult_const (tree_nodeRef op0, unsigned long long int val,
            const struct algorithm &alg,
            enum mult_variant &variant,
            const std::list<tree_nodeRef>::iterator &it_los, std::list<tree_nodeRef> & list_of_stmt,
            tree_manipulationRef &IRman, tree_nodeRef &Scpe, tree_nodeRef &type, tree_managerRef &TM, unsigned int bb_index)
 {
   long long int val_so_far = 0;
   tree_nodeRef accum, tem;
   int opno;
   unsigned data_bitsize = tree_helper::size(TM, tree_helper::get_type_index(TM,GET_INDEX_NODE(type)));
   unsigned long long int data_mask = data_bitsize >= 64 ? ~ static_cast<unsigned long long int>(0) : (static_cast<unsigned long long int>(1) << data_bitsize) -1;
   function_decl * fd = GetPointer<function_decl>(GET_NODE(Scpe));
   std::string srcp_default = fd->include_name +":0:0";
   tree_nodeRef tem_ga;
   unsigned int integer_cst0_id = TM->new_tree_node_id();
   tree_nodeRef COST0 = IRman->CreateIntegerCst(type, 0, integer_cst0_id);


   /* ACCUM starts out either as OP0 or as a zero, depending on
      the first operation.  */

   if (alg.op[0] == alg_zero)
   {
      accum = COST0;
      val_so_far = 0;
   }
   else if (alg.op[0] == alg_m)
   {
      accum = op0;
      val_so_far = 1;
   }
   else
      THROW_ERROR("condition not expected");

   for (opno = 1; opno < alg.ops; opno++)
   {
      int log = alg.log[opno];
      tree_nodeRef log_node;

      if(log == 0)
         log_node = COST0;
      else
      {
         unsigned int log_id = TM->new_tree_node_id();
         log_node = IRman->CreateIntegerCst(type, static_cast<long long int>(log), log_id);
      }


      switch (alg.op[opno])
      {
         case alg_shift:
            tem = IRman->create_binary_operation(type, accum, log_node, srcp_default, lshift_expr_K);
            tem_ga = create_ga(IRman, Scpe, type, tem, bb_index);
            list_of_stmt.insert(it_los, tem_ga);
            accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            val_so_far <<= log;
            break;

         case alg_add_t_m2:
            if(log_node != COST0)
            {
               tem = IRman->create_binary_operation(type, op0, log_node, srcp_default, lshift_expr_K);
               tem_ga = create_ga(IRman, Scpe, type, tem, bb_index);
               list_of_stmt.insert(it_los, tem_ga);
               tem = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            }
            else
               tem = op0;
            if(accum != COST0)
            {
               accum = IRman->create_binary_operation(type, accum, tem, srcp_default, plus_expr_K);
               tem_ga = create_ga(IRman, Scpe, type, accum, bb_index);
               list_of_stmt.insert(it_los, tem_ga);
               accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            }
            else
               accum = tem;
            val_so_far += static_cast<long long int>(1) << log;
            break;

         case alg_sub_t_m2:
            if(log_node != COST0)
            {
               tem = IRman->create_binary_operation(type, op0, log_node, srcp_default, lshift_expr_K);
               tem_ga = create_ga(IRman, Scpe, type, tem, bb_index);
               list_of_stmt.insert(it_los, tem_ga);
               tem = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            }
            else
               tem = op0;
            if(accum != COST0)
               accum = IRman->create_binary_operation(type, accum, tem, srcp_default, minus_expr_K);
            else
               accum = IRman->create_unary_operation(type, tem, srcp_default, negate_expr_K);
            tem_ga = create_ga(IRman, Scpe, type, accum, bb_index);
            list_of_stmt.insert(it_los, tem_ga);
            accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            val_so_far -= static_cast<long long int>(1) << log;
            break;

         case alg_add_t2_m:
            if(log_node != COST0 && accum != COST0)
            {
               accum = IRman->create_binary_operation(type, accum, log_node, srcp_default, lshift_expr_K);
               tem_ga = create_ga(IRman, Scpe, type, accum, bb_index);
               list_of_stmt.insert(it_los, tem_ga);
               accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            }
            if(accum != COST0)
            {
               accum = IRman->create_binary_operation(type, accum, op0, srcp_default, plus_expr_K);
               tem_ga = create_ga(IRman, Scpe, type, accum, bb_index);
               list_of_stmt.insert(it_los, tem_ga);
               accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            }
            else
               accum = op0;
            val_so_far = (val_so_far << log) + 1;
            break;

         case alg_sub_t2_m:
            if(log_node != COST0 && accum != COST0)
            {
               accum = IRman->create_binary_operation(type, accum, log_node, srcp_default, lshift_expr_K);
               tem_ga = create_ga(IRman, Scpe, type, accum, bb_index);
               list_of_stmt.insert(it_los, tem_ga);
               accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            }
            if(accum != COST0)
               accum = IRman->create_binary_operation(type, accum, op0, srcp_default, minus_expr_K);
            else
               accum = IRman->create_unary_operation(type, op0, srcp_default, negate_expr_K);
            tem_ga = create_ga(IRman, Scpe, type, accum, bb_index);
            list_of_stmt.insert(it_los, tem_ga);
            accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            val_so_far = (val_so_far << log) - 1;
            break;

         case alg_add_factor:
            if(log_node != COST0 && accum != COST0)
            {
               tem = IRman->create_binary_operation(type, accum, log_node, srcp_default, lshift_expr_K);
               tem_ga = create_ga(IRman, Scpe, type, tem, bb_index);
               list_of_stmt.insert(it_los, tem_ga);
               tem = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            }
            else
               tem = accum;
            if(accum != COST0)
            {
               accum = IRman->create_binary_operation(type, accum, tem, srcp_default, plus_expr_K);
               tem_ga = create_ga(IRman, Scpe, type, accum, bb_index);
               list_of_stmt.insert(it_los, tem_ga);
               accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            }
            else
               accum = tem;
            val_so_far += val_so_far << log;
            break;

         case alg_sub_factor:
            if(log_node != COST0 && accum != COST0)
            {
               tem = IRman->create_binary_operation(type, accum, log_node, srcp_default, lshift_expr_K);
               tem_ga = create_ga(IRman, Scpe, type, tem, bb_index);
               list_of_stmt.insert(it_los, tem_ga);
               tem = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            }
            else
               tem = accum;
            if(accum != COST0)
            {
               accum = IRman->create_binary_operation(type, tem, accum, srcp_default, minus_expr_K);
               tem_ga = create_ga(IRman, Scpe, type, accum, bb_index);
               list_of_stmt.insert(it_los, tem_ga);
               accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
            }
            else
               accum = tem;
            val_so_far = (val_so_far << log) - val_so_far;
            break;
         case alg_impossible:
         case alg_m:
         case alg_unknown:
         case alg_zero:
         default:
            THROW_UNREACHABLE("");
      }
   }

   if (variant == negate_variant)
   {
      val_so_far = -val_so_far;
      tem = IRman->create_unary_operation(type, accum, srcp_default, negate_expr_K);
      tem_ga = create_ga(IRman, Scpe, type, tem, bb_index);
      list_of_stmt.insert(it_los, tem_ga);
      accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
   }
   else if (variant == add_variant)
   {
      val_so_far = val_so_far + 1;
      tem = IRman->create_binary_operation(type, accum, op0, srcp_default, plus_expr_K);
      tem_ga = create_ga(IRman, Scpe, type, tem, bb_index);
      list_of_stmt.insert(it_los, tem_ga);
      accum = GetPointer<gimple_assign>(GET_NODE(tem_ga))->op0;
   }

   /** Compare only the bits of val and val_so_far that are significant
      in the result mode, to avoid sign-/zero-extension confusion.  */
   val = val & data_mask;
   val_so_far = val_so_far & static_cast<long long int>(data_mask);
   THROW_ASSERT (val == static_cast<unsigned long long int>(val_so_far), "unexpected difference");

   return accum;

}

/// Expand signed modulus of OP0 by a power of two D in mode MODE.

static tree_nodeRef
expand_smod_pow2 (tree_nodeRef op0, unsigned long long int  d, const std::list<tree_nodeRef>::iterator &it_los, std::list<tree_nodeRef> & list_of_stmt, tree_manipulationRef &IRman, tree_nodeRef &Scpe, tree_nodeRef &type, tree_managerRef &TM, unsigned int bb_index)
{
  unsigned long long int  masklow;
  int logd;

  function_decl * fd = GetPointer<function_decl>(GET_NODE(Scpe));
  std::string srcp_default = fd->include_name +":0:0";


  logd = floor_log2 (d);
  unsigned int integer_cst0_id = TM->new_tree_node_id();
  tree_nodeRef const0 = IRman->CreateIntegerCst(type, 0, integer_cst0_id);

  tree_nodeRef bt =  IRman->create_boolean_type();
  tree_nodeRef cond_op0 = IRman->create_binary_operation(bt, op0, const0, srcp_default, lt_expr_K);
  tree_nodeRef signmask_ga = create_ga(IRman, Scpe, type, cond_op0, bb_index);
  list_of_stmt.insert(it_los, signmask_ga);
  tree_nodeRef signmask_var = GetPointer<gimple_assign>(GET_NODE(signmask_ga))->op0;

  masklow = (static_cast<unsigned long long int>(1) << logd) - 1;
  unsigned int integer_masklow_id = TM->new_tree_node_id();
  tree_nodeRef Constmasklow = IRman->CreateIntegerCst(type, static_cast<long long int>(masklow), integer_masklow_id);

  tree_nodeRef temp = IRman->create_binary_operation(type, op0, signmask_var, srcp_default, bit_xor_expr_K);
  tree_nodeRef temp_ga = create_ga(IRman, Scpe, type, temp, bb_index);
  list_of_stmt.insert(it_los, temp_ga);
  tree_nodeRef temp_var = GetPointer<gimple_assign>(GET_NODE(temp_ga))->op0;

  temp = IRman->create_binary_operation(type, temp_var, signmask_var, srcp_default, minus_expr_K);
  temp_ga = create_ga(IRman, Scpe, type, temp, bb_index);
  list_of_stmt.insert(it_los, temp_ga);
  temp_var = GetPointer<gimple_assign>(GET_NODE(temp_ga))->op0;

  temp = IRman->create_binary_operation(type, temp_var, Constmasklow, srcp_default, bit_and_expr_K);
  temp_ga = create_ga(IRman, Scpe, type, temp, bb_index);
  list_of_stmt.insert(it_los, temp_ga);
  temp_var = GetPointer<gimple_assign>(GET_NODE(temp_ga))->op0;

  temp = IRman->create_binary_operation(type, temp_var, signmask_var, srcp_default, bit_xor_expr_K);
  temp_ga = create_ga(IRman, Scpe, type, temp, bb_index);
  list_of_stmt.insert(it_los, temp_ga);
  temp_var = GetPointer<gimple_assign>(GET_NODE(temp_ga))->op0;

  return IRman->create_binary_operation(type, temp_var, signmask_var, srcp_default, minus_expr_K);

}

/* Expand signed division of OP0 by a power of two D in mode MODE.
   This routine is only called for positive values of D.  */

static tree_nodeRef
expand_sdiv_pow2 (tree_nodeRef op0, unsigned long long int  d, const std::list<tree_nodeRef>::iterator &it_los, std::list<tree_nodeRef> & list_of_stmt, tree_manipulationRef &IRman, tree_nodeRef &Scpe, tree_nodeRef &type, tree_managerRef &TM, unsigned int bb_index)
{
   function_decl * fd = GetPointer<function_decl>(GET_NODE(Scpe));
   std::string srcp_default = fd->include_name +":0:0";

   int logd;

  logd = floor_log2 (d);
  unsigned int integer_cst0_id = TM->new_tree_node_id();
  tree_nodeRef const0 = IRman->CreateIntegerCst(type, 0, integer_cst0_id);

  tree_nodeRef bt =  IRman->create_boolean_type();
  tree_nodeRef cond_op0 = IRman->create_binary_operation(bt, op0, const0, srcp_default, lt_expr_K);
  tree_nodeRef cond_op0_ga = create_ga(IRman, Scpe, bt, cond_op0, bb_index);
  list_of_stmt.insert(it_los, cond_op0_ga);
  tree_nodeRef cond_op0_ga_var = GetPointer<gimple_assign>(GET_NODE(cond_op0_ga))->op0;
  tree_nodeRef t_ga;
  tree_nodeRef t2_ga;

  if (d == 2)
  {
     unsigned int integer_cst1_id = TM->new_tree_node_id();
     tree_nodeRef const1 = IRman->CreateIntegerCst(type, 1,
                                                   integer_cst1_id);
     tree_nodeRef cond_op = IRman->create_ternary_operation(type, cond_op0_ga_var, const1, const0, srcp_default, cond_expr_K);
     t_ga = create_ga(IRman, Scpe, type, cond_op, bb_index);
     list_of_stmt.insert(it_los, t_ga);
     tree_nodeRef cond_ga_var = GetPointer<gimple_assign>(GET_NODE(t_ga))->op0;

     tree_nodeRef sum_expr = IRman->create_binary_operation(type, op0, cond_ga_var, srcp_default, plus_expr_K);

     t2_ga = create_ga(IRman, Scpe, type, sum_expr, bb_index);

  }
  else
  {
     unsigned int integer_d_m1_id = TM->new_tree_node_id();
     tree_nodeRef d_m1 = IRman->CreateIntegerCst(type, static_cast<long long int>(d-1), integer_d_m1_id);

     tree_nodeRef t_expr = IRman->create_binary_operation(type, op0, d_m1, srcp_default, plus_expr_K);
     t_ga = create_ga(IRman, Scpe, type, t_expr, bb_index);
     list_of_stmt.insert(it_los, t_ga);
     tree_nodeRef t_ga_var = GetPointer<gimple_assign>(GET_NODE(t_ga))->op0;

     tree_nodeRef cond_op = IRman->create_ternary_operation(type, cond_op0_ga_var, t_ga_var, op0, srcp_default, cond_expr_K);

     t2_ga = create_ga(IRman, Scpe, type, cond_op, bb_index);
  }

  list_of_stmt.insert(it_los, t2_ga);

  tree_nodeRef t2_ga_var = GetPointer<gimple_assign>(GET_NODE(t2_ga))->op0;

  unsigned int integer_logd_id = TM->new_tree_node_id();
  tree_nodeRef logdConst = IRman->CreateIntegerCst(type, logd, integer_logd_id);
  return  IRman->create_binary_operation(type, t2_ga_var, logdConst, srcp_default, rshift_expr_K);

}

static tree_nodeRef
expand_MC(tree_nodeRef op0, integer_cst* ic_node, tree_nodeRef old_target,
          const std::list<tree_nodeRef>::iterator &it_los, std::list<tree_nodeRef> & list_of_stmt,
          tree_manipulationRef &IRman, tree_nodeRef &Scpe, tree_nodeRef &type_expr, tree_managerRef &TM, unsigned int bb_index)
{
   function_decl * fd = GetPointer<function_decl>(GET_NODE(Scpe));
   std::string srcp_default = fd->include_name +":0:0";
   long long int ext_op1 = tree_helper::get_integer_cst_value(ic_node);
   short int mult_plus_ration = 3;

   /// very special case op1 == 0
   if(ext_op1 == 0)
   {
      unsigned int node_nid = TM->new_tree_node_id();
      return IRman->CreateIntegerCst(type_expr, 0, node_nid);
   }
   /// very special case op1 == 1
   else if(ext_op1 == 1)
   {
      return op0;
   }
   /// very special case op1 == -1
   else if(ext_op1 == -1)
   {
      return IRman->create_unary_operation(type_expr, op0, srcp_default, negate_expr_K);
   }
   else
   {
      if(ext_op1 < 0)
      {
         struct algorithm alg;
         enum mult_variant variant;
         short int max_cost = mult_plus_ration;//static_cast<short int>(tree_helper::size(TM, tree_helper::get_type_index(TM,GET_INDEX_NODE(type_expr)))/mult_cost_divisor);
         if(choose_mult_variant (type_expr, -ext_op1, alg, variant, max_cost, TM))
         {
            tree_nodeRef temp_expr = expand_mult_const(op0, static_cast<unsigned long long int>(-ext_op1), alg, variant, it_los, list_of_stmt, IRman, Scpe, type_expr, TM, bb_index);
            tree_nodeRef temp_expr_ga = create_ga(IRman, Scpe, type_expr, temp_expr, bb_index);
            list_of_stmt.insert(it_los, temp_expr_ga);
            tree_nodeRef temp_expr_var = GetPointer<gimple_assign>(GET_NODE(temp_expr_ga))->op0;
            return IRman->create_unary_operation(type_expr, temp_expr_var, srcp_default, negate_expr_K);
         }
         else
         {
            /// keep the old assign
            return old_target;
         }
      }
      else
      {
         unsigned long long int coeff = static_cast<unsigned long long int>(ext_op1);
         if(EXACT_POWER_OF_2_OR_ZERO_P (coeff))
         {
            int l_shift = floor_log2 (coeff);
            unsigned int l_shift_nid = TM->new_tree_node_id();
            tree_nodeRef l_shift_node = IRman->CreateIntegerCst(type_expr, static_cast<long long int>(l_shift), l_shift_nid);
            return IRman->create_binary_operation(type_expr, op0, l_shift_node, srcp_default, lshift_expr_K);
         }
         else
         {
            struct algorithm alg;
            enum mult_variant variant;
            short int max_cost = mult_plus_ration;//static_cast<short int>(tree_helper::size(TM, tree_helper::get_type_index(TM,GET_INDEX_NODE(type_expr)))/mult_cost_divisor);

            if(choose_mult_variant (type_expr, static_cast<long long int>(coeff), alg, variant, max_cost, TM))
               return expand_mult_const(op0, coeff, alg, variant, it_los, list_of_stmt, IRman, Scpe, type_expr, TM, bb_index);
            else
               /// keep the old assign
               return old_target;
         }
      }
   }
}

static void
expand_target_mem_ref(target_mem_ref461 * tmr, const std::list<tree_nodeRef>::iterator &it_los, std::list<tree_nodeRef> & list_of_stmt,
                      tree_manipulationRef &IRman, tree_nodeRef &Scpe, tree_managerRef &TM, unsigned int bb_index)
{
   function_decl * fd = GetPointer<function_decl>(GET_NODE(Scpe));
   std::string srcp_default = fd->include_name +":0:0";
   tree_nodeRef accum;
   tree_nodeRef type_sum;
   unsigned int params = 0;

   if(tmr->idx)
      ++params;
   if(tmr->step)
      ++params;
   if(tmr->offset)
      ++params;
   if(tmr->idx2)
      ++params;

   if(params <=1)
      return; /// nothing to optimize

   if(tmr->idx)
   {
      if(tmr->step)
      {
         integer_cst* ic_step_node = GetPointer<integer_cst>(GET_NODE(tmr->step));
         type_sum = ic_step_node->type;
         accum = expand_MC(tmr->idx, ic_step_node, tree_nodeRef(),
                           it_los, list_of_stmt,
                           IRman, Scpe, type_sum, TM, bb_index);
         if(accum)
         {
            tree_nodeRef t_ga = create_ga(IRman, Scpe, type_sum, accum, bb_index);
            list_of_stmt.insert(it_los, t_ga);
            accum = GetPointer<gimple_assign>(GET_NODE(t_ga))->op0;
         }
         else
         {
            tree_nodeRef t_expr = IRman->create_binary_operation(type_sum, tmr->idx, tmr->step, srcp_default, mult_expr_K);
            tree_nodeRef t_ga = create_ga(IRman, Scpe, type_sum, t_expr, bb_index);
            list_of_stmt.insert(it_los, t_ga);
            accum = GetPointer<gimple_assign>(GET_NODE(t_ga))->op0;
         }

         tmr->step = tree_nodeRef();

      }
      else
         accum = tmr->idx;
      tmr->idx = tree_nodeRef();
   }
   if(tmr->offset)
   {
      integer_cst* ic_node = GetPointer<integer_cst>(GET_NODE(tmr->offset));
      long long int ic_value = tree_helper::get_integer_cst_value(ic_node);
      if(ic_value!=0)
      {
         if(!type_sum)
            type_sum = IRman->create_size_type();

         tree_nodeRef casted_offset_ga = create_ga(IRman, Scpe, type_sum, tmr->offset, bb_index);
         list_of_stmt.insert(it_los, casted_offset_ga);
         tree_nodeRef casted_offset_var = GetPointer<gimple_assign>(GET_NODE(casted_offset_ga))->op0;

         if(accum)
         {
            tree_nodeRef t_expr = IRman->create_binary_operation(type_sum, casted_offset_var, accum, srcp_default, plus_expr_K);
            tree_nodeRef t_ga;
            t_ga = create_ga(IRman, Scpe, type_sum, t_expr, bb_index);
            list_of_stmt.insert(it_los, t_ga);
            accum = GetPointer<gimple_assign>(GET_NODE(t_ga))->op0;
         }
         else
            accum = casted_offset_var;
      }
      tmr->offset = tree_nodeRef();
   }

   if(tmr->idx2)
   {
      if(!type_sum)
         type_sum = IRman->create_size_type();
      tree_nodeRef casted_idx2_ga = create_ga(IRman, Scpe, type_sum, tmr->idx2, bb_index);
      list_of_stmt.insert(it_los, casted_idx2_ga);
      tree_nodeRef casted_idx2_var = GetPointer<gimple_assign>(GET_NODE(casted_idx2_ga))->op0;
      if(accum)
      {
         tree_nodeRef t_expr = IRman->create_binary_operation(type_sum, accum, casted_idx2_var, srcp_default, plus_expr_K);
         tree_nodeRef t_ga = create_ga(IRman, Scpe, type_sum, t_expr, bb_index);
         list_of_stmt.insert(it_los, t_ga);
         accum = GetPointer<gimple_assign>(GET_NODE(t_ga))->op0;
      }
      else
         accum = casted_idx2_var;
   }

   tmr->idx2 = accum;
}

IR_lowering::IR_lowering(const ParameterConstRef Param, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, IR_LOWERING, _design_flow_manager, Param),
   restart_check_system_type(false)
{
   debug_level = Param->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > IR_lowering::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BLOCK_FIX, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SWITCH_FIX, SAME_FUNCTION));
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(USE_COUNTING, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BASIC_BLOCKS_CFG_COMPUTATION, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(DETERMINE_MEMORY_ACCESSES, SAME_FUNCTION));

         //relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SHORT_CIRCUIT_TAF, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      {
         if(restart_check_system_type)
               relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(CHECK_SYSTEM_TYPE, SAME_FUNCTION));
         break;
      }
      case(PRECEDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(PHI_OPT, SAME_FUNCTION));
         break;
      }
      default:
         THROW_UNREACHABLE("");
   }
   return relationships;
}


IR_lowering::~IR_lowering()
{

}

void IR_lowering::Exec()
{
   if(debug_level >= DEBUG_LEVEL_VERY_PEDANTIC)
   {
      PrintTreeManager(true);
   }
   restart_check_system_type = false;
   tree_managerRef TM = AppM->get_tree_manager();
   tree_nodeRef tn = TM->get_tree_node_const(function_id);
   tree_nodeRef Scpe = TM->GetTreeReindex(function_id);
   function_decl * fd = GetPointer<function_decl>(tn);
   THROW_ASSERT(fd && fd->body, "Node is not a function or it hasn't a body");
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));
   THROW_ASSERT(sl, "Body is not a statement_list");
   std::map<unsigned int, blocRef>::iterator B_it_end = sl->list_of_bloc.end();
   /// for each basic block B in CFG do > Consider all blocks successively
   for(std::map<unsigned int, blocRef>::iterator B_it = sl->list_of_bloc.begin(); B_it != B_it_end ; ++B_it)
   {
      blocRef B = B_it->second;
      unsigned int B_id = B->number;
      std::list<tree_nodeRef> & list_of_stmt = B->list_of_stmt;
      /// manage capacity
      std::list<tree_nodeRef>::iterator it_los, it_los_end = list_of_stmt.end();
      size_t n_stmts = list_of_stmt.size();

      for(it_los = list_of_stmt.begin(); it_los != it_los_end; ++it_los)
      {
         if(GET_NODE(*it_los)->get_kind() == gimple_assign_K)
         {
            gimple_assign * ga =  GetPointer<gimple_assign>(GET_NODE(*it_los));
            enum kind code0 = GET_NODE(ga->op0)->get_kind();
            enum kind code1 = GET_NODE(ga->op1)->get_kind();

            if(code1 == cond_expr_K)
            {
               cond_expr * ce = GetPointer<cond_expr>(GET_NODE(ga->op1));
               THROW_ASSERT(ce->op1 && ce->op2, "expected three parameters");
               if(GetPointer<binary_expr>(GET_NODE(ce->op0)))
               {
                  tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));
                  binary_expr* be = GetPointer<binary_expr>(GET_NODE(ce->op0));

                  tree_nodeRef new_ga = create_ga(IRman, Scpe, be->type, ce->op0, B_id);
                  tree_nodeRef ssa_vd = GetPointer<gimple_assign>(GET_NODE(new_ga))->op0;

                  ce->op0 = ssa_vd;
                  list_of_stmt.insert(it_los, new_ga);
                  it_los = list_of_stmt.begin();
                  it_los_end = list_of_stmt.end();
               }
            }
            else if(code1 == vec_cond_expr_K)
            {
               vec_cond_expr * vce = GetPointer<vec_cond_expr>(GET_NODE(ga->op1));
               THROW_ASSERT(vce->op1 && vce->op2, "expected three parameters");
               if(GetPointer<binary_expr>(GET_NODE(vce->op0)))
               {
                  tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));
                  binary_expr* be = GetPointer<binary_expr>(GET_NODE(vce->op0));

                  tree_nodeRef new_ga = create_ga(IRman, Scpe, be->type, vce->op0, B_id);
                  tree_nodeRef ssa_vd = GetPointer<gimple_assign>(GET_NODE(new_ga))->op0;

                  vce->op0 = ssa_vd;
                  list_of_stmt.insert(it_los, new_ga);
                  it_los = list_of_stmt.begin();
                  it_los_end = list_of_stmt.end();
               }
            }
            else if(code1 == view_convert_expr_K)
            {
               view_convert_expr * vce = GetPointer<view_convert_expr>(GET_NODE(ga->op1));
               if(GetPointer<cst_node>(GET_NODE(vce->op)))
               {
                  tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));
                  cst_node* cn = GetPointer<cst_node>(GET_NODE(vce->op));

                  tree_nodeRef new_ga = create_ga(IRman, Scpe, cn->type, vce->op, B_id);
                  tree_nodeRef ssa_vd = GetPointer<gimple_assign>(GET_NODE(new_ga))->op0;
                  vce->op = ssa_vd;
                  list_of_stmt.insert(it_los, new_ga);
                  it_los = list_of_stmt.begin();
                  it_los_end = list_of_stmt.end();
               }
            }
            else if(code1 == target_mem_ref461_K)
            {
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "expand target_mem_ref 1 " + STR(GET_INDEX_NODE(ga->op1)));
               tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));
               std::string srcp_default = fd->include_name +":0:0";
               target_mem_ref461 * tmr = GetPointer<target_mem_ref461>(GET_NODE(ga->op1));
               /// expose some part of the target_mem_ref statement
               expand_target_mem_ref(tmr, it_los, list_of_stmt,
                                     IRman, Scpe, TM, B_id);

               /// split the target_mem_ref in a address computation statement and in a load statement
               tree_nodeRef type = tmr->type;

               tree_nodeRef pt = IRman->create_pointer_type(type);
               tree_nodeRef ae = IRman->create_unary_operation(pt,ga->op1, srcp_default, addr_expr_K);
               tree_nodeRef new_ga = create_ga(IRman, Scpe, pt, ae, B_id);
               GetPointer<gimple_assign>(GET_NODE(new_ga))->temporary_address = true;

               tree_nodeRef ssa_vd = GetPointer<gimple_assign>(GET_NODE(new_ga))->op0;

               unsigned int integer_cst0_id = TM->new_tree_node_id();
               tree_nodeRef offset = IRman->CreateIntegerCst(pt, 0, integer_cst0_id);
               tree_nodeRef mr = IRman->create_binary_operation(type, ssa_vd, offset, srcp_default, mem_ref_K);

               ga->op1 = mr;
               list_of_stmt.insert(it_los, new_ga);
               it_los = list_of_stmt.begin();
               it_los_end = list_of_stmt.end();
            }
            else if(code1  == mem_ref_K)
            {
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "expand mem_ref 1 " + STR(GET_INDEX_NODE(ga->op1)));
               mem_ref * MR = GetPointer<mem_ref>(GET_NODE(ga->op1));
               tree_nodeRef op1 = MR->op1;
               long long int op1_val = tree_helper::get_integer_cst_value(GetPointer<integer_cst>(GET_NODE(op1)));
               if(op1_val != 0)
               {
                  tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));
                  std::string srcp_default = fd->include_name +":0:0";
                  tree_nodeRef type = MR->type;

                  tree_nodeRef pt = IRman->create_pointer_type(type);
                  tree_nodeRef ae = IRman->create_unary_operation(pt,ga->op1, srcp_default, addr_expr_K);
                  tree_nodeRef new_ga = create_ga(IRman, Scpe, pt, ae, B_id);
                  GetPointer<gimple_assign>(GET_NODE(new_ga))->temporary_address = true;
                  tree_nodeRef ssa_vd = GetPointer<gimple_assign>(GET_NODE(new_ga))->op0;

                  unsigned int integer_cst0_id = TM->new_tree_node_id();
                  tree_nodeRef offset = IRman->CreateIntegerCst(pt, 0, integer_cst0_id);
                  tree_nodeRef mr = IRman->create_binary_operation(type, ssa_vd, offset, srcp_default, mem_ref_K);

                  ga->op1 = mr;
                  list_of_stmt.insert(it_los, new_ga);
                  it_los = list_of_stmt.begin();
                  it_los_end = list_of_stmt.end();
               }
            }
            else if(code1 == trunc_div_expr_K || code1 == trunc_mod_expr_K || code1 == exact_div_expr_K)
            {
               tree_nodeRef op1 = GetPointer<binary_expr>(GET_NODE(ga->op1))->op1;
               if(GetPointer<cst_node>(GET_NODE(op1)))
               {
                  tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));
                  std::string srcp_default = fd->include_name +":0:0";
                  cst_node* cn = GetPointer<cst_node>(GET_NODE(op1));
                  tree_nodeRef op0 = GetPointer<binary_expr>(GET_NODE(ga->op1))->op0;
                  tree_nodeRef type_expr = GetPointer<binary_expr>(GET_NODE(ga->op1))->type;

                  bool unsignedp = tree_helper::is_unsigned(TM, GET_INDEX_NODE(type_expr));
                  long long int ext_op1 = tree_helper::get_integer_cst_value(static_cast<integer_cst*>(cn));
                  bool rem_flag = code1 == trunc_mod_expr_K;

                  /// very special case op1 == 1
                  if(ext_op1 == 1)
                  {
                     tree_nodeRef new_op1;
                     if(rem_flag)
                     {
                        unsigned int node_nid = TM->new_tree_node_id();
                        new_op1 = IRman->CreateIntegerCst(type_expr, 0, node_nid);
                     }
                     else
                        new_op1 = op0;
                     ga->op1 = new_op1;
                  }
                  else if(!unsignedp && ext_op1 == -1)
                  {
                     tree_nodeRef new_op1;
                     if(rem_flag)
                     {
                        unsigned int node_nid = TM->new_tree_node_id();
                        new_op1 = IRman->CreateIntegerCst(type_expr, 0, node_nid);
                     }
                     else
                     {
                        new_op1 = IRman->create_unary_operation(type_expr, op0, srcp_default, negate_expr_K);
                     }
                     ga->op1 = new_op1;
                  }
                  else
                  {
                     bool op1_is_pow2 = ((EXACT_POWER_OF_2_OR_ZERO_P (ext_op1)
                                            || (! unsignedp && EXACT_POWER_OF_2_OR_ZERO_P (-ext_op1))));
                     //long long int last_div_const = ! rem_flag ? ext_op1 : 0;

                     if (code1 == exact_div_expr_K && op1_is_pow2)
                         code1 = trunc_div_expr_K;


                     if(ext_op1 != 0)
                     {
                        switch (code1)
                        {
                           case trunc_div_expr_K:
                           case trunc_mod_expr_K:
                           {
                              if (unsignedp)
                              {
                                 int pre_shift;
                                 unsigned long long int d = static_cast<unsigned long long int>(ext_op1);
                                 if (EXACT_POWER_OF_2_OR_ZERO_P (d))
                                 {
                                    pre_shift = floor_log2 (d);
                                    tree_nodeRef new_op1;
                                    if (rem_flag)
                                    {

                                       unsigned int node_nid = TM->new_tree_node_id();
                                       tree_nodeRef mask = IRman->CreateIntegerCst(type_expr, (static_cast<long long int>(1) << pre_shift) - 1, node_nid);
                                       new_op1 = IRman->create_binary_operation(type_expr, op0, mask, srcp_default, bit_and_expr_K);
                                    }
                                    else
                                    {
                                       unsigned int node_nid = TM->new_tree_node_id();
                                       tree_nodeRef shift = IRman->CreateIntegerCst(type_expr, pre_shift, node_nid);

                                       new_op1 = IRman->create_binary_operation(type_expr, op0, shift, srcp_default, rshift_expr_K);
                                    }
                                    ga->op1 = new_op1;
                                 }
                              }
                              else
                              {
                                 tree_nodeRef new_op1;
                                 long long int d = ext_op1;
                                 unsigned long long int abs_d;

                                 /* Since d might be INT_MIN, we have to cast to
                                            unsigned long long before negating to avoid
                                            undefined signed overflow.  */
                                 abs_d = (d >= 0
                                              ? static_cast<unsigned long long int>(d)
                                              : static_cast<unsigned long long int>(-d));

                                 /* n rem d = n rem -d */
                                 if (rem_flag && d < 0)
                                 {
                                    d = static_cast<long long int>(abs_d);
                                 }

                                 unsigned int size = tree_helper::size(TM, tree_helper::get_type_index(TM,GET_INDEX_NODE(type_expr)));
                                 if(abs_d == static_cast<unsigned long long int>(1) << (size - 1))
                                 {
                                    tree_nodeRef quotient_expr = IRman->create_binary_operation(type_expr, op0, op1, srcp_default, eq_expr_K);
                                    if(rem_flag)
                                    {
                                       tree_nodeRef quotient_ga = create_ga(IRman, Scpe, type_expr, quotient_expr, B_id);
                                       list_of_stmt.insert(it_los, quotient_ga);
                                       tree_nodeRef quotient_ga_var = GetPointer<gimple_assign>(GET_NODE(quotient_ga))->op0;
                                       tree_nodeRef mul_expr = IRman->create_binary_operation(type_expr, quotient_ga_var, op1, srcp_default, mult_expr_K);
                                       tree_nodeRef mul_ga = create_ga(IRman, Scpe, type_expr, mul_expr, B_id);
                                       list_of_stmt.insert(it_los, mul_ga);
                                       tree_nodeRef mul_ga_var = GetPointer<gimple_assign>(GET_NODE(mul_ga))->op0;
                                       tree_nodeRef sub_expr = IRman->create_binary_operation(type_expr, op0, mul_ga_var, srcp_default, minus_expr_K);
                                       ga->op1 = sub_expr;
                                    }
                                    else
                                       ga->op1 = quotient_expr;

                                 }
                                 else if (EXACT_POWER_OF_2_OR_ZERO_P (abs_d))
                                 {
                                    if (rem_flag)
                                    {
                                       new_op1 = expand_smod_pow2 (op0, abs_d, it_los, list_of_stmt, IRman, Scpe, type_expr, TM, B_id);
                                       ga->op1 = new_op1;
                                    }
                                    else
                                    {
                                       new_op1 = expand_sdiv_pow2 (op0, abs_d, it_los, list_of_stmt, IRman, Scpe, type_expr, TM, B_id);
                                       /// We have computed OP0 / abs(OP1).  If OP1 is negative, negate the quotient.
                                       if (d < 0)
                                       {
                                          tree_nodeRef sdiv_pow2_ga = create_ga(IRman, Scpe, type_expr, new_op1, B_id);
                                          list_of_stmt.insert(it_los, sdiv_pow2_ga);
                                          tree_nodeRef sdiv_pow2_ga_var = GetPointer<gimple_assign>(GET_NODE(sdiv_pow2_ga))->op0;
                                          new_op1 = IRman->create_unary_operation(type_expr, sdiv_pow2_ga_var, srcp_default, negate_expr_K);
                                       }
                                       ga->op1 = new_op1;
                                    }
                                    it_los = list_of_stmt.begin();
                                    it_los_end = list_of_stmt.end();
                                 }
                              }
                              break;
                           }
                           case exact_div_expr_K:
                           {
                              int pre_shift;
                              long long int d = ext_op1;
                              unsigned long long int ml;
                              unsigned int size = tree_helper::size(TM, tree_helper::get_type_index(TM,GET_INDEX_NODE(type_expr)));

                              pre_shift = floor_log2 (static_cast<unsigned long long int>(d & -d));
                              ml = invert_mod2n (static_cast<unsigned long long int>(d >> pre_shift), size);
                              unsigned int pre_shift_nid = TM->new_tree_node_id();
                              tree_nodeRef pre_shift_node = IRman->CreateIntegerCst(type_expr, pre_shift, pre_shift_nid);
                              unsigned int ml_nid = TM->new_tree_node_id();
                              tree_nodeRef ml_node = IRman->CreateIntegerCst(type_expr, static_cast<long long int>(ml), ml_nid);
                              tree_nodeRef t1_expr = IRman->create_binary_operation(type_expr, op0, pre_shift_node, srcp_default, rshift_expr_K);
                              tree_nodeRef t1_ga = create_ga(IRman, Scpe, type_expr, t1_expr, B_id);
                              list_of_stmt.insert(it_los, t1_ga);
                              tree_nodeRef t1_ga_var = GetPointer<gimple_assign>(GET_NODE(t1_ga))->op0;
                              tree_nodeRef quotient_expr = IRman->create_binary_operation(type_expr, t1_ga_var, ml_node, srcp_default, mult_expr_K);

                              /*if(rem_flag)
                              {
                                 tree_nodeRef quotient_ga = create_ga(IRman, Scpe, "Squot", type_expr, quotient_expr);
                                 list_of_stmt.insert(it_los, quotient_ga);
                                 tree_nodeRef quotient_ga_var = GetPointer<gimple_assign>(GET_NODE(quotient_ga))->op0;
                                 tree_nodeRef mul_expr = IRman->create_binary_operation(type_expr, quotient_ga_var, op1, srcp_default, mult_expr_K);
                                 tree_nodeRef mul_ga = create_ga(IRman, Scpe, "SMod_mul", type_expr, mul_expr);
                                 list_of_stmt.insert(it_los, mul_ga);
                                 tree_nodeRef mul_ga_var = GetPointer<gimple_assign>(GET_NODE(mul_ga))->op0;
                                 tree_nodeRef sub_expr = IRman->create_binary_operation(type_expr, op0, mul_ga_var, srcp_default, minus_expr_K);
                                 ga->op1 = sub_expr;
                              }
                              else*/
                                 ga->op1 = quotient_expr;

                              break;
                           }
                           case assert_expr_K:
                           case bit_and_expr_K:
                           case bit_ior_expr_K:
                           case bit_xor_expr_K:
                           case catch_expr_K:
                           case ceil_div_expr_K:
                           case ceil_mod_expr_K:
                           case complex_expr_K:
                           case compound_expr_K:
                           case eh_filter_expr_K:
                           case eq_expr_K:
                           case fdesc_expr_K:
                           case floor_div_expr_K:
                           case floor_mod_expr_K:
                           case ge_expr_K:
                           case gt_expr_K:
                           case goto_subroutine_K:
                           case in_expr_K:
                           case init_expr_K:
                           case le_expr_K:
                           case lrotate_expr_K:
                           case lshift_expr_K:
                           case lt_expr_K:
                           case max_expr_K:
                           case mem_ref_K:
                           case min_expr_K:
                           case minus_expr_K:
                           case modify_expr_K:
                           case mult_expr_K:
                           case ne_expr_K:
                           case ordered_expr_K:
                           case plus_expr_K:
                           case pointer_plus_expr_K:
                           case postdecrement_expr_K:
                           case postincrement_expr_K:
                           case predecrement_expr_K:
                           case preincrement_expr_K:
                           case range_expr_K:
                           case rdiv_expr_K:
                           case round_div_expr_K:
                           case round_mod_expr_K:
                           case rrotate_expr_K:
                           case rshift_expr_K:
                           case set_le_expr_K:
                           case truth_and_expr_K:
                           case truth_andif_expr_K:
                           case truth_or_expr_K:
                           case truth_orif_expr_K:
                           case truth_xor_expr_K:
                           case try_catch_expr_K:
                           case try_finally_K:
                           case uneq_expr_K:
                           case ltgt_expr_K:
                           case unge_expr_K:
                           case ungt_expr_K:
                           case unle_expr_K:
                           case unlt_expr_K:
                           case unordered_expr_K:
                           case widen_sum_expr_K:
                           case widen_mult_expr_K:
                           case with_size_expr_K:
                           case vec_lshift_expr_K:
                           case vec_rshift_expr_K:
                           case widen_mult_hi_expr_K:
                           case widen_mult_lo_expr_K:
                           case vec_pack_trunc_expr_K:
                           case vec_pack_sat_expr_K:
                           case vec_pack_fix_trunc_expr_K:
                           case vec_extracteven_expr_K:
                           case vec_extractodd_expr_K:
                           case vec_interleavehigh_expr_K:
                           case vec_interleavelow_expr_K:
                           case binfo_K:
                           case block_K:
                           case call_expr_K:
                           case case_label_expr_K:
                           case constructor_K:
                           case identifier_node_K:
                           case ssa_name_K:
                           case statement_list_K:
                           case target_mem_ref_K:
                           case target_mem_ref461_K:
                           case tree_list_K:
                           case tree_vec_K:
                           case CASE_CPP_NODES:
                           case CASE_CST_NODES:
                           case CASE_DECL_NODES:
                           case CASE_FAKE_NODES:
                           case CASE_GIMPLE_NODES:
                           case CASE_PRAGMA_NODES:
                           case CASE_QUATERNARY_EXPRESSION:
                           case CASE_TERNARY_EXPRESSION:
                           case CASE_TYPE_NODES:
                           case CASE_UNARY_EXPRESSION:
                           default:
                           {
                              THROW_ERROR("not yet supported code: " + GET_NODE(ga->op1)->get_kind_text());
                              break;
                           }
                        }
                     }
                  }
               }
            }
            else if( code1 == mult_expr_K)
            {
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "expand mult_expr  " + STR(GET_INDEX_NODE(ga->op1)));
               tree_nodeRef op1 = GetPointer<binary_expr>(GET_NODE(ga->op1))->op1;
               if(GetPointer<cst_node>(GET_NODE(op1)) && !GetPointer<vector_cst>(GET_NODE(op1)))
               {
                  tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));
                  cst_node* cn = GetPointer<cst_node>(GET_NODE(op1));
                  tree_nodeRef op0 = GetPointer<binary_expr>(GET_NODE(ga->op1))->op0;
                  tree_nodeRef type_expr = GetPointer<binary_expr>(GET_NODE(ga->op1))->type;

                  bool realp = tree_helper::is_real(TM, GET_INDEX_NODE(type_expr));
                  if(!realp)
                  {
                     ga->op1 = expand_MC(op0, static_cast<integer_cst*>(cn), ga->op1,
                               it_los, list_of_stmt,
                               IRman, Scpe, type_expr, TM, B_id);
                  }
               }
            }
            else if(code1 == var_decl_K)
            {
               /// check if volatile attribute is missing
               /// it may happen with gcc v4.8 or greater
               var_decl *vd = GetPointer<var_decl>(GET_NODE(ga->op1));
               if(vd->scpe && GET_NODE(vd->scpe)->get_kind() != translation_unit_decl_K && !vd->static_flag && !tree_helper::is_volatile(TM,GET_INDEX_NODE(ga->op1)) &&
                     GET_NODE(vd->type)->get_kind() != array_type_K && GET_NODE(vd->type)->get_kind() != complex_type_K && GET_NODE(vd->type)->get_kind() != record_type_K && GET_NODE(vd->type)->get_kind() != union_type_K)
               {
                  /// w.r.t. bambu this kind of non-ssa variable is equivalent to a static local variable
                  vd->static_flag = true;
               }
            }


            if(code0 == target_mem_ref461_K)
            {
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "expand target_mem_ref 2 " + STR(GET_INDEX_NODE(ga->op0)));
               tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));
               std::string srcp_default = fd->include_name +":0:0";

               target_mem_ref461 * tmr = GetPointer<target_mem_ref461>(GET_NODE(ga->op0));

               /// expose some part of the target_mem_ref statement
               expand_target_mem_ref(tmr, it_los, list_of_stmt,
                                     IRman, Scpe, TM, B_id);

               /// split the target_mem_ref in a address computation statement and in a store statement
               tree_nodeRef type = tmr->type;

               tree_nodeRef pt = IRman->create_pointer_type(type);
               tree_nodeRef ae = IRman->create_unary_operation(pt,ga->op0, srcp_default, addr_expr_K);
               tree_nodeRef new_ga = create_ga(IRman, Scpe, pt, ae, B_id);
               GetPointer<gimple_assign>(GET_NODE(new_ga))->temporary_address = true;
               tree_nodeRef ssa_vd = GetPointer<gimple_assign>(GET_NODE(new_ga))->op0;

               unsigned int integer_cst0_id = TM->new_tree_node_id();
               tree_nodeRef offset = IRman->CreateIntegerCst(pt, 0, integer_cst0_id);
               tree_nodeRef mr = IRman->create_binary_operation(type, ssa_vd, offset, srcp_default, mem_ref_K);

               ga->op0 = mr;
               list_of_stmt.insert(it_los, new_ga);
               it_los = list_of_stmt.begin();
               it_los_end = list_of_stmt.end();
            }
            else if(code0  == mem_ref_K)
            {
               PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "expand mem_ref 2 " + STR(GET_INDEX_NODE(ga->op0)));
               mem_ref * MR = GetPointer<mem_ref>(GET_NODE(ga->op0));

               tree_nodeRef op1 = MR->op1;
               long long int op1_val = tree_helper::get_integer_cst_value(GetPointer<integer_cst>(GET_NODE(op1)));
               if(op1_val != 0)
               {
                  tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));
                  std::string srcp_default = fd->include_name +":0:0";
                  tree_nodeRef type = MR->type;

                  tree_nodeRef pt = IRman->create_pointer_type(type);
                  tree_nodeRef ae = IRman->create_unary_operation(pt,ga->op0, srcp_default, addr_expr_K);
                  tree_nodeRef new_ga = create_ga(IRman, Scpe, pt, ae, B_id);
                  GetPointer<gimple_assign>(GET_NODE(new_ga))->temporary_address = true;
                  tree_nodeRef ssa_vd = GetPointer<gimple_assign>(GET_NODE(new_ga))->op0;

                  unsigned int integer_cst0_id = TM->new_tree_node_id();
                  tree_nodeRef offset = IRman->CreateIntegerCst(pt, 0, integer_cst0_id);
                  tree_nodeRef mr = IRman->create_binary_operation(type, ssa_vd, offset, srcp_default, mem_ref_K);

                  ga->op0 = mr;
                  list_of_stmt.insert(it_los, new_ga);
                  it_los = list_of_stmt.begin();
                  it_los_end = list_of_stmt.end();
               }
            }
            else if(code0 == var_decl_K)
            {
               /// check if volatile attribute is missing
               /// it may happen with gcc v4.8 or greater
               var_decl *vd = GetPointer<var_decl>(GET_NODE(ga->op0));
               if(vd->scpe && GET_NODE(vd->scpe)->get_kind() != translation_unit_decl_K && !vd->static_flag && !tree_helper::is_volatile(TM,GET_INDEX_NODE(ga->op0)) &&
                     GET_NODE(vd->type)->get_kind() != array_type_K && GET_NODE(vd->type)->get_kind() != complex_type_K && GET_NODE(vd->type)->get_kind() != record_type_K && GET_NODE(vd->type)->get_kind() != union_type_K)
               {
                  /// w.r.t. bambu this kind of non-ssa variable is equivalent to a static local variable
                  vd->static_flag = true;
               }
            }
         }
      }

      if(n_stmts != list_of_stmt.size())
         restart_check_system_type = true;
   }
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(false);
   }
}

