/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file determine_memory_accesses.cpp
 * @brief Class to determine the variable to be stored in memory
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "determine_memory_accesses.hpp"
#include "config_HAVE_PRAGMA_BUILT.hpp"

///Behavior include
#include "application_manager.hpp"
#include "call_graph.hpp"
#include "call_graph_manager.hpp"
#include "function_behavior.hpp"

///Parameter include
#include "Parameter.hpp"

///Tree include
#include "behavioral_helper.hpp"
#include "tree_basic_block.hpp"
#include "tree_helper.hpp"
#include "tree_manager.hpp"
#include "ext_tree_node.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"

#include "technology_builtin.hpp"

determine_memory_accesses::determine_memory_accesses(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager):
   FunctionFrontendFlowStep(_AppM, _function_id, DETERMINE_MEMORY_ACCESSES, _design_flow_manager, _parameters),
   behavioral_helper(function_behavior->CGetBehavioralHelper()),
   TM(_AppM->get_tree_manager())
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

determine_memory_accesses::~determine_memory_accesses()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > determine_memory_accesses::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(DETERMINE_MEMORY_ACCESSES, CALLED_FUNCTIONS));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(VAR_DECL_FIX, SAME_FUNCTION));
#if HAVE_PRAGMA_BUILT
         if(parameters->isOption(OPT_parse_pragma) && parameters->getOption<bool>(OPT_parse_pragma))
            relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(PRAGMA_ANALYSIS, SAME_FUNCTION));
#endif
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      case(POST_DEPENDENCE_RELATIONSHIP) :
      {
         break;
      }
      case(PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void determine_memory_accesses::Exec()
{
   tree_nodeRef tn = TM->get_tree_node_const(function_id);
   function_decl * fd = GetPointer<function_decl>(tn);
   if (!fd || !fd->body)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Node is not a function or it hasn't a body");
      return;
   }

   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }

   /// analyze formal parameters
   std::vector <tree_nodeRef>::const_iterator formal_it_end = fd->list_of_args.end();
   for(std::vector <tree_nodeRef>::const_iterator formal_it = fd->list_of_args.begin(); formal_it != formal_it_end; ++formal_it)
      analyze_node(GET_INDEX_NODE(*formal_it), false, false, false);

   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));
   THROW_ASSERT(sl, "Body is not a statement_list");
   std::map<unsigned int, blocRef>::iterator it_bb, it_bb_end = sl->list_of_bloc.end();
   for(it_bb = sl->list_of_bloc.begin(); it_bb != it_bb_end ; it_bb++)
   {
      if (it_bb->second->number == BB_ENTRY || it_bb->second->number == BB_EXIT) continue;
      const std::vector<tree_nodeRef>& list_of_phi = it_bb->second->list_of_phi;
      for(unsigned int s = 0; s < list_of_phi.size(); s++)
      {
         analyze_node(GET_INDEX_NODE(list_of_phi[s]), false, false, false);
      }
      const std::list<tree_nodeRef>& list_of_stmt = it_bb->second->list_of_stmt;
      std::list<tree_nodeRef>::const_iterator stm_it, stm_it_end = list_of_stmt.end();
      for(stm_it = list_of_stmt.begin(); stm_it != stm_it_end; ++stm_it)
      {
         analyze_node(GET_INDEX_NODE(*stm_it), false, false, false);
      }
   }
   const std::list<unsigned int>& fun_parameters = behavioral_helper->get_parameters();
   const std::set<unsigned int>& pdc = function_behavior->get_parm_decl_copied();
   const std::set<unsigned int>& pds = function_behavior->get_parm_decl_stored();
   for(std::list<unsigned int>::const_iterator p = fun_parameters.begin(); p != fun_parameters.end(); p++)
   {
      if(pdc.find(*p) != pdc.end() && pds.find(*p) == pds.end())
      {
         unsigned int memcpy_function_id = TM->function_index("__builtin_memcpy");
         THROW_ASSERT(AppM->GetFunctionBehavior(memcpy_function_id)->GetBehavioralHelper()->has_implementation(), "inconsistent behavioral helper");
         AppM->add_function(function_id, memcpy_function_id, AppM->GetFunctionBehavior(memcpy_function_id)->GetBehavioralHelper(), 0);
         AppM->GetCallGraphManager()->ComputeReachedFunctions();
      }
   }

   if(parameters->getOption<bool>(OPT_print_dot))
   {
      AppM->CGetCallGraphManager()->CGetCallGraph()->WriteDot("call_graph_memory_analysis.dot");
   }
}

void determine_memory_accesses::analyze_node(unsigned int node_id, bool left_p, bool dynamic_address, bool no_dynamic_address)
{
   const tree_nodeRef tn = TM->get_tree_node_const(node_id);
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node " + boost::lexical_cast<std::string>(node_id) + " of type " + tn->get_kind_text());
   std::string function_name = behavioral_helper->get_function_name();

   if(GetPointer<gimple_node>(tn))
   {
      gimple_node* gn = GetPointer<gimple_node>(tn);
      if(gn->use_set)
      {
         const std::vector<tree_nodeRef>::const_iterator usv_it_end = gn->use_set->variables.end();
         for(std::vector<tree_nodeRef>::const_iterator usv_it = gn->use_set->variables.begin(); usv_it != usv_it_end; ++usv_it)
         {
            analyze_node(GET_INDEX_NODE(*usv_it), false, true, false);
         }
      }
   }

   switch(tn->get_kind())
   {
      case gimple_assign_K:
      {
         gimple_assign* gm = GetPointer<gimple_assign>(tn);
         //std::cerr << "gimple assign " << node_id << " " << tn << std::endl;
         analyze_node(GET_INDEX_NODE(gm->op0), true, false, false);
         if(gm->temporary_address)
         {
            addr_expr * ae = GetPointer<addr_expr>(GET_NODE(gm->op1));
            THROW_ASSERT(ae, "expected addr_expr");
            analyze_node(GET_INDEX_NODE(ae->op), false, false, true);
         }
         else
            analyze_node(GET_INDEX_NODE(gm->op1), false, false, false);

         /// check for implicit memcpy calls
         tree_nodeRef op0 = GET_NODE(gm->op0);
         tree_nodeRef op1 = GET_NODE(gm->op1);
         tree_nodeRef op0_type = tree_helper::get_type_node(op0);
         tree_nodeRef op1_type = tree_helper::get_type_node(op1);
         bool is_a_vector_bitfield = false;
         if(op1->get_kind() == bit_field_ref_K)
         {
            bit_field_ref* bfr = GetPointer<bit_field_ref>(op1);
            if(tree_helper::is_a_vector(TM, GET_INDEX_NODE(bfr->op0)))
            is_a_vector_bitfield = true;
         }

         bool load_candidate = (op1->get_kind() == bit_field_ref_K && !is_a_vector_bitfield) ||op1->get_kind() == component_ref_K || op1->get_kind() == indirect_ref_K || op1->get_kind() == misaligned_indirect_ref_K || op1->get_kind() == mem_ref_K || op1->get_kind() == array_ref_K || op1->get_kind() == target_mem_ref_K || op1->get_kind() == target_mem_ref461_K || op1->get_kind() == realpart_expr_K || op1->get_kind() == imagpart_expr_K;
         bool store_candidate = op0->get_kind() == bit_field_ref_K || op0->get_kind() == component_ref_K || op0->get_kind() == indirect_ref_K || op0->get_kind() == misaligned_indirect_ref_K || op0->get_kind() == mem_ref_K || op0->get_kind() == array_ref_K || op0->get_kind() == target_mem_ref_K || op0->get_kind() == target_mem_ref461_K || op0->get_kind() == realpart_expr_K || op0->get_kind() == imagpart_expr_K;
         if(!gm->clobber && op0_type && op1_type &&
               ((op0_type->get_kind()== record_type_K && op1_type->get_kind()== record_type_K && op1->get_kind() != view_convert_expr_K) ||
                (op0_type->get_kind()== union_type_K && op1_type->get_kind()== union_type_K && op1->get_kind() != view_convert_expr_K) ||
                (op0_type->get_kind()== complex_type_K && op1_type->get_kind()== complex_type_K && op1->get_kind() != view_convert_expr_K) ||
                (op0_type->get_kind() == array_type_K) ||
                (function_behavior->is_variable_mem(GET_INDEX_NODE(gm->op0)) && function_behavior->is_variable_mem(GET_INDEX_NODE(gm->op1))) ||
                (function_behavior->is_variable_mem(GET_INDEX_NODE(gm->op0)) && load_candidate) ||
                (store_candidate && function_behavior->is_variable_mem(GET_INDEX_NODE(gm->op1)))
                )
               )
         {
            if(op0->get_kind() == mem_ref_K)
            {
               mem_ref * mr = GetPointer<mem_ref>(op0);
               analyze_node(GET_INDEX_NODE(mr->op0), true, true, false);
            }
            else if(op0->get_kind() == target_mem_ref461_K)
            {
               target_mem_ref461 * tmr = GetPointer<target_mem_ref461>(op0);
               if(tmr->base)
                  analyze_node(GET_INDEX_NODE(tmr->base), true, true, false);
               else
                  analyze_node(GET_INDEX_NODE(gm->op0), true, true, false);
            }
            else
               analyze_node(GET_INDEX_NODE(gm->op0), true, true, false);

            if(op1->get_kind() == mem_ref_K)
            {
               mem_ref * mr = GetPointer<mem_ref>(op1);
               analyze_node(GET_INDEX_NODE(mr->op0), true, true, false);
            }
            else if(op1->get_kind() == target_mem_ref461_K)
            {
               target_mem_ref461 * tmr = GetPointer<target_mem_ref461>(op1);
               if(tmr->base)
                  analyze_node(GET_INDEX_NODE(tmr->base), true, true, false);
               else
                  analyze_node(GET_INDEX_NODE(gm->op1), true, true, false);
            }
            else
               analyze_node(GET_INDEX_NODE(gm->op1), false, true, false);
            //std::cerr << "add memcpy" << std::endl;
            unsigned int memcpy_function_id = TM->function_index("__builtin_memcpy");
            THROW_ASSERT(AppM->GetFunctionBehavior(memcpy_function_id)->GetBehavioralHelper()->has_implementation(), "inconsistent behavioral helper");
            AppM->add_function(function_id, memcpy_function_id, AppM->GetFunctionBehavior(memcpy_function_id)->GetBehavioralHelper(), node_id);
            AppM->GetCallGraphManager()->ComputeReachedFunctions();
         }
         break;
      }
      case CASE_UNARY_EXPRESSION:
      {
         unary_expr* ue = GetPointer<unary_expr>(tn);
         if (GetPointer<addr_expr>(tn))
         {
            if (GetPointer<var_decl>(GET_NODE(ue->op)))
            {
               function_behavior->add_function_mem(GET_INDEX_NODE(ue->op));
               if(!no_dynamic_address)
                  function_behavior->add_dynamic_address(GET_INDEX_NODE(ue->op));
               if(left_p)
                  function_behavior->add_written_object(GET_INDEX_NODE(ue->op));
               if((GetPointer<var_decl>(GET_NODE(ue->op)))->init)
                   analyze_node(GET_INDEX_NODE((GetPointer<var_decl>(GET_NODE(ue->op)))->init), left_p, false, false);
            }
            else if (GetPointer<parm_decl>(GET_NODE(ue->op)))
            {
               function_behavior->add_function_mem(GET_INDEX_NODE(ue->op));
               if(!no_dynamic_address)
                  function_behavior->add_dynamic_address(GET_INDEX_NODE(ue->op));
               /// an address of a parm decl may be used in writing so it has to be copied
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node: formal parameter copied " + STR(GET_INDEX_NODE(ue->op)));
               function_behavior->add_parm_decl_copied(GET_INDEX_NODE(ue->op));
            }
            else if (GetPointer<string_cst>(GET_NODE(ue->op)))
            {
               function_behavior->add_function_mem(GET_INDEX_NODE(ue->op));
               if(!no_dynamic_address)
                  function_behavior->add_dynamic_address(GET_INDEX_NODE(ue->op));
            }
            else if(GetPointer<component_ref>(GET_NODE(ue->op)) ||
                    GetPointer<realpart_expr>(GET_NODE(ue->op)) ||
                    GetPointer<imagpart_expr>(GET_NODE(ue->op)) ||
                    GetPointer<array_ref>(GET_NODE(ue->op)))
               analyze_node(GET_INDEX_NODE(ue->op), true, !no_dynamic_address, no_dynamic_address);
            else if(GetPointer<function_decl>(GET_NODE(ue->op)))
               THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "pointers to functions not yet supported " + STR(node_id) + " in function " + function_name);
            else if(GetPointer<mem_ref>(GET_NODE(ue->op)))
            {
               mem_ref* mr = GetPointer<mem_ref>(GET_NODE(ue->op));
               analyze_node(GET_INDEX_NODE(mr->op0), left_p, !no_dynamic_address, no_dynamic_address);
            }
            else if(GetPointer<target_mem_ref461>(GET_NODE(ue->op)))
            {
               target_mem_ref461* tmr = GetPointer<target_mem_ref461>(GET_NODE(ue->op));
               if(tmr->base)
                  analyze_node(GET_INDEX_NODE(tmr->base), left_p, !no_dynamic_address, no_dynamic_address);
               else
                  analyze_node(GET_INDEX_NODE(ue->op), left_p, !no_dynamic_address, no_dynamic_address);
            }
            else
               THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "determine_memory_accesses addressing currently not supported: " + GET_NODE(ue->op)->get_kind_text()+" @"+STR(node_id) + " in function " + function_name);
         }
         else if(GetPointer<view_convert_expr>(tn))
         {
            view_convert_expr* vc = GetPointer<view_convert_expr>(tn);
            analyze_node(GET_INDEX_NODE(vc->op), left_p, dynamic_address, no_dynamic_address);
         }
         else if(GetPointer<indirect_ref>(tn))
         {
            indirect_ref * ir = GetPointer<indirect_ref>(tn);
            if(GetPointer<integer_cst>(GET_NODE(ir->op)))
            {
               function_behavior->set_dereference_unknown_addr(true);
            }
            dynamic_address = false;
            no_dynamic_address = true;
         }
         else
            analyze_node(GET_INDEX_NODE(ue->op), left_p, dynamic_address, no_dynamic_address);
         break;
      }
      case CASE_BINARY_EXPRESSION:
      {
         binary_expr* be = GetPointer<binary_expr>(tn);
         if (GetPointer<mem_ref>(tn))
         {
            mem_ref * mr = GetPointer<mem_ref>(tn);
            if(GetPointer<integer_cst>(GET_NODE(mr->op0)))
            {
               function_behavior->set_dereference_unknown_addr(true);
            }
            dynamic_address = false;
            no_dynamic_address = true;
         }
         analyze_node(GET_INDEX_NODE(be->op0), left_p, dynamic_address, no_dynamic_address);
         analyze_node(GET_INDEX_NODE(be->op1), left_p, dynamic_address, no_dynamic_address);
         break;
      }
      case gimple_cond_K:
      {
         gimple_cond* gc = GetPointer<gimple_cond>(tn);
         analyze_node(GET_INDEX_NODE(gc->op0), left_p, dynamic_address, no_dynamic_address);
         break;
      }
      case gimple_switch_K:
      {
         gimple_switch* se = GetPointer<gimple_switch>(tn);
         if (se->op0) analyze_node(GET_INDEX_NODE(se->op0), left_p, dynamic_address, no_dynamic_address);
         break;
      }
      case gimple_multi_way_if_K:
      {
         gimple_multi_way_if* gmwi=GetPointer<gimple_multi_way_if>(tn);
         const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it_end = gmwi->list_of_cond.end();
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it = gmwi->list_of_cond.begin(); gmwi_it != gmwi_it_end; ++gmwi_it)
            if((*gmwi_it).first)
               analyze_node(GET_INDEX_NODE((*gmwi_it).first), left_p, dynamic_address, no_dynamic_address);
         break;
      }
      case gimple_phi_K:
      {
         gimple_phi * gp = GetPointer<gimple_phi>(tn);
         size_t n_sources = gp->list_of_def_edge.size();
         for(unsigned int i = 0; i < n_sources; ++i)
            analyze_node(GET_INDEX_NODE(gp->list_of_def_edge[i].first), left_p, dynamic_address, no_dynamic_address);
         break;
      }
      case CASE_TERNARY_EXPRESSION:
      {
         ternary_expr* te = GetPointer<ternary_expr>(tn);
         if(GetPointer<component_ref>(tn))
            left_p = true;
         if (te->op0) analyze_node(GET_INDEX_NODE(te->op0), left_p, dynamic_address, no_dynamic_address);
         if (te->op1) analyze_node(GET_INDEX_NODE(te->op1), left_p, dynamic_address, no_dynamic_address);
         if (te->op2) analyze_node(GET_INDEX_NODE(te->op2), left_p, dynamic_address, no_dynamic_address);
         break;
      }
      case CASE_QUATERNARY_EXPRESSION:
      {
         quaternary_expr* qe = GetPointer<quaternary_expr>(tn);
         if (qe->op0) analyze_node(GET_INDEX_NODE(qe->op0), left_p, dynamic_address, no_dynamic_address);
         if (qe->op1) analyze_node(GET_INDEX_NODE(qe->op1), left_p, dynamic_address, no_dynamic_address);
         if (qe->op2) analyze_node(GET_INDEX_NODE(qe->op2), left_p, dynamic_address, no_dynamic_address);
         if (qe->op3) analyze_node(GET_INDEX_NODE(qe->op3), left_p, dynamic_address, no_dynamic_address);
         break;
      }
      case gimple_return_K:
      {
         gimple_return* re = GetPointer<gimple_return>(tn);
         if (re->op)
         {
            tree_nodeRef res = GET_NODE(re->op);
            tree_nodeRef res_type = tree_helper::get_type_node(res);
            if(res_type->get_kind() == record_type_K || //records have to be allocated
               res_type->get_kind() == union_type_K || // unions have to be allocated
               res_type->get_kind() == complex_type_K //complexes are like structs and so they are allocated
               )
            {
               THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "structs, unions or complex returned by copy are not yet supported: @" + STR(node_id) + " in function " + function_name);
               function_behavior->add_function_mem(node_id);
               function_behavior->add_parm_decl_copied(node_id);
            }
            analyze_node(GET_INDEX_NODE(re->op), left_p, dynamic_address, no_dynamic_address);
         }
         break;
      }
      case call_expr_K:
      {
         call_expr* ce = GetPointer<call_expr>(tn);
         std::vector<tree_nodeRef> & args = ce->args;
         addr_expr* ae = GetPointer<addr_expr>(GET_NODE(ce->fn));
         if(!ae)
            THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "pointers to functions not yet supported in function " + function_name);
         function_decl* fd = GetPointer<function_decl>(GET_NODE(ae->op));
         bool is_var_args_p = GetPointer<function_type>(GET_NODE(fd->type))->varargs_flag;
         THROW_ASSERT(fd, "expected a function_decl");
         std::vector<tree_nodeRef>::iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; ++arg)
         {
            analyze_node(GET_INDEX_NODE(*arg), left_p, dynamic_address, no_dynamic_address);
         }
         if(!fd->undefined_flag)
         {
            std::vector <tree_nodeRef>::const_iterator formal_it = fd->list_of_args.begin();
            std::vector <tree_nodeRef>::const_iterator formal_it_end = fd->list_of_args.end();
            if(!(is_var_args_p || fd->list_of_args.size() == args.size()))
                  THROW_ERROR("In function " + function_name + " a different number of formal and actual parameters is found when function " + tree_helper::print_function_name(TM, fd) + " is called: " + STR(fd->list_of_args.size()) + " - " +STR(args.size()) + "\n Check the C source code since an actual parameter is passed to a function that does have the associated formal parameter");
            for(arg = args.begin(); arg != arg_end && formal_it != formal_it_end; ++arg, ++formal_it)
            {
               unsigned int actual_par_index = GET_INDEX_NODE(*arg);
               unsigned int formal_par_index = GET_INDEX_NODE(*formal_it);
               unsigned int calledFundID = GET_INDEX_NODE(ae->op);
               const FunctionBehaviorRef FBcalled = AppM->GetFunctionBehavior(calledFundID);
               /// check if the actual parameter has been allocated in memory
               if(function_behavior->is_variable_mem(actual_par_index))
               {
                  function_behavior->add_dynamic_address(actual_par_index);
                  /// if the formal parameter has not been allocated in memory then it has to be initialized
                  if(!FBcalled->is_variable_mem(formal_par_index))
                  {
                     PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node: actual parameter loaded " + STR(actual_par_index));
                     function_behavior->add_parm_decl_loaded(actual_par_index);
                  }
               }
               /// check if the formal parameter has been allocated in memory.
               if(FBcalled->is_variable_mem(formal_par_index))
               {
                  /// If the actual has not been allocated in memory then the formal parameter storage has to be initialized with the actual value with a MEMSTORE_STD
                  tree_nodeRef actual_par = GET_NODE(*arg);
                  switch(actual_par->get_kind())
                  {
                     case ssa_name_K:
                     {
                        if(!function_behavior->is_variable_mem(actual_par_index))
                        {
                           PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node: formal parameter stored " + STR(formal_par_index));
                           FBcalled->add_parm_decl_stored(formal_par_index);
                           FBcalled->add_dynamic_address(formal_par_index);

                        }
                        break;
                     }
                     case real_cst_K:
                     case string_cst_K:
                     case integer_cst_K:
                     case addr_expr_K:
                     {
                        PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node: formal parameter stored " + STR(formal_par_index));
                        FBcalled->add_parm_decl_stored(formal_par_index);
                        FBcalled->add_dynamic_address(formal_par_index);
                        if(actual_par->get_kind() == string_cst_K)
                           function_behavior->add_dynamic_address(actual_par_index);
                        break;
                     }
                     case misaligned_indirect_ref_K:
                     case indirect_ref_K:
                     case array_ref_K:
                     case component_ref_K:
                     {
                        PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node: formal parameter copied " + STR(formal_par_index));
                        FBcalled->add_parm_decl_copied(formal_par_index);
                        FBcalled->add_dynamic_address(formal_par_index);
                        tree_nodeRef arg_op = GET_NODE(*arg);
                        tree_nodeRef arg_op_type = tree_helper::get_type_node(arg_op);
                        if(arg_op_type->get_kind() == record_type_K || //records have to be allocated
                           arg_op_type->get_kind() == union_type_K || // unions have to be allocated
                           arg_op_type->get_kind() == complex_type_K //complexes are like structs and so they are allocated
                           )
                           analyze_node(actual_par_index, left_p, true, false);
                        break;
                     }
                     case binfo_K:
                     case block_K:
                     case call_expr_K:
                     case case_label_expr_K:
                     case constructor_K:
                     case identifier_node_K:
                     case statement_list_K:
                     case target_mem_ref_K:
                     case target_mem_ref461_K:
                     case tree_list_K:
                     case tree_vec_K:
                     case abs_expr_K:
                     case arrow_expr_K:
                     case bit_not_expr_K:
                     case buffer_ref_K:
                     case card_expr_K:
                     case cast_expr_K:
                     case cleanup_point_expr_K:
                     case conj_expr_K:
                     case convert_expr_K:
                     case exit_expr_K:
                     case fix_ceil_expr_K:
                     case fix_floor_expr_K:
                     case fix_round_expr_K:
                     case fix_trunc_expr_K:
                     case float_expr_K:
                     case imagpart_expr_K:
                     case loop_expr_K:
                     case negate_expr_K:
                     case non_lvalue_expr_K:
                     case nop_expr_K:
                     case realpart_expr_K:
                     case reference_expr_K:
                     case reinterpret_cast_expr_K:
                     case sizeof_expr_K:
                     case static_cast_expr_K:
                     case throw_expr_K:
                     case truth_not_expr_K:
                     case unsave_expr_K:
                     case va_arg_expr_K:
                     case view_convert_expr_K:
                     case reduc_max_expr_K:
                     case reduc_min_expr_K:
                     case reduc_plus_expr_K:
                     case vec_unpack_hi_expr_K:
                     case vec_unpack_lo_expr_K:
                     case vec_unpack_float_hi_expr_K:
                     case vec_unpack_float_lo_expr_K:
                     case bit_field_ref_K:
                     case vtable_ref_K:
                     case with_cleanup_expr_K:
                     case obj_type_ref_K:
                     case save_expr_K:
                     case cond_expr_K:
                     case vec_cond_expr_K:
                     case vec_perm_expr_K:
                     case dot_prod_expr_K:
                     case complex_cst_K:
                     case vector_cst_K:
                     case array_range_ref_K:
                     case target_expr_K:
                     case CASE_BINARY_EXPRESSION:
                     case CASE_CPP_NODES:
                     case CASE_DECL_NODES:
                     case CASE_FAKE_NODES:
                     case CASE_GIMPLE_NODES:
                     case CASE_PRAGMA_NODES:
                     case CASE_TYPE_NODES:
                     default:
                     {
                        THROW_ASSERT(function_behavior->is_variable_mem(actual_par_index), "actual parameter non allocated in memory: calling @" + STR(calledFundID) + " actual @" + STR(actual_par_index));
                        break;
                     }
                  }
               }
            }
         }
         break;
      }
      case gimple_call_K:
      {
         gimple_call* ce = GetPointer<gimple_call>(tn);
         std::vector<tree_nodeRef> & args = ce->args;
         addr_expr* ae = GetPointer<addr_expr>(GET_NODE(ce->fn));
         if(!ae)
            THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "pointers to functions not yet supported in function " + function_name);
         function_decl* fd = GetPointer<function_decl>(GET_NODE(ae->op));
         if (tree_helper::print_function_name(TM, fd) == BUILTIN_WAIT_CALL_STD)
         {
            function_behavior->add_function_mem(node_id);
            function_behavior->add_written_object(node_id);
         }

         bool is_var_args_p = GetPointer<function_type>(GET_NODE(fd->type))->varargs_flag;
         THROW_ASSERT(fd, "expected a function_decl");
         std::vector<tree_nodeRef>::iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; ++arg)
         {
            analyze_node(GET_INDEX_NODE(*arg), left_p, dynamic_address, no_dynamic_address);
         }
         if(!fd->undefined_flag)
         {
            std::vector <tree_nodeRef>::const_iterator formal_it = fd->list_of_args.begin();
            std::vector <tree_nodeRef>::const_iterator formal_it_end = fd->list_of_args.end();
            if(!(is_var_args_p || fd->list_of_args.size() == args.size()))
               THROW_ERROR("In function " + function_name + " a different number of formal and actual parameters is found when function " + tree_helper::print_function_name(TM, fd) + " is called: " + STR(fd->list_of_args.size()) + " - " +STR(args.size()) + "\n Check the C source code since an actual parameter is passed to a function that does have the associated formal parameter");
            for(arg = args.begin(); arg != arg_end && formal_it != formal_it_end; ++arg, ++formal_it)
            {
               unsigned int actual_par_index = GET_INDEX_NODE(*arg);
               unsigned int formal_par_index = GET_INDEX_NODE(*formal_it);
               unsigned int calledFundID = GET_INDEX_NODE(ae->op);
               const FunctionBehaviorRef FBcalled = AppM->GetFunctionBehavior(calledFundID);
               /// check if the actual parameter has been allocated in memory
               if(function_behavior->is_variable_mem(actual_par_index))
               {
                  function_behavior->add_dynamic_address(actual_par_index);
                  /// if the formal parameter has not been allocated in memory then it has to be initialized
                  if(!FBcalled->is_variable_mem(formal_par_index))
                  {
                     PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node: actual parameter loaded " + STR(actual_par_index));
                     function_behavior->add_parm_decl_loaded(actual_par_index);
                  }
               }
               /// check if the formal parameter has been allocated in memory.
               if(FBcalled->is_variable_mem(formal_par_index))
               {
                  /// If the actual has not been allocated in memory then the formal parameter storage has to be initialized with the actual value with a MEMSTORE_STD
                  tree_nodeRef actual_par = GET_NODE(*arg);
                  switch(actual_par->get_kind())
                  {
                     case ssa_name_K:
                     {
                        if(!function_behavior->is_variable_mem(actual_par_index))
                        {
                           PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node: formal parameter stored " + STR(formal_par_index));
                           FBcalled->add_parm_decl_stored(formal_par_index);
                           FBcalled->add_dynamic_address(formal_par_index);
                        }
                        break;
                     }
                     case real_cst_K:
                     case string_cst_K:
                     case integer_cst_K:
                     case addr_expr_K:
                     {
                        PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node: formal parameter stored " + STR(formal_par_index));
                        FBcalled->add_parm_decl_stored(formal_par_index);
                        FBcalled->add_dynamic_address(formal_par_index);
                        if(actual_par->get_kind() == string_cst_K)
                           function_behavior->add_dynamic_address(actual_par_index);
                        break;
                     }
                     case misaligned_indirect_ref_K:
                     case indirect_ref_K:
                     case array_ref_K:
                     case component_ref_K:
                     {
                        PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node: formal parameter copied " + STR(formal_par_index));
                        FBcalled->add_parm_decl_copied(formal_par_index);
                        FBcalled->add_dynamic_address(formal_par_index);
                        tree_nodeRef arg_op = GET_NODE(*arg);
                        tree_nodeRef arg_op_type = tree_helper::get_type_node(arg_op);
                        if(arg_op_type->get_kind() == record_type_K || //records have to be allocated
                           arg_op_type->get_kind() == union_type_K || // unions have to be allocated
                           arg_op_type->get_kind() == complex_type_K //complexes are like structs and so they are allocated
                           )
                           analyze_node(actual_par_index, left_p, true, false);
                        break;
                     }
                     case binfo_K:
                     case block_K:
                     case call_expr_K:
                     case case_label_expr_K:
                     case constructor_K:
                     case identifier_node_K:
                     case statement_list_K:
                     case target_mem_ref_K:
                     case target_mem_ref461_K:
                     case tree_list_K:
                     case tree_vec_K:
                     case abs_expr_K:
                     case arrow_expr_K:
                     case bit_not_expr_K:
                     case buffer_ref_K:
                     case card_expr_K:
                     case cast_expr_K:
                     case cleanup_point_expr_K:
                     case conj_expr_K:
                     case convert_expr_K:
                     case exit_expr_K:
                     case fix_ceil_expr_K:
                     case fix_floor_expr_K:
                     case fix_round_expr_K:
                     case fix_trunc_expr_K:
                     case float_expr_K:
                     case imagpart_expr_K:
                     case loop_expr_K:
                     case negate_expr_K:
                     case non_lvalue_expr_K:
                     case nop_expr_K:
                     case realpart_expr_K:
                     case reference_expr_K:
                     case reinterpret_cast_expr_K:
                     case sizeof_expr_K:
                     case static_cast_expr_K:
                     case throw_expr_K:
                     case truth_not_expr_K:
                     case unsave_expr_K:
                     case va_arg_expr_K:
                     case view_convert_expr_K:
                     case reduc_max_expr_K:
                     case reduc_min_expr_K:
                     case reduc_plus_expr_K:
                     case vec_unpack_hi_expr_K:
                     case vec_unpack_lo_expr_K:
                     case vec_unpack_float_hi_expr_K:
                     case vec_unpack_float_lo_expr_K:
                     case bit_field_ref_K:
                     case vtable_ref_K:
                     case with_cleanup_expr_K:
                     case obj_type_ref_K:
                     case save_expr_K:
                     case cond_expr_K:
                     case dot_prod_expr_K:
                     case vec_cond_expr_K:
                     case vec_perm_expr_K:
                     case complex_cst_K:
                     case vector_cst_K:
                     case array_range_ref_K:
                     case target_expr_K:
                     case CASE_BINARY_EXPRESSION:
                     case CASE_CPP_NODES:
                     case CASE_DECL_NODES:
                     case CASE_FAKE_NODES:
                     case CASE_GIMPLE_NODES:
                     case CASE_PRAGMA_NODES:
                     case CASE_TYPE_NODES:
                     default:
                     {
                        THROW_ASSERT(function_behavior->is_variable_mem(actual_par_index), "actual parameter non allocated in memory: calling @" + STR(calledFundID) + " actual @" + STR(actual_par_index));
                        break;
                     }
                  }
                 }
            }
         }
         break;
      }
      case ssa_name_K:
      {
         ssa_name* sn = GetPointer<ssa_name>(tn);
         if(sn->var)
         {
            var_decl *vd = GetPointer<var_decl>(GET_NODE(sn->var));
            if(vd && (GET_NODE(vd->type)->get_kind() == complex_type_K))
            {
               function_behavior->add_function_mem(node_id);
               if(dynamic_address && !no_dynamic_address)
                  function_behavior->add_dynamic_address(node_id);
               if(left_p)
                  function_behavior->add_written_object(node_id);
            }
            parm_decl *pd = GetPointer<parm_decl>(GET_NODE(sn->var));
            if(pd && (GET_NODE(pd->type)->get_kind() == complex_type_K))
            {
               function_behavior->add_function_mem(node_id);
               if(dynamic_address && !no_dynamic_address)
                  function_behavior->add_dynamic_address(node_id);
               if(left_p)
                  function_behavior->add_written_object(node_id);

            }
         }
         break;
      }
      case vector_cst_K:
      case real_cst_K:
      case integer_cst_K:
      case gimple_label_K:
      case label_decl_K:
      {
         break;
      }
      case complex_cst_K:
      case string_cst_K:
      {
         function_behavior->add_function_mem(node_id);
         if(dynamic_address && !no_dynamic_address)
            function_behavior->add_dynamic_address(node_id);
         if(left_p)
            function_behavior->add_written_object(node_id);
         break;
      }
      case parm_decl_K:
      {
         parm_decl *pd = GetPointer<parm_decl>(tn);
         if(GET_NODE(pd->type)->get_kind() == record_type_K || //records have to be allocated
            GET_NODE(pd->type)->get_kind() == union_type_K || // unions have to be allocated
            GET_NODE(pd->type)->get_kind() == complex_type_K //complexes are like structs and so they are allocated
            )
         {
            function_behavior->add_function_mem(node_id);
            function_behavior->add_dynamic_address(node_id);
            if(left_p)
            {
               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Analyzing node: formal parameter copied " + STR(node_id));
               function_behavior->add_parm_decl_copied(node_id);
            }
         }
         break;
      }
      case result_decl_K:
      {
         result_decl *rd = GetPointer<result_decl>(tn);
         if(GET_NODE(rd->type)->get_kind() == record_type_K || //records have to be allocated
            GET_NODE(rd->type)->get_kind() == union_type_K || // unions have to be allocated
            GET_NODE(rd->type)->get_kind() == complex_type_K //complexes are like structs and so they are allocated
            )
         {
            THROW_ERROR_CODE(C_EC, "structs, unions or complex returned by copy are not yet supported: @" + STR(node_id) + " in function " + function_name);
            function_behavior->add_function_mem(node_id);
            function_behavior->add_parm_decl_copied(node_id);
         }
         break;
      }
      case tree_list_K:
      {
         tree_list* tl = GetPointer<tree_list>(tn);
         analyze_node(GET_INDEX_NODE(tl->valu), left_p, dynamic_address, no_dynamic_address);
         if (tl->chan) analyze_node(GET_INDEX_NODE(tl->chan), left_p, dynamic_address, no_dynamic_address);
         break;
      }
      case var_decl_K:
      {
         var_decl *vd = GetPointer<var_decl>(tn);
         if(vd->extern_flag)
            THROW_ERROR_CODE(C_EC, "Extern symbols not yet supported " + behavioral_helper->PrintVariable(node_id));
         if (!vd->scpe || GET_NODE(vd->scpe)->get_kind() == translation_unit_decl_K) //memory has to be allocated in case of global variables
         {
            function_behavior->add_function_mem(node_id);
            if(dynamic_address && !no_dynamic_address)
               function_behavior->add_dynamic_address(node_id);
            if(left_p)
               function_behavior->add_written_object(node_id);
            if (vd->init) analyze_node(GET_INDEX_NODE(vd->init), false, false, false);
         }
         else
         {
            THROW_ASSERT(GET_NODE(vd->scpe)->get_kind() != translation_unit_decl_K, "translation_unit_decl not expected a translation unit in this point @" + STR(node_id));
            if(vd->static_flag || //memory has to be allocated in case of local static variables
                  tree_helper::is_volatile(TM,node_id) || // volatile vars have to be allocated
                  GET_NODE(vd->type)->get_kind() == array_type_K || //arrays have to be allocated
                  GET_NODE(vd->type)->get_kind() == complex_type_K || //complexes are like structs and so they are allocated
                  GET_NODE(vd->type)->get_kind() == record_type_K || //records have to be allocated
                  GET_NODE(vd->type)->get_kind() == union_type_K
                  )
            {
               function_behavior->add_function_mem(node_id);
               if(dynamic_address && !no_dynamic_address)
                  function_behavior->add_dynamic_address(node_id);
               if(left_p)
                  function_behavior->add_written_object(node_id);
            }
            else
            {
               //nothing have to be allocated for the variable
               // maybe something has to be allocated for its initialization
               if (vd->init) analyze_node(GET_INDEX_NODE(vd->init), left_p, false, false);
            }
         }
         break;
      }
      case constructor_K:
      {
         constructor* con = GetPointer<constructor>(tn);
         std::vector<std::pair< tree_nodeRef, tree_nodeRef> > &list_of_idx_valu = con->list_of_idx_valu;
         for(std::vector<std::pair< tree_nodeRef, tree_nodeRef> >::iterator el = list_of_idx_valu.begin(); el != list_of_idx_valu.end(); el++)
         {
            if (el->first) analyze_node(GET_INDEX_NODE(el->first), left_p, dynamic_address, no_dynamic_address);
            if (el->second) analyze_node(GET_INDEX_NODE(el->second), left_p, dynamic_address, no_dynamic_address);
         }
         break;
      }
      case gimple_goto_K:
      {
         gimple_goto* ge = GetPointer<gimple_goto>(tn);
         analyze_node(GET_INDEX_NODE(ge->op), left_p, dynamic_address, no_dynamic_address);
         break;
      }
      case gimple_nop_K:
      case field_decl_K:
      case CASE_PRAGMA_NODES:
      {
         break;
      }
      case target_mem_ref_K:
      {
         target_mem_ref* tmr = GetPointer<target_mem_ref>(tn);
         if(tmr->symbol) analyze_node(GET_INDEX_NODE(tmr->symbol), left_p, false, true);
         if(tmr->base) analyze_node(GET_INDEX_NODE(tmr->base), left_p, false, true);
         if(tmr->idx) analyze_node(GET_INDEX_NODE(tmr->idx), left_p, false, false);
         break;
      }
      case target_mem_ref461_K:
      {
         target_mem_ref461* tmr = GetPointer<target_mem_ref461>(tn);
         if(tmr->base)
         {
            tree_nodeRef operand = GET_NODE(tmr->base);
            if(operand->get_kind() == addr_expr_K)
            {
               /// skip the &
               analyze_node(GET_INDEX_NODE(GetPointer<addr_expr>(operand)->op), left_p, false, true);
            }
            else
               analyze_node(GET_INDEX_NODE(tmr->base), left_p, false, true);
         }
         if(tmr->idx) analyze_node(GET_INDEX_NODE(tmr->idx), left_p, false, false);
         if(tmr->idx2) analyze_node(GET_INDEX_NODE(tmr->idx2), left_p, false, false);
         break;
      }
      case function_decl_K:
      {
         THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "pointers to functions not yet supported in function " + function_name);
         break;
      }
      case gimple_asm_K:
      {
         gimple_asm *ga = GetPointer<gimple_asm>(tn);
         if(ga->in) analyze_node(GET_INDEX_NODE(ga->in), false, false, false);
         if(ga->out) analyze_node(GET_INDEX_NODE(ga->out),  true, false, false);
         break;
      }
      case binfo_K:
      case block_K:
      case case_label_expr_K:
      case const_decl_K:
      case CASE_CPP_NODES:
      case CASE_FAKE_NODES:
      case gimple_for_K:
      case gimple_bind_K:
      case gimple_pragma_K:
      case gimple_predict_K:
      case gimple_resx_K:
      case gimple_while_K:
      case identifier_node_K:
      case namespace_decl_K:
      case statement_list_K:
      case translation_unit_decl_K:
      case tree_vec_K:
      case type_decl_K:
      case CASE_TYPE_NODES:
      {
         THROW_ERROR_CODE(NODE_NOT_YET_SUPPORTED_EC, "Not supported node (@" + STR(node_id) + ") of type " + std::string(tn->get_kind_text()) + " in function " + function_name);
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
}
