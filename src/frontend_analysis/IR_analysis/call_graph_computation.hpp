/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file call_graph_computation.hpp
 * @brief Build call_graph data structure starting from the tree_manager.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef CALL_GRAPH_COMPUTATION_HPP
#define CALL_GRAPH_COMPUTATION_HPP

///Superclass include
#include "application_frontend_flow_step.hpp"

#include "refcount.hpp"
#include "utility.hpp"

#include <map>

/**
 * @name forward declarations
 */
//@{
REF_FORWARD_DECL(BehavioralHelper);
REF_FORWARD_DECL(call_graph_computation);
CONSTREF_FORWARD_DECL(FunctionExpander);
REF_FORWARD_DECL(tree_manager);
REF_FORWARD_DECL(tree_node);
//@}

/**
 * Build call graph structures starting from the tree_manager.
*/
class call_graph_computation : public ApplicationFrontendFlowStep
{
   private:
      ///Index of current function
      unsigned int current;

      ///set of already examined addr_expr used to avoid circular recursion
      std::set<tree_nodeRef> already_visited;

      std::map<unsigned int, std::set<unsigned int> > type_to_declaration;
      std::map<std::pair<unsigned int, unsigned int>, unsigned int> pointercall_to_type;

      ///The expander function
      const FunctionExpanderConstRef function_expander;

      /**
       * call graph computation for functions called indirectly.
       * @param tm is the tree manager.
       * @param function_id is the node id of the inner function.
      */
      void exec_internal(const tree_managerRef &tm, unsigned int function_id, unsigned int node_stmt);

      /**
       * Recursive analysis of the tree nodes looking for call expressions.
       * @param tm is the tree manager.
       * @param tn is current tree node.
      */
      void call_graph_computation_recursive(const tree_managerRef &tm, const tree_nodeRef &tn, unsigned int node_stmt);

      void funpointer_analysis(const tree_nodeRef &tn, unsigned int node_stmt);

      /**
       * Return the set of analyses in relationship with this design step
       * @param relationship_type is the type of relationship to be considered
       */
      const std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const;

   public:
      /**
       * Constructor.
       * @param Param is the set of the parameters
       * @param AppM is the application manager
       * @param design_flow_manager is the design flow manager
       */
      call_graph_computation(const ParameterConstRef Param, const application_managerRef AppM, const DesignFlowManagerConstRef design_flow_manager);

      /**
       *  Destructor
       */
      ~call_graph_computation();

      /**
       * Computes the call graph data structure.
       */
      void Exec();
};
#endif
