/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file array_ref_fix.hpp
 * @brief Transformation of indirect_ref to array structures into corresponding array_ref
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * @author Andrea Cuoccio <andrea.cuoccio@gmail.com>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef ARRAY_REF_FIX_HPP
#define ARRAY_REF_FIX_HPP
///Super class include
#include "function_frontend_flow_step.hpp"

///STL include
#include <unordered_set>

///Utility include
#include "refcount.hpp"

///Forward declaration
class array_ref;
CONSTREF_FORWARD_DECL(Parameter);
CONSTREF_FORWARD_DECL(tree_node);
REF_FORWARD_DECL(tree_node);

class array_ref_fix : public FunctionFrontendFlowStep
{
   private:
      /**
       * Function that performs the pattern analisys that changes pointer_plus into array_ref when base is an addr_expr and type array_type
       * @param node is the node containing one of the side of a gimple_assign containing an indirect_ref
       * @param index is the index of node
       * @param tn is the gimple_assign
       */
      bool pointer_plus_fix(tree_nodeRef& node, unsigned int index, tree_nodeRef & tn);

      /**
       * Function that performs the pattern analisys that changes indirect_ref into array_ref
       * @param node is the node containing one of the side of a gimple_assign containing an indirect_ref
       * @param index is the index of node
       * @param tn is the gimple_assign
      */
      bool array_fix(const tree_nodeConstRef node, unsigned int index, tree_nodeRef & tn);

      /**
       * Try to replace an indirect ref of an addr_expr with the argument of addr_expr
       * @param ir is the field corresponding to ir
       * NOTE: ir must be passed by reference so that replacement is performed directly in the value of the field
       * @return true if the transformation has been performed
       */
      bool IndirectRefFix(tree_nodeRef & ir);

      /**
       * Function that rebuilds the offset of an array ref
       * @param partial_off is the node containing the initial offset
       * @param new_offset is the node containing the offset rebuilt
       * @param size is the size of the initial pointer plus.
       * @param div_by_size the variable to decide if it's necessary to divide numbers by size
       */
      bool rebuild_offset(const tree_nodeRef partial_off, tree_nodeRef & new_offset, long long& size, bool div_by_size);

      /**
       * Function that rebuilds the base of an array ref
       * @param pointer_int is the node containing the initial base
       * @param new_pointer is the node containing the base rebuilt
       * @return true if rebuilding is successful
       */
      bool rebuild_base(const tree_nodeRef pointer_int, tree_nodeRef & new_pointer);

      /**
       * Return the set of analyses in relationship with this design step
       * @param relationship_type is the type of relationship to be considered
       */
      const std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const;

   public:

      /**
       * Constructor
       * @param Param is the set of the parameters
       * @param AppM is the application manager
       * @param function_id is the index of the function
       * @param design_flow_manager is the design flow manager
       */
      array_ref_fix(const ParameterConstRef Param, const application_managerRef AppM, unsigned int fun_id, const DesignFlowManagerConstRef design_flow_manager);

      /**
       * Destructor
       */
      ~array_ref_fix();

      /**
       * Transforms the indirect_ref of array structures into the corresponding array_ref.
       */
      void Exec();

};

#endif

