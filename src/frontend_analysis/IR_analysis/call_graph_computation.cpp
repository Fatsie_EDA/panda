   /*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file call_graph_computation.cpp
 * @brief Build call_graph data structure starting from the tree_manager.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "call_graph_computation.hpp"

///Behavior include
#include "application_manager.hpp"
#include "call_graph.hpp"
#include "call_graph_manager.hpp"
#include "function_behavior.hpp"

///Parameter include
#include "Parameter.hpp"

///Tree include
#include "behavioral_helper.hpp"
#include "tree_basic_block.hpp"
#include "tree_manager.hpp"
#include "tree_helper.hpp"
#include "tree_node.hpp"
#include "ext_tree_node.hpp"
#include "tree_reindex.hpp"

///Utility include
#include "dbgPrintHelper.hpp"
#include "exceptions.hpp"

///Wrapper include
#include "gcc_wrapper.hpp"

call_graph_computation::call_graph_computation(const ParameterConstRef _parameters, const application_managerRef _AppM, const DesignFlowManagerConstRef _design_flow_manager) :
   ApplicationFrontendFlowStep(_AppM, FUNCTION_ANALYSIS, _design_flow_manager, _parameters),
   current(0),
   function_expander(_AppM->CGetCallGraphManager()->function_expander)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}


call_graph_computation::~call_graph_computation()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > call_graph_computation::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(CREATE_TREE_MANAGER, WHOLE_APPLICATION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(POST_PRECEDENCE_RELATIONSHIP) :
      case(PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void call_graph_computation::Exec()
{
   INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "-->Creating call graph data structure");
   const CallGraphManagerRef call_graph_manager = AppM->GetCallGraphManager();
   const tree_managerRef TM = AppM->get_tree_manager();
   if (debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }

   const std::unordered_set<unsigned int> functions = TM->GetAllFunctions();
   std::unordered_set<unsigned int>::const_iterator function, function_end = functions.end();
   for(function = functions.begin(); function != function_end; function++)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Analyzing function " + boost::lexical_cast<std::string>(*function));
      if(AppM->is_function(*function))
         continue;
      current = *function;
      exec_internal(TM, *function, 0);
   }

   for (std::map<std::pair<unsigned int, unsigned int>, unsigned int>::iterator i = pointercall_to_type.begin(); i != pointercall_to_type.end(); i++)
   {
      for(std::set<unsigned int>::iterator p = type_to_declaration[i->second].begin(); p != type_to_declaration[i->second].end(); p++)
      {
         BehavioralHelperRef helper;
         AppM->add_function(i->first.first, *p, helper, i->first.second);
      }
   }
   call_graph_manager->ComputeRootFunctions();

   call_graph_manager->ComputeReachedFunctions();

   if(parameters->getOption<bool>(OPT_print_dot))
   {
      AppM->CGetCallGraphManager()->CGetCallGraph()->WriteDot("call_graph.dot");
   }

   already_visited.clear();
   type_to_declaration.clear();
   pointercall_to_type.clear();
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Created call graph");
}

void call_graph_computation::exec_internal(const tree_managerRef&tm, unsigned int function_id, unsigned int node_stmt)
{
   tree_nodeRef tn;

   THROW_ASSERT(function_id > 0, "The function with id " + boost::lexical_cast<std::string>(function_id) + " doesn't exist");

   bool body = true;
   if(tm->get_implementation_node(function_id))
      function_id = tm->get_implementation_node(function_id);
   else
      body = false;
   std::string name = tree_helper::name_function(tm, function_id);
   if (name == "__start_pragma__" || name == "__close_pragma__" || name.find("__pragma__") == 0) return;

   INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "-->Starting the analysis of function " + name);
   //creating helper
   BehavioralHelperRef helper = BehavioralHelperRef(new BehavioralHelper(parameters, tm, function_id, body));
   if(node_stmt)
   {
      AppM->add_function(current, function_id, helper, node_stmt);
   }
   else
      AppM->add_function(function_id, helper);
   current = function_id;
   tree_nodeRef fun = tm->get_tree_node_const(function_id);
   function_decl* fd = GetPointer<function_decl>(fun);
   if(fd->scpe && GET_NODE(fd->scpe)->get_kind() == function_decl_K)
   {
      THROW_ERROR_CODE(NESTED_FUNCTIONS_EC,"Nested functions not yet supported "+boost::lexical_cast<std::string>(function_id));
   }
   const std::vector <tree_nodeRef> &list_of_args = fd->list_of_args;
   if(fd->list_of_args.size())
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Parameter list size: " + boost::lexical_cast<std::string>(list_of_args.size()));
      for(unsigned int i = 0; i < list_of_args.size(); i++)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Adding parameter " + boost::lexical_cast<std::string>(GET_INDEX_NODE(list_of_args[i])));
         helper->add_parameter(GET_INDEX_NODE(list_of_args[i]));
      }
   }
   else
   {
      function_type * ft = GetPointer<function_type>(GET_NODE(fd->type));
      if(ft->prms)
      {
         tree_nodeRef currentp = ft->prms;
         while(currentp)
         {
            tree_list * tl = GetPointer<tree_list>(GET_NODE(currentp));
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Adding parameter " + boost::lexical_cast<std::string>(GET_INDEX_NODE(tl->valu)));
            if(GET_NODE(tl->valu)->get_kind() != void_type_K)
               helper->add_parameter(GET_INDEX_NODE(tl->valu));
            currentp = tl->chan;
         }
      }
   }
   if(body)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "---Implementation node of this function is " + boost::lexical_cast<std::string>(tm->get_implementation_node(function_id)));
      THROW_ASSERT(fd->body, "Expected the function body");
      call_graph_computation_recursive(tm, fd->body, node_stmt);
   }
   else
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "No body");
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "<--Completed recursive analysis of not main function " + name);
}

void call_graph_computation::call_graph_computation_recursive(const tree_managerRef &tm, const tree_nodeRef &tn, unsigned int node_stmt)
{
   THROW_ASSERT(tn->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   const tree_nodeRef &curr_tn = GET_NODE(tn);
   unsigned int ind = GET_INDEX_NODE(tn);
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Recursive analysis of " + boost::lexical_cast<std::string>(ind) + " of type " + curr_tn->get_kind_text() + "(statement is " + boost::lexical_cast<std::string>(node_stmt) + ")");

   switch (curr_tn->get_kind())
   {
      case function_decl_K:
      {
         unsigned int impl = tm->get_implementation_node(ind);
         if(impl)
            ind = impl;
         ///check for nested function
         function_decl* fd = GetPointer<function_decl>(curr_tn);
         type_to_declaration[GET_INDEX_NODE(fd->type)].insert(ind);
         if(fd->scpe && GET_NODE(fd->scpe)->get_kind() == function_decl_K)
         {
            THROW_ERROR_CODE(NESTED_FUNCTIONS_EC,"Nested functions not yet supported "+boost::lexical_cast<std::string>(ind));
            THROW_ERROR("Nested functions not yet supported "+boost::lexical_cast<std::string>(ind));
         }

         //build the behavioral_manager associated with the body if there exist and if it should be expanded
         if((*function_expander)(curr_tn))
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Expanding");
            if(!AppM->is_function(ind))
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---To be analyzed");
               unsigned int fun_id = current;
               exec_internal(tm, ind, node_stmt);
               current = fun_id;
            }
            else
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Already analyzed");
               BehavioralHelperRef helper;
               AppM->add_function(current, ind, helper, node_stmt);
            }
         }
         else
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Not expanding");
            BehavioralHelperRef helper;
            if(!AppM->is_function(ind))
               helper = BehavioralHelperRef(new BehavioralHelper(parameters, tm, ind, false));
            AppM->add_function(current, ind, helper, node_stmt);
         }
         break;
      }
      case statement_list_K:
      {
         statement_list* sl = GetPointer<statement_list>(curr_tn);
         if(sl->list_of_bloc.empty())
         {
            THROW_ERROR("We can only work on CFG provided by GCC");
         }
         else
         {
            std::map<unsigned int, blocRef>::const_iterator b_end = sl->list_of_bloc.end();
            for(std::map<unsigned int, blocRef>::const_iterator b = sl->list_of_bloc.begin(); b != b_end; b++)
            {
               std::list<tree_nodeRef>::iterator s_end = b->second->list_of_stmt.end();
               for(std::list<tree_nodeRef>::iterator s = b->second->list_of_stmt.begin(); s != s_end; s++)
               {
                  call_graph_computation_recursive(tm, *s, GET_INDEX_NODE(*s));
               }
            }
         }
         break;
      }
      case gimple_return_K:
      {
         gimple_return* re = GetPointer<gimple_return>(curr_tn);
         if(re->op)
         {
            call_graph_computation_recursive(tm, re->op, node_stmt);
         }
         break;
      }
      case gimple_assign_K:
      {
         gimple_assign* me = GetPointer<gimple_assign>(curr_tn);

         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Analyzing left part");
         call_graph_computation_recursive(tm, me->op0, node_stmt);
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Analyzed left part - Analyzing right part");
         call_graph_computation_recursive(tm, me->op1, node_stmt);
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Analyzing right part");
         break;
      }
      case gimple_nop_K:
      {
         break;
      }
      case call_expr_K:
      {
         call_expr* ce = GetPointer<call_expr>(curr_tn);
         tree_nodeRef fun_node = GET_NODE(ce->fn);
         if(fun_node->get_kind() == addr_expr_K)
         {
            unary_expr* ue = GetPointer<unary_expr>(fun_node);
            fun_node = ue->op;
         }
         else if(fun_node->get_kind() == obj_type_ref_K)
         {
            fun_node = tree_helper::find_obj_type_ref_function(ce->fn);
         }
         else if(fun_node->get_kind() == ssa_name_K)
         {
            funpointer_analysis(ce->fn, node_stmt);
            fun_node = ce->fn;
         }
         else
            fun_node = ce->fn;

         const std::vector<tree_nodeRef> & args = ce->args;
         std::vector<tree_nodeRef>::const_iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            call_graph_computation_recursive(tm, *arg, node_stmt);
         }
         call_graph_computation_recursive(tm, fun_node, node_stmt);
         break;
      }
      case gimple_call_K:
      {
         gimple_call* ce = GetPointer<gimple_call>(curr_tn);
         tree_nodeRef fun_node = GET_NODE(ce->fn);
         if(fun_node->get_kind() == addr_expr_K)
         {
            unary_expr* ue = GetPointer<unary_expr>(fun_node);
            fun_node = ue->op;
         }
         else if(fun_node->get_kind() == obj_type_ref_K)
         {
            fun_node = tree_helper::find_obj_type_ref_function(ce->fn);
         }
         else if(fun_node->get_kind() == ssa_name_K)
         {
            funpointer_analysis(ce->fn, node_stmt);
            fun_node = ce->fn;
         }
         else
            fun_node = ce->fn;

         const std::vector<tree_nodeRef> & args = ce->args;
         std::vector<tree_nodeRef>::const_iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            call_graph_computation_recursive(tm, *arg, node_stmt);
         }
         call_graph_computation_recursive(tm, fun_node, node_stmt);
         break;
      }
      case cond_expr_K:
      {
         cond_expr* ce = GetPointer<cond_expr>(curr_tn);
         call_graph_computation_recursive(tm, ce->op0, node_stmt);
         call_graph_computation_recursive(tm, ce->op1, node_stmt);
         call_graph_computation_recursive(tm, ce->op2, node_stmt);
         break;
      }
      case gimple_cond_K:
      {
         gimple_cond* gc = GetPointer<gimple_cond>(curr_tn);
         call_graph_computation_recursive(tm, gc->op0, node_stmt);
         break;
      }
      /* Unary expressions.  */
      case CASE_UNARY_EXPRESSION:
      {
         if(curr_tn->get_kind() == addr_expr_K)
         {
            if(already_visited.find(curr_tn) != already_visited.end())
            {
               break;
            }
            already_visited.insert(curr_tn);
         }
         unary_expr * ue = GetPointer<unary_expr>(curr_tn);
         call_graph_computation_recursive(tm, ue->op, node_stmt);
         break;
      }
      case CASE_BINARY_EXPRESSION:
      {
         binary_expr* be = GetPointer<binary_expr>(curr_tn);
         call_graph_computation_recursive(tm, be->op0, node_stmt);
         call_graph_computation_recursive(tm, be->op1, node_stmt);
         break;
      }
      /*ternary expressions*/
      case gimple_switch_K:
      {
         gimple_switch* se = GetPointer<gimple_switch>(curr_tn);
         call_graph_computation_recursive(tm, se->op0, node_stmt);
         break;
      }
      case gimple_multi_way_if_K:
      {
         gimple_multi_way_if* gmwi=GetPointer<gimple_multi_way_if>(curr_tn);
         const std::vector<std::pair< tree_nodeRef, unsigned int> >::const_iterator gmwi_it_end = gmwi->list_of_cond.end();
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::const_iterator gmwi_it = gmwi->list_of_cond.begin(); gmwi_it != gmwi_it_end; ++gmwi_it)
            if((*gmwi_it).first)
               call_graph_computation_recursive(tm, (*gmwi_it).first, node_stmt);
         break;
      }
      case obj_type_ref_K:
      {
         tree_nodeRef fun = tree_helper::find_obj_type_ref_function(tn);
         call_graph_computation_recursive(tm, fun, node_stmt);
         break;
      }
      case save_expr_K:
      case component_ref_K:
      case bit_field_ref_K:
      case vtable_ref_K:
      case with_cleanup_expr_K:
      case vec_cond_expr_K:
      case vec_perm_expr_K:
      case dot_prod_expr_K:
      {
         ternary_expr * te = GetPointer<ternary_expr>(curr_tn);
         call_graph_computation_recursive(tm, te->op0, node_stmt);
         call_graph_computation_recursive(tm, te->op1, node_stmt);
         if(te->op2)
            call_graph_computation_recursive(tm, te->op2, node_stmt);
         break;
      }
      case CASE_QUATERNARY_EXPRESSION:
      {
         quaternary_expr * qe = GetPointer<quaternary_expr>(curr_tn);
         call_graph_computation_recursive(tm, qe->op0, node_stmt);
         call_graph_computation_recursive(tm, qe->op1, node_stmt);
         if(qe->op2)
            call_graph_computation_recursive(tm, qe->op2, node_stmt);
         if(qe->op3)
            call_graph_computation_recursive(tm, qe->op3, node_stmt);
         break;
      }
      case constructor_K:
      {
         constructor * c = GetPointer<constructor>(curr_tn);
         std::vector<std::pair< tree_nodeRef, tree_nodeRef> > &list_of_idx_valu = c->list_of_idx_valu;
         std::vector<std::pair< tree_nodeRef, tree_nodeRef> >::const_iterator vend = list_of_idx_valu.end();
         for (std::vector<std::pair< tree_nodeRef, tree_nodeRef> >::const_iterator i = list_of_idx_valu.begin(); i != vend; i++)
         {
            call_graph_computation_recursive(tm, i->second, node_stmt);
         }
         break;
      }
      case var_decl_K:
      {
         ///var decl performs an assignment when init is not null
         var_decl * vd = GetPointer<var_decl>(curr_tn);
         if(vd->init)
            call_graph_computation_recursive(tm, vd->init, node_stmt);
      }
      case result_decl_K:
      case parm_decl_K:
      case ssa_name_K:
      case integer_cst_K:
      case real_cst_K:
      case string_cst_K:
      case vector_cst_K:
      case complex_cst_K:
      case field_decl_K:
      case label_decl_K:
      case gimple_label_K:
      case gimple_goto_K:
      case gimple_asm_K:
      case gimple_phi_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case CASE_PRAGMA_NODES:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case CASE_CPP_NODES:
      case case_label_expr_K:
      case CASE_FAKE_NODES:
      case const_decl_K:
      case gimple_bind_K:
      case gimple_for_K:
      case gimple_pragma_K:
      case gimple_predict_K:
      case gimple_resx_K:
      case gimple_while_K:
      case identifier_node_K:
      case namespace_decl_K:
      case translation_unit_decl_K:
      case tree_list_K:
      case tree_vec_K:
      case type_decl_K:
      case CASE_TYPE_NODES:
      {
         THROW_ERROR(std::string("Node not supported (") + boost::lexical_cast<std::string>(ind) + std::string("): ") + curr_tn->get_kind_text());
         break;
      }
      default:
         THROW_UNREACHABLE("");
   };
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Completed the recursive analysis of node " + boost::lexical_cast<std::string>(ind));

}

void call_graph_computation::funpointer_analysis(const tree_nodeRef &fun_node, unsigned int node_stmt)
{
   ssa_name* sa = GetPointer<ssa_name>(GET_NODE(fun_node));
   THROW_ASSERT(sa, "Function pointer not in SSA-form");
   pointer_type* pt;
   if(sa->var)
   {
      decl_node* var = GetPointer<decl_node>(GET_NODE(sa->var));
      THROW_ASSERT(var, "Call expression does not point to a declaration node");
      pt = GetPointer<pointer_type>(GET_NODE(var->type));
   }
   else
      pt = GetPointer<pointer_type>(GET_NODE(sa->type));
   THROW_ASSERT(pt, "Declaration node has not information about pointer_type");
   THROW_ASSERT(GetPointer<function_type>(GET_NODE(pt->ptd)), "Pointer type has not information about pointed function_type");
   pointercall_to_type[std::make_pair(current, node_stmt)] = GET_INDEX_NODE(pt->ptd);
}
