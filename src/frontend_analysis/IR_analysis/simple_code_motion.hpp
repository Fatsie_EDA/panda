/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file simple_code_motion.hpp
 * @brief Analysis step that performs some simple code motions over GCC IR
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef SIMPLE_CODE_MOTION_HPP
#define SIMPLE_CODE_MOTION_HPP

///Superclass include
#include "function_frontend_flow_step.hpp"

///Utility include
#include "refcount.hpp"

/**
 * @name forward declarations
 */
//@{
REF_FORWARD_DECL(tree_manager);
REF_FORWARD_DECL(tree_node);
REF_FORWARD_DECL(bloc);
class ssa_name;
class gimple_assign;
//@}

/**
 * Restructure the tree control flow graph
 */
class simple_code_motion: public FunctionFrontendFlowStep
{
   private:

      /// flag to check if initial tree has been dumped
      static bool tree_dumped;

      /// flag used to restart short circuit taf step
      bool restart_short_circuit_taf;

      /**
       * Return the set of analyses in relationship with this design step
       * @param relationship_type is the type of relationship to be considered
       */
      virtual const std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const;

      bool check_if_can_be_moved(gimple_assign * gn, bool &zero_delay, const tree_managerRef TM);

      void add_stmt_back(std::list<tree_nodeRef>& list_of_pred_stmt, tree_nodeRef curr_stmt);

   public:
      /**
       * Constructor.
       * @param parameters is the set of input parameters
       * @param AppM is the application manager
       * @param function_id is the identifier of the function
       * @param design_flow_manager is the design flow manager
       */
      simple_code_motion(const ParameterConstRef parameters, const application_managerRef AppM, unsigned int function_id, const DesignFlowManagerConstRef design_flow_manager);

      /**
       *  Destructor
       */
      ~simple_code_motion();

      /**
       * Updates the tree to have a more compliant CFG
       */
      void Exec();

};
#endif
