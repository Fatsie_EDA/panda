/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file array_ref_fix.cpp
 * @brief Transformation of indirect_ref to array structures into corresponding array_ref
 *
 * @author Andrea Cuoccio <andrea.cuoccio@gmail.com>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader include
#include "config_HAVE_CODE_ESTIMATION_BUILT.hpp"

///header include
#include "array_ref_fix.hpp"

///Behavior include
#include "application_manager.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <fstream>

///STL include
#include <map>
#include <vector>
#include <string>

///Tree include
#include "ext_tree_node.hpp"
#include "tree_basic_block.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"
#include "tree_helper.hpp"
#if HAVE_CODE_ESTIMATION_BUILT
#include "weight_information.hpp"
#endif
array_ref_fix::array_ref_fix(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, ARRAY_REF_FIX, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

array_ref_fix::~array_ref_fix()
{

}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > array_ref_fix::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(VAR_DECL_FIX, SAME_FUNCTION));
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
#if HAVE_BAMBU_BUILT
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(IR_LOWERING, SAME_FUNCTION));
#endif
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void array_ref_fix::Exec()
{
   const tree_managerRef TM = AppM->get_tree_manager();
   if (debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }

   const tree_nodeRef curr_tn = TM->GetTreeNode(function_id);
   function_decl * fd = GetPointer<function_decl>(curr_tn);
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));
   ///Retrive the list of block
   std::map<unsigned int, blocRef> &blocks = sl->list_of_bloc;
   std::map<unsigned int, blocRef>::iterator block_it, block_it_end;
   block_it_end = blocks.end();

   for (block_it = blocks.begin(); block_it != block_it_end; block_it++)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "-->Analisys of BB" + boost::lexical_cast<std::string>(block_it->second->number));
      ///Retrive the list of statement of the block
      std::list<tree_nodeRef> &stmt_list = block_it->second->list_of_stmt;
      std::list<tree_nodeRef>::iterator stm_it, stm_it_end = stmt_list.end();
      ///for each statement, if it is a gimple_assign there could be an indirect_ref in both of the operands
      for (stm_it = stmt_list.begin(); stm_it != stm_it_end; ++stm_it)
      {
         tree_nodeRef &node = *stm_it;
         INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "-->Examining operation " + boost::lexical_cast<std::string>(GET_INDEX_NODE(node)) + ": " + node->ToString());
         ///retrieve the real node
         if (node->get_kind() == tree_reindex_K)
         {
            const tree_nodeRef stmt = GET_NODE(node);
            ///find out if it is a gimple_assign
            if (stmt->get_kind() == gimple_assign_K)
            {
               gimple_assign *gms;
               gms = GetPointer<gimple_assign>(stmt);
               ///check if one of the two operands is an indirect_ref
               ///note: in a gimple modify stmt one of the operands could be an indirect ref, but not both the operands
               tree_nodeRef op0 = GET_NODE(gms->op0);
               tree_nodeRef op1 = GET_NODE(gms->op1);

               if (op0->get_kind() == indirect_ref_K) ///definition, indirect_ref in the left side
               {
                  array_fix(op0, GET_INDEX_NODE(gms->op0), node);
               }

               if (op1->get_kind() == indirect_ref_K) ///use, indirect_ref in the right side
               {
                  array_fix(op1, GET_INDEX_NODE(gms->op1), node);
               }
               if (op1->get_kind() == pointer_plus_expr_K) ///use, pointer_plus_expr in the right side
               {
                  pointer_plus_fix(op1, GET_INDEX_NODE(gms->op1), node);
               }

               while (op0->get_kind() == component_ref_K)
               {
                  component_ref * comp = GetPointer<component_ref>(op0);
                  tree_nodeRef comp_op0 = GET_NODE(comp->op0);
                  if (comp_op0->get_kind() == indirect_ref_K) ///use, indirect_ref in a component ref
                  {
                     array_fix(comp_op0, GET_INDEX_NODE(comp->op0), node);
                  }
                  op0 = comp_op0;
               }
               while (op1->get_kind() == component_ref_K)
               {
                  component_ref * comp = GetPointer<component_ref>(op1);
                  tree_nodeRef comp_op0 = GET_NODE(comp->op0);
                  if (comp_op0->get_kind() == indirect_ref_K) ///use, indirect_ref in a component ref
                  {
                     array_fix(comp_op0, GET_INDEX_NODE(comp->op0), node);
                  }
                  op1 = comp_op0;
               }
            }
         }
         INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "<--Examined operation " + boost::lexical_cast<std::string>(GET_INDEX_NODE(node)) + ": " + node->ToString());
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "<--Analyzed of BB" + boost::lexical_cast<std::string>(block_it->second->number));
   }

   if (debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(false);
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "End of array_ref_fix step");
}

bool array_ref_fix::array_fix(const tree_nodeConstRef node, unsigned int index, tree_nodeRef & DEBUG_PARAMETER(tn))
{
   const tree_managerRef TM = AppM->get_tree_manager();
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Fixing " + boost::lexical_cast<std::string>(index) + ": " + tn->ToString());
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->");

   ///retrive the treereindex containig the effective address
   const indirect_ref * ind_ref = GetPointer<const indirect_ref>(node);
   tree_nodeRef address = GET_NODE(ind_ref->op);

   const gimple_assign * gm = nullptr;
   ///Address is a ssa_name
   while (address->get_kind() == ssa_name_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Address is " + address->ToString());
      const ssa_name * sn = GetPointer<ssa_name>(address);

      if(!sn->var)
         break;
      const tree_nodeRef vard = GET_NODE(sn->var);
      if (vard->get_kind() == var_decl_K)
      {
         const var_decl * vd = GetPointer<var_decl>(vard);
         if (vd->artificial_flag)
         {
            ///ssa_name must be defined in a single place
            if (sn->def_stmts.size() != 1)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixing: ssa with multiple definition");
               return false;
            }
            const tree_nodeRef assignment = GET_NODE(*(sn->def_stmts.begin()));

            if (assignment->get_kind() != gimple_assign_K)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixing: ssa not assigned in gimple assigment");
               return false;
            }
            gm = GetPointer<gimple_assign>(assignment);
            if(gm->vdef)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixing: ssa defines a new Virtual SSA");
               return false;
            }
            address = GET_NODE(gm->op1);
         }
         else
            break;
      }
      else
         break;
   }

   if (address->get_kind() == array_ref_K)
   {
      std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_ir;
      IR_schema_ir[TOK(TOK_OP)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(gm->op1));
      IR_schema_ir[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ind_ref->type));
      IR_schema_ir[TOK(TOK_SRCP)] = ind_ref->include_name + ":" + boost::lexical_cast<std::string>(ind_ref->line_number)+":"+boost::lexical_cast<std::string>(ind_ref->column_number);
      TM->create_tree_node(index, indirect_ref_K, IR_schema_ir);

      TM->increment_removed_pointer_plus();
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Rebuilt as indirect_ref " + boost::lexical_cast<std::string>(index) + ": " + TM->get_tree_node_const(index)->ToString());
      return true;
   }
   if (address->get_kind() == pointer_plus_expr_K)
   {
      ///ssa used as address by the indirect_ref
      pointer_plus_expr *ppe = GetPointer<pointer_plus_expr>(address);
      tree_nodeRef pointer = ppe->op0; ///pointer
      tree_nodeRef offset_internal = ppe->op1; ///offset
      ///the if path is done when the offset is rappresented by a variable
      ///the else path is done when the offset is a costant

      if (GET_NODE(offset_internal)->get_kind() == ssa_name_K)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Address is pointer_plus_expr with variable offset");
         ///the node following the definition is the node containing the operation that calculate the value of the address
         ///ssa containing the offset multiply by the size of the corresponding type
         const ssa_name * sn = GetPointer<ssa_name>(GET_NODE(offset_internal));
         ///ssa_name is defined must be defined in a single place
         if (sn->def_stmts.size() != 1)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Offset defined in multiple locations");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed");
            return false;
         }
         tree_nodeRef assignment = GET_NODE(*(sn->def_stmts.begin()));
         if (assignment->get_kind() != gimple_assign_K)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Offset not defined in gimple assignment");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed");
            return false;
         }
         gm = GetPointer<gimple_assign>(assignment);
         if(gm->vdef)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixing: ssa defines a new Virtual SSA");
            return false;
         }
         tree_nodeRef mult_expr_node = GET_NODE(gm->op1);
         if (mult_expr_node->get_kind() == nop_expr_K)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is assigned by a nop expression");
            nop_expr * nop = GetPointer<nop_expr>(mult_expr_node);
            tree_nodeRef offset = GET_NODE(nop->op);

            while (offset->get_kind() == ssa_name_K)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Current offset is " + offset->ToString());
               sn = GetPointer<ssa_name>(offset);

               if (sn->def_stmts.size() != 1)
               {
                  offset = gm->op1;
                  std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
                  tree_nodeRef point = ppe->op0;
                  if(not rebuild_base(ppe->op0, point))
                  {
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Error in rebuilding base");
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not Fixed");
                     return false;
                  }

                  IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(point));

                  long long size = 1;
                  tree_nodeRef off = nop->op;
                  rebuild_offset(GET_NODE(nop->op), off, size, false);

                  IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off));
                  IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ind_ref->type));
                  IR_schema[TOK(TOK_SRCP)] = ind_ref->include_name + ":" + boost::lexical_cast<std::string>(ind_ref->line_number)+":"+boost::lexical_cast<std::string>(ind_ref->column_number);
                  TM->create_tree_node(index, array_ref_K, IR_schema);
                  TM->increment_removed_pointer_plus();
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Going back to source of offset chain");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Fixed " + boost::lexical_cast<std::string>(index) + ": " + TM->get_tree_node_const(index)->ToString());
                  return true;
               }
               assignment = GET_NODE(*(sn->def_stmts.begin()));
               ///ssa_name must be defined in a single place
               if (assignment->get_kind() != gimple_assign_K)
               {
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Offset not assigned in a gimple assigment");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed");
                  return false;
               }
               gm = GetPointer<gimple_assign>(assignment);
               if(gm->vdef)
               {
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed: ssa defines a new Virtual SSA");
                  return false;
               }
               offset = GET_NODE(gm->op1);
               if (offset->get_kind() == mult_expr_K || offset->get_kind() == plus_expr_K)
               {
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Found new pointer plus or mult expr");
                  mult_expr_node = offset;
                  break;
               }
               if (offset->get_kind() == array_ref_K)
               {

                  std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
                  IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(pointer));
                  IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(gm->op1));
                  IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ind_ref->type));
                  IR_schema[TOK(TOK_SRCP)] = ind_ref->include_name + ":" + boost::lexical_cast<std::string>(ind_ref->line_number) + ":" + boost::lexical_cast<std::string>(ind_ref->column_number);
                  TM->create_tree_node(index, array_ref_K, IR_schema);
                  TM->increment_removed_pointer_plus();
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Offset is an array");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Fixed " + boost::lexical_cast<std::string>(index) + ": " + TM->get_tree_node_const(index)->ToString());
                  return true;
               }
            } // END while(offset->get_kind() == ssa_name_K)
         } // END if (mult_expr_node->get_kind() == nop_expr_K)
         if (mult_expr_node->get_kind() == mult_expr_K)
         {
            mult_expr * me = GetPointer<mult_expr>(mult_expr_node);
            tree_nodeRef type_node = GET_NODE(ppe->type);
            THROW_ASSERT(GET_NODE(ppe->type)->get_kind() == pointer_type_K, "expected a pointer type");
            tree_nodeRef pointed_type = GetPointer<pointer_type>(type_node)->ptd;
            long long int size = tree_helper::size(TM, GET_INDEX_NODE(pointed_type))/8;
            const tree_nodeRef cst = GET_NODE(me->op1);
            if (cst->get_kind() == integer_cst_K)
            {
               bool negate = false;
               long long cst_value = tree_helper::get_integer_cst_value(GetPointer<integer_cst>(cst));
               long long const_value = 0;
               if (cst_value < 0)
               {
                  const_value = - cst_value / size;
                  negate = true;
               }
               else
                  const_value = cst_value / size;

               if (const_value == 1)
               {
                  //tree_nodeRef& offset = GetPointer<mult_expr>(mult_expr_node)->op0;
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is +/-1");
                  tree_nodeRef offset_neg;
                  if (negate)
                  {
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is -1");
                     unsigned int neg_id = TM->new_tree_node_id();

                     tree_nodeRef off = me->op0;
                     rebuild_offset(GET_NODE(me->op0), off, size, false);

                     std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_neg;
                     IR_schema_neg[TOK(TOK_OP)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off));
                     IR_schema_neg[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ppe->type));
                     IR_schema_neg[TOK(TOK_SRCP)] = ind_ref->include_name + ":" + boost::lexical_cast<std::string>(ind_ref->line_number) + ":" + boost::lexical_cast<std::string>(ind_ref->column_number);
                     TM->create_tree_node(neg_id, negate_expr_K, IR_schema_neg);
                     offset_neg = TM->GetTreeReindex(neg_id);
                  }

                  tree_nodeRef point = ppe->op0;
                  if (not rebuild_base(ppe->op0, point))
                  {
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Error in rebuilding base");
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not Fixed");
                     return false;
                  }

                  std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
                  IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(point));
                  if (negate)
                     IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(offset_neg));
                  else
                  {
                     tree_nodeRef off = me->op0;
                     rebuild_offset(GET_NODE(me->op0), off, size, false);
                     IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off/*me->op0*/));
                  }
                  IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ind_ref->type));
                  IR_schema[TOK(TOK_SRCP)] = ind_ref->include_name + ":" + boost::lexical_cast<std::string>(ind_ref->line_number) + ":" + boost::lexical_cast<std::string>(ind_ref->column_number);
                  TM->create_tree_node(index, array_ref_K, IR_schema);
                  TM->increment_removed_pointer_plus();
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Offset is +/-1");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Fixed " + boost::lexical_cast<std::string>(index) + ": " + TM->get_tree_node_const(index)->ToString());
                  return true;
               }

               if ((cst_value % size) != 0)
               {
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Offset is not integer");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not Fixed");
                  return false;
               }
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is " + boost::lexical_cast<std::string>(cst_value/size));
               unsigned int cst_id = TM->new_tree_node_id();
               std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_cst;
               IR_schema_cst[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(me->type));
               IR_schema_cst[TOK(TOK_VALUE)] = boost::lexical_cast<std::string>(cst_value / size);
               TM->create_tree_node(cst_id, integer_cst_K, IR_schema_cst);
               tree_nodeRef node_cost = TM->GetTreeReindex(cst_id);

               tree_nodeRef off = me->op0;
               rebuild_offset(GET_NODE(me->op0), off, size, false);

               unsigned int me_id = TM->new_tree_node_id();
               std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_me;
               IR_schema_me[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off/*me->op0*/));
               IR_schema_me[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(node_cost));
               IR_schema_me[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(me->type));
               IR_schema_me[TOK(TOK_SRCP)] = me->include_name + ":" + boost::lexical_cast<std::string>(me->line_number) + ":" + boost::lexical_cast<std::string>(me->column_number);
               TM->create_tree_node(me_id, mult_expr_K, IR_schema_me);
               tree_nodeRef offset = TM->GetTreeReindex(me_id);

               tree_nodeRef point = ppe->op0;
               if(not rebuild_base(ppe->op0, point))
               {
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Error in rebuilding base");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not Fixed");
                  return false;
               }
               std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_array;
               IR_schema_array[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(point));  //pp->op0));
               IR_schema_array[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(offset));
               IR_schema_array[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ind_ref->type));
               IR_schema_array[TOK(TOK_SRCP)] = ind_ref->include_name + ":" + boost::lexical_cast<std::string>(ind_ref->line_number) + ":" + boost::lexical_cast<std::string>(ind_ref->column_number);
               TM->create_tree_node(index, array_ref_K, IR_schema_array);

               TM->increment_removed_pointer_plus();
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Offset is " + boost::lexical_cast<std::string>(cst_value/size));
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Fixed " + boost::lexical_cast<std::string>(index) + ": " + TM->get_tree_node_const(index)->ToString());
               return true;
            }
            else if (cst->get_kind() == ssa_name_K)
            {

               tree_nodeRef off_op0 = me->op0;
               rebuild_offset(GET_NODE(me->op0), off_op0, size, true);

               unsigned int me_id = TM->new_tree_node_id();
               std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_me;
               IR_schema_me[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off_op0));

               tree_nodeRef off_op1 = me->op1;
               rebuild_offset(GET_NODE(me->op1), off_op1, size, true);

               IR_schema_me[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off_op1));
               IR_schema_me[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(me->type));
               IR_schema_me[TOK(TOK_SRCP)] = me->include_name + ":" + boost::lexical_cast<std::string>(me->line_number) + ":" + boost::lexical_cast<std::string>(me->column_number);
               TM->create_tree_node(me_id, mult_expr_K, IR_schema_me);
               tree_nodeRef offset = TM->GetTreeReindex(me_id);

               tree_nodeRef point = ppe->op0;
               if(not rebuild_base(ppe->op0, point))
               {
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Error in rebuilding base");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not Fixed");
                  return false;
               }

               std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_array;
               IR_schema_array[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(point));
               IR_schema_array[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(offset));
               IR_schema_array[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ind_ref->type));
               IR_schema_array[TOK(TOK_SRCP)] = ind_ref->include_name + ":" + boost::lexical_cast<std::string>(ind_ref->line_number) + ":" + boost::lexical_cast<std::string>(ind_ref->column_number);
               TM->create_tree_node(index, array_ref_K, IR_schema_array);

               TM->increment_removed_pointer_plus();
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Offset is a ssa name");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Fixed " + boost::lexical_cast<std::string>(index) + ": " + TM->get_tree_node_const(index)->ToString());
               return true;
            }
         }
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      }
      else
      {
         ///constant offset
         if (GET_NODE(offset_internal)->get_kind() == integer_cst_K)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Address is pointer_plus_expr with constant offset");

            long long cst_value = tree_helper::get_integer_cst_value(GetPointer<integer_cst>(GET_NODE(offset_internal)));
            const tree_nodeRef type_cst = ind_ref->type;
            const tree_nodeRef int_cst = GetPointer<integer_cst>(GET_NODE(offset_internal))->type;
            long long size = tree_helper::size(TM, GET_INDEX_NODE(type_cst)) / 8;
            if ((cst_value % size) != 0)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Offset is not constant");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not Fixed");
               return false;
            }
            bool negate = false;
            long long const_value = 0;
            if (cst_value < 0)
            {
               const_value= - cst_value / size;
               negate=true;
            }
            else
               const_value = cst_value / size;

            unsigned int cst_id = TM->new_tree_node_id();
            std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_cst;
            IR_schema_cst[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(int_cst));
            IR_schema_cst[TOK(TOK_VALUE)] = boost::lexical_cast<std::string>(const_value);
            TM->create_tree_node(cst_id, integer_cst_K, IR_schema_cst);
            tree_nodeRef offset = TM->GetTreeReindex(cst_id);

            tree_nodeRef offset_n = offset;
            if (negate)
            {
               unsigned int neg_id = TM->new_tree_node_id();
               std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_neg;
               IR_schema_neg[TOK(TOK_OP)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(offset));
               IR_schema_neg[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ind_ref->type));
               IR_schema_neg[TOK(TOK_SRCP)] = ind_ref->include_name + ":" + boost::lexical_cast<std::string>(ind_ref->line_number) + ":" + boost::lexical_cast<std::string>(ind_ref->column_number);
               TM->create_tree_node(neg_id, negate_expr_K, IR_schema_neg);
               offset_n = TM->GetTreeReindex(neg_id);
            }

            ///Definition of the array ref with constant  offset
            std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
            tree_nodeRef point = ppe->op0;
            if(not rebuild_base(ppe->op0, point))
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Error in rebuilding base");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not Fixed");
               return false;
            }

            IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(point));
            IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(offset_n));
            IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ind_ref->type));
            IR_schema[TOK(TOK_SRCP)] = ind_ref->include_name + ":" + boost::lexical_cast<std::string>(ind_ref->line_number) + ":" + boost::lexical_cast<std::string>(ind_ref->column_number);
            TM->create_tree_node(index, array_ref_K, IR_schema);
            TM->increment_removed_pointer_plus();
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Offset is constant");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Fixed " + boost::lexical_cast<std::string>(index) + ": " + TM->get_tree_node_const(index)->ToString());
            return true;
         }
      }
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not Fixed");
   return false;
}

///////////////////////////////////////////////////////////////////////////////////
bool array_ref_fix::pointer_plus_fix(tree_nodeRef& node, unsigned int index, tree_nodeRef & DEBUG_PARAMETER(tn))
{
   const tree_managerRef TM = AppM->get_tree_manager();

   pointer_plus_expr * pp = GetPointer<pointer_plus_expr>(node);
   //int pp_index = index;
   tree_nodeRef pointer = GET_NODE(pp->op0);
   tree_nodeRef offset_internal = pp->op1;
   if (pointer->get_kind() == addr_expr_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Pointer plus based on addr_expr " + boost::lexical_cast<std::string>(index) + "; " + tn->ToString());

      ///Checking if type of pointer plus is the same of base address; if not there is an implicit pointer conversion
      if(GET_INDEX_NODE(pp->type) != tree_helper::get_type_index(TM, pointer->index))
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed: base is of type " + boost::lexical_cast<std::string>(GET_INDEX_NODE(pp->type)) + " - pointer plus type is " + boost::lexical_cast<std::string>(tree_helper::get_type_index(TM, pointer->index)));
         return false;
      }

      const addr_expr * addr = GetPointer<addr_expr>(pointer);
      const tree_nodeRef base_address = GET_NODE(addr->op);
      if (base_address->get_kind() == var_decl_K)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Base of addr_expr is a variable declaration " + base_address->ToString());
         const var_decl * vd = GetPointer<var_decl>(base_address);
         tree_nodeRef temp = GET_NODE(vd->type);
         if (temp->get_kind() == array_type_K)
         {
            tree_nodeRef mult_expr_node;
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Base of addr_expr is an array " + base_address->ToString());
            if (GET_NODE(offset_internal)->get_kind() == ssa_name_K)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is an ssa_name");
               ///the node following the definition is the node containing the operation that calculate the value of the address
               ///ssa containing the offset multiply by the size of the corresponding type
               ssa_name * sn = GetPointer<ssa_name>(GET_NODE(offset_internal));
               ///ssa_name must be defined in a single place
               if (sn->def_stmts.size() != 1)
                  return false;
               tree_nodeRef assignment = GET_NODE(*(sn->def_stmts.begin()));
               if (assignment->get_kind() != gimple_assign_K)
                  return false;
               gimple_assign * gm = GetPointer<gimple_assign>(assignment);
               if(gm->vdef)
                  return false;
               tree_nodeRef lnode = GET_NODE(gm->op1);
               while (lnode->get_kind() == nop_expr_K)
               {
                  nop_expr * nop = GetPointer<nop_expr>(lnode);
                  tree_nodeRef node1 = GET_NODE(nop->op);
                  sn = GetPointer<ssa_name>(node1);
                  ///ssa_name must be defined in a single place
                  if (sn->def_stmts.size() != 1)
                     return false;
                  assignment = GET_NODE(*(sn->def_stmts.begin()));
                  if (assignment->get_kind() != gimple_assign_K)
                     return false;
                  gm = GetPointer<gimple_assign>(assignment);
                  if(gm->vdef)
                     return false;
                  lnode = GET_NODE(gm->op1);
               }
               mult_expr_node = lnode;

               if (mult_expr_node->get_kind() == mult_expr_K)
               {
                  bool negate = false;
                  const mult_expr * me = GetPointer<mult_expr>(mult_expr_node);
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "ssa_name is produced starting from a multiply");
                  tree_nodeRef type_node = GET_NODE(pp->type);
                  THROW_ASSERT(type_node->get_kind() == pointer_type_K, "expected a pointer type, found " + type_node->get_kind_text());
                  tree_nodeRef pointed_type = GetPointer<pointer_type>(type_node)->ptd;
                  if (GET_NODE(pointed_type)->get_kind() == void_type_K)
                  {
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed: pointed type is void");
                     return false;
                  }
                  long long size = tree_helper::size(TM, GET_INDEX_NODE(pointed_type)) / 8;
                  const tree_nodeRef cst = GET_NODE(me->op1);
                  if (cst->get_kind() == integer_cst_K)
                  {
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Second operand of the multiply is a constant");
                     long long cst_value = tree_helper::get_integer_cst_value(GetPointer<integer_cst>(cst));
                     if ((cst_value % size) != 0)
                     {
                        INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed: offset is not integer");
                        return false;
                     }
                     long long const_value = 0;
                     if (cst_value < 0)
                     {
                        const_value= -cst_value / size;
                        negate=true;
                     }
                     else
                        const_value = cst_value / size;
                     if(const_value==1)
                     {
                        tree_nodeRef offset;
                        if (negate)
                        {
                           tree_nodeRef off = me->op0;
                           rebuild_offset(GET_NODE(me->op0), off, size, false);
                           ///chiamare rebuild_offset e usare il risultato nella linea di sotto
                           unsigned int neg_id = TM->new_tree_node_id();
                           std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_neg;
                           IR_schema_neg[TOK(TOK_OP)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off/*me->op0*/));
                           IR_schema_neg[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(me->type));
                           IR_schema_neg[TOK(TOK_SRCP)] = me->include_name + ":" + boost::lexical_cast<std::string>(me->line_number) + ":" + boost::lexical_cast<std::string>(me->column_number);
                           TM->create_tree_node(neg_id, negate_expr_K, IR_schema_neg);
                           offset = TM->GetTreeReindex(neg_id);
                        }
                        unsigned int ar_id = TM->new_tree_node_id();
                        std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_array;
                        IR_schema_array[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(addr->op));
                        if (negate)
                           IR_schema_array[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(offset));
                        else
                        {
                           tree_nodeRef off = me->op0;
                           rebuild_offset(GET_NODE(me->op0), off, size, false);
                           IR_schema_array[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off/*me->op0*/));
                        }
                        IR_schema_array[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(pp->type));
                        IR_schema_array[TOK(TOK_SRCP)] = addr->include_name + ":" + boost::lexical_cast<std::string>(addr->line_number) + ":" + boost::lexical_cast<std::string>(addr->column_number);
                        TM->create_tree_node(ar_id, array_ref_K, IR_schema_array);
                        tree_nodeRef array = TM->GetTreeReindex(ar_id);

                        std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
                        IR_schema[TOK(TOK_OP)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(array));  //pp->op0));
                        IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(pp->type));
                        IR_schema[TOK(TOK_SRCP)] = pp->include_name + ":" + boost::lexical_cast<std::string>(pp->line_number) + ":" + boost::lexical_cast<std::string>(pp->column_number);
                        TM->create_tree_node(index, mult_expr_K, IR_schema);

                        TM->increment_removed_pointer_plus();
                        return true;
                     }
                     unsigned int cst_id = TM->new_tree_node_id();
                     std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_cst;
                     IR_schema_cst[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(me->type));
                     IR_schema_cst[TOK(TOK_VALUE)] = boost::lexical_cast<std::string>(cst_value / size);
                     TM->create_tree_node(cst_id, integer_cst_K, IR_schema_cst);
                     tree_nodeRef node_cost = TM->GetTreeReindex(cst_id);

                     tree_nodeRef off = me->op0;
                     rebuild_offset(GET_NODE(me->op0), off, size, false);

                     unsigned int me_id = TM->new_tree_node_id();
                     std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_me;
                     IR_schema_me[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off/*me->op0*/));
                     IR_schema_me[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(node_cost));
                     IR_schema_me[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(me->type));
                     IR_schema_me[TOK(TOK_SRCP)] = me->include_name + ":" + boost::lexical_cast<std::string>(me->line_number) + ":" + boost::lexical_cast<std::string>(me->column_number);
                     TM->create_tree_node(me_id, mult_expr_K, IR_schema_me);
                     tree_nodeRef offset = TM->GetTreeReindex(me_id);

                     unsigned int ar_id = TM->new_tree_node_id();
                     std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_array;
                     IR_schema_array[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(addr->op));  //pp->op0));
                     IR_schema_array[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(offset));
                     IR_schema_array[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(pp->type));
                     IR_schema_array[TOK(TOK_SRCP)] = addr->include_name + ":" + boost::lexical_cast<std::string>(addr->line_number) + ":" + boost::lexical_cast<std::string>(addr->column_number);
                     TM->create_tree_node(ar_id, array_ref_K, IR_schema_array);
                     tree_nodeRef array = TM->GetTreeReindex(ar_id);

                     std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
                     IR_schema[TOK(TOK_OP)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(array));
                     IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(pp->type));
                     IR_schema[TOK(TOK_SRCP)] = pp->include_name + ":" + boost::lexical_cast<std::string>(pp->line_number) + ":" + boost::lexical_cast<std::string>(pp->column_number);
                     TM->create_tree_node(index, mult_expr_K, IR_schema);

                     TM->increment_removed_pointer_plus();
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Fixed " + boost::lexical_cast<std::string>(index) + "; " + tn->ToString());
                     return true;
                  }
                  else if (cst->get_kind() == ssa_name_K)
                  {
                     PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "ssaname");
                     PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, GET_INDEX_NODE(GetPointer<mult_expr>(mult_expr_node)->op1));

                     tree_nodeRef off_op0 = me->op0;
                     rebuild_offset(GET_NODE(me->op0), off_op0, size, true);

                     unsigned int me_id = TM->new_tree_node_id();
                     std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_me;
                     IR_schema_me[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off_op0 /*me->op0*/));

                     tree_nodeRef off_op1 = me->op1;
                     rebuild_offset(GET_NODE(me->op1), off_op1, size, true);

                     IR_schema_me[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(off_op1 /*me->op1*/));
                     IR_schema_me[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(me->type));
                     IR_schema_me[TOK(TOK_SRCP)] = me->include_name + ":" + boost::lexical_cast<std::string>(me->line_number) + ":" + boost::lexical_cast<std::string>(me->column_number);
                     TM->create_tree_node(me_id, mult_expr_K, IR_schema_me);
                     tree_nodeRef offset = TM->GetTreeReindex(me_id);

                     unsigned int ar_id = TM->new_tree_node_id();
                     std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_array;
                     IR_schema_array[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(addr->op));
                     IR_schema_array[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(offset));
                     IR_schema_array[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(pp->type));
                     IR_schema_array[TOK(TOK_SRCP)] = addr->include_name + ":" + boost::lexical_cast<std::string>(addr->line_number) + ":" + boost::lexical_cast<std::string>(addr->column_number);
                     TM->create_tree_node(ar_id, array_ref_K, IR_schema_array);
                     tree_nodeRef array = TM->GetTreeReindex(ar_id);

                     std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
                     IR_schema[TOK(TOK_OP)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(array));
                     IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(pp->type));
                     IR_schema[TOK(TOK_SRCP)] = pp->include_name + ":" + boost::lexical_cast<std::string>(pp->line_number) + ":" + boost::lexical_cast<std::string>(pp->column_number);
                     TM->create_tree_node(index, mult_expr_K, IR_schema);
                     INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Fixed " + boost::lexical_cast<std::string>(index) + "; " + tn->ToString());
                     TM->increment_removed_pointer_plus();
                     return true;
                  }
               }
            }//END offset_internal=ssa_name
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed");
   }
   return false;
}

bool array_ref_fix::rebuild_offset(const tree_nodeRef offset_int, tree_nodeRef & new_offset, long long int & size, bool div_by_size)
{
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Rebuilding offset starting from " + offset_int->ToString());
   const tree_managerRef TM = AppM->get_tree_manager();
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->");
   //tree_nodeRef off=offset_int;
   tree_nodeRef address = offset_int;

   if (address->get_kind() == ssa_name_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is an ssa_name");
      const ssa_name * sn = GetPointer<ssa_name>(address);
      const tree_nodeRef vard = GET_NODE(sn->var);
      if (vard->get_kind() == var_decl_K)
      {
         const var_decl * vd = GetPointer<var_decl>(vard);
         if (vd->artificial_flag)
         {
            ///ssa_name must be defined in a single place
            if (sn->def_stmts.size() != 1)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Ssa name defined in multiple locations");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
               return false;
            }
            const tree_nodeRef assignment = GET_NODE(*(sn->def_stmts.begin()));
            if (assignment->get_kind() != gimple_assign_K)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Ssa name not defined in gimple assignment");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
               return false;
            }
            const gimple_assign * gm = GetPointer<gimple_assign>(assignment);
            tree_nodeRef partial = gm->op1;
            if (rebuild_offset(GET_NODE(gm->op1), partial, size, div_by_size))
            {
               new_offset = partial;
            }
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New offset " + new_offset->ToString()); 

            return true;
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not artificial variable");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
      return false;
   }
   if (address->get_kind() == nop_expr_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is a nop_expr");
      nop_expr * nop = GetPointer<nop_expr>(address);
      tree_nodeRef partial = nop->op;
      rebuild_offset(GET_NODE(nop->op), partial, size, div_by_size);
      new_offset = partial;
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New offset " + new_offset->ToString()); 
      return true;
   }

   if (address->get_kind() == minus_expr_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is a minus_expr");

      tree_nodeRef partial;//=gm->op1;
      minus_expr * minus = GetPointer<minus_expr>(address);

      partial = minus->op0;
      rebuild_offset(GET_NODE(minus->op0), partial, size, div_by_size);
      tree_nodeRef oper1 = minus->op1;
      rebuild_offset(GET_NODE(minus->op1), oper1, size, div_by_size);

      unsigned int minus_id = TM->new_tree_node_id();
      std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_minus;
      IR_schema_minus[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(partial));
      IR_schema_minus[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(oper1));
      IR_schema_minus[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(minus->type));
      IR_schema_minus[TOK(TOK_SRCP)] = minus->include_name + ":" + boost::lexical_cast<std::string>(minus->line_number) + ":" + boost::lexical_cast<std::string>(minus->column_number);
      TM->create_tree_node(minus_id, minus_expr_K, IR_schema_minus);
      new_offset = TM->GetTreeReindex(minus_id);
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New offset " + new_offset->ToString()); 
      return true;
   }
   if (address->get_kind() == plus_expr_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is a plus_expr");
      const plus_expr * plus = GetPointer<plus_expr>(address);
      const tree_nodeRef cst = GET_NODE(plus->op1);
      PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, cst->get_kind_text());
      tree_nodeRef partial;// = gm->op1;
      if (cst->get_kind() == integer_cst_K)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is constant");
         long long cst_value = tree_helper::get_integer_cst_value(GetPointer<integer_cst>(cst));
         tree_nodeRef node_t = plus->op1;
         if (cst_value < 0)
         {
            unsigned int cst_id = TM->new_tree_node_id();
            std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_cst;
            IR_schema_cst[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(plus->type));
            IR_schema_cst[TOK(TOK_VALUE)] = boost::lexical_cast<std::string>(-cst_value);
            TM->create_tree_node(cst_id, integer_cst_K, IR_schema_cst);
            tree_nodeRef node_cost = TM->GetTreeReindex(cst_id);

            unsigned int neg_id = TM->new_tree_node_id();
            std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_neg;
            IR_schema_neg[TOK(TOK_OP)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(node_cost));
            IR_schema_neg[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(plus->type));
            IR_schema_neg[TOK(TOK_SRCP)] = plus->include_name + ":" + boost::lexical_cast<std::string>(plus->line_number) + ":" + boost::lexical_cast<std::string>(plus->column_number);
            TM->create_tree_node(neg_id, negate_expr_K, IR_schema_neg);
            node_t = TM->GetTreeReindex(neg_id);
         }
         partial = plus->op0;
         rebuild_offset(GET_NODE(plus->op0), partial, size, div_by_size);
         unsigned int plus_id = TM->new_tree_node_id();
         std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_plus;
         IR_schema_plus[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(partial));
         IR_schema_plus[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(node_t));
         IR_schema_plus[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(plus->type));
         IR_schema_plus[TOK(TOK_SRCP)] = plus->include_name + ":" + boost::lexical_cast<std::string>(plus->line_number) + ":" + boost::lexical_cast<std::string>(plus->column_number);
         TM->create_tree_node(plus_id, plus_expr_K, IR_schema_plus);
         new_offset = TM->GetTreeReindex(plus_id);
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New offset " + new_offset->ToString()); 
         return true;
      }
      else
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is not constant");
         partial = plus->op0;
         rebuild_offset(GET_NODE(plus->op0), partial, size, div_by_size);
         tree_nodeRef oper1 = plus->op1;
         rebuild_offset(GET_NODE(plus->op1), oper1, size, div_by_size);

         unsigned int plus_id = TM->new_tree_node_id();
         std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_plus;
         IR_schema_plus[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(partial));
         IR_schema_plus[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(oper1));
         IR_schema_plus[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(plus->type));
         IR_schema_plus[TOK(TOK_SRCP)] = plus->include_name + ":" + boost::lexical_cast<std::string>(plus->line_number) + ":" + boost::lexical_cast<std::string>(plus->column_number);
         TM->create_tree_node(plus_id, plus_expr_K, IR_schema_plus);
         new_offset = TM->GetTreeReindex(plus_id);
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New offset " + new_offset->ToString());
         return true;
      }
   }
   if (address->get_kind() == mult_expr_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is a mult_expr");
      const mult_expr * mult = GetPointer<mult_expr>(address);

      const tree_nodeRef cst0 = GET_NODE(mult->op0);
      const tree_nodeRef cst1 = GET_NODE(mult->op1);
      tree_nodeRef partial;// = gm->op1;
      if (div_by_size)
      {
         if (cst0->get_kind() == integer_cst_K)
         {
            long long cst_value = tree_helper::get_integer_cst_value(GetPointer<integer_cst>(cst0));

            if ((cst_value % size) != 0)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not integer offset");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed");
               return false;
            }

            long long const_value = cst_value / size;

            unsigned int cst_id = TM->new_tree_node_id();
            std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_cst;
            IR_schema_cst[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(mult->type));
            IR_schema_cst[TOK(TOK_VALUE)] = boost::lexical_cast<std::string>(const_value);
            TM->create_tree_node(cst_id, integer_cst_K, IR_schema_cst);
            tree_nodeRef node_cost = TM->GetTreeReindex(cst_id);

            unsigned int mult_id = TM->new_tree_node_id();
            std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_mult;

            partial = mult->op1;
            rebuild_offset(GET_NODE(mult->op0), partial, size, div_by_size);
            IR_schema_mult[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(node_cost));
            IR_schema_mult[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(partial));
            IR_schema_mult[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(mult->type));
            IR_schema_mult[TOK(TOK_SRCP)] = mult->include_name + ":" + boost::lexical_cast<std::string>(mult->line_number) + ":" + boost::lexical_cast<std::string>(mult->column_number);
            TM->create_tree_node(mult_id, mult_expr_K, IR_schema_mult);
            new_offset = TM->GetTreeReindex(mult_id);
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New offset " + new_offset->ToString());
            return true;
         }
         else if (cst1->get_kind() == integer_cst_K)
         {
            long long cst_value = tree_helper::get_integer_cst_value(GetPointer<integer_cst>(cst1));

            if ((cst_value % size) != 0)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not integer offset");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed");
               return false;
            }
            long long const_value = cst_value / size;
            unsigned int cst_id = TM->new_tree_node_id();
            std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_cst;
            IR_schema_cst[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(mult->type));
            IR_schema_cst[TOK(TOK_VALUE)] = boost::lexical_cast<std::string>(const_value);
            TM->create_tree_node(cst_id, integer_cst_K, IR_schema_cst);
            tree_nodeRef node_cost = TM->GetTreeReindex(cst_id);

            unsigned int mult_id = TM->new_tree_node_id();
            std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_mult;

            partial = mult->op0;
            rebuild_offset(GET_NODE(mult->op0), partial, size, div_by_size);
            IR_schema_mult[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(partial));
            IR_schema_mult[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(node_cost));
            IR_schema_mult[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(mult->type));
            IR_schema_mult[TOK(TOK_SRCP)] = mult->include_name + ":" + boost::lexical_cast<std::string>(mult->line_number) + ":" + boost::lexical_cast<std::string>(mult->column_number);
            TM->create_tree_node(mult_id, mult_expr_K, IR_schema_mult);
            new_offset = TM->GetTreeReindex(mult_id);
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New offset " + new_offset->ToString());
            return true;
         }
      }

      //tree_nodeRef partial;// = gm->op1;
      partial = mult->op0;
      rebuild_offset(GET_NODE(mult->op0), partial, size, div_by_size);
      tree_nodeRef oper1 = mult->op1;
      rebuild_offset(GET_NODE(mult->op1), oper1, size, div_by_size);

      unsigned int mult_id = TM->new_tree_node_id();
      std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_mult;
      IR_schema_mult[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(partial));
      IR_schema_mult[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(oper1));
      IR_schema_mult[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(mult->type));
      IR_schema_mult[TOK(TOK_SRCP)] = mult->include_name + ":" + boost::lexical_cast<std::string>(mult->line_number) + ":" + boost::lexical_cast<std::string>(mult->column_number);
      TM->create_tree_node(mult_id, mult_expr_K, IR_schema_mult);
      //tree_nodeRef offset_minus = tree_nodeRef(new tree_reindex(minus_id, TM->add(minus_id)));
      new_offset = TM->GetTreeReindex(mult_id);
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New offset " + new_offset->ToString());
      return true;

   }
   if (address->get_kind() == trunc_div_expr_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is a trunc_div_expr_K");

      const trunc_div_expr * t_div = GetPointer<trunc_div_expr>(address);
      //PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, cst->get_kind_text());
      //tree_nodeRef partial;// = gm->op1;

      tree_nodeRef partial = t_div->op0;
      rebuild_offset(GET_NODE(t_div->op0), partial, size, div_by_size);
      tree_nodeRef oper1 = t_div->op1;
      rebuild_offset(GET_NODE(t_div->op1), oper1, size, div_by_size);

      unsigned int t_div_id = TM->new_tree_node_id();
      std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_t_div;
      IR_schema_t_div[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(partial));
      IR_schema_t_div[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(oper1));
      IR_schema_t_div[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(t_div->type));
      IR_schema_t_div[TOK(TOK_SRCP)] = t_div->include_name + ":" + boost::lexical_cast<std::string>(t_div->line_number) + ":" + boost::lexical_cast<std::string>(t_div->column_number);
      TM->create_tree_node(t_div_id, trunc_div_expr_K, IR_schema_t_div);
      //tree_nodeRef offset_minus = tree_nodeRef(new tree_reindex(minus_id, TM->add(minus_id)));
      new_offset = TM->GetTreeReindex(t_div_id);
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New offset " + new_offset->ToString());
      return true;
   }
   if (address->get_kind() == trunc_mod_expr_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Offset is a trunc_mod_expr_K");
      const trunc_mod_expr * t_mod = GetPointer<trunc_mod_expr>(address);
      tree_nodeRef partial = t_mod->op0;
      rebuild_offset(GET_NODE(t_mod->op0), partial, size, div_by_size );
      tree_nodeRef oper1 = t_mod->op1;
      rebuild_offset(GET_NODE(t_mod->op1), oper1, size, div_by_size);

      unsigned int t_mod_id = TM->new_tree_node_id();
      std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_t_mod;
      IR_schema_t_mod[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(partial));
      IR_schema_t_mod[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(oper1));
      IR_schema_t_mod[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(t_mod->type));
      IR_schema_t_mod[TOK(TOK_SRCP)] = t_mod->include_name + ":" + boost::lexical_cast<std::string>(t_mod->line_number) + ":" + boost::lexical_cast<std::string>(t_mod->column_number);
      TM->create_tree_node(t_mod_id, trunc_mod_expr_K, IR_schema_t_mod);
      new_offset = TM->GetTreeReindex(t_mod_id);
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New offset " + new_offset->ToString());
      return true;
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not fixed");
   return false;
}

bool array_ref_fix::rebuild_base(const tree_nodeRef pointer_int, tree_nodeRef & new_pointer)
{
   const tree_managerRef TM = AppM->get_tree_manager();
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Rebuilding base starting from " + pointer_int->ToString());
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->");
   tree_nodeRef address = GET_NODE(pointer_int);

   if (address->get_kind() == ssa_name_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Base is an ssa_name");
      const ssa_name * sn = GetPointer<ssa_name>(address);
      const tree_nodeRef vard = GET_NODE(sn->var);
      if (vard->get_kind() == var_decl_K)
      {
         const var_decl * vd = GetPointer<var_decl>(vard);
         if (vd->artificial_flag)
         {
            ///ssa_name must be defined in a single place
            if (sn->def_stmts.size() != 1)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Ssa name defined in multiple locations");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
               return false;
            }
            if (GetPointer<tree_reindex>(vd->name) != nullptr)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Variable is artificial but has name");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
               return false;
            }
            const tree_nodeRef assignment = GET_NODE(*(sn->def_stmts.begin()));
            if (assignment->get_kind() != gimple_assign_K)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Ssa name is not assigned in gimple assignment");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
               return false;
            }
            const gimple_assign * gm = GetPointer<gimple_assign>(assignment);
            if(gm->vdef)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Assignment defines a new Virtual SSA");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
               return false;
            }

            if (GET_NODE(gm->op1)->get_kind() == call_expr_K)
            {
               new_pointer = gm->op0;
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New base " + new_pointer->ToString());
               return true;
            }

            if(not rebuild_base(gm->op1, new_pointer))
            {
               new_pointer = gm->op0;
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Failed recursion");
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
               return true;
            }
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New base " + new_pointer->ToString());
            return true;
         }
      }
      else if(vard->get_kind() == parm_decl_K)
      {
         new_pointer = pointer_int;
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New base " + new_pointer->ToString());
         return true;

      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
      return false;
   }
   if (address->get_kind() == nop_expr_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Base is a nop_expr");
      nop_expr * nop = GetPointer<nop_expr>(address);
      tree_nodeRef partial = nop->op;
      if(not rebuild_base(nop->op, partial))
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Failed recursion");
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
         return false;
      }

      unsigned int nop_id = TM->new_tree_node_id();
      std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_nop;
      IR_schema_nop[TOK(TOK_OP)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(partial));
      IR_schema_nop[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(nop->type));
      IR_schema_nop[TOK(TOK_SRCP)] = nop->include_name + ":" + boost::lexical_cast<std::string>(nop->line_number) + ":" + boost::lexical_cast<std::string>(nop->column_number);
      TM->create_tree_node(nop_id, nop_expr_K, IR_schema_nop);
      new_pointer = TM->GetTreeReindex(nop_id);
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New base " + new_pointer->ToString());
      return true;
   }
   if (address->get_kind() == component_ref_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Base is a component_ref");
      const component_ref * cr = GetPointer<component_ref>(address);
      tree_nodeRef new_op0;
      if(not rebuild_base(cr->op0, new_op0))
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Failed recursion");
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
         return false;
      }
      unsigned int component_ref_id = TM->new_tree_node_id();
      std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_cr;
      IR_schema_cr[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(new_op0));
      IR_schema_cr[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(cr->op1));
      if(cr->op2)
         IR_schema_cr[TOK(TOK_OP2)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(cr->op2));
      IR_schema_cr[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(cr->type));
      IR_schema_cr[TOK(TOK_SRCP)] = cr->include_name + ":" + boost::lexical_cast<std::string>(cr->line_number) + ":" + boost::lexical_cast<std::string>(cr->column_number);
      TM->create_tree_node(component_ref_id, component_ref_K, IR_schema_cr);
      new_pointer = TM->GetTreeReindex(component_ref_id);
#if HAVE_CODE_ESTIMATION_BUILT
      component_ref * new_cr = GetPointer<component_ref>(TM->GetTreeNode(component_ref_id));
      new_cr->weight_information = new_cr->weight_information;
#endif
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New base " + new_pointer->ToString());
      return true;
   }
   if (address->get_kind() == indirect_ref_K)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Base is an indirect ref");
      const indirect_ref * ir = GetPointer<indirect_ref>(address);
      tree_nodeRef new_op;
      if(not rebuild_base(ir->op, new_op))
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Failed recursion");
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilding");
         return false;
      }
      unsigned int indirect_ref_id = TM->new_tree_node_id();
      std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema_ir;
      IR_schema_ir[TOK(TOK_OP)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(new_op));
      IR_schema_ir[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ir->type));
      IR_schema_ir[TOK(TOK_SRCP)] = ir->include_name + ":" + boost::lexical_cast<std::string>(ir->line_number) + ":" + boost::lexical_cast<std::string>(ir->column_number);
      TM->create_tree_node(indirect_ref_id, indirect_ref_K, IR_schema_ir);
      new_pointer = TM->GetTreeReindex(indirect_ref_id);
#if HAVE_CODE_ESTIMATION_BUILT
      indirect_ref * new_ir = GetPointer<indirect_ref>(TM->GetTreeNode(indirect_ref_id));
      new_ir->weight_information = ir->weight_information;
#endif
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--New base " + new_pointer->ToString());
      return true;
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Not rebuilt");

   return false;
}
