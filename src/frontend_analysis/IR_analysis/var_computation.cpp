/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file var_computation.cpp
 * @brief Analyzes operations and creates the sets of read and written variables.
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "var_computation.hpp"

///Behavior include
#include "behavioral_helper.hpp"
#include "application_manager.hpp"
#include "function_behavior.hpp"
#include "graph.hpp"
#include "op_graph.hpp"
#include "operations_graph_constructor.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <fstream>

///Tree include
#include "ext_tree_node.hpp"
#include "tree_helper.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"

///Utility include
#include "boost/lexical_cast.hpp"
#include "dbgPrintHelper.hpp"
#include "exceptions.hpp"



#define TOSTRING(id) boost::lexical_cast<std::string>(id)

VarComputation::VarComputation(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, VAR_ANALYSIS, _design_flow_manager, _parameters),
   ogc(function_behavior->ogc),
   cfg(function_behavior->CGetOpGraph(FunctionBehavior::CFG)),
   behavioral_helper(function_behavior->GetBehavioralHelper())
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

VarComputation::~VarComputation()
{}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > VarComputation::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(OPERATIONS_CFG_COMPUTATION, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(POST_PRECEDENCE_RELATIONSHIP) :
      case(PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void VarComputation::Exec()
{
   const tree_managerConstRef tree_manager = AppM->get_tree_manager();
   VertexIterator VerIt, VerItEnd;
   std::list<vertex> Vertices;
   for(boost::tie(VerIt, VerItEnd) = boost::vertices(*cfg); VerIt != VerItEnd; VerIt++)
   {
      Vertices.push_back(*VerIt);
   }
   std::list<vertex> PhiNodes;
   for(std::list<vertex>::iterator Ver = Vertices.begin(); Ver != Vertices.end(); )
   {
      std::list<vertex>::iterator curr_Ver = Ver;
      ++Ver;
      if (GET_TYPE(cfg, *curr_Ver) == TYPE_VPHI)
      {
         PhiNodes.push_back(*curr_Ver);
         Vertices.erase(curr_Ver);
      }
   }
   for(std::list<vertex>::iterator Ver = Vertices.begin(); Ver != Vertices.end(); Ver++)
   {
      const unsigned int node_id = cfg->CGetOpNodeInfo(*Ver)->node_id;
      if(node_id)
      {
         RecursivelyAnalyze(*Ver, tree_manager->CGetTreeNode(node_id), AT_UNKNOWN);
      }
   }
   for(std::list<vertex>::iterator Ver = PhiNodes.begin(); Ver != PhiNodes.end(); Ver++)
   {
      const unsigned int node_id = cfg->CGetOpNodeInfo(*Ver)->node_id;
      if(node_id)
      {
         RecursivelyAnalyze(*Ver, tree_manager->CGetTreeNode(node_id), AT_UNKNOWN);
      }
   }
   if(parameters->getOption<bool>(OPT_print_dot))
   {
      function_behavior->CGetOpGraph(FunctionBehavior::CFG)->WriteDot("OP_Variables.dot", 1);
   }
}

void VarComputation::RecursivelyAnalyze(const vertex op_vertex, const tree_nodeConstRef tree_node, const VariableAccessType access_type) const
{
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Analyzing node " + tree_node->ToString());

   if (GetPointer<const gimple_node>(tree_node) && (GetPointer<const gimple_node>(tree_node)->vuse || GetPointer<const gimple_node>(tree_node)->vdef))
   {
       AnalyzeVops(op_vertex, GetPointer<const gimple_node>(tree_node));
   }

   switch(tree_node->get_kind())
   {
      case gimple_nop_K:
      case gimple_pragma_K:
         break;
      case gimple_assign_K:
      {
         const gimple_assign * ga = GetPointer<const gimple_assign>(tree_node);
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(ga->op0), AT_DEF);
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(ga->op1), AT_USE);
         break;
      }
      case gimple_phi_K:
      {
         const gimple_phi * pn = GetPointer<const gimple_phi>(tree_node);
         std::vector<std::pair< tree_nodeRef, unsigned int> >::const_iterator rdef, rdef_end = pn->list_of_def_edge.end();
         for(rdef = pn->list_of_def_edge.begin(); rdef != rdef_end; rdef++)
         {
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(rdef->first), AT_USE);
         }
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(pn->res),AT_DEF);
         break;
      }
      case gimple_return_K:
      {
         const gimple_return * gr = GetPointer<const gimple_return>(tree_node);
         const tree_nodeConstRef op = gr->op;
         if (op)
         {
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(op), AT_USE);
         }
         break;
      }
      case call_expr_K:
      {
         const call_expr * ce = GetPointer<const call_expr>(tree_node);
         ///Needed to correctly support function pointers
         if(GET_CONST_NODE(ce->fn)->get_kind() == ssa_name_K)
         {
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(ce->fn), AT_USE);
         }
         const std::vector<tree_nodeRef> & args = ce->args;
         std::vector<tree_nodeRef>::const_iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            ///add parameter to the vertex
            ogc->add_parameter(op_vertex, GET_CONST_NODE(*arg)->index);
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(*arg), AT_USE);
         }

         break;
      }

      case gimple_call_K:
      {
         const gimple_call * gc = GetPointer<const gimple_call>(tree_node);
         ///Needed to correctly support function pointers
         if(GET_CONST_NODE(gc->fn)->get_kind() == ssa_name_K)
         {
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(gc->fn), AT_USE);
         }
         const std::vector<tree_nodeRef> & args = gc->args;
         std::vector<tree_nodeRef>::const_iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            ///add parameter to the vertex
            ogc->add_parameter(op_vertex, GET_CONST_NODE(*arg)->index);
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(*arg), AT_USE);
         }

         break;
      }
      case gimple_cond_K:
      {
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(GetPointer<const gimple_cond>(tree_node)->op0), AT_USE);
         break;
      }
      case gimple_while_K:
      {
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(GetPointer<const gimple_while>(tree_node)->op0), AT_USE);
         break;
      }
      case gimple_for_K:
      {
         const gimple_for * fe = GetPointer<const gimple_for>(tree_node);
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(fe->op0), AT_USE);
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(fe->op1), AT_USE);
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(fe->op2), AT_USE);
         break;
      }
      case gimple_multi_way_if_K:
      {
         const gimple_multi_way_if * gmwi = GetPointer<const gimple_multi_way_if>(tree_node);
         for(const auto cond : gmwi->list_of_cond)
         {
            if(cond.first)
            {
               RecursivelyAnalyze(op_vertex, GET_CONST_NODE(cond.first), AT_USE);
            }
         }
         break;
      }
      case gimple_switch_K:
      {
         const gimple_switch * gs = GetPointer<const gimple_switch>(tree_node);
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(gs->op0), AT_USE);
         if(gs->op1)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(gs->op1), AT_USE);
         break;
      }
      case gimple_label_K:
      {
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(GetPointer<const gimple_label>(tree_node)->op), AT_USE);
         break;
      }
      case gimple_goto_K:
      {
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(GetPointer<const gimple_goto>(tree_node)->op), AT_USE);
         break;
      }
      case gimple_asm_K:
      {
         const gimple_asm * asme = GetPointer<const gimple_asm>(tree_node);
         if(asme->out)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(asme->out), AT_DEF);
         if(asme->in)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(asme->in), AT_USE);
         if(asme->clob)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(asme->clob), AT_USE);
         
         break;
      }
      case function_decl_K:
      {
         THROW_WARNING("Function pointer");
         break;
      }
      case var_decl_K:
      {
         ogc->AddSourceCodeVariable(op_vertex, tree_node->index);
         const var_decl * vd = GetPointer<const var_decl>(tree_node);
         if (vd and (not vd->scpe or GET_CONST_NODE(vd->scpe)->get_kind() == translation_unit_decl_K))
            AppM->add_global_variable(vd->index);
         break;
      }
      case parm_decl_K:
      {
         ogc->AddScalar(cfg->CGetOpGraphInfo()->entry_vertex, tree_node->index, AT_DEF);
         ogc->AddSourceCodeVariable(op_vertex, tree_node->index);
         break;
      }
      case result_decl_K:
      {
         ogc->AddSourceCodeVariable(op_vertex, tree_node->index);
         break;
      }
      case ssa_name_K:
      {
         const ssa_name * sn = GetPointer<const ssa_name>(tree_node);
         if(sn->virtual_flag)
         {
            switch(access_type)
            {
               case(AT_USE):
               case(AT_DEF):
                  ogc->AddVirtual_DATADEPS(op_vertex, tree_node->index, access_type);
                  break;
               case(AT_ADDRESS):
                  THROW_UNREACHABLE("Address expresion of a virtual variable");
                  break;
               case(AT_UNKNOWN):
               default:
                  THROW_UNREACHABLE("");
            }
         }
         else
         {
            if((sn->volatile_flag or (sn->def_stmts.size() == 1 and GET_NODE(*(sn->def_stmts.begin()))->get_kind() == gimple_nop_K)) and sn->var)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Adding " + sn->ToString() + " to defs of Entry");
               ogc->AddScalar(cfg->CGetOpGraphInfo()->entry_vertex, tree_node->index, AT_DEF);
            }
            ogc->AddSourceCodeVariable(op_vertex, tree_node->index);
            switch(access_type)
            {
               case(AT_USE):
               case(AT_DEF):
                  ogc->AddScalar(op_vertex, tree_node->index, access_type);
                  break;
               case(AT_ADDRESS):
                  break;
               case(AT_UNKNOWN):
               default:
                  THROW_UNREACHABLE("");
            }
         }
         break;
      }
      case tree_list_K:
      {
         const tree_list * tl = GetPointer<const tree_list>(tree_node);
         if(tl->purp)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(tl->purp), AT_USE);
         tree_nodeConstRef current_args = tree_node;
         while (current_args)
         {
            const tree_list * current_tree_list = GetPointer<const tree_list>(current_args);
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(current_tree_list->valu), access_type);
            if(current_tree_list->chan)
               current_args = GET_CONST_NODE(current_tree_list->chan);
            else
               break;
         }
         break;
      }
      case tree_vec_K:
      {
         const tree_vec * tv = GetPointer<const tree_vec>(tree_node);
         const std::vector<tree_nodeRef> & list_of_op = tv->list_of_op;
         const std::vector<tree_nodeRef>::const_iterator op_end = list_of_op.end();
         for(std::vector<tree_nodeRef>::const_iterator op = list_of_op.begin(); op != op_end; op++)
         {
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(*op), access_type);
         }
         break;
      }
      case CASE_UNARY_EXPRESSION:
      {
         const unary_expr * ue = GetPointer<const unary_expr>(tree_node);
         if(ue->get_kind() == addr_expr_K)
         {
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(ue->op),AT_ADDRESS);
         }
         else
         {
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(ue->op), AT_USE);
         }
         break;
      }
      case CASE_BINARY_EXPRESSION:
      {
         const binary_expr * be = GetPointer<const binary_expr>(tree_node);
         if(be->get_kind() == postincrement_expr_K or be->get_kind() == postdecrement_expr_K)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(be->op0), AT_DEF);
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(be->op0), AT_USE);
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(be->op1), AT_USE);
         break;
      }
      case CASE_TERNARY_EXPRESSION:
      {
         const ternary_expr * te = GetPointer<const ternary_expr>(tree_node);
         if(te->get_kind() == component_ref_K or te->get_kind() == bit_field_ref_K)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(te->op0), access_type);
         else
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(te->op0), AT_USE);
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(te->op1), AT_USE);
         if(te->op2)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(te->op2), AT_USE);
         break;
      }
      case CASE_QUATERNARY_EXPRESSION:
      {
         const quaternary_expr * qe = GetPointer<const quaternary_expr>(tree_node);
         const tree_nodeConstRef first_operand = GET_CONST_NODE(qe->op0);
         if((qe->get_kind() == array_ref_K or qe->get_kind() == array_range_ref_K) and tree_helper::CGetType(first_operand)->get_kind() == array_type_K)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(qe->op0), access_type);
         else
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(qe->op0), AT_USE);
         RecursivelyAnalyze(op_vertex, GET_CONST_NODE(qe->op1), AT_USE);
         if(qe->op2)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(qe->op2), AT_USE);
         if(qe->op3)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(qe->op3), AT_USE);
         break;
      }
      case target_mem_ref_K:
      {
         const target_mem_ref * tm = GetPointer<const target_mem_ref>(tree_node);
         if(tm->symbol)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(tm->symbol), access_type);
         if(tm->base)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(tm->base), AT_USE);
         if(tm->idx)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(tm->idx), AT_USE);
         break;
      }
      case target_mem_ref461_K:
      {
         const target_mem_ref461 * tm = GetPointer<const target_mem_ref461>(tree_node);
         if(tm->base)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(tm->base), AT_USE);
         if(tm->idx)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(tm->idx), AT_USE);
         if(tm->idx2)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(tm->idx2), AT_USE);
         break;
      }
      case constructor_K:
      {
         const constructor * constr = GetPointer<const constructor>(tree_node);
         const std::vector<std::pair< tree_nodeRef, tree_nodeRef> > & list_of_idx_valu = constr->list_of_idx_valu;
         std::vector<std::pair< tree_nodeRef, tree_nodeRef> >::const_iterator valu, valu_end = list_of_idx_valu.end();
         for(valu = list_of_idx_valu.begin(); valu != valu_end; valu++)
         {
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(valu->second), AT_USE);
         }
         break;
      }
      case case_label_expr_K:
      {
         const case_label_expr * cle = GetPointer<const case_label_expr>(tree_node);
         if(cle->op0)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(cle->op0), AT_USE);
         if(cle->op1)
            RecursivelyAnalyze(op_vertex, GET_CONST_NODE(cle->op1), AT_USE);
         break;
      }
      case complex_cst_K:
      case const_decl_K:
      case field_decl_K:
      case integer_cst_K:
      case label_decl_K:
      case namespace_decl_K:
      case real_cst_K:
      case string_cst_K:
      case vector_cst_K:
      {
         break;
      }
      case binfo_K:
      case block_K:
      case gimple_bind_K:
      case gimple_predict_K:
      case gimple_resx_K:
      case identifier_node_K:
      case translation_unit_decl_K:
      case type_decl_K:
      case CASE_CPP_NODES:
      case CASE_FAKE_NODES:
      case issue_pragma_K:
      case blackbox_pragma_K:
      case profiling_pragma_K:
      case statistical_profiling_K:
      case map_pragma_K:
      case call_hw_pragma_K:
      case call_point_hw_pragma_K:
      case omp_pragma_K:
      case null_node_K:
      case omp_for_pragma_K:
      case omp_parallel_pragma_K:
      case omp_sections_pragma_K:
      case omp_parallel_sections_pragma_K:
      case omp_section_pragma_K:
      case omp_task_pragma_K:
      case omp_target_pragma_K:
      case statement_list_K:
      case CASE_TYPE_NODES:
      {
         THROW_UNREACHABLE("Unexpected tree node: " + tree_node->get_kind_text());
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Analyzed node " + tree_node->ToString());
   return;
}

void VarComputation::AnalyzeVops(const vertex op_vertex, const gimple_node* vop) const
{
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Adding virtual operands"); 

   /// VIRTUAL DEFINITIONS
   if (vop->vdef)
   {
      /// Adding the (virtual) variable defined by the operation
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "    Adding virtual variable defined " + TOSTRING(GET_INDEX_NODE(vop->vdef)));
      ogc->AddVirtual_DATADEPS(op_vertex, GET_INDEX_NODE(vop->vdef), AT_DEF);
      if(parameters->getOption<bool>(OPT_gcc_single_write))
         ogc->AddVirtual_ANTIDEPS(op_vertex, GET_INDEX_NODE(vop->vdef), AT_DEF);
   }
   if(vop->redef_vuse)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "    Adding redefined variable " + TOSTRING(GET_INDEX_NODE(vop->redef_vuse)));
      ogc->AddVirtual_ANTIDEPS(op_vertex, GET_INDEX_NODE(vop->redef_vuse), AT_DEF);
   }

   /// virtual use
   if(vop->vuse)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "    Adding virtual variable used" + TOSTRING(GET_INDEX_NODE(vop->vuse)));
      //ogc->AddVirtual(op_vertex, GET_INDEX_NODE(vop->vuse), AT_USE);
      const std::vector <tree_nodeRef>::const_iterator lods_it_end = vop->list_of_dep_vuses.end();
      for(std::vector <tree_nodeRef>::const_iterator lods_it = vop->list_of_dep_vuses.begin(); lods_it != lods_it_end; ++lods_it)
      {
         ogc->AddVirtual_DATADEPS(op_vertex, GET_INDEX_NODE(*lods_it), AT_USE);
      }
      ogc->AddVirtual_ANTIDEPS(op_vertex, GET_INDEX_NODE(vop->vuse), AT_USE);
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Analyzed vops");
}

