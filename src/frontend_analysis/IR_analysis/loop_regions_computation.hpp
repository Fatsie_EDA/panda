/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file loop_regions_computation.hpp
 * @brief Analysis step creating the regions for loops. See PDG definition for details.
 * 
 * To easy the parallelization task each header of a loop has to have a unique starting point and all basic blocks of a loop having an
 * edge going towards the header block has to have an unique ending point. This can be obtained quite easily by adding to the tree
 * intermediate representation, a gimple_label working as unique starting point and some gimple_goto working as unique ending point.
 * No modification is required in case 
 * - the last statement is a gimple_goto or a gimple_cond or a gimple_switch
 * - the first statement is a gimple_label.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef LOOP_REGIONS_COMPUTATION_HPP
#define LOOP_REGIONS_COMPUTATION_HPP

///Superclass include
#include "function_frontend_flow_step.hpp"

#include "refcount.hpp"

/**
 * @name forward declarations
 */
//@{
REF_FORWARD_DECL(loop_regions_computation);
REF_FORWARD_DECL(tree_manager);
//@}

/**
 * Compute the regions of a loop.
 */
class loop_regions_computation : public FunctionFrontendFlowStep
{
   private:
      /**
       * Return the set of analyses in relationship with this design step
       * @param relationship_type is the type of relationship to be considered
       */
      const std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const;

   public:

      /**
       * Constructor.
       * @param Param is the set of the parameters
       * @param AppM is the application manager
       * @param function_id is the identifier of the function
       * @param design_flow_manager is the design flow manager
       */
      loop_regions_computation(const ParameterConstRef Param, const application_managerRef AppM, unsigned int function_id, const DesignFlowManagerConstRef design_flow_manager);

      /**
       *  Destructor
       */
      virtual ~loop_regions_computation();

      /**
       * Computes the regions of a loop.
       */
      void Exec();
};

#endif
