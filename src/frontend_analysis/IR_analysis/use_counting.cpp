/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file use_counting.cpp
 * @brief Analysis step counting how many times a ssa_name is used
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "use_counting.hpp"

///Behavior include
#include "application_manager.hpp"

///Parameter include
#include "Parameter.hpp"

///tree include
#include "tree_basic_block.hpp"
#include "tree_helper.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "ext_tree_node.hpp"
#include "tree_reindex.hpp"

use_counting::use_counting(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, USE_COUNTING, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

use_counting::~use_counting()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > use_counting::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(VAR_DECL_FIX, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      case(PRECEDENCE_RELATIONSHIP) :
      {
#if HAVE_ZEBU_BUILT || HAVE_BAMBU_BUILT
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(ARRAY_REF_FIX, SAME_FUNCTION));
#endif
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SPLIT_PHINODES, SAME_FUNCTION));
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void use_counting::Exec()
{
   const tree_managerRef TM = AppM->get_tree_manager();
   tree_nodeRef temp = TM->get_tree_node_const(function_id);
   function_decl * fd = GetPointer<function_decl>(temp);
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));
   std::map<unsigned int, blocRef> & list_of_bloc = sl->list_of_bloc;
   std::map<unsigned int, blocRef>::const_iterator it, it_end = list_of_bloc.end();
   for(it = list_of_bloc.begin(); it != it_end; it++)
   {
      std::list<tree_nodeRef> & list_of_stmt = it->second->list_of_stmt;
      std::list<tree_nodeRef>::iterator it2, it2_end = list_of_stmt.end();
      for(it2 = list_of_stmt.begin(); it2 != it2_end; it2++)
      {
         /// [breadshe] This set contains the ssa_name nodes "used" by the statement
         std::set<tree_nodeRef> ssa_uses;
         PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "***** Analyzing statement " + boost::lexical_cast<std::string>(GET_INDEX_NODE(*it2)));
         analyze_node(*it2, ssa_uses);
         /// [Fabrizio] add the ssa uses to the bloc: needed by the liveness
         it->second->ssa_uses.insert(ssa_uses.begin(), ssa_uses.end());
         PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "***** Analyzed statement " + boost::lexical_cast<std::string>(GET_INDEX_NODE(*it2)));
         /// [breadshe] Add current statement it2 to the use_stmts corresponding to the ssa_name nodes contained in ssa_uses
         for(std::set<tree_nodeRef>::iterator uses_it = ssa_uses.begin(); uses_it != ssa_uses.end(); uses_it++)
         {
            //THROW_ASSERT;
            // Problem: a statement could be inserted in sn->use_stmts more than one time
            ssa_name * sn = GetPointer<ssa_name>(GET_NODE(*uses_it));
            (sn->use_stmts).push_back(*it2);
         }
      }
   }
   std::map<unsigned int, unsigned int>::const_iterator it2, it2_end = counter.end();
   for(it2 = counter.begin(); it2 != it2_end; it2++)
   {
      temp = TM->get_tree_node_const(it2->first);
      THROW_ASSERT(temp->get_kind() == ssa_name_K, "Tree node " + boost::lexical_cast<std::string>(it2->first) + " is not a ssa_name");
      GetPointer<ssa_name>(temp)->uses = it2->second;
   }
}

void use_counting::analyze_node(tree_nodeRef & tn, std::set<tree_nodeRef> & ssa_uses)
{
   THROW_ASSERT(tn->get_kind() == tree_reindex_K, "Node is not a tree reindex");
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Analyzing node " + boost::lexical_cast<std::string>(GET_INDEX_NODE(tn)));
   tree_nodeRef curr_tn = GET_NODE(tn);
   switch (curr_tn->get_kind())
   {
      case gimple_return_K:
      {
         gimple_return* re = GetPointer<gimple_return>(curr_tn);
         if(re->op)
         {
            analyze_node(re->op, ssa_uses);
         }
         break;
      }
      case gimple_assign_K:
      {
         gimple_assign* me = GetPointer<gimple_assign>(curr_tn);
         if(GET_NODE(me->op0)->get_kind() != ssa_name_K)
            analyze_node(me->op0, ssa_uses);
         analyze_node(me->op1, ssa_uses);
         break;
      }
      case gimple_nop_K:
      {
         break;
      }
      case call_expr_K:
      {
         call_expr* ce = GetPointer<call_expr>(curr_tn);
         analyze_node(ce->fn, ssa_uses);
         std::vector<tree_nodeRef> & args = ce->args;
         std::vector<tree_nodeRef>::iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            analyze_node(*arg, ssa_uses);
         }
         break;
      }
      case gimple_call_K:
      {
         gimple_call* ce = GetPointer<gimple_call>(curr_tn);
         analyze_node(ce->fn, ssa_uses);
         std::vector<tree_nodeRef> & args = ce->args;
         std::vector<tree_nodeRef>::iterator arg, arg_end = args.end();
         for(arg = args.begin(); arg != arg_end; arg++)
         {
            analyze_node(*arg, ssa_uses);
         }
         break;
      }
      case gimple_cond_K:
      {
         gimple_cond* gc = GetPointer<gimple_cond>(curr_tn);
         analyze_node(gc->op0, ssa_uses);
         break;
      }
      /* Unary expressions.  */
      case CASE_UNARY_EXPRESSION:
      {
         /*if(curr_tn->get_kind() == addr_expr_K)
         {
            if(already_visited.find(curr_tn) != already_visited.end())
            {
               break;
            }
            already_visited.insert(curr_tn);
         }*/
         unary_expr * ue = GetPointer<unary_expr>(curr_tn);
         analyze_node(ue->op, ssa_uses);
         break;
      }
      case CASE_BINARY_EXPRESSION:
      {
         binary_expr* be = GetPointer<binary_expr>(curr_tn);
         analyze_node(be->op0, ssa_uses);
         analyze_node(be->op1, ssa_uses);
         break;
      }
      /*ternary expressions*/
      case gimple_switch_K:
      {
         gimple_switch* se = GetPointer<gimple_switch>(curr_tn);
         analyze_node(se->op0, ssa_uses);
         break;
      }
      case CASE_TERNARY_EXPRESSION:
      {
         ternary_expr * te = GetPointer<ternary_expr>(curr_tn);
         analyze_node(te->op0, ssa_uses);
         analyze_node(te->op1, ssa_uses);
         if(te->op2)
            analyze_node(te->op2, ssa_uses);
         break;
      }
      case CASE_QUATERNARY_EXPRESSION:
      {
         quaternary_expr * qe = GetPointer<quaternary_expr>(curr_tn);
         analyze_node(qe->op0, ssa_uses);
         analyze_node(qe->op1, ssa_uses);
         if(qe->op2)
            analyze_node(qe->op2, ssa_uses);
         if(qe->op3)
            analyze_node(qe->op3, ssa_uses);
         break;
      }
      case constructor_K:
      {
         constructor * c = GetPointer<constructor>(curr_tn);
         std::vector<std::pair< tree_nodeRef, tree_nodeRef> > &list_of_idx_valu = c->list_of_idx_valu;
         std::vector<std::pair< tree_nodeRef, tree_nodeRef> >::const_iterator vend = list_of_idx_valu.end();
         for (std::vector<std::pair< tree_nodeRef, tree_nodeRef> >::iterator i = list_of_idx_valu.begin(); i != vend; i++)
         {
            analyze_node(i->second, ssa_uses);
         }
         break;
      }
      case var_decl_K:
      {
         ///var decl performs an assignment when init is not null
         var_decl * vd = GetPointer<var_decl>(curr_tn);
         if(vd->init)
            analyze_node(vd->init, ssa_uses);
         break;
      }
      case gimple_asm_K:
      {
         gimple_asm * ae = GetPointer<gimple_asm>(curr_tn);
         if(ae->out)
            analyze_node(ae->out, ssa_uses);
         if(ae->in)
            analyze_node(ae->in, ssa_uses);
         if(ae->clob)
            analyze_node(ae->in, ssa_uses);
         break;
      }
      case gimple_goto_K:
      {
         gimple_goto * ge = GetPointer<gimple_goto>(curr_tn);
         analyze_node(ge->op, ssa_uses);
         break;
      }
      case tree_list_K:
      {
         tree_list * tl = GetPointer<tree_list>(curr_tn);
         if(tl->purp)
            analyze_node(tl->purp, ssa_uses);
         if(tl->valu)
            analyze_node(tl->valu, ssa_uses);
         if(tl->chan)
            analyze_node(tl->chan, ssa_uses);
         break;
      }
      case gimple_multi_way_if_K:
      {
         gimple_multi_way_if* gmwi=GetPointer<gimple_multi_way_if>(curr_tn);
         const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it_end = gmwi->list_of_cond.end();
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it = gmwi->list_of_cond.begin(); gmwi_it != gmwi_it_end; ++gmwi_it)
            if((*gmwi_it).first)
               analyze_node((*gmwi_it).first, ssa_uses);
         break;
      }
      case result_decl_K:
      case parm_decl_K:
      case function_decl_K:
      case integer_cst_K:
      case real_cst_K:
      case string_cst_K:
      case vector_cst_K:
      case complex_cst_K:
      case field_decl_K:
      case label_decl_K:
      case gimple_label_K:
      case gimple_phi_K:
      case gimple_pragma_K:
      case gimple_resx_K:
      case CASE_PRAGMA_NODES:
      {
         break;
      }
      case target_mem_ref_K:
      {
         target_mem_ref* tmr = GetPointer<target_mem_ref>(curr_tn);
         if(tmr->base)
            analyze_node(tmr->base, ssa_uses);
         if(tmr->symbol)
            analyze_node(tmr->symbol, ssa_uses);
         if(tmr->idx)
            analyze_node(tmr->idx, ssa_uses);
         /// step and offset are constants
         break;
      }
      case target_mem_ref461_K:
      {
         target_mem_ref461* tmr = GetPointer<target_mem_ref461>(curr_tn);
         if(tmr->base)
            analyze_node(tmr->base, ssa_uses);
         if(tmr->idx)
            analyze_node(tmr->idx, ssa_uses);
         if(tmr->idx2)
            analyze_node(tmr->idx2, ssa_uses);
         /// step and offset are constants
         break;
      }
      case ssa_name_K:
      {
         counter[GET_INDEX_NODE(tn)]++;
         ssa_uses.insert(tn);
         break;
      }
      case binfo_K:
      case block_K:
      case case_label_expr_K:
      case CASE_CPP_NODES:
      case const_decl_K:
      case CASE_FAKE_NODES:
      case gimple_bind_K:
      case gimple_for_K:
      case gimple_predict_K:
      case gimple_while_K:
      case identifier_node_K:
      case namespace_decl_K:
      case statement_list_K:
      case translation_unit_decl_K:
      case tree_vec_K:
      case type_decl_K:
      case CASE_TYPE_NODES:
      {
         THROW_UNREACHABLE("Node is " + curr_tn->get_kind_text());
         break;
      }
      default:
         THROW_UNREACHABLE("");

   }
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Analyzed node " + boost::lexical_cast<std::string>(GET_INDEX_NODE(tn)));
}
