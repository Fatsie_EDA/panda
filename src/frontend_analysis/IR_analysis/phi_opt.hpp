/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file phi_opt.hpp
 * @brief Analysis step that optimize the phis starting from the GCC IR
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef PHI_OPT_HPP
#define PHI_OPT_HPP

///Superclass include
#include "function_frontend_flow_step.hpp"

///Utility include
#include "refcount.hpp"

/**
 * @name forward declarations
 */
//@{
REF_FORWARD_DECL(tree_manager);
REF_FORWARD_DECL(tree_node);
REF_FORWARD_DECL(bloc);
class statement_list;
class gimple_phi;
//@}

/**
 * Restructure the tree control flow graph
 */
class phi_opt: public FunctionFrontendFlowStep
{
      friend class short_circuit_taf;
   private:

      /// flag to check if initial tree has been dumped
      static bool tree_dumped;

      /// flag used to restart code motion step
      bool restart_code_motion;

      /**
       * Return the set of analyses in relationship with this design step
       * @param relationship_type is the type of relationship to be considered
       */
      virtual const std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const;

      /**
       * replace where possible phi with cond_expr
       * @param empty_bb_candidate_list seed for phi replacement
       * @param list_of_bloc is the list of basic blocks
       * @param list_of_bloc is the list of basic blocks to be removed
       */
      void predicate_phi_scalar_subst(std::list<unsigned int>&empty_bb_candidate_list, std::map<unsigned int, blocRef> &list_of_bloc, std::set<unsigned int> &to_be_removed, const tree_managerRef TM);

      /**
       * update the CFG by removing curr_bb
       * @param succ
       * @param curr_bb
       * @param pred
       * @param list_of_bloc
       */
      void update_cfg(unsigned int succ, unsigned int curr_bb, unsigned int pred, std::map<unsigned int, blocRef>& list_of_bloc);

      void create_cond_expr_from_phi(unsigned int succ, std::set<unsigned int> &to_be_removed, const tree_managerRef TM, std::map<unsigned int, blocRef>& list_of_bloc, unsigned int pred, std::vector<tree_nodeRef>& list_of_phi_succ, unsigned int curr_bb);

      tree_nodeRef create_cond_expr_from_phi_simple(const tree_managerRef TM, std::map<unsigned int, blocRef>& list_of_bloc, unsigned int pred, std::pair< tree_nodeRef, unsigned int>& edge_def1, std::pair< tree_nodeRef, unsigned int>& edge_def2, gimple_phi *phi, unsigned int phi_index, unsigned int &edge_def1_cond, unsigned int &edge_def2_cond);

      void compute_ssa_uses_rec_ref(tree_nodeRef & tn, std::map<unsigned int, std::vector<tree_nodeRef* > > &ssa_uses);

      bool check_phi_combine(unsigned int curr_bb, std::map<unsigned int, blocRef>& list_of_bloc, std::map<unsigned int, std::vector<tree_nodeRef*> > &stmt_ssa_uses, std::map<unsigned int, std::set<unsigned int> > &phi_bb_ssa_uses);

      void single_phi_substitution(std::map<unsigned int, blocRef>& list_of_bloc, std::map<unsigned int, std::vector<tree_nodeRef*> > &stmt_ssa_uses, std::map<unsigned int, std::set<unsigned int> > &phi_bb_ssa_uses, const tree_managerRef TM);

      void clean_CFG(std::map<unsigned int, blocRef>& list_of_bloc, std::list<unsigned int>& empty_bb_candidate_list, std::set<unsigned int> &to_be_removed);

      static
      void fix_multi_way_if(unsigned int curr_bb, std::map<unsigned int, blocRef>& list_of_bloc, unsigned int succ);

   public:
      /**
       * Constructor.
       * @param AppM is the application manager
       * @param function_id is the identifier of the function
       * @param design_flow_manager is the design flow manager
       * @param parameters is the set of input parameters
       */
      phi_opt(const application_managerRef AppM, unsigned int function_id, const DesignFlowManagerConstRef design_flow_manager, const ParameterConstRef parameters);

      /**
       *  Destructor
       */
      ~phi_opt();

      /**
       * Updates the tree to have a more compliant CFG
       */
      void Exec();

};
#endif
