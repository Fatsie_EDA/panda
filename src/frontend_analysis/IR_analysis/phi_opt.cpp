/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file phi_opt.cpp
 * @brief Analysis step that improves the GCC IR w.r.t. phis
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */

///header include
#include "phi_opt.hpp"

#include "application_manager.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <fstream>

///Tree include
#include "tree_basic_block.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "ext_tree_node.hpp"
#include "tree_reindex.hpp"
#include "tree_helper.hpp"

///Utility include
#include "dbgPrintHelper.hpp"

#define TRUE_CONDITION (std::numeric_limits<unsigned int>::max())
#define FALSE_CONDITION (std::numeric_limits<unsigned int>::max()-1)
#define DEFAULT_CONDITION (std::numeric_limits<unsigned int>::max()-2)
phi_opt::phi_opt(const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager, const ParameterConstRef _parameters) :
   FunctionFrontendFlowStep(_AppM, _function_id, PHI_OPT, _design_flow_manager, _parameters),
   restart_code_motion(false)
{
   debug_level = _parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

phi_opt::~phi_opt()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FunctionFrontendFlowStep::FunctionRelationship> > phi_opt::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BLOCK_FIX, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SWITCH_FIX, SAME_FUNCTION));
         break;
      }
      case(PRECEDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SIMPLE_CODE_MOTION, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      {
         if(restart_code_motion)
            relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SIMPLE_CODE_MOTION, SAME_FUNCTION));
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(CHECK_SYSTEM_TYPE, SAME_FUNCTION));
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void phi_opt::update_cfg(unsigned int succ, unsigned int curr_bb, unsigned int pred, std::map<unsigned int, blocRef>& list_of_bloc)
{
   if(list_of_bloc[succ]->list_of_pred.begin() != list_of_bloc[succ]->list_of_pred.end())
   {
      while(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), curr_bb) != list_of_bloc[succ]->list_of_pred.end())
         list_of_bloc[succ]->list_of_pred.erase(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), curr_bb));
   }
   if(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), pred) == list_of_bloc[succ]->list_of_pred.end())
      list_of_bloc[succ]->list_of_pred.push_back(pred);
   std::vector<tree_nodeRef> & list_of_phi_succ = list_of_bloc[succ]->list_of_phi;
   std::vector<tree_nodeRef>::iterator phi, phi_end = list_of_phi_succ.end();
   for(phi = list_of_phi_succ.begin(); phi != phi_end; phi++)
   {
      gimple_phi * current_phi = GetPointer<gimple_phi>(GET_NODE(*phi));
      std::vector<std::pair< tree_nodeRef, unsigned int> > & list_of_def_edge = current_phi->list_of_def_edge;
      std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator definition, definition_end = list_of_def_edge.end();
      for(definition = list_of_def_edge.begin(); definition != definition_end; definition++)
      {
         if(definition->second == curr_bb)
            definition->second = pred;
      }
   }
    fix_multi_way_if(curr_bb, list_of_bloc, succ);
}

bool phi_opt::check_phi_combine(unsigned int curr_bb, std::map<unsigned int, blocRef>& list_of_bloc, std::map<unsigned int, std::vector<tree_nodeRef*> > &stmt_ssa_uses, std::map<unsigned int, std::set<unsigned int> > &phi_bb_ssa_uses)
{
   if(curr_bb == bloc::ENTRY_BLOCK_ID || curr_bb == bloc::EXIT_BLOCK_ID) return false;
   bool is_a_candidate = list_of_bloc[curr_bb]->list_of_stmt.empty();
   if(!is_a_candidate)
      return false;
   bool has_entry = std::find(list_of_bloc[curr_bb]->list_of_pred.begin(), list_of_bloc[curr_bb]->list_of_pred.end(), bloc::ENTRY_BLOCK_ID) != list_of_bloc[curr_bb]->list_of_pred.end();
   if(has_entry)
      return false;
   THROW_ASSERT(list_of_bloc[curr_bb]->list_of_succ.size() == 1, "unexpected condition " + STR(curr_bb));
   unsigned int succ = list_of_bloc[curr_bb]->list_of_succ.front();
   if(succ == curr_bb) return false;
   if(list_of_bloc[curr_bb]->loop_id == list_of_bloc[succ]->loop_id)
   {
      std::vector<tree_nodeRef> & list_of_phi_curr_bb = list_of_bloc[curr_bb]->list_of_phi;
      const size_t n_curr_bb_phis = list_of_phi_curr_bb.size();
      for(size_t curr_bb_phi_index = 0; curr_bb_phi_index < n_curr_bb_phis; ++curr_bb_phi_index)
      {
         gimple_phi *cb_phi = GetPointer<gimple_phi>(GET_NODE(list_of_phi_curr_bb[curr_bb_phi_index]));
         unsigned int def_index = GET_INDEX_NODE(cb_phi->res);
         if(stmt_ssa_uses.find(def_index) != stmt_ssa_uses.end())
            return false;
         THROW_ASSERT(phi_bb_ssa_uses.find(def_index) != phi_bb_ssa_uses.end(), "unexpected condition " + STR(def_index));
         if(phi_bb_ssa_uses.find(def_index)->second.size() != 1 || phi_bb_ssa_uses.find(def_index)->second.find(succ) == phi_bb_ssa_uses.find(def_index)->second.end())
            return false;
      }

      return true;
   }
   else
      return false;
}

void phi_opt::single_phi_substitution(std::map<unsigned int, blocRef>& list_of_bloc, std::map<unsigned int, std::vector<tree_nodeRef*> > &stmt_ssa_uses, std::map<unsigned int, std::set<unsigned int> > &phi_bb_ssa_uses, const tree_managerRef TM)
{
   std::set<unsigned int> BBs_single_def_edge;
   std::map<unsigned int, std::vector<tree_nodeRef*> > phi_ssa_uses;
   const std::map<unsigned int, blocRef>::iterator it3_end = list_of_bloc.end();
   for(std::map<unsigned int, blocRef>::iterator it3 = list_of_bloc.begin(); it3 != it3_end; ++it3)
   {
      const std::vector<tree_nodeRef> & list_of_phi = it3->second->list_of_phi;
      const std::vector<tree_nodeRef>::const_iterator lop_it_end = list_of_phi.end();
      for(std::vector<tree_nodeRef>::const_iterator lop_it = list_of_phi.begin(); lop_it_end != lop_it; ++lop_it)
      {
         gimple_phi *phi = GetPointer<gimple_phi>(GET_NODE(*lop_it));
         if(phi->list_of_def_edge.size()==1)
            BBs_single_def_edge.insert(it3->first);
         const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_it_end = phi->list_of_def_edge.end();
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_it = phi->list_of_def_edge.begin(); lode_it_end != lode_it; ++lode_it)
         {
            if(GetPointer<ssa_name>(GET_NODE(lode_it->first)))
            {
               unsigned int ssa_index = GET_INDEX_NODE(lode_it->first);

               phi_ssa_uses[ssa_index].push_back(&lode_it->first);
               phi_bb_ssa_uses[ssa_index].insert(it3->first);
            }
         }
      }
      std::list<tree_nodeRef> & list_of_stmt = it3->second->list_of_stmt;
      const std::list<tree_nodeRef>::iterator los_it_end = list_of_stmt.end();
      for(std::list<tree_nodeRef>::iterator los_it = list_of_stmt.begin(); los_it != los_it_end; ++los_it)
      {
         tree_helper::compute_ssa_uses_rec_ref(*los_it, stmt_ssa_uses);
      }
   }
   /// substitute single def_edge phis
   const std::set<unsigned int>::const_iterator bsde_it_end = BBs_single_def_edge.end();
   for(std::set<unsigned int>::const_iterator bsde_it = BBs_single_def_edge.begin(); bsde_it != bsde_it_end; ++bsde_it)
   {
      unsigned int curr_bb = *bsde_it;
      std::vector<tree_nodeRef> & list_of_phi_curr_bb = list_of_bloc[curr_bb]->list_of_phi;
      const size_t n_curr_bb_phis = list_of_phi_curr_bb.size();
      for(size_t curr_bb_phi_index = 0; curr_bb_phi_index < n_curr_bb_phis; ++curr_bb_phi_index)
      {
         gimple_phi *cb_phi = GetPointer<gimple_phi>(GET_NODE(list_of_phi_curr_bb[curr_bb_phi_index]));
         tree_nodeRef right_value = cb_phi->list_of_def_edge.front().first;
         unsigned int def_index = GET_INDEX_NODE(cb_phi->res);
         if(phi_ssa_uses.find(def_index) != phi_ssa_uses.end())
         {
            restart_code_motion = true;
            std::vector<tree_nodeRef*> & uses = phi_ssa_uses.find(def_index)->second;
            const std::vector<tree_nodeRef*>::iterator u_it_end = uses.end();
            for(std::vector<tree_nodeRef*>::iterator u_it = uses.begin(); u_it != u_it_end; ++u_it)
               *(*u_it) = TM->GetTreeReindex(GET_INDEX_NODE(right_value));
         }
         if(stmt_ssa_uses.find(def_index) != stmt_ssa_uses.end())
         {
            restart_code_motion = true;
            std::vector<tree_nodeRef*> & uses = stmt_ssa_uses.find(def_index)->second;
            const std::vector<tree_nodeRef*>::iterator u_it_end = uses.end();
            for(std::vector<tree_nodeRef*>::iterator u_it = uses.begin(); u_it != u_it_end; ++u_it)
               *(*u_it) = TM->GetTreeReindex(GET_INDEX_NODE(right_value));
         }
      }
      list_of_phi_curr_bb.clear();
   }
}

void phi_opt::clean_CFG(std::map<unsigned int, blocRef>& list_of_bloc, std::list<unsigned int>& empty_bb_candidate_list, std::set<unsigned int> &to_be_removed)
{
   std::map<unsigned int, blocRef>::iterator it3_end = list_of_bloc.end();
   for(std::map<unsigned int, blocRef>::iterator it3 = list_of_bloc.begin(); it3 != it3_end; it3++)
   {

      std::list<tree_nodeRef> & list_of_stmt = it3->second->list_of_stmt;
      unsigned int curr_bb = it3->first;
      if(to_be_removed.find(curr_bb) != to_be_removed.end()) continue;
      if(curr_bb == bloc::EXIT_BLOCK_ID) continue;
      if(curr_bb == bloc::ENTRY_BLOCK_ID) continue;
      if(list_of_stmt.empty() && it3->second->list_of_phi.empty() && list_of_bloc[curr_bb]->list_of_pred.size() == 1 && list_of_bloc[curr_bb]->list_of_succ.size() == 1)
      {
         unsigned int pred = list_of_bloc[curr_bb]->list_of_pred.front();
         if(pred == bloc::ENTRY_BLOCK_ID) continue;
         unsigned int succ = list_of_bloc[curr_bb]->list_of_succ.front();

         if(list_of_bloc[pred]->true_edge == curr_bb && list_of_bloc[pred]->false_edge == succ)
         {
            empty_bb_candidate_list.push_back(curr_bb);
            continue;
         }
         if(list_of_bloc[pred]->false_edge == curr_bb && list_of_bloc[pred]->true_edge == succ)
         {
            empty_bb_candidate_list.push_back(curr_bb);
            continue;
         }
         if(succ == curr_bb)
            continue;
         /// the next check fixes this testcase:
         /// /opt/panda/bin/bambu -lm -O2 -fwhole-program --simulate=gcc_regression_simple/test.xml  --benchmark-name=gcc_regression_simple ./gcc_regression_simple/pr24716.c
         if(list_of_bloc[succ]->list_of_stmt.empty() && list_of_bloc[succ]->list_of_phi.empty() &&
               ((list_of_bloc[pred]->true_edge == curr_bb && list_of_bloc[list_of_bloc[pred]->false_edge]->list_of_stmt.empty() && list_of_bloc[list_of_bloc[pred]->false_edge]->list_of_phi.empty()) ||
                (list_of_bloc[pred]->false_edge == curr_bb && list_of_bloc[list_of_bloc[pred]->true_edge]->list_of_stmt.empty() && list_of_bloc[list_of_bloc[pred]->true_edge]->list_of_phi.empty())))
            continue;

         to_be_removed.insert(curr_bb);
         restart_code_motion = true;
         while(std::find(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end(), curr_bb) != list_of_bloc[pred]->list_of_succ.end())
            list_of_bloc[pred]->list_of_succ.erase(std::find(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end(), curr_bb));
         list_of_bloc[pred]->list_of_succ.push_back(succ);
         update_cfg(succ, curr_bb, pred, list_of_bloc);
         if(list_of_bloc[pred]->true_edge == curr_bb)
            list_of_bloc[pred]->true_edge = succ;
         else if (list_of_bloc[pred]->false_edge == curr_bb)
            list_of_bloc[pred]->false_edge = succ;
      }
      else if(list_of_bloc[curr_bb]->list_of_succ.size() == 1)
      {
         unsigned int succ = list_of_bloc[curr_bb]->list_of_succ.front();
         if(succ != bloc::EXIT_BLOCK_ID &&
               list_of_bloc[curr_bb]->loop_id == list_of_bloc[succ]->loop_id &&
               list_of_bloc[succ]->list_of_pred.size() == 1 &&
               !list_of_bloc[curr_bb]->list_of_stmt.empty() &&
               list_of_bloc[succ]->list_of_phi.size() == 0)
         {
            tree_nodeRef last_statement = list_of_bloc[curr_bb]->list_of_stmt.back();
            if(GET_NODE(last_statement)->get_kind() != gimple_goto_K)
            {
               /// add succ statements to curr_bb
               std::list<tree_nodeRef>& list_of_curr_bb_stmt = list_of_bloc[curr_bb]->list_of_stmt;
               std::list<tree_nodeRef>& list_of_succ_stmt = list_of_bloc[succ]->list_of_stmt;
               std::list<tree_nodeRef>::const_iterator loss_it_end = list_of_succ_stmt.end();
               for(std::list<tree_nodeRef>::const_iterator loss_it = list_of_succ_stmt.begin(); loss_it != loss_it_end; ++loss_it)
               {
                  GetPointer<gimple_node>(GET_NODE(*loss_it))->bb_index = curr_bb;
                  list_of_curr_bb_stmt.push_back(*loss_it);
               }
               /// update list of succ of curr_bb
               std::vector<unsigned int>::const_iterator los_it_end = list_of_bloc[succ]->list_of_succ.end();
               for(std::vector<unsigned int>::const_iterator los_it = list_of_bloc[succ]->list_of_succ.begin(); los_it != los_it_end; ++los_it)
                  update_cfg(*los_it, succ, curr_bb, list_of_bloc);
               list_of_bloc[curr_bb]->list_of_succ = list_of_bloc[succ]->list_of_succ;
               ///update true and false edges
               list_of_bloc[curr_bb]->true_edge = list_of_bloc[succ]->true_edge;
               list_of_bloc[curr_bb]->false_edge = list_of_bloc[succ]->false_edge;
               /// update live out of curr_bb
               list_of_bloc[curr_bb]->live_out = list_of_bloc[succ]->live_out;
               /// update ssa_uses of curr_bb
               list_of_bloc[curr_bb]->ssa_uses.insert(list_of_bloc[succ]->ssa_uses.begin(), list_of_bloc[succ]->ssa_uses.end());
               ///remove succ
               to_be_removed.insert(succ);
               restart_code_motion = true;
            }
         }

      }
   }
}

void phi_opt::fix_multi_way_if(unsigned int curr_bb, std::map<unsigned int, blocRef>& list_of_bloc, unsigned int succ)
{
   std::vector<unsigned int>::const_iterator lop_it_end = list_of_bloc[curr_bb]->list_of_pred.end();
   for(std::vector<unsigned int>::iterator lop_it = list_of_bloc[curr_bb]->list_of_pred.begin(); lop_it_end != lop_it; ++lop_it)
   {
      std::list<tree_nodeRef>& list_of_pred_stmt = list_of_bloc[*lop_it]->list_of_stmt;
      tree_nodeRef cond_statement = list_of_pred_stmt.begin() != list_of_pred_stmt.end() ? list_of_pred_stmt.back() : tree_nodeRef();
      tree_nodeRef cond_statement_node = cond_statement ? GET_NODE(cond_statement) : cond_statement;
      if(cond_statement_node && GetPointer<gimple_multi_way_if>(cond_statement_node))
      {
         gimple_multi_way_if * gmwi = GetPointer<gimple_multi_way_if>(cond_statement_node);
         const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it_end = gmwi->list_of_cond.end();
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it = gmwi->list_of_cond.begin(); gmwi_it != gmwi_it_end; ++gmwi_it)
            if(gmwi_it->second == curr_bb)
               gmwi_it->second = succ;
      }
   }
}

static
unsigned int return_edge_id(unsigned int curr_bb, unsigned int pred, std::map<unsigned int, blocRef> & list_of_bloc)
{
   std::list<tree_nodeRef>& list_of_pred_stmt = list_of_bloc[pred]->list_of_stmt;
   if(list_of_pred_stmt.empty())
      return 0;
   tree_nodeRef cond_statement = list_of_pred_stmt.back();
   ///extract the condition from the gimple_cond
   gimple_cond *gc = GetPointer<gimple_cond>(GET_NODE(cond_statement));
   if(gc)
   {
      if(list_of_bloc[pred]->true_edge == curr_bb)
         return TRUE_CONDITION;
      if(list_of_bloc[pred]->false_edge == curr_bb)
         return FALSE_CONDITION;
      THROW_UNREACHABLE("unexpected case");
   }
   else
   {
      gimple_multi_way_if *gmwi = GetPointer<gimple_multi_way_if>(GET_NODE(cond_statement));
      if(!gmwi)
         return 0;
      const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it_end = gmwi->list_of_cond.end();
      for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it = gmwi->list_of_cond.begin(); gmwi_it != gmwi_it_end; ++gmwi_it)
         if(gmwi_it->second == curr_bb)
         {
            if((*gmwi_it).first)
               return GET_INDEX_NODE((*gmwi_it).first);
            else
               return DEFAULT_CONDITION;
         }
      THROW_UNREACHABLE("unexpected case");
   }
   THROW_UNREACHABLE("unexpected case");
   return 0;
}

static
unsigned int get_edge_id(std::map<unsigned int, std::set<std::pair< tree_nodeRef, unsigned int> > > &cond_edges_set, std::pair< tree_nodeRef, unsigned int> & edge)
{
   std::map<unsigned int, std::set<std::pair< tree_nodeRef, unsigned int> > >::const_iterator ces_it_end = cond_edges_set.end();
   for(std::map<unsigned int, std::set<std::pair< tree_nodeRef, unsigned int> > >::const_iterator ces_it = cond_edges_set.begin(); ces_it != ces_it_end; ++ces_it)
      if(ces_it->second.find(edge) != ces_it->second.end())
         return ces_it->first;
   return 0;
}

void phi_opt::Exec()
{
   const tree_managerRef TM = AppM->get_tree_manager();
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }
   restart_code_motion = false;
   tree_nodeRef temp = TM->get_tree_node_const(function_id);
   function_decl * fd = GetPointer<function_decl>(temp);
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));

   std::map<unsigned int, blocRef> & list_of_bloc = sl->list_of_bloc;
   std::map<unsigned int, blocRef>::iterator it3, it3_end = list_of_bloc.end();
   std::list<unsigned int> empty_bb_candidate_list;

   std::set<unsigned int> to_be_removed;

   /// compute uses
   /// and collect BBs having phi with a single def_edge
   std::map<unsigned int, std::set<unsigned int> > phi_bb_ssa_uses;
   std::map<unsigned int, std::vector<tree_nodeRef*> > stmt_ssa_uses;

   single_phi_substitution(list_of_bloc, stmt_ssa_uses, phi_bb_ssa_uses, TM);

#if 1
   /// combine simple phi-based basic-blocks
   for(it3 = list_of_bloc.begin(); it3 != it3_end; it3++)
   {
      unsigned int curr_bb = it3->first;
      if(check_phi_combine(curr_bb, list_of_bloc, stmt_ssa_uses, phi_bb_ssa_uses))
      {
         unsigned int succ = list_of_bloc[curr_bb]->list_of_succ.front();
         std::vector<tree_nodeRef> & list_of_phi_curr_bb = list_of_bloc[curr_bb]->list_of_phi;
         std::vector<tree_nodeRef> & list_of_phi_succ = list_of_bloc[succ]->list_of_phi;
         std::vector<tree_nodeRef>::const_iterator lops_it_end = list_of_phi_succ.end();
         std::map<unsigned int, std::set<std::pair< tree_nodeRef, unsigned int> > >cond_edges_set;
         const size_t n_curr_bb_phis = list_of_phi_curr_bb.size();
         std::set<size_t> curr_bb_analyzed;
         for(size_t curr_bb_phi_index = 0; curr_bb_phi_index < n_curr_bb_phis; ++curr_bb_phi_index)
         {
            gimple_phi *cb_phi = GetPointer<gimple_phi>(GET_NODE(list_of_phi_curr_bb[curr_bb_phi_index]));
            for(std::vector<tree_nodeRef>::const_iterator lops_it = list_of_phi_succ.begin(); lops_it_end != lops_it; ++lops_it)
            {
               gimple_phi *sphi = GetPointer<gimple_phi>(GET_NODE(*lops_it));
               const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_s_it_end = sphi->list_of_def_edge.end();
               std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_curr_bb=lode_s_it_end;
               std::list< std::pair< tree_nodeRef, unsigned int> > phi_to_be_added;
               std::list< std::pair< tree_nodeRef, unsigned int> > phi_to_be_removed;
               for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_s_it = sphi->list_of_def_edge.begin(); lode_s_it_end != lode_s_it; ++lode_s_it)
               {
                  if(lode_s_it->second == curr_bb)
                  {
                     if(GET_INDEX_NODE(lode_s_it->first) == GET_INDEX_NODE(cb_phi->res))
                     {
                        if(curr_bb_analyzed.find(curr_bb_phi_index) == curr_bb_analyzed.end())
                           curr_bb_analyzed.insert(curr_bb_phi_index);
                        lode_curr_bb = lode_s_it;
                        phi_to_be_removed.push_back(*lode_curr_bb);
                        const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_cb_it_end = cb_phi->list_of_def_edge.end();
                        for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_cb_it = cb_phi->list_of_def_edge.begin(); lode_cb_it_end != lode_cb_it; ++lode_cb_it)
                        {
                           phi_to_be_added.push_back(*lode_cb_it);
                           cond_edges_set[return_edge_id(curr_bb, lode_cb_it->second, list_of_bloc)].insert(*lode_cb_it);
                        }
                     }
                  }
               }
               if(!phi_to_be_removed.empty())
               {
                  const std::list< std::pair< tree_nodeRef, unsigned int> >::const_iterator ptbr_it_end = phi_to_be_removed.end();
                  for(std::list< std::pair< tree_nodeRef, unsigned int> >::const_iterator ptbr_it = phi_to_be_removed.begin(); ptbr_it_end != ptbr_it; ++ptbr_it)
                  {
                     THROW_ASSERT(std::find(sphi->list_of_def_edge.begin(), sphi->list_of_def_edge.end(), *ptbr_it) != sphi->list_of_def_edge.end(), "unexpected condition");
                     sphi->list_of_def_edge.erase(std::find(sphi->list_of_def_edge.begin(), sphi->list_of_def_edge.end(), *ptbr_it));
                  }
                  const std::list< std::pair< tree_nodeRef, unsigned int> >::const_iterator ptba_it_end = phi_to_be_added.end();
                  for(std::list< std::pair< tree_nodeRef, unsigned int> >::const_iterator ptba_it = phi_to_be_added.begin(); ptba_it_end != ptba_it; ++ptba_it)
                  {
                     sphi->list_of_def_edge.push_back(*ptba_it);
                  }
               }
            }
         }
         to_be_removed.insert(curr_bb);
         restart_code_motion = true;

         ///fix the def_edges still coming from curr_bb
         for(std::vector<tree_nodeRef>::const_iterator lops_it = list_of_phi_succ.begin(); lops_it_end != lops_it; ++lops_it)
         {
            gimple_phi *sphi = GetPointer<gimple_phi>(GET_NODE(*lops_it));
            /// find the candidate_def_edge
            const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_s_it_end = sphi->list_of_def_edge.end();
            std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_curr_bb=lode_s_it_end;
            for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_s_it = sphi->list_of_def_edge.begin(); lode_s_it_end != lode_s_it; ++lode_s_it)
            {
               if(lode_s_it->second == curr_bb)
                  lode_curr_bb = lode_s_it;
            }
            if(lode_curr_bb!=lode_s_it_end)
            {
               for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_s_it = sphi->list_of_def_edge.begin(); lode_s_it_end != lode_s_it; ++lode_s_it)
               {
                  if(lode_s_it->second != curr_bb)
                  {
                     cond_edges_set[return_edge_id(succ, lode_s_it->second, list_of_bloc)].insert(*lode_s_it);
                  }
               }
               std::pair< tree_nodeRef, unsigned int> edge_def = *lode_curr_bb;
               THROW_ASSERT(std::find(sphi->list_of_def_edge.begin(), sphi->list_of_def_edge.end(), edge_def) != sphi->list_of_def_edge.end(), "unexpected condition");
               sphi->list_of_def_edge.erase(std::find(sphi->list_of_def_edge.begin(), sphi->list_of_def_edge.end(), edge_def));
               std::vector<unsigned int>::const_iterator lop_it_end = list_of_bloc[curr_bb]->list_of_pred.end();
               for(std::vector<unsigned int>::iterator lop_it = list_of_bloc[curr_bb]->list_of_pred.begin(); lop_it_end != lop_it; ++lop_it)
               {
                  edge_def.second = *lop_it;
                  cond_edges_set[return_edge_id(curr_bb, *lop_it, list_of_bloc)].insert(edge_def);
                  sphi->list_of_def_edge.push_back(edge_def);
               }
            }
         }

         /// check for BB with gimple_multi_way_if
         fix_multi_way_if(curr_bb, list_of_bloc, succ);

         ///convert multi-edges in cond_expr
         for(std::vector<tree_nodeRef>::const_iterator lops_it = list_of_phi_succ.begin(); lops_it_end != lops_it; ++lops_it)
         {
            gimple_phi *sphi = GetPointer<gimple_phi>(GET_NODE(*lops_it));
            std::map<unsigned int, std::pair< tree_nodeRef, unsigned int> > already_seen;
            std::list<std::pair< tree_nodeRef, unsigned int> > to_be_removed_def_edges;
            std::list<std::pair< tree_nodeRef, unsigned int> > to_be_added_def_edges;
            const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_s_it_end = sphi->list_of_def_edge.end();
            for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_s_it = sphi->list_of_def_edge.begin(); lode_s_it_end != lode_s_it; ++lode_s_it)
            {
               if(already_seen.find(lode_s_it->second) != already_seen.end())
               {
                  if(sphi->virtual_flag)
                  {
                     THROW_ASSERT(GET_INDEX_NODE(lode_s_it->first) == GET_INDEX_NODE(already_seen.find(lode_s_it->second)->second.first), "unexpected condition");
                     to_be_removed_def_edges.push_back(*lode_s_it);
                  }
                  else
                  {
                     std::pair< tree_nodeRef, unsigned int> edge_def1 = *lode_s_it;
                     std::pair< tree_nodeRef, unsigned int> edge_def2 = already_seen.find(lode_s_it->second)->second;
                     unsigned int edge_def1_cond = get_edge_id(cond_edges_set, edge_def1);
                     unsigned int edge_def2_cond = get_edge_id(cond_edges_set, edge_def2);
                     to_be_removed_def_edges.push_back(edge_def1);
                     to_be_removed_def_edges.push_back(edge_def2);
                     tree_nodeRef res_node = create_cond_expr_from_phi_simple(TM, list_of_bloc, edge_def1.second, edge_def1, edge_def2, sphi, GET_INDEX_NODE(*lops_it), edge_def1_cond, edge_def2_cond);
                     std::pair< tree_nodeRef, unsigned int> edge_def3;
                     edge_def3.first = res_node;
                     edge_def3.second = edge_def1.second;
                     to_be_added_def_edges.push_back(edge_def3);
                     cond_edges_set[edge_def1_cond].insert(edge_def3);
                     already_seen.erase(lode_s_it->second);
                  }
               }
               else
                  already_seen[lode_s_it->second] = *lode_s_it;
            }
            const std::list<std::pair< tree_nodeRef, unsigned int> >::iterator tbrde_it_end = to_be_removed_def_edges.end();
            for(std::list<std::pair< tree_nodeRef, unsigned int> >::iterator tbrde_it = to_be_removed_def_edges.begin(); tbrde_it_end != tbrde_it; ++tbrde_it)
               sphi->list_of_def_edge.erase(std::find(sphi->list_of_def_edge.begin(), sphi->list_of_def_edge.end(), *tbrde_it));
            const std::list<std::pair< tree_nodeRef, unsigned int> >::iterator tbade_it_end = to_be_added_def_edges.end();
            for(std::list<std::pair< tree_nodeRef, unsigned int> >::iterator tbade_it = to_be_added_def_edges.begin(); tbade_it_end != tbade_it; ++tbade_it)
               sphi->list_of_def_edge.push_back(*tbade_it);
            if(already_seen.find(curr_bb) != already_seen.end())
            {
               std::pair< tree_nodeRef, unsigned int> edge_def = already_seen.find(curr_bb)->second;
               sphi->list_of_def_edge.erase(std::find(sphi->list_of_def_edge.begin(), sphi->list_of_def_edge.end(), edge_def));
               std::vector<unsigned int>::const_iterator lop_it_end = list_of_bloc[curr_bb]->list_of_pred.end();
               for(std::vector<unsigned int>::const_iterator lop_it = list_of_bloc[curr_bb]->list_of_pred.begin(); lop_it != lop_it_end; ++lop_it)
               {
                  edge_def.second = *lop_it;
                  sphi->list_of_def_edge.push_back(edge_def);
               }
            }
         }
         std::vector<unsigned int>::const_iterator lop_it_end = list_of_bloc[curr_bb]->list_of_pred.end();
         for(std::vector<unsigned int>::const_iterator lop_it = list_of_bloc[curr_bb]->list_of_pred.begin(); lop_it != lop_it_end && curr_bb_analyzed.size() == n_curr_bb_phis; ++lop_it)
         {
            unsigned int pred = *lop_it;

            if(list_of_bloc[pred]->true_edge == curr_bb)
               list_of_bloc[pred]->true_edge = succ;
            if(list_of_bloc[pred]->false_edge == curr_bb)
               list_of_bloc[pred]->false_edge = succ;
            while(std::find(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end(), curr_bb) != list_of_bloc[pred]->list_of_succ.end())
               list_of_bloc[pred]->list_of_succ.erase(std::find(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end(), curr_bb));
            if(std::find(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end(), succ) == list_of_bloc[pred]->list_of_succ.end())
               list_of_bloc[pred]->list_of_succ.push_back(succ);
            if(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), pred) == list_of_bloc[succ]->list_of_pred.end())
               list_of_bloc[succ]->list_of_pred.push_back(pred);
         }
         THROW_ASSERT(list_of_bloc[succ]->list_of_pred.begin() != list_of_bloc[succ]->list_of_pred.end(), "unexpected condition");
         while(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), curr_bb) != list_of_bloc[succ]->list_of_pred.end())
            list_of_bloc[succ]->list_of_pred.erase(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), curr_bb));
      }
   }
   std::set<unsigned int>::iterator it_tbr, it_tbr_end = to_be_removed.end();
   for(it_tbr=to_be_removed.begin(); it_tbr != it_tbr_end; ++it_tbr)
   {
      list_of_bloc.erase(*it_tbr);
   }
   to_be_removed.clear();

   single_phi_substitution(list_of_bloc, stmt_ssa_uses, phi_bb_ssa_uses, TM);

#else
   std::set<unsigned int>::iterator it_tbr, it_tbr_end;
#endif

   /// remove forwarder basic blocks
   clean_CFG(list_of_bloc, empty_bb_candidate_list, to_be_removed);

   do
   {
      predicate_phi_scalar_subst(empty_bb_candidate_list, list_of_bloc, to_be_removed, TM);
      empty_bb_candidate_list.clear();
      clean_CFG(list_of_bloc, empty_bb_candidate_list, to_be_removed);
   } while(!empty_bb_candidate_list.empty());

   it_tbr_end = to_be_removed.end();
   for(it_tbr=to_be_removed.begin(); it_tbr != it_tbr_end; ++it_tbr)
   {
      list_of_bloc.erase(*it_tbr);
   }


   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(false);
   }
}

void phi_opt::create_cond_expr_from_phi(unsigned int succ, std::set<unsigned int> &to_be_removed, const tree_managerRef TM, std::map<unsigned int, blocRef>& list_of_bloc, unsigned int pred, std::vector<tree_nodeRef>& list_of_phi_succ, unsigned int curr_bb)
{
   std::vector<tree_nodeRef>::const_iterator lops_it_end = list_of_phi_succ.end();
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;

   std::list<tree_nodeRef>& list_of_pred_stmt = list_of_bloc[pred]->list_of_stmt;
   tree_nodeRef cond_statement = list_of_pred_stmt.back();
   list_of_pred_stmt.erase(std::find(list_of_pred_stmt.begin(), list_of_pred_stmt.end(), cond_statement));
   ///extract the condition from the gimple_cond
   gimple_cond *cond = GetPointer<gimple_cond>(GET_NODE(cond_statement));
   THROW_ASSERT(cond, "expected a gimple_cond");
   unsigned int condition = GET_INDEX_NODE(cond->op0);


   /// create the ssa_var representing the condition
   unsigned int ssa_vers = TM->get_next_vers();
   unsigned int ssa_node_nid = TM->new_tree_node_id();
   IR_schema[TOK(TOK_TYPE)] = STR(tree_helper::get_type_index(TM, condition));
   IR_schema[TOK(TOK_VERS)] = STR(ssa_vers);
   IR_schema[TOK(TOK_VOLATILE)] = STR(false);
   IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
   TM->create_tree_node(ssa_node_nid, ssa_name_K, IR_schema);
   tree_nodeRef ssa_cond_node = TM->GetTreeReindex(ssa_node_nid);
   ///create the assignment between condition and the new ssa var
   unsigned int cond_gimple_stmt_id = TM->new_tree_node_id();
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa_node_nid);
   IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(condition);
   TM->create_tree_node(cond_gimple_stmt_id, gimple_assign_K, IR_schema);
   tree_nodeRef cond_created_stmt = TM->GetTreeReindex(cond_gimple_stmt_id);
   GetPointer<ssa_name>(GET_NODE(ssa_cond_node))->add_def(cond_created_stmt);
   /// and then add to the statement list
   GetPointer<gimple_node>(GET_NODE(cond_created_stmt))->bb_index = pred;
   list_of_pred_stmt.push_back(cond_created_stmt);
   unsigned int T_bb = list_of_bloc[pred]->true_edge;

   std::vector<tree_nodeRef>::const_iterator lops_it_next;
   for(std::vector<tree_nodeRef>::const_iterator lops_it = list_of_phi_succ.begin(); lops_it_end != lops_it; lops_it=lops_it_next)
   {
      lops_it_next = lops_it;
      ++lops_it_next;
      gimple_phi *phi = GetPointer<gimple_phi>(GET_NODE(*lops_it));

      if(phi->virtual_flag)
      {
         /// just remove one of the def_edges
         std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_it_end = phi->list_of_def_edge.end();
         std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_curr_bb=lode_it_end;
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_it = phi->list_of_def_edge.begin(); lode_it_end != lode_it; ++lode_it)
         {
            if(lode_it->second == curr_bb)
               lode_curr_bb = lode_it;
         }
         THROW_ASSERT(lode_curr_bb != lode_it_end, "unexpected condition");
         phi->list_of_def_edge.erase(lode_curr_bb);
      }
      else
      {
         unsigned int type_index = tree_helper::get_type_index(TM, GET_INDEX_NODE(phi->res));
         /// convert gimple cond and phi in a cond_expr assignment
         /// first, create cond_expr tree node
         unsigned int res;
         tree_nodeRef res_node;
         if(phi->list_of_def_edge.size() == 2)
         {
            res = GET_INDEX_NODE(phi->res);
            res_node = phi->res;
         }
         else
         {
            /// create a new ssa
            unsigned int cond_expr_ssa_vers = TM->get_next_vers();
            res = TM->new_tree_node_id();
            IR_schema[TOK(TOK_TYPE)] = STR(type_index);
            IR_schema[TOK(TOK_VERS)] = STR(cond_expr_ssa_vers);
            IR_schema[TOK(TOK_VOLATILE)] = STR(false);
            IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
            TM->create_tree_node(res, ssa_name_K, IR_schema);
            res_node = TM->GetTreeReindex(res);
         }
         std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_it_end = phi->list_of_def_edge.end();
         std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_curr_bb=lode_it_end;
         std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_pred=lode_it_end;
         for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_it = phi->list_of_def_edge.begin(); lode_it_end != lode_it; ++lode_it)
         {
            if(lode_it->second == curr_bb)
               lode_curr_bb = lode_it;
            else if(lode_it->second == pred)
               lode_pred = lode_it;
         }
         THROW_ASSERT(lode_curr_bb != lode_it_end && lode_pred != lode_it_end, "unexpected condition");

         unsigned int op1 = GET_INDEX_NODE(lode_curr_bb->first);
         unsigned int op2 = GET_INDEX_NODE(lode_pred->first);
         if(T_bb != curr_bb )
            std::swap(op1, op2);
         list_of_bloc[pred]->true_edge = 0;
         unsigned int cond_expr_id = TM->new_tree_node_id();
         IR_schema.clear();
         IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
         IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(type_index);
         IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa_node_nid);
         IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(op1);
         IR_schema[TOK(TOK_OP2)] = boost::lexical_cast<std::string>(op2);
         TM->create_tree_node(cond_expr_id, cond_expr_K, IR_schema);
         /// second, create the gimple assignment
         IR_schema.clear();
         unsigned int gimple_stmt_id = TM->new_tree_node_id();
         IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
         IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(res);
         IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(cond_expr_id);
         IR_schema[TOK(TOK_ORIG)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(list_of_phi_succ.front()));
         TM->create_tree_node(gimple_stmt_id, gimple_assign_K, IR_schema);
         tree_nodeRef created_stmt = TM->GetTreeReindex(gimple_stmt_id);
         GetPointer<ssa_name>(GET_NODE(res_node))->def_stmts.clear();
         GetPointer<ssa_name>(GET_NODE(res_node))->add_def(created_stmt);

         /// and then add to the statement list
         GetPointer<gimple_node>(GET_NODE(created_stmt))->bb_index = pred;
         list_of_pred_stmt.push_back(created_stmt);

         if(phi->list_of_def_edge.size() == 2 && lops_it_next == lops_it_end)
         {
            /// add succ statements to pred
            std::list<tree_nodeRef>& list_of_succ_stmt = list_of_bloc[succ]->list_of_stmt;
            std::list<tree_nodeRef>::const_iterator loss_it_end = list_of_succ_stmt.end();
            for(std::list<tree_nodeRef>::const_iterator loss_it = list_of_succ_stmt.begin(); loss_it != loss_it_end; ++loss_it)
            {
               GetPointer<gimple_node>(GET_NODE(*loss_it))->bb_index = pred;
               list_of_pred_stmt.push_back(*loss_it);
            }
            /// update list of succ of pred
            std::vector<unsigned int>::const_iterator los_it_end = list_of_bloc[succ]->list_of_succ.end();
            for(std::vector<unsigned int>::const_iterator los_it = list_of_bloc[succ]->list_of_succ.begin(); los_it != los_it_end; ++los_it)
               update_cfg(*los_it, succ, pred, list_of_bloc);
            list_of_bloc[pred]->list_of_succ = list_of_bloc[succ]->list_of_succ;
            ///update true and false edges
            list_of_bloc[pred]->true_edge = list_of_bloc[succ]->true_edge;
            list_of_bloc[pred]->false_edge = list_of_bloc[succ]->false_edge;
            /// update live out of pred
            list_of_bloc[pred]->live_out = list_of_bloc[succ]->live_out;
            /// update ssa_uses of pred
            list_of_bloc[pred]->ssa_uses.insert(list_of_bloc[succ]->ssa_uses.begin(), list_of_bloc[succ]->ssa_uses.end());
            ///remove succ
            to_be_removed.insert(succ);
         }
         else if(phi->list_of_def_edge.size() != 2)
         {
            /// update the lode_pred with the new ssa
            lode_pred->first = res_node;
            /// then fix the gimple_phi by removing the curr_bb def_edge
            phi->list_of_def_edge.erase(lode_curr_bb);
         }
      }
   }

}

tree_nodeRef phi_opt::create_cond_expr_from_phi_simple(const tree_managerRef TM, std::map<unsigned int, blocRef>& list_of_bloc, unsigned int pred, std::pair< tree_nodeRef, unsigned int>& edge_def1, std::pair< tree_nodeRef, unsigned int>& edge_def2, gimple_phi *phi, unsigned int phi_index, unsigned int &edge_def1_cond, unsigned int &edge_def2_cond)
{
   std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;

   std::list<tree_nodeRef>& list_of_pred_stmt = list_of_bloc[pred]->list_of_stmt;
   tree_nodeRef cond_statement = list_of_pred_stmt.back();
   ///extract the condition from the gimple_cond
   gimple_cond *cond = GetPointer<gimple_cond>(GET_NODE(cond_statement));
   unsigned int ssa_node_nid;
   list_of_pred_stmt.erase(std::find(list_of_pred_stmt.begin(), list_of_pred_stmt.end(), cond_statement));
   bool add_back_cond_statement = false;
   if(cond)
   {
      unsigned int condition = GET_INDEX_NODE(cond->op0);

      /// create the ssa_var representing the condition
      unsigned int ssa_vers = TM->get_next_vers();
      ssa_node_nid = TM->new_tree_node_id();
      IR_schema[TOK(TOK_TYPE)] = STR(tree_helper::get_type_index(TM, condition));
      IR_schema[TOK(TOK_VERS)] = STR(ssa_vers);
      IR_schema[TOK(TOK_VOLATILE)] = STR(false);
      IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
      TM->create_tree_node(ssa_node_nid, ssa_name_K, IR_schema);
      IR_schema.clear();
      tree_nodeRef ssa_cond_node = TM->GetTreeReindex(ssa_node_nid);
      ///create the assignment between condition and the new ssa var
      unsigned int cond_gimple_stmt_id = TM->new_tree_node_id();
      IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
      IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa_node_nid);
      IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(condition);
      IR_schema[TOK(TOK_ORIG)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(cond_statement));
      TM->create_tree_node(cond_gimple_stmt_id, gimple_assign_K, IR_schema);
      IR_schema.clear();
      tree_nodeRef cond_created_stmt = TM->GetTreeReindex(cond_gimple_stmt_id);
      GetPointer<ssa_name>(GET_NODE(ssa_cond_node))->add_def(cond_created_stmt);
      /// and then add to the statement list
      GetPointer<gimple_node>(GET_NODE(cond_created_stmt))->bb_index = pred;
      list_of_pred_stmt.push_back(cond_created_stmt);
   }
   else /// gimple cond already tranformed or gimple_multi_way_if
   {
      gimple_multi_way_if *gmwi = GetPointer<gimple_multi_way_if>(GET_NODE(cond_statement));
      if(gmwi)
      {
         const std::list<tree_nodeRef>::const_reverse_iterator lops_r_it_end = list_of_pred_stmt.rbegin();
         std::list<tree_nodeRef>::const_reverse_iterator lops_r_it = list_of_pred_stmt.rbegin();
         while(lops_r_it != lops_r_it_end && GetPointer<gimple_assign>(GET_NODE(*lops_r_it)) && GetPointer<gimple_assign>(GET_NODE(*lops_r_it))->orig && GetPointer<gimple_phi>(GET_NODE(GetPointer<gimple_assign>(GET_NODE(*lops_r_it))->orig)))
            ++lops_r_it;
         gimple_assign * ga = lops_r_it != lops_r_it_end ? GetPointer<gimple_assign>(GET_NODE(*lops_r_it)) : nullptr;
         if(!ga || !ga->orig || !GetPointer<gimple_multi_way_if>(GET_NODE(ga->orig)))
         {
            /// first time
            unsigned int cond1=0;
            if(edge_def1_cond != DEFAULT_CONDITION && edge_def2_cond != DEFAULT_CONDITION)
            {
               THROW_ASSERT(edge_def1_cond && edge_def2_cond, "unexpected condition");
               IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(tree_helper::get_type_index(TM, edge_def1_cond));
               IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(edge_def1_cond);
               IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(edge_def2_cond);
               IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
               unsigned int expr_index = TM->new_tree_node_id();
               TM->create_tree_node(expr_index, truth_or_expr_K, IR_schema);
               IR_schema.clear();

               /// create the ssa_var representing the or
               unsigned int or_ssa_vers = TM->get_next_vers();
               unsigned int or_ssa_node_nid = TM->new_tree_node_id();
               IR_schema[TOK(TOK_TYPE)] = STR(tree_helper::get_type_index(TM, edge_def1_cond));
               IR_schema[TOK(TOK_VERS)] = STR(or_ssa_vers);
               IR_schema[TOK(TOK_VOLATILE)] = STR(false);
               IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
               TM->create_tree_node(or_ssa_node_nid, ssa_name_K, IR_schema);
               IR_schema.clear();
               tree_nodeRef or_ssa_cond_node = TM->GetTreeReindex(or_ssa_node_nid);
               ///create the assignment between condition and the new ssa var
               unsigned int cond_gimple_stmt_id = TM->new_tree_node_id();
               IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
               IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(or_ssa_node_nid);
               IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(expr_index);
               IR_schema[TOK(TOK_ORIG)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(cond_statement));
               TM->create_tree_node(cond_gimple_stmt_id, gimple_assign_K, IR_schema);
               IR_schema.clear();
               tree_nodeRef cond_created_stmt = TM->GetTreeReindex(cond_gimple_stmt_id);
               GetPointer<ssa_name>(GET_NODE(or_ssa_cond_node))->add_def(cond_created_stmt);
               /// and then add to the statement list
               GetPointer<gimple_node>(GET_NODE(cond_created_stmt))->bb_index = pred;
               list_of_pred_stmt.push_back(cond_created_stmt);
               cond1 = edge_def1_cond;
               edge_def1_cond = or_ssa_node_nid;
               const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it_end = gmwi->list_of_cond.end();
               std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it_erase = gmwi_it_end;
               for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it = gmwi->list_of_cond.begin(); gmwi_it != gmwi_it_end; ++gmwi_it)
               {
                  if((*gmwi_it).first && GET_INDEX_NODE((*gmwi_it).first) == cond1)
                  {
                     (*gmwi_it).first = or_ssa_cond_node;
                  }
                  else if((*gmwi_it).first && GET_INDEX_NODE((*gmwi_it).first) == edge_def2_cond)
                  {
                     gmwi_it_erase = gmwi_it;
                  }
               }
               THROW_ASSERT(gmwi_it_erase != gmwi_it_end, "unexpected condition");
               gmwi->list_of_cond.erase(gmwi_it_erase);
            }
            else
            {
               if(edge_def1_cond == DEFAULT_CONDITION)
               {
                  std::swap(edge_def1, edge_def2);
                  std::swap(edge_def1_cond, edge_def2_cond);
               }
               cond1 = edge_def1_cond;
               edge_def1_cond = DEFAULT_CONDITION;
               THROW_ASSERT(edge_def1_cond, "unexpected condition");

               const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it_end = gmwi->list_of_cond.end();
               for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator gmwi_it = gmwi->list_of_cond.begin(); gmwi_it != gmwi_it_end; ++gmwi_it)
                  if((*gmwi_it).first && GET_INDEX_NODE((*gmwi_it).first) == cond1)
                  {
                     gmwi->list_of_cond.erase(gmwi_it);
                     break;
                  }
            }
            THROW_ASSERT(cond1 != DEFAULT_CONDITION && cond1 != 0, "unexpected condition");
            ///compute ssa_node_id
            /// create the ssa_var representing the condition
            unsigned int ssa_vers = TM->get_next_vers();
            ssa_node_nid = TM->new_tree_node_id();
            IR_schema[TOK(TOK_TYPE)] = STR(tree_helper::get_type_index(TM, cond1));
            IR_schema[TOK(TOK_VERS)] = STR(ssa_vers);
            IR_schema[TOK(TOK_VOLATILE)] = STR(false);
            IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
            TM->create_tree_node(ssa_node_nid, ssa_name_K, IR_schema);
            IR_schema.clear();
            tree_nodeRef ssa_cond_node = TM->GetTreeReindex(ssa_node_nid);
            ///create the assignment between condition and the new ssa var
            unsigned int cond_gimple_stmt_id = TM->new_tree_node_id();
            IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
            IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa_node_nid);
            IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(cond1);
            IR_schema[TOK(TOK_ORIG)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(cond_statement));
            TM->create_tree_node(cond_gimple_stmt_id, gimple_assign_K, IR_schema);
            IR_schema.clear();
            tree_nodeRef cond_created_stmt = TM->GetTreeReindex(cond_gimple_stmt_id);
            GetPointer<ssa_name>(GET_NODE(ssa_cond_node))->add_def(cond_created_stmt);
            /// and then add to the statement list
            GetPointer<gimple_node>(GET_NODE(cond_created_stmt))->bb_index = pred;
            list_of_pred_stmt.push_back(cond_created_stmt);


         }
         else
         {
            if(GET_INDEX_NODE(ga->op1) != edge_def1_cond)
            {
               THROW_ASSERT(GET_INDEX_NODE(ga->op1) == edge_def2_cond, "unexpected condition");
               std::swap(edge_def1, edge_def2);
               std::swap(edge_def1_cond, edge_def2_cond);
            }
            ssa_node_nid = GET_INDEX_NODE(ga->op0);
         }
         if(gmwi->list_of_cond.size()>1)
            add_back_cond_statement = true;
      }
      else
      {
         list_of_pred_stmt.push_back(cond_statement);
         std::list<tree_nodeRef>::const_reverse_iterator lops_r_it = list_of_pred_stmt.rbegin();
         while(GetPointer<gimple_assign>(GET_NODE(*lops_r_it)) && GetPointer<gimple_assign>(GET_NODE(*lops_r_it))->orig && GetPointer<gimple_phi>(GET_NODE(GetPointer<gimple_assign>(GET_NODE(*lops_r_it))->orig)))
            ++lops_r_it;
         gimple_assign * ga = GetPointer<gimple_assign>(GET_NODE(*lops_r_it));
         THROW_ASSERT(ga, "unexpected condition");
         THROW_ASSERT(ga->orig && GetPointer<gimple_cond>(GET_NODE(ga->orig)), "expected a gimple_cond: " + STR(GET_INDEX_NODE(cond_statement)));
         ssa_node_nid = GET_INDEX_NODE(ga->op0);
      }
   }

   unsigned int type_index = tree_helper::get_type_index(TM, GET_INDEX_NODE(phi->res));
   /// convert gimple cond and phi in a cond_expr assignment
   /// first, create cond_expr tree node
   unsigned int res;
   tree_nodeRef res_node;
   /// create a new ssa
   unsigned int cond_expr_ssa_vers = TM->get_next_vers();
   res = TM->new_tree_node_id();
   IR_schema[TOK(TOK_TYPE)] = STR(type_index);
   IR_schema[TOK(TOK_VERS)] = STR(cond_expr_ssa_vers);
   IR_schema[TOK(TOK_VOLATILE)] = STR(false);
   IR_schema[TOK(TOK_VIRTUAL)] = STR(false);
   TM->create_tree_node(res, ssa_name_K, IR_schema);
   IR_schema.clear();
   res_node = TM->GetTreeReindex(res);

   if(edge_def2_cond == TRUE_CONDITION || edge_def1_cond == FALSE_CONDITION)
   {
      std::swap(edge_def1, edge_def2);
      std::swap(edge_def1_cond, edge_def2_cond);
   }

   unsigned int op1 = GET_INDEX_NODE(edge_def1.first);
   unsigned int op2 = GET_INDEX_NODE(edge_def2.first);
   list_of_bloc[pred]->true_edge = 0;
   list_of_bloc[pred]->false_edge = 0;
   unsigned int cond_expr_id = TM->new_tree_node_id();
   IR_schema.clear();
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(type_index);
   IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(ssa_node_nid);
   IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(op1);
   IR_schema[TOK(TOK_OP2)] = boost::lexical_cast<std::string>(op2);
   TM->create_tree_node(cond_expr_id, cond_expr_K, IR_schema);
   IR_schema.clear();
   /// second, create the gimple assignment
   unsigned int gimple_stmt_id = TM->new_tree_node_id();
   IR_schema[TOK(TOK_SRCP)] = "<built-in>:0:0";
   IR_schema[TOK(TOK_OP0)] = boost::lexical_cast<std::string>(res);
   IR_schema[TOK(TOK_OP1)] = boost::lexical_cast<std::string>(cond_expr_id);
   IR_schema[TOK(TOK_ORIG)] = boost::lexical_cast<std::string>(phi_index);
   TM->create_tree_node(gimple_stmt_id, gimple_assign_K, IR_schema);
   IR_schema.clear();
   tree_nodeRef created_stmt = TM->GetTreeReindex(gimple_stmt_id);
   GetPointer<ssa_name>(GET_NODE(res_node))->def_stmts.clear();
   GetPointer<ssa_name>(GET_NODE(res_node))->add_def(created_stmt);

   /// and then add to the statement list
   GetPointer<gimple_node>(GET_NODE(created_stmt))->bb_index = pred;
   list_of_pred_stmt.push_back(created_stmt);

   if(add_back_cond_statement)
      list_of_pred_stmt.push_back(cond_statement);
   return res_node;

}

void phi_opt::predicate_phi_scalar_subst(std::list<unsigned int>&empty_bb_candidate_list, std::map<unsigned int, blocRef> & list_of_bloc, std::set<unsigned int> &to_be_removed, const tree_managerRef TM)
{
   std::list<unsigned int>::iterator it_ebcl, it_ebcl_end = empty_bb_candidate_list.end();
   for(it_ebcl=empty_bb_candidate_list.begin(); it_ebcl != it_ebcl_end; ++it_ebcl)
   {
      unsigned int curr_bb = *it_ebcl;
      unsigned int pred = list_of_bloc[curr_bb]->list_of_pred.front();
      unsigned int succ = list_of_bloc[curr_bb]->list_of_succ.front();
      std::vector<tree_nodeRef> & list_of_phi_succ = list_of_bloc[succ]->list_of_phi;

      /// check if a virtual phi is present and the number of def_edge is two
      bool has_virtuals2 = false;
      std::vector<tree_nodeRef>::const_iterator lops_it_end = list_of_phi_succ.end();
      for(std::vector<tree_nodeRef>::const_iterator lops_it = list_of_phi_succ.begin(); lops_it_end != lops_it; ++lops_it)
      {
         gimple_phi *phi = GetPointer<gimple_phi>(GET_NODE(*lops_it));
         if(phi->virtual_flag && phi->list_of_def_edge.size() == 2)
         {
            has_virtuals2 = true;
            break;
         }
      }
      if(has_virtuals2) continue;


      to_be_removed.insert(curr_bb);
      list_of_bloc[pred]->false_edge = 0;
      while(std::find(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end(), curr_bb) != list_of_bloc[pred]->list_of_succ.end())
         list_of_bloc[pred]->list_of_succ.erase(std::find(list_of_bloc[pred]->list_of_succ.begin(), list_of_bloc[pred]->list_of_succ.end(), curr_bb));
      while(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), curr_bb) != list_of_bloc[succ]->list_of_pred.end())
         list_of_bloc[succ]->list_of_pred.erase(std::find(list_of_bloc[succ]->list_of_pred.begin(), list_of_bloc[succ]->list_of_pred.end(), curr_bb));

      ///save and then remove the gimple_cond ending pred
      create_cond_expr_from_phi(succ, to_be_removed, TM, list_of_bloc, pred, list_of_phi_succ, curr_bb);

   }
}

