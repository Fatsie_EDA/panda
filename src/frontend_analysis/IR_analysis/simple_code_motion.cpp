/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file simple_code_motion.cpp
 * @brief * @brief Analysis step that performs some simple code motions over GCC IR
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */

///Skipping warnings due to operator() redefinition
#pragma GCC diagnostic ignored "-Woverloaded-virtual"

///header include
#include "simple_code_motion.hpp"

#include "application_manager.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <fstream>

///Tree include
#include "tree_node_dup.hpp"
#include "tree_basic_block.hpp"
#include "tree_manager.hpp"
#include "ext_tree_node.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"
#include "tree_helper.hpp"

///Utility include
#include "dbgPrintHelper.hpp"

#include "Dominance.hpp"

#include "cyclic_topological_sort.hpp"

REF_FORWARD_DECL(bare_bbgraph);
struct bare_bbgraph : public boost::adjacency_list< boost::vecS, boost::vecS, boost::bidirectionalS >
{
};


simple_code_motion::simple_code_motion(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, SIMPLE_CODE_MOTION, _design_flow_manager, _parameters),
   restart_short_circuit_taf(false)
{
   debug_level = _parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);

}

simple_code_motion::~simple_code_motion()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FunctionFrontendFlowStep::FunctionRelationship> > simple_code_motion::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BLOCK_FIX, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SWITCH_FIX, SAME_FUNCTION));
         break;
      }
      case(PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      {
         if(restart_short_circuit_taf)
            relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SHORT_CIRCUIT_TAF, SAME_FUNCTION));
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}


bool simple_code_motion::check_if_can_be_moved(gimple_assign * ga, bool &zero_delay, const tree_managerRef TM)
{
   tree_nodeRef left = GET_NODE(ga->op0);
   if(!GetPointer<ssa_name>(left)) return false;
   if(tree_helper::is_constant(TM, GET_INDEX_NODE(ga->op1))) return true;
   std::set<ssa_name*> rhs_ssa_uses;
   tree_helper::compute_ssa_uses_rec_ptr(ga->op1, rhs_ssa_uses);
   if(rhs_ssa_uses.empty()) return true;

   tree_nodeRef right = GET_NODE(ga->op1);


   switch (right->get_kind())
   {
      case assert_expr_K:
      case convert_expr_K:
      case view_convert_expr_K:
      case ssa_name_K:
      {
         return true;
      }
      /// binary expressions
      case eq_expr_K:
      case ge_expr_K:
      case gt_expr_K:
      case le_expr_K:
      case lrotate_expr_K:
      case lshift_expr_K:
      case lt_expr_K:
      case max_expr_K:
      case min_expr_K:
      case ne_expr_K:
      case rrotate_expr_K:
      case rshift_expr_K:
      {
         binary_expr* be = GetPointer<binary_expr>(right);
         if(!tree_helper::is_constant(TM, GET_INDEX_NODE(be->op1)))
            zero_delay = false;
         return true;
      }
      case mult_expr_K:
      case widen_mult_expr_K:
      {
         binary_expr* be = GetPointer<binary_expr>(right);
         if(tree_helper::is_constant(TM, GET_INDEX_NODE(be->op1)))
         {
            integer_cst *ic = GetPointer<integer_cst>(GET_NODE(be->op1));
            if(ic)
            {
               long long v = tree_helper::get_integer_cst_value(ic);
               if(!(v && !(v & (v - 1))))
                  zero_delay = false;
               return true;
            }
            else
               zero_delay = false;
         }
         else
            zero_delay = false;
         return false;
      }
      case nop_expr_K:
      {
         nop_expr * ne = GetPointer<nop_expr>(GET_NODE(ga->op1));
         unsigned int left_type_index;
         tree_helper::get_type_node(GET_NODE(ga->op0), left_type_index);
         unsigned int right_type_index;
         tree_helper::get_type_node(GET_NODE(ne->op), right_type_index);
         bool is_realR = tree_helper::is_real(TM, right_type_index);
         bool is_realL = tree_helper::is_real(TM, left_type_index);
         if(is_realR || is_realL) zero_delay = false;

         return true;
      }
      case addr_expr_K:
      {
         zero_delay = false;
         return true;
      }
      case bit_and_expr_K:
      case bit_ior_expr_K:
      case bit_xor_expr_K:
      case truth_and_expr_K:
      case truth_andif_expr_K:
      case truth_or_expr_K:
      case truth_orif_expr_K:
      case truth_xor_expr_K:
      case bit_not_expr_K:
      case truth_not_expr_K:
      case cond_expr_K:
      {
         return true;
      }
      case minus_expr_K:
      case plus_expr_K:
      case pointer_plus_expr_K:
      case postdecrement_expr_K:
      case postincrement_expr_K:
      case predecrement_expr_K:
      case preincrement_expr_K:
      {
         binary_expr* be = GetPointer<binary_expr>(right);
         unsigned int left_type_index;
         tree_nodeRef left_type_node = tree_helper::get_type_node(GET_NODE(be->op0), left_type_index);
         unsigned int n_bit = tree_helper::Size(left_type_node);
         if(n_bit > 8)
            zero_delay = false;
         return true;
      }
      case negate_expr_K:
      case float_expr_K:
      {
         zero_delay = false;
         return true;
      }
      case exact_div_expr_K:
      case trunc_div_expr_K:
      case trunc_mod_expr_K:
      {
         binary_expr* be = GetPointer<binary_expr>(right);
         if(tree_helper::is_constant(TM, GET_INDEX_NODE(be->op1)))
         {
            integer_cst *ic = GetPointer<integer_cst>(GET_NODE(be->op1));
            if(ic)
            {
               long long v = tree_helper::get_integer_cst_value(ic);
               if(v)
               {
                  if(!(v && !(v & (v - 1))))
                     zero_delay = false;
                  return true;
               }
            }
         }
         return false;
      }
      case catch_expr_K:
      case ceil_div_expr_K:
      case ceil_mod_expr_K:
      case complex_expr_K:
      case compound_expr_K:
      case eh_filter_expr_K:
      case fdesc_expr_K:
      case floor_div_expr_K:
      case floor_mod_expr_K:
      case goto_subroutine_K:
      case in_expr_K:
      case init_expr_K:
      case mem_ref_K:
      case modify_expr_K:
      case ordered_expr_K:
      case range_expr_K:
      case round_div_expr_K:
      case round_mod_expr_K:
      case set_le_expr_K:
      case try_catch_expr_K:
      case try_finally_K:
      case uneq_expr_K:
      case ltgt_expr_K:
      case unge_expr_K:
      case ungt_expr_K:
      case unle_expr_K:
      case unlt_expr_K:
      case unordered_expr_K:
      case widen_sum_expr_K:
      case with_size_expr_K:
      case vec_lshift_expr_K:
      case vec_rshift_expr_K:
      case widen_mult_hi_expr_K:
      case widen_mult_lo_expr_K:
      case vec_pack_trunc_expr_K:
      case vec_pack_sat_expr_K:
      case vec_pack_fix_trunc_expr_K:
      case vec_extracteven_expr_K:
      case vec_extractodd_expr_K:\
      case vec_interleavehigh_expr_K:
      case vec_interleavelow_expr_K:
      case CASE_CPP_NODES:
      case CASE_CST_NODES:
      case CASE_DECL_NODES:
      case CASE_GIMPLE_NODES:
      case component_ref_K:
      case bit_field_ref_K:
      case vtable_ref_K:
      case with_cleanup_expr_K:
      case obj_type_ref_K:
      case save_expr_K:
      case vec_cond_expr_K:
      case vec_perm_expr_K:
      case dot_prod_expr_K:
      case call_expr_K:
      case CASE_FAKE_NODES:
      case rdiv_expr_K:
      case case_label_expr_K:
      case target_mem_ref_K:
      case target_mem_ref461_K:
      case binfo_K:
      case block_K:
      case constructor_K:
      case identifier_node_K:
      case CASE_PRAGMA_NODES:
      case statement_list_K:
      case tree_list_K:
      case tree_vec_K:
      case CASE_QUATERNARY_EXPRESSION:
      case CASE_TYPE_NODES:
      case abs_expr_K:
      case arrow_expr_K:
      case buffer_ref_K:
      case card_expr_K:
      case cast_expr_K:
      case cleanup_point_expr_K:
      case conj_expr_K:
      case exit_expr_K:
      case fix_ceil_expr_K:
      case fix_floor_expr_K:
      case fix_round_expr_K:
      case fix_trunc_expr_K:
      case imagpart_expr_K:
      case indirect_ref_K:
      case misaligned_indirect_ref_K:
      case loop_expr_K:
      case non_lvalue_expr_K:
      case realpart_expr_K:
      case reference_expr_K:
      case reinterpret_cast_expr_K:
      case sizeof_expr_K:
      case static_cast_expr_K:
      case throw_expr_K:
      case unsave_expr_K:
      case va_arg_expr_K:
      case reduc_max_expr_K:
      case reduc_min_expr_K:
      case reduc_plus_expr_K:
      case vec_unpack_hi_expr_K:
      case vec_unpack_lo_expr_K:
      case vec_unpack_float_hi_expr_K:
      case vec_unpack_float_lo_expr_K:
      {
         return false;
      }
      default:
      {
         THROW_UNREACHABLE("");
         return false;
      }
   }
}

void simple_code_motion::add_stmt_back(std::list<tree_nodeRef>& list_of_pred_stmt, tree_nodeRef curr_stmt)
{
   tree_nodeRef cond_statement = list_of_pred_stmt.begin() != list_of_pred_stmt.end() ? list_of_pred_stmt.back() : tree_nodeRef();
   tree_nodeRef cond_statement_node = cond_statement ? GET_NODE(cond_statement) : cond_statement;
   bool last_statement_cannot_be_moved = false;

   if(cond_statement_node && ( GetPointer<gimple_cond>(cond_statement_node) ||
         GetPointer<gimple_switch>(cond_statement_node) ||
         GetPointer<gimple_multi_way_if>(cond_statement_node) ||
         GetPointer<gimple_goto>(cond_statement_node)))
      last_statement_cannot_be_moved = true;
   if(last_statement_cannot_be_moved)
      list_of_pred_stmt.erase(std::find(list_of_pred_stmt.begin(), list_of_pred_stmt.end(), cond_statement));
   list_of_pred_stmt.push_back(curr_stmt);
   if(last_statement_cannot_be_moved)
      list_of_pred_stmt.push_back(cond_statement);
}

void simple_code_motion::Exec()
{
   const tree_managerRef TM = AppM->get_tree_manager();
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }
   restart_short_circuit_taf = false;

   tree_nodeRef temp = TM->get_tree_node_const(function_id);
   function_decl * fd = GetPointer<function_decl>(temp);
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));

   std::map<unsigned int, blocRef> & list_of_bloc = sl->list_of_bloc;
   std::map<unsigned int, blocRef>::iterator it3, it3_end = list_of_bloc.end();


   /// store the GCC BB graph ala boost::graph
   bare_bbgraphRef GCC_bb_graph = bare_bbgraphRef(new bare_bbgraph());
   std::unordered_map<bare_bbgraph::vertex_descriptor, unsigned int> direct_vertex_map;
   std::unordered_map<unsigned int, bare_bbgraph::vertex_descriptor> inverse_vertex_map;
   /// add vertices
   for(it3 = list_of_bloc.begin(); it3 != it3_end; ++it3)
   {
      inverse_vertex_map[it3->first] = boost::add_vertex(*GCC_bb_graph);
      direct_vertex_map[inverse_vertex_map[it3->first]]=it3->first;
   }
   /// add edges
   for(it3 = list_of_bloc.begin(); it3 != it3_end; ++it3)
   {
      unsigned int curr_bb = it3->first;
      std::vector<unsigned int>::const_iterator lop_it_end = list_of_bloc[curr_bb]->list_of_pred.end();
      for(std::vector<unsigned int>::const_iterator lop_it = list_of_bloc[curr_bb]->list_of_pred.begin(); lop_it != lop_it_end; ++lop_it)
      {
         boost::add_edge(inverse_vertex_map[*lop_it], inverse_vertex_map[curr_bb], *GCC_bb_graph);
      }
      std::vector<unsigned int>::const_iterator los_it_end = list_of_bloc[curr_bb]->list_of_succ.end();
      for(std::vector<unsigned int>::const_iterator los_it = list_of_bloc[curr_bb]->list_of_succ.begin(); los_it != los_it_end; ++los_it)
      {
         if(*los_it == bloc::EXIT_BLOCK_ID)
            boost::add_edge(inverse_vertex_map[curr_bb], inverse_vertex_map[*los_it], *GCC_bb_graph);
      }
      if(list_of_bloc[curr_bb]->list_of_succ.empty())
         boost::add_edge(inverse_vertex_map[curr_bb], inverse_vertex_map[bloc::EXIT_BLOCK_ID], *GCC_bb_graph);

   }
   /// add a connection between entry and exit thus avoiding problems with non terminating code
   boost::add_edge(inverse_vertex_map[bloc::ENTRY_BLOCK_ID], inverse_vertex_map[bloc::EXIT_BLOCK_ID], *GCC_bb_graph);
   /// sort basic block vertices from the entry till the exit
   std::list<bare_bbgraph::vertex_descriptor> bb_sorted_vertices;
   cyclic_topological_sort(*GCC_bb_graph, std::front_inserter(bb_sorted_vertices));


   refcount<dominance<bare_bbgraph> > bb_dominators;
   bb_dominators = refcount<dominance<bare_bbgraph> >(new dominance<bare_bbgraph>(*GCC_bb_graph, inverse_vertex_map[bloc::ENTRY_BLOCK_ID], inverse_vertex_map[bloc::EXIT_BLOCK_ID], parameters));
   bb_dominators->calculate_dominance_info(dominance<bare_bbgraph>::CDI_DOMINATORS);
   std::unordered_map<bare_bbgraph::vertex_descriptor, bare_bbgraph::vertex_descriptor> bb_dominator_map = bb_dominators->get_dominator_map();


   const std::list<bare_bbgraph::vertex_descriptor>::iterator bsv_it_end = bb_sorted_vertices.end();
   for(std::list<bare_bbgraph::vertex_descriptor>::iterator bsv_it = bb_sorted_vertices.begin(); bsv_it != bsv_it_end; ++bsv_it)
   {
      unsigned int curr_bb = direct_vertex_map[*bsv_it];
      if(curr_bb == bloc::ENTRY_BLOCK_ID) continue;
      if(curr_bb == bloc::EXIT_BLOCK_ID) continue;
      /// skip BB having a pred block with a gimple_multi_way_if statement as last statement
      bool have_multi_way_if_pred = false;
      std::vector<unsigned int>::const_iterator lop_it_end = list_of_bloc[curr_bb]->list_of_pred.end();
      for(std::vector<unsigned int>::const_iterator lop_it = list_of_bloc[curr_bb]->list_of_pred.begin(); lop_it != lop_it_end && !have_multi_way_if_pred; ++lop_it)
         if(!list_of_bloc[*lop_it]->list_of_stmt.empty())
         {
            tree_nodeRef last = GET_NODE(list_of_bloc[*lop_it]->list_of_stmt.back());
            if(GetPointer<gimple_multi_way_if>(last))
               have_multi_way_if_pred = true;
         }
      if(have_multi_way_if_pred) continue;
      bool restart_bb_code_motion = false;
      do
      {
         std::list<tree_nodeRef> & list_of_stmt = list_of_bloc[curr_bb]->list_of_stmt;
         const std::list<tree_nodeRef>::iterator los_it_end = list_of_stmt.end();
         std::list<tree_nodeRef> to_be_removed;
         std::set<unsigned int> zero_delay_stmts;
         std::list<tree_nodeRef> to_be_added_back;
         std::list<tree_nodeRef> to_be_added_front;
         for(std::list<tree_nodeRef>::iterator los_it = list_of_stmt.begin(); los_it != los_it_end; ++los_it)
         {
            /// skip gimple statements defining or using virtual operands
            tree_nodeRef tn = GET_NODE(*los_it);
            gimple_node * gn = GetPointer<gimple_node>(tn);

            THROW_ASSERT(gn, "unexpected condition");
            if(gn->vuse || gn->vdef) continue;

            ///only gimple_assign are considered for code motion
            if(!GetPointer<gimple_assign>(tn)) continue;

            ///compute the SSA variables used by stmt
            std::set<ssa_name*> stmt_ssa_uses;
            tree_helper::compute_ssa_uses_rec_ptr(*los_it, stmt_ssa_uses);

            /// compute BB where the SSA variables are defined
            std::set<unsigned int> BB_def;
            const std::set<ssa_name*>::const_iterator ssu_it_end = stmt_ssa_uses.end();
            for(std::set<ssa_name*>::const_iterator ssu_it = stmt_ssa_uses.begin(); ssu_it != ssu_it_end; ++ssu_it)
            {
               ssa_name* sn = *ssu_it;
               const std::vector<tree_nodeRef>::const_iterator ds_it_end = sn->def_stmts.end();
               for(std::vector<tree_nodeRef>::const_iterator ds_it = sn->def_stmts.begin(); ds_it_end != ds_it; ++ds_it)
               {
                  tree_nodeRef def_stmt = GET_NODE(*ds_it);
                  gimple_node * def_gn = GetPointer<gimple_node>(def_stmt);
                  BB_def.insert(def_gn->bb_index);
               }
            }
            //std::cerr << "CURR STMT " << GET_INDEX_NODE(*los_it) << std::endl;

            /// skip the statement in case it uses ssa variables defined in the current BB
            if(BB_def.find(curr_bb) != BB_def.end())
            {
               continue;
            }


            bool zero_delay = true;
            if(!check_if_can_be_moved(GetPointer<gimple_assign>(tn), zero_delay, TM)) continue;
            /// find in which BB can be moved
            bare_bbgraph::vertex_descriptor dom_bb = *bsv_it;
            unsigned int dest_bb_index = curr_bb;
            unsigned int prev_dest_bb_index = curr_bb;
            if(bb_dominator_map.find(dom_bb) != bb_dominator_map.end())
            {
               dom_bb = bb_dominator_map.find(dom_bb)->second;
               unsigned int dom_bb_index = direct_vertex_map[dom_bb];
               while(dom_bb_index != bloc::ENTRY_BLOCK_ID)
               {
                  if(list_of_bloc[curr_bb]->loop_id >= list_of_bloc[dom_bb_index]->loop_id)
                  {
                     prev_dest_bb_index = dest_bb_index;
                     dest_bb_index = dom_bb_index;
                  }
                  if(BB_def.find(dom_bb_index) != BB_def.end()) break;
                  if(bb_dominator_map.find(dom_bb) == bb_dominator_map.end()) break;
                  dom_bb = bb_dominator_map.find(dom_bb)->second;
                  dom_bb_index = direct_vertex_map[dom_bb];
               }
            }
            //std::cerr << "dest bb "  << dest_bb_index << std::endl;
            /// check the result of the dominator tree analysis
            if(dest_bb_index == curr_bb) continue;

            /// finally we found something of meaningful

            /// check if the current uses in dest_bb_index are due only to phis
            bool only_phis = true;
            for(std::set<ssa_name*>::const_iterator ssu_it = stmt_ssa_uses.begin(); ssu_it != ssu_it_end && only_phis; ++ssu_it)
            {
               ssa_name* sn = *ssu_it;
               const std::vector<tree_nodeRef>::const_iterator ds_it_end = sn->def_stmts.end();
               for(std::vector<tree_nodeRef>::const_iterator ds_it = sn->def_stmts.begin(); ds_it_end != ds_it && only_phis; ++ds_it)
               {
                  tree_nodeRef def_stmt = GET_NODE(*ds_it);
                  gimple_node * def_gn = GetPointer<gimple_node>(def_stmt);
                  if(def_gn->bb_index == dest_bb_index && !GetPointer<gimple_phi>(def_stmt) && zero_delay_stmts.find(GET_INDEX_NODE(*ds_it)) == zero_delay_stmts.end())
                     only_phis = false;
               }
            }
            if(only_phis && zero_delay)
               zero_delay_stmts.insert(GET_INDEX_NODE(*los_it));
            if(!only_phis && !zero_delay)
               dest_bb_index = prev_dest_bb_index;

            /// check if the statement can be really moved
            if(dest_bb_index == curr_bb) continue;
            /// update bb_index associated with the statement
            GetPointer<gimple_node>(tn)->bb_index = dest_bb_index;
            restart_short_circuit_taf = true;

            std::list<tree_nodeRef>& list_of_pred_stmt = list_of_bloc[dest_bb_index]->list_of_stmt;
            add_stmt_back(list_of_pred_stmt, *los_it);
            to_be_removed.push_back(*los_it);
         }

         const std::list<tree_nodeRef>::iterator it_tbr_end = to_be_removed.end();
         for(std::list<tree_nodeRef>::iterator it_tbr=to_be_removed.begin(); it_tbr != it_tbr_end; ++it_tbr)
         {
            //std::cerr << "removed " << GET_INDEX_NODE(*it_tbr) << std::endl;
            list_of_stmt.erase(std::find(list_of_stmt.begin(), list_of_stmt.end(), *it_tbr));
         }
         const std::list<tree_nodeRef>::iterator it_tbab_end = to_be_added_back.end();
         for(std::list<tree_nodeRef>::iterator it_tbab=to_be_added_back.begin(); it_tbab != it_tbab_end; ++it_tbab)
         {
            //std::cerr << "added back" << GET_INDEX_NODE(*it_tbab) << std::endl;
            add_stmt_back(list_of_stmt, *it_tbab);
         }
         const std::list<tree_nodeRef>::iterator it_tbaf_end = to_be_added_front.end();
         for(std::list<tree_nodeRef>::iterator it_tbaf=to_be_added_front.begin(); it_tbaf != it_tbaf_end; ++it_tbaf)
         {
            //std::cerr << "added in front" << GET_INDEX_NODE(*it_tbaf) << std::endl;
            list_of_stmt.push_front(*it_tbaf);
         }
         restart_bb_code_motion = to_be_added_back.begin() != it_tbab_end || to_be_added_front.begin() != it_tbaf_end;
         //std::cerr << "iteration completed"<< std::endl;
      } while(restart_bb_code_motion);
   }



   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(false);
   }
}
