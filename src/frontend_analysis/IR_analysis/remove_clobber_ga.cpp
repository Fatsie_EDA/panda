/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file remove_clobber_ga.cpp
 * @brief Analysis step that removes clobber gimple_assign introduced by GCC v4.7 and greater.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */

///header include
#include "remove_clobber_ga.hpp"

#include "application_manager.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <fstream>

///Tree include
#include "tree_basic_block.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"
#include "tree_helper.hpp"

///Utility include
#include "dbgPrintHelper.hpp"

remove_clobber_ga::remove_clobber_ga(const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager, const ParameterConstRef _parameters) :
   FunctionFrontendFlowStep(_AppM, _function_id, REMOVE_CLOBBER_GA, _design_flow_manager, _parameters)
{
   debug_level = _parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

remove_clobber_ga::~remove_clobber_ga()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FunctionFrontendFlowStep::FunctionRelationship> > remove_clobber_ga::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BLOCK_FIX, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SWITCH_FIX, SAME_FUNCTION));
         break;
      }
      case(PRECEDENCE_RELATIONSHIP) :
      case(POST_DEPENDENCE_RELATIONSHIP) :
      {
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(SIMPLE_CODE_MOTION, SAME_FUNCTION));
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void remove_clobber_ga::Exec()
{
   const tree_managerRef TM = AppM->get_tree_manager();
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }
   tree_nodeRef temp = TM->get_tree_node_const(function_id);
   function_decl * fd = GetPointer<function_decl>(temp);
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));



   std::map<unsigned int, blocRef> & list_of_bloc = sl->list_of_bloc;
   const std::map<unsigned int, blocRef>::iterator it3_end = list_of_bloc.end();
   ///compute variable substitution
   std::map<unsigned int,tree_nodeRef > var_substitution_table;
   std::map<unsigned int, std::set<tree_nodeRef> > stmt_to_be_removed;
   for(std::map<unsigned int, blocRef>::iterator it3 = list_of_bloc.begin(); it3 != it3_end; ++it3)
   {
      unsigned int curr_bb = it3->first;
      if(curr_bb == bloc::ENTRY_BLOCK_ID) continue;
      if(curr_bb == bloc::EXIT_BLOCK_ID) continue;
      std::list<tree_nodeRef> & list_of_stmt = list_of_bloc[curr_bb]->list_of_stmt;
      const std::list<tree_nodeRef>::iterator los_it_end = list_of_stmt.end();
      for(std::list<tree_nodeRef>::iterator los_it = list_of_stmt.begin(); los_it != los_it_end; ++los_it)
      {
         /// skip all non-clobber gimple_assign
         tree_nodeRef tn = GET_NODE(*los_it);
         gimple_assign * ga = GetPointer<gimple_assign>(tn);
         if(!ga || !ga->clobber) continue;
         var_substitution_table[GET_INDEX_NODE(ga->vdef)] = ga->vuse;
         stmt_to_be_removed[curr_bb].insert(*los_it);
      }
   }

   /// perform the substitution
   for(std::map<unsigned int, blocRef>::iterator it3 = list_of_bloc.begin(); it3 != it3_end; ++it3)
   {
      unsigned int curr_bb = it3->first;
      if(curr_bb == bloc::ENTRY_BLOCK_ID) continue;
      if(curr_bb == bloc::EXIT_BLOCK_ID) continue;
      std::vector<tree_nodeRef> & list_of_phi = list_of_bloc[curr_bb]->list_of_phi;
      std::vector<tree_nodeRef>::const_iterator lop_it_end = list_of_phi.end();
      for(std::vector<tree_nodeRef>::const_iterator lop_it = list_of_phi.begin(); lop_it_end != lop_it; ++lop_it)
      {
         gimple_phi *phi = GetPointer<gimple_phi>(GET_NODE(*lop_it));
         if(phi->virtual_flag)
         {
            const std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_it_end = phi->list_of_def_edge.end();
            for(std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator lode_it = phi->list_of_def_edge.begin(); lode_it_end != lode_it; ++lode_it)
            {
               if(var_substitution_table.find(GET_INDEX_NODE(lode_it->first)) != var_substitution_table.end())
               {
                  tree_nodeRef res = var_substitution_table.find(GET_INDEX_NODE(lode_it->first))->second;
                  while(var_substitution_table.find(GET_INDEX_NODE(res)) != var_substitution_table.end())
                     res = var_substitution_table.find(GET_INDEX_NODE(res))->second;
                  THROW_ASSERT(!(GetPointer<ssa_name>(GET_NODE(res)) && GetPointer<gimple_assign>(GET_NODE(GetPointer<ssa_name>(GET_NODE(res))->def_stmts.front())) && GetPointer<gimple_assign>(GET_NODE(GetPointer<ssa_name>(GET_NODE(res))->def_stmts.front()))->clobber), "unexpected condition");
                  lode_it->first = TM->GetTreeReindex(GET_INDEX_NODE(res));
               }
            }
         }
      }
      std::list<tree_nodeRef> & list_of_stmt = list_of_bloc[curr_bb]->list_of_stmt;
      const std::list<tree_nodeRef>::iterator los_it_end = list_of_stmt.end();
      for(std::list<tree_nodeRef>::iterator los_it = list_of_stmt.begin(); los_it != los_it_end; ++los_it)
      {
         /// consider only gimple statements using virtual operands
         tree_nodeRef tn = GET_NODE(*los_it);
         gimple_node * gn = GetPointer<gimple_node>(tn);
         THROW_ASSERT(gn, "unexpected condition");
         if(!gn->vuse) continue;
         if(var_substitution_table.find(GET_INDEX_NODE(gn->vuse)) != var_substitution_table.end())
            gn->vuse = TM->GetTreeReindex(GET_INDEX_NODE(var_substitution_table.find(GET_INDEX_NODE(gn->vuse))->second));
      }
   }


   /// now remove the clobber gimple_assign
   const std::map<unsigned int, std::set<tree_nodeRef> >::iterator stbr_it_end = stmt_to_be_removed.end();
   for(std::map<unsigned int, std::set<tree_nodeRef> >::iterator stbr_it = stmt_to_be_removed.begin(); stbr_it != stbr_it_end; ++stbr_it)
   {
      unsigned int curr_bb = stbr_it->first;
      std::list<tree_nodeRef> & list_of_stmt = list_of_bloc[curr_bb]->list_of_stmt;
      const std::set<tree_nodeRef>::const_iterator s_it_end = stbr_it->second.end();
      for(std::set<tree_nodeRef>::const_iterator s_it = stbr_it->second.begin(); s_it != s_it_end; ++s_it)
      {
         list_of_stmt.erase(std::find(list_of_stmt.begin(), list_of_stmt.end(), *s_it));
      }
   }


   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(false);
   }
}
