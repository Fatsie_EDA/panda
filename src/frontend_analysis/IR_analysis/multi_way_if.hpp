/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file multi_way_if.hpp
 * @brief Analysis step rebuilding multi-way if.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef MULTI_WAY_IF_HPP
#define MULTI_WAY_IF_HPP

///Superclass include
#include "function_frontend_flow_step.hpp"

#include "refcount.hpp"
/**
 * @name forward declarations
 */
//@{
REF_FORWARD_DECL(tree_manager);
REF_FORWARD_DECL(tree_node);
REF_FORWARD_DECL(bloc);
class statement_list;
class gimple_cond;
//@}

///STL include
#include <unordered_map>
#include <deque>
#include <list>
#include <map>
#include <set>
#include <string>

/**
 * Structure the original short circuit
 */
class multi_way_if : public FunctionFrontendFlowStep
{
   private:

      void create_gimple_multi_way_if(unsigned int pred, unsigned int curr_bb, std::map<unsigned int, blocRef> & list_of_bloc);

      void update_cfg(unsigned int succ, unsigned int curr_bb, unsigned int pred, std::map<unsigned int, blocRef>& list_of_bloc);

      void extract_condition(const tree_managerRef TM, unsigned int &ssa1_node_nid, std::list<tree_nodeRef>& list_of_stmt_cond1, gimple_cond* ce1, unsigned int bb1, unsigned int type_index);

      void compute_not(unsigned int &res_ssa_id, unsigned int type_index, unsigned int ssa1_node_nid, const tree_managerRef TM, std::list<tree_nodeRef>& list_of_stmt, unsigned int bb_index);

      void compute_and(unsigned int &ssa_res_node_nid, unsigned int type_index, unsigned int ssa1_node_nid, unsigned int ssa2_node_nid, const tree_managerRef TM, std::list<tree_nodeRef>& list_of_stmt, unsigned int bb_index, bool second_negated);

      /**
       * Return the set of analyses in relationship with this design step
       * @param relationship_type is the type of relationship to be considered
       */
      const std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const;

   public:
      /**
       * Constructor.
       * @param Param is the set of the parameters
       * @param AppM is the application manager
       * @param function_id is the identifier of the function
       * @param DesignFlowManagerConstRef is the design flow manager
       */
      multi_way_if(const ParameterConstRef _Param, const application_managerRef _AppM, unsigned int function_id, const DesignFlowManagerConstRef design_flow_manager);

      /**
       *  Destructor
       */
      ~multi_way_if();

      /**
       * Restructures the unstructured code
       */
      void Exec();
};

#endif
