/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file loop_regions_computation.cpp
 * @brief Analysis step creating the regions for loops.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Autoheader include
#include "config_HAVE_ZEBU_BUILT.hpp"

///Header include
#include "loop_regions_computation.hpp"

///Behavior include
#include "behavioral_helper.hpp"
#include "application_manager.hpp"
#include "function_behavior.hpp"
#include "loop.hpp"
#include "loops.hpp"

///Frontend include
#include "Parameter.hpp"

///Graph include
#include "graph.hpp"
#include "basic_block.hpp"
#include "basic_blocks_graph_constructor.hpp"

///STD include
#include <fstream>

///Tree include
#include "tree_basic_block.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"
#include "tree_manipulation.hpp"

///Utility include
#include "Dominance.hpp"

loop_regions_computation::loop_regions_computation(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, LOOP_REGIONS_COMPUTATION, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

loop_regions_computation::~loop_regions_computation()
{}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > loop_regions_computation::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(LOOPS_IDENTIFICATION, SAME_FUNCTION));
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(OPERATIONS_CFG_COMPUTATION, SAME_FUNCTION));
         break;
      }
      case(PRECEDENCE_RELATIONSHIP) :
      {
#if HAVE_ZEBU_BUILT
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(LOOPS_ANALYSIS, SAME_FUNCTION));
#endif
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(LOOP_COMPUTATION, SAME_FUNCTION));
#if HAVE_ZEBU_BUILT && HAVE_EXPERIMENTAL
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(PARALLEL_LOOP_SWAP, SAME_FUNCTION));
#endif
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void loop_regions_computation::Exec()
{
   const BehavioralHelperRef behavioral_helper = function_behavior->GetBehavioralHelper();

   tree_managerRef TM = AppM->get_tree_manager();
   tree_manipulationRef IRman=tree_manipulationRef(new tree_manipulation(TM,debug_level));

   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }

   bool added_block = false;
   const BBGraphRef fbb = function_behavior->fbb;
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Adding labels");

   ///first label expressions are inserted
   const std::list<LoopConstRef> & loops = function_behavior->CGetLoops()->GetList();
   std::list<LoopConstRef>::const_iterator loop, loop_end = loops.end();
   for(loop = loops.begin(); loop != loop_end; loop++)
   {
      if ((*loop)->IsReducible())
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Analyzing loop " + boost::lexical_cast<std::string>((*loop)->GetId()));
         if((*loop)->GetId() != 0 and (((*loop)->loop_type & WHILE_LOOP) == 0 or ((*loop)->loop_type & SINGLE_EXIT_LOOP) == 0))
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Loop has not been built");
            const BBNodeInfoConstRef bb_node_info = fbb->CGetBBNodeInfo((*loop)->GetHeader());
            if(!behavioral_helper->start_with_a_label(bb_node_info->block))
            {
               ///create the tree node for a label expression
               IRman->create_label(bb_node_info->block, behavioral_helper->get_function_index());
            }
         }
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      }
      else
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level,  "Found an irreducible loop");
      }
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
   ///then gotos are added
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Adding gotos");
   loop_end = loops.end();
   for(loop = loops.begin(); loop != loop_end; loop++)
   {
      if ((*loop)->IsReducible())
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Analyzing loop " + boost::lexical_cast<std::string>((*loop)->GetId()));
         if((*loop)->GetId() != 0 and (((*loop)->loop_type & WHILE_LOOP) == 0 or ((*loop)->loop_type & SINGLE_EXIT_LOOP) == 0))
         {
            vertex bb_header = (*loop)->GetHeader();
            const BBNodeInfoConstRef bb_node_info = fbb->CGetBBNodeInfo(bb_header);
            unsigned int label_expr_nid = behavioral_helper->start_with_a_label(bb_node_info->block);
            THROW_ASSERT(label_expr_nid, "BB" + boost::lexical_cast<std::string>(bb_node_info->block->number) + " does not beigin with a label");
            InEdgeIterator ei, ei_end;
            const std::unordered_set<vertex> loop_blocks = (*loop)->get_blocks();
            std::unordered_set<vertex>::const_iterator bb_it = loop_blocks.begin(), bb_it_end = loop_blocks.end();
            std::unordered_set<vertex> incoming;
            for(boost::tie(ei, ei_end) = boost::in_edges(bb_header, *fbb); ei != ei_end; ei++)
            {
               incoming.insert(boost::source(*ei, *fbb));
            }
            std::unordered_set<vertex>::const_iterator it, it_end = incoming.end();
            for(it = incoming.begin(); it != it_end; it++)
            {
               vertex from_bb = *it;
               const BBNodeInfoConstRef bb_node_info_from = fbb->CGetBBNodeInfo(from_bb);
               if(std::find(bb_it, bb_it_end, from_bb) != bb_it_end)
               {
                  if(!behavioral_helper->end_with_a_cond_or_goto(bb_node_info_from->block))
                  {
                     IRman->create_goto(bb_node_info_from->block, behavioral_helper->get_function_index(), label_expr_nid);
                  }
                  else
                  {
                     //Checking if block finishes with an if
                     const std::list<tree_nodeRef> & list_of_stmt = bb_node_info_from->block->list_of_stmt;
                     if(!list_of_stmt.empty() && GET_NODE(list_of_stmt.back())->get_kind() == gimple_cond_K)
                     {
                        bool true_case;
                        unsigned int source_index = bb_node_info_from->block->number;
                        unsigned int target_index = bb_node_info->block->number;
                        if(bb_node_info_from->block->true_edge == target_index)
                           true_case = true;
                        else
                           true_case = false;
                        //We have to insert a basic block on the feedback edge
                        function_decl * fd = GetPointer<function_decl>(TM->GetTreeNode(function_id));
                        statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));
                        std::map<unsigned int, blocRef> & list_of_bloc = sl->list_of_bloc;
                        //Creating new block
                        blocRef new_bb = blocRef(new bloc());
                        new_bb->number = (list_of_bloc.rbegin())->second->number + 1;
                        new_bb->list_of_pred.push_back(source_index);
                        new_bb->list_of_succ.push_back(target_index);
                        list_of_bloc[new_bb->number] = new_bb;
                        //Updated target
                        for(unsigned int index = 0; index < list_of_bloc[target_index]->list_of_pred.size(); index++)
                        {
                           if(list_of_bloc[target_index]->list_of_pred[index] == source_index)
                           {
                              list_of_bloc[target_index]->list_of_pred[index] = new_bb->number;
                              break;
                           }
                        }

                        //Updated source
                        if(true_case)
                           bb_node_info_from->block->true_edge = new_bb->number;
                        else
                           bb_node_info_from->block->false_edge = new_bb->number;
                        for(unsigned int index = 0; index < list_of_bloc[source_index]->list_of_succ.size(); index++)
                        {
                           if(list_of_bloc[source_index]->list_of_succ[index] == target_index)
                           {
                              list_of_bloc[source_index]->list_of_succ[index] = new_bb->number;
                              break;
                           }
                        }

                        //Creating new vertex
                        const BasicBlocksGraphConstructorRef bbgc = function_behavior->bbgc;
                        bbgc->add_vertex(new_bb);

                        vertex source = bbgc->Cget_vertex(source_index);
                        vertex target = bbgc->Cget_vertex(target_index);
                        vertex new_vertex = bbgc->Cget_vertex(new_bb->number);

                        //Updating basic block graph
                        bbgc->AddEdge(source, new_vertex, CFG_SELECTOR);
                        bbgc->AddEdge(new_vertex, target, FB_CFG_SELECTOR);
                        if(true_case)
                           bbgc->add_bb_edge_info(source, new_vertex, CFG_SELECTOR, T_COND);
                        else
                           bbgc->add_bb_edge_info(source, new_vertex, CFG_SELECTOR, F_COND);
                        bbgc->RemoveEdge(source, target, fbb->GetSelector(source, target));

                        //Updating dominators
                        function_behavior->dominators->dom[new_vertex] = source;
                        function_behavior->post_dominators->dom[new_vertex] = target;
                        bbgc->AddEdge(source, new_vertex, D_SELECTOR);
                        bbgc->AddEdge(target, new_vertex, PD_SELECTOR);

                        //Updating loop information
                        unsigned int loop_id = fbb->CGetBBNodeInfo(source)->loop_id;
                        fbb->GetBBNodeInfo(new_vertex)->loop_id = loop_id;
                        function_behavior->GetLoops()->GetLoop(loop_id)->add_block(new_vertex);
                        added_block = true;
                        IRman->create_goto(new_bb, behavioral_helper->get_function_index(), label_expr_nid);
                     }
                  }
               }
            }
         }
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      }
      else
      {
         THROW_ERROR_CODE(IRREDUCIBLE_LOOPS_EC,"Irreducible loops not yet supported");
      }
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
   if(added_block and parameters->getOption<bool>(OPT_print_dot))
   {
      function_behavior->GetBBGraph(FunctionBehavior::BB)->WriteDot("BB_CFG.dot");
      function_behavior->GetBBGraph(FunctionBehavior::FBB)->WriteDot("BB_FCFG.dot");
      function_behavior->GetBBGraph(FunctionBehavior::DOM_TREE)->WriteDot("BB_dom_tree.dot");
      function_behavior->GetBBGraph(FunctionBehavior::POST_DOM_TREE)->WriteDot("BB_post_dom_tree.dot");

   }
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(false);
   }

}
