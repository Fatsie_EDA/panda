/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file switch_fix.cpp
 * @brief Analysis step that modifies the control flow graph to fix switches
 *
 * @author Marco Lattuada <marco.lattuada@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "switch_fix.hpp"

///. include
#include "Parameter.hpp"

///behavior include
#include "application_manager.hpp"

///tree include
#include "tree_basic_block.hpp"
#include "tree_manager.hpp"
#include "tree_node.hpp"
#include "tree_reindex.hpp"

SwitchFix::SwitchFix(const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager, const ParameterConstRef _parameters) :
   FunctionFrontendFlowStep(_AppM, _function_id, SWITCH_FIX, _design_flow_manager, _parameters)
{
   debug_level = _parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

SwitchFix::~SwitchFix()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FunctionFrontendFlowStep::FunctionRelationship> > SwitchFix::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
         {
            relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BLOCK_FIX, SAME_FUNCTION));
            break;
         }
      case(PRECEDENCE_RELATIONSHIP) :
      case(POST_DEPENDENCE_RELATIONSHIP) :
      {
         break;
      }
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void SwitchFix::Exec()
{
   const tree_managerRef TM = AppM->get_tree_manager();
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }
   tree_nodeRef temp = TM->get_tree_node_const(function_id);
   function_decl * fd = GetPointer<function_decl>(temp);
   statement_list * sl = GetPointer<statement_list>(GET_NODE(fd->body));

   std::map<unsigned int, blocRef> & list_of_bloc = sl->list_of_bloc;
   std::map<unsigned int, blocRef>::iterator it3, it3_end = list_of_bloc.end();

   /// Fix switch statements
   for(it3 = list_of_bloc.begin(); it3 != it3_end; it3++)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Examining BB" + boost::lexical_cast<std::string>(it3->first));
      std::list<tree_nodeRef> & list_of_stmt = it3->second->list_of_stmt;
      //Checking for switch
      if(!list_of_stmt.empty() && GET_NODE(*(list_of_stmt.rbegin()))->get_kind() == gimple_switch_K)
      {
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->BB" + boost::lexical_cast<std::string>(it3->first) + " ends with a switch");
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Checking if some successor has more than two gimple_label");
         ///The set of basic blocks which contain more than a label; this fix has to be performed before multiple_pred_switch check
         std::unordered_set<unsigned int> multiple_labels_blocks;
         std::vector<unsigned int> & list_of_successors = it3->second->list_of_succ;
         std::vector<unsigned int>::const_iterator succ, succ_end = list_of_successors.end();
         for(succ = list_of_successors.begin(); succ != succ_end; succ++)
         {
            std::list<tree_nodeRef> succ_list_of_stmt = list_of_bloc[*succ]->list_of_stmt;
            std::list<tree_nodeRef>::iterator next = succ_list_of_stmt.begin();
            next = succ_list_of_stmt.size()>1 ? ++next : next;
            if(succ_list_of_stmt.size() > 1 and GET_NODE(*next)->get_kind() == gimple_label_K)
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---BB" + boost::lexical_cast<std::string>(*succ));
               multiple_labels_blocks.insert(*succ);
            }
         }
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");

         std::unordered_set<unsigned int>::iterator multiple_labels_block, multiple_labels_block_end = multiple_labels_blocks.end();
         for(multiple_labels_block = multiple_labels_blocks.begin(); multiple_labels_block != multiple_labels_block_end; multiple_labels_block++)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Splitting BB" + boost::lexical_cast<std::string>(*multiple_labels_block));
            ///Compute the case labels of the switch
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Computing case labels");
            std::unordered_set<tree_nodeRef> cases;
            const gimple_switch * se = GetPointer<gimple_switch>(GET_NODE(*(list_of_stmt.rbegin())));
            const tree_vec * tv = GetPointer<tree_vec>(GET_NODE(se->op1));
            std::vector<tree_nodeRef>::const_iterator it, it_end = tv->list_of_op.end();
            for(it = tv->list_of_op.begin(); it != it_end; it++)
            {
               cases.insert(GET_NODE(GetPointer<case_label_expr>(GET_NODE(*it))->got));
            }
            ///First check that the first label is a case
            blocRef current_block = list_of_bloc[*multiple_labels_block];
            std::list<tree_nodeRef> & current_list_of_stmt = list_of_bloc[*multiple_labels_block]->list_of_stmt;
            tree_nodeRef current_tree_node = current_list_of_stmt.front();
            tree_nodeRef ld = GET_NODE(GetPointer<gimple_label>(GET_NODE(current_tree_node))->op);
            if(cases.find(ld) == cases.end())
            {
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->First label is not a case");
               size_t stmt_i = 1;
               std::list<tree_nodeRef>::iterator stmt_index = current_list_of_stmt.begin();
               THROW_ASSERT(current_list_of_stmt.size()>1, "expected more than one statement");
               ++stmt_index;
               while(stmt_index != current_list_of_stmt.end())
               {
                  INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Examining operation " + boost::lexical_cast<std::string>(stmt_i));
                  current_tree_node = *stmt_index;
                  THROW_ASSERT(GET_NODE(current_tree_node)->get_kind() == gimple_label_K, "An artificial label_decl has not been found at the beginning of the basic block");
                  ld = GET_NODE(GetPointer<gimple_label>(GET_NODE(current_tree_node))->op);
                  if(cases.find(ld) != cases.end())
                  {
                     break;
                  }
                  ++stmt_index;
                  ++stmt_i;
               }
               THROW_ASSERT(stmt_i < current_list_of_stmt.size(), "An artificial label_decl has not been found at the beginning of the basic block");
               std::iter_swap(current_list_of_stmt.begin(), stmt_index);
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Labels inverted");
            }
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Splitting first label");

            current_tree_node = current_list_of_stmt.front();
            current_list_of_stmt.erase(current_list_of_stmt.begin());
            ///Each label has to be put into a different basic block; first label is threated in a different way since it can have multiple predecessor
            ///Create new basic block
            blocRef new_bb = blocRef(new bloc());
            new_bb->number = list_of_bloc.rbegin()->first + 1;
            new_bb->loop_id = current_block->loop_id;
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Created BB" + boost::lexical_cast<std::string>(new_bb->number));
            list_of_bloc[new_bb->number] = new_bb;
            new_bb->list_of_pred = current_block->list_of_pred;
            ///Updating successors of the predecessor of the current block when they are not the switch
            for(auto predecessor : current_block->list_of_pred)
            {
               if(predecessor != it3->first)
               {
                  auto & successors = list_of_bloc[predecessor]->list_of_succ;
                  successors.erase(std::find(successors.begin(), successors.end(), *multiple_labels_block));
                  successors.push_back(new_bb->number);
               }
            }
            new_bb->list_of_stmt.push_front(current_tree_node);
            it3->second->list_of_succ.push_back(new_bb->number);
            blocRef previous_block = new_bb;
            std::list<tree_nodeRef>::iterator next = current_list_of_stmt.begin();
            next = current_list_of_stmt.size()>1 ? ++next : next;
            while(current_list_of_stmt.size() > 1 and GET_NODE(*next)->get_kind() == gimple_label_K)
            {
               ++next;
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Splitting next label");
               ///This is an intermediate label
               current_tree_node = current_list_of_stmt.front();
               current_list_of_stmt.erase(current_list_of_stmt.begin());
               ///Create new basic block
               new_bb = blocRef(new bloc());
               new_bb->number = list_of_bloc.rbegin()->first + 1;
               new_bb->loop_id = previous_block->loop_id;
               INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Created BB" + boost::lexical_cast<std::string>(new_bb->number));
               list_of_bloc[new_bb->number] = new_bb;
               new_bb->list_of_pred.push_back(previous_block->number);
               previous_block->list_of_succ.push_back(new_bb->number);
               new_bb->list_of_stmt.push_front(current_tree_node);
               previous_block = new_bb;

               ld = GET_NODE(GetPointer<gimple_label>(GET_NODE(current_tree_node))->op);
               if(cases.find(ld) != cases.end())
               {
                  new_bb->list_of_pred.push_back(it3->first);
                  it3->second->list_of_succ.push_back(new_bb->number);
               }
            }
            ///Predecessor of current node has to be fixed
            current_block->list_of_pred.clear();
            current_block->list_of_pred.push_back(previous_block->number);
            previous_block->list_of_succ.push_back(*multiple_labels_block);

            ///Check if this is a case : and not a label : - if so remove  this from successor of switch, otherwise add switch to predecessor of this
            current_tree_node = current_list_of_stmt.front();

            ld = GET_NODE(GetPointer<gimple_label>(GET_NODE(current_tree_node))->op);
            if(cases.find(ld) == cases.end())
            {
               std::vector<unsigned int> & switch_list_of_succ = it3->second->list_of_succ;

               THROW_ASSERT(std::find(switch_list_of_succ.begin(), switch_list_of_succ.end(), current_block->number) != switch_list_of_succ.end(), "current block not found between successors of current one");

               switch_list_of_succ.erase(std::find(switch_list_of_succ.begin(), switch_list_of_succ.end(), current_block->number));
            }
            else
            {
               current_block->list_of_pred.push_back(it3->first);
            }
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Split BB" + boost::lexical_cast<std::string>(*multiple_labels_block));
         }
         std::unordered_set<unsigned int> to_be_fixed;
         std::vector<unsigned int> & list_of_succ = it3->second->list_of_succ;
         std::vector<unsigned int>::const_iterator s, s_end = list_of_succ.end();
         for(s = list_of_succ.begin(); s != s_end; s++)
         {
            //if this check is true, *s follows a case without break or there is an empty case and *s is the basic block which closes the switch
            if(list_of_bloc[*s]->list_of_pred.size() > 1)
               to_be_fixed.insert(*s);
         }
         std::unordered_set<unsigned int>::const_iterator t, t_end = to_be_fixed.end();
         for(t = to_be_fixed.begin(); t != t_end; t++)
         {
            //Creating new basic block
            blocRef new_bb = blocRef(new bloc());
            new_bb->number = list_of_bloc.rbegin()->first + 1;
            new_bb->loop_id = list_of_bloc[it3->first]->loop_id;
            list_of_bloc[new_bb->number] = new_bb;
            new_bb->list_of_pred.push_back(it3->first);
            new_bb->list_of_succ.push_back(*t);
            size_t i, i_end;
            i_end = it3->second->list_of_succ.size();
            for(i = 0; i != i_end; i++)
            {
               if(it3->second->list_of_succ[i] == *t)
               {
                  it3->second->list_of_succ[i] = new_bb->number;
                  break;
               }
            }
            //if this flag true, there are more than one predecessor of *t basic block which end with a switch
            bool multiple_pred_switch = false;
            i_end = list_of_bloc[*t]->list_of_pred.size();
            for(i = 0; i != i_end; i++)
            {
               if(list_of_bloc[*t]->list_of_pred[i] == it3->first)
               {
                  list_of_bloc[*t]->list_of_pred[i] = new_bb->number;
                  std::vector<tree_nodeRef> & list_of_phi = list_of_bloc[*t]->list_of_phi;
                  std::vector<tree_nodeRef>::iterator phi, phi_end = list_of_phi.end();
                  for(phi = list_of_phi.begin(); phi != phi_end; phi++)
                  {
                     gimple_phi * current_phi = GetPointer<gimple_phi>(GET_NODE(*phi));
                     std::vector<std::pair< tree_nodeRef, unsigned int> > & list_of_def_edge = current_phi->list_of_def_edge;
                     std::vector<std::pair< tree_nodeRef, unsigned int> >::iterator definition, definition_end = list_of_def_edge.end();
                     for(definition = list_of_def_edge.begin(); definition != definition_end; definition++)
                     {
                        if(definition->second == it3->first)
                        {
                           definition->second = new_bb->number;
                        }
                     }
                  }
               }
               else
               {
                  if(list_of_bloc[list_of_bloc[*t]->list_of_pred[i]] && list_of_bloc[list_of_bloc[*t]->list_of_pred[i]]->list_of_stmt.size())
                  {
                     if(GET_NODE(*(list_of_bloc[list_of_bloc[*t]->list_of_pred[i]]->list_of_stmt.rbegin()))->get_kind() == gimple_switch_K)
                     {
                        multiple_pred_switch = true;
                     }
                  }
               }
            }
            //moving label expr
            THROW_ASSERT(GET_NODE(*(list_of_bloc[*t]->list_of_stmt.begin()))->get_kind() == gimple_label_K, "Block " + boost::lexical_cast<std::string>(*t) + " which follows a switch " + boost::lexical_cast<std::string>(it3->first) + " but it does not start with a label");
            if(multiple_pred_switch)
            {
               tree_nodeRef label_expr_node = *(list_of_bloc[*t]->list_of_stmt.begin());
               gimple_label * le = GetPointer<gimple_label>(GET_NODE(label_expr_node));
               tree_nodeRef label_decl_node = le->op;
               label_decl * ld = GetPointer<label_decl>(GET_NODE(label_decl_node));
               unsigned int new_label_decl_id = TM->new_tree_node_id();
               std::map<treeVocabularyTokenTypes::token_enum, std::string> IR_schema;
               IR_schema[TOK(TOK_TYPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ld->type));
               IR_schema[TOK(TOK_SCPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ld->scpe));
               IR_schema[TOK(TOK_SRCP)] = ld->include_name + ":" + boost::lexical_cast<std::string>(ld->line_number)+":"+boost::lexical_cast<std::string>(ld->column_number);
               IR_schema[TOK(TOK_ARTIFICIAL)] = boost::lexical_cast<std::string>(ld->artificial_flag);
               TM->create_tree_node(new_label_decl_id, label_decl_K, IR_schema);
               unsigned int new_label_expr_id = TM->new_tree_node_id();
               IR_schema.clear();
               IR_schema[TOK(TOK_SCPE)] = boost::lexical_cast<std::string>(GET_INDEX_NODE(ld->scpe));
               IR_schema[TOK(TOK_OP)] = boost::lexical_cast<std::string>(new_label_decl_id);
               IR_schema[TOK(TOK_SRCP)] = le->include_name + ":" + boost::lexical_cast<std::string>(le->line_number)+":"+boost::lexical_cast<std::string>(ld->column_number);
               TM->create_tree_node(new_label_expr_id, gimple_label_K, IR_schema);
               gimple_switch * se = GetPointer<gimple_switch>(GET_NODE(*(list_of_stmt.rbegin())));
               tree_vec * te = GetPointer<tree_vec>(GET_NODE(se->op1));
               std::vector<tree_nodeRef> & list_of_op = te->list_of_op;
               for(size_t ind = 0; ind < list_of_op.size(); ind++)
               {
                  case_label_expr * cl = GetPointer<case_label_expr>(GET_NODE(list_of_op[ind]));
                  if(GET_INDEX_NODE(cl->got) == GET_INDEX_NODE(le->op))
                  {
                     tree_nodeRef new_label_decl_reindex = TM->GetTreeReindex(new_label_decl_id);
                     cl->got = new_label_decl_reindex;
                     break;
                  }
               }
               new_bb->list_of_stmt.push_front(TM->GetTreeReindex(new_label_expr_id));
            }
            else
            {
               tree_nodeRef label = *(list_of_bloc[*t]->list_of_stmt.begin());
               list_of_bloc[*t]->list_of_stmt.erase(list_of_bloc[*t]->list_of_stmt.begin());
               new_bb->list_of_stmt.push_front(label);
            }
         }
         INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--");
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Examined BB" + boost::lexical_cast<std::string>(it3->first));
   }
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(false);
   }
}

