/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file function_frontend_flow_step.hpp
 * @brief This class contains the base representation for a generic frontend flow step which works on a single function
 *
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef FUNCTION_FRONTEND_FLOW_STEP_HPP
#define FUNCTION_FRONTEND_FLOW_STEP_HPP

///Autoheader include
#include "config_HAVE_ARCH_BUILT.hpp"

///Superclass include
#include "frontend_flow_step.hpp"

///Utility include
#include "refcount.hpp"

REF_FORWARD_DECL(ArchManager);
CONSTREF_FORWARD_DECL(DesignFlowManager);
REF_FORWARD_DECL(FunctionBehavior);

class FunctionFrontendFlowStep : public FrontendFlowStep
{
   protected:
      ///The function behavior of the function to be analyzed
      const FunctionBehaviorRef function_behavior;

      ///The index of the function to be analyzed
      const unsigned int function_id;

   public:
      /**
       * Constructor
       * @param _Param is the set of the parameters
       */
      FunctionFrontendFlowStep(const application_managerRef AppM, const unsigned int function_id, const FrontendFlowStepType frontend_flow_step_type, const DesignFlowManagerConstRef design_flow_manager, const ParameterConstRef parameters);

      /**
       * Destructor
       */
      virtual ~FunctionFrontendFlowStep();

      /**
       * Compute the relationships of a step with other steps
       * @param dependencies is where relationships will be stored
       * @param relationship_type is the type of relationship to be computed
       */
      virtual void ComputeRelationships(DesignFlowStepSet & relationship, const DesignFlowStep::RelationshipType relationship_type);

      /**
       * Execute this step
       */
      virtual void Exec() = 0;

      /**
       * Return the signature of this step
       */
      virtual const std::string GetSignature() const;

      /**
       * Return the name of this design step
       * @return the name of the pass (for debug purpose)
       */
      virtual const std::string GetName() const;

      /**
       * Compute the signature of a function frontend flow step
       * @param frontend_flow_step_type is the type of frontend flow
       * @param function_id is the index of the function
       * @return the corresponding signature
       */
      static
      const std::string ComputeSignature(const FrontendFlowStepType frontend_flow_step_type, const unsigned int function_id);
};
#endif
