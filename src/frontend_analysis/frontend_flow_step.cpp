/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file frontend_flow_step.cpp
 * @brief This class contains the base representation for a generic frontend flow step
 *
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "frontend_flow_step.hpp"

///Behavior include
#include "application_manager.hpp"
#include "call_graph_manager.hpp"

///Design flow include
#include "application_frontend_flow_step.hpp"
#include "design_flow_graph.hpp"
#include "design_flow_manager.hpp"
#include "frontend_flow_step_factory.hpp"
#include "function_frontend_flow_step.hpp"
#include "symbolic_application_frontend_flow_step.hpp"

///Parameter include
#include "Parameter.hpp"

///tree includes
#include "tree_manager.hpp"

FrontendFlowStep::FrontendFlowStep(const application_managerRef _AppM, const FrontendFlowStepType _frontend_flow_step_type, const DesignFlowManagerConstRef _design_flow_manager, const ParameterConstRef _parameters) :
   DesignFlowStep(_design_flow_manager, _parameters),
   AppM(_AppM),
   frontend_flow_step_type(_frontend_flow_step_type)
{
   debug_level = _parameters->get_class_debug_level(GET_CLASS(*this));
}

FrontendFlowStep::~FrontendFlowStep()
{}

void FrontendFlowStep::ComputeRelationships(DesignFlowStepSet & relationships, const DesignFlowStep::RelationshipType relationship_type)
{
   const CallGraphManagerConstRef call_graph_manager = AppM->CGetCallGraphManager();
   const DesignFlowGraphConstRef design_flow_graph = design_flow_manager.lock()->CGetDesignFlowGraph();
   const FrontendFlowStepFactory * frontend_flow_step_factory = GetPointer<const FrontendFlowStepFactory>(CGetDesignFlowStepFactory());
   const std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > frontend_relationships = ComputeFrontendRelationships(relationship_type);
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> >::const_iterator frontend_relationship, frontend_relationship_end = frontend_relationships.end();
   for(frontend_relationship = frontend_relationships.begin(); frontend_relationship != frontend_relationship_end; frontend_relationship++)
   {
      switch(frontend_relationship->second)
      {
         case(ALL_FUNCTIONS) :
         {
            const vertex call_graph_computation_step = design_flow_manager.lock()->GetDesignFlowStep(ApplicationFrontendFlowStep::ComputeSignature(FUNCTION_ANALYSIS));
            const DesignFlowStepRef cg_design_flow_step = call_graph_computation_step ? design_flow_graph->CGetDesignFlowStepInfo(call_graph_computation_step)->design_flow_step : frontend_flow_step_factory->CreateApplicationFrontendFlowStep(FUNCTION_ANALYSIS);
            relationships.insert(cg_design_flow_step);
            std::unordered_set<unsigned int> functions_with_body;
            const std::unordered_set<unsigned int> root_functions = AppM->CGetCallGraphManager()->GetRootFunctions();
            std::unordered_set<unsigned int>::const_iterator root_function, root_function_end = root_functions.end();
            for(root_function = root_functions.begin(); root_function != root_function_end; root_function++)
            {
               call_graph_manager->GetBodyFunctionRecursivelyCalledBy(*root_function, functions_with_body);
            }
            std::unordered_set<unsigned int>::const_iterator function_with_body, function_with_body_end = functions_with_body.end();
            for(function_with_body = functions_with_body.begin(); function_with_body != function_with_body_end; function_with_body++)
            {
               const vertex sdf_step = design_flow_manager.lock()->GetDesignFlowStep(FunctionFrontendFlowStep::ComputeSignature(frontend_relationship->first, *function_with_body));
               const DesignFlowStepRef design_flow_step = sdf_step ? design_flow_graph->CGetDesignFlowStepInfo(sdf_step)->design_flow_step : frontend_flow_step_factory->CreateFunctionFrontendFlowStep(frontend_relationship->first, *function_with_body);
               relationships.insert(design_flow_step);
            }
            break;
         }
         case(CALLING_FUNCTIONS):
         case(CALLED_FUNCTIONS) :
         case(SAME_FUNCTION) :
         {
            ///This is managed by FunctionFrontendFlowStep::ComputeRelationships
            break;
         }
         case(WHOLE_APPLICATION) :
         {
            vertex sdf_step = design_flow_manager.lock()->GetDesignFlowStep(ApplicationFrontendFlowStep::ComputeSignature(frontend_relationship->first));
            DesignFlowStepRef design_flow_step;
            if(sdf_step)
            {
               design_flow_step = design_flow_graph->CGetDesignFlowStepInfo(sdf_step)->design_flow_step;
               relationships.insert(design_flow_step);
            }
            else
            {
               design_flow_step = frontend_flow_step_factory->GenerateFrontendStep(frontend_relationship->first);
               relationships.insert(design_flow_step);
            }
            break;
         }
         default:
         {
            THROW_UNREACHABLE("Function relationship does not exist");
         }
      }
   }
}

const std::string FrontendFlowStep::GetKindText() const
{
   return EnumToKindText(frontend_flow_step_type);
}

const std::string FrontendFlowStep::EnumToKindText(const FrontendFlowStepType frontend_flow_step_type)
{
   switch(frontend_flow_step_type)
   {
      case(CREATE_TREE_MANAGER) :
         return "CreateTreeManager";
      case(FUNCTION_ANALYSIS) :
         return "CallGraphComputation";
#if HAVE_HOST_PROFILING_BUILT
      case(HPP_PROFILING) :
         return "HppProfiling";
      case(LOOPS_PROFILING) :
         return "LoopsProfiling";
#endif
#if HAVE_FROM_PRAGMA_BUILT
      case(PRAGMA_SUBSTITUTION) :
         return "PragmaSubstitution";
#endif
#if HAVE_HOST_PROFILING_BUILT
      case(READ_PROFILING_DATA) :
         return "ReadProfilingData";
#endif
#if HAVE_ZEBU_BUILT
      case(SIZEOF_SUBSTITUTION) :
         return "SizeofSubstitution";
#endif
      case(SYMBOLIC_APPLICATION_FRONTEND_FLOW_STEP) :
         return "SymbolicApplicationFrontendFlowStep";
#if HAVE_HOST_PROFILING_BUILT
      case(TP_PROFILING):
         return "TpProfiling";
#endif

      case(ADD_BB_ECFG_EDGES):
         return "AddBbEcfgdges";
#if HAVE_ZEBU_BUILT
      case(ADD_OP_ECFG_EDGES):
         return "AddOpEcfgEdges";
#endif
      case(ADD_OP_FLOW_EDGES):
         return "AddOpFlowEdges";
#if HAVE_ZEBU_BUILT || HAVE_BAMBU_BUILT
      case(ARRAY_REF_FIX):
         return "ArrayRefFix";
#endif
      case(BASIC_BLOCKS_CFG_COMPUTATION) :
         return "BasicBlocksCfgComputation";
      case(BB_FEEDBACK_EDGES_IDENTIFICATION) :
         return "BBFeedbackEdgesIdentification";
      case(BLOCK_FIX) :
         return "BlockFix";
#if HAVE_ZEBU_BUILT
      case(CALL_ARGS_STRUCTURING):
         return "CallArgsStructuring";
#endif
      case(CHECK_SYSTEM_TYPE) :
         return "CheckSystemType";
      case(CONTROL_DEPENDENCE_COMPUTATION) :
         return "ControlDependenceComputation";
#if HAVE_ZEBU_BUILT
      case(DEAD_CODE_ELIMINATION) :
         return "DeadCodeElimination";
#endif
#if HAVE_BAMBU_BUILT
      case(DETERMINE_MEMORY_ACCESSES):
         return "DetermineMemoryAccesses";
#endif
      case(DOM_POST_DOM_COMPUTATION):
         return "DomPostDomComputation";
#if HAVE_HOST_PROFILING_BUILT
      case(DUMP_PROFILING_DATA):
         return "DumpProfilingData";
#endif
#if HAVE_EXPERIMENTAL
      case(EXTENDED_PDG_COMPUTATION):
         return "ExtendedPdgComputation";
#endif
#if HAVE_ZEBU_BUILT
      case(GLOBAL_VARIABLES_ANALYSIS) :
         return "GlobalVariablesAnalysis";
#endif
#if HAVE_ZEBU_BUILT
      case(HEADER_STRUCTURING) :
         return "HeaderStructuring";
#endif
#if HAVE_HOST_PROFILING_BUILT
      case(HOST_PROFILING) :
         return "HostProfiling";
#endif
#if HAVE_ZEBU_BUILT
      case(INSTRUCTION_SEQUENCES_COMPUTATION) :
         return "InstructionSequencesComputation";
#endif
#if HAVE_BAMBU_BUILT
      case(IR_LOWERING) :
         return "IrLowering";
#endif
      case(LOOP_COMPUTATION) :
         return "LoopComputation";
      case(LOOP_REGIONS_COMPUTATION) :
         return "LoopRegionsComputation";
      case(LOOP_REGIONS_FLOW_COMPUTATION) :
         return "LoopRegionsFlowComputation";
#if HAVE_ZEBU_BUILT
      case(LOOPS_ANALYSIS) :
         return "LoopsAnalysis";
#endif
      case(LOOPS_IDENTIFICATION) :
         return "LoopsIdentification";
#if HAVE_BAMBU_BUILT
      case MULTI_WAY_IF:
         return "MultiWayIf";
      case NI_SSA_LIVENESS:
         return "NiSsaLiveness";
#endif
      case(OP_FEEDBACK_EDGES_IDENTIFICATION):
         return "OpFeedbackEdgesIdentification";
      case(OPERATIONS_CFG_COMPUTATION) :
         return "OperationsCfgComputation";
      case(ORDER_COMPUTATION) :
         return "OrderComputation";
#if HAVE_ZEBU_BUILT && HAVE_EXPERIMENTAL
      case(PARALLEL_LOOP_SWAP) :
         return "ParallelLoopSwap";
      case(PARALLEL_LOOPS_ANALYSIS):
         return "ParallelLoopsAnalysis";
#endif
#if HAVE_EXPERIMENTAL
      case(PARALLEL_REGIONS_GRAPH_COMPUTATION):
         return "ParallelRegionsGraphComputation";
#endif
#if HAVE_BAMBU_BUILT
      case PHI_OPT:
         return "PhiOpt";
#endif
#if HAVE_ZEBU_BUILT
      case(POINTED_DATA_COMPUTATION):
         return "PointedDataComputation";
      case(POINTED_DATA_EVALUATION):
         return "PointedDataEvaluation";
#endif
#if HAVE_FROM_PRAGMA_BUILT
      case(PRAGMA_ANALYSIS) :
         return "PragmaAnalysis";
#endif
#if HAVE_ZEBU_BUILT
      case(PREDICTABILITY_ANALYSIS):
         return "PredictabilityAnalysis";
      case(PROBABILITY_PATH) :
         return "ProbabilityPath";
#endif
      case(REACHABILITY_COMPUTATION) :
         return "ReachabilityComputation";
#if HAVE_EXPERIMENTAL
      case(REDUCED_PDG_COMPUTATION):
         return "ReducedPdgComputation";
#endif
#if HAVE_ZEBU_BUILT
#if HAVE_EXPERIMENTAL
      case(REFINED_DATA_FLOW_ANALYSIS):
         return "RefinedDataFlowAnalysis";
      case(REFINED_VAR_COMPUTATION):
         return "RefinedVarComputation";
#endif
#endif
#if HAVE_BAMBU_BUILT
      case REMOVE_CLOBBER_GA:
         return "RemoveClobberGA";
#endif
#if HAVE_ZEBU_BUILT
      case(REVERSE_RESTRICT_COMPUTATION):
         return "ReverseRestrictComputation";
      case(SHORT_CIRCUIT_STRUCTURING):
         return "ShortCircuitStructuring";
#endif

#if HAVE_BAMBU_BUILT
      case SHORT_CIRCUIT_TAF:
         return "ShortCircuitTAF";
      case SIMPLE_CODE_MOTION:
         return "SimpleCodeMotion";
      case(SOFT_FLOAT_CG_EXT):
         return "SoftFloatCgExt";
#endif
#if HAVE_BAMBU_BUILT && HAVE_EXPERIMENTAL
      case(SPECULATION_EDGES_COMPUTATION):
         return "SpeculationEdgesComputation";
#endif
      case(SPLIT_PHINODES) :
         return "SplitPhinodes";
      case(SSA_DATA_FLOW_ANALYSIS):
         return "SsaDataFlowAnalysis";
      case(SWITCH_FIX):
         return "SwitchFix";
#if HAVE_BAMBU_BUILT
      case(UNROLLING_DEGREE):
         return "UnrollingDegree";
#endif
#if HAVE_RTL_BUILT && HAVE_ZEBU_BUILT
      case(UPDATE_RTL_WEIGHT):
         return "UpdateRtlWeight";
#endif
#if HAVE_ZEBU_BUILT
      case(UPDATE_TREE_WEIGHT) :
         return "UpdateTreeWeight";
#endif
      case(USE_COUNTING) :
         return "UseCounting";
      case(VAR_ANALYSIS):
         return "VarAnalysis";
      case(VAR_DECL_FIX):
         return "VarDeclFix";
#if HAVE_BAMBU_BUILT
      case(VERIFICATION_OPERATION):
         return "VerificationOperation";
      case VIRTUAL_PHI_NODES_SPLIT:
         return "VirtualPhiNodesSplit";
#endif
      default:
         THROW_UNREACHABLE("Frontend flow step type does not exist");
   }
   return "";
}

const DesignFlowStepFactoryConstRef FrontendFlowStep::CGetDesignFlowStepFactory() const
{
   return design_flow_manager.lock()->CGetDesignFlowStepFactory("Frontend");
}

void FrontendFlowStep::PrintTreeManager(const bool before) const
{
   const tree_managerConstRef tree_manager = AppM->get_tree_manager();
   const std::string prefix = before ? "before" : "after";
   const std::string file_name = parameters->getOption<std::string>(OPT_output_temporary_directory) + prefix + "_" + GetName();
   const std::string raw_file_name = file_name + ".raw";
   std::ofstream raw_file(raw_file_name.c_str());
   tree_manager->print(raw_file);
   raw_file.close();
   const std::string gimple_file_name = file_name + ".gimple";
   std::ofstream gimple_file(gimple_file_name.c_str());
   tree_manager->PrintGimple(gimple_file, false);
   gimple_file.close();
}

