/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file function_frontend_flow_step.cpp
 * @brief This class contains the base representation for a generic frontend flow step
 *
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "function_frontend_flow_step.hpp"

///Behavior include
#include "application_manager.hpp"
#include "behavioral_helper.hpp"
#include "call_graph.hpp"
#include "call_graph_manager.hpp"
#include "function_behavior.hpp"

///Design flow include
#include "design_flow_graph.hpp"
#include "design_flow_manager.hpp"
#include "frontend_flow_step_factory.hpp"

///Parameter include
#include "Parameter.hpp"

///Utility include
#include "utility.hpp"

FunctionFrontendFlowStep::FunctionFrontendFlowStep(const application_managerRef _AppM, const unsigned int _function_id, const FrontendFlowStepType _frontend_flow_step_type,  const DesignFlowManagerConstRef _design_flow_manager, const ParameterConstRef _parameters) :
   FrontendFlowStep(_AppM, _frontend_flow_step_type, _design_flow_manager, _parameters),
   function_behavior(_AppM->GetFunctionBehavior(_function_id)),
   function_id(_function_id)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this));
}

FunctionFrontendFlowStep::~FunctionFrontendFlowStep()
{}

const std::string FunctionFrontendFlowStep::GetSignature() const
{
   return ComputeSignature(frontend_flow_step_type, function_id);
}

const std::string FunctionFrontendFlowStep::ComputeSignature(const FrontendFlowStepType frontend_flow_step_type, const unsigned int function_id)
{
   return "Frontend::" + boost::lexical_cast<std::string>(frontend_flow_step_type) + "::" + boost::lexical_cast<std::string>(function_id);
}

const std::string FunctionFrontendFlowStep::GetName() const
{
   return "Frontend::" + GetKindText() + "::" + function_behavior->CGetBehavioralHelper()->get_function_name();
}

void FunctionFrontendFlowStep::ComputeRelationships(DesignFlowStepSet & relationships, const DesignFlowStep::RelationshipType relationship_type)
{
   const DesignFlowGraphConstRef design_flow_graph = design_flow_manager.lock()->CGetDesignFlowGraph();
   const FrontendFlowStepFactory * frontend_flow_step_factory = GetPointer<const FrontendFlowStepFactory>(CGetDesignFlowStepFactory());
   const std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > frontend_relationships = ComputeFrontendRelationships(relationship_type);
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> >::const_iterator frontend_relationship, frontend_relationship_end = frontend_relationships.end();
   for(frontend_relationship = frontend_relationships.begin(); frontend_relationship != frontend_relationship_end; frontend_relationship++)
   {
      switch(frontend_relationship->second)
      {
         case(ALL_FUNCTIONS) :
         {
            ///This is managed by FrontendFlowStep::ComputeRelationships
            break;
         }
         case(CALLED_FUNCTIONS):
         {
            const CallGraphManagerConstRef call_graph_manager = AppM->CGetCallGraphManager();
            const CallGraphConstRef acyclic_call_graph = call_graph_manager->CGetAcyclicCallGraph();
            const vertex function_vertex = call_graph_manager->GetVertex(function_id);
            OutEdgeIterator oe, oe_end;
            for(boost::tie(oe, oe_end) = boost::out_edges(function_vertex, *acyclic_call_graph); oe != oe_end; oe++)
            {
               const vertex target = boost::target(*oe, *acyclic_call_graph);
               const unsigned int called_function = call_graph_manager->get_function(target);
               if(AppM->CGetFunctionBehavior(called_function)->CGetBehavioralHelper()->has_implementation() and function_id != called_function)
               {
                  vertex function_frontend_flow_step = design_flow_manager.lock()->GetDesignFlowStep(FunctionFrontendFlowStep::ComputeSignature(frontend_relationship->first, called_function));
                  DesignFlowStepRef design_flow_step;
                  if(function_frontend_flow_step)
                  {
                     design_flow_step = design_flow_graph->CGetDesignFlowStepInfo(function_frontend_flow_step)->design_flow_step;
                     relationships.insert(design_flow_step);
                  }
                  else
                  {
                     design_flow_step = frontend_flow_step_factory->CreateFunctionFrontendFlowStep(frontend_relationship->first, called_function);
                     relationships.insert(design_flow_step);
                  }
               }
            }
            break;
         }
         case(CALLING_FUNCTIONS):
         {
            const CallGraphManagerConstRef call_graph_manager = AppM->CGetCallGraphManager();
            const CallGraphConstRef acyclic_call_graph = call_graph_manager->CGetAcyclicCallGraph();
            const vertex function_vertex = call_graph_manager->GetVertex(function_id);
            InEdgeIterator ie, ie_end;
            for(boost::tie(ie, ie_end) = boost::in_edges(function_vertex, *acyclic_call_graph); ie != ie_end; ie++)
            {
               const vertex source = boost::source(*ie, *acyclic_call_graph);
               const unsigned int calling_function = call_graph_manager->get_function(source);
               if(calling_function != function_id)
               {
                  vertex function_frontend_flow_step = design_flow_manager.lock()->GetDesignFlowStep(FunctionFrontendFlowStep::ComputeSignature(frontend_relationship->first, calling_function));
                  DesignFlowStepRef design_flow_step;
                  if(function_frontend_flow_step)
                  {
                     design_flow_step = design_flow_graph->CGetDesignFlowStepInfo(function_frontend_flow_step)->design_flow_step;
                     relationships.insert(design_flow_step);
                  }
                  else
                  {
                     design_flow_step = frontend_flow_step_factory->CreateFunctionFrontendFlowStep(frontend_relationship->first, calling_function);
                     relationships.insert(design_flow_step);
                  }
               }
            }
            break;
         }
         case(SAME_FUNCTION) :
         {
            vertex sdf_step = design_flow_manager.lock()->GetDesignFlowStep(FunctionFrontendFlowStep::ComputeSignature(frontend_relationship->first, function_id));
            DesignFlowStepRef design_flow_step;
            if(sdf_step)
            {
               design_flow_step = design_flow_graph->CGetDesignFlowStepInfo(sdf_step)->design_flow_step;
               relationships.insert(design_flow_step);
            }
            else
            {
               design_flow_step = frontend_flow_step_factory->CreateFunctionFrontendFlowStep(frontend_relationship->first, function_id);
               relationships.insert(design_flow_step);
            }
            break;
         }
         case(WHOLE_APPLICATION) :
         {
            ///This is managed by FrontendFlowStep::ComputeRelationships
            break;
         }
         default:
         {
            THROW_UNREACHABLE("Function relationship does not exist");
         }
      }
   }
   FrontendFlowStep::ComputeRelationships(relationships, relationship_type);
}

