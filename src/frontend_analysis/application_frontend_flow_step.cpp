/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file application_frontend_flow_step.cpp
 * @brief This class contains the base representation for a generic frontend flow step which works on the whole function
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "application_frontend_flow_step.hpp"

///Frontend flow step include
#include "symbolic_application_frontend_flow_step.hpp"

///Parameter include
#include "Parameter.hpp"

ApplicationFrontendFlowStep::ApplicationFrontendFlowStep(const application_managerRef _AppM, const FrontendFlowStepType _frontend_flow_step_type, const DesignFlowManagerConstRef _design_flow_manager, const ParameterConstRef _parameters) :
   FrontendFlowStep(_AppM, _frontend_flow_step_type, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this));
}

ApplicationFrontendFlowStep::~ApplicationFrontendFlowStep()
{}

const std::string ApplicationFrontendFlowStep::ComputeSignature(const FrontendFlowStepType frontend_flow_step_type)
{
   switch(frontend_flow_step_type)
   {
      case ADD_BB_ECFG_EDGES:
#if HAVE_ZEBU_BUILT
      case ADD_OP_ECFG_EDGES:
#endif
      case ADD_OP_FLOW_EDGES:
#if HAVE_ZEBU_BUILT || HAVE_BAMBU_BUILT
      case(ARRAY_REF_FIX):
#endif
      case BASIC_BLOCKS_CFG_COMPUTATION:
      case BB_FEEDBACK_EDGES_IDENTIFICATION:
      case BLOCK_FIX:
#if HAVE_ZEBU_BUILT
      case CALL_ARGS_STRUCTURING:
#endif
      case CHECK_SYSTEM_TYPE:
      case CONTROL_DEPENDENCE_COMPUTATION:
#if HAVE_ZEBU_BUILT
      case DEAD_CODE_ELIMINATION:
#endif
#if HAVE_BAMBU_BUILT
      case DETERMINE_MEMORY_ACCESSES:
#endif
      case DOM_POST_DOM_COMPUTATION:
#if HAVE_EXPERIMENTAL
      case EXTENDED_PDG_COMPUTATION:
#endif
#if HAVE_ZEBU_BUILT
      case GLOBAL_VARIABLES_ANALYSIS:
#endif
#if HAVE_ZEBU_BUILT
      case HEADER_STRUCTURING:
      case INSTRUCTION_SEQUENCES_COMPUTATION:
#endif
#if HAVE_BAMBU_BUILT
      case IR_LOWERING:
#endif
      case LOOP_COMPUTATION:
      case LOOP_REGIONS_COMPUTATION:
      case LOOP_REGIONS_FLOW_COMPUTATION:
#if HAVE_ZEBU_BUILT
      case LOOPS_ANALYSIS :
#endif
      case LOOPS_IDENTIFICATION:
#if HAVE_BAMBU_BUILT
      case MULTI_WAY_IF:
      case NI_SSA_LIVENESS:
#endif
      case OP_FEEDBACK_EDGES_IDENTIFICATION:
      case OPERATIONS_CFG_COMPUTATION:
      case ORDER_COMPUTATION:
#if HAVE_ZEBU_BUILT && HAVE_EXPERIMENTAL
      case PARALLEL_LOOP_SWAP:
      case PARALLEL_LOOPS_ANALYSIS:
#endif
#if HAVE_EXPERIMENTAL
      case PARALLEL_REGIONS_GRAPH_COMPUTATION:
#endif
#if HAVE_BAMBU_BUILT
      case PHI_OPT:
#endif
#if HAVE_ZEBU_BUILT
      case POINTED_DATA_COMPUTATION:
#endif
#if HAVE_FROM_PRAGMA_BUILT
      case(PRAGMA_ANALYSIS) :
#endif
#if HAVE_ZEBU_BUILT
      case PREDICTABILITY_ANALYSIS:
      case PROBABILITY_PATH:
#endif
      case REACHABILITY_COMPUTATION:
#if HAVE_ZEBU_BUILT
#if HAVE_EXPERIMENTAL
      case REFINED_DATA_FLOW_ANALYSIS:
      case REFINED_VAR_COMPUTATION:
#endif
#endif
#if HAVE_EXPERIMENTAL
      case REDUCED_PDG_COMPUTATION:
#endif
#if HAVE_BAMBU_BUILT
      case REMOVE_CLOBBER_GA:
#endif
#if HAVE_ZEBU_BUILT
      case REVERSE_RESTRICT_COMPUTATION:
      case SHORT_CIRCUIT_STRUCTURING:
#endif
#if HAVE_BAMBU_BUILT
      case SHORT_CIRCUIT_TAF:
      case SIMPLE_CODE_MOTION:
      case SOFT_FLOAT_CG_EXT:
#endif
#if HAVE_BAMBU_BUILT && HAVE_EXPERIMENTAL
      case SPECULATION_EDGES_COMPUTATION:
#endif
      case SPLIT_PHINODES:
      case SSA_DATA_FLOW_ANALYSIS:
      case SWITCH_FIX:
#if HAVE_RTL_BUILT && HAVE_ZEBU_BUILT
      case UPDATE_RTL_WEIGHT:
#endif
#if HAVE_ZEBU_BUILT
      case UPDATE_TREE_WEIGHT:
#endif
#if HAVE_BAMBU_BUILT
      case UNROLLING_DEGREE:
#endif
      case USE_COUNTING:
      case VAR_ANALYSIS:
      case VAR_DECL_FIX:
#if HAVE_BAMBU_BUILT
      case VERIFICATION_OPERATION:
      case VIRTUAL_PHI_NODES_SPLIT:
#endif
         {
            return SymbolicApplicationFrontendFlowStep::ComputeSignature(frontend_flow_step_type);
         }
      case(CREATE_TREE_MANAGER) :
#if HAVE_HOST_PROFILING_BUILT
      case(DUMP_PROFILING_DATA) :
#endif
      case(FUNCTION_ANALYSIS) :
#if HAVE_HOST_PROFILING_BUILT
      case(HOST_PROFILING):
      case(HPP_PROFILING):
      case(LOOPS_PROFILING) :
#endif
#if HAVE_ZEBU_BUILT
      case POINTED_DATA_EVALUATION:
#endif
#if HAVE_FROM_PRAGMA_BUILT
      case(PRAGMA_SUBSTITUTION) :
#endif
#if HAVE_HOST_PROFILING_BUILT
      case(READ_PROFILING_DATA) :
#endif
#if HAVE_ZEBU_BUILT
      case(SIZEOF_SUBSTITUTION):
#endif
      case(SYMBOLIC_APPLICATION_FRONTEND_FLOW_STEP) :
#if HAVE_HOST_PROFILING_BUILT
      case(TP_PROFILING) :
#endif
         {
            return "Frontend::" + boost::lexical_cast<std::string>(frontend_flow_step_type);
         }
      default:
         THROW_UNREACHABLE("Frontend flow step type does not exist");

   }
   return "";
}

const std::string ApplicationFrontendFlowStep::GetSignature() const
{
   return ComputeSignature(frontend_flow_step_type);
}

const std::string ApplicationFrontendFlowStep::GetName() const
{
   return "Frontend::" + GetKindText();
}

