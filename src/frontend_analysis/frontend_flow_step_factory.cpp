/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file frontend_flow_step_factory.hpp
 * @brief This class contains the methods to create a frontend flow step
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Header include
#include "frontend_flow_step_factory.hpp"

///Autoheader include
#include "config_HAVE_ARCH_BUILT.hpp"
#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_FROM_PRAGMA_BUILT.hpp"
#include "config_HAVE_HOST_PROFILING_BUILT.hpp"
#include "config_HAVE_ZEBU_BUILT.hpp"

///Passes includes
#include "design_flow_step.hpp"
#include "add_bb_ecfg_edges.hpp"
#if HAVE_ZEBU_BUILT
#include "add_op_ecfg_edges.hpp"
#endif
#include "add_op_flow_edges.hpp"
#if HAVE_ZEBU_BUILT || HAVE_BAMBU_BUILT
#include "array_ref_fix.hpp"
#endif
#include "basic_blocks_cfg_computation.hpp"
#include "bb_feedback_edges_computation.hpp"
#include "block_fix.hpp"
#if HAVE_ZEBU_BUILT
#include "call_args_structuring.hpp"
#endif
#include "call_graph_computation.hpp"
#include "cdg_computation.hpp"
#include "check_system_type.hpp"
#include "create_tree_manager.hpp"
#if HAVE_ZEBU_BUILT
#include "dead_code_elimination.hpp"
#endif
#if HAVE_BAMBU_BUILT
#include "determine_memory_accesses.hpp"
#endif
#include "dom_post_dom_computation.hpp"
#if HAVE_HOST_PROFILING_BUILT
#include "dump_profiling_data.hpp"
#endif
#if HAVE_EXPERIMENTAL
#include "extended_pdg_computation.hpp"
#include "parallel_regions_graph_computation.hpp"
#endif
#if HAVE_ZEBU_BUILT
#include "global_variables_analysis.hpp"
#include "header_structuring.hpp"
#endif
#if HAVE_HOST_PROFILING_BUILT
#include "hpp_profiling.hpp"
#endif
#include "IR_lowering.hpp"
#if HAVE_ZEBU_BUILT
#include "instruction_sequences_computation.hpp"
#include "loops_analysis.hpp"
#endif
#include "loops_computation.hpp"
#include "loop_regions_computation.hpp"
#include "loop_regions_flow_computation.hpp"
#if HAVE_BAMBU_BUILT
#include "multi_way_if.hpp"
#include "NI_SSA_liveness.hpp"
#endif
#include "op_feedback_edges_computation.hpp"
#include "operations_cfg_computation.hpp"
#include "order_computation.hpp"
#if HAVE_EXPERIMENTAL
#include "parallel_loop_swap.hpp"
#include "parallel_loops_analysis.hpp"
#endif
#if HAVE_BAMBU_BUILT
#include "phi_opt.hpp"
#endif
#if HAVE_ZEBU_BUILT
#include "pointed_data_computation.hpp"
#include "pointed_data_evaluation.hpp"
#endif
#if HAVE_FROM_PRAGMA_BUILT
#include "pragma_substitution.hpp"
#endif
#if HAVE_ZEBU_BUILT
#include "predictability_analysis.hpp"
#endif
#if HAVE_HOST_PROFILING_BUILT
#include "probability_path.hpp"
#include "profiling.hpp"
#endif
#include "reachability_computation.hpp"
#if HAVE_EXPERIMENTAL
#include "reduced_pdg_computation.hpp"
#endif
#if HAVE_ZEBU_BUILT
#include "refined_data_flow_analysis.hpp"
#include "refined_var_computation.hpp"
#endif
#if HAVE_BAMBU_BUILT
#include "remove_clobber_ga.hpp"
#endif
#if HAVE_ZEBU_BUILT
#include "reverse_restrict_computation.hpp"
#endif
#include "ssa_data_dependence_computation.hpp"
#if HAVE_ZEBU_BUILT
#include "short_circuit_structuring.hpp"
#include "sizeof_substitution.hpp"
#endif
#if HAVE_BAMBU_BUILT
#include "short_circuit_taf.hpp"
#include "simple_code_motion.hpp"
#include "soft_float_cg_ext.hpp"
#if HAVE_EXPERIMENTAL
#include "speculation_edges_computation.hpp"
#endif
#endif
#include "split_phi_nodes.hpp"
#include "switch_fix.hpp"
#if HAVE_HOST_PROFILING_BUILT
#include "tp_profiling.hpp"
#endif
#if HAVE_EXPERIMENTAL
#if HAVE_BAMBU_BUILT
#include "unrolling_degree.hpp"
#endif
#endif
#if HAVE_ZEBU_BUILT && HAVE_RTL_BUILT
#include "update_rtl_weight.hpp"
#endif
#if HAVE_ZEBU_BUILT
#include "update_tree_weight.hpp"
#endif
#include "use_counting.hpp"
#include "var_computation.hpp"
#include "var_decl_fix.hpp"
#if HAVE_BAMBU_BUILT
#if HAVE_EXPERIMENTAL
#include "verification_operation.hpp"
#endif
#include "virtual_phi_nodes_split.hpp"
#endif

#include "call_graph_computation.hpp"
#include "create_tree_manager.hpp"
#if HAVE_HOST_PROFILING_BUILT
#include "loops_profiling.hpp"
#endif
#if HAVE_FROM_PRAGMA_BUILT
#include "pragma_analysis.hpp"
#endif
#if HAVE_HOST_PROFILING_BUILT
#include "read_profiling_data.hpp"
#endif
#include "symbolic_application_frontend_flow_step.hpp"

#if HAVE_ARCH_BUILT
FrontendFlowStepFactory::FrontendFlowStepFactory(const application_managerRef _AppM, const ArchManagerRef _arch_manager, const DesignFlowManagerConstRef _design_flow_manager, const ParameterConstRef _parameters) :
   DesignFlowStepFactory(_design_flow_manager, _parameters),
   AppM(_AppM),
   arch_manager(_arch_manager)
{}
#endif

FrontendFlowStepFactory::FrontendFlowStepFactory(const application_managerRef _AppM, const DesignFlowManagerConstRef _design_flow_manager,  const ParameterConstRef _parameters) :
   DesignFlowStepFactory(_design_flow_manager, _parameters),
   AppM(_AppM)
{}

FrontendFlowStepFactory::~FrontendFlowStepFactory()
{}

const DesignFlowStepSet FrontendFlowStepFactory::GenerateFrontendSteps(const std::unordered_set<FrontendFlowStepType> frontend_flow_step_types) const
{
   DesignFlowStepSet frontend_flow_steps;
   std::unordered_set<FrontendFlowStepType>::const_iterator frontend_flow_step_type, frontend_flow_step_type_end = frontend_flow_step_types.end();
   for(frontend_flow_step_type = frontend_flow_step_types.begin(); frontend_flow_step_type != frontend_flow_step_type_end; frontend_flow_step_type++)
   {
      frontend_flow_steps.insert(GenerateFrontendStep(*frontend_flow_step_type));
   }
   return frontend_flow_steps;
}

const DesignFlowStepRef FrontendFlowStepFactory::GenerateFrontendStep(FrontendFlowStepType frontend_flow_step_type) const
{
   switch(frontend_flow_step_type)
   {
      case ADD_BB_ECFG_EDGES:
#if HAVE_ZEBU_BUILT
      case ADD_OP_ECFG_EDGES:
#endif
      case ADD_OP_FLOW_EDGES:
#if HAVE_ZEBU_BUILT || HAVE_BAMBU_BUILT
      case(ARRAY_REF_FIX):
#endif
      case BASIC_BLOCKS_CFG_COMPUTATION:
      case BB_FEEDBACK_EDGES_IDENTIFICATION:
      case BLOCK_FIX:
#if HAVE_ZEBU_BUILT
      case CALL_ARGS_STRUCTURING:
#endif
      case CHECK_SYSTEM_TYPE:
      case CONTROL_DEPENDENCE_COMPUTATION:
#if HAVE_ZEBU_BUILT
      case DEAD_CODE_ELIMINATION:
#endif
#if HAVE_BAMBU_BUILT
      case DETERMINE_MEMORY_ACCESSES:
#endif
      case DOM_POST_DOM_COMPUTATION:
#if HAVE_EXPERIMENTAL
      case EXTENDED_PDG_COMPUTATION:
#endif
#if HAVE_ZEBU_BUILT
      case GLOBAL_VARIABLES_ANALYSIS:
#endif
#if HAVE_ZEBU_BUILT
      case HEADER_STRUCTURING:
      case INSTRUCTION_SEQUENCES_COMPUTATION:
#endif
#if HAVE_BAMBU_BUILT
      case IR_LOWERING:
#endif
      case LOOP_COMPUTATION:
      case LOOP_REGIONS_COMPUTATION:
      case LOOP_REGIONS_FLOW_COMPUTATION:
#if HAVE_ZEBU_BUILT
      case LOOPS_ANALYSIS :
#endif
      case LOOPS_IDENTIFICATION:
#if HAVE_BAMBU_BUILT
      case MULTI_WAY_IF:
      case NI_SSA_LIVENESS:
#endif
      case OP_FEEDBACK_EDGES_IDENTIFICATION:
      case OPERATIONS_CFG_COMPUTATION:
      case ORDER_COMPUTATION:
#if HAVE_ZEBU_BUILT && HAVE_EXPERIMENTAL
      case PARALLEL_LOOP_SWAP:
         case PARALLEL_LOOPS_ANALYSIS:
#endif
#if HAVE_EXPERIMENTAL
      case PARALLEL_REGIONS_GRAPH_COMPUTATION:
#endif
#if HAVE_BAMBU_BUILT
      case PHI_OPT:
#endif
#if HAVE_ZEBU_BUILT
      case POINTED_DATA_COMPUTATION:
#endif
#if HAVE_FROM_PRAGMA_BUILT
      case(PRAGMA_ANALYSIS) :
#endif
#if HAVE_ZEBU_BUILT
      case PREDICTABILITY_ANALYSIS:
      case PROBABILITY_PATH:
#endif
      case REACHABILITY_COMPUTATION:
#if HAVE_EXPERIMENTAL
      case REDUCED_PDG_COMPUTATION:
#endif
#if HAVE_ZEBU_BUILT
#if HAVE_EXPERIMENTAL
      case REFINED_DATA_FLOW_ANALYSIS:
      case REFINED_VAR_COMPUTATION:
#endif
#endif
#if HAVE_BAMBU_BUILT
      case REMOVE_CLOBBER_GA:
#endif
#if HAVE_ZEBU_BUILT
      case REVERSE_RESTRICT_COMPUTATION:
      case SHORT_CIRCUIT_STRUCTURING:
#endif
#if HAVE_BAMBU_BUILT
      case SHORT_CIRCUIT_TAF:
      case SIMPLE_CODE_MOTION:
      case SOFT_FLOAT_CG_EXT:
#endif
#if HAVE_BAMBU_BUILT && HAVE_EXPERIMENTAL
      case SPECULATION_EDGES_COMPUTATION:
#endif
      case SPLIT_PHINODES:
      case SSA_DATA_FLOW_ANALYSIS:
      case SWITCH_FIX:
#if HAVE_RTL_BUILT && HAVE_ZEBU_BUILT
      case UPDATE_RTL_WEIGHT:
#endif
#if HAVE_ZEBU_BUILT
      case UPDATE_TREE_WEIGHT:
#endif
#if HAVE_BAMBU_BUILT
      case UNROLLING_DEGREE:
#endif
      case USE_COUNTING:
      case VAR_ANALYSIS:
      case VAR_DECL_FIX:
#if HAVE_BAMBU_BUILT
      case VERIFICATION_OPERATION:
      case VIRTUAL_PHI_NODES_SPLIT:
#endif
         {
            return DesignFlowStepRef(new SymbolicApplicationFrontendFlowStep(AppM, frontend_flow_step_type, design_flow_manager.lock(), parameters));
            break;
         }
      case(CREATE_TREE_MANAGER) :
#if HAVE_HOST_PROFILING_BUILT
      case(DUMP_PROFILING_DATA) :
#endif
      case(FUNCTION_ANALYSIS) :
#if HAVE_HOST_PROFILING_BUILT
      case(HOST_PROFILING):
      case(HPP_PROFILING):
      case(LOOPS_PROFILING) :
#endif
#if HAVE_ZEBU_BUILT
      case POINTED_DATA_EVALUATION:
#endif
#if HAVE_FROM_PRAGMA_BUILT
      case(PRAGMA_SUBSTITUTION):
#endif
#if HAVE_HOST_PROFILING_BUILT
      case(READ_PROFILING_DATA) :
#endif
      case(SYMBOLIC_APPLICATION_FRONTEND_FLOW_STEP) :
#if HAVE_ZEBU_BUILT
      case(SIZEOF_SUBSTITUTION) :
#endif
#if HAVE_HOST_PROFILING_BUILT
      case(TP_PROFILING) :
#endif
         {
            return CreateApplicationFrontendFlowStep(frontend_flow_step_type);
         }
      default:
         THROW_UNREACHABLE("Frontend flow step type does not exist");

   }
   return DesignFlowStepRef();
}

const DesignFlowStepRef FrontendFlowStepFactory::CreateApplicationFrontendFlowStep(const FrontendFlowStepType design_flow_step_type) const
{
   switch(design_flow_step_type)
   {
      case(CREATE_TREE_MANAGER) :
      {
         return DesignFlowStepRef(new create_tree_manager(parameters, AppM, design_flow_manager.lock()));
      }
#if HAVE_HOST_PROFILING_BUILT
      case(DUMP_PROFILING_DATA):
      {
         return DesignFlowStepRef(new DumpProfilingData(AppM, design_flow_manager.lock(), parameters));
      }
#endif
      case(FUNCTION_ANALYSIS) :
      {
         return DesignFlowStepRef(new call_graph_computation(parameters, AppM, design_flow_manager.lock()));
      }
#if HAVE_HOST_PROFILING_BUILT
      case(HOST_PROFILING) :
      {
         return DesignFlowStepRef(new HostProfiling(AppM, design_flow_manager.lock(), parameters));
      }
      case(HPP_PROFILING):
      {
         return DesignFlowStepRef(new hpp_profiling(parameters, AppM, design_flow_manager.lock()));
      }
      case(LOOPS_PROFILING) :
      {
         return DesignFlowStepRef(new LoopsProfiling(parameters, AppM, design_flow_manager.lock()));
      }
#endif
#if HAVE_ZEBU_BUILT
      case(POINTED_DATA_EVALUATION):
      {
         return DesignFlowStepRef(new PointedDataEvaluation(AppM, design_flow_manager.lock(), parameters));
      }
#endif
#if HAVE_FROM_PRAGMA_BUILT
      case(PRAGMA_SUBSTITUTION):
      {
         return DesignFlowStepRef(new PragmaSubstitution(AppM, design_flow_manager.lock(), parameters));
      }
#endif
#if HAVE_HOST_PROFILING_BUILT
      case(READ_PROFILING_DATA) :
      {
         return DesignFlowStepRef(new read_profiling_data(parameters, AppM, design_flow_manager.lock()));
      }
#endif
#if HAVE_ZEBU_BUILT
      case(SIZEOF_SUBSTITUTION) :
      {
         return DesignFlowStepRef(new SizeofSubstitution(AppM, design_flow_manager.lock(), parameters));
      }
#endif
#if HAVE_HOST_PROFILING_BUILT
      case(TP_PROFILING) :
      {
          return DesignFlowStepRef(new tp_profiling(parameters, AppM, design_flow_manager.lock()));
      }
#endif
      case ADD_BB_ECFG_EDGES:
#if HAVE_ZEBU_BUILT
      case ADD_OP_ECFG_EDGES:
#endif
      case ADD_OP_FLOW_EDGES:
#if HAVE_ZEBU_BUILT || HAVE_BAMBU_BUILT
      case(ARRAY_REF_FIX):
#endif
      case BASIC_BLOCKS_CFG_COMPUTATION:
      case BB_FEEDBACK_EDGES_IDENTIFICATION:
      case BLOCK_FIX:
#if HAVE_ZEBU_BUILT
      case CALL_ARGS_STRUCTURING:
#endif
      case CHECK_SYSTEM_TYPE:
      case CONTROL_DEPENDENCE_COMPUTATION:
#if HAVE_ZEBU_BUILT
      case DEAD_CODE_ELIMINATION:
#endif
#if HAVE_BAMBU_BUILT
      case DETERMINE_MEMORY_ACCESSES:
#endif
      case DOM_POST_DOM_COMPUTATION:
#if HAVE_EXPERIMENTAL
      case EXTENDED_PDG_COMPUTATION:
#endif
#if HAVE_ZEBU_BUILT
      case GLOBAL_VARIABLES_ANALYSIS:
#endif
#if HAVE_ZEBU_BUILT
      case HEADER_STRUCTURING:
      case INSTRUCTION_SEQUENCES_COMPUTATION:
#endif
#if HAVE_BAMBU_BUILT
      case IR_LOWERING:
#endif
      case LOOP_COMPUTATION:
      case LOOP_REGIONS_COMPUTATION:
      case LOOP_REGIONS_FLOW_COMPUTATION:
#if HAVE_ZEBU_BUILT
      case LOOPS_ANALYSIS :
#endif
      case LOOPS_IDENTIFICATION:
#if HAVE_BAMBU_BUILT
      case MULTI_WAY_IF:
      case NI_SSA_LIVENESS:
#endif
      case OP_FEEDBACK_EDGES_IDENTIFICATION:
      case OPERATIONS_CFG_COMPUTATION:
      case ORDER_COMPUTATION:
#if HAVE_EXPERIMENTAL && HAVE_ZEBU_BUILT
      case PARALLEL_LOOP_SWAP:
      case PARALLEL_LOOPS_ANALYSIS:
#endif
#if HAVE_EXPERIMENTAL
      case PARALLEL_REGIONS_GRAPH_COMPUTATION:
#endif
#if HAVE_BAMBU_BUILT
      case PHI_OPT:
#endif
#if HAVE_ZEBU_BUILT
      case POINTED_DATA_COMPUTATION:
#endif
#if HAVE_PRAGMA_BUILT
      case PRAGMA_ANALYSIS:
#endif
#if HAVE_ZEBU_BUILT
      case PREDICTABILITY_ANALYSIS:
      case PROBABILITY_PATH:
#endif
      case REACHABILITY_COMPUTATION:
#if HAVE_EXPERIMENTAL
      case REDUCED_PDG_COMPUTATION:
#endif
#if HAVE_ZEBU_BUILT
#if HAVE_EXPERIMENTAL
      case REFINED_DATA_FLOW_ANALYSIS:
      case REFINED_VAR_COMPUTATION:
#endif
#endif
#if HAVE_BAMBU_BUILT
      case REMOVE_CLOBBER_GA:
#endif
#if HAVE_ZEBU_BUILT
      case REVERSE_RESTRICT_COMPUTATION:
      case SHORT_CIRCUIT_STRUCTURING:
#endif
#if HAVE_BAMBU_BUILT
      case SHORT_CIRCUIT_TAF:
      case SIMPLE_CODE_MOTION:
      case SOFT_FLOAT_CG_EXT:
#endif
#if HAVE_BAMBU_BUILT && HAVE_EXPERIMENTAL
      case SPECULATION_EDGES_COMPUTATION:
#endif
      case SPLIT_PHINODES:
      case SSA_DATA_FLOW_ANALYSIS:
      case SWITCH_FIX:
#if HAVE_BAMBU_BUILT
      case UNROLLING_DEGREE:
#endif
#if HAVE_RTL_BUILT && HAVE_ZEBU_BUILT
      case UPDATE_RTL_WEIGHT:
#endif
#if HAVE_ZEBU_BUILT
      case UPDATE_TREE_WEIGHT:
#endif
      case USE_COUNTING:
      case VAR_ANALYSIS:
      case VAR_DECL_FIX:
#if HAVE_BAMBU_BUILT
      case VERIFICATION_OPERATION:
      case VIRTUAL_PHI_NODES_SPLIT:
#endif
      {
         THROW_UNREACHABLE("Trying to create an application flow step from " + FrontendFlowStep::EnumToKindText(design_flow_step_type));
         break;
      }
      case SYMBOLIC_APPLICATION_FRONTEND_FLOW_STEP:
      {
         THROW_UNREACHABLE("Symbolic Application Frontend Flow Step must be created by GenerateFrontendSteps");
      }
      default:
         THROW_UNREACHABLE("Frontend flow step type does not exist");
   }
   return DesignFlowStepRef();
}

const DesignFlowStepRef FrontendFlowStepFactory::CreateFunctionFrontendFlowStep(const FrontendFlowStepType design_flow_step_type, const unsigned int function_id) const
{
   switch(design_flow_step_type)
   {
      case ADD_BB_ECFG_EDGES:
      {
         return DesignFlowStepRef(new AddBbEcfgEdges(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#if HAVE_ZEBU_BUILT
      case ADD_OP_ECFG_EDGES:
      {
         return DesignFlowStepRef(new AddOpEcfgEdges(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#endif
      case ADD_OP_FLOW_EDGES:
      {
         return DesignFlowStepRef(new add_op_flow_edges(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#if HAVE_ZEBU_BUILT || HAVE_BAMBU_BUILT
      case(ARRAY_REF_FIX):
      {
         return DesignFlowStepRef(new array_ref_fix(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
      case BASIC_BLOCKS_CFG_COMPUTATION:
      {
         return DesignFlowStepRef(new BasicBlocksCfgComputation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case BB_FEEDBACK_EDGES_IDENTIFICATION:
      {
         return DesignFlowStepRef(new bb_feedback_edges_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case BLOCK_FIX:
      {
         return DesignFlowStepRef(new BlockFix(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#if HAVE_ZEBU_BUILT
      case CALL_ARGS_STRUCTURING:
      {
         return DesignFlowStepRef(new CallArgsStructuring(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#endif
      case CHECK_SYSTEM_TYPE:
      {
         return DesignFlowStepRef(new CheckSystemType(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case CONTROL_DEPENDENCE_COMPUTATION:
      {
         return DesignFlowStepRef(new cdg_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#if HAVE_ZEBU_BUILT
      case DEAD_CODE_ELIMINATION:
      {
         return DesignFlowStepRef(new dead_code_elimination(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
#if HAVE_BAMBU_BUILT
      case DETERMINE_MEMORY_ACCESSES:
      {
         return DesignFlowStepRef(new determine_memory_accesses(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
      case DOM_POST_DOM_COMPUTATION:
      {
         return DesignFlowStepRef(new dom_post_dom_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#if HAVE_EXPERIMENTAL
      case EXTENDED_PDG_COMPUTATION:
      {
         return DesignFlowStepRef(new extended_pdg_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
#if HAVE_ZEBU_BUILT
      case GLOBAL_VARIABLES_ANALYSIS:
      {
         return DesignFlowStepRef(new GlobalVariablesAnalysis(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
#if HAVE_ZEBU_BUILT
      case HEADER_STRUCTURING:
      {
         return DesignFlowStepRef(new header_structuring(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case INSTRUCTION_SEQUENCES_COMPUTATION:
      {
         return DesignFlowStepRef(new InstructionSequencesComputation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
#if HAVE_BAMBU_BUILT
      case IR_LOWERING:
      {
         return DesignFlowStepRef(new IR_lowering(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
      case LOOP_COMPUTATION:
      {
         return DesignFlowStepRef(new loops_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case LOOP_REGIONS_COMPUTATION:
      {
         return DesignFlowStepRef(new loop_regions_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case LOOP_REGIONS_FLOW_COMPUTATION:
      {
         return DesignFlowStepRef(new loop_regions_flow_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#if HAVE_ZEBU_BUILT
      case LOOPS_ANALYSIS :
      {
         return DesignFlowStepRef(new loops_analysis(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
      case LOOPS_IDENTIFICATION:
      {
         return DesignFlowStepRef(new loops_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#if HAVE_BAMBU_BUILT
      case MULTI_WAY_IF:
      {
         return DesignFlowStepRef(new multi_way_if(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case NI_SSA_LIVENESS:
      {
         return DesignFlowStepRef(new NI_SSA_liveness(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
      case OP_FEEDBACK_EDGES_IDENTIFICATION:
      {
         return DesignFlowStepRef(new op_feedback_edges_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case OPERATIONS_CFG_COMPUTATION:
      {
         return DesignFlowStepRef(new operations_cfg_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case ORDER_COMPUTATION:
      {
         return DesignFlowStepRef(new order_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#if HAVE_EXPERIMENTAL && HAVE_ZEBU_BUILT
      case PARALLEL_LOOP_SWAP:
      {
         return DesignFlowStepRef(new parallel_loop_swap(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case PARALLEL_LOOPS_ANALYSIS:
      {
         return DesignFlowStepRef(new ParallelLoopsAnalysis(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#endif
#if HAVE_EXPERIMENTAL
      case PARALLEL_REGIONS_GRAPH_COMPUTATION:
      {
         return DesignFlowStepRef(new ParallelRegionsGraphComputation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
#if HAVE_BAMBU_BUILT
      case PHI_OPT:
      {
         return DesignFlowStepRef(new phi_opt(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#endif
#if HAVE_ZEBU_BUILT
      case POINTED_DATA_COMPUTATION:
      {
         return DesignFlowStepRef(new PointedDataComputation(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#endif
#if HAVE_FROM_PRAGMA_BUILT
      case PRAGMA_ANALYSIS :
      {
         return DesignFlowStepRef(new pragma_analysis(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#endif
#if HAVE_ZEBU_BUILT
      case PREDICTABILITY_ANALYSIS:
      {
         return DesignFlowStepRef(new predictability_analysis(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
#if HAVE_HOST_PROFILING_BUILT
      case PROBABILITY_PATH:
      {
         return DesignFlowStepRef(new probability_path(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
      case REACHABILITY_COMPUTATION:
      {
         return DesignFlowStepRef(new reachability_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#if HAVE_EXPERIMENTAL
      case REDUCED_PDG_COMPUTATION:
      {
         return DesignFlowStepRef(new reduced_pdg_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
#if HAVE_ZEBU_BUILT
#if HAVE_EXPERIMENTAL
      case REFINED_DATA_FLOW_ANALYSIS:
      {
         return DesignFlowStepRef(new RefinedDataFlowAnalysis(AppM, function_id, design_flow_manager.lock(), parameters));
      }
      case REFINED_VAR_COMPUTATION:
      {
         return DesignFlowStepRef(new RefinedVarComputation(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#endif
#endif
#if HAVE_BAMBU_BUILT
      case REMOVE_CLOBBER_GA:
      {
         return DesignFlowStepRef(new remove_clobber_ga(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#endif
#if HAVE_ZEBU_BUILT
      case REVERSE_RESTRICT_COMPUTATION:
      {
         return DesignFlowStepRef(new ReverseRestrictComputation(AppM, function_id, design_flow_manager.lock(), parameters));
      }
      case SHORT_CIRCUIT_STRUCTURING:
      {
         return DesignFlowStepRef(new short_circuit_structuring(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
#if HAVE_BAMBU_BUILT
      case SHORT_CIRCUIT_TAF:
      {
         return DesignFlowStepRef(new short_circuit_taf(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case SIMPLE_CODE_MOTION:
      {
         return DesignFlowStepRef(new simple_code_motion(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case SOFT_FLOAT_CG_EXT:
      {
         return DesignFlowStepRef(new soft_float_cg_ext(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
#if HAVE_BAMBU_BUILT && HAVE_EXPERIMENTAL
      case SPECULATION_EDGES_COMPUTATION:
      {
         return DesignFlowStepRef(new speculation_edges_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
#endif
      case SPLIT_PHINODES:
      {
         return DesignFlowStepRef(new split_phi_nodes(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case SSA_DATA_FLOW_ANALYSIS:
      {
         return DesignFlowStepRef(new ssa_data_dependence_computation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case SWITCH_FIX:
      {
         return DesignFlowStepRef(new SwitchFix(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#if HAVE_BAMBU_BUILT
      case UNROLLING_DEGREE:
      {
         THROW_UNREACHABLE("Not updated step");
      }
#endif
#if HAVE_RTL_BUILT && HAVE_ZEBU_BUILT
      case UPDATE_RTL_WEIGHT:
      {
         return DesignFlowStepRef(new update_rtl_weight(parameters, AppM, arch_manager, function_id, design_flow_manager.lock()));
      }
#endif
#if HAVE_ZEBU_BUILT
      case UPDATE_TREE_WEIGHT:
      {
         return DesignFlowStepRef(new update_tree_weight(parameters, AppM, arch_manager, function_id, design_flow_manager.lock()));
      }
#endif
      case USE_COUNTING:
      {
         return DesignFlowStepRef(new use_counting(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case VAR_ANALYSIS:
      {
         return DesignFlowStepRef(new VarComputation(parameters, AppM, function_id, design_flow_manager.lock()));
      }
      case VAR_DECL_FIX:
      {
         return DesignFlowStepRef(new VarDeclFix(AppM, function_id, design_flow_manager.lock(), parameters));
      }
#if HAVE_BAMBU_BUILT
      case VERIFICATION_OPERATION:
      {
         THROW_UNREACHABLE("Not updated step");
      }
      case VIRTUAL_PHI_NODES_SPLIT:
      {
         return DesignFlowStepRef(new virtual_phi_nodes_split(parameters, AppM, function_id, design_flow_manager.lock()));

      }
#endif
      case(CREATE_TREE_MANAGER) :
#if HAVE_HOST_PROFILING_BUILT
      case(DUMP_PROFILING_DATA) :
#endif
      case(FUNCTION_ANALYSIS) :
#if HAVE_HOST_PROFILING_BUILT
      case(HOST_PROFILING):
      case(HPP_PROFILING):
      case(LOOPS_PROFILING) :
#endif
#if HAVE_ZEBU_BUILT
      case(POINTED_DATA_EVALUATION):
#endif
#if HAVE_FROM_PRAGMA_BUILT
      case(PRAGMA_SUBSTITUTION):
#endif
#if HAVE_HOST_PROFILING_BUILT
      case(READ_PROFILING_DATA) :
#endif
#if HAVE_ZEBU_BUILT
      case(SIZEOF_SUBSTITUTION):
#endif
      case(SYMBOLIC_APPLICATION_FRONTEND_FLOW_STEP) :
#if HAVE_HOST_PROFILING_BUILT
      case(TP_PROFILING) :
#endif
      {
         THROW_UNREACHABLE("Trying to create a function frontend flow step from " + FrontendFlowStep::EnumToKindText(design_flow_step_type));
         break;
      }
      default:
         THROW_UNREACHABLE("Frontend flow step type does not exist");
   }
   return DesignFlowStepRef();
}

const std::string FrontendFlowStepFactory::GetPrefix() const
{
   return "Frontend";
}
