/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file loop_regions_flow_computation.cpp
 * @brief Analysis step performing edge flow computation for loop regions.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#include "loop_regions_flow_computation.hpp"
#include "application_manager.hpp"
#include "graph.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"
#include "basic_block.hpp"
#include "loops.hpp"
#include "loop.hpp"
#include "op_graph.hpp"
#include "operations_graph_constructor.hpp"
#include "tree_basic_block.hpp"
#include "Parameter.hpp"

loop_regions_flow_computation::loop_regions_flow_computation(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, LOOP_REGIONS_FLOW_COMPUTATION, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

loop_regions_flow_computation::~loop_regions_flow_computation()
{}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > loop_regions_flow_computation::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(CONTROL_DEPENDENCE_COMPUTATION, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(LOOP_REGIONS_COMPUTATION, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(OP_FEEDBACK_EDGES_IDENTIFICATION, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(POST_PRECEDENCE_RELATIONSHIP) :
      case(PRECEDENCE_RELATIONSHIP):
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void loop_regions_flow_computation::Exec()
{
   const BBGraphRef fbb = function_behavior->GetBBGraph(FunctionBehavior::FBB);
   const BBGraphRef dom = function_behavior->GetBBGraph(FunctionBehavior::CDG_BB);
   const BehavioralHelperConstRef  helper = function_behavior->CGetBehavioralHelper();

   ///first consider the label
   std::list<LoopConstRef> loops = function_behavior->CGetLoops()->GetList();
   std::list<LoopConstRef>::const_iterator loop_end = loops.end();
   for(std::list<LoopConstRef>::const_iterator loop = loops.begin(); loop != loop_end; loop++)
   {
      if ((*loop)->IsReducible())
      {
         if((*loop)->GetId() != 0 and (((*loop)->loop_type & WHILE_LOOP) == 0 or ((*loop)->loop_type & SINGLE_EXIT_LOOP) == 0))
         {
            vertex header = (*loop)->GetHeader();
            const BBNodeInfoConstRef bb_node_info = fbb->CGetBBNodeInfo(header);
            //THROW_ASSERT((*loop)->num_blocks() != 1 || helper->end_with_a_cond_or_goto(bb_node_info->block), "malformed loop region "+boost::lexical_cast<std::string>(bb_node_info->block->number));

            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Examing loop " + boost::lexical_cast<std::string>(bb_node_info->block->number));
            InEdgeIterator ei, ei_end;
            boost::tie(ei, ei_end) = boost::in_edges(header, *dom);
            THROW_ASSERT(boost::in_degree(header, *dom) >= 1, "Header of loop " + boost::lexical_cast<std::string>(bb_node_info->block->number) + " hasn't controller");

            vertex header_dom = boost::source(*ei, *dom);
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Controller of header is BB" + boost::lexical_cast<std::string>(dom->CGetBBNodeInfo(header_dom)->block->number));
            if(helper->start_with_a_label(bb_node_info->block))
            {
               std::list<vertex>::const_iterator vcur_it_end = bb_node_info->statements_list.end();
               std::list<vertex>::const_iterator vcur_it = bb_node_info->statements_list.begin();
               if(vcur_it_end != vcur_it)
               {
                  vertex label_vertex = *vcur_it;
                  vcur_it++;
                  for(; vcur_it != vcur_it_end; vcur_it++)
                  {
                     function_behavior->ogc->AddEdge(label_vertex, *vcur_it, FLG_SELECTOR);
                  }
                  OutEdgeIterator eo, eo_end;
                  for(boost::tie(eo, eo_end) = boost::out_edges(header_dom, *dom); eo != eo_end; eo++)
                  {
                     PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "  Controller controlls BB" + boost::lexical_cast<std::string>(dom->CGetBBNodeInfo(boost::target(*eo, *dom))->block->number));
                     if(dom->CGetBBNodeInfo(boost::target(*eo, *dom))->loop_id == bb_node_info->block->number and dom->CGetBBNodeInfo(boost::target(*eo, *dom))->block->number != bb_node_info->block->number)
                     {
                        const BBNodeInfoConstRef target_node_info = fbb->CGetBBNodeInfo(boost::target(*eo, *dom));
                        std::list<vertex>::const_iterator vcur_iit_end = target_node_info->statements_list.end();
                        std::list<vertex>::const_iterator vcur_iit;
                        for(vcur_iit = target_node_info->statements_list.begin(); vcur_iit != vcur_iit_end; vcur_iit++)
                        {
                           function_behavior->ogc->AddEdge(label_vertex, *vcur_iit, FLG_SELECTOR);
                        }
                     }
                  }
               }
               else
                  THROW_ERROR("something of wrong happened");
            }
            else
               THROW_ERROR("something of wrong happened");
         }
      }
      else
      {
         THROW_ERROR_CODE(IRREDUCIBLE_LOOPS_EC,"Irreducible loops not yet supported");
      }
   }
   ///and then the gotos
   for(std::list<LoopConstRef>::const_iterator loop = loops.begin(); loop != loop_end; loop++)
   {
      if ((*loop)->IsReducible())
      {
         if((*loop)->GetId() != 0 and (((*loop)->loop_type & WHILE_LOOP) == 0 or ((*loop)->loop_type & SINGLE_EXIT_LOOP) == 0))
         {
            vertex bb_header = (*loop)->GetHeader();
#ifndef NDEBUG
            const BBNodeInfoConstRef bb_node_info = fbb->CGetBBNodeInfo(bb_header);
            unsigned int label_expr_nid = helper->start_with_a_label(bb_node_info->block);
            THROW_ASSERT(label_expr_nid, "gimple_label has to be inserted into the intermediate representation");
#endif
            InEdgeIterator ei, ei_end;
            const std::unordered_set<vertex> blocks = (*loop)->get_blocks();
            std::unordered_set<vertex>::const_iterator bb_it = blocks.begin(), bb_it_end = blocks.end();
            for (boost::tie(ei, ei_end) = boost::in_edges(bb_header, *fbb); ei != ei_end; ei++)
            {
               vertex from_bb = boost::source(*ei, *fbb);
               if(std::find(bb_it, bb_it_end, from_bb) != bb_it_end)
               {
                  const BBNodeInfoConstRef bb_node_info_from = fbb->CGetBBNodeInfo(from_bb);
                  if(helper->end_with_a_cond_or_goto(bb_node_info_from->block))
                  {
                     vertex goto_vertex = bb_node_info_from->statements_list.back();
   #if 1
                     std::unordered_set<vertex>::const_iterator bb_it_local = bb_it;
                     while(bb_it_local != bb_it_end)
                     {
                        const std::list<vertex> &sl = fbb->CGetBBNodeInfo(*bb_it_local)->statements_list;

                        std::list<vertex>::const_iterator vcur_it_end = sl.end();
                        for(std::list<vertex>::const_iterator vcur_it = sl.begin(); vcur_it != vcur_it_end; vcur_it++)
                           if(*vcur_it != goto_vertex)
                              function_behavior->ogc->AddEdge(*vcur_it, goto_vertex, FLG_SELECTOR);
                        bb_it_local++;
                     }
   #else
                     const std::list<vertex> &sl = bi_from->statements_list;
                     std::list<vertex>::const_iterator vcur_it_end = sl.end();
                     std::list<vertex>::const_iterator vcur_it = sl.begin();
                     for(std::list<vertex>::const_iterator vcur_it = sl.begin(); vcur_it != vcur_it_end; vcur_it++)
                        if(*vcur_it != goto_vertex)
                           function_behavior->ogc->AddEdge(*vcur_it, goto_vertex, FLG_SELECTOR);
   #endif
                  }
                  else
                     THROW_ERROR("something of wrong happened");
               }
            }
         }
      }
      else
      {
         THROW_ERROR_CODE(IRREDUCIBLE_LOOPS_EC,"Irreducible loops not yet supported");
      }
   }
   if(parameters->getOption<bool>(OPT_print_dot))
   {
      function_behavior->CGetOpGraph(FunctionBehavior::FLG)->WriteDot("OP_FLG.dot");
   }
}
