/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file bb_feedback_edges_computation.cpp
 * @brief Analysis step computing feedback edges of basic block control flow graph
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#include "bb_feedback_edges_computation.hpp"

#include "application_manager.hpp"
#include "function_behavior.hpp"
#include "behavioral_helper.hpp"
#include "loops.hpp"
#include "loop.hpp"
#include "basic_block.hpp"
#include "operations_graph_constructor.hpp"
#include "basic_blocks_graph_constructor.hpp"

#include "Parameter.hpp"

bb_feedback_edges_computation::bb_feedback_edges_computation(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
    FunctionFrontendFlowStep(_AppM, _function_id, BB_FEEDBACK_EDGES_IDENTIFICATION, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}


bb_feedback_edges_computation::~bb_feedback_edges_computation()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > bb_feedback_edges_computation::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(LOOPS_IDENTIFICATION, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(POST_PRECEDENCE_RELATIONSHIP) :
      case(PRECEDENCE_RELATIONSHIP):
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void bb_feedback_edges_computation::Exec()
{
   const BBGraphRef  fbb = function_behavior->GetBBGraph(FunctionBehavior::FBB);
   const BehavioralHelperConstRef  helper = function_behavior->CGetBehavioralHelper();
   PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, helper->get_function_name() + " Starting feedback edge computation");
    ///then consider loops
   std::list<LoopConstRef> loops = function_behavior->CGetLoops()->GetList();
   std::list<LoopConstRef>::const_iterator loop_end = loops.end();
   for(std::list<LoopConstRef>::const_iterator loop = loops.begin(); loop != loop_end; loop++)
   {
      if((*loop)->GetId() == 0)
         continue;

      if ((*loop)->IsReducible())
      {
         vertex bb_header = (*loop)->GetHeader();
         InEdgeIterator ei, ei_end;
         std::unordered_set<vertex> blocks;
         (*loop)->get_recursively_bb(blocks);
         for (boost::tie(ei, ei_end) = boost::in_edges(bb_header, *fbb); ei != ei_end; ei++)
         {
            vertex from_bb = boost::source(*ei, *fbb);
            if(std::find(blocks.begin(), blocks.end(), from_bb) != blocks.end())
            {
               function_behavior->bbgc->RemoveEdge(from_bb, bb_header, CFG_SELECTOR);
               function_behavior->bbgc->AddEdge(from_bb, bb_header, FB_CFG_SELECTOR);
            }
         }
      }
      else
      {
         THROW_ERROR_CODE(IRREDUCIBLE_LOOPS_EC,"Irreducible loops not yet supported");
      }
   }
   if(parameters->getOption<bool>(OPT_print_dot))
   {
      function_behavior->GetBBGraph(FunctionBehavior::FBB)->WriteDot("BB_FCFG.dot");
   }
}
