/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEI
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file add_bb_ecfg_edges.cpp
 * @brief Analysis step which extends basic blocks cfg
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Header include
#include "add_bb_ecfg_edges.hpp"

///Behavior include
#include "basic_block.hpp"
#include "basic_blocks_graph_constructor.hpp"
#include "behavioral_helper.hpp"
#include "application_manager.hpp"
#include "function_behavior.hpp"
#include "loop.hpp"
#include "loops.hpp"
#include "tree_basic_block.hpp"

///Graph include
#include "graph.hpp"

///Parameter include
#include "Parameter.hpp"

///STL include
#include <list>
#include <unordered_set>

///Utility include
#include "boost/lexical_cast.hpp"
#include "dbgPrintHelper.hpp"
#include "exceptions.hpp"
#include "utility.hpp"

AddBbEcfgEdges::AddBbEcfgEdges(const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager, const ParameterConstRef _parameters) :
   FunctionFrontendFlowStep(_AppM, _function_id, ADD_BB_ECFG_EDGES, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

AddBbEcfgEdges::~AddBbEcfgEdges()
{}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > AddBbEcfgEdges::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BB_FEEDBACK_EDGES_IDENTIFICATION, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(ORDER_COMPUTATION, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(POST_PRECEDENCE_RELATIONSHIP) :
      case(PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void AddBbEcfgEdges::Exec()
{
   ///The behavioral helper
   const BehavioralHelperConstRef behavioral_helper = function_behavior->CGetBehavioralHelper();

   ///The function name
   const std::string function_name = behavioral_helper->get_function_name();

   ///The ordering among basic block in control flow graph
   const std::map<vertex, unsigned int> & bb_map_levels = function_behavior->get_bb_map_levels();

   ///The control flow graph with feedback of basic blocks
   const BBGraphRef  fbb = function_behavior->GetBBGraph(FunctionBehavior::FBB);

   ///The loop structure
   const std::list<LoopConstRef> & loops = function_behavior->CGetLoops()->GetList();

   ///Adding edges in basic block graphs from sources of feedback edges to landing pads
   std::list<LoopConstRef>::const_iterator loop, loop_end = loops.end();
   for(loop = loops.begin(); loop != loop_end; loop++)
   {
      if((*loop)->GetId() == 0)
         continue;
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Considering loop " + boost::lexical_cast<std::string>((*loop)->GetId()));

      ///Sources of feedback loop
      std::unordered_set<vertex> sources;

      ///The targets of the flow edges they can be different from landing_pads of this loop if the edge which connects a block
      ///of the loop to a landing_pads is the feedback edge of an external loop. In this case the block must be connected to the
      ///landing pads of the external loop
      std::unordered_set<vertex> targets;

      ///Header of the loop
      const vertex header = (*loop)->GetHeader();

      InEdgeIterator ei, ei_end;
      for(boost::tie(ei, ei_end) = boost::in_edges(header, *fbb); ei != ei_end; ei++)
      {
         if((FB_CFG_SELECTOR) & fbb->GetSelector(*ei))
         {
            sources.insert(boost::source(*ei, *fbb));
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Found source BB" + boost::lexical_cast<std::string>(fbb->CGetBBNodeInfo(boost::source(*ei, *fbb))->block->number));
         }
      }
      ///Landing pads
      std::unordered_set<vertex> landing_pads = (*loop)->GetLandingPadBlocks();
      LoopConstRef other_loop = *loop;
      ///While at least one landing pad of the current loop or of one of its ancestor is before the header of the current loop
      while([&] () -> bool {
            const std::unordered_set<vertex>::const_iterator landing_pad_end = landing_pads.end();
            for(std::unordered_set<vertex>::const_iterator landing_pad = landing_pads.begin(); landing_pad != landing_pad_end; landing_pad++)
            {
               if(bb_map_levels.find(*landing_pad)->second < bb_map_levels.find(header)->second)
                  return true;
            }
            return false;
         }()
      )
      {
         other_loop = other_loop->Parent();
         landing_pads = other_loop->GetLandingPadBlocks();
      }
      targets.insert(landing_pads.begin(), landing_pads.end());

      std::unordered_set<vertex>::const_iterator s, s_end = sources.end(), t, t_end = targets.end();
      for(s = sources.begin(); s != s_end; s++)
      {
         for(t = targets.begin(); t != t_end; t++)
         {
            INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "---Adding edge from BB" + boost::lexical_cast<std::string>(fbb->CGetBBNodeInfo(*s)->block->number) + " to BB" + boost::lexical_cast<std::string>(fbb->CGetBBNodeInfo(*t)->block->number));
            function_behavior->bbgc->AddEdge(*s, *t, ECFG_SELECTOR);
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Considered loop " + boost::lexical_cast<std::string>((*loop)->GetId()));
   }

   if(parameters->getOption<bool>(OPT_print_dot))
   {
      function_behavior->GetBBGraph(FunctionBehavior::EBB)->WriteDot("BB_EBB.dot");
   }

#ifndef NDEBUG
   try
   {
      const BBGraphRef ebb_graph = function_behavior->GetBBGraph(FunctionBehavior::EBB);
      std::list<vertex> vertices;
      ebb_graph->TopologicalSort(vertices);
   }
   catch (const char* msg)
   {
      THROW_UNREACHABLE("ecfg graph of function " + function_name + " is not acyclic");
   }
   catch (const std::string & msg)
   {
      THROW_UNREACHABLE("ecfg graph of function " + function_name + " is not acyclic");
   }
   catch (const std::exception& ex)
   {
      THROW_UNREACHABLE("ecfg graph of function " + function_name + " is not acyclic");
   }
   catch ( ... )
   {
      THROW_UNREACHABLE("ecfg graph of function " + function_name + " is not acyclic");
   }
#endif
}
