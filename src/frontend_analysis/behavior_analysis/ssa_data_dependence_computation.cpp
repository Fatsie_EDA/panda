/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file ssa_data_dependence_computation.cpp
 * @brief Analysis step performing data flow analysis based on ssa variables
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */

///Header include 
#include "ssa_data_dependence_computation.hpp"

///Behavior include
#include "application_manager.hpp"
#include "basic_block.hpp"
#include "behavioral_helper.hpp"
#include "function_behavior.hpp"
#include "op_graph.hpp"
#include "operations_graph_constructor.hpp"
#include "loop.hpp"
#include "loops.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <iostream>

ssa_data_dependence_computation::ssa_data_dependence_computation(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, SSA_DATA_FLOW_ANALYSIS, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

ssa_data_dependence_computation::~ssa_data_dependence_computation()
{}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > ssa_data_dependence_computation::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(REACHABILITY_COMPUTATION, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(VAR_ANALYSIS, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(POST_PRECEDENCE_RELATIONSHIP) :
      case(PRECEDENCE_RELATIONSHIP):
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void ssa_data_dependence_computation::Exec()
{
   const BehavioralHelperConstRef behavioral_helper = function_behavior->CGetBehavioralHelper();
   const OpGraphConstRef cfg = function_behavior->CGetOpGraph(FunctionBehavior::CFG);
   const std::string function_name = behavioral_helper->get_function_name();

   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Computing defs and uses");
   //Maps between a variable and its definitions
   std::map<unsigned int, std::set<vertex> > scalar_defs, virtual_DATADEPS_defs, virtual_ANTIDEPS_defs;
   VertexIterator vi, vi_end;
   for (boost::tie(vi, vi_end) = boost::vertices(*cfg); vi != vi_end; vi++)
   {
      const CustomSet<unsigned int> & local_scalar_defs = cfg->CGetOpNodeInfo(*vi)->GetScalarVariables(AT_DEF);
      CustomSet<unsigned int>::const_iterator scalar_def, scalar_def_end = local_scalar_defs.end();
      for(scalar_def = local_scalar_defs.begin(); scalar_def != scalar_def_end; scalar_def++)
      {
         scalar_defs[*scalar_def].insert(*vi);
      }
      const CustomSet<unsigned int> & local_virtual_DATADEPS_defs = cfg->CGetOpNodeInfo(*vi)->GetVirtualVariables_DATADEPS(AT_DEF);
      CustomSet<unsigned int>::const_iterator virtual_def, virtual_def_end = local_virtual_DATADEPS_defs.end();
      for(virtual_def = local_virtual_DATADEPS_defs.begin(); virtual_def != virtual_def_end; virtual_def++)
      {
         virtual_DATADEPS_defs[*virtual_def].insert(*vi);
      }
      const CustomSet<unsigned int> & local_virtual_ANTIDEPS_defs = cfg->CGetOpNodeInfo(*vi)->GetVirtualVariables_ANTIDEPS(AT_DEF);
      CustomSet<unsigned int>::const_iterator virtual_ANTIDEPS_def, virtual_ANTIDEPS_def_end = local_virtual_ANTIDEPS_defs.end();
      for(virtual_ANTIDEPS_def = local_virtual_ANTIDEPS_defs.begin(); virtual_ANTIDEPS_def != virtual_ANTIDEPS_def_end; virtual_ANTIDEPS_def++)
      {
         virtual_ANTIDEPS_defs[*virtual_ANTIDEPS_def].insert(*vi);
      }
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Computed defs and uses");
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Computing dependencies");
   for (boost::tie(vi, vi_end) = boost::vertices(*cfg); vi != vi_end; vi++)
   {
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Computing scalar anti and data dependences of vertex " + GET_NAME(cfg, *vi));
      const CustomSet<unsigned int> & local_scalar_uses = cfg->CGetOpNodeInfo(*vi)->GetScalarVariables(AT_USE);
      CustomSet<unsigned int>::const_iterator local_scalar_use, local_scalar_use_end = local_scalar_uses.end();
      for(local_scalar_use = local_scalar_uses.begin(); local_scalar_use != local_scalar_use_end; local_scalar_use++)
      {
         if(scalar_defs.find(*local_scalar_use) != scalar_defs.end())
         {
            const std::set<vertex> this_defs = scalar_defs.find(*local_scalar_use)->second;
            std::set<vertex>::const_iterator this_def, this_def_end = this_defs.end();
            for(this_def = this_defs.begin(); this_def != this_def_end; this_def++)
            {
               const bool forward_dependence = function_behavior->CheckReachability(*this_def, *vi);
               const bool feedback_dependence = function_behavior->CheckReachability(*vi, *this_def);
               THROW_ASSERT(!(forward_dependence and feedback_dependence), "Dependence between operation " + GET_NAME(cfg, *this_def) + " and " + GET_NAME(cfg, *vi) + " is in both the direction");
               if(forward_dependence)
               {
                  function_behavior->ogc->AddEdge(*this_def, *vi, DFG_SELECTOR);
                  function_behavior->ogc->add_edge_info(*this_def, *vi, DFG_SELECTOR, *local_scalar_use);
                  if(function_behavior->CheckFeedbackReachability(*vi, *this_def))
                  {
                     function_behavior->ogc->AddEdge(*vi, *this_def, FB_ADG_SELECTOR);
                     ///NOTE: label associated with forward selector also on feedback edge
                     function_behavior->ogc->add_edge_info(*this_def, *vi, ADG_SELECTOR, *local_scalar_use);
                  }
               }

               if(feedback_dependence)
               {
                  if(*vi != *this_def)
                  {
                     function_behavior->ogc->AddEdge(*vi, *this_def, ADG_SELECTOR);
                     function_behavior->ogc->add_edge_info(*vi, *this_def, ADG_SELECTOR, *local_scalar_use);
                  }
                  if(function_behavior->CheckFeedbackReachability(*this_def, *vi))
                  {
                     function_behavior->ogc->AddEdge(*this_def, *vi, FB_DFG_SELECTOR);
                     ///NOTE: label associated with forward selector also on feedback edge
                     function_behavior->ogc->add_edge_info(*this_def, *vi, DFG_SELECTOR, *local_scalar_use);
                  }
               }

               if(*vi == *this_def)
               {
                  function_behavior->ogc->AddEdge(*vi, *vi, FB_DFG_SELECTOR);
                  function_behavior->ogc->add_edge_info(*vi, *vi, DFG_SELECTOR, *local_scalar_use);
               }
            }
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Computed scalar anti and data dependences of vertex " + GET_NAME(cfg, *vi));
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Computing virtual (1) anti and data dependences of vertex " + GET_NAME(cfg, *vi));
      const CustomSet<unsigned int> & local_virtual_DATADEPS_uses = cfg->CGetOpNodeInfo(*vi)->GetVirtualVariables_DATADEPS(AT_USE);
      CustomSet<unsigned int>::const_iterator local_virtual_DATADEPS_use, local_virtual_DATADEPS_use_end = local_virtual_DATADEPS_uses.end();
      for(local_virtual_DATADEPS_use = local_virtual_DATADEPS_uses.begin(); local_virtual_DATADEPS_use != local_virtual_DATADEPS_use_end; local_virtual_DATADEPS_use++)
      {
         if(virtual_DATADEPS_defs.find(*local_virtual_DATADEPS_use) != virtual_DATADEPS_defs.end())
         {
            const std::set<vertex> this_defs = virtual_DATADEPS_defs.find(*local_virtual_DATADEPS_use)->second;
            std::set<vertex>::const_iterator this_def, this_def_end = this_defs.end();
            for(this_def = this_defs.begin(); this_def != this_def_end; this_def++)
            {
               const bool forward_dependence = function_behavior->CheckReachability(*this_def, *vi);
               const bool feedback_dependence = function_behavior->CheckReachability(*vi, *this_def);
               THROW_ASSERT(!(forward_dependence and feedback_dependence), "Dependence between operation " + GET_NAME(cfg, *this_def) + " and " + GET_NAME(cfg, *vi) + " is in both the direction");
               if(forward_dependence)
               {
                  function_behavior->ogc->AddEdge(*this_def, *vi, DFG_SELECTOR);
                  function_behavior->ogc->add_edge_info(*this_def, *vi, DFG_SELECTOR, *local_virtual_DATADEPS_use);
                  if(function_behavior->CheckFeedbackReachability(*vi, *this_def))
                  {
                     function_behavior->ogc->AddEdge(*vi, *this_def, FB_ADG_SELECTOR);
                     ///NOTE: label associated with forward selector also on feedback edge
                     function_behavior->ogc->add_edge_info(*this_def, *vi, ADG_SELECTOR, *local_virtual_DATADEPS_use);
                  }
               }

               if(feedback_dependence)
               {
                  if(*vi != *this_def)
                  {
                     function_behavior->ogc->AddEdge(*vi, *this_def, ADG_SELECTOR);
                     function_behavior->ogc->add_edge_info(*vi, *this_def, ADG_SELECTOR, *local_virtual_DATADEPS_use);
                  }
                  if(function_behavior->CheckFeedbackReachability(*this_def, *vi))
                  {
                     function_behavior->ogc->AddEdge(*this_def, *vi, FB_DFG_SELECTOR);
                     ///NOTE: label associated with forward selector also on feedback edge
                     function_behavior->ogc->add_edge_info(*this_def, *vi, DFG_SELECTOR, *local_virtual_DATADEPS_use);
                  }
               }
            }
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Computed virtual (1) anti and data dependences of vertex " + GET_NAME(cfg, *vi));
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Computing virtual (2) anti and data dependences of vertex " + GET_NAME(cfg, *vi));
      const CustomSet<unsigned int> & local_virtual_ANTIDEPS_uses = cfg->CGetOpNodeInfo(*vi)->GetVirtualVariables_ANTIDEPS(AT_USE);
      CustomSet<unsigned int>::const_iterator local_virtual_ANTIDEPS_use, local_virtual_ANTIDEPS_use_end = local_virtual_ANTIDEPS_uses.end();
      for(local_virtual_ANTIDEPS_use = local_virtual_ANTIDEPS_uses.begin(); local_virtual_ANTIDEPS_use != local_virtual_ANTIDEPS_use_end; local_virtual_ANTIDEPS_use++)
      {
         if(virtual_ANTIDEPS_defs.find(*local_virtual_ANTIDEPS_use) != virtual_ANTIDEPS_defs.end())
         {
            const std::set<vertex> this_defs = virtual_ANTIDEPS_defs.find(*local_virtual_ANTIDEPS_use)->second;
            std::set<vertex>::const_iterator this_def, this_def_end = this_defs.end();
            for(this_def = this_defs.begin(); this_def != this_def_end; this_def++)
            {
               const bool forward_dependence = function_behavior->CheckReachability(*this_def, *vi);
               const bool feedback_dependence = function_behavior->CheckReachability(*vi, *this_def);
               THROW_ASSERT(!(forward_dependence and feedback_dependence), "Dependence between operation " + GET_NAME(cfg, *this_def) + " and " + GET_NAME(cfg, *vi) + " is in both the direction");
               if(forward_dependence)
               {
                  function_behavior->ogc->AddEdge(*this_def, *vi, DFG_SELECTOR);
                  function_behavior->ogc->add_edge_info(*this_def, *vi, DFG_SELECTOR, *local_virtual_ANTIDEPS_use);
                  if(function_behavior->CheckFeedbackReachability(*vi, *this_def))
                  {
                     function_behavior->ogc->AddEdge(*vi, *this_def, FB_ADG_SELECTOR);
                     ///NOTE: label associated with forward selector also on feedback edge
                     function_behavior->ogc->add_edge_info(*this_def, *vi, ADG_SELECTOR, *local_virtual_ANTIDEPS_use);
                  }
               }

               if(feedback_dependence)
               {
                  if(*vi != *this_def)
                  {
                     function_behavior->ogc->AddEdge(*vi, *this_def, ADG_SELECTOR);
                     function_behavior->ogc->add_edge_info(*vi, *this_def, ADG_SELECTOR, *local_virtual_ANTIDEPS_use);
                  }
                  if(function_behavior->CheckFeedbackReachability(*this_def, *vi))
                  {
                     function_behavior->ogc->AddEdge(*this_def, *vi, FB_DFG_SELECTOR);
                     ///NOTE: label associated with forward selector also on feedback edge
                     function_behavior->ogc->add_edge_info(*this_def, *vi, DFG_SELECTOR, *local_virtual_ANTIDEPS_use);
                  }
               }
            }
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Computed virtual (2) anti and data dependences of vertex " + GET_NAME(cfg, *vi));
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Computing scalar output dependences of vertex " + GET_NAME(cfg, *vi));
      const CustomSet<unsigned int> & local_scalar_defs = cfg->CGetOpNodeInfo(*vi)->GetScalarVariables(AT_DEF);
      CustomSet<unsigned int>::const_iterator local_scalar_def, local_scalar_def_end = local_scalar_defs.end();
      for(local_scalar_def = local_scalar_defs.begin(); local_scalar_def != local_scalar_def_end; local_scalar_def++)
      {
         if(scalar_defs.find(*local_scalar_def) != scalar_defs.end())
         {
            const std::set<vertex> this_defs = scalar_defs.find(*local_scalar_def)->second;
            std::set<vertex>::const_iterator this_def, this_def_end = this_defs.end();
            for(this_def = this_defs.begin(); this_def != this_def_end; this_def++)
            {
               const bool forward_dependence = function_behavior->CheckReachability(*this_def, *vi);
               if(forward_dependence)
               {
                  function_behavior->ogc->AddEdge(*this_def, *vi, ODG_SELECTOR);
                  function_behavior->ogc->add_edge_info(*this_def, *vi, ODG_SELECTOR, *local_scalar_def);
                  if(function_behavior->CheckFeedbackReachability(*vi, *this_def))
                  {
                     function_behavior->ogc->AddEdge(*vi, *this_def, FB_ODG_SELECTOR);
                     ///NOTE: label associated with forward selector also on feedback edge
                     function_behavior->ogc->add_edge_info(*vi, *this_def, ODG_SELECTOR, *local_scalar_def);
                  }
               }
            }
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Computed scalar anti and data dependences of vertex " + GET_NAME(cfg, *vi));
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Computing virtual (3) dependences of vertex " + GET_NAME(cfg, *vi));
      const CustomSet<unsigned int> & local_virtual_defs = cfg->CGetOpNodeInfo(*vi)->GetVirtualVariables_ANTIDEPS(AT_DEF);
      CustomSet<unsigned int>::const_iterator local_virtual_def, local_virtual_def_end = local_virtual_defs.end();
      for(local_virtual_def = local_virtual_defs.begin(); local_virtual_def != local_virtual_def_end; local_virtual_def++)
      {
         if(virtual_DATADEPS_defs.find(*local_virtual_def) != virtual_DATADEPS_defs.end())
         {
            const std::set<vertex> this_defs = virtual_DATADEPS_defs.find(*local_virtual_def)->second;
            std::set<vertex>::const_iterator this_def, this_def_end = this_defs.end();
            for(this_def = this_defs.begin(); this_def != this_def_end; this_def++)
            {
               const bool forward_dependence = function_behavior->CheckReachability(*this_def, *vi);
               if(forward_dependence)
               {
                  function_behavior->ogc->AddEdge(*this_def, *vi, ODG_SELECTOR);
                  function_behavior->ogc->add_edge_info(*this_def, *vi, ODG_SELECTOR, *local_virtual_def);
                  if(function_behavior->CheckFeedbackReachability(*vi, *this_def))
                  {
                     function_behavior->ogc->AddEdge(*vi, *this_def, FB_ODG_SELECTOR);
                     ///NOTE: label associated with forward selector also on feedback edge
                     function_behavior->ogc->add_edge_info(*vi, *this_def, ODG_SELECTOR, *local_virtual_def);
                  }
               }
            }
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Computed virtual (3) dependences of vertex " + GET_NAME(cfg, *vi));
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "-->Computing virtual 4 dependences of vertex " + GET_NAME(cfg, *vi));
      for(local_virtual_DATADEPS_use = local_virtual_DATADEPS_uses.begin(); local_virtual_DATADEPS_use != local_virtual_DATADEPS_use_end; local_virtual_DATADEPS_use++)
      {
         if(virtual_ANTIDEPS_defs.find(*local_virtual_DATADEPS_use) != virtual_ANTIDEPS_defs.end())
         {
            const std::set<vertex> this_defs = virtual_ANTIDEPS_defs.find(*local_virtual_DATADEPS_use)->second;
            std::set<vertex>::const_iterator this_def, this_def_end = this_defs.end();
            for(this_def = this_defs.begin(); this_def != this_def_end; this_def++)
            {
               const bool forward_dependence = function_behavior->CheckReachability(*vi, *this_def);
               if(forward_dependence)
               {
                  function_behavior->ogc->AddEdge(*vi, *this_def, ADG_SELECTOR);
                  function_behavior->ogc->add_edge_info(*vi, *this_def, ADG_SELECTOR, *local_virtual_DATADEPS_use);
               }
            }
         }
      }
      INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Computed virtual (4) dependences of vertex " + GET_NAME(cfg, *vi));
   }
   INDENT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "<--Computed dependencies");
   if(parameters->getOption<bool>(OPT_print_dot))
   {
      std::string file_name;
      function_behavior->CGetOpGraph(FunctionBehavior::DFG)->WriteDot("OP_DFG.dot", 1);
      function_behavior->CGetOpGraph(FunctionBehavior::FDFG)->WriteDot("OP_FDFG.dot", 1);
      function_behavior->CGetOpGraph(FunctionBehavior::ADG)->WriteDot("OP_ADG.dot", 1);
      function_behavior->CGetOpGraph(FunctionBehavior::FADG)->WriteDot("OP_FADG.dot", 1);
      function_behavior->CGetOpGraph(FunctionBehavior::ODG)->WriteDot("OP_ODG.dot", 1);
      function_behavior->CGetOpGraph(FunctionBehavior::FODG)->WriteDot("OP_FODG.dot", 1);
      function_behavior->CGetOpGraph(FunctionBehavior::SAODG)->WriteDot("OP_SAODG.dot", 1);
   }
#ifndef NDEBUG
   try
   {
      const OpGraphConstRef dfg = function_behavior->CGetOpGraph(FunctionBehavior::DFG);
      std::deque<vertex> vertices;
      boost::topological_sort(*dfg, std::front_inserter(vertices));
   }
   catch (const char* msg)
   {
      THROW_UNREACHABLE("dfg graph of function " + function_name + " is not acyclic");
   }
   catch (const std::string & msg)
   {
      THROW_UNREACHABLE("dfg graph of function " + function_name + " is not acyclic");
   }
   catch (const std::exception& ex)
   {
      THROW_UNREACHABLE("dfg graph of function " + function_name + " is not acyclic");
   }
   catch ( ... )
   {
      THROW_UNREACHABLE("dfg graph of function " + function_name + " is not acyclic");
   }
#endif

}
