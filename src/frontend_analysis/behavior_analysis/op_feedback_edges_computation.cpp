/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file op_feedback_edges_computation.cpp
 * @brief Analysis step computing Analysis step computing feedback edges for operation control flow graph
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Header include
#include "op_feedback_edges_computation.hpp"

///Behavior include
#include "behavioral_helper.hpp"
#include "application_manager.hpp"
#include "function_behavior.hpp"
#include "loop.hpp"
#include "loops.hpp"
#include "op_graph.hpp"

///Graph include
#include "basic_block.hpp"
#include "operations_graph_constructor.hpp"
#include "tree_basic_block.hpp"

///Frontend include
#include "Parameter.hpp"

///Tree include
#include "tree_manager.hpp"

op_feedback_edges_computation::op_feedback_edges_computation(const ParameterConstRef _parameters, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, OP_FEEDBACK_EDGES_IDENTIFICATION, _design_flow_manager, _parameters)
{
   debug_level = parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE);
}

op_feedback_edges_computation::~op_feedback_edges_computation()
{}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > op_feedback_edges_computation::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(OPERATIONS_CFG_COMPUTATION, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(POST_PRECEDENCE_RELATIONSHIP) :
      {
         break;
      }
      case(PRECEDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(LOOP_REGIONS_COMPUTATION, SAME_FUNCTION));
#if HAVE_ZEBU_BUILT
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(LOOPS_ANALYSIS, SAME_FUNCTION));
#endif
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void op_feedback_edges_computation::Exec()
{
   if(debug_level >= DEBUG_LEVEL_PEDANTIC)
   {
      PrintTreeManager(true);
   }
   const BBGraphConstRef fbb = function_behavior->GetBBGraph(FunctionBehavior::FBB);
    ///then consider loops
   std::list<LoopConstRef> loops = function_behavior->CGetLoops()->GetList();
   std::list<LoopConstRef>::const_iterator loop_end = loops.end();
   for(std::list<LoopConstRef>::const_iterator loop = loops.begin(); loop != loop_end; loop++)
   {
      if((*loop)->GetId() == 0)
         continue;
      if ((*loop)->IsReducible())
      {
         vertex bb_header = (*loop)->GetHeader();
         const BBNodeInfoConstRef bb_node_info = fbb->CGetBBNodeInfo(bb_header);
         vertex label_vertex = bb_node_info->statements_list.front();
         InEdgeIterator ei, ei_end;
         std::unordered_set<vertex> blocks;
         (*loop)->get_recursively_bb(blocks);
         for (boost::tie(ei, ei_end) = boost::in_edges(bb_header, *fbb); ei != ei_end; ei++)
         {
            vertex from_bb = boost::source(*ei, *fbb);
            if(std::find(blocks.begin(), blocks.end(), from_bb) != blocks.end())
            {
               const BBNodeInfoConstRef bb_node_info_from = fbb->CGetBBNodeInfo(from_bb);
               THROW_ASSERT(bb_node_info_from->statements_list.size(), "Empty block " + boost::lexical_cast<std::string>(bb_node_info_from->block->number));
               vertex goto_vertex = bb_node_info_from->statements_list.back();
               ///add the feedback control dependence and the feedback control flow graph edges
               function_behavior->ogc->RemoveEdge(goto_vertex, label_vertex, CFG_SELECTOR);
               function_behavior->ogc->AddEdge(goto_vertex, label_vertex, FB_CDG_SELECTOR|FB_CFG_SELECTOR);
            }
         }
      }
      else
      {
         THROW_ERROR_CODE(IRREDUCIBLE_LOOPS_EC,"Irreducible loops not yet supported");
      }
   }

   if(parameters->getOption<bool>(OPT_print_dot))
   {
      function_behavior->CGetOpGraph(FunctionBehavior::FCFG)->WriteDot("OP_FCFG.dot");
      function_behavior->CGetOpGraph(FunctionBehavior::CFG)->WriteDot("OP_CFG.dot");
   }
}
