/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file cdg_computation.cpp
 * @brief Analysis step performing control dependence computation.
 *
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Header include
#include "cdg_computation.hpp"

///Behavior include
#include "basic_block.hpp"
#include "basic_blocks_graph_constructor.hpp"
#include "behavioral_helper.hpp"
#include "application_manager.hpp"
#include "function_behavior.hpp"
#include "loops.hpp"
#include "op_graph.hpp"
#include "operations_graph_constructor.hpp"

///Graph include
#include "graph.hpp"

///Parameter include
#include "Parameter.hpp"

///STD include
#include <iostream>

///Utility include
#include "Dominance.hpp"

cdg_computation::cdg_computation(const ParameterConstRef _Param, const application_managerRef _AppM, unsigned int _function_id, const DesignFlowManagerConstRef _design_flow_manager) :
   FunctionFrontendFlowStep(_AppM, _function_id, CONTROL_DEPENDENCE_COMPUTATION, _design_flow_manager, _Param)
{}

cdg_computation::~cdg_computation()
{
}

const std::unordered_set<std::pair<FrontendFlowStepType, FrontendFlowStep::FunctionRelationship> > cdg_computation::ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const
{
   std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > relationships;
   switch(relationship_type)
   {
      case(DEPENDENCE_RELATIONSHIP) :
      {
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(BB_FEEDBACK_EDGES_IDENTIFICATION, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(DOM_POST_DOM_COMPUTATION, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(OPERATIONS_CFG_COMPUTATION, SAME_FUNCTION));
         relationships.insert(std::pair<FrontendFlowStepType, FunctionRelationship>(ORDER_COMPUTATION, SAME_FUNCTION));
         break;
      }
      case(POST_DEPENDENCE_RELATIONSHIP) :
      case(POST_PRECEDENCE_RELATIONSHIP) :
      case(PRECEDENCE_RELATIONSHIP):
      {
         break;
      }
      default:
      {
         THROW_UNREACHABLE("");
      }
   }
   return relationships;
}

void cdg_computation::Exec()
{
   const BBGraphRef  bb = function_behavior->GetBBGraph(FunctionBehavior::BB);
   OpGraphRef fcfg = function_behavior->fcfg;
   const dominance<BBGraph> * post_dominators = function_behavior->post_dominators;
   const BehavioralHelperConstRef  helper = function_behavior->CGetBehavioralHelper();
   if(boost::num_vertices(*fcfg) == 0)
      return;

   EdgeIterator ei, ei_end;
   std::list<vertex> bb_levels;
   boost::topological_sort(*bb, std::front_inserter(bb_levels));
   std::map<vertex, unsigned int> bb_sorted;
   unsigned int counter = 0;
   for(std::list<vertex>::iterator it = bb_levels.begin(); it != bb_levels.end(); it++)
      bb_sorted[*it] = ++counter;
   //iterate over outgoing edges of the basic block CFG.
   for (boost::tie(ei, ei_end) = boost::edges(*bb); ei != ei_end ; ++ei )
   {
      vertex A = boost::source(*ei, *bb);
      vertex B = boost::target(*ei, *bb);
      InEdgeIterator pd_ei, pd_ei_end;
      vertex current_node = B;
      while (current_node && current_node != A && current_node != post_dominators->get_immediate_dominator(A))
      {
         const std::list<vertex> operations_A = bb->CGetBBNodeInfo(A)->statements_list;
         const std::list<vertex> operations_B = bb->CGetBBNodeInfo(current_node)->statements_list;
         if (operations_A.size() && operations_B.size())
         {
            std::list<vertex>::const_iterator op_B, op_B_end = operations_B.end();
            if(bb_sorted[current_node]>bb_sorted[A])
            {
               function_behavior->bbgc->AddEdge(A, current_node, CDG_SELECTOR);
               std::set<unsigned int> labels = bb->CGetBBEdgeInfo(*ei)->get_labels(CFG_SELECTOR);
               if(labels.size())
               {
                  std::set<unsigned int>::iterator it_end = labels.end();
                  for(std::set<unsigned int>::iterator it = labels.begin(); it != it_end; it++)
                  {
                     function_behavior->bbgc->add_bb_edge_info(A, current_node, CDG_SELECTOR, *it);
                  }
               }
               for (op_B = operations_B.begin();op_B != op_B_end; op_B++)
               {
                  function_behavior->ogc->AddEdge(operations_A.back(), *op_B, CDG_SELECTOR);
                  if (labels.size())
                  {
                     std::set<unsigned int>::iterator it_end = labels.end();
                     for(std::set<unsigned int>::iterator it = labels.begin(); it != it_end; it++)
                        function_behavior->ogc->add_edge_info(operations_A.back(), *op_B, CDG_SELECTOR, *it);
                  }
               }
            }
            else
            {
               break;
            }
         }
         current_node = post_dominators->get_immediate_dominator(current_node);
      }
   }

   BBGraphRef cdg_bb = function_behavior->cdg_bb;

   //Counter used to enumerate different
   unsigned int cer_counter = 0;
   //Map control equivalent region codification to control equivalent index;
   //The codification is the set of pair predecessor-edge label in the cdg_computation
   std::map<std::set<std::pair<vertex, std::set<unsigned int> > >, unsigned int> cdg_to_index;

   const std::deque<vertex> & topological_sorted_nodes = function_behavior->get_bb_levels();
   std::deque<vertex>::const_iterator it, it_end;
   it_end = topological_sorted_nodes.end();
   for(it = topological_sorted_nodes.begin(); it != it_end; it++)
   {
      unsigned int cer_index = cer_counter;
      const BBNodeInfoRef bb_node_info = cdg_bb->GetBBNodeInfo(*it);
      if (boost::in_degree(*it, *cdg_bb) > 0)
      {
         //codification of this basic block
         std::set<std::pair<vertex, std::set<unsigned int> > > this_cod;
         InEdgeIterator eii, eii_end;
         for(boost::tie(eii, eii_end) = boost::in_edges(*it, *cdg_bb); eii != eii_end; eii++)
         {
            this_cod.insert(std::pair<vertex, std::set<unsigned int> >(boost::source(*eii, *cdg_bb), cdg_bb->CGetBBEdgeInfo(*eii)->get_labels(CDG_SELECTOR)));
         }
         if(cdg_to_index.find(this_cod) == cdg_to_index.end())
         {
            cdg_to_index[this_cod] = cer_counter;
            cer_counter++;
         }
         else
         {
            cer_index = cdg_to_index[this_cod];
         }
         bb_node_info->cer = cer_index;
         std::list<vertex> statements_list = bb_node_info->statements_list;
         std::list<vertex>::iterator it2, it2_end = statements_list.end();
         for(it2 = statements_list.begin(); it2 != it2_end; it2++)
         {
           OpNodeInfo * cinfo = GET_NODE_INFO(fcfg, OpNodeInfo, *it2);
           cinfo->cer = cer_index;
         }
      }
   }

   if(parameters->getOption<bool>(OPT_print_dot))
   {
      function_behavior->CGetBBGraph(FunctionBehavior::CDG_BB)->WriteDot("BB_CDG.dot");
      function_behavior->CGetOpGraph(FunctionBehavior::CDG)->WriteDot("OP_CDG.dot");
   }
}
