/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file frontend_flow_step.hpp
 * @brief This class contains the base representation for a generic frontend flow step
 *
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

#ifndef FRONTEND_FLOW_STEP_HPP
#define FRONTEND_FLOW_STEP_HPP

///Autoheader include
#include "config_HAVE_BAMBU_BUILT.hpp"
#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_FROM_PRAGMA_BUILT.hpp"
#include "config_HAVE_HOST_PROFILING_BUILT.hpp"
#include "config_HAVE_MPPB.hpp"
#include "config_HAVE_PRAGMA_BUILT.hpp"
#include "config_HAVE_RTL_BUILT.hpp"
#include "config_HAVE_ZEBU_BUILT.hpp"

///Superclass include
#include "design_flow_step.hpp"

///STL include
#include <unordered_map>

///Utility include
#include "refcount.hpp"

///Forward declaration
REF_FORWARD_DECL(application_manager);
REF_FORWARD_DECL(ArchManager);
REF_FORWARD_DECL(DesignFlowManager);

typedef enum
{
///Application frontend flow steps
   CREATE_TREE_MANAGER = 0,
#if HAVE_HOST_PROFILING_BUILT
   DUMP_PROFILING_DATA,
#endif
   FUNCTION_ANALYSIS,                  //! Creation of the call graph
#if HAVE_HOST_PROFILING_BUILT
   HOST_PROFILING,
   HPP_PROFILING,
   LOOPS_PROFILING,
#endif
#if HAVE_FROM_PRAGMA_BUILT
   PRAGMA_SUBSTITUTION,
#endif
#if HAVE_HOST_PROFILING_BUILT
   READ_PROFILING_DATA,
#endif
#if HAVE_ZEBU_BUILT
   SIZEOF_SUBSTITUTION,
#endif
   SYMBOLIC_APPLICATION_FRONTEND_FLOW_STEP,
#if HAVE_HOST_PROFILING_BUILT
   TP_PROFILING,
#endif
///Function frontend flow steps
   ADD_BB_ECFG_EDGES,
#if HAVE_ZEBU_BUILT
   ADD_OP_ECFG_EDGES,
#endif
   ADD_OP_FLOW_EDGES,
#if HAVE_ZEBU_BUILT || HAVE_BAMBU_BUILT
   ARRAY_REF_FIX,
#endif
   BASIC_BLOCKS_CFG_COMPUTATION,
   BB_FEEDBACK_EDGES_IDENTIFICATION,
   BLOCK_FIX,
#if HAVE_ZEBU_BUILT
   CALL_ARGS_STRUCTURING,
#endif
   CHECK_SYSTEM_TYPE,                  //! Set the system flag to variables and types
   CONTROL_DEPENDENCE_COMPUTATION,
#if HAVE_ZEBU_BUILT
   DEAD_CODE_ELIMINATION,
#endif
#if HAVE_BAMBU_BUILT
   DETERMINE_MEMORY_ACCESSES,
#endif
   DOM_POST_DOM_COMPUTATION,
#if HAVE_EXPERIMENTAL
   EXTENDED_PDG_COMPUTATION,
   REDUCED_PDG_COMPUTATION,
   PARALLEL_REGIONS_GRAPH_COMPUTATION,
#endif
#if HAVE_ZEBU_BUILT
   GLOBAL_VARIABLES_ANALYSIS,
   HEADER_STRUCTURING,
#endif
#if HAVE_ZEBU_BUILT
   INSTRUCTION_SEQUENCES_COMPUTATION,
#endif
#if HAVE_BAMBU_BUILT
   IR_LOWERING,
#endif
   LOOP_COMPUTATION,
   LOOP_REGIONS_COMPUTATION,
   LOOP_REGIONS_FLOW_COMPUTATION,
#if HAVE_ZEBU_BUILT
   LOOPS_ANALYSIS,
#endif
   LOOPS_IDENTIFICATION,
#if HAVE_BAMBU_BUILT
   MULTI_WAY_IF,
   NI_SSA_LIVENESS,
#endif
   OP_FEEDBACK_EDGES_IDENTIFICATION,
   OPERATIONS_CFG_COMPUTATION,
   ORDER_COMPUTATION,
#if HAVE_ZEBU_BUILT && HAVE_EXPERIMENTAL
   PARALLEL_LOOP_SWAP,
   PARALLEL_LOOPS_ANALYSIS,
#endif
#if HAVE_BAMBU_BUILT
   PHI_OPT,
#endif
#if HAVE_ZEBU_BUILT
   POINTED_DATA_COMPUTATION,
   POINTED_DATA_EVALUATION,
#endif
#if HAVE_PRAGMA_BUILT
   PRAGMA_ANALYSIS,
#endif
#if HAVE_ZEBU_BUILT
   PREDICTABILITY_ANALYSIS,
   PROBABILITY_PATH,
#endif
   REACHABILITY_COMPUTATION,
#if HAVE_ZEBU_BUILT
#if HAVE_EXPERIMENTAL
   REFINED_DATA_FLOW_ANALYSIS,
   REFINED_VAR_COMPUTATION,
#endif
#endif
#if HAVE_BAMBU_BUILT
   REMOVE_CLOBBER_GA,
#endif
#if HAVE_ZEBU_BUILT
   REVERSE_RESTRICT_COMPUTATION,
   SHORT_CIRCUIT_STRUCTURING,
#endif
#if HAVE_BAMBU_BUILT
   SHORT_CIRCUIT_TAF,
   SIMPLE_CODE_MOTION,
   SOFT_FLOAT_CG_EXT,
#endif
#if HAVE_BAMBU_BUILT && HAVE_EXPERIMENTAL
   SPECULATION_EDGES_COMPUTATION,
#endif
   SPLIT_PHINODES,
   SSA_DATA_FLOW_ANALYSIS,
   SWITCH_FIX,
#if HAVE_BAMBU_BUILT
   UNROLLING_DEGREE,
#endif
#if HAVE_RTL_BUILT && HAVE_ZEBU_BUILT
   UPDATE_RTL_WEIGHT,
#endif
#if HAVE_ZEBU_BUILT
   UPDATE_TREE_WEIGHT,
#endif
   USE_COUNTING,
   VAR_ANALYSIS,
   VAR_DECL_FIX,
#if HAVE_BAMBU_BUILT
   VERIFICATION_OPERATION,
   VIRTUAL_PHI_NODES_SPLIT
#endif
} FrontendFlowStepType;

/**
 * Definition of hash function for FunctionFrontendAnalysisType
 */
namespace std
{
   template <>
      struct hash<FrontendFlowStepType> : public unary_function<FrontendFlowStepType, size_t>
      {
         size_t operator()(FrontendFlowStepType algorithm) const
         {
            hash<int> hasher;
            return hasher(static_cast<int>(algorithm));
         }
      };
}

class FrontendFlowStep : public DesignFlowStep
{
   public:
      ///The different relationship type between function analysis
      typedef enum
      {
         ALL_FUNCTIONS,     /**! All the functions composing the application */
         CALLED_FUNCTIONS,  /**! All the functions called by the current one */
         CALLING_FUNCTIONS, /**! All the functions which call the current one */
         SAME_FUNCTION,     /**! Same function */
         WHOLE_APPLICATION  /**! The whole application */
      } FunctionRelationship;

   protected:
      ///The application manager
      const application_managerRef AppM;

      ///The type of this step
      const FrontendFlowStepType frontend_flow_step_type;

      /**
       * Return the set of analyses in relationship with this design step
       * @param relationship_type is the type of relationship to be considered
       */
      virtual const std::unordered_set<std::pair<FrontendFlowStepType, FunctionRelationship> > ComputeFrontendRelationships(const DesignFlowStep::RelationshipType relationship_type) const = 0;

   public:
      /**
       * Constructor
       * @param AppM is the application manager
       * @param design_flow_manager is the design flow manager
       * @param frontend_flow_step_type is the type of the analysis
       * @param _Param is the set of the parameters
       */
      FrontendFlowStep(const application_managerRef AppM, const FrontendFlowStepType frontend_flow_step_type, const DesignFlowManagerConstRef design_flow_manager, const ParameterConstRef parameters);

      /**
       * Destructor
       */
      virtual ~FrontendFlowStep();

      /**
       * Execute this step
       */
      virtual void Exec() = 0;

      /**
       * Compute the relationships of a step with other steps
       * @param dependencies is where relationships will be stored
       * @param relationship_type is the type of relationship to be computed
       */
      virtual void ComputeRelationships(DesignFlowStepSet & relationship, const DesignFlowStep::RelationshipType relationship_type);

      /**
       * Return the signature of this step
       */
      virtual const std::string GetSignature() const = 0;

      /**
       * Return the name of this design step
       * @return the name of the pass (for debug purpose)
       */
      virtual const std::string GetName() const = 0;

      /**
       * Return the name of the type of this frontend flow step
       */
      virtual const std::string GetKindText() const;

      /**
       * Given a frontend flow step type, return the name of the type
       * @param type is the type to be consiedred
       * @return the name of the type
       */
      static
      const std::string EnumToKindText(const FrontendFlowStepType frontend_flow_step_type);

      /**
       * Return the factory to create this type of steps
       * @return the factory to create frontend flow step
       */
      const DesignFlowStepFactoryConstRef CGetDesignFlowStepFactory() const;

      /**
       * Dump the tree manager
       * @param before specifies if printing is performed before execution of this step"
       */
      void PrintTreeManager(const bool before) const;
};

/**
 * Definition of hash function for FrontendFlowStep::FunctionRelationship
 */
namespace std
{
   template <>
      struct hash<FrontendFlowStep::FunctionRelationship> : public unary_function<FrontendFlowStep::FunctionRelationship, size_t>
      {
         size_t operator()(FrontendFlowStep::FunctionRelationship relationship) const
         {
            hash<int> hasher;
            return hasher(static_cast<int>(relationship));
         }
      };
}

#endif
