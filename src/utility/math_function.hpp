/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

/**
 * @file math_function.hpp 
 * @brief mathematical utility function not provided by standard libraries 
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */

#ifndef MATH_FUNCTION_HPP
#define MATH_FUNCTION_HPP

///Utility include
#include "augmented_vector.hpp"
#include <boost/math/common_factor_rt.hpp>

/**
 * Return the distance between a point and a line (represented as a couple of points) in a n-dimensional space
 */
long double get_point_line_distance(const AugmentedVector<long double> point, AugmentedVector<long double> line_point1, AugmentedVector<long double> line_point2);

/**
 * Return the greatest common divisor
 * @param first is the first operand
 * @param second is the second operand
 * @return the greatest common divisor of first and second
 */
template<typename Integer>
Integer GreatestCommonDivisor(const Integer first, const Integer second)
{
   return boost::math::gcd<Integer>(first, second);
}

/**
 * Return the least common multiple of two integers
 * @param first is the first operand
 * @param second is the second operand
 * @return the least common multiple of first and second
 */
template<typename Integer>
Integer LeastCommonMultiple(const Integer first, const Integer second)
{
   return boost::math::lcm<Integer>(first, second);
}
#endif
