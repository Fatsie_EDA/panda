/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file utility.hpp
 * @brief This file collects some utility functions and macros.
 *
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef UTILITY_HPP
#define UTILITY_HPP

///STD include
#include <limits>
#include <sstream>
#include <string>

///Utility include
#include "string_manipulation.hpp"
#include <boost/filesystem/convenience.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/functional/hash/hash.hpp>
#include <boost/version.hpp>


/// INT representing infinite
#define INFINITE_INT (std::numeric_limits<int>::max())

/// UNSIGNED INT representing infinite
#define INFINITE_UINT (std::numeric_limits<unsigned int>::max())

/// SIZE_T representing infinite
#define INFINITE_SIZE_T (std::numeric_limits<size_t>::max())

/// DOUBLE representing infinite
#define INFINITE_DOUBLE (std::numeric_limits<double>::max())

/// long double representing infinite
#define INFINITE_LONG_DOUBLE (std::numeric_limits<long double>::max())

/// LONGLONG representing infinite
#define INFINITE_ULONGLONG_INT (std::numeric_limits<unsigned long long int>::max())

/// long double representing -infinite
#define MINUS_INFINITE_LONG_DOUBLE (std::numeric_limits<long double>::min())

///macro used to solve problem of parameters used only in not-release
#ifdef NDEBUG
#define DEBUG_PARAMETER(parameter)
#else
#define DEBUG_PARAMETER(parameter) parameter
#endif


/**
 * @return return the maximum between two numbers.
 * @param a is the first number.
 * @param b is the second number.
*/
#ifndef MAX
template<typename type_t>
type_t MAX(const type_t a, const type_t b)
{
   return a > b ? a : b;
}
#endif

/**
 * @return return the minimum between two numbers.
 * @param a is the first number.
 * @param b is the second number.
*/
#ifndef MIN
template<typename type_t>
type_t MIN(const type_t a, const type_t b)
{
   return a < b ? a : b;
}
#endif

/**
* Macro which defines the get_kind_text function that returns the parameter as a string.
*/
#define GET_KIND_TEXT(meth) std::string get_kind_text() const {return std::string(#meth);}
/**
* Macro returning the name of a class. It uses the static version of get_kind_text
*/
#define GET_CLASS_NAME(meth) #meth

/**
 * Macro returning the actual type of an object
 */
#define GET_CLASS(obj) string_demangle(typeid(obj).name())

#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

template<class G>
std::string convert_to_binary(G _value, unsigned int precision)
{
   unsigned long long int value = static_cast<unsigned long long int>(_value);
   std::string bin_value;
   for(unsigned int ind = 0; ind < precision; ind++)
      bin_value = bin_value + (((1LLU << (precision-ind-1)) & value) ? '1' : '0');
   return bin_value;
}

template<class G>
std::string convert_vector_to_string(const std::vector<G>& vector_form, const std::string& separator, bool trim_empty_elements = true)
{
   std::string string_form;
   for(unsigned int i = 0; i < vector_form.size(); i++)
   {
      std::string element_string = boost::lexical_cast<std::string>(vector_form[i]);
      if (trim_empty_elements and element_string.size() == 0) continue;
      if (string_form.size()) string_form += separator;
      string_form += element_string;
   }
   return string_form;
}

template<class G>
std::vector<G> convert_string_to_vector(const std::string& string_form, const std::string& separator, bool trim_empty_elements = true)
{
   std::vector<G> vector_form;
   std::vector<std::string> tmp_vector_form;
   boost::split(tmp_vector_form, string_form, boost::is_any_of(separator));
   for(unsigned int i = 0; i < tmp_vector_form.size(); i++)
   {
      if (trim_empty_elements and tmp_vector_form[i].size() == 0) continue;
      vector_form.push_back(boost::lexical_cast<G>(tmp_vector_form[i]));
   }
   return vector_form;
}

/**
 * Functor to tokenize string used with boost::tokenizer
 */
class string_separator
{
   private :
      ///The delimiter
      const std::string delimiter;

   public:
      /**
       * Empty constructor
       */
      string_separator() :
         delimiter(std::string())
      {}

      /**
       * Constructor
       * @param delimiter is the string used to divide the string
       */
      string_separator(const std::string & _delimiter) :
         delimiter(_delimiter)
      {
      }

      /**
       * Tokenize operator
       * @param next is the start of the portion of the string to be tokenized
       * @param end is the end of the string
       * @param tok is the token found
       * @return true if a token has been found)
       */
      bool operator()(std::string::const_iterator & next, std::string::const_iterator & end, std::basic_string<char, std::char_traits<char>, std::allocator<char> >& tok)
      {
         if(next == end)
            return false;
         std::string current(next, end);
         if(current.find(delimiter) != std::string::npos)
         {
            tok = current.substr(0, current.find(delimiter));
            for(size_t counter = current.find(delimiter) + delimiter.size(); counter != 0; counter --)
              next++;
            return true;
         }
         else
         {
            tok = current;
            next = end;
            return true;
         }
      }
      string_separator operator=(string_separator other)
      {
         return string_separator(other.delimiter);
      }

      /**
       * Reset function (required to implement boost tokenizerFunction model
       */
      void reset()
      {}
};

/**
 * Macro which performs a lexical_cast to a string
 */
#define STR(s) boost::lexical_cast<std::string>(s)

/**
 * Macro which "pretty prints" a multi-line string
 */
#define PP_ONE_LINE(multi_line_string) boost::regex_replace(multi_line_string, boost::regex("\\n"), "\\\\n")

/**
 * Concept checking class
 * This class is used to check that an object can be converted into long double
 */
template <class T>
struct check_long_double
{
   private:
      T example;

   public:
      BOOST_CONCEPT_USAGE(check_long_double)
      {
         long double ld = example;
         (void) ld;
      }
};

/**
 * Randomly shuffle a vector
 * @param shuffle is the vector to be shuffled
 * @param seed is the seed to be used in the random generator
 */
template <class T>
void ShuffleVector(typename std::vector<T> & shuffle, const unsigned int seed)
{
   srand(seed);
   size_t size = shuffle.size();
   while(size > 1)
   {
      size_t k = static_cast<size_t>(rand())%size;

      size--;

      T temp = shuffle[size];
      shuffle[size] = shuffle[k];
      shuffle[k] = temp;
   }
}

///Hash function for std::vector
namespace std
{
   template<typename T>
      struct hash<std::vector<T> > : public std::unary_function<std::vector<T>, std::size_t>
      {
         std::size_t operator()(const std::vector<T> &val) const
         {
            return boost::hash_range<typename std::vector<T>::const_iterator>(val.begin(), val.end());
         }
      };
}

///Hash function for std::pair<T, U>
namespace std
{
   template<typename T, typename U>
      struct hash<std::pair<T, U> > : public std::unary_function<std::pair<T, U>, std::size_t>
      {
         private:
            const hash<T> Th;
            const hash<U> Uh;

         public:
            std::size_t operator()(const std::pair<T, U> &val) const
            {
               return Th(val.first) ^ Uh(val.second);
            }
      };
}

/**
 * convert a real number stored in a string into a string o bits with a given precision
 */
inline std::string convert_fp_to_string(std::string num, unsigned int precision)
{
   union
   {
         unsigned long long ll;
         double d;
         unsigned int i;
         float f;
   } u;
   std::string res;
   char *endptr=nullptr;

   switch(precision)
   {
      case 32:
      {
         u.f = strtof(num.c_str(), &endptr);
         res = "";
         for(unsigned int ind = 0; ind < precision; ind++)
            res = res + (((1U << (precision-ind-1)) & u.i) ? '1' : '0');
         break;
      }
      case 64:
      {
         u.d = strtod(num.c_str(), &endptr);
         res = "";
         for(unsigned int ind = 0; ind < precision; ind++)
            res = res + (((1LLU << (precision-ind-1)) & u.ll) ? '1' : '0');
         break;
      }
      default:
         throw std::string("not supported precision ") + STR(precision);
   }
   return res;
}

#endif
