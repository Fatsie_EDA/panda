/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file global_enums.hpp
 * @brief global enums used which are widely used in framework
 *
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Date$
 * Last modified by $Author$
 *
 */
#ifndef GLOBAL_ENUMS_HPP
#define GLOBAL_ENUMS_HPP

///Autoheader include
#include "config_HAVE_A3.hpp"
#include "config_HAVE_ARM_COMPILER.hpp"
#include "config_HAVE_BAMBU_BUILT.hpp"
#include "config_HAVE_BAMBU_RESULTS_XML.hpp"
#include "config_HAVE_DESIGN_ANALYSIS_BUILT.hpp"
#include "config_HAVE_DIOPSIS.hpp"
#include "config_HAVE_FROM_ARCH_BUILT.hpp"
#include "config_HAVE_FROM_C_BUILT.hpp"
#include "config_HAVE_FROM_LIBERTY.hpp"
#include "config_HAVE_FROM_PSPLIB_BUILT.hpp"
#include "config_HAVE_FROM_SDF3_BUILT.hpp"
#include "config_HAVE_GRAPH_PARTITIONING_BUILT.hpp"
#include "config_HAVE_I386_GCC45_COMPILER.hpp"
#include "config_HAVE_I386_GCC46_COMPILER.hpp"
#include "config_HAVE_I386_GCC47_COMPILER.hpp"
#include "config_HAVE_I386_GCC48_COMPILER.hpp"
#include "config_HAVE_I386_GCC49_COMPILER.hpp"
#include "config_HAVE_LEON3.hpp"
#include "config_HAVE_MPPB.hpp"
#include "config_HAVE_REGRESSORS_BUILT.hpp"
#include "config_HAVE_SIMIT.hpp"
#include "config_HAVE_SOURCE_CODE_STATISTICS_XML.hpp"
#include "config_HAVE_SPARC_COMPILER.hpp"
#include "config_HAVE_TO_C_BUILT.hpp"
#include "config_HAVE_TSIM.hpp"
#include "config_HAVE_TUCANO_BUILT.hpp"
#include "config_HAVE_ZEBU_BUILT.hpp"
#include "config_HAVE_EXPERIMENTAL.hpp"


///STD include
#include <string>

///STL include
#include <unordered_map>

/**
 * Profiling analysis level (at which level data are aggregated
 */
typedef enum
{
   AL_NONE = 0,     /**< No analysis is performed */
   AL_ALL,          /**< Analysis performed at application level */
   AL_OS,           /**< Analysis performed at application level (system function excluded)*/
   AL_SOURCE,       /**< Analysis performed at application level (system and library function excluded)*/
   AL_COMPONENT,    /**< Analysis performed at component level (data of functions mapped on the same component are aggregated)*/
   AL_FUNCTION,     /**< Analysis performed at function level*/
   AL_LOOP          /**< Analysis performed at loop level*/
} AnalysisLevel;

#if HAVE_TO_C_BUILT
/**
 * C thread backend to be used
 */
typedef enum
{
#if HAVE_MPPB
   BA_MPPB,         /**< Mppb backend */
#endif
   BA_NONE,         /**< Plain backend */
#if HAVE_GRAPH_PARTITIONING_BUILT
   BA_OPENMP,       /**< Openmp backend */
   BA_PTHREAD       /**< Pthread backend */
#endif
} CThreadBackendType;
#endif

/**
 * target of the compiler
 */
typedef enum
{
   CT_NO_GCC = 0,
#if HAVE_I386_GCC45_COMPILER
   CT_I386_GCC45 = 1,
#endif
#if HAVE_I386_GCC46_COMPILER
   CT_I386_GCC46 = 2,
#endif
#if HAVE_I386_GCC47_COMPILER
   CT_I386_GCC47 = 4,
#endif
#if HAVE_I386_GCC48_COMPILER
   CT_I386_GCC48 = 8,
#endif
#if HAVE_I386_GCC49_COMPILER
   CT_I386_GCC49 = 16,
#endif
#if HAVE_ARM_COMPILER
   CT_ARM_GCC = 32,
#endif
#if HAVE_SPARC_COMPILER
   CT_SPARC_GCC = 64,
   CT_SPARC_ELF_GCC = 128
#endif
} CompilerTarget;

#if HAVE_DIOPSIS
/**
 * Instrumentation type used to profile diopsis
 */
typedef enum
{
   DI_GNAM,           /**< instrumentation based on gettimeofday function */
   DI_CPU_TIME,       /**< instrumentation based on gnam_linux_hw library */ 
   DI_HGET            /**< instrumentation based on hget_timer function */
} DiopsisInstrumentation;
#endif

/**
 * File formats
 */
typedef enum
{
   FF_UNKNOWN = 0,             /**< UNKNOWN */
#if HAVE_EXPERIMENTAL
   FF_CSV,                     /**< (Input) comma separated value */
   FF_CSV_RTL,                 /**< (Output) comma separated value rtl sequences */
   FF_CSV_TRE,                 /**< (Output) comma seperated value tree sequences */
#endif
#if HAVE_FROM_LIBERTY
   FF_LIB,                     /**< (Input) Liberty file */
#endif
#if HAVE_EXPERIMENTAL
   FF_LOG,                     /**< (Input) log file */
   FF_PA,                      /**< (Input) Profiling analysis */
#endif
#if HAVE_FROM_PSPLIB_BUILT
   FF_PSPLIB_MM,               /**< (Input) Multi-mode Project Scheduling Problem */
   FF_PSPLIB_SM,               /**< (Input) Single-mode Project Scheduling Problem */
#endif
   FF_TEX,                     /**< (Output) Latex table */
   FF_TGFF,                    /**< (Input) task graph for free */
   FF_VERILOG,                 /**< (Input) verilog */
   FF_VHDL,                    /**< (Input) vhdl */
   FF_XML,                     /**< (Input/Output) XML */
#if HAVE_REGRESSORS_BUILT
   FF_XML_AGG,                 /**< (Input) XML aggregated features */
#endif
#if HAVE_FROM_ARCH_BUILT
   FF_XML_ARCHITECTURE,        /**< (Input) XML architecture file */
#endif
#if HAVE_BAMBU_RESULTS_XML
   FF_XML_BAMBU_RESULTS,       /**< (Input) XML bambu results*/
#endif
#if HAVE_FROM_LIBERTY
   FF_XML_CELLS,               /**< (Input) XML describing list of cells */
#endif
#if HAVE_DESIGN_ANALYSIS_BUILT
   FF_XML_DESIGN_HIERARCHY,    /**< (Input) XML storing hierarchy of a design */
#endif
   FF_XML_IP_XACT_COMPONENT,   /**< (Input) XML storing IP-XACT component */
   FF_XML_IP_XACT_DESIGN,      /**< (Input) XML storing IP-XACT design */
   FF_XML_IP_XACT_GENERATOR,   /**< (Input) XML storing IP-XACT generator chain */
   FF_XML_IP_XACT_CONFIG,      /**< (Input) XML storing IP-XACT design configuration */
#if HAVE_FROM_SDF3_BUILT
   FF_XML_SDF3,                /**< (Input) XML storing synchronous data flow graph */
#endif
   FF_XML_SKIP_ROW,            /**< (Input) XML benchhmarks to be execluded from training set */
#if HAVE_SOURCE_CODE_STATISTICS_XML
   FF_XML_STAT,                /**< (Input) XML statistics about source code files */
#endif
   FF_XML_SYM_SIM,             /**< (Input) XML storing symbolic symulation results */
   FF_XML_TEX_TABLE,           /**< (Input) XML storing format of latex table to be produced */
   FF_XML_WGT_GM,              /**< (Output) XML weights of single operations computed */ 
   FF_XML_WGT_SYM,             /**< (Input/Output) XML symbolic weights */
} FileFormat;

#if HAVE_DESIGN_ANALYSIS_BUILT
/**
 * High level design flow steps
 */
typedef enum
{
   HLD_VERILOG_STRUCTURAL=1,            /**< Generate structural verilog */
   HLD_VERILOG_SPLIT=2,                 /**< Split HDL verilog in multiple files */
   HLD_ANALYZE_DC_LOG=4,                /**< Compute instances */
   HLD_IPXACT_INSTANTATION=8,           /**< Generate IP-XACT architecture instance */
   HLD_IPXACT_NORMALIZATION=16,         /**< Normalize name in IP-XACT xmls */
   HLD_IPXACT_CHARACTERIZATION=32,      /**< Characterize components */
   HLD_IPXACT_CANDIDATE_EXPLORATION=64, /**< Candidate exploration and identification */
   HLD_IPXACT_DESIGN_CONFIGURATION=128, /**< Generate design configuration */
   HLD_SCRIPT_GENERATION=256,           /**< Generate script for synthesis tools */
   HLD_IR_BUILDING=512,                 /**< Build graph based IR */
} DesignAnalysisStep;
#endif

/**
 * Profiling architecture
 */
typedef enum
{
#if HAVE_A3
   PA_A3,                 /**< absint a3 */
#endif
#if HAVE_DIOPSIS
   PA_ARM_DIOPSIS,        /**< arm of diopsis board */
#endif
#if HAVE_LEON3
   PA_LEON3,              /**< Leon3 board */
#endif
#if HAVE_MPPB
   PA_MPPB,               /**< MPPB - leon + dsps*/
   PA_MPPB_LEON,          /**< MPPB - leon*/
#endif
#if HAVE_SIMIT
   PA_SIMIT,              /**< SimIt simulator */
#endif
#if HAVE_TSIM
   PA_TSIM_BREAK,         /**< TSIM simulator based on brakpoint */
   PA_TSIM_CLOCK,         /**< TSIM simulator based on clock */
   PA_TSIM_PROF,          /**< TSIM simulator based on prof */
   PA_TSIM_SINGLE_PROF,   /**< TSIM simulator based on prof with sampling period fixed to 1*/
   PA_TSIM_TRACE,         /**< TSIM simulator based on trace */
#endif
   PA_UNKNOWN,            /**< Unknown */
   PA_LAST
} ProfilingArchitectureKind;

/**
 * Algorithm used to estimate the performance of applications and tasks
 */
typedef enum
{
   PE_NONE = 0,        /** No algorithm selected */
   PE_WORST_CASE = 2,  /** Worst case execution time for each task */
   PE_PATH_BASED = 4,  /** Path based */
   PE_AVERAGE_CASE = 8 /** Average case execution time for each task */
} PerformanceEstimationAlgorithm;

/**
 * The name of the performance estimation algorithms
 */
static
std::string performance_estimation_algorithm_names[] = {"None", "", "Worst_Case", "", "Path_Based", "", "", "", "Average_Case"};

/**
 * Definition of hash function for PerformanceEstimationAlgorithm
 */
namespace std
{
   template <>
      struct hash<PerformanceEstimationAlgorithm> : public unary_function<PerformanceEstimationAlgorithm, size_t>
      {
         size_t operator()(PerformanceEstimationAlgorithm algorithm) const
         {
            hash<int> hasher;
            return hasher(static_cast<int>(algorithm));
         }
      };
}

/// Different profiling method
typedef enum
{
   PM_NONE                =  0, /**< None profiling method selected */
   PM_HPP                 =  1, /**< Hierarchical Path Profiling */
   PM_TP                  =  2, /**< Tree based Path Profiling */
   PM_MAX_LOOP_ITERATIONS =  4, /**< Maximum number of iteration profiling */
   PM_PATH_PROBABILITY    =  8, /**< Probability based path */
   PM_XML_FILE            = 16  /**< Data read from XML file */
} ProfilingMethod;

#if HAVE_FROM_C_BUILT
///The access type to a global variable
typedef enum
{
   AT_UNKNOWN=0,
   AT_ADDRESS,
   AT_USE,
   AT_DEF
} VariableAccessType;

/**
 * Definition of hash function for VariableAccessType
 */
namespace std
{
   template <>
      struct hash<VariableAccessType> : public unary_function<VariableAccessType, size_t>
      {
         size_t operator()(VariableAccessType variable_access_type) const
         {
            hash<int> hasher;
            return hasher(static_cast<int>(variable_access_type));
         }
      };
}
#endif

#endif
