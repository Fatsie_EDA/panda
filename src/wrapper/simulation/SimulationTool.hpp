/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file SimulationTool.hpp
 * @brief Abstract class for a generic simulation tool
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef _SIMULATION_TOOL_HPP_
#define _SIMULATION_TOOL_HPP_

#include "refcount.hpp"
CONSTREF_FORWARD_DECL(Parameter);
REF_FORWARD_DECL(SimulationTool);

#include <string>

class SimulationTool
{
   public:

      ///supported synthesis tools
      typedef enum
      {
         UNKNOWN = 0,
         MODELSIM,
         ISIM,
         XSIM,
         ICARUS,
         VERILATOR
      } type_t;

   protected:

      ///class containing all the parameters
      const ParameterConstRef Param;

      ///debug level of the class
      int debug_level;

      ///verbosity level of the class
      unsigned int output_level;

      ///generated script
      std::string generated_script;

      ///log file
      std::string log_file;

      /**
       * Performs the actual writing
       */
      virtual void GenerateScript(std::ostringstream& script, const std::string& top_filename, const std::string& FileList) = 0;

   public:

      /**
       * Constructor
       */
      SimulationTool(const ParameterConstRef Param);

      /**
       * Destructor
       */
      virtual ~SimulationTool();

      /**
       * Factory method
       */
      static
      SimulationToolRef CreateSimulationTool(type_t type, const ParameterConstRef Param, const std::string& suffix);

      /**
       * Checks if the current specification can be executed or not
       */
      virtual void CheckExecution();

      /**
       * Generates the proper simulation script
       */
      virtual std::string GenerateSimulationScript(const std::string& top_filename, const std::string& FileList);

      /**
       * Performs the simulation and returns the number of cycles
       */
      virtual unsigned long long int Simulate();

      /**
       * Determines the average number of cycles for the simulation(s)
       */
      unsigned long long int DetermineCycles();

};
///refcount definition of the class
typedef refcount<SimulationTool> SimulationToolRef;
#endif
