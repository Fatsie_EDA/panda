/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file SimulationTool.cpp
 * @brief Implementation of some methods for the interface with simulation tools
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Date$
 * Last modified by $Author$
 *
*/
#include "SimulationTool.hpp"

#include "ToolManager.hpp"

#include "modelsimWrapper.hpp"
#include "ISE_isim_wrapper.hpp"
#include "VIVADO_xsim_wrapper.hpp"
#include "IcarusWrapper.hpp"
#include "VerilatorWrapper.hpp"

#include "Parameter.hpp"
#include "fileIO.hpp"

SimulationTool::SimulationTool(const ParameterConstRef _Param) :
   Param(_Param),
   debug_level(Param->getOption<int>(OPT_debug_level)),
   output_level(Param->getOption<unsigned int>(OPT_output_level))
{

}

SimulationTool::~SimulationTool()
{

}

SimulationToolRef SimulationTool::CreateSimulationTool(type_t type, const ParameterConstRef _Param, const std::string& suffix)
{
   switch (type)
   {
      case UNKNOWN:
         THROW_ERROR("Simulation tool not specified");
      case MODELSIM:
         return SimulationToolRef(new modelsimWrapper(_Param, suffix));
      case ISIM:
         return SimulationToolRef(new ISE_isim_wrapper(_Param, suffix));
      case XSIM:
         return SimulationToolRef(new VIVADO_xsim_wrapper(_Param, suffix));
      case ICARUS:
         return SimulationToolRef(new IcarusWrapper(_Param, suffix));
      case VERILATOR:
         return SimulationToolRef(new VerilatorWrapper(_Param, suffix));
      default:
         THROW_ERROR("Simulation tool currently not supported");
   }
   ///this point should never be reached
   return SimulationToolRef();
}

void SimulationTool::CheckExecution()
{
   ///for default, nothing to do
}

unsigned long long int SimulationTool::Simulate()
{
   if (generated_script.size()==0)
      THROW_ERROR("Simulation script not yet generated");

   /// remove previous simulation results
   std::string result_file = Param->getOption<std::string>(OPT_simulation_output);
   if (boost::filesystem::exists(result_file))
      boost::filesystem::remove_all(result_file);
   ToolManagerRef tool(new ToolManager(Param));
   tool->configure("./" + generated_script, "");
   std::vector<std::string> parameters, input_files;
   tool->execute(parameters, input_files);

   if(log_file.size() != 0 && output_level == OUTPUT_LEVEL_VERBOSE)
      CopyStdout(log_file);

   return DetermineCycles();
}

unsigned long long int SimulationTool::DetermineCycles()
{
   unsigned long long int num_cycles = 0;
   unsigned int i = 0;
   std::string result_file = Param->getOption<std::string>(OPT_simulation_output);
   if (!boost::filesystem::exists(result_file)) THROW_ERROR("The simulation did not correctly produce the results");
   std::ifstream res_file(result_file.c_str());
   if (res_file.is_open())
   {
      PRINT_OUT_MEX(OUTPUT_LEVEL_PEDANTIC, output_level, "File \"" + result_file + "\" opened");
      while(!res_file.eof())
      {
         std::string line;
         getline (res_file, line);
         if (!line.size()) continue;
         std::vector<std::string> filevalues;
         boost::algorithm::split(filevalues, line, boost::algorithm::is_any_of("\t"));
         boost::trim(filevalues[0]);
         boost::trim(filevalues[1]);
         if (filevalues[0] == "X")
         {
            CopyStdout(log_file);
            THROW_ERROR("Simulation not terminated!");
         }
         else if (filevalues[0] == "0")
         {
            CopyStdout(log_file);
            THROW_ERROR("Simulation not correct!");
         }
         else if (filevalues[0] == "-")
         {
            THROW_WARNING("Simulation completed but it is not possible to determine if it is correct!");
         }
         else if (filevalues[0] != "1")
         {
            CopyStdout(log_file);
            THROW_ERROR("String not valid: " + line);
         }
         unsigned long long int sim_cycles = boost::lexical_cast<unsigned long long int>(filevalues[1]);
         PRINT_OUT_MEX(OUTPUT_LEVEL_VERBOSE, output_level, (i+1) << ". Simulation completed with SUCCESS; Execution time " << sim_cycles << " cycles;");
         num_cycles += sim_cycles;
         i++;
      }
   }
   else
   {
      CopyStdout(log_file);
      THROW_ERROR("Result file not correctly created");
   }

   THROW_ASSERT(i!= 0, "Expected a number of cycles different from zero. Something of wrong happen during the simulation!");
   return num_cycles/i;
}

std::string SimulationTool::GenerateSimulationScript(const std::string& top_filename, const std::string& FileList)
{
   std::ostringstream script;
   script << "#!/bin/bash" << std::endl;
   script << "##########################################################" << std::endl;
   script << "#     Automatically generated by the PandA framework     #" << std::endl;
   script << "##########################################################" << std::endl << std::endl;
   script << "# COMPONENT: " << top_filename << std::endl << std::endl;

   GenerateScript(script, top_filename, FileList);

   if(log_file.size() != 0 && output_level >= OUTPUT_LEVEL_VERBOSE)
   {
      script << "cat " << log_file << std::endl << std::endl;
   }

   //Create the simulation script
   generated_script = std::string("simulate_") + top_filename + std::string(".sh");
   std::ofstream file_stream(generated_script.c_str());
   file_stream << script.str() << std::endl;
   file_stream.close();

   ToolManagerRef tool(new ToolManager(Param));
   tool->configure("chmod", "");
   std::vector<std::string> parameters, input_files;
   parameters.push_back("+x");
   parameters.push_back(generated_script);
   input_files.push_back(generated_script);
   tool->execute(parameters, input_files);

   return generated_script;
}
