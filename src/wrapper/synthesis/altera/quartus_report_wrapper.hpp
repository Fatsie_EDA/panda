/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file quartus_report_wrapper.hpp
 * @brief Wrapper to invoke quartus_report tool by Altera
 *
 * A object used to invoke quartus_report tool by Altera
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef _ALTERA_QUARTUS_REPORT_HPP_
#define _ALTERA_QUARTUS_REPORT_HPP_

#include "AlteraWrapper.hpp"

#define QUARTUS_REPORT_TOOL_ID    "quartus_sta"
#define QUARTUS_REPORT_TOOL_EXEC  "quartus_sta"

/**
 * @class quartus_report_wrapper
 * Main class for wrapping quartus_report tool by Altera
 */
class quartus_report_wrapper : public AlteraWrapper
{
   protected:

      /**
       * Evaluates the design variables
       */
      void EvaluateVariables(const DesignParametersRef dp);

   public:

      /**
       * Constructor
       * @param Param is the set of parameters
       */
      quartus_report_wrapper(const ParameterConstRef Param, const std::string& _output_dir, const target_deviceRef _device);

      /**
       * Destructor
       */
      virtual ~quartus_report_wrapper();

};
///Refcount definition for the class
typedef refcount<quartus_report_wrapper> quartus_report_wrapperRef;

#endif
