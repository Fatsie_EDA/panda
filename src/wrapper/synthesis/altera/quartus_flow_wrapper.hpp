/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file quartus_flow_wrapper.hpp
 * @brief Wrapper to invoke quartus_flow tool by Altera
 *
 * A object used to invoke quartus_flow tool by Altera
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * @author Christian Pilato <pilato@elet.polimi.it>
 * $Date$
 * Last modified by $Author$
 *
*/
#ifndef _ALTERA_QUARTUS_FLOW_HPP_
#define _ALTERA_QUARTUS_FLOW_HPP_

#include "AlteraWrapper.hpp"

#define QUARTUS_SETUP_TOOL_ID     "quartus_setup"
#define QUARTUS_FLOW_TOOL_ID      "quartus_flow"
#define QUARTUS_FLOW_TOOL_EXEC    "quartus_sh"

/**
 * @class quartus_flow_wrapper
 * Main class for wrapping quartus_flow tool by Altera
 */
class quartus_flow_wrapper : public AlteraWrapper
{
   protected:

      /**
       * Evaluates the design variables
       */
      void EvaluateVariables(const DesignParametersRef dp);

   public:

      /**
       * Constructor
       * @param Param is the set of parameters
       */
      quartus_flow_wrapper(const ParameterConstRef Param, const std::string& _output_dir, const target_deviceRef _device);

      /**
       * Destructor
       */
      virtual ~quartus_flow_wrapper();

};
///Refcount definition for the class
typedef refcount<quartus_flow_wrapper> quartus_flow_wrapperRef;

#endif
