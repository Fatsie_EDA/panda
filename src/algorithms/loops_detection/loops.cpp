/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file loops.cpp
 * @brief implementation of loops finding algorithm
 *
 * Loops are detection described in
 * in Vugranam C. Sreedhar, Guang R. Gao, Yong-Fong Lee: Identifying Loops Using DJ Graphs. ACM Trans. Program. Lang. Syst. 18(6): 649-658 (1996)
 *
 * @author Marco Garatti <m.garatti@gmail.com>
 * @author Marco Lattuada <lattuada@elet.polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/
///Header include
#include "loops.hpp"
#include "loop.hpp"

///Autoheader include
#include "config_HAVE_HOST_PROFILING_BUILT.hpp"

///Algorithms include
#include "Dominance.hpp"

///Behavior include
#include "basic_block.hpp"
#include "basic_blocks_graph_constructor.hpp"
#include "behavioral_writer_helper.hpp"
#include "function_behavior.hpp"

#if HAVE_HOST_PROFILING_BUILT
#include "profiling_information.hpp"
#endif

///Graph include
#include "graph.hpp"
#include "Vertex.hpp"

///Parameter include
#include "Parameter.hpp"

///STL include
#include <list>
#include <map>

///STD Include
#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>
#include <utility>

///Tree include
#include "behavioral_helper.hpp"
#include "tree_basic_block.hpp"

///Utility include
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/reverse_graph.hpp>
#include <boost/graph/transitive_closure.hpp>
#include <boost/graph/visitors.hpp>
#include <boost/lexical_cast.hpp>
#include "dbgPrintHelper.hpp"
#include "exceptions.hpp"
#include "simple_indent.hpp"
#include "utility.hpp"

/**
 * Visitor for the dfs of the DJ graph.
 */
struct djgraph_dfs_tree_visitor : public boost::default_dfs_visitor
{
   private:

      ///topological level of the vertices
      std::unordered_map<vertex, int> & vertex_level;

      ///dfs order of vertices
      std::unordered_map<vertex, int> & dfs_order;

      /// ???
      int dfs_number;

      ///maximum level of vertices
      int & max_level;

      ///???
      vertex2obj<vertex> &parent_depth_search_spanning_tree;

   public:

      /**
       * Constructor of the postorder tree visitor. Used to label the node in postorder way.
       * @param vertex_level will store levels of vertices
       * @param dfs_order will store vertices order
       * @param max_level will store the max level of vertices
       * @param parent_depth_search_spanning_tree is ???
       */
      djgraph_dfs_tree_visitor
      (
         std::unordered_map<vertex, int> & _vertex_level,
         std::unordered_map<vertex, int> & _dfs_order,
         int & _max_level,
         vertex2obj<vertex> &_parent_depth_search_spanning_tree
      )
      : vertex_level(_vertex_level),
         dfs_order(_dfs_order),
         dfs_number(0),
         max_level(_max_level),
         parent_depth_search_spanning_tree(_parent_depth_search_spanning_tree)
      {
      }

      template <class Vertex, class Graph>
      void discover_vertex(Vertex v, Graph& g)
      {
         // Set the dfs order
         dfs_order[v] = dfs_number++;
         // and the vertex level
         if (v == g.CGetBBGraphInfo()->entry_vertex)
         {
            vertex_level[v] = 0;
         }
         else
         {
            // The level of at least one predecessor must have been set already
            typename boost::graph_traits<Graph>::in_edge_iterator ei, ei_end;
            int new_level = 0;
            for (boost::tie(ei, ei_end) = boost::in_edges(v, g) ; ei != ei_end ; ++ei )
            {
               vertex source = boost::source(*ei, g);
               if (vertex_level.find(source) != vertex_level.end())
               {
                  int source_level = vertex_level[source];
                  if (new_level == 0)
                  {
                     new_level = source_level + 1;
                  }
                  else
                  {
                     if (source_level + 1 < new_level)
                     {
                        new_level = source_level + 1;
                     }
                  }
               }
            }
            THROW_ASSERT(new_level > 0, "Cannot determine a proper vertex level");
            vertex_level[v] = new_level;
            if (new_level > max_level)
            {
               max_level = new_level;
            }
         }
      }

      template <class Edge, class Graph>
      void tree_edge(Edge e, const Graph& g) const
      {
         vertex src, tgt;
         src = boost::source(e, g);
         tgt = boost::target(e, g);
         parent_depth_search_spanning_tree[tgt] = src;
      }
};


Loops::Loops()
{
   THROW_UNREACHABLE("Empty constructor should never be called");
}

Loops::Loops(const FunctionBehaviorRef _FB, const ParameterConstRef parameters) :
   FB(_FB),
   Param(parameters),
   debug_level(parameters->get_class_debug_level(GET_CLASS(*this), DEBUG_LEVEL_NONE))
{
   // Detect loops
   DetectLoops();
   ///FIXME: maybe this function can be integrated in the loop detection
   BuildZeroLoop();
   computeDepth(GetLoop(0));
   std::list<LoopRef>::const_iterator loop, loop_end = modifiable_loops_list.end();
   for(loop = modifiable_loops_list.begin(); loop != loop_end; loop++)
   {
      (*loop)->ComputeLandingPadExits();
   }
}

bool Loops::is_edge_in_list(std::list<vertex_pair>& l, vertex source, vertex target)
{
   std::list<vertex_pair>::iterator list_iter = l.begin();
   std::list<vertex_pair>::iterator list_end  = l.end();

   for (; list_iter != list_end; ++list_iter)
   {
      vertex_pair source_target = *list_iter;
      if (source == source_target.first && target == source_target.second)
      {
         return true;
      }
   }
   return false;
}

static
bool check_ancestor(vertex x, vertex y, const vertex2obj<vertex> &parent_depth_search_spanning_tree)
{
   return ((x != y && y == parent_depth_search_spanning_tree(x)) || (x != parent_depth_search_spanning_tree(x) && check_ancestor(parent_depth_search_spanning_tree(x), y, parent_depth_search_spanning_tree)));
}

void Loops::DetectLoops()
{
   const BasicBlocksGraphConstructorRef bbcg = this->FB.lock()->bbgc;
   const dominance<BBGraph> * dom = this->FB.lock()->dominators;
   const BBGraphRef  cfg = this->FB.lock()->GetBBGraph(FunctionBehavior::BB);
   const BehavioralHelperConstRef helper = this->FB.lock()->CGetBehavioralHelper();
   const std::string function_name = helper->get_function_name();
   PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Starting Loops Detection of " + function_name);

   // Compute dominators
   const vertex entry = cfg->CGetBBGraphInfo()->entry_vertex;

   // Create the DJ graph. This is done in two steps:
   // 1) build the dominator tree
   // 2) add the "J" edges

   EdgeIterator ei, ei_end;
   // Add J edges (and detect CJ ones)
   const std::unordered_map<vertex, std::set<vertex> > full_dom = dom->getAllDominated();
   std::list<vertex_pair> cj_edges;
   std::list<vertex_pair> bj_edges;
   for (boost::tie(ei, ei_end) = boost::edges(*cfg); ei != ei_end ; ++ei )
   {
      vertex source = boost::source(*ei, *cfg);
      vertex target = boost::target(*ei, *cfg);

      if (source != dom->get_immediate_dominator(target))
      {
         bbcg->AddEdge(source, target, J_SELECTOR);

         // Check for CJ and BJ edges
         bool cj;
         if (full_dom.find(target) == full_dom.end())
         {
            cj = true;
         }
         else
         {
            const std::unordered_map<vertex, std::set<vertex> >::const_iterator dom_set_iter = full_dom.find(target);
            const std::set<vertex> dom_set = (*dom_set_iter).second;
            if (dom_set.find(source) != dom_set.end())
            {
               // This is a BJ edge, and we mark it as such
               cj = false;
            }
            else
            {
               // This is a CJ edge, and we mark it as such
               cj = true;
            }
         }
         if (cj)
         {
            std::pair<vertex, vertex> cj_edge(source, target);
            cj_edges.push_back(cj_edge);
         }
         else
         {
            std::pair<vertex, vertex> bj_edge(source, target);
            bj_edges.push_back(bj_edge);
         }
      }
   }

   const BBGraphRef  djg = FB.lock()->GetBBGraph(FunctionBehavior::DJ);

   if (Param->getOption<bool>(OPT_print_dot))
   {
      djg->WriteDot("BB_DJ.dot");
   }

   // Build the spanning tree and detect the sp-back edges
   std::unordered_map<vertex, int> vertex_level;
   std::unordered_map<vertex, int> dfs_order;
   int num_level = 0;
   ///spanning tree ancestor data structure
   vertex2obj<vertex> parent_depth_search_spanning_tree;
   ///initialization
   parent_depth_search_spanning_tree[entry] = entry;

   std::vector<boost::default_color_type> color_vec(boost::num_vertices(*djg));
   djgraph_dfs_tree_visitor vis(vertex_level, dfs_order, num_level, parent_depth_search_spanning_tree);
   boost::depth_first_visit(*djg, entry, vis, boost::make_iterator_property_map(color_vec.begin(), boost::get(boost::vertex_index, *djg), boost::white_color));

   // Detect the sp-edges
   std::list<vertex_pair> sp_edges;
   for (boost::tie(ei, ei_end) = boost::edges(*djg); ei != ei_end ; ++ei )
   {
      vertex source = boost::source(*ei, *djg);
      vertex target = boost::target(*ei, *djg);

      if (source == target || check_ancestor(source, target, parent_depth_search_spanning_tree))
      {
         // This is a sp-back edge
         std::pair<vertex, vertex> sp_edge(source, target);
         sp_edges.push_back(sp_edge);
      }
   }

   for (int i = num_level; i >= 0; --i)
   {
      PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Iteration " + boost::lexical_cast<std::string>(i));
      bool irriducible = false;
      std::unordered_map<vertex, int>::iterator vertex_level_iter = vertex_level.begin();
      std::unordered_map<vertex,  int>::iterator vertex_level_end  = vertex_level.end();
      for (; vertex_level_iter != vertex_level_end; ++vertex_level_iter)
      {
         PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Considering BB" + boost::lexical_cast<std::string>(cfg->CGetBBNodeInfo(vertex_level_iter->first)->block->number));
         int vl = (*vertex_level_iter).second;
         if (vl != i)
            continue;
         vertex v = (*vertex_level_iter).first;
         graph::in_edge_iterator e_iter, e_iter_end;
         const LoopRef loop = LoopRef(new Loop(cfg, v));
         bool reducible = false;
         std::set<vertex> visited;
         for (boost::tie(e_iter, e_iter_end) = boost::in_edges(v, *djg) ; e_iter != e_iter_end ; ++e_iter )
         {
            vertex m = boost::source(*e_iter, *djg);
            vertex n = boost::target(*e_iter, *djg);
            if (is_edge_in_list(cj_edges, m, n) && is_edge_in_list(sp_edges, m, n))
            {
               irriducible = true;
            }
            if (is_edge_in_list(bj_edges, m, n))
            {
               reducible = true;
               // Detect this Loop (n is the header)
               block_to_loop[n] = loop;
               DetectReducibleLoop(djg, visited, loop, m, n);

               PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Found a reducible Loop");
            }
         }
         if(reducible)
         {
            modifiable_loops_list.push_back(loop);
            const_loops_list.push_back(loop);
         }
      }
      if (irriducible)
      {
         // Identify SCCs for the subgraphs induced by nodes at level >= i
         DetectIrreducibleLoop(djg, i, vertex_level);
      }
   }
   PRINT_DBG_MEX(DEBUG_LEVEL_PEDANTIC, debug_level, "Ended Loops Detection of " + function_name);
}

void Loops::DetectReducibleLoop(const BBGraphRef djg, std::set<vertex> &visited, LoopRef l, vertex real_node, vertex header)
{
   graph::in_edge_iterator e_iter, e_iter_end;

   if (real_node == header)
      return;
   visited.insert(real_node);

   if (block_to_loop.find(real_node) == block_to_loop.end())
   {
      // Update the loop
      l->add_block(real_node);
      // Update the block to loop map
      block_to_loop[real_node] = l;
   }
   else if (block_to_loop[real_node] != l)
   {
      // This block belongs to another loop therefore we have a nesting here and we should not
      // add this block to the outermost
      LoopRef innermost_loop = LoopRef(block_to_loop[real_node]);
      if (innermost_loop->Parent() == nullptr)
      {
         l->AddChild(innermost_loop);
         innermost_loop->SetParent(l);
      }
   }
   // else the block already belongs to this loop. Nothing to be done

   // Note that since this is a reducible loop, we cannot have any edge entering the loop
   // body other then the predecessors of the loop header
   for (boost::tie(e_iter, e_iter_end) = boost::in_edges(real_node, *djg) ; e_iter != e_iter_end ; ++e_iter )
   {
      vertex source = boost::source(*e_iter, *djg);
      // In order to avoid infinite recursion, we use dfs order to visit nodes only once
      if (visited.find(source) == visited.end())
         DetectReducibleLoop(djg, visited, l, source, header);
   }
}

void Loops::DetectIrreducibleLoop(const BBGraphRef djg, int min_level, std::unordered_map<vertex, int>& vertex_level)
{
   std::unordered_map<vertex, unsigned int> dfs_order;
   graph::vertex_iterator v_iter, v_iter_end;

   std::list<vertex> s;
   std::unordered_map<vertex, unsigned int> lowlink;
   std::set<vertex> u;

   // Popoulate u with all the non-visisted node whose level is >= min_level
   for (boost::tie(v_iter, v_iter_end) = boost::vertices(*djg); v_iter != v_iter_end; ++v_iter)
   {
      vertex v = *v_iter;
      if (vertex_level[v] >= min_level)
      {
         u.insert(v);
      }
   }

   THROW_ASSERT(u.size() > 0, "There must be at least one item in u");
   unsigned int max_dfs = 0;
   tarjan_scc(djg, *(u.begin()), dfs_order, lowlink, s, u, max_dfs);
}

bool Loops::stack_contains(std::list<vertex> stack, vertex v)
{
   std::list<vertex>::iterator stack_iter = stack.begin();
   std::list<vertex>::iterator stack_end  = stack.end();

   for (; stack_iter != stack_end; ++stack_iter)
   {
      vertex current_node = *stack_iter;
      if (current_node == v)
         return true;
   }
   return false;
}

void Loops::tarjan_scc(const BBGraphRef djg, vertex v,
                       std::unordered_map<vertex, unsigned int>& dfs_order,
                       std::unordered_map<vertex, unsigned int>& lowlink,
                       std::list<vertex>& s,
                       std::set<vertex> u,
                       unsigned int& max_dfs)
{
   dfs_order[v] = max_dfs++;
   lowlink[v] = dfs_order[v];
   s.push_back(v);
   u.erase(v);

   graph::out_edge_iterator e_out_iter, e_out_iter_end;
   for (boost::tie(e_out_iter, e_out_iter_end) = boost::out_edges(v, *djg) ; e_out_iter != e_out_iter_end ; ++e_out_iter )
   {
      vertex target = boost::target(*e_out_iter, *djg);

      if (u.find(target) != u.end())
      {
         tarjan_scc(djg, target, dfs_order, lowlink, s, u, max_dfs);
         lowlink[v] = lowlink[v] < lowlink[target] ? lowlink[v] : lowlink[target];
      }
      else if (stack_contains(s, target))
      {
         lowlink[v] = lowlink[v] < dfs_order[target] ? lowlink[v] : dfs_order[target];
      }
   }

   //FIXME: should this be a comparison? Then changed by FF
   if (lowlink[v] == dfs_order[v])
   {
      // v is the root of a strongly connectec component
      LoopRef l = LoopRef(new Loop(djg));
      vertex v1;
      do
      {
         v1 = s.back();
         s.pop_back();
         // Add v1 to the loop
         vertex real_node;

         real_node = v1;
         l->add_block(real_node);
      }
      while (v1 != v);
      modifiable_loops_list.push_back(l);
      const_loops_list.push_back(l);
   }
}

const std::list<LoopConstRef> & Loops::GetList() const
{
   return const_loops_list;
}

const std::list<LoopRef> & Loops::GetModifiableList() const
{
   return modifiable_loops_list;
}

const LoopConstRef Loops::CGetLoop(unsigned int id) const
{
   std::list<LoopConstRef>::const_iterator it, it_end;
   it_end = const_loops_list.end();
   for(it = const_loops_list.begin(); it != it_end; it++)
      if((*it)->GetId() == id)
         return *it;
   THROW_UNREACHABLE("Loop with id " + boost::lexical_cast<std::string>(id) + " doesn't exist");
   return LoopConstRef();
}

const LoopRef Loops::GetLoop(unsigned int id)
{
   std::list<LoopRef>::const_iterator it, it_end;
   it_end = modifiable_loops_list.end();
   for(it = modifiable_loops_list.begin(); it != it_end; it++)
      if((*it)->GetId() == id)
         return *it;
   THROW_UNREACHABLE("Loop with id " + boost::lexical_cast<std::string>(id) + " doesn't exist");
   return LoopRef();
}

size_t Loops::NumLoops() const
{
   return const_loops_list.size();
}

void Loops::WriteDot(const std::string & file_name
#if HAVE_HOST_PROFILING_BUILT
   ,const ProfilingInformationConstRef profiling_information
#endif
) const
{
   std::string output_directory = Param->getOption<std::string>(OPT_dot_directory);
   output_directory += FB.lock()->CGetBehavioralHelper()->get_function_name() + "/";
   const BBGraphRef  cfg = this->FB.lock()->GetBBGraph(FunctionBehavior::BB);
   std::ofstream dot((output_directory + file_name).c_str());
   dot << "digraph LoopForest {" << std::endl;
   for(std::list<LoopConstRef>::const_iterator loop = const_loops_list.begin(); loop != const_loops_list.end(); loop++)
   {
      dot << (*loop)->GetId() << " [label=\"LoopId=" << (*loop)->GetId() << " - Depth: " << (*loop)->depth;
#if HAVE_HOST_PROFILING_BUILT
      if(profiling_information)
         dot << "\\nAvg. Iterations=" << profiling_information->GetLoopAvgIterations((*loop)) << "- Max Iterations=" << profiling_information->GetLoopMaxIterations((*loop)->GetId());
#endif
      dot << "\\nBlocks:";
      const std::unordered_set<vertex> & blocks = (*loop)->get_blocks();
      std::unordered_set<vertex>::const_iterator bb, bb_end = blocks.end();
      for(bb = blocks.begin(); bb != bb_end; bb++)
      {
         dot << " BB" + boost::lexical_cast<std::string>(cfg->CGetBBNodeInfo(*bb)->block->number);
      }
      dot << "\\n\"];" << std::endl;
   }
   for(std::list<LoopConstRef>::const_iterator loop = const_loops_list.begin(); loop != const_loops_list.end(); loop++)
   {
      const std::set<LoopConstRef> child = (*loop)->GetChildren();
      for(std::set<LoopConstRef>::iterator c = child.begin(); c != child.end(); c++)
      {
         dot << (*loop)->GetId() << "->" << (*c)->GetId() << ";" << std::endl;
      }
   }
   dot << "}" << std::endl;
}

void Loops::computeDepth(const LoopConstRef loop)
{
   const std::set<LoopConstRef> children = loop->GetChildren();
   std::set<LoopConstRef>::const_iterator child, child_end = children.end();
   for(child = children.begin(); child != child_end; child++)
   {
      Loop * child_loop = const_cast<Loop *>(child->get());
      child_loop->depth = loop->depth + 1;
      computeDepth(*child);
   }
}

void Loops::BuildZeroLoop()
{
   const BBGraphRef  cfg = this->FB.lock()->GetBBGraph(FunctionBehavior::BB);
   const LoopRef zero_loop(new Loop(cfg, cfg->CGetBBGraphInfo()->entry_vertex));
   ///Putting all the basic blocks in blocks
   VertexIterator v, v_end;
   for(boost::tie(v, v_end) = boost::vertices(*cfg); v != v_end; v++)
   {
      zero_loop->blocks.insert(*v);
   }

   if(const_loops_list.size())
      zero_loop->is_innermost_loop = false;
   else
      zero_loop->is_innermost_loop = true;

   std::list<LoopRef>::iterator loop, loop_end = modifiable_loops_list.end();
   for(loop = modifiable_loops_list.begin(); loop != loop_end; loop++)
   {
      if(not (*loop)->Parent())
      {
         (*loop)->parent_loop = zero_loop;
         zero_loop->children.insert(*loop);
         std::unordered_set<vertex> children_blocks;
         (*loop)->get_recursively_bb(children_blocks);
         std::unordered_set<vertex>::const_iterator child_block, child_block_end = children_blocks.end();
         for(child_block = children_blocks.begin(); child_block != child_block_end; child_block++)
         {
            if(zero_loop->blocks.find(*child_block) != zero_loop->blocks.end())
               zero_loop->blocks.erase(zero_loop->blocks.find(*child_block));
         }
      }
   }

   zero_loop->header_block = cfg->CGetBBGraphInfo()->entry_vertex;
   modifiable_loops_list.push_front(zero_loop);
   const_loops_list.push_front(zero_loop);
}
