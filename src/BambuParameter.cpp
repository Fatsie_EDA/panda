/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file BambuParameter.cpp
 * @brief This file contains the implementation of some methods for parameter parsing in Bambu tool
 *
 * @author Christian Pilato <pilato@elet.polimi.it>
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
*/

///Autoheader
#include "config_HAVE_ALTERA.hpp"
#include "config_HAVE_BEAGLE.hpp"
#include "config_HAVE_COIN_OR.hpp"
#include "config_HAVE_CUDD.hpp"
#include "config_HAVE_EXPERIMENTAL.hpp"
#include "config_HAVE_ICARUS.hpp"
#include "config_HAVE_ILP_BUILT.hpp"
#include "config_HAVE_LP_SOLVE.hpp"
#include "config_HAVE_MODELSIM.hpp"
#include "config_HAVE_XILINX.hpp"
#include "config_HAVE_XILINX_VIVADO.hpp"
#include "config_SKIP_WARNING_SECTIONS.hpp"
#include "config_HAVE_MAPPING_BUILT.hpp"
#include "config_HAVE_VCD_BUILT.hpp"
#include "config_HAVE_I386_GCC45_COMPILER.hpp"
#include "config_HAVE_I386_GCC46_COMPILER.hpp"
#include "config_HAVE_I386_GCC47_COMPILER.hpp"
#include "config_HAVE_I386_GCC48_COMPILER.hpp"
#include "config_HAVE_I386_GCC49_COMPILER.hpp"
#include "config_HAVE_FLOPOCO.hpp"
#include "config_HAVE_LIBRARY_CHARACTERIZATION_BUILT.hpp"
#include "config_HAVE_VERILATOR.hpp"

///Header include
#include "BambuParameter.hpp"

///Constants include
#include "constants.hpp"

///HLS include
#include "evaluation.hpp"
#include "hls_constraints.hpp"
#if HAVE_ILP_BUILT
#include "meilp_solver.hpp"
#endif
#include "memory_allocation.hpp"
#include "scheduling.hpp"
#include "parametric_list_based.hpp"
#include "STG_creator.hpp"
#include "liveness_computer.hpp"
#include "storage_value_insertion.hpp"
#include "reg_binding_creator.hpp"
#include "fu_binding_creator.hpp"
#include "conn_binding_creator.hpp"
#include "module_interface.hpp"
#include "export_core.hpp"

///creation of the target architecture
#include "controller_creator.hpp"
#include "datapath_creator.hpp"
#include "chaining.hpp"

#include "hls_synthesis_flow.hpp"
#include "hls_flow.hpp"
#if HAVE_BEAGLE
#if SKIP_WARNING_SECTIONS
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Woverloaded-virtual"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wold-style-cast"
#pragma GCC diagnostic ignored "-Wextra"
#pragma GCC diagnostic ignored "-Wignored-qualifiers"
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wsign-promo"
#endif
#include "FitnessFunction.hpp"
#include "dse_hls.hpp"
#endif

///Constant option include
#include "constant_strings.hpp"

///STD include
#include <cstring>
#include <iostream>
#include <string.h>

///Technology include
#include "language_writer.hpp"
#include "parse_technology.hpp"
#include "technology_manager.hpp"
#include "target_device.hpp"
#include "technology_builtin.hpp"

///Utility include
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include "cpu_time.hpp"
#include "dbgPrintHelper.hpp"
#include "fileIO.hpp"
#include <getopt.h>
#include "global_enums.hpp"
#include "utility.hpp"

///Wrapper include
#include "gcc_wrapper.hpp"

///=========== PARAMETERS STUFF =================///
///Design Space Exploration
#define OPT_DSE 284
#define OPT_MAX_EVALUATIONS 256
//ACO
#define OPT_ACO_FLOW 257
#define OPT_ACO_GENERATIONS 258
//Genetic Algorithm
#if HAVE_BEAGLE
#define OPT_MAX_INHERITANCE 259
#define OPT_MIN_INHERITANCE 260
#define OPT_GENERATION 261
#define OPT_TIME_WEIGHT 262
#define OPT_AREA_WEIGHT 263
#endif
//Simulated Annealing
#define OPT_MOSA_FLOW 281

///Scheduling
#define OPT_FIXED_SCHED 264
#define OPT_LIST_BASED 265
///Finite state machine
#define OPT_STG 267
///Estimation
#define OPT_EVALUTION 268
#define OPT_EVALUTION_MODEL 269
#define OPT_AREA_RESULT 270
#define OPT_DEVICE_NAME 275
#define OPT_PERIOD_CLOCK 276
#define OPT_POWER_OPTIMIZATION 277

///Output
#define OPT_PRETTY_PRINT 280

#define OPT_PRAGMA_PARSE 279

#define OPT_INSERT_VERIFICATION_OPERATION 282

#define OPT_DUMP_CONSTRAINTS 283

#define OPT_TESTBENCH 285

#define OPT_GENERATE_VCD 286

///Parallel controller
#define OPT_PARALLEL_CONTROLLER 287
#define OPT_LOGICAL_OPTIMIZATION 288

#define OPT_ENABLE_IOB 289
#define OPT_DYNAMIC_GENERATORS_DIR 290
#define OPT_REGISTER_ALLOCATION 291
#define OPT_INSERT_MEMORY_PROFILE 293

///XML configuration
#define OPT_XML_CONFIG 294

#define OPT_RESET 295
#define OPT_CLOCK_PERIOD_RESOURCE_FRACTION 296
#define OPT_TOP_FNAME 297

#define OPT_CHANNELS_TYPE 298
#define OPT_CHANNELS_NUMBER 299
#define OPT_SOFT_FLOAT 300
#define OPT_MAX_SIM_CYCLES 301
#define OPT_MAX_ULP 302
#define OPT_SKIP_PIPE_PARAMETER 303


/// constant correspond to the "parametric list based option"
#define PAR_LIST_BASED_OPT "parametric-list-based"
/// constant correspond to the fixed scheduling option
#define FIXED_SCHEDULING_OPT "fixed-scheduling"


void BambuParameter::PrintHelp(std::ostream &os) const
{
   os
   << "Usage:\n"
   << "       " << getOption<std::string>(OPT_program_name) << " [Options] <source_file> [<constraints_file>] [<technology_file>]\n"
   << "Options:\n"
   << "\n";
   PrintGeneralOptionsUsage(os);
   PrintOutputOptionsUsage(os);
   PrintGccOptionsUsage(os);
   os
   << "  Target:\n"
   << "    --target-file=file, -b<file>    Specify an XML description of the target device.\n";
#if HAVE_EXPERIMENTAL
   os
   << "    --edk-config <file>             Specify the configuration file for Xilinx EDK.\n"
   << "\n"
   << "  Frontend analysis:\n"
   << "    --pdg-reduction[=<algorithm>]   Perform Program Dependence Graph reduction.\n"
   << "                                    The reduced PDG is used to build the parallel controller, if enabled\n"
   << "                                       GP - Girkar-Polychronopoulos algorithm (default)\n"
   << "                                       P  - Polimi algorithm (not supported yet)\n";
#endif
   os
   << "\n"
   << "  Scheduling:\n"
   << "    --parametric-list-based[=<type] Perform priority list-based scheduling (default scheduling algorithm):\n"
   << "                                        0 - Dynamic mobility (default)\n"
   << "                                        1 - Static mobility\n"
   << "                                        2 - Priority-fixed mobility\n";
#if HAVE_EXPERIMENTAL
#if HAVE_ILP_BUILT
   os << "    --ilp=[<solver>]             Perform scheduling by using the integer linear programming\n"
   << "                                     formulation (Default option is <solver>=glpk) (off):\n"
   << "                                        G - Solve the ilp problem using glpk solver. (default)\n";
#if HAVE_COIN_OR
   os
   << "                                        C - Solve the ilp problem using the COIN-OR solver.\n";
#endif
#if HAVE_LP_SOLVE
   os
   << "                                        L - Solve the ilp problem using lp_solve solver.\n";
#endif
   os << "    --ilp-newform=[<solver>]     Perform scheduling by using the integer linear programming\n"
   << "                                     with the new formulation (Default option is <solver>=glpk) (off):\n"
   << "                                        G - Solve the ilp problem using glpk solver. (default)\n";
#if HAVE_COIN_OR
   os
   << "                                        C - Solve the ilp problem using the COIN-OR solver.\n";
#endif
#if HAVE_LP_SOLVE
   os
   << "                                        L - Solve the ilp problem using lp_solve solver.\n";
#endif
   os << "    --silp=[<solver>]            Perform scheduling by using the scheduling and allocation with ILP\n"
   << "                                     formulation (Default option is <solver>=glpk) (off):\n"
   << "                                        G - Solve the silp problem using glpk solver. (default)\n";
#if HAVE_COIN_OR
   os
   << "                                        C - Solve the silp problem using the COIN-OR solver.\n";
#endif
#if HAVE_LP_SOLVE
   os
   << "                                        L - Solve the silp problem using lp_solve solver.\n";
#endif
//   << "    --ilp-loop=[<solver>]        Perform loops scheduling by using the integer linear programming\n"
//   << "                                     formulation (Default option is <solver>=glpk) (off):\n"
//   << "                                        G - Solve the silp problem using glpk solver. (default)\n"
//#if HAVE_COIN_OR
//   << "                                        C - Solve the ilp problem using the COIN-OR solver.\n"
//#endif
//#if HAVE_LP_SOLVE
//   << "                                        L - Solve the ilp problem using lp_solve solver.\n"
//#endif
#endif
//   << "    --speculative, -S               Perform scheduling by using speculative computation (only list\n"
//   << "                                     and ILP scheduling have been affected by this option) (off).\n"
#endif
   os
   << "    --fixed-scheduling=<file>       Provide scheduling as an XML file.\n"
   << "    --no-chaining                   Disable chaining optimization.\n"
   << "\n"
   << "  Controller style:\n"
   << "    --stg[=<type>]                  Create the FSM-based controller (default controller style):\n"
   << "                                        0 - Basic Block-based (exploiting scheduling information) (default)\n";
#if HAVE_EXPERIMENTAL
   os
   << "    --parallel-controller           Create the parallel controller\n";
#endif
   os
   << "\n"
   << "  Binding:\n"
   << "    --storage-value-insertion       Specify the storage value insertion algorithm:\n"
   << "                                        0 - VARIABLE/VALUES (default)\n";
#if HAVE_EXPERIMENTAL
   os
   << "                                        1 - VALUES_INSTANCES\n"
   << "                                        2 - LIMITED_VALUES_INSTANCES\n"
   << "                                        3 - PAULIN_SCHEME\n";
#endif

#if HAVE_EXPERIMENTAL
   os
   << "    --explore-fu-reg=[string]       Perform simultaneous FU/reg binding.\n";
#endif
   os
   << "    --register-allocation=type      Perform register allocation with one of the following algorithms:\n"
   << "                                        WEIGHTED_COLORING - use weighted coloring algorithm (default)\n"
   << "                                        COLORING - use simple coloring algorithm\n"
   << "                                        CHORDAL_COLORING - use chordal coloring algorithm\n"
   << "                                        BIPARTITE_MATCHING - use bipartite matching algorithm\n"
   << "                                        TTT_CLIQUE_COVERING - use a weighted clique covering algorithm\n"
   << "                                        UNIQUE_BINDING - unique binding algorithm\n";
#if HAVE_EXPERIMENTAL
   os
   << "                                        K_COFAMILY - use k_cofamily algorithm\n"
   << "                                        LEFT_EDGE - use left edge algorithm\n"
   << "                                        CYCLIC_ALLOCATION - use cyclic allocation algorithm\n";
#endif
   os
   << "    --module-binding=type           Perform module binding with one of the following algorithms:\n"
   << "                                        WEIGHTED_TS - solve the weighted clique covering problem by exploiting the Tseng&Siewiorek heuristic (default)\n"
   << "                                        WEIGHTED_COLORING  - solve the weighted clique covering problem performing a coloring on the conflict graph\n"
   << "                                        COLORING    - solve the unweighted clique covering problem performing a coloring on the conflict graph\n"
   << "                                        TTT_FAST    - use Tomita, A. Tanaka, H. Takahashi maximal weighted cliques heuristic to solve the clique covering problem \n"
   << "                                        TTT_FAST2   - use Tomita, A. Tanaka, H. Takahashi maximal weighted cliques heuristic to incrementally solve the clique covering problem \n"
   << "                                        TTT_FULL    - use Tomita, A. Tanaka, H. Takahashi maximal weighted cliques algorithm to solve the clique covering problem\n"
   << "                                        TTT_FULL2   - use Tomita, A. Tanaka, H. Takahashi maximal weighted cliques algorithm to incrementally solve the clique covering problem\n"
   << "                                        TS          - solve the unweighted clique covering problem by exploiting the Tseng&Siewiorek heuristic\n"
   << "                                        BIPARTITE_MATCHING - solve the weighted clique covering problem exploiting the bipartite matching approach\n"
   << "                                        UNIQUE      - use a 1-to-1 binding algorithm\n"
   << "\n"
   << "  Memory allocation:\n"
   << "    --memory-allocation=type        Perform memory allocation with one of the following algorithms:\n"
   << "                                        DOMINATOR          - all local variables, static variables and strings are allocated on BRAMs (default)\n"
   << "                                        XML_SPECIFICATION  - import the memory allocation from an XML specification\n"
   << "    --xml-memory-allocation=<file>  Specify the file where the XML configuration has been defined.\n"
   << "    --memory-allocation-policy=type Perform memory allocation with one of the following policies:\n"
   << "                                        LSS        - all local variables, static variables and strings are allocated on BRAMs (default)\n"
   << "                                        GSS        - all global variables, static variables and strings are allocated on BRAMs\n"
   << "                                        ALL_BRAM   - all objects that need to be stored in memory are allocated on BRAMs\n"
   << "                                        NO_BRAM    - all objects that need to be stored in memory are allocated on an external memory\n"
   << "                                        EXT_PIPELINED_BRAM - all objects that need to be stored in memory are allocated on an external pipelined memory\n"
   << "   --base-address=address           Define the starting address for the externally allocated objects.\n"
   << "   --channels-type=type             Build an architecture with different type of memory connections:\n"
   << "                                        MEM_ACC_11 - the accesses to the memory have a single direct connection or a single indirect connection (default)\n"
   << "                                        MEM_ACC_N1 - the accesses to the memory have n parallel direct connections or a single indirect connection\n"
   << "                                        MEM_ACC_NN - the accesses to the memory have n parallel direct connections or n parallel indirect connections\n"
   << "   --channels-number=<n>            Define the number of parallel direct or indirect accesses.\n"
   << "   --memory-ctrl-type=type          Define which type of memory controller is used:\n"
   << "                                        D00 - no extra delay (default)\n"
   << "                                        D10 - 1 clock cycle extra-delay for LOAD 0 for STORE\n"
   << "                                        D11 - 1 clock cycle extra-delay for LOAD 1 for STORE\n"
   << "                                        D21 - 2 clock cycle extra-delay for LOAD 1 for STORE\n"
   << "   --sparse-memory[=on/off]         Control how the memory allocation happen:\n"
   << "                                        on  - allocate the addresses of data choosing the addresses in order to reduce the decoding logic (default)\n"
   << "                                        off - allocate the addresses of data in a contiguous way\n"
   << "\n"
   << "  Interconnection:\n"
   << "    --interconnection, -C[<type>]   Perform interconnection binding:\n"
   << "                                        M - mux-based architecture (default)\n"
   << "\n";
#if HAVE_EXPERIMENTAL
#if HAVE_BEAGLE
   os
   << "  High-Level Synthesis Design Space Exploration:\n"
   << "    --dse=<type>                    Perform design space exploration (off)\n"
   << "                                      MOGA  - Multi-objective Genetic Algorithm with NSGA-II\n"
   << "                                      MOSA  - Multi-objective Simulated Annealing\n"
   << "                                      MOTS  - Multi-objective Tabu Search\n"
   << "                                      ACO   - Multi-objective Ant Colony Optimization\n"
   << "                                      GRASP - Grasp\n"
   << "    --max-evalutions=<num>          Perform no more than the specified number of evalutions.\n"
   << "    --aco-flow[=<number of ants>]   Use Ant Colony Optimization - (off)\n"
   << "    --generations[=<num>]           Number ant colony's generations\n"
   << "    --mixed_synthesis, -H[<type>]   Determine the exploration space\n"
   << "                                        0 - based on operation binding information (default)\n"
   << "                                        1 - based on scheduling priority information\n"
   << "                                        2 - based on binding and scheduling information\n"
   << "\n";
#endif
#endif
   os
   << "  Evaluation of HLS results:\n"
   << "    --evaluation[=type]             Perform evaluation of the results:\n"
   << "                                      EXACT: based on actual synthesis and simulation (default)\n";
   #if HAVE_EXPERIMENTAL
   os
   << "                                      LINEAR: based on linear regression\n"
   << "                                      STATISTICAL: based on statistical information\n";
   #endif
   os
   << "    --objective=<obj1,obj2>         Select the objective to be analyzed:\n"
   << "                                      AREA         - Area usage\n"
   << "                                      TIME         - Latency for the whole computation\n"
   << "                                      CYCLES       - n. of cycles for the whole computation\n"
   << "                                      FREQUENCY    - Maximum target frequency\n"
   << "                                      CLOCK_SLACK  - Slack between actual and required clock period\n"
   << "                                      REGISTERS    - number of registers\n"
   << "                                      DSPS         - number of DSPs\n"
   << "                                      BRAMS        - number of BRAMs\n";
#if HAVE_EXPERIMENTAL
   os
   << "                                      NUM_AF_EDGES    - Number of incoming edges in the FAFG\n"
   << "                                      EDGES_REDUCTION - Performance evaluation for dependence reduction techniques\n"
   << "    --evaluation-model=<model>      Set XML evaluation model.\n"
   << "    --area_result=<variable>        Select the variable to be shown for area evaluation.\n"
   << "    --timing-simulation             Perform a simulation considering the timing delays.\n";
#endif
   os
   << "\n";
#if (HAVE_EXPERIMENTAL && (HAVE_XILINX || HAVE_MODELSIM || HAVE_VERILATOR || HAVE_ICARUS) && HAVE_VCD_BUILT) || HAVE_MODELSIM
   os
   << "  Checks and debugging:\n"
#if HAVE_EXPERIMENTAL && (HAVE_XILINX || HAVE_MODELSIM || HAVE_VERILATOR || HAVE_ICARUS) && HAVE_VCD_BUILT
   << "    --vcd-debug                     Enable debugging based on vcd files.\n"
#endif
#if HAVE_MODELSIM
   << "    --assert-debug                  Enable assertion debugging performed by Modelsim.\n"
#endif
   << "\n";
#endif
   os
   << "  Export:\n"
   << "    --export-core <type>            Exports the generated accelerator:\n"
   << "                                      PCORE         - Xilinx XPS pcore\n"
   << "\n"
   << "  RTL synthesis:\n"
   #if HAVE_LIBRARY_CHARACTERIZATION_BUILT
   << "    --synthesize                    Synthesize the design.\n"
#endif
   << "                                      - Note that for a more complete design evaluation you should use the option --evaluation\n"
   << "    --clock-period=value            Specify the period of the clock signal (default = 10ns).\n"
   << "    --device-name=value             Specify the name of the device. Two different cases are foreseen:\n"
   << "                                    - Xilinx: comma separated string specifying device, speed grade and package (e.g.,: \"xc7z020,-1,clg484,VVD\")\n"
   << "                                    - Altera: a string defining the device string (e.g., EP2C70F896C6-DSP)\n"
   << "                                    - Lattice: a string defining the device string (e.g., LFE335EA8FN484C)\n"
   << "    --power-optimization            Enable Xilinx power based optimization (default no).\n"
   << "    --no-connect-iob                Disconnect primary ports from the IOB (default primary ports are connected to IOBs).\n"
   << "    --soft-float                    Enable use of soft-based implementation of floating-point operations.\n"
   << "    --max-ulp                       Define the maximal ULP (Unit in the last place, i.e., is the spacing between floating-point numbers) accepted.\n"
   << "    --skip-pipe-parameter=value     Used during the allocation of pipelined units. 'value' specifiies how many pipelined units, compliant with the clock period, will be skiped. (default=1).\n"
   << "    --reset-type=value              Specify the type of reset:\n"
   << "                                       no    - use registers without reset (default)\n"
   << "                                       async - use registers with asynchronous reset\n"
   << "                                       sync  - use registers with synchronous reset\n"
   << "    --cprf=value                    Clock Period Resource Fraction (default = 0.9).\n"
   << "\n";
   os
   << "  Other options:\n"
   << "    --pretty-print=<file>           C-based pretty print of the internal IR.\n";
#if HAVE_EXPERIMENTAL
   os
   << "    --pragma-parse                  Perform source code parsing to extract information about pragmas (default=no).\n";
#endif
   os
   << "    --writer,-w<language>           Output RTL language:\n"
   << "                                        V - Verilog (default)\n"
#if HAVE_EXPERIMENTAL
   << "                                        S - SystemC\n"
   << "                                        H - VHDL\n"
#endif
   << "    --generate-tb=<file>            Generate testbench for the input values defined in the specified XML file.\n"
   << "    --generate-vcd                  Enable .vcd output file generation for waveform visualization (requires testbench generation).\n"
   << "    --max-sim-cycles=<cycles>       Specify the maximum number of cycles a HDL simulation may run (default 20000000).\n"
   << "    --data-bus-bitsize=<bitsize>    Set the bitsize of the external data bus.\n"
   << "    --addr-bus-bitsize=<bitsize>    Set the bitsize of the external address bus.\n"
   << "    --generate-interface=<type>     Wrap the top level with an interface:\n"
   << "                                        minimal (minimal interface) - default\n"
   << "                                        WB4 (WishBone 4 interface)\n"
   << "                                        AXI4LITE (AXI4-Lite interface)\n";
#if HAVE_EXPERIMENTAL
   os
   << "                                        FSL (interface to the FSL bus)\n"
   << "                                        NPI (interface to the NPI bus)\n";
#endif
#if HAVE_EXPERIMENTAL
   os
   << "    --xml-config <file>             Define the path to the XML configuration file for the synthesis.\n";
#endif
#if HAVE_ICARUS || HAVE_XILINX || HAVE_VERILATOR || HAVE_MODELSIM
   os
   << "    --simulate[=<file>]             Simulate the RTL implementation.\n"
   << "    --simulator=<type>              Specify which simulator is used during the HDL-based simulation:\n";
#if HAVE_MODELSIM
   os
   << "                                        MODELSIM - Mentor Modelsim\n";
#endif
#if HAVE_XILINX
   os
   << "                                        XSIM - Xilinx XSim\n"
   << "                                        ISIM - Xilinx iSim\n";
#endif
#if HAVE_ICARUS
   os
   << "                                        ICARUS - Verilog Icarus simulator\n";
#endif
#if HAVE_VERILATOR
   os
   << "                                        VERILATOR - Verilator simulator\n";
#endif
#endif
#if HAVE_EXPERIMENTAL
   os
   << "    --logical-optimization=<level>  Enable logic optimization on EPDG:\n"
   << "                                        1  - enable optimizations based on CONDITIONAL instructions information\n"
   << "                                        2  - enable optimizations based on ACTIVATION PATH information\n"
   << "                                        3  - enable CONDITIONAL and ACTIVATION PATH optimizations\n"
   << "                                        4  - enable DATA_TRANSITIVITY optimizations\n"
   << "                                        5  - enable CONDITIONAL and DATA_TRANSITIVITY optimizations\n"
   << "                                        6  - enable ACTIVATION PATH and DATA_TRANSITIVITY optimizations\n"
   << "                                        7  - enable CONDITIONAL, ACTIVATION PATH and DATA_TRANSITIVITY optimizations\n"
   << "                                        8  - enable CONTROL_DATA_TRANSITIVITY optimizations\n"
   << "                                        9  - enable CONTROL_DATA_TRANSITIVITY and CONDITIONAL instructions optimizations\n"
   << "                                        16 - enable CONTROL_DATA_CONTROL_FLOW_TRANSITIVITY and CONDITIONAL instructions optimizations\n";
#endif
   os
   << "    --top-fname=<fun_name>          Define the top function to be synthesized.\n";
#if HAVE_ILP_BUILT
   os
   << "    --time, -t <time>               Set maximun execution time (in seconds) for ILP solvers. (infinite).\n";
#endif
   os << std::endl;
}

void BambuParameter::PrintProgramName(std::ostream & os) const
{
   os << "" << std::endl;
   os << "********************************************************************************" << std::endl;
   os << "                    ____                  _" << std::endl;
   os << "                   | __ )  __ _ _ __ ___ | |_   _   _" << std::endl;
   os << "                   |  _ \\ / _` | \'_ ` _ \\| \'_ \\| | | |" << std::endl;
   os << "                   | |_) | (_| | | | | | | |_) | |_| |" << std::endl;
   os << "                   |____/ \\__,_|_| |_| |_|_.__/ \\__,_|" << std::endl;
   os << "" << std::endl;
   os << "********************************************************************************" << std::endl;
   os << "                         High-Level Synthesis Tool" << std::endl;
   os << "" << std::endl;
}

BambuParameter::BambuParameter(const std::string _program_name) :
   Parameter(_program_name)
{
   SetDefaults();
}

int BambuParameter::exec(int argc, char* argv[])
{
   exit_code = PARAMETER_NOTPARSED;

   /// flag to check if scheduling algorithm has been already chosen
   bool scheduling_set_p = false;
   /// variable used into option parsing
   int option_index;
   int next_option;

   // Bambu short option. An option character in this string can be followed by a colon (`:') to indicate that it
   // takes a required argument. If an option character is followed by two colons (`::'), its argument is optional;
   // this is a GNU extension.
   const char* const short_options = COMMON_SHORT_OPTIONS_STRING
                                     "c:t:U:H:SC::b:w:"
                                     GCC_SHORT_OPTIONS_STRING;

   const struct option long_options[] =
      {
         COMMON_LONG_OPTIONS,
         ///General options
         {"top-fname",                  required_argument, nullptr, OPT_TOP_FNAME},
         {"xml-config",                 required_argument, nullptr, OPT_XML_CONFIG},
         {"time",                       required_argument, nullptr, 't'},
         /// Frontend options
         {"circuit-dbg",                required_argument, nullptr,  0 },
#if HAVE_EXPERIMENTAL
         {"pdg-reduction",              optional_argument, nullptr,  0 },
         ///--- Flow options ---
         {"dump-constraints",           optional_argument, nullptr, OPT_DUMP_CONSTRAINTS},
         /// --- Design Space Exploration options ---
#if HAVE_BEAGLE
         {"dse",                        required_argument, nullptr, OPT_DSE},
         {"max_evaluations",            required_argument, nullptr, OPT_MAX_EVALUATIONS},
         //ACO
         {"aco-flow",                   optional_argument, nullptr, OPT_ACO_FLOW},
         {"generations",                optional_argument, nullptr, OPT_ACO_GENERATIONS},
         //Genetic algorithm
         {"mixed_synthesis",            optional_argument, nullptr, 'H'},
         {"seed",                       required_argument, nullptr, 'A'},
         {"run",                        required_argument, nullptr, 'B'},
         {"population",                 required_argument, nullptr, 'E'},
         {"generation",                 required_argument, nullptr, OPT_GENERATION},
         {"fitness_function",           required_argument, nullptr, 'P'},
         {"inheritance_rate",           required_argument, nullptr, 'F'},
         {"inheritance_mode",           required_argument, nullptr, 'G'},
         {"max_inheritance",            required_argument, nullptr, OPT_MAX_INHERITANCE},
         {"min_inheritance",            required_argument, nullptr, OPT_MIN_INHERITANCE},
         {"distance_rate",              required_argument, nullptr, 'M'},
         {"weighting_function",         required_argument, nullptr, 'N'},
         {"normalize",                  optional_argument, nullptr, 'Q'},
         {"time-weight",                required_argument, nullptr, OPT_TIME_WEIGHT}, //no short option
         {"area-weight",                required_argument, nullptr, OPT_AREA_WEIGHT}, //no short option
#endif
         /// --- Algorithms options ---
         {"explore-mux",                optional_argument,       nullptr,  0 },
         {"explore-fu-reg",             required_argument,       nullptr,  0 },
#endif
         /// Scheduling options
         {FIXED_SCHEDULING_OPT,         required_argument, nullptr, OPT_FIXED_SCHED},
         {PAR_LIST_BASED_OPT,           optional_argument, nullptr, OPT_LIST_BASED}, //no short option
#if HAVE_ILP_BUILT
         {"ilp",                        optional_argument, nullptr, 0},
         {"ilp-newform",                optional_argument, nullptr, 0},
         {"silp",                       optional_argument, nullptr, 0},
         {"ilp-loop",                   optional_argument, nullptr, 0},
#endif
         {"speculative",                no_argument,       nullptr, 'S'},
         {"no-chaining",                no_argument,       nullptr,  0 },
         /// Finite state machine options
         {"stg",                        required_argument, nullptr, OPT_STG},
         /// module binding
         {"module-binding",             required_argument, nullptr, 0 },
         /// register allocation
         {"register-allocation",        required_argument, nullptr, OPT_REGISTER_ALLOCATION },
         {"storage-value-insertion",    required_argument, nullptr, 0 },
         /// Memory allocation
         {"memory-allocation",          required_argument, nullptr,  0 },
         {"memory-allocation-policy",   required_argument, nullptr,  0 },
         {"xml-memory-allocation",      required_argument, nullptr,  0 },
         {"base-address",               required_argument, nullptr,  0 },
         {"channels-type",              required_argument, nullptr,  0 },
         {"channels-number",            required_argument, nullptr,  0 },
         {"memory-ctrl-type",           required_argument, nullptr,  0 },
         {"sparse-memory",              optional_argument, nullptr,  0 },
         //interconnections
         {"interconnection",            required_argument, nullptr, 'C' },
#if HAVE_EXPERIMENTAL
         {"parallel-controller",        no_argument,       nullptr, OPT_PARALLEL_CONTROLLER},
#if HAVE_CUDD
         {"logical-optimization",       required_argument, nullptr, OPT_LOGICAL_OPTIMIZATION},
#endif
#endif
         /// evaluation options
         {"evaluation",                 optional_argument, nullptr, OPT_EVALUTION},
         {"objective",                  required_argument, nullptr,  0 },
#if HAVE_EXPERIMENTAL
         {"evaluation_model",           required_argument, nullptr, OPT_EVALUTION_MODEL},
         {"area_result",                required_argument, nullptr, OPT_AREA_RESULT},
         {"timing-simulation",          no_argument,       nullptr,  0 },
#endif
#if HAVE_EXPERIMENTAL && (HAVE_XILINX || HAVE_MODELSIM || HAVE_ICARUS || HAVE_VERILATOR) && HAVE_VCD_BUILT
            {"vcd-debug",                  no_argument,       nullptr,  0 },
#endif
         {"assert-debug",               no_argument,       nullptr,  0 },
#if HAVE_LIBRARY_CHARACTERIZATION_BUILT
         {"synthesize",                 no_argument,       nullptr,  0 },
#endif
         {"device-name",                required_argument, nullptr, OPT_DEVICE_NAME},
         {"clock-period",               required_argument, nullptr, OPT_PERIOD_CLOCK},
         {"power-optimization",         no_argument,       nullptr, OPT_POWER_OPTIMIZATION},
         {"no-connect-iob",             no_argument,       nullptr, OPT_ENABLE_IOB},
         {"reset-type",                 required_argument, nullptr, OPT_RESET},
         {"soft-float",                 no_argument,       nullptr, OPT_SOFT_FLOAT},
         {"max-ulp",                    required_argument, nullptr, OPT_MAX_ULP},
         {"skip-pipe-parameter",        required_argument, nullptr, OPT_SKIP_PIPE_PARAMETER},
         {"cprf",                       required_argument, nullptr, OPT_CLOCK_PERIOD_RESOURCE_FRACTION},
         ///target options
         {"target-file",                required_argument, nullptr, 'b'},
#if HAVE_EXPERIMENTAL
         {"edk-config",                 required_argument, nullptr,  0 },
#endif
         {"export-core",                required_argument, nullptr,  0 },
         ///Output options
         {"writer",                     required_argument, nullptr, 'w'},
         {"dynamic-generators-dir",     required_argument, nullptr, OPT_DYNAMIC_GENERATORS_DIR},
         {"pretty-print",               required_argument, nullptr, OPT_PRETTY_PRINT},
#if HAVE_EXPERIMENTAL
         {"pragma-parse",              no_argument,       nullptr, OPT_PRAGMA_PARSE},
#endif
         {"generate-interface",         required_argument, nullptr,  0 },
         {"data-bus-bitsize",           required_argument, nullptr,  0 },
         {"addr-bus-bitsize",           required_argument, nullptr,  0 },
#if HAVE_EXPERIMENTAL
         {"resp-model",                 required_argument, nullptr,  0 },
#endif
         {"generate-tb",                required_argument, nullptr, OPT_TESTBENCH},
         {"max-sim-cycles",             required_argument, nullptr, OPT_MAX_SIM_CYCLES},
         {"generate-vcd",               no_argument,       nullptr, OPT_GENERATE_VCD},
         {"simulate",                   optional_argument, nullptr,  0 },
         {"simulator",                  required_argument, nullptr,  0 },
         GCC_LONG_OPTIONS,
         {nullptr, 0, nullptr, 0}
      };

   if (argc == 1)   // Bambu called without arguments, it simple prints help message
   {
      PrintUsage(std::cout);
      return EXIT_SUCCESS;
   }

   while (1)
   {
      next_option = getopt_long(argc, argv, short_options, long_options, &option_index);

      // no more options are available
      if (next_option == -1)
         break;

      switch (next_option)
      {
         ///general options
         case OPT_TOP_FNAME:
            // set name of the function to be take into account as top function
            setOption(OPT_top_function_name, optarg);
            setOption(OPT_top_file, optarg);
            break;
         case 't':
            setOption(OPT_ilp_max_time, optarg);
            break;
#if HAVE_EXPERIMENTAL
         case OPT_XML_CONFIG:
            setOption(OPT_synthesis_flow, HLS_synthesis_flow::XML_CONFIG);
            setOption(OPT_xml_input_configuration, optarg);
            break;
         case OPT_DUMP_CONSTRAINTS:
            setOption(OPT_hls_flow, hls_flow::DUMP);
            setOption("dumpConstraints", true);
            if (optarg)
               setOption("dumpConstraints_file", optarg);
            break;
         ///frontend options
         case 'U': // compute the unrolling degree of a loop
            setOption("compute_unrolling_degree",true);
            if (optarg)
               setOption("IImax", optarg);
            else
               throw "BadParameters: compute unrolling degree needs a additional parameter";
            break;
         case OPT_INSERT_VERIFICATION_OPERATION:
            setOption("insert_verification_operation", true);
            break;
         ///design space exploration options
#if HAVE_BEAGLE
         case OPT_DSE:
         {
            setOption(OPT_hls_flow, hls_flow::DSE);
            if (std::string(optarg) == ("MOSA"))
               setOption("dse_algorithm", dse_hls::MOSA);
            else if (std::string(optarg) == ("MOTS"))
               setOption("dse_algorithm", dse_hls::MOTS);
            else if (std::string(optarg) == ("ACO"))
               setOption("dse_algorithm", dse_hls::ACO);
            else if (std::string(optarg) == ("GRASP"))
               setOption("dse_algorithm", dse_hls::GRASP);
            else if (std::string(optarg) == ("MOGA"))
               setOption("dse_algorithm", dse_hls::MOGA);
            else
               THROW_ERROR("Not supported exploration algorithm: " + std::string(optarg));
            break;
         }
         case OPT_MAX_EVALUATIONS:
            setOption("max_evaluations", optarg);
            break;
         //ACO
         case OPT_ACO_FLOW:
            setOption(OPT_hls_flow, hls_flow::DSE);
            setOption("dse_algorithm", dse_hls::ACO);
            if (optarg)
               setOption("aco_ant_num", optarg);
            break;
         case OPT_ACO_GENERATIONS:
            if (optarg)
               setOption("aco_generations", optarg);
            break;
         case OPT_MOSA_FLOW:
            setOption(OPT_hls_flow, hls_flow::DSE);
            setOption("dse_algorithm", dse_hls::MOSA);
            break;
         //multi-objective genetic algorithm
         case 'H':  // enable mixed high level synthesis
            setOption(OPT_hls_flow, hls_flow::DSE);
            setOption("dse_algorithm", dse_hls::MOGA);
            if (optarg)
               setOption("exploration_technique", optarg);
            else
               setOption("exploration_technique", dse_hls::BINDING);
            break;
         case 'Q':
            if (optarg)
               setOption("to_normalize", optarg);
            else
               setOption("to_normalize", 1);
            break;
         case 'P':  // mixed high level synthesis fitness function
            setOption("fitness_function", optarg);
            break;
         case 'A':
            setOption("seed", optarg);
            break;
         case 'B':
            setOption("run", optarg);
            break;
         case 'E':
            setOption("population", optarg);
            break;
         case OPT_GENERATION:
            setOption("GA_generation", optarg);
            break;
         case 'F':
            setOption("fitness_inheritance_rate", optarg);
            break;
         case 'G':
            setOption("inheritance_mode", optarg);
            break;
         case OPT_MAX_INHERITANCE:
            setOption("max_for_inheritance", optarg);
            break;
         case OPT_MIN_INHERITANCE:
            setOption("min_for_inheritance", optarg);
            break;
         case 'M':
            setOption("distance_rate", optarg);
            break;
         case 'N':
            setOption("weighting_function", optarg);
            break;
         case OPT_TIME_WEIGHT:
            setOption("time_weight", optarg);
            break;
         case OPT_AREA_WEIGHT:
            setOption("area_weight", optarg);
            break;
#endif
#endif
         ///scheduling options
         case OPT_FIXED_SCHED:
            if (scheduling_set_p)
               throw "BadParameters: only one scheduler can be specified";
            scheduling_set_p = true;
            setOption(OPT_scheduling_algorithm, scheduling::FIXED);
            if (optarg)
               setOption("fixed_scheduling_file", optarg);
            break;
         case OPT_LIST_BASED: // enable list based scheduling
            if (scheduling_set_p)
               throw "BadParameters: only one scheduler can be specified";
            scheduling_set_p = true;
            setOption(OPT_scheduling_algorithm, scheduling::LIST_BASED);
            if (optarg)
               setOption(OPT_scheduling_priority, optarg);
            break;
#if HAVE_EXPERIMENTAL
#if HAVE_ILP_BUILT
         case 'i': // enable ILP scheduling
            if (scheduling_set_p)
               throw "BadParameters: only one scheduler can be specified";
            scheduling_set_p = true;
            setOption(OPT_scheduling_algorithm, scheduling::ILP);
            setOption(OPT_chaining, true);/// ILP formulation tries to chain the operations
            if (optarg)
            {
#if HAVE_GLPK
               if (optarg[0] == 'G')
                  setOption(OPT_ilp_solver, meilp_solver::GLPK);
               else
#endif
#if HAVE_COIN_OR
               if (optarg[0] == 'C')
                  setOption(OPT_ilp_solver, meilp_solver::COIN_OR);
               else
#endif
#if HAVE_LP_SOLVE
               if (optarg[0] == 'L')
                  setOption(OPT_ilp_solver, meilp_solver::LP_SOLVE);
               else
#endif
                  throw "BadParameters: not recognized ilp solver";
            }
            break;
         case 'I': // enable ILP scheduling, new-formulation
            if (scheduling_set_p)
               throw "BadParameters: only one scheduler can be specified";
            scheduling_set_p = true;
            PRINT_DBG_MEX(DEBUG_LEVEL_VERY_PEDANTIC, debug_level, "Set new ilp formulation");
            setOption(OPT_scheduling_algorithm, scheduling::ILP_NEW_FORM);
            if (optarg)
            {
#if HAVE_GLPK
               if (optarg[0] == 'G')
                  setOption(OPT_ilp_solver, meilp_solver::GLPK);
               else
#endif
#if HAVE_COIN_OR
               if (optarg[0] == 'C')
                  setOption(OPT_ilp_solver, meilp_solver::COIN_OR);
               else
#endif
#if HAVE_LP_SOLVE
               if (optarg[0] == 'L')
                  setOption(OPT_ilp_solver, meilp_solver::LP_SOLVE);
               else
#endif
                  throw "BadParameters: not recognized ilp solver";
            }
            break;
#if 0
         case 'J': // enable ILP scheduling, loop pipelining
            if (scheduling_set_p)
               throw "BadParameters: only one scheduler can be specified";
            scheduling_set_p = true;
            setOption(OPT_scheduling_algorithm, scheduling::ILP_LOOP);
            if (optarg)
            {
               if (optarg[0] == 'G')
                  setOption(OPT_ilp_solver, meilp_solver::GLPK);
#if HAVE_COIN_OR
               else if (optarg[0] == 'C')
                  setOption(OPT_ilp_solver, meilp_solver::COIN_OR);
#endif
#if HAVE_LP_SOLVE
               else if (optarg[0] == 'L')
                  setOption(OPT_ilp_solver, meilp_solver::LP_SOLVE);
#endif
               else
                  throw "BadParameters: not recognized ilp solver";
            }
            break;
#endif
         case 'a': // enable SILP scheduling
            if (scheduling_set_p)
               throw "BadParameters: only one scheduler can be specified";
            scheduling_set_p = true;
            setOption(OPT_scheduling_algorithm, scheduling::SILP);
            if (optarg)
            {
#if HAVE_GLPK
               if (optarg[0] == 'G')
                  setOption(OPT_ilp_solver, meilp_solver::GLPK);
               else
#endif
#if HAVE_COIN_OR
               if (optarg[0] == 'C')
                  setOption(OPT_ilp_solver, meilp_solver::COIN_OR);
               else
#endif
#if HAVE_LP_SOLVE
               if (optarg[0] == 'L')
                  setOption(OPT_ilp_solver, meilp_solver::LP_SOLVE);
               else
#endif
                  throw "BadParameters: not recognized silp solver";
            }
            break;
#endif
         case 'S': // enable scheduling with speculative computation
            setOption(OPT_speculative, true);
            break;
#endif
         case OPT_STG:
            setOption(OPT_stg, true);
            if (optarg)
               setOption(OPT_stg_algorithm, optarg);
            break;
         ///binding options
         //register
         case OPT_REGISTER_ALLOCATION: // enable register allocation
         {
            if (std::string(optarg) == "COLORING")
               setOption(OPT_register_allocation_algorithm, reg_binding_creator::COLORING);
            else if(std::string(optarg) == "CHORDAL_COLORING")
               setOption(OPT_register_allocation_algorithm, reg_binding_creator::CHORDAL_COLORING);
#if HAVE_EXPERIMENTAL
            else if(std::string(optarg) == "LEFT_EDGE")
               setOption(OPT_register_allocation_algorithm, reg_binding_creator::LEFT_EDGE);
            else if(std::string(optarg) == "K_COFAMILY")
               setOption(OPT_register_allocation_algorithm, reg_binding_creator::K_COFAMILY);
#endif
            else if(std::string(optarg) == "WEIGHTED_COLORING")
               setOption(OPT_register_allocation_algorithm, reg_binding_creator::WEIGHTED_COLORING);
            else if(std::string(optarg) == "BIPARTITE_MATCHING")
               setOption(OPT_register_allocation_algorithm, reg_binding_creator::BIPARTITE_MATCHING);
            else if(std::string(optarg) == "TTT_CLIQUE_COVERING")
               setOption(OPT_register_allocation_algorithm, reg_binding_creator::TTT_CLIQUE_COVERING);
            else if(std::string(optarg) == "UNIQUE_BINDING")
               setOption(OPT_register_allocation_algorithm, reg_binding_creator::UNIQUE_BINDING);
            else
               throw "BadParameters: register allocation not correctly specified";
            break;
         }
         //interconnection
         case 'C':
            if (std::string(optarg) == "M")
               setOption(OPT_datapath_interconnection_algorithm, conn_binding_creator::MUX_INTERCONNECTION_BINDING);
            else
               throw "BadParameters: interconnection binding not correctly specified";
            break;
#if HAVE_EXPERIMENTAL
         case OPT_PARALLEL_CONTROLLER:
            setOption(OPT_controller_architecture, controller_creator::PARALLEL_CONTROLLER);
            break;
#endif
         ///target options
         case 'b':
            setOption(OPT_target_device_file, optarg);
            break;
         ///evaluation
         case OPT_EVALUTION:
            setOption(OPT_evaluation, true);
            if(optarg==nullptr)
               setOption(OPT_evaluation_method, objective_evaluator::EXACT);
            else if (std::string(optarg) == "EXACT")
               setOption(OPT_evaluation_method, objective_evaluator::EXACT);
#if HAVE_EXPERIMENTAL
            else if (std::string(optarg) == "LINEAR")
               setOption(OPT_evaluation_method, objective_evaluator::LINEAR);
            else if (std::string(optarg) == "STATISTICAL")
               setOption(OPT_evaluation_method, objective_evaluator::STATISTICAL);
#endif
            else
               throw "BadParameters: evaluation type not correctly specified";
            break;
#if HAVE_EXPERIMENTAL
         case OPT_EVALUTION_MODEL:
            setOption("evaluation_xml_model", optarg);
            break;
         case OPT_AREA_RESULT:
            setOption("area_result", true);
            if (optarg)
               setOption("area_final_variable", optarg);
            break;
#endif
         case OPT_DEVICE_NAME:
            {
               std::string tmp_string = optarg;
               std::vector<std::string> values = convert_string_to_vector<std::string>(tmp_string, std::string(","));
               setOption("device_name", "");
               setOption("device_speed", "");
               setOption("device_package", "");
               setOption("device_synthesis_tool", "");
               if(values.size() == 1)
               {
                  setOption(OPT_device_string, values[0]);
               }
               else if (values.size() == 3)
               {
                  setOption("device_name", values[0]);
                  setOption("device_speed", values[1]);
                  setOption("device_package", values[2]);
               }
               else if (values.size() == 4)
               {
                  setOption("device_name", values[0]);
                  setOption("device_speed", values[1]);
                  setOption("device_package", values[2]);
                  setOption("device_synthesis_tool", values[3]);
               }
               else
                   THROW_ERROR("Malformed device: " + tmp_string);
               break;
            }
         case OPT_PERIOD_CLOCK:
            setOption(OPT_clock_period, optarg);
            break;
         case OPT_POWER_OPTIMIZATION:
            setOption("power_optimization", true);
            break;
         case OPT_ENABLE_IOB:
            setOption("connect_iob", false);
            break;
         case OPT_RESET:
         {
            if (std::string(optarg) == "no")
               setOption(OPT_sync_reset, std::string(optarg));
            else if(std::string(optarg) == "async")
               setOption(OPT_sync_reset, std::string(optarg));
            else if(std::string(optarg) == "sync")
               setOption(OPT_sync_reset, std::string(optarg));
            else
               throw "BadParameters: reset type not correctly specified";

            break;
         }
         case OPT_SOFT_FLOAT:
            setOption(OPT_soft_float, true);
            break;
         case OPT_CLOCK_PERIOD_RESOURCE_FRACTION:
            setOption(OPT_clock_period_resource_fraction, optarg);
            break;
         case OPT_MAX_ULP:
            setOption(OPT_max_ulp, optarg);
            break;
         case OPT_SKIP_PIPE_PARAMETER:
            setOption(OPT_skip_pipe_parameter, optarg);
            break;
         ///output options
         case 'w':
            if (std::string(optarg) == "V")
               setOption(OPT_writer_language, language_writer::VERILOG);
#if HAVE_EXPERIMENTAL
            else if (std::string(optarg) == "S")
               setOption(OPT_writer_language, language_writer::SYSTEMC);
            else if (std::string(optarg) == "H")
               setOption(OPT_writer_language, language_writer::VHDL);
#endif
            else
               throw "BadParameters: backend language not correctly specified";
            break;
         case OPT_DYNAMIC_GENERATORS_DIR:
            setOption("dynamic_generators_dir", optarg);
            break;
         case OPT_PRETTY_PRINT:
            setOption(OPT_pretty_print, optarg);
            break;
#if HAVE_EXPERIMENTAL
         case OPT_PRAGMA_PARSE:
            setOption(OPT_parse_pragma, true);
            break;
#endif
         case OPT_TESTBENCH:
            setOption(OPT_generate_testbench, true);
            setOption("tb_input_xml", optarg);
            break;
         case OPT_MAX_SIM_CYCLES:
            setOption(OPT_max_sim_cycles, optarg);
            break;
         case OPT_GENERATE_VCD:
            setOption(OPT_generate_vcd, true);
            break;
#if HAVE_CUDD
         case OPT_LOGICAL_OPTIMIZATION:
            if (optarg)
               setOption("logical_optimization", optarg);
            break;
#endif
         case 0:
         {
            if (strcmp(long_options[option_index].name, "module-binding") == 0)
            {
               if (std::string(optarg) == "TTT_FAST")
                  setOption(OPT_fu_binding_algorithm, fu_binding_creator::CDFC_TTT_FAST);
               else if (std::string(optarg) == "TTT_FAST2")
                  setOption(OPT_fu_binding_algorithm, fu_binding_creator::CDFC_TTT_FAST2);
               else if (std::string(optarg) == "TTT_FULL")
                  setOption(OPT_fu_binding_algorithm, fu_binding_creator::CDFC_TTT_FULL);
               else if (std::string(optarg) == "TTT_FULL2")
                  setOption(OPT_fu_binding_algorithm, fu_binding_creator::CDFC_TTT_FULL2);
               else if (std::string(optarg) == "TS")
                  setOption(OPT_fu_binding_algorithm, fu_binding_creator::CDFC_TS);
               else if (std::string(optarg) == "WEIGHTED_TS")
                  setOption(OPT_fu_binding_algorithm, fu_binding_creator::CDFC_WEIGHTED_TS);
               else if (std::string(optarg) == "COLORING")
                  setOption(OPT_fu_binding_algorithm, fu_binding_creator::CDFC_COLORING);
               else if (std::string(optarg) == "WEIGHTED_COLORING")
                  setOption(OPT_fu_binding_algorithm, fu_binding_creator::CDFC_WEIGHTED_COLORING);
               else if (std::string(optarg) == "BIPARTITE_MATCHING")
                  setOption(OPT_fu_binding_algorithm, fu_binding_creator::CDFC_BIPARTITE_MATCHING);
               else if (std::string(optarg) == "UNIQUE")
                  setOption(OPT_fu_binding_algorithm, fu_binding_creator::UNIQUE);
               else
                  throw "BadParameters: module binding option not correctly specified";
               break;
            }
#if HAVE_LIBRARY_CHARACTERIZATION_BUILT
      if (strcmp(long_options[option_index].name, "synthesize") == 0)
            {
               setOption(OPT_perform_synthesis, true);

               std::string objective_string = getOption<std::string>(OPT_evaluation_objectives);
               std::vector<std::string> objective_vector = convert_string_to_vector<std::string>(objective_string, ",");
               if (std::find(objective_vector.begin(), objective_vector.end(), "AREA") == objective_vector.end())
               {
                  if(objective_string == "")
                     objective_string = "AREA";
                  else
                     objective_string = objective_string + ",AREA";
                  setOption(OPT_evaluation_objectives, objective_string);
                  setOption(OPT_evaluation_method, objective_evaluator::EXACT);
                  setOption(OPT_evaluation, true);
               }
               break;
            }
#endif
            if (strcmp(long_options[option_index].name, "memory-allocation") == 0)
            {
               if (std::string(optarg) == "DOMINATOR")
                  setOption(OPT_memory_allocation_algorithm, memory_allocation::DOMINATOR);
               else if (std::string(optarg) == "XML_SPECIFICATION")
                  setOption(OPT_memory_allocation_algorithm, memory_allocation::XML_SPECIFICATION);
               else
                  throw "BadParameters: memory allocation option not correctly specified";
               break;
            }
            if (strcmp(long_options[option_index].name, "xml-memory-allocation") == 0)
            {
               setOption(OPT_memory_allocation_algorithm, memory_allocation::XML_SPECIFICATION);
               setOption(OPT_xml_memory_allocation, optarg);
               break;
            }
            if (strcmp(long_options[option_index].name, "memory-allocation-policy") == 0)
            {
               if (std::string(optarg) == "LSS")
                  setOption(OPT_memory_allocation_policy, memory_allocation::LSS);
               else if (std::string(optarg) == "GSS")
                  setOption(OPT_memory_allocation_policy, memory_allocation::GSS);
               else if (std::string(optarg) == "ALL_BRAM")
                  setOption(OPT_memory_allocation_policy, memory_allocation::ALL_BRAM);
               else if (std::string(optarg) == "NO_BRAM")
                  setOption(OPT_memory_allocation_policy, memory_allocation::NO_BRAM);
               else if (std::string(optarg) == "EXT_PIPELINED_BRAM")
                  setOption(OPT_memory_allocation_policy, memory_allocation::EXT_PIPELINED_BRAM);
               else
                  throw "BadParameters: memory allocation policy option not correctly specified";
               break;
            }
            if (strcmp(long_options[option_index].name, "base-address") == 0)
            {
               setOption(OPT_base_address, optarg);
               break;
            }
            if (strcmp(long_options[option_index].name, "channels-type") == 0)
            {
               if (std::string(optarg) == "MEM_ACC_11")
                  setOption(OPT_channels_type, memory_allocation::MEM_ACC_11);
               else if (std::string(optarg) == "MEM_ACC_N1")
                  setOption(OPT_channels_type, memory_allocation::MEM_ACC_N1);
               else if (std::string(optarg) == "MEM_ACC_NN")
                  setOption(OPT_channels_type, memory_allocation::MEM_ACC_NN);
               else
                  throw "BadParameters: memory accesses type not correctly specified";
               break;
            }
            if (strcmp(long_options[option_index].name, "channels-number") == 0)
            {
               setOption(OPT_channels_number, optarg);
               break;
            }
            if (strcmp(long_options[option_index].name, "memory-ctrl-type") == 0)
            {
               if (std::string(optarg) == "D00")
                  setOption(OPT_memory_controller_type, "");
               else if (std::string(optarg) == "D10")
                  setOption(OPT_memory_controller_type, std::string("_")+optarg);
               else if (std::string(optarg) == "D11")
                  setOption(OPT_memory_controller_type, std::string("_")+optarg);
               else if (std::string(optarg) == "D21")
                  setOption(OPT_memory_controller_type, std::string("_")+optarg);
               else
                  throw "BadParameters: memory controller type not correctly specified";
               break;
            }
            if (strcmp(long_options[option_index].name, "sparse-memory") == 0)
            {
               if (!optarg || std::string(optarg) == "on")
                  setOption(OPT_sparse_memory, true);
               else if (std::string(optarg) == "off")
                  setOption(OPT_sparse_memory, false);
               else
                  throw "BadParameters: sparse-memory option not expected";
               break;
            }


#if HAVE_EXPERIMENTAL
            if (strcmp(long_options[option_index].name, "timing-simulation") == 0)
            {
               setOption(OPT_timing_simulation, true);
               break;
            }
            if (strcmp(long_options[option_index].name, "resp-model") == 0)
            {
               setOption(OPT_resp_model, optarg);
               break;
            }
#if (HAVE_XILINX || HAVE_ALTERA) && (HAVE_XILINX || HAVE_MODELSIM || HAVE_ICARUS || HAVE_VERILATOR) && HAVE_VCD_BUILT
            if (strcmp(long_options[option_index].name, "vcd-debug") == 0)
            {
               setOption("vcd-debug", true);
               setOption(OPT_generate_vcd, true);
               break;
            }
#endif
            //front-end analysis option
            if (strcmp(long_options[option_index].name, "pdg-reduction") == 0)
            {
               //Set the default
               if(optarg)
                  setOption("pdg-reduction", optarg);
               break;
            }
            if (strcmp(long_options[option_index].name, "explore-mux") == 0)
            {
               setOption("explore-mux", true);
               if (optarg) setOption("worst_case_delay", optarg);
               break;
            }
            if (strcmp(long_options[option_index].name, "explore-fu-reg") == 0)
            {
               setOption("explore-fu-reg", true);
               setOption("explore-fu-reg-param", optarg);
               break;
            }
#endif
            if (strcmp(long_options[option_index].name, "assert-debug") == 0)
            {
               setOption(OPT_assert_debug, true);
               break;
            }
            if (strcmp(long_options[option_index].name, "objective") == 0)
            {
               setOption(OPT_evaluation, true);
               setOption(OPT_evaluation_objectives, optarg);
               break;
            }
            if (strcmp(long_options[option_index].name, "circuit-dbg") == 0)
            {
               setOption(OPT_circuit_debug_level, optarg);
               break;
            }
            ///scheduling options
            if (strcmp(long_options[option_index].name, "no-chaining") == 0)
            {
               setOption(OPT_chaining, false);
               break;
            }
            if (strcmp(long_options[option_index].name, "generate-interface") == 0)
            {
               setOption(OPT_interface, true);
               if (std::string(optarg) == "MINIMAL")
               {
                  setOption(OPT_interface_type, module_interface::MINIMAL);
               }
               else if (std::string(optarg) == "WB4")
               {
                  setOption(OPT_interface_type, module_interface::WB4);
               }
               else if (std::string(optarg) == "AXI4LITE")
               {
                  setOption(OPT_interface_type, module_interface::AXI4LITE);
               }
#if HAVE_EXPERIMENTAL
               else if (std::string(optarg) == "FSL")
               {
                  setOption(OPT_interface_type, module_interface::FSL);
               }
               else if (std::string(optarg) == "NPI")
               {
                  setOption(OPT_interface_type, module_interface::NPI);
               }
#endif
               else
               {
                  THROW_ERROR("Not supported interface: " + std::string(optarg));
               }
               break;
            }
            if (strcmp(long_options[option_index].name, "export-core") == 0)
            {
               setOption(OPT_export_core, true);
               if (std::string(optarg) == "PCORE")
               {
                  setOption(OPT_export_core_mode, export_core::PCORE);
               }
               else
               {
                  THROW_ERROR("Not supported export mode: " + std::string(optarg));
               }
               break;
            }
            if (strcmp(long_options[option_index].name, "data-bus-bitsize") == 0)
            {
               setOption(OPT_data_bus_bitsize, boost::lexical_cast<int>(optarg));
               break;
            }
            if (strcmp(long_options[option_index].name, "addr-bus-bitsize") == 0)
            {
               setOption(OPT_addr_bus_bitsize, boost::lexical_cast<int>(optarg));
               break;
            }
#if HAVE_EXPERIMENTAL
            if (strcmp(long_options[option_index].name, "edk-config") == 0)
            {
               setOption("edk_wrapper", true);
               setOption("edk_config_file", std::string(optarg));
               break;
            }
#endif
            if (strcmp(long_options[option_index].name, "simulate") == 0)
            {
               setOption(OPT_generate_testbench, true);
               if (optarg) setOption("tb_input_xml", optarg);
               else setOption("tb_input_xml", "test.xml");

               std::string objective_string = getOption<std::string>(OPT_evaluation_objectives);
               std::vector<std::string> objective_vector = convert_string_to_vector<std::string>(objective_string, ",");
               if (std::find(objective_vector.begin(), objective_vector.end(), "CYCLES") == objective_vector.end())
               {
                  if(objective_string == "")
                     objective_string = "CYCLES";
                  else
                     objective_string = objective_string + ",CYCLES";
                  setOption(OPT_evaluation_objectives, objective_string);
                  setOption(OPT_evaluation_method, objective_evaluator::EXACT);
                  setOption(OPT_evaluation, true);
               }
               break;
            }
            if (strcmp(long_options[option_index].name, "simulator") == 0)
            {
               setOption(OPT_simulator, std::string(optarg));
               break;
            }
         }
         ///other options
         default:
         {
            bool exit_success=false;
            bool res = ManageGccOptions(next_option, optarg);
            if (res)
               res = ManageDefaultOptions(next_option, optarg, exit_success);
            if(exit_success)
               return EXIT_SUCCESS;
            if(res)
            {
               return PARAMETER_NOTPARSED;
            }
         }
      }
   }

#if HAVE_EXPERIMENTAL
   if (isOption(OPT_gcc_write_xml))
   {
      const std::string filenameXML = getOption<std::string>(OPT_gcc_write_xml);
      write_xml_configuration_file(filenameXML);
      PRINT_MSG("Configuration saved into file \"" + filenameXML + "\"");
      return EXIT_SUCCESS;
   }
#endif

   std::string cat_args;

   for(int i = 0; i < argc; i++)
   {
      cat_args += std::string(argv[i]) + " ";
   }
   setOption(OPT_cat_args, cat_args);

   if (getOption<int>(OPT_output_level) >= OUTPUT_LEVEL_MINIMUM)
   {
      std::cerr << " ==  Bambu executed with: ";
      std::cerr << cat_args;
      std::cerr << std::endl << std::endl;
   }

   bool is_constraints_file = true;
   bool is_tech_file = true;
   if (not getOption<bool>(OPT_use_raw))
   {
      while (optind < argc)
      {
         std::string extension_str = GetExtension(argv[optind]);
         if(extension_str == "xml" || extension_str == "XML")
         {
            if(is_constraints_file)
            {
               is_constraints_file = false;
               setOption("constraintsfile", argv[optind]);
            }
            else if(is_tech_file)
            {
               is_tech_file = false;
               setOption("techfile", argv[optind]);
            }
            else
               THROW_ERROR("BadParameters: only two configuration files are considered");
         }
         else
         {
            std::string input_file;
            if(isOption(OPT_input_file))
            {
               input_file = getOption<std::string>(OPT_input_file) + STR_CST_string_separator;
            }
            setOption(OPT_input_file, input_file + argv[optind]);
         }
         optind++;
      }
   }
   else
   {
      if (optind < argc)                    // the first non-option param is input file
      {
         std::string input_file;
         if(isOption(OPT_input_file))
         {
            input_file = getOption<std::string>(OPT_input_file) + STR_CST_string_separator;
         }
         setOption(OPT_input_file, input_file + argv[optind]);
      }
      optind++;
      if (optind < argc)                    // the second non-option param is constraint file
      {
         setOption("constraintsfile", argv[optind]);
      }
      optind++;
      if (optind < argc)                    // the third non-option param is tech file
      {
         setOption("techfile", argv[optind]);
      }

   }

   CheckParameters();

   return PARAMETER_PARSED;

}

void BambuParameter::CheckParameters()
{
   Parameter::CheckParameters();
   ///target device options
   if (not isOption(OPT_device_string))
   {
      std::string device_string = getOption<std::string>("device_name") + getOption<std::string>("device_speed")  + getOption<std::string>("device_package");
      if(isOption("device_synthesis_tool") && getOption<std::string>("device_synthesis_tool") != "")
         device_string += "-"+getOption<std::string>("device_synthesis_tool");
      setOption(OPT_device_string, device_string);
   }

   ///memory-related options
   if (getOption<unsigned int>(OPT_memory_allocation_algorithm) == memory_allocation::XML_SPECIFICATION && !isOption(OPT_xml_memory_allocation))
   {
      if (!isOption(OPT_xml_input_configuration))
         THROW_ERROR("XML specification of the memory allocation has not been specified!");
   }

   if ((getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_N1 || getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_NN) && !isOption(OPT_channels_number))
   {
      setOption(OPT_channels_number, 2);
   }
   if(isOption(OPT_channels_number) && getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_11)
   {
      THROW_ERROR("the number of channes cannot be specified for MEM_ACC_11");
   }
   if(isOption(OPT_channels_number) && (getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_N1 || getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_NN))
   {
      if(getOption<unsigned int>(OPT_channels_number)>2 &&
            getOption<unsigned int>(OPT_memory_allocation_policy) != memory_allocation::NO_BRAM &&
            getOption<unsigned int>(OPT_memory_allocation_policy) != memory_allocation::EXT_PIPELINED_BRAM)
         THROW_ERROR("no more than two channels is supported for MEM_ACC_N1 and MEM_ACC_NN: try to add this option --memory-allocation-policy=NO_BRAM or --memory-allocation-policy=EXT_PIPELINED_BRAM");
   }
   if(getOption<unsigned int>(OPT_channels_type) == memory_allocation::MEM_ACC_NN && isOption(OPT_interface_type) && getOption<unsigned int>(OPT_interface_type) == module_interface::WB4)
      THROW_ERROR("Wishbone 4 interface does not yet support multi-channel architectures (MEM_ACC_NN)");

   ///DSE options
#if HAVE_EXPERIMENTAL
#if HAVE_BEAGLE
   if (getOption<int>("exploration_technique") == dse_hls::PRIORITY and getOption<int>("to_normalize"))
      setOption("to_normalize", false);
   if (isOption("dse_algorithm") and getOption<unsigned int>("dse_algorithm") == dse_hls::ACO)
   {
      if (!isOption("aco_ant_num"))
         setOption("aco_ant_num", 5);
      if (!isOption("aco_generations"))
         setOption("aco_generations", 10);
   }
#endif
#endif

   ///circuit debugging options
   if (isOption(OPT_generate_vcd) and getOption<bool>(OPT_generate_vcd))
   {
      if (!isOption(OPT_generate_testbench) || !getOption<bool>(OPT_generate_testbench))
         THROW_ERROR("Testbench generation required. (--generate-tb or --simulate undeclared).");
      setOption(OPT_assert_debug, true);
   }

   ///controller options
   if (getOption<unsigned int>(OPT_controller_architecture) == controller_creator::FSM_CONTROLLER)
      setOption(OPT_stg, true);

   ///chaining options
   setOption(OPT_chaining_algorithm, chaining::SCHED_CHAINING);
#if HAVE_EXPERIMENTAL
   if (getOption<unsigned int>(OPT_controller_architecture) == controller_creator::PARALLEL_CONTROLLER)
   {
      setOption(OPT_liveness_algorithm, liveness_computer::CHAINING_BASED_LIVENESS);
      setOption(OPT_chaining_algorithm, chaining::EPDG_SCHED_CHAINING);
   }
#endif

   ///evaluation options
   if (getOption<unsigned int>(OPT_evaluation_method) == objective_evaluator::EXACT)
   {
      std::string objective_string = getOption<std::string>(OPT_evaluation_objectives);
      if(objective_string == "")
      {
#if HAVE_LIBRARY_CHARACTERIZATION_BUILT
         objective_string = "AREA,TIME,CYCLES,FREQUENCY,CLOCK_SLACK,REGISTERS,DSPS,BRAMS";
#else
         objective_string = "CYCLES";
#endif
         setOption(OPT_evaluation_objectives, objective_string);
      }
      std::vector<std::string> objective_vector = convert_string_to_vector<std::string>(objective_string, ",");
      if (std::find(objective_vector.begin(), objective_vector.end(), "TIME") != objective_vector.end() ||
          std::find(objective_vector.begin(), objective_vector.end(), "CYCLES") != objective_vector.end())
      {
         if(getOption<bool>(OPT_evaluation) && !getOption<bool>(OPT_generate_testbench))
         {
            setOption(OPT_generate_testbench, true);
            setOption("tb_input_xml", "test.xml");
         }
      }
   }

   ///Export and interface generation
   if (getOption<bool>(OPT_export_core) && getOption<unsigned int>(OPT_export_core_mode) == export_core::PCORE)
   {
      setOption(OPT_interface, true);
      setOption(OPT_interface_type, module_interface::AXI4LITE);
   }
   if(isOption(OPT_interface_type) && getOption<unsigned int>(OPT_interface_type) == module_interface::AXI4LITE
         )
   {
      if(isOption(OPT_evaluation_objectives) && getOption<std::string>(OPT_evaluation_objectives).find("AREA") != std::string::npos)
         THROW_ERROR("AXI-based synthesis has to be performed outside of bambu");
      else
         THROW_WARNING("Note that synthesis and simulation scripts generated by bambu for AXI-based designs are actually only templates not real synthesis scripts\n");
   }

   ///framework debugging options
   if (getOption<int>(OPT_debug_level)>=DEBUG_LEVEL_VERY_PEDANTIC)
   {
      setOption(OPT_assert_debug, true);
   }

   add_bambu_library("bambu");

   if(isOption(OPT_soft_float) && getOption<bool>(OPT_soft_float))
      add_bambu_library("softfloat");

   if(isOption(OPT_gcc_libraries))
   {
      const std::unordered_set<std::string> libraries = getOption<const std::unordered_set<std::string> >(OPT_gcc_libraries);
      std::unordered_set<std::string>::const_iterator library, library_end = libraries.end();
      for(library = libraries.begin(); library != library_end; library++)
      {
         add_bambu_library(*library);
      }
   }

}

void BambuParameter::SetDefaults()
{
   // ---------- general options ----------- //
   /// Revision
   setOption(OPT_dot_directory, "HLS_output/dot/");
   setOption(OPT_output_directory, "HLS_output/");
   setOption(OPT_simulation_output, "results.txt");
   /// Debugging level
   setOption(OPT_output_level, OUTPUT_LEVEL_MINIMUM);
   setOption(OPT_debug_level, DEBUG_LEVEL_NONE);
   setOption(OPT_circuit_debug_level, DEBUG_LEVEL_NONE);
   setOption(OPT_print_dot, false);
   /// maximum execution time: 0 means no time limit
   setOption(OPT_ilp_max_time, 0);
   setOption(OPT_use_raw, false);

#if HAVE_MAPPING_BUILT
   setOption(OPT_driving_component_type, "ARM");
#endif

   /// pragmas related
   setOption(OPT_parse_pragma, false);
   setOption(OPT_ignore_parallelism, false);

   /// ---------- frontend analysis ----------//   
#if HAVE_FROM_RTL_BUILT
   setOption(OPT_use_rtl, false);
#endif

   //setOption("pdg-reduction", false);

#if HAVE_EXPERIMENTAL
   setOption("compute_unrolling_degree", false);
   setOption("insert_verification_operation",false);
#endif
   setOption(OPT_frontend_statistics, false);

   /// ---------- HLS process options ----------- //
   setOption(OPT_synthesis_flow, HLS_synthesis_flow::CLASSICAL_SYNTHESIS);
   setOption(OPT_hls_flow, hls_flow::STANDARD);

   /// ---------- HLS specification reference ----------- //
   setOption(OPT_generate_testbench, false);
   setOption(OPT_max_sim_cycles, 200000000);
   setOption(OPT_chaining, true);

   /// High-level synthesis contraints dump -- //
   setOption("dumpConstraints", false);
   setOption("dumpConstraints_file", "Constraints.XML");

   /// -- Scheduling -- //
   /// Scheduling algorithm (default is list based one)
   setOption(OPT_scheduling_algorithm, scheduling::LIST_BASED);
   setOption(OPT_scheduling_priority, parametric_list_based::dynamic_mobility);
   /// ilp solver
#if HAVE_ILP_BUILT
#if HAVE_GLPK
   setOption(OPT_ilp_solver, meilp_solver::GLPK);
#elif HAVE_COIN_OR
   setOption(OPT_ilp_solver, meilp_solver::COIN_OR);
#elif HAVE_LP_SOLVE
   setOption(OPT_ilp_solver, meilp_solver::LP_SOLVE);
#endif
#endif
   /// speculative execution flag
   setOption(OPT_speculative, false);

   /// -- Module binding -- //
   /// module binding algorithm
   setOption(OPT_fu_binding_algorithm, fu_binding_creator::CDFC_WEIGHTED_TS);

   /// -- Finite state machine -- //
   /// flag to check if finite state machine has to be created
   setOption(OPT_stg, false);
   /// state transition graph creation algorithm
   setOption(OPT_stg_algorithm, STG_creator::BB_BASED);

   /// -- Dataflow analysis -- //
   setOption(OPT_liveness_algorithm, liveness_computer::FSM_NI_SSA_LIVENESS);

   /// -- Register allocation -- //
   /// register allocation algorithm
   setOption(OPT_register_allocation_algorithm, reg_binding_creator::WEIGHTED_COLORING);
   /// storage value insertion algorithm
   setOption(OPT_storage_value_insertion_algorithm, storage_value_insertion::VALUES);
   setOption(OPT_sync_reset, "no");

   /// -- Memory allocation -- //
   setOption(OPT_memory_allocation_algorithm, memory_allocation::DOMINATOR);
   setOption(OPT_memory_allocation_policy, memory_allocation::LSS);
   setOption(OPT_base_address, 2097152); // 2Mbytes maximum address space reserved for the accelerator
   setOption(OPT_channels_type, memory_allocation::MEM_ACC_11);
   setOption(OPT_memory_controller_type, "");
   setOption(OPT_sparse_memory, true);

   /// -- Datapath -- //
   /// Datapath interconnection architecture
   setOption(OPT_datapath_interconnection_algorithm, conn_binding_creator::MUX_INTERCONNECTION_BINDING);
   /// Datapath architecture
   setOption(OPT_datapath_architecture, datapath_creator::CLASSIC);

   /// -- Controller -- //
   /// target architecture for the controller
   setOption(OPT_controller_architecture, controller_creator::FSM_CONTROLLER);
#if HAVE_CUDD
   setOption("logical_optimization", 0);
#endif


   /// -- top entity -- //
   /// Output file name for top entity
   setOption(OPT_top_file, "top");

   /// backend HDL
   setOption(OPT_writer_language, language_writer::VERILOG);
   setOption("dynamic_generators_dir", std::string(PKGDATADIR)+"-etc");

   /// -- Module Interfaces -- //
   setOption(OPT_interface, true);
   setOption(OPT_interface_type, module_interface::MINIMAL);

   /// -- Module Characterization -- //
   setOption(OPT_evaluation, false);
   setOption(OPT_evaluation_method, objective_evaluator::NONE);
   setOption(OPT_evaluation_objectives, "");
#if HAVE_EXPERIMENTAL
   setOption("evaluation_statistic", false);
   setOption("evaluation_reduced", false);
   setOption("evaluation_output", "evaluation.txt");
   setOption("area_result", true);
   setOption("area_final_variable", "total_area");
#endif

   /// -- Module Synthesis -- //
   setOption(OPT_perform_synthesis, false);
   setOption(OPT_rtl, true); ///the resulting specification will be a RTL description
#ifdef HAVE_MODELSIM
   setOption(OPT_simulator, "MODELSIM"); /// Mixed language simulator
#elif HAVE_XILINX_VIVADO
   setOption(OPT_simulator, "XSIM"); /// Mixed language simulator
#elif HAVE_XILINX
   setOption(OPT_simulator, "ISIM"); /// Mixed language simulator
#elif HAVE_VERILATOR
   setOption(OPT_simulator, "VERILATOR");
#else
   setOption(OPT_simulator, "ICARUS");
#endif
   setOption("device_name", "xc7z020");
   setOption("device_speed", "-1");
   setOption("device_package", "clg484");
   setOption("device_synthesis_tool", "VVD");
   setOption(OPT_timing_simulation, false);
   setOption(OPT_target_device_type, target_device::FPGA);
   setOption(OPT_export_core, false);
#if HAVE_EXPERIMENTAL
   setOption("vcd-debug", false);
   setOption("edk_wrapper", false);
#endif
   setOption("connect_iob", true);

#if (HAVE_EXPERIMENTAL && HAVE_BEAGLE)
   // -- Parameters for the design space exploration -- //
   setOption("exploration_technique", dse_hls::BINDING);
   setOption("to_normalize", 0);
   setOption("seed", 0);
   setOption("run", 1);
   setOption("deme_size", 1);
   setOption("population", 50);
   setOption("GA_generation", 10);
   setOption("max_evaluations", 0);
   setOption("fitness_function", objective_evaluator::LINEAR);
   setOption("fitness_inheritance_rate", 0.0);
   /// if the parameter is even, the cache is analysed. Otherwise only the latest population
   setOption("inheritance_mode", 0);
   setOption("max_for_inheritance", 50);
   setOption("min_for_inheritance", 1);
   setOption("distance_rate", 0.20);
   setOption("weighting_function", FitnessFunction::QUADRATIC);
   setOption("time_weight", 0.5);
   setOption("area_weight", 0.5);
   setOption("remove_duplicated", true);
   setOption("lower", 0.05);
   setOption("upper", 0.05);
   setOption("prob_dupl", 0.50);
#endif

   /// -- GCC options -- //
#if HAVE_I386_GCC47_COMPILER
   setOption(OPT_default_compiler, CT_I386_GCC47);
#elif HAVE_I386_GCC46_COMPILER
   setOption(OPT_default_compiler, CT_I386_GCC46);
#elif HAVE_I386_GCC45_COMPILER
   setOption(OPT_default_compiler, CT_I386_GCC45);
#elif HAVE_I386_GCC48_COMPILER
   setOption(OPT_default_compiler, CT_I386_GCC48);
#elif HAVE_I386_GCC49_COMPILER
   setOption(OPT_default_compiler, CT_I386_GCC49);
#else
   THROW_ERROR("No GCC compiler available");
#endif
   setOption(OPT_compatible_compilers, 0
#if HAVE_I386_GCC45_COMPILER
      | CT_I386_GCC45
#endif
#if HAVE_I386_GCC46_COMPILER
      | CT_I386_GCC46
#endif
#if HAVE_I386_GCC47_COMPILER
      | CT_I386_GCC47
#endif
#if HAVE_I386_GCC48_COMPILER
      | CT_I386_GCC48
#endif
#if HAVE_I386_GCC49_COMPILER
      | CT_I386_GCC49
#endif
#if HAVE_ARM_COMPILER
      | CT_ARM_GCC
#endif
#if HAVE_SPARC_COMPILER
      | CT_SPARC_GCC
#endif
      );
   setOption(OPT_without_transformation, true);
   setOption(OPT_compute_size_of, true);
   setOption(OPT_precision, 3);
   setOption(OPT_gcc_opt_level, GccWrapper_OptimizationSet::O0);
   setOption(OPT_gcc_c, true);
   setOption(OPT_gcc_config, false);
   setOption(OPT_gcc_costs, false);
   setOption(OPT_gcc_standard, "gnu89");
   setOption(OPT_gcc_optimization_set, GccWrapper_OptimizationSet::OBAMBU);
   setOption(OPT_gcc_include_sysdir, false);
   setOption(OPT_model_costs, false);

   std::string defines;
   if(isOption(OPT_gcc_defines))
      defines = getOption<std::string>(OPT_gcc_defines) + STR_CST_string_separator;
   defines += "__BAMBU__";
   setOption(OPT_gcc_defines, defines);

#if !HAVE_FLOPOCO
   setOption(OPT_soft_float, true);
#endif
   setOption(OPT_max_ulp, 1.0);
   setOption(OPT_skip_pipe_parameter, 1);
   setOption(OPT_gcc_single_write, false);
}

void BambuParameter::add_bambu_library(std::string lib)
{
   unsigned int preferred_compiler = getOption<unsigned int>(OPT_default_compiler);
   std::string archive_files;
   if(isOption(OPT_archive_files))
      archive_files= getOption<std::string>(OPT_archive_files) + STR_CST_string_separator;
#if HAVE_I386_GCC45_COMPILER
   if(preferred_compiler & CT_I386_GCC45)
   {
      setOption(OPT_archive_files, archive_files+std::string(PKGLIBDIR)+"-etc/lib"+lib+"_gcc45.a");
   }
#endif
#if HAVE_I386_GCC46_COMPILER
   if(preferred_compiler & CT_I386_GCC46)
   {
      setOption(OPT_archive_files, archive_files+std::string(PKGLIBDIR)+"-etc/lib"+lib+"_gcc46.a");
   }
#endif
#if HAVE_I386_GCC47_COMPILER
   if(preferred_compiler & CT_I386_GCC47)
   {
      setOption(OPT_archive_files, archive_files+std::string(PKGLIBDIR)+"-etc/lib"+lib+"_gcc47.a");
   }
#endif
#if HAVE_I386_GCC48_COMPILER
   if(preferred_compiler & CT_I386_GCC48)
   {
      setOption(OPT_archive_files, archive_files+std::string(PKGLIBDIR)+"-etc/lib"+lib+"_gcc48.a");
   }
#endif
#if HAVE_I386_GCC49_COMPILER
   if(preferred_compiler & CT_I386_GCC49)
   {
      setOption(OPT_archive_files, archive_files+std::string(PKGLIBDIR)+"-etc/lib"+lib+"_gcc49.a");
   }
#endif
}
