/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.5"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */


/**
* Includes
*/

///Autoheader include
#include "config_HAVE_CODE_ESTIMATION_BUILT.hpp"
#include "config_HAVE_MAPPING_BUILT.hpp"
#include "config_HAVE_RTL_BUILT.hpp"
#include "config_HAVE_BISON_2_7_OR_GREATER.hpp"

#define YYLTYPE_IS_TRIVIAL 0

///Utility include
#include "refcount.hpp"

///NOTE: forward declarations of BisonParserData and TreeFlexLexer are in this point since they will be used in treeParser.h
REF_FORWARD_DECL(BisonParserData);
struct info_map;
REF_FORWARD_DECL(TreeFlexLexer);

///Header include
#if HAVE_BISON_2_7_OR_GREATER
#include "treeParser.hpp"
#else
#include "treeParser.h"
#endif
///Lexer include
#include "treeLexer.hpp"

///Machine include
#if HAVE_MAPPING_BUILT
#include "machine_node.hpp"
#endif

///Parameter include
#include "Parameter.hpp"

///RTL include
#if HAVE_RTL_BUILT
#include "rtl_common.hpp"
#endif

///STD include
#include <cstdio>
#include <cstring>
#include <fstream>
#include <string>
#include <iostream>

///Tree include
#include "tree_basic_block.hpp"
#include "tree_helper.hpp"
#include "tree_manager.hpp"
#include "ext_tree_node.hpp"
#include "tree_reindex.hpp"

///Utiliy include
#include "exceptions.hpp"
#include "fileIO.hpp"
#if HAVE_CODE_ESTIMATION_BUILT
#include "probability_distribution.hpp"
#include "weight_information.hpp"
#endif

///Wrapper include
#include "gcc_wrapper.hpp"
/**
* Defines
*/
//#define YYDEBUG 1
//#define NDEBUG 1
///Skipping warnings due to bison
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wwrite-strings"
#pragma GCC diagnostic ignored "-Wold-style-cast"
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
/**
* MACROS
*/

#if HAVE_RTL_BUILT
#define SET_DATA(data, value) data = value
#else
#define SET_DATA(data, value)
#endif

#define NO_OPT(cond, action) if(cond) action else YYABORT;
#define OPT(cond, action) if(cond) action

/**
 * Macro used to set a generic field of a generic object.
 * @param obj_node is the type of the object to which the field is setted.
 * @param field is the name of the member of the obj_node to be setted. The type of field member has to be tree_nodeRef.
*/
#define NS(obj_node, field) \
  assign_tree_reindex_to_member(data->curr_tree_nodeRef, &obj_node::field, data->curr_NODE_ID, data->current_TM);

/**
 * Macro used to set a generic field of a generic object.
 * @param curr is the current object reference.
 * @param obj_node is the type of the object to which the field is setted.
 * @param field is the name of the member of the obj_node to be setted. The type of field member has to be tree_nodeRef.
*/
#define NS2(curr, obj_node, field) \
  assign_tree_reindex_to_member(curr, &obj_node::field, data->curr_NODE_ID, data->current_TM);

/**
 * Macro used to set a generic field with a generic value of a generic object.
 * @param obj_node is the type of the object to which the field is setted.
 * @param field is the name of the member of the obj_node to be setted.
 * @param value is the value used to set the field member.
*/
#define NSV(obj_node, field, value) \
    assign_value_to_member(data->curr_tree_nodeRef, &obj_node::field, value);

/**
 * Macro used to set a generic field with a generic value of a generic object.
 * @param obj_node is the type of the object to which the field is setted.
 * @param field is the name of the member of the obj_node to be setted.
 * @param value is the value used to set the field member.
*/
#define NBV(obj_node, field, value) \
  assign_value_to_member<obj_node, blocRef, unsigned int>(data->curr_blocRef, &obj_node::field, value);

/**
 * Create the empty tree_node and set the curr_tree_nodeRef.
 * @param obj_node is the type of the tree_node to create.
*/
#define CTN(obj_node) \
  create_Ref_id<obj_node>(data, &BisonParserData::curr_tree_nodeRef);

/**
 * Create the identifier_node
*/
#define CTN_ID(parameter) \
  create_Ref_identifier(&BisonParserData::curr_tree_nodeRef, data, parameter, data->current_TM);

/**
 * Create the empty bloc and set the curr_blocRef.
 * @param obj_node is the type of the bloc to create.
*/
#define CB() \
  create_Ref<bloc,bloc>(&BisonParserData::curr_blocRef, data);

/**
 * Macro used to add attribute to the class attr.
 * @param token is the token associated to the attribute.
*/
#define ADD_ATTR(token) \
  add_attr<attr>(data->curr_tree_nodeRef, token);

/**
 * Macro used to add attribute to the class function_decl.
 * @param token is the token associated to the attribute.
*/
#define ADD_OP_NAME(token) \
  add_op_name<function_decl>(data->curr_tree_nodeRef, token);

/**
* Macro used to add a NODE_ID to the object obj_node through the function add_function.
* @param obj_node is the type of the object to which the field is setted.
* @param add_function is the name of function that add a NODE_ID
*/
#define ADDV(obj_node, add_function) \
  addv(data->curr_tree_nodeRef, &obj_node::add_function, data->curr_NODE_ID, data->current_TM);

/**
 * Macro used to add a phi node to the object obj_node through the function add_function.
 * @param obj_node is the type of the object to which the field is setted.
 * @param add_function is the name of function that add a phi
*/
#define ADDP(obj_node, add_function) \
  addv(data->curr_blocRef, &obj_node::add_function, data->curr_NODE_ID, data->current_TM);

/**
* Macro used to add a NODE_ID to the object obj_node through the function add_function.
* @param obj_node is the type of the object to which the field is setted.
* @param add_function is the name of function that add a NODE_ID
*/
#define ADDV2(obj_node, add_function) \
  addv2<obj_node, tree_nodeRef>(data->curr_tree_nodeRef, &obj_node::add_function, data->curr_NODE_ID_BIS, data->curr_NODE_ID, data->current_TM);

/**
* Macro used to add a NODE_ID to the object obj_node through the function add_function.
* @param obj_node is the type of the object to which the field is setted.
* @param add_function is the name of function that add a NODE_ID
*/
#define ADDV3(obj_node, add_function) \
  addv3<obj_node, tree_nodeRef>(data->curr_tree_nodeRef, &obj_node::add_function, data->curr_NODE_ID, data->curr_unumber, data->current_TM);

/**
* Macro used to add an unsigned int to the object obj_node through the function add_function.
* @param obj_node is the type of the object to which the field is setted.
* @param add_function is the name of function that add a NODE_ID
*/
#define ADD_UNSIGNED(obj_node, add_function) \
  add_unsigned<obj_node, tree_nodeRef>(data->curr_tree_nodeRef, &obj_node::add_function, data->curr_NODE_ID, data->curr_unumber, data->current_TM);

/**
* Macro used to add a bloc to the object obj_node through the function add_function.
* @param obj_node is the type of the object to which the field is setted.
* @param add_function is the name of function that add a bloc
* @param bloc is the bloc to add.
*/
#define ADDB(obj_node, add_function, vbloc) \
  addo<obj_node, tree_nodeRef, blocRef>(data->curr_tree_nodeRef, &obj_node::add_function, vbloc);

/**
 * Macro used to add a value to the object obj_node through the function add_function.
 * @param obj_node is the type of the object to which the field is setted.
 * @param add_function is the name of function that add a pred
 * @param val is the value to add.
*/
#define ADDVALUE(obj_node, add_function, val) \
  addo(data->curr_blocRef, &obj_node::add_function, val);


/**
 * Macro used to add access_binf
 * @param obj_node is the type of the object to which the field is setted.
 * @param add_function is the name of function that add a vuse
 * @param binf is the binf
 * @param access is a token between TOK_PUB TOK_PROT TOK_PRIV.
*/
#define ADD_ACCESS_BINF(obj_node, add_function) \
  add_access_binf(data->curr_tree_nodeRef, data->curr_NODE_ID, data->curr_token_enum, data->current_TM);


struct BisonParserData
{
   BisonParserData(const ParameterConstRef _Param, int _debug_level) :
      Param(_Param),
      debug_level(_debug_level),
      curr_NODE_ID(0),
      curr_NODE_ID_BIS(0),
      implement_node(false),
      curr_number(0),
      curr_size_t_number(0),
      id(0)
   {
#if HAVE_MAPPING_BUILT
      std::string driving_component_string = Param->getOption<std::string>(OPT_driving_component_type);
      driving_component = processingElement::get_component_type((driving_component_string));
#endif
   }
   //the set of parameter
   const ParameterConstRef Param;

   ///debug level
   int debug_level;

   /// Current tree_node reference
   tree_nodeRef curr_tree_nodeRef;

   /// Current bloc reference
   blocRef curr_blocRef;

   /// Current string reference
   std::string curr_string;

   /// GCC version
   std::string gcc_version;

   /// Store the current NODE_ID as integer.
   unsigned int curr_NODE_ID;

   /// Store the current NODE_ID_BIS as integer.
   unsigned int curr_NODE_ID_BIS;

   /// Is true if the current node is an implementation node
   bool implement_node;

   /// Store the current signed NUMBER.
   int curr_number;

   /// Store the current size_t NUMBER.
   size_t curr_size_t_number;

   /// Store the current unsigned NUMBER
   unsigned int curr_unumber;

   /// Store the current NUMBER (long long)
   int long long curr_long_long_number;

   /// Store the current NUMBER (long double)
   long double curr_double_number;

   /// Store the current token_enum (long double)
   treeVocabularyTokenTypes::token_enum curr_token_enum;

   /// Store the current id of the node. Used by node to identify the id of the current node.
   int id;

   ///merge all the raw read
   tree_managerRef final_TM;

   ///current tree manager
   tree_managerRef current_TM;

#if HAVE_RTL_BUILT
   ///current list of enum rtl_kind
   std::list<std::pair<enum rtl_kind, enum mode_kind> > rtls;

   ///current rtl
   enum rtl_kind single_rtl;

   ///current mode_kind
   enum mode_kind mode_rtl;
#endif

#if HAVE_MAPPING_BUILT
   ///The type of the driving_component
   ComponentTypeConstRef driving_component;
#endif

   /// unique version identifier used for SSA_NAMEs objects
   static unsigned int global_uniq_vers_id;
};

unsigned int BisonParserData::global_uniq_vers_id = 1;

/**
* Local Data Structures
*/

/**
* Global Data Structures
*/

/** 
* Function Prototypes
*/
extern int yylex(YYSTYPE *lvalp, const TreeFlexLexerRef lexer);
extern void yyerror(const BisonParserDataRef data, const TreeFlexLexerRef lexer, char *s);

/**
* Templates
*/
template<class obj_node, class TRef, class value_t>
inline
void assign_value_to_member(const TRef &curr, value_t obj_node::* field, const value_t value)
  {
    THROW_ASSERT(curr, "");
    ((dynamic_cast<obj_node*>(curr.get())) ->* field) = value;
  }

template<class obj_node, class TRef>
inline
void assign_tree_reindex_to_member(const TRef &curr, tree_nodeRef obj_node::* field, const unsigned int tree_index, const tree_managerRef TM)
  {
    THROW_ASSERT(curr, "");
((dynamic_cast<obj_node*>(curr.get())) ->* field) = TM->GetTreeReindex(tree_index);
  }

template<class obj_node, class ret>
inline
void create_Ref( refcount<ret> BisonParserData::* field, const BisonParserDataRef data)
  {
   (*data .* field) = refcount<ret>(new obj_node());
  }

template<class obj_node>
inline
void create_Ref_id(const BisonParserDataRef data, tree_nodeRef BisonParserData::* field)
{
    (*data .* field) = tree_nodeRef(new obj_node(data->id));
}

template<class constructor_parameter_type>
inline
void create_Ref_identifier(tree_nodeRef BisonParserData::* field, const BisonParserDataRef data, constructor_parameter_type par, tree_managerRef const &Root)
{
    (*data .* field) = tree_nodeRef(new identifier_node(data->id,par,Root.get()));
}

template<class attr>
inline
void add_attr(const tree_nodeRef &curr, const int &token_i)
  {
    (dynamic_cast<attr*>(curr.get()))->add(token_i);
  }

template<class attr>
inline
void add_attr_node(const tree_nodeRef &curr, const unsigned int tree_index, const tree_managerRef TM)
  {
    (dynamic_cast<attr*>(curr.get()))->add(TM->GetTreeReindex(tree_index));
  }

template<class function_decl>
inline
void add_op_name(const tree_nodeRef &curr, const std::string & token_s)
  {
    (dynamic_cast<function_decl*>(curr.get()))->add(token_s);
  }

template<class obj_node, class TRef>
inline
void  addv(const TRef &curr, void (obj_node::*add_function)(const tree_nodeRef), const unsigned int tree_index, const tree_managerRef TM)
  {
((dynamic_cast<obj_node*>(curr.get()))->*add_function)(TM->GetTreeReindex(tree_index));
  }

template<class obj_node, class TRef>
inline
void  addv2(const TRef &curr, void (obj_node::*add_function)(const tree_nodeRef, const tree_nodeRef), const unsigned int tree_index_1, const unsigned int tree_index_2, const tree_managerRef TM)
{
((dynamic_cast<obj_node*>(curr.get()))->*add_function)(TM->GetTreeReindex(tree_index_1), TM->GetTreeReindex(tree_index_2));
}

template<class obj_node, class TRef>
inline
void  addv3(const TRef &curr, void (obj_node::*add_function)(const tree_nodeRef, int), const unsigned int tree_index, const int value, const tree_managerRef TM)
{
((dynamic_cast<obj_node*>(curr.get()))->*add_function)(TM->GetTreeReindex(tree_index), value);
}

template<class obj_node, class TRef>
inline
void  add_unsigned(const TRef &curr, void (obj_node::*add_function)(const tree_nodeRef, unsigned int), const unsigned int tree_index, const unsigned int value, const tree_managerRef TM)
{
   ((dynamic_cast<obj_node*>(curr.get()))->*add_function)(TM->GetTreeReindex(tree_index), value);
}


template<class obj_node, class TRefCurr, class TRef>
inline
void  addo(const TRefCurr &curr, void (obj_node::*add_function)(const TRef), const TRef & obj)
  {
    ((dynamic_cast<obj_node*>(curr.get()))->*add_function)(obj);
  }

inline
void add_access_binf(const tree_nodeRef & curr, const unsigned int binf_index, const treeVocabularyTokenTypes::token_enum enum_access, const tree_managerRef TM)
{
   (dynamic_cast<binfo*>(curr.get()))->add_access_binf(TM->GetTreeReindex(binf_index), enum_access);
}

  
inline
void do_fun_decl(const bool &in, const int & cnib, const int & cni, const BisonParserDataRef data, const tree_managerRef TM)
  {
    TM->add_function(cni, data->curr_tree_nodeRef);
  }

template<class type_node>
inline
void do_qual(const tree_nodeRef &curr, const int & value)
  {
    THROW_ASSERT(curr, "");
    ((dynamic_cast<type_node*>(curr.get()))->qual) = value;
  }





/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     NODE_ID = 258,
     TOK_BISON_STRING = 259,
     TOK_BISON_NUMBER = 260,
     TOK_BISON_GCC_VERSION = 261,
     TOK_BISON_PLUGIN_VERSION = 262,
     TOK_BISON_ERROR_MARK = 263,
     TOK_BISON_IDENTIFIER_NODE = 264,
     TOK_BISON_TREE_LIST = 265,
     TOK_BISON_TREE_VEC = 266,
     TOK_BISON_BLOCK = 267,
     TOK_BISON_VOID_TYPE = 268,
     TOK_BISON_INTEGER_TYPE = 269,
     TOK_BISON_REAL_TYPE = 270,
     TOK_BISON_COMPLEX_TYPE = 271,
     TOK_BISON_VECTOR_TYPE = 272,
     TOK_BISON_ENUMERAL_TYPE = 273,
     TOK_BISON_BOOLEAN_TYPE = 274,
     TOK_BISON_CHAR_TYPE = 275,
     TOK_BISON_POINTER_TYPE = 276,
     TOK_BISON_OFFSET_TYPE = 277,
     TOK_BISON_REFERENCE_TYPE = 278,
     TOK_BISON_METHOD_TYPE = 279,
     TOK_BISON_ARRAY_TYPE = 280,
     TOK_BISON_SET_TYPE = 281,
     TOK_BISON_RECORD_TYPE = 282,
     TOK_BISON_UNION_TYPE = 283,
     TOK_BISON_QUAL_UNION_TYPE = 284,
     TOK_BISON_FUNCTION_TYPE = 285,
     TOK_BISON_LANG_TYPE = 286,
     TOK_BISON_INTEGER_CST = 287,
     TOK_BISON_REAL_CST = 288,
     TOK_BISON_COMPLEX_CST = 289,
     TOK_BISON_VECTOR_CST = 290,
     TOK_BISON_STRING_CST = 291,
     TOK_BISON_FUNCTION_DECL = 292,
     TOK_BISON_LABEL_DECL = 293,
     TOK_BISON_CONST_DECL = 294,
     TOK_BISON_TYPE_DECL = 295,
     TOK_BISON_VAR_DECL = 296,
     TOK_BISON_PARM_DECL = 297,
     TOK_BISON_RESULT_DECL = 298,
     TOK_BISON_FIELD_DECL = 299,
     TOK_BISON_NAMESPACE_DECL = 300,
     TOK_BISON_TRANSLATION_UNIT_DECL = 301,
     TOK_BISON_COMPONENT_REF = 302,
     TOK_BISON_BIT_FIELD_REF = 303,
     TOK_BISON_INDIRECT_REF = 304,
     TOK_BISON_MISALIGNED_INDIRECT_REF = 305,
     TOK_BISON_BUFFER_REF = 306,
     TOK_BISON_ARRAY_REF = 307,
     TOK_BISON_ARRAY_RANGE_REF = 308,
     TOK_BISON_VTABLE_REF = 309,
     TOK_BISON_CONSTRUCTOR = 310,
     TOK_BISON_DESTRUCTOR = 311,
     TOK_BISON_COMPOUND_EXPR = 312,
     TOK_BISON_MODIFY_EXPR = 313,
     TOK_BISON_GIMPLE_ASSIGN = 314,
     TOK_BISON_INIT_EXPR = 315,
     TOK_BISON_TARGET_EXPR = 316,
     TOK_BISON_COND_EXPR = 317,
     TOK_BISON_GIMPLE_COND = 318,
     TOK_BISON_GIMPLE_BIND = 319,
     TOK_BISON_GIMPLE_CALL = 320,
     TOK_BISON_CALL_EXPR = 321,
     TOK_BISON_WITH_CLEANUP_EXPR = 322,
     TOK_BISON_GIMPLE_NOP = 323,
     TOK_BISON_CLEANUP_POINT_EXPR = 324,
     TOK_BISON_PLACEHOLDER_EXPR = 325,
     TOK_BISON_REDUC_MAX_EXPR = 326,
     TOK_BISON_REDUC_MIN_EXPR = 327,
     TOK_BISON_REDUC_PLUS_EXPR = 328,
     TOK_BISON_PLUS_EXPR = 329,
     TOK_BISON_MINUS_EXPR = 330,
     TOK_BISON_MULT_EXPR = 331,
     TOK_BISON_TRUNC_DIV_EXPR = 332,
     TOK_BISON_CEIL_DIV_EXPR = 333,
     TOK_BISON_FLOOR_DIV_EXPR = 334,
     TOK_BISON_ROUND_DIV_EXPR = 335,
     TOK_BISON_TRUNC_MOD_EXPR = 336,
     TOK_BISON_CEIL_MOD_EXPR = 337,
     TOK_BISON_FLOOR_MOD_EXPR = 338,
     TOK_BISON_ROUND_MOD_EXPR = 339,
     TOK_BISON_RDIV_EXPR = 340,
     TOK_BISON_EXACT_DIV_EXPR = 341,
     TOK_BISON_FIX_TRUNC_EXPR = 342,
     TOK_BISON_FIX_CEIL_EXPR = 343,
     TOK_BISON_FIX_FLOOR_EXPR = 344,
     TOK_BISON_FIX_ROUND_EXPR = 345,
     TOK_BISON_FLOAT_EXPR = 346,
     TOK_BISON_NEGATE_EXPR = 347,
     TOK_BISON_MIN_EXPR = 348,
     TOK_BISON_MAX_EXPR = 349,
     TOK_BISON_ABS_EXPR = 350,
     TOK_BISON_LSHIFT_EXPR = 351,
     TOK_BISON_RSHIFT_EXPR = 352,
     TOK_BISON_LROTATE_EXPR = 353,
     TOK_BISON_RROTATE_EXPR = 354,
     TOK_BISON_BIT_IOR_EXPR = 355,
     TOK_BISON_BIT_XOR_EXPR = 356,
     TOK_BISON_BIT_AND_EXPR = 357,
     TOK_BISON_BIT_NOT_EXPR = 358,
     TOK_BISON_TRUTH_ANDIF_EXPR = 359,
     TOK_BISON_TRUTH_ORIF_EXPR = 360,
     TOK_BISON_TRUTH_AND_EXPR = 361,
     TOK_BISON_TRUTH_OR_EXPR = 362,
     TOK_BISON_TRUTH_XOR_EXPR = 363,
     TOK_BISON_TRUTH_NOT_EXPR = 364,
     TOK_BISON_LT_EXPR = 365,
     TOK_BISON_LE_EXPR = 366,
     TOK_BISON_GT_EXPR = 367,
     TOK_BISON_GE_EXPR = 368,
     TOK_BISON_EQ_EXPR = 369,
     TOK_BISON_NE_EXPR = 370,
     TOK_BISON_UNORDERED_EXPR = 371,
     TOK_BISON_ORDERED_EXPR = 372,
     TOK_BISON_UNLT_EXPR = 373,
     TOK_BISON_UNLE_EXPR = 374,
     TOK_BISON_UNGT_EXPR = 375,
     TOK_BISON_UNGE_EXPR = 376,
     TOK_BISON_UNEQ_EXPR = 377,
     TOK_BISON_LTGT_EXPR = 378,
     TOK_BISON_IN_EXPR = 379,
     TOK_BISON_SET_LE_EXPR = 380,
     TOK_BISON_CARD_EXPR = 381,
     TOK_BISON_RANGE_EXPR = 382,
     TOK_BISON_CONVERT_EXPR = 383,
     TOK_BISON_NOP_EXPR = 384,
     TOK_BISON_NON_LVALUE_EXPR = 385,
     TOK_BISON_VIEW_CONVERT_EXPR = 386,
     TOK_BISON_SAVE_EXPR = 387,
     TOK_BISON_UNSAVE_EXPR = 388,
     TOK_BISON_ADDR_EXPR = 389,
     TOK_BISON_REFERENCE_EXPR = 390,
     TOK_BISON_FDESC_EXPR = 391,
     TOK_BISON_COMPLEX_EXPR = 392,
     TOK_BISON_CONJ_EXPR = 393,
     TOK_BISON_REALPART_EXPR = 394,
     TOK_BISON_IMAGPART_EXPR = 395,
     TOK_BISON_PREDECREMENT_EXPR = 396,
     TOK_BISON_PREINCREMENT_EXPR = 397,
     TOK_BISON_POSTDECREMENT_EXPR = 398,
     TOK_BISON_POSTINCREMENT_EXPR = 399,
     TOK_BISON_VA_ARG_EXPR = 400,
     TOK_BISON_TRY_CATCH_EXPR = 401,
     TOK_BISON_TRY_FINALLY = 402,
     TOK_BISON_ = 403,
     TOK_BISON_GIMPLE_GOTO = 404,
     TOK_BISON_GOTO_SUBROUTINE = 405,
     TOK_BISON_GIMPLE_RETURN = 406,
     TOK_BISON_EXIT_EXPR = 407,
     TOK_BISON_LOOP_EXPR = 408,
     TOK_BISON_GIMPLE_SWITCH = 409,
     TOK_BISON_GIMPLE_MULTI_WAY_IF = 410,
     TOK_BISON_CASE_LABEL_EXPR = 411,
     TOK_BISON_GIMPLE_RESX = 412,
     TOK_BISON_GIMPLE_ASM = 413,
     TOK_BISON_SSA_NAME = 414,
     TOK_BISON_GIMPLE_PHI = 415,
     TOK_BISON_CATCH_EXPR = 416,
     TOK_BISON_EH_FILTER_EXPR = 417,
     TOK_BISON_STATEMENT_LIST = 418,
     TOK_BISON_TEMPLATE_DECL = 419,
     TOK_BISON_TEMPLATE_TYPE_PARM = 420,
     TOK_BISON_CAST_EXPR = 421,
     TOK_BISON_STATIC_CAST_EXPR = 422,
     TOK_BISON_TYPENAME_TYPE = 423,
     TOK_BISON_SIZEOF_EXPR = 424,
     TOK_BISON_AGGR_INIT_EXPR = 425,
     TOK_BISON_SCOPE_REF = 426,
     TOK_BISON_CTOR_INITIALIZER = 427,
     TOK_BISON_DO_STMT = 428,
     TOK_BISON_EXPR_STMT = 429,
     TOK_BISON_FOR_STMT = 430,
     TOK_BISON_IF_STMT = 431,
     TOK_BISON_RETURN_STMT = 432,
     TOK_BISON_WHILE_STMT = 433,
     TOK_BISON_MODOP_EXPR = 434,
     TOK_BISON_NEW_EXPR = 435,
     TOK_BISON_VEC_COND_EXPR = 436,
     TOK_BISON_VEC_PERM_EXPR = 437,
     TOK_BISON_DOT_PROD_EXPR = 438,
     TOK_BISON_VEC_LSHIFT_EXPR = 439,
     TOK_BISON_VEC_RSHIFT_EXPR = 440,
     TOK_BISON_WIDEN_MULT_HI_EXPR = 441,
     TOK_BISON_WIDEN_MULT_LO_EXPR = 442,
     TOK_BISON_VEC_UNPACK_HI_EXPR = 443,
     TOK_BISON_VEC_UNPACK_LO_EXPR = 444,
     TOK_BISON_VEC_UNPACK_FLOAT_HI_EXPR = 445,
     TOK_BISON_VEC_UNPACK_FLOAT_LO_EXPR = 446,
     TOK_BISON_VEC_PACK_TRUNC_EXPR = 447,
     TOK_BISON_VEC_PACK_SAT_EXPR = 448,
     TOK_BISON_VEC_PACK_FIX_TRUNC_EXPR = 449,
     TOK_BISON_VEC_EXTRACTEVEN_EXPR = 450,
     TOK_BISON_VEC_EXTRACTODD_EXPR = 451,
     TOC_BISON_VEC_INTERLEAVEHIGH_EXPR = 452,
     TOC_BISON_VEC_INTERLEAVELOW_EXPR = 453,
     TOK_BISON_VEC_NEW_EXPR = 454,
     TOK_BISON_OVERLOAD = 455,
     TOK_BISON_REINTERPRET_CAST_EXPR = 456,
     TOK_BISON_TEMPLATE_ID_EXPR = 457,
     TOK_BISON_THROW_EXPR = 458,
     TOK_BISON_TRY_BLOCK = 459,
     TOK_BISON_ARROW_EXPR = 460,
     TOK_BISON_HANDLER = 461,
     TOK_BISON_BASELINK = 462,
     TOK_BISON_NAME = 463,
     TOK_BISON_TYPE = 464,
     TOK_BISON_SRCP = 465,
     TOK_BISON_ARG = 466,
     TOK_BISON_BODY = 467,
     TOK_BISON_STRG = 468,
     TOK_BISON_LNGT = 469,
     TOK_BISON_SIZE = 470,
     TOK_BISON_ALGN = 471,
     TOK_BISON_RETN = 472,
     TOK_BISON_PRMS = 473,
     TOK_BISON_SCPE = 474,
     TOK_BISON_USED = 475,
     TOK_BISON_VALUE = 476,
     TOK_BISON_ARGT = 477,
     TOK_BISON_PREC = 478,
     TOK_BISON_BB_INDEX = 479,
     TOK_BISON_MIN = 480,
     TOK_BISON_MAX = 481,
     TOK_BISON_VALU = 482,
     TOK_BISON_CHAN = 483,
     TOK_BISON_STMT = 484,
     TOK_BISON_OP = 485,
     TOK_BISON_TIME_WEIGHT = 486,
     TOK_BISON_SIZE_WEIGHT = 487,
     TOK_BISON_RTL_SIZE_WEIGHT = 488,
     TOK_BISON_VARS = 489,
     TOK_BISON_UNQL = 490,
     TOK_BISON_ELTS = 491,
     TOK_BISON_DOMN = 492,
     TOK_BISON_BLOC = 493,
     TOK_BISON_DCLS = 494,
     TOK_BISON_MNGL = 495,
     TOK_BISON_PTD = 496,
     TOK_BISON_REFD = 497,
     TOK_BISON_QUAL = 498,
     TOK_BISON_VALR = 499,
     TOK_BISON_VALX = 500,
     TOK_BISON_FLDS = 501,
     TOK_BISON_VFLD = 502,
     TOK_BISON_BPOS = 503,
     TOK_BISON_FN = 504,
     TOK_BISON_GOTO = 505,
     TOK_BISON_REAL = 506,
     TOK_BISON_IMAG = 507,
     TOK_BISON_BINFO = 508,
     TOK_BISON_PUB = 509,
     TOK_BISON_PROT = 510,
     TOK_BISON_PRIV = 511,
     TOK_BISON_BINF = 512,
     TOK_BISON_UID = 513,
     TOK_BISON_OLD_UID = 514,
     TOK_BISON_INIT = 515,
     TOK_BISON_FINI = 516,
     TOK_BISON_PURP = 517,
     TOK_BISON_PRED = 518,
     TOK_BISON_SUCC = 519,
     TOK_BISON_HPL = 520,
     TOK_BISON_LOOP_ID = 521,
     TOK_BISON_ATTRIBUTES = 522,
     TOK_BISON_PHI = 523,
     TOK_BISON_RES = 524,
     TOK_BISON_DEF = 525,
     TOK_BISON_EDGE = 526,
     TOK_BISON_VAR = 527,
     TOK_BISON_DEF_STMT = 528,
     TOK_BISON_ADDR_STMT = 529,
     TOK_BISON_USE_STMT = 530,
     TOK_BISON_VERS = 531,
     TOK_BISON_ORIG_VERS = 532,
     TOK_BISON_CNST = 533,
     TOK_BISON_CLAS = 534,
     TOK_BISON_DECL = 535,
     TOK_BISON_CLNP = 536,
     TOK_BISON_LAB = 537,
     TOK_BISON_TRY = 538,
     TOK_BISON_EX = 539,
     TOK_BISON_OUT = 540,
     TOK_BISON_IN = 541,
     TOK_BISON_STR = 542,
     TOK_BISON_CLOB = 543,
     TOK_BISON_CLOBBER = 544,
     TOK_BISON_REF = 545,
     TOK_BISON_FNCS = 546,
     TOK_BISON_CSTS = 547,
     TOK_BISON_RSLT = 548,
     TOK_BISON_INST = 549,
     TOK_BISON_SPCS = 550,
     TOK_BISON_CLS = 551,
     TOK_BISON_BFLD = 552,
     TOK_BISON_CTOR = 553,
     TOK_BISON_NEXT = 554,
     TOK_BISON_COND = 555,
     TOK_BISON_EXPR = 556,
     TOK_BISON_THEN = 557,
     TOK_BISON_ELSE = 558,
     TOK_BISON_CRNT = 559,
     TOK_BISON_HDLR = 560,
     TOK_BISON_USE_TMPL = 561,
     TOK_BISON_TMPL_PARMS = 562,
     TOK_BISON_TMPL_ARGS = 563,
     TOK_BISON_ARTIFICIAL = 564,
     TOK_BISON_SYSTEM = 565,
     TOK_BISON_OPERATING_SYSTEM = 566,
     TOK_BISON_LIBRARY_SYSTEM = 567,
     TOK_BISON_EXTERN = 568,
     TOK_BISON_C = 569,
     TOK_BISON_LSHIFT = 570,
     TOK_BISON_GLOBAL_INIT = 571,
     TOK_BISON_GLOBAL_FINI = 572,
     TOK_BISON_UNDEFINED = 573,
     TOK_BISON_BUILTIN = 574,
     TOK_BISON_OPERATOR = 575,
     TOK_BISON_OVERFLOW = 576,
     TOK_BISON_VIRT = 577,
     TOK_BISON_UNSIGNED = 578,
     TOK_BISON_STRUCT = 579,
     TOK_BISON_UNION = 580,
     TOK_BISON_REGISTER = 581,
     TOK_BISON_STATIC = 582,
     TOK_BISON_REVERSE_RESTRICT = 583,
     TOK_BISON_DEFAULT = 584,
     TOK_BISON_VOLATILE = 585,
     TOK_BISON_INF = 586,
     TOK_BISON_NAN = 587,
     TOK_BISON_VARARGS = 588,
     TOK_BISON_ENTRY = 589,
     TOK_BISON_EXIT = 590,
     TOK_BISON_NEW = 591,
     TOK_BISON_DELETE = 592,
     TOK_BISON_ASSIGN = 593,
     TOK_BISON_MEMBER = 594,
     TOK_BISON_PUBLIC = 595,
     TOK_BISON_PRIVATE = 596,
     TOK_BISON_PROTECTED = 597,
     TOK_BISON_NORETURN = 598,
     TOK_BISON_NOINLINE = 599,
     TOK_BISON_ALWAYS_INLINE = 600,
     TOK_BISON_UNUSED = 601,
     TOK_BISON_CONST = 602,
     TOK_BISON_TRANSPARENT_UNION = 603,
     TOK_BISON_MODE = 604,
     TOK_BISON_SECTION = 605,
     TOK_BISON_ALIGNED = 606,
     TOK_BISON_PACKED = 607,
     TOK_BISON_WEAK = 608,
     TOK_BISON_ALIAS = 609,
     TOK_BISON_NO_INSTRUMENT_FUNCTION = 610,
     TOK_BISON_MALLOC = 611,
     TOK_BISON_NO_STACK_LIMIT = 612,
     TOK_BISON_NO_STACK = 613,
     TOK_BISON_PURE = 614,
     TOK_BISON_DEPRECATED = 615,
     TOK_BISON_VECTOR_SIZE = 616,
     TOK_BISON_VISIBILITY = 617,
     TOK_BISON_TLS_MODEL = 618,
     TOK_BISON_NONNULL = 619,
     TOK_BISON_NOTHROW = 620,
     TOK_BISON_MAY_ALIAS = 621,
     TOK_BISON_WARN_UNUSED_RESULT = 622,
     TOK_BISON_FORMAT = 623,
     TOK_BISON_FORMAT_ARG = 624,
     TOK_BISON_NULL = 625,
     TOK_BISON_CONVERSION = 626,
     TOK_BISON_VIRTUAL = 627,
     TOK_BISON_MUTABLE = 628,
     TOK_BISON_PSEUDO_TMPL = 629,
     TOK_BISON_SPEC = 630,
     TOK_BISON_LINE = 631,
     TOK_BISON_FIXD = 632,
     TOK_BISON_VECNEW = 633,
     TOK_BISON_VECDELETE = 634,
     TOK_BISON_POS = 635,
     TOK_BISON_NEG = 636,
     TOK_BISON_ADDR = 637,
     TOK_BISON_DEREF = 638,
     TOK_BISON_NOT = 639,
     TOK_BISON_LNOT = 640,
     TOK_BISON_PREINC = 641,
     TOK_BISON_PREDEC = 642,
     TOK_BISON_PLUSASSIGN = 643,
     TOK_BISON_PLUS = 644,
     TOK_BISON_MINUSASSIGN = 645,
     TOK_BISON_MINUS = 646,
     TOK_BISON_MULTASSIGN = 647,
     TOK_BISON_MULT = 648,
     TOK_BISON_DIVASSIGN = 649,
     TOK_BISON_DIV = 650,
     TOK_BISON_MODASSIGN = 651,
     TOK_BISON_MOD = 652,
     TOK_BISON_ANDASSIGN = 653,
     TOK_BISON_AND = 654,
     TOK_BISON_ORASSIGN = 655,
     TOK_BISON_OR = 656,
     TOK_BISON_XORASSIGN = 657,
     TOK_BISON_XOR = 658,
     TOK_BISON_LSHIFTASSIGN = 659,
     TOK_BISON_RSHIFTASSIGN = 660,
     TOK_BISON_RSHIFT = 661,
     TOK_BISON_EQ = 662,
     TOK_BISON_NE = 663,
     TOK_BISON_LT = 664,
     TOK_BISON_GT = 665,
     TOK_BISON_LE = 666,
     TOK_BISON_GE = 667,
     TOK_BISON_LAND = 668,
     TOK_BISON_LOR = 669,
     TOK_BISON_COMPOUND = 670,
     TOK_BISON_MEMREF = 671,
     TOK_BISON_SUBS = 672,
     TOK_BISON_POSTINC = 673,
     TOK_BISON_POSTDEC = 674,
     TOK_BISON_CALL = 675,
     TOK_BISON_THUNK = 676,
     TOK_BISON_THIS_ADJUSTING = 677,
     TOK_BISON_RESULT_ADJUSTING = 678,
     TOK_BISON_PTRMEM = 679,
     TOK_BISON_QUAL_R = 680,
     TOK_BISON_QUAL_V = 681,
     TOK_BISON_QUAL_VR = 682,
     TOK_BISON_QUAL_C = 683,
     TOK_BISON_QUAL_CR = 684,
     TOK_BISON_QUAL_CV = 685,
     TOK_BISON_QUAL_CVR = 686,
     TOK_BISON_TEMPLATE_PARM_INDEX = 687,
     TOK_BISON_INLINE_BODY = 688,
     TOK_BISON_BITFIELD = 689,
     TOK_BISON_WITH_SIZE_EXPR = 690,
     TOK_BISON_OBJ_TYPE_REF = 691,
     TOK_BISON_VUSE = 692,
     TOK_BISON_VDEF = 693,
     TOK_BISON_REDEF_VUSE = 694,
     TOK_BISON_VDEP_VUSE = 695,
     TOK_BISON_PTR_INFO = 696,
     TOK_BISON_TRUE_EDGE = 697,
     TOK_BISON_FALSE_EDGE = 698,
     TOK_BISON_POINTER_PLUS_EXPR = 699,
     TOK_BISON_TARGET_MEM_REF = 700,
     TOK_BISON_TARGET_MEM_REF461 = 701,
     TOK_BISON_SYMBOL = 702,
     TOK_BISON_BASE = 703,
     TOK_BISON_IDX = 704,
     TOK_BISON_IDX2 = 705,
     TOK_BISON_STEP = 706,
     TOK_BISON_OFFSET = 707,
     TOK_BISON_ORIG = 708,
     TOK_BISON_TAG = 709,
     TOK_BISON_SMT_ANN = 710,
     TOK_BISON_TRAIT_EXPR = 711,
     TOK_BISON_GIMPLE_PREDICT = 712,
     TOK_BISON_MEM_REF = 713,
     TOK_BISON_WIDEN_SUM_EXPR = 714,
     TOK_BISON_WIDEN_MULT_EXPR = 715,
     TOK_BISON_ASSERT_EXPR = 716,
     TOK_BISON_RTL = 717,
     TOK_BISON_ABS_R = 718,
     TOK_BISON_AND_R = 719,
     TOK_BISON_ASHIFT_R = 720,
     TOK_BISON_ASHIFTRT_R = 721,
     TOK_BISON_BSWAP_R = 722,
     TOK_BISON_CALL_R = 723,
     TOK_BISON_CALL_INSN_R = 724,
     TOK_BISON_CLZ_R = 725,
     TOK_BISON_CODE_LABEL_R = 726,
     TOK_BISON_COMPARE_R = 727,
     TOK_BISON_CONCAT_R = 728,
     TOK_BISON_CTZ_R = 729,
     TOK_BISON_DIV_R = 730,
     TOK_BISON_EQ_R = 731,
     TOK_BISON_FFS_R = 732,
     TOK_BISON_FIX_R = 733,
     TOK_BISON_FLOAT_R = 734,
     TOK_BISON_FLOAT_EXTEND_R = 735,
     TOK_BISON_FLOAT_TRUNCATE_R = 736,
     TOK_BISON_FRACT_CONVERT_R = 737,
     TOK_BISON_GE_R = 738,
     TOK_BISON_GEU_R = 739,
     TOK_BISON_GT_R = 740,
     TOK_BISON_GTU_R = 741,
     TOK_BISON_HIGH_R = 742,
     TOK_BISON_IF_THEN_ELSE_R = 743,
     TOK_BISON_INSN_R = 744,
     TOK_BISON_IOR_R = 745,
     TOK_BISON_JUMP_INSN_R = 746,
     TOK_BISON_LABEL_REF_R = 747,
     TOK_BISON_LE_R = 748,
     TOK_BISON_LEU_R = 749,
     TOK_BISON_LSHIFTRT_R = 750,
     TOK_BISON_LO_SUM_R = 751,
     TOK_BISON_LT_R = 752,
     TOK_BISON_LTGT_R = 753,
     TOK_BISON_LTU_R = 754,
     TOK_BISON_WRITE_MEM_R = 755,
     TOK_BISON_READ_MEM_R = 756,
     TOK_BISON_MINUS_R = 757,
     TOK_BISON_MOD_R = 758,
     TOK_BISON_MULT_R = 759,
     TOK_BISON_NE_R = 760,
     TOK_BISON_NEG_R = 761,
     TOK_BISON_NOT_R = 762,
     TOK_BISON_ORDERED_R = 763,
     TOK_BISON_PARALLEL_R = 764,
     TOK_BISON_PARITY_R = 765,
     TOK_BISON_PC_R = 766,
     TOK_BISON_PLUS_R = 767,
     TOK_BISON_POPCOUNT_R = 768,
     TOK_BISON_REG_R = 769,
     TOK_BISON_ROTATE_R = 770,
     TOK_BISON_ROTATERT_R = 771,
     TOK_BISON_SAT_FRACT_R = 772,
     TOK_BISON_SET_R = 773,
     TOK_BISON_SIGN_EXTEND_R = 774,
     TOK_BISON_SMAX_R = 775,
     TOK_BISON_SMIN_R = 776,
     TOK_BISON_SQRT_R = 777,
     TOK_BISON_SYMBOL_REF_R = 778,
     TOK_BISON_TRUNCATE_R = 779,
     TOK_BISON_UDIV_R = 780,
     TOK_BISON_UMAX_R = 781,
     TOK_BISON_UMIN_R = 782,
     TOK_BISON_UMOD_R = 783,
     TOK_BISON_UNEQ_R = 784,
     TOK_BISON_UNGE_R = 785,
     TOK_BISON_UNGT_R = 786,
     TOK_BISON_UNLE_R = 787,
     TOK_BISON_UNLT_R = 788,
     TOK_BISON_UNORDERED_R = 789,
     TOK_BISON_UNSIGNED_FIX_R = 790,
     TOK_BISON_UNSIGNED_FLOAT_R = 791,
     TOK_BISON_UNSIGNED_FRACT_CONVERT_R = 792,
     TOK_BISON_UNSIGNED_SAT_FRACT_R = 793,
     TOK_BISON_XOR_R = 794,
     TOK_BISON_ZERO_EXTEND_R = 795,
     TOK_BISON_QC_R = 796,
     TOK_BISON_HC_R = 797,
     TOK_BISON_SC_R = 798,
     TOK_BISON_DC_R = 799,
     TOK_BISON_TC_R = 800,
     TOK_BISON_CQI_R = 801,
     TOK_BISON_CHI_R = 802,
     TOK_BISON_CSI_R = 803,
     TOK_BISON_CDI_R = 804,
     TOK_BISON_CTI_R = 805,
     TOK_BISON_QF_R = 806,
     TOK_BISON_HF_R = 807,
     TOK_BISON_SF_R = 808,
     TOK_BISON_DF_R = 809,
     TOK_BISON_TF_R = 810,
     TOK_BISON_QI_R = 811,
     TOK_BISON_HI_R = 812,
     TOK_BISON_SI_R = 813,
     TOK_BISON_DO_R = 814,
     TOK_BISON_TI_R = 815,
     TOK_BISON_V2SI_R = 816,
     TOK_BISON_V4HI_R = 817,
     TOK_BISON_V8QI_R = 818,
     TOK_BISON_CC_R = 819,
     TOK_BISON_CCFP_R = 820,
     TOK_BISON_CCFPE_R = 821,
     TOK_BISON_CCZ_R = 822,
     TOK_BISON_CLB = 823,
     TOK_BISON_CLB_VARS = 824,
     TOK_BISON_USE = 825,
     TOK_BISON_USE_VARS = 826
   };
#endif
/* Tokens.  */
#define NODE_ID 258
#define TOK_BISON_STRING 259
#define TOK_BISON_NUMBER 260
#define TOK_BISON_GCC_VERSION 261
#define TOK_BISON_PLUGIN_VERSION 262
#define TOK_BISON_ERROR_MARK 263
#define TOK_BISON_IDENTIFIER_NODE 264
#define TOK_BISON_TREE_LIST 265
#define TOK_BISON_TREE_VEC 266
#define TOK_BISON_BLOCK 267
#define TOK_BISON_VOID_TYPE 268
#define TOK_BISON_INTEGER_TYPE 269
#define TOK_BISON_REAL_TYPE 270
#define TOK_BISON_COMPLEX_TYPE 271
#define TOK_BISON_VECTOR_TYPE 272
#define TOK_BISON_ENUMERAL_TYPE 273
#define TOK_BISON_BOOLEAN_TYPE 274
#define TOK_BISON_CHAR_TYPE 275
#define TOK_BISON_POINTER_TYPE 276
#define TOK_BISON_OFFSET_TYPE 277
#define TOK_BISON_REFERENCE_TYPE 278
#define TOK_BISON_METHOD_TYPE 279
#define TOK_BISON_ARRAY_TYPE 280
#define TOK_BISON_SET_TYPE 281
#define TOK_BISON_RECORD_TYPE 282
#define TOK_BISON_UNION_TYPE 283
#define TOK_BISON_QUAL_UNION_TYPE 284
#define TOK_BISON_FUNCTION_TYPE 285
#define TOK_BISON_LANG_TYPE 286
#define TOK_BISON_INTEGER_CST 287
#define TOK_BISON_REAL_CST 288
#define TOK_BISON_COMPLEX_CST 289
#define TOK_BISON_VECTOR_CST 290
#define TOK_BISON_STRING_CST 291
#define TOK_BISON_FUNCTION_DECL 292
#define TOK_BISON_LABEL_DECL 293
#define TOK_BISON_CONST_DECL 294
#define TOK_BISON_TYPE_DECL 295
#define TOK_BISON_VAR_DECL 296
#define TOK_BISON_PARM_DECL 297
#define TOK_BISON_RESULT_DECL 298
#define TOK_BISON_FIELD_DECL 299
#define TOK_BISON_NAMESPACE_DECL 300
#define TOK_BISON_TRANSLATION_UNIT_DECL 301
#define TOK_BISON_COMPONENT_REF 302
#define TOK_BISON_BIT_FIELD_REF 303
#define TOK_BISON_INDIRECT_REF 304
#define TOK_BISON_MISALIGNED_INDIRECT_REF 305
#define TOK_BISON_BUFFER_REF 306
#define TOK_BISON_ARRAY_REF 307
#define TOK_BISON_ARRAY_RANGE_REF 308
#define TOK_BISON_VTABLE_REF 309
#define TOK_BISON_CONSTRUCTOR 310
#define TOK_BISON_DESTRUCTOR 311
#define TOK_BISON_COMPOUND_EXPR 312
#define TOK_BISON_MODIFY_EXPR 313
#define TOK_BISON_GIMPLE_ASSIGN 314
#define TOK_BISON_INIT_EXPR 315
#define TOK_BISON_TARGET_EXPR 316
#define TOK_BISON_COND_EXPR 317
#define TOK_BISON_GIMPLE_COND 318
#define TOK_BISON_GIMPLE_BIND 319
#define TOK_BISON_GIMPLE_CALL 320
#define TOK_BISON_CALL_EXPR 321
#define TOK_BISON_WITH_CLEANUP_EXPR 322
#define TOK_BISON_GIMPLE_NOP 323
#define TOK_BISON_CLEANUP_POINT_EXPR 324
#define TOK_BISON_PLACEHOLDER_EXPR 325
#define TOK_BISON_REDUC_MAX_EXPR 326
#define TOK_BISON_REDUC_MIN_EXPR 327
#define TOK_BISON_REDUC_PLUS_EXPR 328
#define TOK_BISON_PLUS_EXPR 329
#define TOK_BISON_MINUS_EXPR 330
#define TOK_BISON_MULT_EXPR 331
#define TOK_BISON_TRUNC_DIV_EXPR 332
#define TOK_BISON_CEIL_DIV_EXPR 333
#define TOK_BISON_FLOOR_DIV_EXPR 334
#define TOK_BISON_ROUND_DIV_EXPR 335
#define TOK_BISON_TRUNC_MOD_EXPR 336
#define TOK_BISON_CEIL_MOD_EXPR 337
#define TOK_BISON_FLOOR_MOD_EXPR 338
#define TOK_BISON_ROUND_MOD_EXPR 339
#define TOK_BISON_RDIV_EXPR 340
#define TOK_BISON_EXACT_DIV_EXPR 341
#define TOK_BISON_FIX_TRUNC_EXPR 342
#define TOK_BISON_FIX_CEIL_EXPR 343
#define TOK_BISON_FIX_FLOOR_EXPR 344
#define TOK_BISON_FIX_ROUND_EXPR 345
#define TOK_BISON_FLOAT_EXPR 346
#define TOK_BISON_NEGATE_EXPR 347
#define TOK_BISON_MIN_EXPR 348
#define TOK_BISON_MAX_EXPR 349
#define TOK_BISON_ABS_EXPR 350
#define TOK_BISON_LSHIFT_EXPR 351
#define TOK_BISON_RSHIFT_EXPR 352
#define TOK_BISON_LROTATE_EXPR 353
#define TOK_BISON_RROTATE_EXPR 354
#define TOK_BISON_BIT_IOR_EXPR 355
#define TOK_BISON_BIT_XOR_EXPR 356
#define TOK_BISON_BIT_AND_EXPR 357
#define TOK_BISON_BIT_NOT_EXPR 358
#define TOK_BISON_TRUTH_ANDIF_EXPR 359
#define TOK_BISON_TRUTH_ORIF_EXPR 360
#define TOK_BISON_TRUTH_AND_EXPR 361
#define TOK_BISON_TRUTH_OR_EXPR 362
#define TOK_BISON_TRUTH_XOR_EXPR 363
#define TOK_BISON_TRUTH_NOT_EXPR 364
#define TOK_BISON_LT_EXPR 365
#define TOK_BISON_LE_EXPR 366
#define TOK_BISON_GT_EXPR 367
#define TOK_BISON_GE_EXPR 368
#define TOK_BISON_EQ_EXPR 369
#define TOK_BISON_NE_EXPR 370
#define TOK_BISON_UNORDERED_EXPR 371
#define TOK_BISON_ORDERED_EXPR 372
#define TOK_BISON_UNLT_EXPR 373
#define TOK_BISON_UNLE_EXPR 374
#define TOK_BISON_UNGT_EXPR 375
#define TOK_BISON_UNGE_EXPR 376
#define TOK_BISON_UNEQ_EXPR 377
#define TOK_BISON_LTGT_EXPR 378
#define TOK_BISON_IN_EXPR 379
#define TOK_BISON_SET_LE_EXPR 380
#define TOK_BISON_CARD_EXPR 381
#define TOK_BISON_RANGE_EXPR 382
#define TOK_BISON_CONVERT_EXPR 383
#define TOK_BISON_NOP_EXPR 384
#define TOK_BISON_NON_LVALUE_EXPR 385
#define TOK_BISON_VIEW_CONVERT_EXPR 386
#define TOK_BISON_SAVE_EXPR 387
#define TOK_BISON_UNSAVE_EXPR 388
#define TOK_BISON_ADDR_EXPR 389
#define TOK_BISON_REFERENCE_EXPR 390
#define TOK_BISON_FDESC_EXPR 391
#define TOK_BISON_COMPLEX_EXPR 392
#define TOK_BISON_CONJ_EXPR 393
#define TOK_BISON_REALPART_EXPR 394
#define TOK_BISON_IMAGPART_EXPR 395
#define TOK_BISON_PREDECREMENT_EXPR 396
#define TOK_BISON_PREINCREMENT_EXPR 397
#define TOK_BISON_POSTDECREMENT_EXPR 398
#define TOK_BISON_POSTINCREMENT_EXPR 399
#define TOK_BISON_VA_ARG_EXPR 400
#define TOK_BISON_TRY_CATCH_EXPR 401
#define TOK_BISON_TRY_FINALLY 402
#define TOK_BISON_ 403
#define TOK_BISON_GIMPLE_GOTO 404
#define TOK_BISON_GOTO_SUBROUTINE 405
#define TOK_BISON_GIMPLE_RETURN 406
#define TOK_BISON_EXIT_EXPR 407
#define TOK_BISON_LOOP_EXPR 408
#define TOK_BISON_GIMPLE_SWITCH 409
#define TOK_BISON_GIMPLE_MULTI_WAY_IF 410
#define TOK_BISON_CASE_LABEL_EXPR 411
#define TOK_BISON_GIMPLE_RESX 412
#define TOK_BISON_GIMPLE_ASM 413
#define TOK_BISON_SSA_NAME 414
#define TOK_BISON_GIMPLE_PHI 415
#define TOK_BISON_CATCH_EXPR 416
#define TOK_BISON_EH_FILTER_EXPR 417
#define TOK_BISON_STATEMENT_LIST 418
#define TOK_BISON_TEMPLATE_DECL 419
#define TOK_BISON_TEMPLATE_TYPE_PARM 420
#define TOK_BISON_CAST_EXPR 421
#define TOK_BISON_STATIC_CAST_EXPR 422
#define TOK_BISON_TYPENAME_TYPE 423
#define TOK_BISON_SIZEOF_EXPR 424
#define TOK_BISON_AGGR_INIT_EXPR 425
#define TOK_BISON_SCOPE_REF 426
#define TOK_BISON_CTOR_INITIALIZER 427
#define TOK_BISON_DO_STMT 428
#define TOK_BISON_EXPR_STMT 429
#define TOK_BISON_FOR_STMT 430
#define TOK_BISON_IF_STMT 431
#define TOK_BISON_RETURN_STMT 432
#define TOK_BISON_WHILE_STMT 433
#define TOK_BISON_MODOP_EXPR 434
#define TOK_BISON_NEW_EXPR 435
#define TOK_BISON_VEC_COND_EXPR 436
#define TOK_BISON_VEC_PERM_EXPR 437
#define TOK_BISON_DOT_PROD_EXPR 438
#define TOK_BISON_VEC_LSHIFT_EXPR 439
#define TOK_BISON_VEC_RSHIFT_EXPR 440
#define TOK_BISON_WIDEN_MULT_HI_EXPR 441
#define TOK_BISON_WIDEN_MULT_LO_EXPR 442
#define TOK_BISON_VEC_UNPACK_HI_EXPR 443
#define TOK_BISON_VEC_UNPACK_LO_EXPR 444
#define TOK_BISON_VEC_UNPACK_FLOAT_HI_EXPR 445
#define TOK_BISON_VEC_UNPACK_FLOAT_LO_EXPR 446
#define TOK_BISON_VEC_PACK_TRUNC_EXPR 447
#define TOK_BISON_VEC_PACK_SAT_EXPR 448
#define TOK_BISON_VEC_PACK_FIX_TRUNC_EXPR 449
#define TOK_BISON_VEC_EXTRACTEVEN_EXPR 450
#define TOK_BISON_VEC_EXTRACTODD_EXPR 451
#define TOC_BISON_VEC_INTERLEAVEHIGH_EXPR 452
#define TOC_BISON_VEC_INTERLEAVELOW_EXPR 453
#define TOK_BISON_VEC_NEW_EXPR 454
#define TOK_BISON_OVERLOAD 455
#define TOK_BISON_REINTERPRET_CAST_EXPR 456
#define TOK_BISON_TEMPLATE_ID_EXPR 457
#define TOK_BISON_THROW_EXPR 458
#define TOK_BISON_TRY_BLOCK 459
#define TOK_BISON_ARROW_EXPR 460
#define TOK_BISON_HANDLER 461
#define TOK_BISON_BASELINK 462
#define TOK_BISON_NAME 463
#define TOK_BISON_TYPE 464
#define TOK_BISON_SRCP 465
#define TOK_BISON_ARG 466
#define TOK_BISON_BODY 467
#define TOK_BISON_STRG 468
#define TOK_BISON_LNGT 469
#define TOK_BISON_SIZE 470
#define TOK_BISON_ALGN 471
#define TOK_BISON_RETN 472
#define TOK_BISON_PRMS 473
#define TOK_BISON_SCPE 474
#define TOK_BISON_USED 475
#define TOK_BISON_VALUE 476
#define TOK_BISON_ARGT 477
#define TOK_BISON_PREC 478
#define TOK_BISON_BB_INDEX 479
#define TOK_BISON_MIN 480
#define TOK_BISON_MAX 481
#define TOK_BISON_VALU 482
#define TOK_BISON_CHAN 483
#define TOK_BISON_STMT 484
#define TOK_BISON_OP 485
#define TOK_BISON_TIME_WEIGHT 486
#define TOK_BISON_SIZE_WEIGHT 487
#define TOK_BISON_RTL_SIZE_WEIGHT 488
#define TOK_BISON_VARS 489
#define TOK_BISON_UNQL 490
#define TOK_BISON_ELTS 491
#define TOK_BISON_DOMN 492
#define TOK_BISON_BLOC 493
#define TOK_BISON_DCLS 494
#define TOK_BISON_MNGL 495
#define TOK_BISON_PTD 496
#define TOK_BISON_REFD 497
#define TOK_BISON_QUAL 498
#define TOK_BISON_VALR 499
#define TOK_BISON_VALX 500
#define TOK_BISON_FLDS 501
#define TOK_BISON_VFLD 502
#define TOK_BISON_BPOS 503
#define TOK_BISON_FN 504
#define TOK_BISON_GOTO 505
#define TOK_BISON_REAL 506
#define TOK_BISON_IMAG 507
#define TOK_BISON_BINFO 508
#define TOK_BISON_PUB 509
#define TOK_BISON_PROT 510
#define TOK_BISON_PRIV 511
#define TOK_BISON_BINF 512
#define TOK_BISON_UID 513
#define TOK_BISON_OLD_UID 514
#define TOK_BISON_INIT 515
#define TOK_BISON_FINI 516
#define TOK_BISON_PURP 517
#define TOK_BISON_PRED 518
#define TOK_BISON_SUCC 519
#define TOK_BISON_HPL 520
#define TOK_BISON_LOOP_ID 521
#define TOK_BISON_ATTRIBUTES 522
#define TOK_BISON_PHI 523
#define TOK_BISON_RES 524
#define TOK_BISON_DEF 525
#define TOK_BISON_EDGE 526
#define TOK_BISON_VAR 527
#define TOK_BISON_DEF_STMT 528
#define TOK_BISON_ADDR_STMT 529
#define TOK_BISON_USE_STMT 530
#define TOK_BISON_VERS 531
#define TOK_BISON_ORIG_VERS 532
#define TOK_BISON_CNST 533
#define TOK_BISON_CLAS 534
#define TOK_BISON_DECL 535
#define TOK_BISON_CLNP 536
#define TOK_BISON_LAB 537
#define TOK_BISON_TRY 538
#define TOK_BISON_EX 539
#define TOK_BISON_OUT 540
#define TOK_BISON_IN 541
#define TOK_BISON_STR 542
#define TOK_BISON_CLOB 543
#define TOK_BISON_CLOBBER 544
#define TOK_BISON_REF 545
#define TOK_BISON_FNCS 546
#define TOK_BISON_CSTS 547
#define TOK_BISON_RSLT 548
#define TOK_BISON_INST 549
#define TOK_BISON_SPCS 550
#define TOK_BISON_CLS 551
#define TOK_BISON_BFLD 552
#define TOK_BISON_CTOR 553
#define TOK_BISON_NEXT 554
#define TOK_BISON_COND 555
#define TOK_BISON_EXPR 556
#define TOK_BISON_THEN 557
#define TOK_BISON_ELSE 558
#define TOK_BISON_CRNT 559
#define TOK_BISON_HDLR 560
#define TOK_BISON_USE_TMPL 561
#define TOK_BISON_TMPL_PARMS 562
#define TOK_BISON_TMPL_ARGS 563
#define TOK_BISON_ARTIFICIAL 564
#define TOK_BISON_SYSTEM 565
#define TOK_BISON_OPERATING_SYSTEM 566
#define TOK_BISON_LIBRARY_SYSTEM 567
#define TOK_BISON_EXTERN 568
#define TOK_BISON_C 569
#define TOK_BISON_LSHIFT 570
#define TOK_BISON_GLOBAL_INIT 571
#define TOK_BISON_GLOBAL_FINI 572
#define TOK_BISON_UNDEFINED 573
#define TOK_BISON_BUILTIN 574
#define TOK_BISON_OPERATOR 575
#define TOK_BISON_OVERFLOW 576
#define TOK_BISON_VIRT 577
#define TOK_BISON_UNSIGNED 578
#define TOK_BISON_STRUCT 579
#define TOK_BISON_UNION 580
#define TOK_BISON_REGISTER 581
#define TOK_BISON_STATIC 582
#define TOK_BISON_REVERSE_RESTRICT 583
#define TOK_BISON_DEFAULT 584
#define TOK_BISON_VOLATILE 585
#define TOK_BISON_INF 586
#define TOK_BISON_NAN 587
#define TOK_BISON_VARARGS 588
#define TOK_BISON_ENTRY 589
#define TOK_BISON_EXIT 590
#define TOK_BISON_NEW 591
#define TOK_BISON_DELETE 592
#define TOK_BISON_ASSIGN 593
#define TOK_BISON_MEMBER 594
#define TOK_BISON_PUBLIC 595
#define TOK_BISON_PRIVATE 596
#define TOK_BISON_PROTECTED 597
#define TOK_BISON_NORETURN 598
#define TOK_BISON_NOINLINE 599
#define TOK_BISON_ALWAYS_INLINE 600
#define TOK_BISON_UNUSED 601
#define TOK_BISON_CONST 602
#define TOK_BISON_TRANSPARENT_UNION 603
#define TOK_BISON_MODE 604
#define TOK_BISON_SECTION 605
#define TOK_BISON_ALIGNED 606
#define TOK_BISON_PACKED 607
#define TOK_BISON_WEAK 608
#define TOK_BISON_ALIAS 609
#define TOK_BISON_NO_INSTRUMENT_FUNCTION 610
#define TOK_BISON_MALLOC 611
#define TOK_BISON_NO_STACK_LIMIT 612
#define TOK_BISON_NO_STACK 613
#define TOK_BISON_PURE 614
#define TOK_BISON_DEPRECATED 615
#define TOK_BISON_VECTOR_SIZE 616
#define TOK_BISON_VISIBILITY 617
#define TOK_BISON_TLS_MODEL 618
#define TOK_BISON_NONNULL 619
#define TOK_BISON_NOTHROW 620
#define TOK_BISON_MAY_ALIAS 621
#define TOK_BISON_WARN_UNUSED_RESULT 622
#define TOK_BISON_FORMAT 623
#define TOK_BISON_FORMAT_ARG 624
#define TOK_BISON_NULL 625
#define TOK_BISON_CONVERSION 626
#define TOK_BISON_VIRTUAL 627
#define TOK_BISON_MUTABLE 628
#define TOK_BISON_PSEUDO_TMPL 629
#define TOK_BISON_SPEC 630
#define TOK_BISON_LINE 631
#define TOK_BISON_FIXD 632
#define TOK_BISON_VECNEW 633
#define TOK_BISON_VECDELETE 634
#define TOK_BISON_POS 635
#define TOK_BISON_NEG 636
#define TOK_BISON_ADDR 637
#define TOK_BISON_DEREF 638
#define TOK_BISON_NOT 639
#define TOK_BISON_LNOT 640
#define TOK_BISON_PREINC 641
#define TOK_BISON_PREDEC 642
#define TOK_BISON_PLUSASSIGN 643
#define TOK_BISON_PLUS 644
#define TOK_BISON_MINUSASSIGN 645
#define TOK_BISON_MINUS 646
#define TOK_BISON_MULTASSIGN 647
#define TOK_BISON_MULT 648
#define TOK_BISON_DIVASSIGN 649
#define TOK_BISON_DIV 650
#define TOK_BISON_MODASSIGN 651
#define TOK_BISON_MOD 652
#define TOK_BISON_ANDASSIGN 653
#define TOK_BISON_AND 654
#define TOK_BISON_ORASSIGN 655
#define TOK_BISON_OR 656
#define TOK_BISON_XORASSIGN 657
#define TOK_BISON_XOR 658
#define TOK_BISON_LSHIFTASSIGN 659
#define TOK_BISON_RSHIFTASSIGN 660
#define TOK_BISON_RSHIFT 661
#define TOK_BISON_EQ 662
#define TOK_BISON_NE 663
#define TOK_BISON_LT 664
#define TOK_BISON_GT 665
#define TOK_BISON_LE 666
#define TOK_BISON_GE 667
#define TOK_BISON_LAND 668
#define TOK_BISON_LOR 669
#define TOK_BISON_COMPOUND 670
#define TOK_BISON_MEMREF 671
#define TOK_BISON_SUBS 672
#define TOK_BISON_POSTINC 673
#define TOK_BISON_POSTDEC 674
#define TOK_BISON_CALL 675
#define TOK_BISON_THUNK 676
#define TOK_BISON_THIS_ADJUSTING 677
#define TOK_BISON_RESULT_ADJUSTING 678
#define TOK_BISON_PTRMEM 679
#define TOK_BISON_QUAL_R 680
#define TOK_BISON_QUAL_V 681
#define TOK_BISON_QUAL_VR 682
#define TOK_BISON_QUAL_C 683
#define TOK_BISON_QUAL_CR 684
#define TOK_BISON_QUAL_CV 685
#define TOK_BISON_QUAL_CVR 686
#define TOK_BISON_TEMPLATE_PARM_INDEX 687
#define TOK_BISON_INLINE_BODY 688
#define TOK_BISON_BITFIELD 689
#define TOK_BISON_WITH_SIZE_EXPR 690
#define TOK_BISON_OBJ_TYPE_REF 691
#define TOK_BISON_VUSE 692
#define TOK_BISON_VDEF 693
#define TOK_BISON_REDEF_VUSE 694
#define TOK_BISON_VDEP_VUSE 695
#define TOK_BISON_PTR_INFO 696
#define TOK_BISON_TRUE_EDGE 697
#define TOK_BISON_FALSE_EDGE 698
#define TOK_BISON_POINTER_PLUS_EXPR 699
#define TOK_BISON_TARGET_MEM_REF 700
#define TOK_BISON_TARGET_MEM_REF461 701
#define TOK_BISON_SYMBOL 702
#define TOK_BISON_BASE 703
#define TOK_BISON_IDX 704
#define TOK_BISON_IDX2 705
#define TOK_BISON_STEP 706
#define TOK_BISON_OFFSET 707
#define TOK_BISON_ORIG 708
#define TOK_BISON_TAG 709
#define TOK_BISON_SMT_ANN 710
#define TOK_BISON_TRAIT_EXPR 711
#define TOK_BISON_GIMPLE_PREDICT 712
#define TOK_BISON_MEM_REF 713
#define TOK_BISON_WIDEN_SUM_EXPR 714
#define TOK_BISON_WIDEN_MULT_EXPR 715
#define TOK_BISON_ASSERT_EXPR 716
#define TOK_BISON_RTL 717
#define TOK_BISON_ABS_R 718
#define TOK_BISON_AND_R 719
#define TOK_BISON_ASHIFT_R 720
#define TOK_BISON_ASHIFTRT_R 721
#define TOK_BISON_BSWAP_R 722
#define TOK_BISON_CALL_R 723
#define TOK_BISON_CALL_INSN_R 724
#define TOK_BISON_CLZ_R 725
#define TOK_BISON_CODE_LABEL_R 726
#define TOK_BISON_COMPARE_R 727
#define TOK_BISON_CONCAT_R 728
#define TOK_BISON_CTZ_R 729
#define TOK_BISON_DIV_R 730
#define TOK_BISON_EQ_R 731
#define TOK_BISON_FFS_R 732
#define TOK_BISON_FIX_R 733
#define TOK_BISON_FLOAT_R 734
#define TOK_BISON_FLOAT_EXTEND_R 735
#define TOK_BISON_FLOAT_TRUNCATE_R 736
#define TOK_BISON_FRACT_CONVERT_R 737
#define TOK_BISON_GE_R 738
#define TOK_BISON_GEU_R 739
#define TOK_BISON_GT_R 740
#define TOK_BISON_GTU_R 741
#define TOK_BISON_HIGH_R 742
#define TOK_BISON_IF_THEN_ELSE_R 743
#define TOK_BISON_INSN_R 744
#define TOK_BISON_IOR_R 745
#define TOK_BISON_JUMP_INSN_R 746
#define TOK_BISON_LABEL_REF_R 747
#define TOK_BISON_LE_R 748
#define TOK_BISON_LEU_R 749
#define TOK_BISON_LSHIFTRT_R 750
#define TOK_BISON_LO_SUM_R 751
#define TOK_BISON_LT_R 752
#define TOK_BISON_LTGT_R 753
#define TOK_BISON_LTU_R 754
#define TOK_BISON_WRITE_MEM_R 755
#define TOK_BISON_READ_MEM_R 756
#define TOK_BISON_MINUS_R 757
#define TOK_BISON_MOD_R 758
#define TOK_BISON_MULT_R 759
#define TOK_BISON_NE_R 760
#define TOK_BISON_NEG_R 761
#define TOK_BISON_NOT_R 762
#define TOK_BISON_ORDERED_R 763
#define TOK_BISON_PARALLEL_R 764
#define TOK_BISON_PARITY_R 765
#define TOK_BISON_PC_R 766
#define TOK_BISON_PLUS_R 767
#define TOK_BISON_POPCOUNT_R 768
#define TOK_BISON_REG_R 769
#define TOK_BISON_ROTATE_R 770
#define TOK_BISON_ROTATERT_R 771
#define TOK_BISON_SAT_FRACT_R 772
#define TOK_BISON_SET_R 773
#define TOK_BISON_SIGN_EXTEND_R 774
#define TOK_BISON_SMAX_R 775
#define TOK_BISON_SMIN_R 776
#define TOK_BISON_SQRT_R 777
#define TOK_BISON_SYMBOL_REF_R 778
#define TOK_BISON_TRUNCATE_R 779
#define TOK_BISON_UDIV_R 780
#define TOK_BISON_UMAX_R 781
#define TOK_BISON_UMIN_R 782
#define TOK_BISON_UMOD_R 783
#define TOK_BISON_UNEQ_R 784
#define TOK_BISON_UNGE_R 785
#define TOK_BISON_UNGT_R 786
#define TOK_BISON_UNLE_R 787
#define TOK_BISON_UNLT_R 788
#define TOK_BISON_UNORDERED_R 789
#define TOK_BISON_UNSIGNED_FIX_R 790
#define TOK_BISON_UNSIGNED_FLOAT_R 791
#define TOK_BISON_UNSIGNED_FRACT_CONVERT_R 792
#define TOK_BISON_UNSIGNED_SAT_FRACT_R 793
#define TOK_BISON_XOR_R 794
#define TOK_BISON_ZERO_EXTEND_R 795
#define TOK_BISON_QC_R 796
#define TOK_BISON_HC_R 797
#define TOK_BISON_SC_R 798
#define TOK_BISON_DC_R 799
#define TOK_BISON_TC_R 800
#define TOK_BISON_CQI_R 801
#define TOK_BISON_CHI_R 802
#define TOK_BISON_CSI_R 803
#define TOK_BISON_CDI_R 804
#define TOK_BISON_CTI_R 805
#define TOK_BISON_QF_R 806
#define TOK_BISON_HF_R 807
#define TOK_BISON_SF_R 808
#define TOK_BISON_DF_R 809
#define TOK_BISON_TF_R 810
#define TOK_BISON_QI_R 811
#define TOK_BISON_HI_R 812
#define TOK_BISON_SI_R 813
#define TOK_BISON_DO_R 814
#define TOK_BISON_TI_R 815
#define TOK_BISON_V2SI_R 816
#define TOK_BISON_V4HI_R 817
#define TOK_BISON_V8QI_R 818
#define TOK_BISON_CC_R 819
#define TOK_BISON_CCFP_R 820
#define TOK_BISON_CCFPE_R 821
#define TOK_BISON_CCZ_R 822
#define TOK_BISON_CLB 823
#define TOK_BISON_CLB_VARS 824
#define TOK_BISON_USE 825
#define TOK_BISON_USE_VARS 826




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{


const char *text;
int value;
long long int long_value;
bool pred;



} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */



#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  7
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   2010

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  572
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  810
/* YYNRULES -- Number of rules.  */
#define YYNRULES  1372
/* YYNRULES -- Number of states.  */
#define YYNSTATES  1820

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   826

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint16 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,   472,   473,   474,
     475,   476,   477,   478,   479,   480,   481,   482,   483,   484,
     485,   486,   487,   488,   489,   490,   491,   492,   493,   494,
     495,   496,   497,   498,   499,   500,   501,   502,   503,   504,
     505,   506,   507,   508,   509,   510,   511,   512,   513,   514,
     515,   516,   517,   518,   519,   520,   521,   522,   523,   524,
     525,   526,   527,   528,   529,   530,   531,   532,   533,   534,
     535,   536,   537,   538,   539,   540,   541,   542,   543,   544,
     545,   546,   547,   548,   549,   550,   551,   552,   553,   554,
     555,   556,   557,   558,   559,   560,   561,   562,   563,   564,
     565,   566,   567,   568,   569,   570,   571
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     8,    10,    13,    14,    20,    21,
      25,    27,    29,    31,    33,    35,    37,    39,    41,    43,
      45,    47,    49,    51,    53,    55,    57,    59,    61,    63,
      65,    67,    69,    71,    73,    75,    77,    79,    81,    83,
      85,    87,    89,    91,    93,    95,    97,    99,   101,   103,
     105,   107,   109,   111,   113,   115,   117,   119,   121,   123,
     125,   127,   129,   131,   133,   135,   137,   139,   141,   143,
     145,   147,   149,   151,   153,   155,   157,   159,   161,   163,
     165,   167,   169,   171,   173,   175,   177,   179,   181,   183,
     185,   187,   189,   191,   193,   195,   197,   199,   201,   203,
     205,   207,   209,   211,   213,   215,   217,   219,   221,   223,
     225,   227,   229,   231,   233,   235,   237,   239,   241,   243,
     245,   247,   249,   251,   253,   255,   257,   259,   261,   263,
     265,   267,   269,   271,   273,   275,   277,   279,   281,   283,
     285,   287,   289,   291,   293,   295,   297,   299,   301,   303,
     305,   307,   309,   311,   313,   315,   317,   319,   321,   323,
     325,   327,   329,   331,   333,   335,   337,   339,   341,   343,
     345,   347,   349,   351,   353,   355,   357,   359,   361,   363,
     365,   367,   369,   371,   373,   375,   377,   379,   381,   383,
     385,   387,   389,   391,   393,   395,   397,   399,   401,   403,
     405,   407,   409,   411,   413,   415,   417,   419,   421,   423,
     425,   427,   429,   431,   433,   435,   436,   438,   439,   441,
     442,   444,   445,   447,   448,   451,   452,   453,   457,   458,
     461,   462,   463,   467,   468,   469,   470,   471,   472,   473,
     474,   475,   476,   477,   478,   479,   480,   481,   511,   512,
     517,   520,   521,   522,   523,   532,   533,   535,   536,   538,
     539,   540,   541,   542,   543,   544,   545,   546,   564,   565,
     566,   570,   571,   577,   578,   579,   585,   586,   587,   588,
     596,   597,   600,   602,   604,   607,   608,   610,   611,   612,
     613,   614,   615,   616,   617,   618,   619,   620,   621,   622,
     623,   624,   654,   655,   658,   661,   662,   665,   668,   669,
     672,   675,   676,   677,   678,   686,   687,   691,   692,   696,
     697,   701,   702,   704,   705,   710,   711,   716,   717,   721,
     722,   726,   727,   731,   732,   736,   737,   741,   742,   746,
     747,   752,   753,   755,   756,   757,   758,   759,   771,   772,
     774,   775,   776,   777,   778,   790,   791,   796,   797,   802,
     803,   804,   811,   812,   814,   815,   817,   818,   820,   821,
     824,   825,   828,   829,   830,   834,   835,   836,   837,   838,
     839,   858,   859,   862,   863,   866,   867,   875,   876,   877,
     878,   887,   888,   889,   893,   894,   899,   900,   904,   905,
     909,   910,   915,   916,   921,   922,   923,   924,   925,   926,
     927,   941,   942,   943,   944,   945,   946,   947,   948,   949,
     967,   968,   969,   970,   971,   982,   983,   988,   989,   994,
     995,  1000,  1001,  1005,  1006,  1010,  1011,  1015,  1016,  1020,
    1021,  1025,  1026,  1030,  1031,  1035,  1036,  1040,  1041,  1045,
    1046,  1050,  1051,  1055,  1056,  1060,  1061,  1065,  1066,  1070,
    1071,  1075,  1076,  1080,  1081,  1085,  1086,  1090,  1091,  1095,
    1096,  1100,  1101,  1105,  1106,  1110,  1111,  1115,  1116,  1120,
    1121,  1125,  1126,  1130,  1131,  1135,  1136,  1140,  1141,  1145,
    1146,  1150,  1151,  1155,  1156,  1160,  1161,  1165,  1166,  1170,
    1171,  1175,  1176,  1177,  1178,  1179,  1180,  1181,  1195,  1196,
    1199,  1202,  1205,  1206,  1209,  1212,  1215,  1216,  1220,  1221,
    1225,  1226,  1230,  1231,  1235,  1236,  1240,  1241,  1245,  1246,
    1250,  1251,  1255,  1256,  1260,  1261,  1265,  1266,  1270,  1271,
    1275,  1276,  1280,  1281,  1285,  1286,  1290,  1291,  1295,  1296,
    1300,  1301,  1305,  1306,  1310,  1311,  1315,  1316,  1320,  1321,
    1325,  1326,  1330,  1331,  1335,  1336,  1340,  1341,  1345,  1346,
    1350,  1351,  1355,  1356,  1360,  1361,  1365,  1366,  1370,  1371,
    1375,  1376,  1380,  1381,  1385,  1386,  1390,  1391,  1395,  1396,
    1400,  1401,  1405,  1406,  1410,  1411,  1415,  1416,  1420,  1421,
    1425,  1426,  1430,  1431,  1435,  1436,  1440,  1441,  1445,  1446,
    1450,  1451,  1455,  1456,  1460,  1461,  1465,  1466,  1470,  1471,
    1475,  1476,  1480,  1481,  1485,  1486,  1490,  1492,  1495,  1496,
    1497,  1503,  1504,  1508,  1509,  1513,  1514,  1518,  1519,  1523,
    1524,  1528,  1529,  1533,  1534,  1538,  1539,  1543,  1544,  1545,
    1546,  1547,  1557,  1559,  1562,  1563,  1565,  1566,  1567,  1573,
    1574,  1578,  1579,  1583,  1584,  1585,  1586,  1587,  1588,  1589,
    1590,  1591,  1592,  1613,  1614,  1617,  1620,  1623,  1625,  1627,
    1629,  1632,  1633,  1635,  1636,  1637,  1644,  1645,  1646,  1653,
    1654,  1656,  1657,  1661,  1662,  1663,  1670,  1672,  1675,  1677,
    1680,  1681,  1683,  1685,  1686,  1690,  1691,  1695,  1696,  1700,
    1701,  1705,  1706,  1710,  1711,  1715,  1716,  1720,  1721,  1725,
    1726,  1730,  1731,  1735,  1736,  1740,  1741,  1745,  1746,  1750,
    1751,  1755,  1756,  1760,  1761,  1765,  1766,  1770,  1771,  1775,
    1776,  1780,  1781,  1785,  1786,  1787,  1793,  1794,  1798,  1800,
    1801,  1804,  1807,  1808,  1809,  1810,  1811,  1821,  1822,  1825,
    1828,  1829,  1830,  1836,  1837,  1841,  1842,  1844,  1845,  1846,
    1847,  1856,  1858,  1859,  1860,  1866,  1867,  1871,  1872,  1873,
    1879,  1880,  1884,  1885,  1889,  1890,  1891,  1892,  1900,  1901,
    1902,  1903,  1911,  1912,  1916,  1918,  1919,  1920,  1926,  1927,
    1930,  1931,  1935,  1937,  1938,  1942,  1943,  1944,  1945,  1954,
    1955,  1960,  1961,  1965,  1966,  1968,  1969,  1970,  1971,  1972,
    1984,  1985,  1989,  1991,  1992,  1998,  1999,  2002,  2003,  2007,
    2008,  2012,  2013,  2017,  2018,  2020,  2021,  2022,  2029,  2030,
    2034,  2035,  2039,  2040,  2041,  2048,  2049,  2050,  2054,  2055,
    2056,  2065,  2066,  2067,  2071,  2072,  2075,  2078,  2081,  2082,
    2085,  2088,  2091,  2092,  2096,  2097,  2101,  2103,  2104,  2108,
    2109,  2113,  2114,  2116,  2117,  2119,  2120,  2122,  2123,  2125,
    2126,  2128,  2129,  2131,  2132,  2134,  2135,  2137,  2138,  2139,
    2140,  2141,  2142,  2143,  2144,  2145,  2146,  2147,  2171,  2172,
    2176,  2177,  2180,  2181,  2182,  2183,  2184,  2185,  2186,  2187,
    2188,  2189,  2190,  2212,  2213,  2214,  2215,  2216,  2217,  2218,
    2219,  2235,  2236,  2239,  2242,  2245,  2248,  2251,  2254,  2257,
    2258,  2261,  2263,  2265,  2267,  2269,  2271,  2273,  2275,  2277,
    2279,  2281,  2283,  2285,  2287,  2289,  2291,  2293,  2295,  2297,
    2299,  2301,  2303,  2305,  2307,  2309,  2311,  2313,  2315,  2317,
    2319,  2321,  2323,  2325,  2327,  2329,  2331,  2333,  2335,  2337,
    2339,  2341,  2343,  2345,  2347,  2349,  2351,  2353,  2355,  2357,
    2359,  2361,  2363,  2365,  2367,  2369,  2371,  2373,  2375,  2377,
    2379,  2381,  2383,  2385,  2387,  2389,  2391,  2393,  2395,  2397,
    2399,  2401,  2403,  2405,  2407,  2409,  2411,  2413,  2415,  2417,
    2419,  2421,  2423,  2425,  2427,  2429,  2431,  2433,  2435,  2437,
    2439,  2441,  2443,  2445,  2448,  2449,  2454,  2455,  2456,  2463,
    2464,  2465,  2472,  2473,  2475,  2476,  2480,  2481,  2482,  2483,
    2490,  2491,  2493,  2494,  2497,  2498,  2501,  2502,  2505,  2506,
    2509,  2510,  2513,  2514,  2519,  2521,  2523,  2525,  2527,  2529,
    2531,  2533,  2535,  2537,  2539,  2541,  2543,  2545,  2547,  2549,
    2551,  2553,  2555,  2557,  2559,  2561,  2563,  2565,  2567,  2569,
    2571,  2573,  2575,  2577,  2579,  2581,  2583,  2585,  2587,  2589,
    2591,  2593,  2595,  2597,  2599,  2601,  2603,  2605,  2607,  2609,
    2611,  2613,  2615,  2617,  2619,  2621,  2623,  2625,  2627,  2629,
    2631,  2633,  2635,  2637,  2639,  2641,  2643,  2645,  2647,  2649,
    2651,  2653,  2655,  2657,  2659,  2661,  2663,  2665,  2667,  2669,
    2671,  2673,  2675,  2676,  2678,  2680,  2682,  2684,  2686,  2688,
    2690,  2692,  2694,  2696,  2698,  2700,  2702,  2704,  2706,  2708,
    2710,  2712,  2714,  2716,  2718,  2720,  2722,  2724,  2726,  2728,
    2730,  2731,  2732,  2733,  2734,  2735,  2736,  2737,  2738,  2739,
    2740,  2762,  2765,  2768,  2771,  2774,  2777,  2778,  2780,  2783,
    2784,  2786,  2789,  2790,  2792,  2793,  2794,  2795,  2796,  2797,
    2798,  2812,  2813,  2814,  2815,  2816,  2817,  2818,  2819,  2820,
    2838,  2840,  2841,  2844,  2845,  2848,  2849,  2852,  2855,  2858,
    2861,  2864,  2867,  2870,  2873,  2876,  2879,  2882,  2885,  2888,
    2891,  2894,  2897,  2900,  2903,  2906,  2909,  2912,  2915,  2918,
    2921,  2924,  2927,  2930,  2933,  2936,  2939,  2940,  2943,  2946,
    2949,  2952,  2955,  2958,  2961,  2964,  2967,  2968,  2971,  2974,
    2977,  2980,  2983,  2986,  2989,  2992,  2995,  2998,  2999,  3002,
    3003,  3006,  3007,  3010,  3011,  3014,  3015,  3018,  3019,  3022,
    3023,  3026,  3027,  3030,  3031,  3034,  3035,  3038,  3039,  3042,
    3043,  3046,  3047,  3050,  3051,  3054,  3055,  3058,  3059,  3062,
    3063,  3066,  3067,  3070,  3071,  3074,  3075,  3078,  3079,  3082,
    3083,  3086,  3087,  3090,  3091,  3093,  3094,  3097,  3098,  3101,
    3102,  3105,  3106,  3108,  3109,  3112,  3113,  3116,  3117,  3120,
    3121,  3124,  3125,  3128,  3129,  3132,  3133,  3136,  3137,  3140,
    3143,  3144,  3147,  3148,  3151,  3152,  3155,  3156,  3159,  3160,
    3163,  3164,  3167,  3168,  3171,  3172,  3175,  3176,  3179,  3180,
    3183,  3184,  3187,  3188,  3191,  3192,  3195,  3196,  3199,  3200,
    3203,  3204,  3207,  3208,  3211,  3212,  3215,  3216,  3219,  3221,
    3223,  3225,  3226
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     573,     0,    -1,   574,    -1,   573,   574,    -1,   575,    -1,
     574,   577,    -1,    -1,     6,  1379,   576,     7,  1379,    -1,
      -1,     3,   578,   579,    -1,   590,    -1,   605,    -1,   607,
      -1,   613,    -1,   624,    -1,   626,    -1,   686,    -1,   629,
      -1,   660,    -1,  1018,    -1,   727,    -1,   758,    -1,   635,
      -1,   834,    -1,   656,    -1,   701,    -1,  1115,    -1,  1119,
      -1,   845,    -1,   662,    -1,   853,    -1,   847,    -1,   849,
      -1,   851,    -1,   855,    -1,   857,    -1,   859,    -1,   861,
      -1,   863,    -1,   865,    -1,   867,    -1,   780,    -1,   760,
      -1,   762,    -1,   774,    -1,   776,    -1,   869,    -1,   778,
      -1,   989,    -1,   871,    -1,   735,    -1,   697,    -1,   664,
      -1,   699,    -1,   667,    -1,   731,    -1,   737,    -1,   683,
      -1,  1079,    -1,   782,    -1,   729,    -1,   711,    -1,   744,
      -1,  1146,    -1,  1151,    -1,  1094,    -1,  1004,    -1,   982,
      -1,  1113,    -1,   830,    -1,  1162,    -1,   953,    -1,  1083,
      -1,  1121,    -1,   669,    -1,  1131,    -1,   692,    -1,   671,
      -1,   673,    -1,   721,    -1,   675,    -1,   719,    -1,   677,
      -1,   784,    -1,  1144,    -1,   814,    -1,   816,    -1,   788,
      -1,  1007,    -1,   786,    -1,  1100,    -1,   956,    -1,   733,
      -1,  1160,    -1,   961,    -1,   873,    -1,   875,    -1,   877,
      -1,  1066,    -1,   987,    -1,   879,    -1,   881,    -1,   883,
      -1,   885,    -1,   887,    -1,   891,    -1,   790,    -1,   792,
      -1,   794,    -1,   796,    -1,   893,    -1,   895,    -1,   800,
      -1,   897,    -1,   899,    -1,   901,    -1,   903,    -1,   905,
      -1,   907,    -1,   909,    -1,   911,    -1,   913,    -1,   915,
      -1,   917,    -1,   919,    -1,   921,    -1,   923,    -1,   925,
      -1,   927,    -1,   929,    -1,   931,    -1,   933,    -1,   935,
      -1,   937,    -1,   939,    -1,   941,    -1,   943,    -1,   808,
      -1,   945,    -1,   810,    -1,   812,    -1,   764,    -1,  1087,
      -1,   947,    -1,   802,    -1,   804,    -1,   806,    -1,   949,
      -1,   951,    -1,  1076,    -1,  1061,    -1,   768,    -1,   959,
      -1,   963,    -1,   770,    -1,   772,    -1,   766,    -1,  1124,
      -1,  1092,    -1,  1104,    -1,  1107,    -1,  1139,    -1,   753,
      -1,   681,    -1,   679,    -1,   818,    -1,   820,    -1,   824,
      -1,  1012,    -1,   798,    -1,   889,    -1,  1106,    -1,  1096,
      -1,  1058,    -1,  1063,    -1,  1020,    -1,  1024,    -1,  1026,
      -1,  1022,    -1,  1028,    -1,  1030,    -1,  1032,    -1,  1034,
      -1,  1036,    -1,  1038,    -1,  1040,    -1,  1042,    -1,  1044,
      -1,  1046,    -1,  1048,    -1,  1050,    -1,  1052,    -1,  1054,
      -1,  1056,    -1,   973,    -1,  1084,    -1,   822,    -1,   985,
      -1,   826,    -1,   975,    -1,   828,    -1,  1142,    -1,  1089,
      -1,  1164,    -1,  1165,    -1,  1167,    -1,  1262,    -1,  1255,
      -1,  1271,    -1,   832,    -1,   965,    -1,   969,    -1,   971,
      -1,   967,    -1,  1073,    -1,    -1,   327,    -1,    -1,   328,
      -1,    -1,   318,    -1,    -1,   319,    -1,    -1,   584,  1281,
      -1,    -1,    -1,   320,   586,   587,    -1,    -1,   587,  1379,
      -1,    -1,    -1,  1310,   589,  1311,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    37,   591,  1177,   592,   585,   593,  1211,   594,   588,
     595,  1361,   596,  1360,   597,  1342,   598,   584,   599,   582,
     600,   583,   601,   580,   602,   581,   603,  1325,   604,  1326,
      -1,    -1,     9,  1287,   606,  1318,    -1,     9,   320,    -1,
      -1,    -1,    -1,    30,   608,  1202,  1294,   609,  1330,   610,
     611,    -1,    -1,   333,    -1,    -1,   326,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    42,   614,  1177,   615,
    1331,   616,  1327,   617,  1314,   618,  1170,   619,  1319,   620,
     612,   621,  1375,    -1,    -1,    -1,  1282,   623,   622,    -1,
      -1,    64,   625,  1188,   622,  1291,    -1,    -1,    -1,    32,
     627,  1290,   628,  1317,    -1,    -1,    -1,    -1,    10,   630,
    1345,   631,  1346,   632,  1328,    -1,    -1,   327,   327,    -1,
     327,    -1,   313,    -1,   313,   327,    -1,    -1,   326,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    41,   636,  1177,   637,  1362,   638,
    1211,   639,   633,   640,  1344,   641,  1327,   642,  1314,   643,
    1176,   644,  1319,   645,   650,   646,   652,   647,   654,   648,
     634,   649,  1375,    -1,    -1,   651,   650,    -1,   273,  1378,
      -1,    -1,   653,   652,    -1,   275,  1378,    -1,    -1,   655,
     654,    -1,   274,  1378,    -1,    -1,    -1,    -1,    36,   657,
    1324,   658,  1287,   659,  1318,    -1,    -1,    13,   661,  1202,
      -1,    -1,    19,   663,  1202,    -1,    -1,    31,   665,  1202,
      -1,    -1,   323,    -1,    -1,    16,   668,  1202,   666,    -1,
      -1,    17,   670,  1202,  1295,    -1,    -1,    20,   672,  1202,
      -1,    -1,    22,   674,  1202,    -1,    -1,    26,   676,  1202,
      -1,    -1,    29,   678,  1202,    -1,    -1,   168,   680,  1202,
      -1,    -1,   165,   682,  1202,    -1,    -1,    15,   684,  1202,
    1315,    -1,    -1,   323,    -1,    -1,    -1,    -1,    -1,    14,
     687,  1202,  1315,   688,  1381,   689,   685,  1363,   690,  1364,
      -1,    -1,   323,    -1,    -1,    -1,    -1,    -1,    18,   693,
    1202,  1315,   694,   691,  1363,   695,  1364,   696,  1293,    -1,
      -1,    21,   698,  1202,  1296,    -1,    -1,    23,   700,  1202,
    1297,    -1,    -1,    -1,    25,   702,  1202,  1295,   703,  1336,
      -1,    -1,   424,    -1,    -1,   375,    -1,    -1,   324,    -1,
      -1,   707,  1283,    -1,    -1,   708,  1284,    -1,    -1,    -1,
    1310,   710,  1311,    -1,    -1,    -1,    -1,    -1,    -1,    27,
     712,  1202,   709,   704,  1339,   713,  1354,   714,  1355,   715,
    1340,   716,   705,   706,   707,   708,  1343,    -1,    -1,   717,
    1283,    -1,    -1,   718,  1284,    -1,    -1,    28,   720,  1202,
     325,   717,   718,  1343,    -1,    -1,    -1,    -1,    24,   722,
    1202,  1332,   723,  1330,   724,  1348,    -1,    -1,    -1,  1310,
     726,  1311,    -1,    -1,    40,   728,  1177,   725,    -1,    -1,
      46,   730,  1177,    -1,    -1,    38,   732,  1177,    -1,    -1,
      39,   734,  1177,  1347,    -1,    -1,    45,   736,  1177,  1337,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    43,   738,  1177,
     739,  1344,   740,  1292,   741,  1314,   742,  1176,   743,  1375,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    44,
     745,  1177,   746,  1211,   747,  1344,   748,  1327,   749,  1314,
     750,  1176,   751,  1341,   752,  1375,    -1,    -1,    -1,    -1,
      -1,   164,   754,  1177,  1351,   755,  1352,   756,  1353,   757,
    1330,    -1,    -1,   151,   759,  1191,  1335,    -1,    -1,   149,
     761,  1191,  1286,    -1,    -1,   148,   763,  1191,  1286,    -1,
      -1,   133,   765,  1213,    -1,    -1,   157,   767,  1213,    -1,
      -1,   145,   769,  1213,    -1,    -1,   152,   771,  1213,    -1,
      -1,   153,   773,  1213,    -1,    -1,   129,   775,  1213,    -1,
      -1,    68,   777,  1191,    -1,    -1,    92,   779,  1213,    -1,
      -1,   109,   781,  1213,    -1,    -1,   128,   783,  1213,    -1,
      -1,   134,   785,  1213,    -1,    -1,   135,   787,  1213,    -1,
      -1,    69,   789,  1213,    -1,    -1,    87,   791,  1213,    -1,
      -1,    88,   793,  1213,    -1,    -1,    89,   795,  1213,    -1,
      -1,    90,   797,  1213,    -1,    -1,    91,   799,  1213,    -1,
      -1,    95,   801,  1213,    -1,    -1,   138,   803,  1213,    -1,
      -1,   139,   805,  1213,    -1,    -1,   140,   807,  1213,    -1,
      -1,   126,   809,  1213,    -1,    -1,   130,   811,  1213,    -1,
      -1,   131,   813,  1213,    -1,    -1,    49,   815,  1213,    -1,
      -1,    50,   817,  1213,    -1,    -1,   166,   819,  1213,    -1,
      -1,   167,   821,  1213,    -1,    -1,   201,   823,  1213,    -1,
      -1,   169,   825,  1213,    -1,    -1,   203,   827,  1213,    -1,
      -1,   205,   829,  1213,    -1,    -1,    51,   831,  1213,    -1,
      -1,   457,   833,  1213,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    59,   835,  1191,   836,  1286,   837,  1286,   838,   841,
     839,   843,   840,  1350,    -1,    -1,   842,   841,    -1,   570,
    1379,    -1,   571,  1378,    -1,    -1,   844,   843,    -1,   568,
    1379,    -1,   569,  1378,    -1,    -1,   112,   846,  1214,    -1,
      -1,    71,   848,  1213,    -1,    -1,    72,   850,  1213,    -1,
      -1,    73,   852,  1213,    -1,    -1,    74,   854,  1214,    -1,
      -1,   444,   856,  1214,    -1,    -1,    75,   858,  1214,    -1,
      -1,    76,   860,  1214,    -1,    -1,    77,   862,  1214,    -1,
      -1,    81,   864,  1214,    -1,    -1,   111,   866,  1214,    -1,
      -1,   107,   868,  1214,    -1,    -1,   115,   870,  1214,    -1,
      -1,   114,   872,  1214,    -1,    -1,    57,   874,  1214,    -1,
      -1,    60,   876,  1214,    -1,    -1,    78,   878,  1214,    -1,
      -1,    79,   880,  1214,    -1,    -1,    80,   882,  1214,    -1,
      -1,    82,   884,  1214,    -1,    -1,    83,   886,  1214,    -1,
      -1,    84,   888,  1214,    -1,    -1,    85,   890,  1214,    -1,
      -1,    86,   892,  1214,    -1,    -1,    93,   894,  1214,    -1,
      -1,    94,   896,  1214,    -1,    -1,    96,   898,  1214,    -1,
      -1,    97,   900,  1214,    -1,    -1,    98,   902,  1214,    -1,
      -1,    99,   904,  1214,    -1,    -1,   100,   906,  1214,    -1,
      -1,   101,   908,  1214,    -1,    -1,   102,   910,  1214,    -1,
      -1,   103,   912,  1213,    -1,    -1,   104,   914,  1214,    -1,
      -1,   105,   916,  1214,    -1,    -1,   106,   918,  1214,    -1,
      -1,   108,   920,  1214,    -1,    -1,   110,   922,  1214,    -1,
      -1,   113,   924,  1214,    -1,    -1,   116,   926,  1214,    -1,
      -1,   117,   928,  1214,    -1,    -1,   118,   930,  1214,    -1,
      -1,   119,   932,  1214,    -1,    -1,   120,   934,  1214,    -1,
      -1,   121,   936,  1214,    -1,    -1,   122,   938,  1214,    -1,
      -1,   123,   940,  1214,    -1,    -1,   124,   942,  1214,    -1,
      -1,   125,   944,  1214,    -1,    -1,   127,   946,  1214,    -1,
      -1,   137,   948,  1214,    -1,    -1,   141,   950,  1214,    -1,
      -1,   142,   952,  1214,    -1,    -1,    67,   954,  1216,    -1,
    1285,    -1,   955,  1285,    -1,    -1,    -1,    35,   957,  1290,
     958,   955,    -1,    -1,   146,   960,  1214,    -1,    -1,    54,
     962,  1216,    -1,    -1,   147,   964,  1214,    -1,    -1,   458,
     966,  1214,    -1,    -1,   461,   968,  1214,    -1,    -1,   459,
     970,  1214,    -1,    -1,   460,   972,  1214,    -1,    -1,   199,
     974,  1188,    -1,    -1,    -1,    -1,    -1,   204,   976,  1320,
     977,  1291,   978,  1309,   979,  1356,    -1,  1286,    -1,   980,
    1286,    -1,    -1,   980,    -1,    -1,    -1,    11,   983,  1318,
     984,   981,    -1,    -1,   202,   986,  1188,    -1,    -1,    61,
     988,  1219,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   159,   990,  1324,   991,  1304,   992,  1312,   993,
    1313,   994,   999,   995,  1001,  1172,   996,  1173,   997,  1363,
     998,  1364,    -1,    -1,  1000,   999,    -1,   570,  1379,    -1,
     571,  1378,    -1,  1003,    -1,  1002,    -1,  1279,    -1,  1002,
    1279,    -1,    -1,   330,    -1,    -1,    -1,   154,  1005,  1191,
    1286,  1006,  1286,    -1,    -1,    -1,   132,  1008,  1188,  1286,
    1009,  1010,    -1,    -1,  1286,    -1,    -1,  1286,  1011,  1286,
      -1,    -1,    -1,   171,  1013,  1188,  1286,  1014,  1286,    -1,
    1276,    -1,  1015,  1276,    -1,  1236,    -1,  1016,  1236,    -1,
      -1,  1015,    -1,  1016,    -1,    -1,   163,  1019,  1017,    -1,
      -1,   180,  1021,  1188,    -1,    -1,   183,  1023,  1216,    -1,
      -1,   181,  1025,  1216,    -1,    -1,   182,  1027,  1216,    -1,
      -1,   184,  1029,  1214,    -1,    -1,   185,  1031,  1214,    -1,
      -1,   186,  1033,  1214,    -1,    -1,   187,  1035,  1214,    -1,
      -1,   188,  1037,  1213,    -1,    -1,   189,  1039,  1213,    -1,
      -1,   190,  1041,  1213,    -1,    -1,   191,  1043,  1213,    -1,
      -1,   192,  1045,  1214,    -1,    -1,   193,  1047,  1214,    -1,
      -1,   194,  1049,  1214,    -1,    -1,   195,  1051,  1214,    -1,
      -1,   196,  1053,  1214,    -1,    -1,   197,  1055,  1214,    -1,
      -1,   198,  1057,  1214,    -1,    -1,    -1,   177,  1059,  1320,
    1060,  1357,    -1,    -1,   144,  1062,  1214,    -1,   179,    -1,
      -1,  1064,  1065,    -1,  1277,  1280,    -1,    -1,    -1,    -1,
      -1,   160,  1067,  1191,  1068,  1303,  1069,  1064,  1070,  1172,
      -1,    -1,  1071,  1072,    -1,  1277,  1280,    -1,    -1,    -1,
     155,  1074,  1191,  1075,  1071,    -1,    -1,   143,  1077,  1214,
      -1,    -1,   321,    -1,    -1,    -1,    -1,    33,  1080,  1290,
    1081,  1078,  1288,  1082,  1289,    -1,    70,    -1,    -1,    -1,
     200,  1085,  1308,  1086,  1328,    -1,    -1,   136,  1088,  1214,
      -1,    -1,    -1,   206,  1090,  1320,  1091,  1291,    -1,    -1,
     161,  1093,  1214,    -1,    -1,    47,  1095,  1216,    -1,    -1,
      -1,    -1,   174,  1097,  1320,  1098,  1307,  1099,  1306,    -1,
      -1,    -1,    -1,    34,  1101,  1290,  1102,  1300,  1103,  1301,
      -1,    -1,   162,  1105,  1214,    -1,   172,    -1,    -1,    -1,
      55,  1108,  1324,  1109,  1110,    -1,    -1,  1110,  1111,    -1,
      -1,  1322,  1112,  1285,    -1,  1285,    -1,    -1,    12,  1114,
    1381,    -1,    -1,    -1,    -1,    62,  1116,  1188,  1286,  1117,
    1286,  1118,  1286,    -1,    -1,    63,  1120,  1191,  1286,    -1,
      -1,   150,  1122,  1214,    -1,    -1,   330,    -1,    -1,    -1,
      -1,    -1,   158,  1125,  1191,  1123,  1305,  1126,  1365,  1127,
    1366,  1128,  1349,    -1,    -1,  1286,  1130,  1335,    -1,   329,
      -1,    -1,   156,  1132,  1188,  1129,  1299,    -1,    -1,  1133,
    1134,    -1,    -1,   254,  1135,  1302,    -1,    -1,   255,  1136,
    1302,    -1,    -1,   256,  1137,  1302,    -1,    -1,   322,    -1,
      -1,    -1,   253,  1140,  1324,  1141,  1138,  1133,    -1,    -1,
     207,  1143,  1324,    -1,    -1,    52,  1145,  1219,    -1,    -1,
      -1,    66,  1147,  1188,  1298,  1148,  1149,    -1,    -1,    -1,
    1281,  1150,  1149,    -1,    -1,    -1,    65,  1152,  1191,  1298,
    1153,  1154,  1156,  1158,    -1,    -1,    -1,  1281,  1155,  1154,
      -1,    -1,  1157,  1156,    -1,   570,  1379,    -1,   571,  1378,
      -1,    -1,  1159,  1158,    -1,   568,  1379,    -1,   569,  1378,
      -1,    -1,    48,  1161,  1216,    -1,    -1,    53,  1163,  1219,
      -1,   432,    -1,    -1,   435,  1166,  1214,    -1,    -1,   436,
    1168,  1216,    -1,    -1,   314,    -1,    -1,   309,    -1,    -1,
     310,    -1,    -1,   372,    -1,    -1,   329,    -1,    -1,   311,
      -1,    -1,   312,    -1,    -1,   352,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1323,  1178,  1338,
    1179,  1374,  1180,  1324,  1181,  1333,  1182,  1224,  1328,  1183,
    1329,  1184,  1170,  1185,  1174,  1186,  1175,  1187,  1169,  1358,
      -1,    -1,  1324,  1189,  1224,    -1,    -1,  1190,  1278,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1333,  1192,  1316,  1193,  1250,  1194,  1252,  1195,  1190,  1196,
    1254,  1197,  1224,  1198,  1272,  1199,  1273,  1200,  1274,  1201,
    1233,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1210,
    1203,  1323,  1204,  1334,  1205,  1327,  1206,  1333,  1207,  1359,
    1208,  1176,  1209,  1171,    -1,    -1,   243,   425,    -1,   243,
     426,    -1,   243,   427,    -1,   243,   428,    -1,   243,   429,
      -1,   243,   430,    -1,   243,   431,    -1,    -1,  1211,  1212,
      -1,   336,    -1,   337,    -1,   338,    -1,   339,    -1,   340,
      -1,   342,    -1,   341,    -1,   343,    -1,   330,    -1,   344,
      -1,   345,    -1,   220,    -1,   346,    -1,   347,    -1,   348,
      -1,    55,    -1,    56,    -1,   349,    -1,   350,    -1,   351,
      -1,   353,    -1,   354,    -1,   355,    -1,   356,    -1,   357,
      -1,   359,    -1,   360,    -1,   361,    -1,   362,    -1,   363,
      -1,   364,    -1,   365,    -1,   366,    -1,   367,    -1,   368,
      -1,   369,    -1,   370,    -1,   316,    -1,   317,    -1,   371,
      -1,   372,    -1,   315,    -1,   373,    -1,   374,    -1,   378,
      -1,   379,    -1,   380,    -1,   381,    -1,   382,    -1,   383,
      -1,   385,    -1,   384,    -1,   386,    -1,   387,    -1,   388,
      -1,   389,    -1,   390,    -1,   391,    -1,   392,    -1,   393,
      -1,   394,    -1,   395,    -1,   396,    -1,   397,    -1,   398,
      -1,   399,    -1,   400,    -1,   401,    -1,   402,    -1,   403,
      -1,   404,    -1,   405,    -1,   406,    -1,   407,    -1,   408,
      -1,   409,    -1,   410,    -1,   411,    -1,   412,    -1,   413,
      -1,   414,    -1,   415,    -1,   416,    -1,   290,    -1,   417,
      -1,   418,    -1,   419,    -1,   420,    -1,   421,    -1,   422,
      -1,   423,    -1,   434,    -1,  1188,  1335,    -1,    -1,  1188,
    1286,  1215,  1335,    -1,    -1,    -1,  1188,  1286,  1217,  1286,
    1218,  1335,    -1,    -1,    -1,  1188,  1286,  1220,  1286,  1221,
    1222,    -1,    -1,  1286,    -1,    -1,  1286,  1223,  1286,    -1,
      -1,    -1,    -1,   210,  1379,  1225,  1380,  1226,  1380,    -1,
      -1,   265,    -1,    -1,   266,  1380,    -1,    -1,  1229,  1247,
      -1,    -1,  1230,  1248,    -1,    -1,  1231,  1275,    -1,    -1,
    1232,  1276,    -1,    -1,  1233,   462,  1234,  1235,    -1,   463,
      -1,   464,    -1,   465,    -1,   466,    -1,   467,    -1,   468,
      -1,   469,    -1,   470,    -1,   471,    -1,   472,    -1,   473,
      -1,   474,    -1,   475,    -1,   476,    -1,   477,    -1,   478,
      -1,   479,    -1,   480,    -1,   481,    -1,   482,    -1,   483,
      -1,   484,    -1,   485,    -1,   486,    -1,   487,    -1,   488,
      -1,   489,    -1,   490,    -1,   491,    -1,   492,    -1,   493,
      -1,   494,    -1,   496,    -1,   495,    -1,   497,    -1,   498,
      -1,   499,    -1,   500,    -1,   501,    -1,   502,    -1,   503,
      -1,   504,    -1,   505,    -1,   506,    -1,   507,    -1,   508,
      -1,   509,    -1,   510,    -1,   511,    -1,   512,    -1,   513,
      -1,   514,    -1,   515,    -1,   516,    -1,   517,    -1,   518,
      -1,   519,    -1,   520,    -1,   521,    -1,   522,    -1,   523,
      -1,   524,    -1,   525,    -1,   526,    -1,   527,    -1,   528,
      -1,   529,    -1,   530,    -1,   531,    -1,   532,    -1,   533,
      -1,   534,    -1,   535,    -1,   536,    -1,   537,    -1,   538,
      -1,   539,    -1,   540,    -1,    -1,   541,    -1,   542,    -1,
     543,    -1,   544,    -1,   545,    -1,   546,    -1,   547,    -1,
     548,    -1,   549,    -1,   550,    -1,   551,    -1,   552,    -1,
     553,    -1,   554,    -1,   555,    -1,   556,    -1,   557,    -1,
     558,    -1,   559,    -1,   560,    -1,   561,    -1,   562,    -1,
     563,    -1,   564,    -1,   565,    -1,   566,    -1,   567,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     238,  1237,  1380,  1238,  1227,  1239,  1228,  1240,  1229,  1241,
    1230,  1242,  1367,  1243,  1368,  1244,  1231,  1245,  1232,  1246,
    1233,    -1,   263,   334,    -1,   263,  1380,    -1,   264,   335,
      -1,   264,  1380,    -1,   437,  1378,    -1,    -1,  1249,    -1,
     438,  1378,    -1,    -1,  1251,    -1,   439,  1378,    -1,    -1,
    1253,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   446,  1256,
    1290,  1257,  1370,  1258,  1372,  1259,  1376,  1260,  1371,  1261,
    1377,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     445,  1263,  1290,  1264,  1369,  1265,  1370,  1266,  1376,  1267,
    1371,  1268,  1372,  1269,  1321,  1270,  1373,    -1,   456,    -1,
      -1,   231,  1380,    -1,    -1,   232,  1380,    -1,    -1,   233,
    1380,    -1,   268,  1378,    -1,   229,  1378,    -1,   270,  1378,
      -1,   440,  1378,    -1,   273,  1378,    -1,   271,  1380,    -1,
     211,  1378,    -1,   234,  1378,    -1,   246,  1378,    -1,   291,
    1378,    -1,   227,  1378,    -1,   230,  1378,    -1,   213,  1379,
      -1,   244,  1379,    -1,   245,  1379,    -1,   209,  1378,    -1,
     212,  1378,    -1,   215,  1378,    -1,   292,  1378,    -1,   217,
    1378,    -1,   236,  1378,    -1,   241,  1378,    -1,   242,  1378,
      -1,   249,  1378,    -1,   250,  1378,    -1,   251,  1378,    -1,
     252,  1378,    -1,   257,  1378,    -1,   269,  1378,    -1,    -1,
     272,  1378,    -1,   287,  1379,    -1,   299,  1378,    -1,   301,
    1378,    -1,   304,  1378,    -1,   305,  1378,    -1,   307,  1378,
      -1,   308,  1378,    -1,   276,  1380,    -1,    -1,   277,  1380,
      -1,   216,  1380,    -1,   223,  1380,    -1,   224,  1380,    -1,
     221,  1380,    -1,   214,  1380,    -1,   220,  1380,    -1,   376,
    1380,    -1,   453,  1378,    -1,   449,  1378,    -1,    -1,   208,
    1378,    -1,    -1,   209,  1378,    -1,    -1,   212,  1378,    -1,
      -1,   433,  1378,    -1,    -1,   215,  1378,    -1,    -1,   228,
    1378,    -1,    -1,   267,  1378,    -1,    -1,   218,  1378,    -1,
      -1,   222,  1378,    -1,    -1,   217,  1378,    -1,    -1,   219,
    1378,    -1,    -1,   235,  1378,    -1,    -1,   230,  1378,    -1,
      -1,   237,  1378,    -1,    -1,   239,  1378,    -1,    -1,   240,
    1378,    -1,    -1,   241,  1378,    -1,    -1,   247,  1378,    -1,
      -1,   248,  1378,    -1,    -1,   249,  1378,    -1,    -1,   257,
    1378,    -1,    -1,   260,  1378,    -1,    -1,   262,  1378,    -1,
      -1,  1285,    -1,    -1,   278,  1378,    -1,    -1,   279,  1378,
      -1,    -1,   288,  1378,    -1,    -1,   289,    -1,    -1,   293,
    1378,    -1,    -1,   294,  1378,    -1,    -1,   295,  1378,    -1,
      -1,   296,  1378,    -1,    -1,   297,  1378,    -1,    -1,   299,
    1378,    -1,    -1,   301,  1378,    -1,    -1,   258,  1380,    -1,
     259,  1380,    -1,    -1,   216,  1380,    -1,    -1,   322,  1380,
      -1,    -1,   377,  1380,    -1,    -1,   306,  1380,    -1,    -1,
     225,  1378,    -1,    -1,   226,  1378,    -1,    -1,   285,  1378,
      -1,    -1,   286,  1378,    -1,    -1,   442,  1380,    -1,    -1,
     443,  1380,    -1,    -1,   447,  1378,    -1,    -1,   448,  1378,
      -1,    -1,   451,  1378,    -1,    -1,   452,  1378,    -1,    -1,
     454,  1378,    -1,    -1,   453,  1378,    -1,    -1,   455,  1378,
      -1,    -1,   449,  1378,    -1,    -1,   450,  1378,    -1,     3,
      -1,     4,    -1,     5,    -1,    -1,  1379,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   654,   654,   655,   658,   659,   662,   661,   675,   675,
     681,   682,   683,   684,   685,   686,   687,   688,   689,   690,
     691,   692,   693,   694,   695,   696,   697,   698,   699,   700,
     701,   702,   703,   704,   705,   706,   707,   708,   709,   710,
     711,   712,   713,   714,   715,   716,   717,   718,   719,   720,
     721,   722,   723,   724,   725,   726,   727,   728,   729,   730,
     731,   732,   733,   734,   735,   736,   737,   738,   739,   740,
     741,   742,   743,   744,   745,   746,   747,   748,   749,   750,
     751,   752,   753,   754,   755,   756,   757,   758,   759,   760,
     761,   762,   763,   764,   765,   766,   767,   768,   769,   770,
     771,   772,   773,   774,   775,   776,   777,   778,   779,   780,
     781,   782,   783,   784,   785,   786,   787,   788,   789,   790,
     791,   792,   793,   794,   795,   796,   797,   798,   799,   800,
     801,   802,   803,   804,   805,   806,   807,   808,   809,   810,
     811,   812,   813,   814,   815,   816,   817,   818,   819,   820,
     821,   822,   823,   824,   825,   826,   827,   828,   829,   830,
     831,   832,   833,   834,   835,   836,   837,   838,   839,   840,
     841,   842,   843,   844,   845,   846,   847,   848,   849,   850,
     851,   852,   853,   854,   855,   856,   857,   858,   859,   860,
     861,   862,   863,   864,   865,   866,   867,   868,   869,   870,
     871,   872,   873,   874,   875,   876,   877,   878,   879,   880,
     881,   882,   883,   884,   885,   888,   890,   893,   894,   897,
     898,   900,   901,   904,   905,   908,   909,   909,   912,   913,
     916,   917,   917,   920,   921,   922,   923,   924,   925,   926,
     927,   928,   929,   930,   931,   932,   933,   920,   938,   938,
     939,   944,   945,   946,   944,   949,   950,   956,   957,   961,
     962,   963,   964,   965,   966,   967,   968,   961,   972,   973,
     973,   976,   976,   980,   984,   979,   993,   993,   993,   993,
     995,   996,   997,   998,   999,  1001,  1001,  1004,  1005,  1006,
    1007,  1008,  1009,  1010,  1011,  1012,  1013,  1014,  1015,  1016,
    1017,  1004,  1021,  1022,  1025,  1027,  1028,  1031,  1033,  1034,
    1037,  1040,  1040,  1040,  1040,  1043,  1043,  1046,  1046,  1049,
    1049,  1051,  1051,  1053,  1053,  1056,  1056,  1059,  1059,  1062,
    1062,  1065,  1065,  1068,  1068,  1071,  1071,  1074,  1074,  1077,
    1077,  1082,  1082,  1085,  1087,  1088,  1090,  1085,  1094,  1094,
    1097,  1099,  1101,  1102,  1097,  1107,  1107,  1111,  1111,  1115,
    1115,  1115,  1118,  1119,  1122,  1123,  1126,  1127,  1130,  1131,
    1134,  1135,  1138,  1139,  1139,  1143,  1143,  1143,  1143,  1143,
    1143,  1145,  1146,  1149,  1150,  1154,  1154,  1157,  1157,  1157,
    1157,  1160,  1161,  1161,  1164,  1164,  1167,  1167,  1170,  1170,
    1173,  1173,  1176,  1176,  1179,  1180,  1181,  1182,  1183,  1184,
    1179,  1189,  1190,  1191,  1192,  1193,  1194,  1195,  1196,  1189,
    1201,  1201,  1201,  1201,  1201,  1206,  1206,  1214,  1214,  1217,
    1217,  1220,  1220,  1223,  1223,  1226,  1226,  1229,  1229,  1232,
    1232,  1235,  1235,  1238,  1238,  1241,  1241,  1244,  1244,  1247,
    1247,  1250,  1250,  1253,  1253,  1256,  1256,  1259,  1259,  1262,
    1262,  1265,  1265,  1268,  1268,  1271,  1271,  1274,  1274,  1277,
    1277,  1280,  1280,  1283,  1283,  1286,  1286,  1289,  1289,  1292,
    1292,  1295,  1295,  1298,  1298,  1301,  1301,  1304,  1304,  1307,
    1307,  1310,  1310,  1313,  1313,  1316,  1316,  1319,  1319,  1322,
    1322,  1326,  1327,  1328,  1329,  1330,  1331,  1326,  1335,  1336,
    1339,  1340,  1343,  1344,  1347,  1348,  1352,  1352,  1355,  1355,
    1357,  1357,  1359,  1359,  1362,  1362,  1365,  1365,  1368,  1368,
    1371,  1371,  1374,  1374,  1377,  1377,  1380,  1380,  1383,  1383,
    1386,  1386,  1389,  1389,  1392,  1392,  1395,  1395,  1398,  1398,
    1401,  1401,  1404,  1404,  1407,  1407,  1410,  1410,  1413,  1413,
    1416,  1416,  1419,  1419,  1422,  1422,  1425,  1425,  1428,  1428,
    1431,  1431,  1434,  1434,  1437,  1437,  1440,  1440,  1443,  1443,
    1446,  1446,  1449,  1449,  1452,  1452,  1455,  1455,  1458,  1458,
    1461,  1461,  1464,  1464,  1467,  1467,  1470,  1470,  1473,  1473,
    1476,  1476,  1479,  1479,  1482,  1482,  1485,  1485,  1488,  1488,
    1491,  1491,  1494,  1494,  1497,  1497,  1500,  1500,  1503,  1503,
    1506,  1506,  1509,  1509,  1512,  1512,  1514,  1515,  1517,  1517,
    1517,  1520,  1520,  1523,  1523,  1526,  1526,  1530,  1530,  1532,
    1532,  1535,  1535,  1538,  1538,  1541,  1541,  1544,  1544,  1544,
    1544,  1544,  1546,  1547,  1549,  1549,  1552,  1552,  1552,  1555,
    1555,  1557,  1557,  1560,  1561,  1562,  1563,  1564,  1565,  1567,
    1568,  1569,  1560,  1573,  1574,  1577,  1578,  1581,  1581,  1583,
    1584,  1587,  1587,  1591,  1592,  1591,  1597,  1600,  1596,  1604,
    1605,  1608,  1607,  1614,  1614,  1614,  1616,  1617,  1619,  1620,
    1623,  1624,  1625,  1629,  1629,  1633,  1633,  1636,  1636,  1639,
    1639,  1642,  1642,  1645,  1645,  1648,  1648,  1651,  1651,  1654,
    1654,  1657,  1657,  1660,  1660,  1663,  1663,  1666,  1666,  1669,
    1669,  1672,  1672,  1675,  1675,  1678,  1678,  1681,  1681,  1684,
    1684,  1687,  1687,  1690,  1690,  1690,  1694,  1694,  1697,  1699,
    1700,  1703,  1707,  1708,  1709,  1710,  1707,  1714,  1715,  1718,
    1721,  1722,  1721,  1727,  1727,  1729,  1729,  1732,  1732,  1732,
    1732,  1735,  1738,  1738,  1738,  1741,  1741,  1744,  1744,  1744,
    1747,  1747,  1750,  1750,  1753,  1753,  1753,  1753,  1756,  1756,
    1756,  1756,  1759,  1759,  1762,  1765,  1765,  1765,  1768,  1769,
    1772,  1772,  1773,  1777,  1777,  1781,  1783,  1784,  1780,  1790,
    1789,  1796,  1796,  1798,  1798,  1801,  1801,  1801,  1801,  1801,
    1803,  1803,  1804,  1807,  1807,  1810,  1811,  1814,  1814,  1815,
    1815,  1816,  1816,  1819,  1819,  1822,  1822,  1822,  1826,  1826,
    1830,  1830,  1833,  1835,  1833,  1839,  1840,  1840,  1843,  1845,
    1843,  1851,  1852,  1852,  1855,  1856,  1859,  1860,  1863,  1864,
    1867,  1868,  1872,  1872,  1875,  1875,  1878,  1881,  1881,  1884,
    1884,  1887,  1889,  1892,  1892,  1894,  1894,  1896,  1896,  1898,
    1898,  1900,  1900,  1902,  1902,  1904,  1905,  1909,  1910,  1911,
    1912,  1913,  1915,  1916,  1917,  1918,  1919,  1909,  1924,  1924,
    1928,  1929,  1933,  1934,  1935,  1936,  1937,  1938,  1939,  1941,
    1952,  1958,  1933,  1973,  1974,  1975,  1976,  1977,  1978,  1979,
    1973,  1984,  1985,  1986,  1987,  1988,  1989,  1990,  1991,  2006,
    2007,  2010,  2011,  2012,  2013,  2014,  2015,  2016,  2017,  2018,
    2019,  2020,  2021,  2022,  2023,  2024,  2025,  2026,  2027,  2028,
    2029,  2030,  2031,  2032,  2033,  2034,  2035,  2036,  2037,  2038,
    2039,  2040,  2041,  2042,  2043,  2044,  2045,  2046,  2047,  2048,
    2049,  2050,  2051,  2052,  2053,  2054,  2055,  2056,  2057,  2058,
    2059,  2060,  2061,  2062,  2063,  2064,  2065,  2066,  2067,  2068,
    2069,  2070,  2071,  2072,  2073,  2074,  2075,  2076,  2077,  2078,
    2079,  2080,  2081,  2082,  2083,  2084,  2085,  2086,  2087,  2088,
    2089,  2090,  2091,  2092,  2093,  2094,  2095,  2096,  2097,  2098,
    2099,  2100,  2101,  2105,  2108,  2108,  2111,  2111,  2111,  2116,
    2118,  2114,  2122,  2123,  2126,  2125,  2132,  2132,  2132,  2132,
    2137,  2138,  2140,  2141,  2145,  2146,  2149,  2150,  2153,  2154,
    2157,  2158,  2161,  2162,  2173,  2174,  2175,  2176,  2177,  2178,
    2179,  2180,  2181,  2182,  2183,  2184,  2185,  2186,  2187,  2188,
    2189,  2190,  2191,  2192,  2193,  2194,  2195,  2196,  2197,  2198,
    2199,  2200,  2201,  2202,  2203,  2204,  2205,  2206,  2207,  2208,
    2209,  2210,  2211,  2212,  2213,  2214,  2215,  2216,  2217,  2218,
    2219,  2220,  2221,  2222,  2223,  2224,  2225,  2226,  2227,  2228,
    2229,  2230,  2231,  2232,  2233,  2234,  2235,  2236,  2237,  2238,
    2239,  2240,  2241,  2242,  2243,  2244,  2245,  2246,  2247,  2248,
    2249,  2250,  2253,  2254,  2255,  2256,  2257,  2258,  2259,  2260,
    2261,  2262,  2263,  2264,  2265,  2266,  2267,  2268,  2269,  2270,
    2271,  2272,  2273,  2274,  2275,  2276,  2277,  2278,  2279,  2280,
    2283,  2284,  2285,  2286,  2287,  2288,  2289,  2290,  2291,  2292,
    2283,  2297,  2298,  2301,  2302,  2306,  2309,  2310,  2313,  2316,
    2317,  2320,  2322,  2323,  2326,  2327,  2328,  2329,  2330,  2331,
    2326,  2335,  2336,  2337,  2338,  2339,  2340,  2341,  2342,  2335,
    2346,  2348,  2348,  2350,  2350,  2352,  2352,  2354,  2355,  2356,
    2357,  2358,  2359,  2360,  2361,  2362,  2363,  2364,  2365,  2369,
    2371,  2373,  2376,  2378,  2380,  2382,  2384,  2386,  2388,  2390,
    2392,  2394,  2396,  2398,  2400,  2402,  2404,  2404,  2406,  2408,
    2410,  2412,  2414,  2416,  2418,  2420,  2422,  2422,  2424,  2426,
    2428,  2430,  2432,  2434,  2436,  2438,  2440,  2443,  2443,  2445,
    2445,  2447,  2447,  2448,  2448,  2450,  2450,  2452,  2452,  2454,
    2454,  2456,  2456,  2458,  2458,  2460,  2460,  2462,  2462,  2464,
    2464,  2466,  2466,  2468,  2468,  2470,  2470,  2472,  2472,  2474,
    2474,  2476,  2476,  2478,  2478,  2480,  2480,  2482,  2482,  2484,
    2484,  2486,  2486,  2488,  2488,  2490,  2490,  2492,  2492,  2494,
    2494,  2496,  2496,  2498,  2498,  2500,  2500,  2502,  2502,  2504,
    2504,  2506,  2506,  2508,  2508,  2510,  2510,  2512,  2512,  2512,
    2514,  2514,  2516,  2516,  2518,  2518,  2520,  2520,  2522,  2522,
    2524,  2524,  2526,  2526,  2528,  2528,  2530,  2530,  2532,  2532,
    2534,  2534,  2536,  2536,  2538,  2538,  2540,  2540,  2542,  2542,
    2544,  2544,  2546,  2546,  2548,  2548,  2550,  2550,  2553,  2554,
    2555,  2556,  2556
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NODE_ID", "TOK_BISON_STRING",
  "TOK_BISON_NUMBER", "TOK_BISON_GCC_VERSION", "TOK_BISON_PLUGIN_VERSION",
  "TOK_BISON_ERROR_MARK", "TOK_BISON_IDENTIFIER_NODE",
  "TOK_BISON_TREE_LIST", "TOK_BISON_TREE_VEC", "TOK_BISON_BLOCK",
  "TOK_BISON_VOID_TYPE", "TOK_BISON_INTEGER_TYPE", "TOK_BISON_REAL_TYPE",
  "TOK_BISON_COMPLEX_TYPE", "TOK_BISON_VECTOR_TYPE",
  "TOK_BISON_ENUMERAL_TYPE", "TOK_BISON_BOOLEAN_TYPE",
  "TOK_BISON_CHAR_TYPE", "TOK_BISON_POINTER_TYPE", "TOK_BISON_OFFSET_TYPE",
  "TOK_BISON_REFERENCE_TYPE", "TOK_BISON_METHOD_TYPE",
  "TOK_BISON_ARRAY_TYPE", "TOK_BISON_SET_TYPE", "TOK_BISON_RECORD_TYPE",
  "TOK_BISON_UNION_TYPE", "TOK_BISON_QUAL_UNION_TYPE",
  "TOK_BISON_FUNCTION_TYPE", "TOK_BISON_LANG_TYPE",
  "TOK_BISON_INTEGER_CST", "TOK_BISON_REAL_CST", "TOK_BISON_COMPLEX_CST",
  "TOK_BISON_VECTOR_CST", "TOK_BISON_STRING_CST",
  "TOK_BISON_FUNCTION_DECL", "TOK_BISON_LABEL_DECL",
  "TOK_BISON_CONST_DECL", "TOK_BISON_TYPE_DECL", "TOK_BISON_VAR_DECL",
  "TOK_BISON_PARM_DECL", "TOK_BISON_RESULT_DECL", "TOK_BISON_FIELD_DECL",
  "TOK_BISON_NAMESPACE_DECL", "TOK_BISON_TRANSLATION_UNIT_DECL",
  "TOK_BISON_COMPONENT_REF", "TOK_BISON_BIT_FIELD_REF",
  "TOK_BISON_INDIRECT_REF", "TOK_BISON_MISALIGNED_INDIRECT_REF",
  "TOK_BISON_BUFFER_REF", "TOK_BISON_ARRAY_REF",
  "TOK_BISON_ARRAY_RANGE_REF", "TOK_BISON_VTABLE_REF",
  "TOK_BISON_CONSTRUCTOR", "TOK_BISON_DESTRUCTOR",
  "TOK_BISON_COMPOUND_EXPR", "TOK_BISON_MODIFY_EXPR",
  "TOK_BISON_GIMPLE_ASSIGN", "TOK_BISON_INIT_EXPR",
  "TOK_BISON_TARGET_EXPR", "TOK_BISON_COND_EXPR", "TOK_BISON_GIMPLE_COND",
  "TOK_BISON_GIMPLE_BIND", "TOK_BISON_GIMPLE_CALL", "TOK_BISON_CALL_EXPR",
  "TOK_BISON_WITH_CLEANUP_EXPR", "TOK_BISON_GIMPLE_NOP",
  "TOK_BISON_CLEANUP_POINT_EXPR", "TOK_BISON_PLACEHOLDER_EXPR",
  "TOK_BISON_REDUC_MAX_EXPR", "TOK_BISON_REDUC_MIN_EXPR",
  "TOK_BISON_REDUC_PLUS_EXPR", "TOK_BISON_PLUS_EXPR",
  "TOK_BISON_MINUS_EXPR", "TOK_BISON_MULT_EXPR",
  "TOK_BISON_TRUNC_DIV_EXPR", "TOK_BISON_CEIL_DIV_EXPR",
  "TOK_BISON_FLOOR_DIV_EXPR", "TOK_BISON_ROUND_DIV_EXPR",
  "TOK_BISON_TRUNC_MOD_EXPR", "TOK_BISON_CEIL_MOD_EXPR",
  "TOK_BISON_FLOOR_MOD_EXPR", "TOK_BISON_ROUND_MOD_EXPR",
  "TOK_BISON_RDIV_EXPR", "TOK_BISON_EXACT_DIV_EXPR",
  "TOK_BISON_FIX_TRUNC_EXPR", "TOK_BISON_FIX_CEIL_EXPR",
  "TOK_BISON_FIX_FLOOR_EXPR", "TOK_BISON_FIX_ROUND_EXPR",
  "TOK_BISON_FLOAT_EXPR", "TOK_BISON_NEGATE_EXPR", "TOK_BISON_MIN_EXPR",
  "TOK_BISON_MAX_EXPR", "TOK_BISON_ABS_EXPR", "TOK_BISON_LSHIFT_EXPR",
  "TOK_BISON_RSHIFT_EXPR", "TOK_BISON_LROTATE_EXPR",
  "TOK_BISON_RROTATE_EXPR", "TOK_BISON_BIT_IOR_EXPR",
  "TOK_BISON_BIT_XOR_EXPR", "TOK_BISON_BIT_AND_EXPR",
  "TOK_BISON_BIT_NOT_EXPR", "TOK_BISON_TRUTH_ANDIF_EXPR",
  "TOK_BISON_TRUTH_ORIF_EXPR", "TOK_BISON_TRUTH_AND_EXPR",
  "TOK_BISON_TRUTH_OR_EXPR", "TOK_BISON_TRUTH_XOR_EXPR",
  "TOK_BISON_TRUTH_NOT_EXPR", "TOK_BISON_LT_EXPR", "TOK_BISON_LE_EXPR",
  "TOK_BISON_GT_EXPR", "TOK_BISON_GE_EXPR", "TOK_BISON_EQ_EXPR",
  "TOK_BISON_NE_EXPR", "TOK_BISON_UNORDERED_EXPR",
  "TOK_BISON_ORDERED_EXPR", "TOK_BISON_UNLT_EXPR", "TOK_BISON_UNLE_EXPR",
  "TOK_BISON_UNGT_EXPR", "TOK_BISON_UNGE_EXPR", "TOK_BISON_UNEQ_EXPR",
  "TOK_BISON_LTGT_EXPR", "TOK_BISON_IN_EXPR", "TOK_BISON_SET_LE_EXPR",
  "TOK_BISON_CARD_EXPR", "TOK_BISON_RANGE_EXPR", "TOK_BISON_CONVERT_EXPR",
  "TOK_BISON_NOP_EXPR", "TOK_BISON_NON_LVALUE_EXPR",
  "TOK_BISON_VIEW_CONVERT_EXPR", "TOK_BISON_SAVE_EXPR",
  "TOK_BISON_UNSAVE_EXPR", "TOK_BISON_ADDR_EXPR",
  "TOK_BISON_REFERENCE_EXPR", "TOK_BISON_FDESC_EXPR",
  "TOK_BISON_COMPLEX_EXPR", "TOK_BISON_CONJ_EXPR",
  "TOK_BISON_REALPART_EXPR", "TOK_BISON_IMAGPART_EXPR",
  "TOK_BISON_PREDECREMENT_EXPR", "TOK_BISON_PREINCREMENT_EXPR",
  "TOK_BISON_POSTDECREMENT_EXPR", "TOK_BISON_POSTINCREMENT_EXPR",
  "TOK_BISON_VA_ARG_EXPR", "TOK_BISON_TRY_CATCH_EXPR",
  "TOK_BISON_TRY_FINALLY", "TOK_BISON_", "TOK_BISON_GIMPLE_GOTO",
  "TOK_BISON_GOTO_SUBROUTINE", "TOK_BISON_GIMPLE_RETURN",
  "TOK_BISON_EXIT_EXPR", "TOK_BISON_LOOP_EXPR", "TOK_BISON_GIMPLE_SWITCH",
  "TOK_BISON_GIMPLE_MULTI_WAY_IF", "TOK_BISON_CASE_LABEL_EXPR",
  "TOK_BISON_GIMPLE_RESX", "TOK_BISON_GIMPLE_ASM", "TOK_BISON_SSA_NAME",
  "TOK_BISON_GIMPLE_PHI", "TOK_BISON_CATCH_EXPR",
  "TOK_BISON_EH_FILTER_EXPR", "TOK_BISON_STATEMENT_LIST",
  "TOK_BISON_TEMPLATE_DECL", "TOK_BISON_TEMPLATE_TYPE_PARM",
  "TOK_BISON_CAST_EXPR", "TOK_BISON_STATIC_CAST_EXPR",
  "TOK_BISON_TYPENAME_TYPE", "TOK_BISON_SIZEOF_EXPR",
  "TOK_BISON_AGGR_INIT_EXPR", "TOK_BISON_SCOPE_REF",
  "TOK_BISON_CTOR_INITIALIZER", "TOK_BISON_DO_STMT", "TOK_BISON_EXPR_STMT",
  "TOK_BISON_FOR_STMT", "TOK_BISON_IF_STMT", "TOK_BISON_RETURN_STMT",
  "TOK_BISON_WHILE_STMT", "TOK_BISON_MODOP_EXPR", "TOK_BISON_NEW_EXPR",
  "TOK_BISON_VEC_COND_EXPR", "TOK_BISON_VEC_PERM_EXPR",
  "TOK_BISON_DOT_PROD_EXPR", "TOK_BISON_VEC_LSHIFT_EXPR",
  "TOK_BISON_VEC_RSHIFT_EXPR", "TOK_BISON_WIDEN_MULT_HI_EXPR",
  "TOK_BISON_WIDEN_MULT_LO_EXPR", "TOK_BISON_VEC_UNPACK_HI_EXPR",
  "TOK_BISON_VEC_UNPACK_LO_EXPR", "TOK_BISON_VEC_UNPACK_FLOAT_HI_EXPR",
  "TOK_BISON_VEC_UNPACK_FLOAT_LO_EXPR", "TOK_BISON_VEC_PACK_TRUNC_EXPR",
  "TOK_BISON_VEC_PACK_SAT_EXPR", "TOK_BISON_VEC_PACK_FIX_TRUNC_EXPR",
  "TOK_BISON_VEC_EXTRACTEVEN_EXPR", "TOK_BISON_VEC_EXTRACTODD_EXPR",
  "TOC_BISON_VEC_INTERLEAVEHIGH_EXPR", "TOC_BISON_VEC_INTERLEAVELOW_EXPR",
  "TOK_BISON_VEC_NEW_EXPR", "TOK_BISON_OVERLOAD",
  "TOK_BISON_REINTERPRET_CAST_EXPR", "TOK_BISON_TEMPLATE_ID_EXPR",
  "TOK_BISON_THROW_EXPR", "TOK_BISON_TRY_BLOCK", "TOK_BISON_ARROW_EXPR",
  "TOK_BISON_HANDLER", "TOK_BISON_BASELINK", "TOK_BISON_NAME",
  "TOK_BISON_TYPE", "TOK_BISON_SRCP", "TOK_BISON_ARG", "TOK_BISON_BODY",
  "TOK_BISON_STRG", "TOK_BISON_LNGT", "TOK_BISON_SIZE", "TOK_BISON_ALGN",
  "TOK_BISON_RETN", "TOK_BISON_PRMS", "TOK_BISON_SCPE", "TOK_BISON_USED",
  "TOK_BISON_VALUE", "TOK_BISON_ARGT", "TOK_BISON_PREC",
  "TOK_BISON_BB_INDEX", "TOK_BISON_MIN", "TOK_BISON_MAX", "TOK_BISON_VALU",
  "TOK_BISON_CHAN", "TOK_BISON_STMT", "TOK_BISON_OP",
  "TOK_BISON_TIME_WEIGHT", "TOK_BISON_SIZE_WEIGHT",
  "TOK_BISON_RTL_SIZE_WEIGHT", "TOK_BISON_VARS", "TOK_BISON_UNQL",
  "TOK_BISON_ELTS", "TOK_BISON_DOMN", "TOK_BISON_BLOC", "TOK_BISON_DCLS",
  "TOK_BISON_MNGL", "TOK_BISON_PTD", "TOK_BISON_REFD", "TOK_BISON_QUAL",
  "TOK_BISON_VALR", "TOK_BISON_VALX", "TOK_BISON_FLDS", "TOK_BISON_VFLD",
  "TOK_BISON_BPOS", "TOK_BISON_FN", "TOK_BISON_GOTO", "TOK_BISON_REAL",
  "TOK_BISON_IMAG", "TOK_BISON_BINFO", "TOK_BISON_PUB", "TOK_BISON_PROT",
  "TOK_BISON_PRIV", "TOK_BISON_BINF", "TOK_BISON_UID", "TOK_BISON_OLD_UID",
  "TOK_BISON_INIT", "TOK_BISON_FINI", "TOK_BISON_PURP", "TOK_BISON_PRED",
  "TOK_BISON_SUCC", "TOK_BISON_HPL", "TOK_BISON_LOOP_ID",
  "TOK_BISON_ATTRIBUTES", "TOK_BISON_PHI", "TOK_BISON_RES",
  "TOK_BISON_DEF", "TOK_BISON_EDGE", "TOK_BISON_VAR", "TOK_BISON_DEF_STMT",
  "TOK_BISON_ADDR_STMT", "TOK_BISON_USE_STMT", "TOK_BISON_VERS",
  "TOK_BISON_ORIG_VERS", "TOK_BISON_CNST", "TOK_BISON_CLAS",
  "TOK_BISON_DECL", "TOK_BISON_CLNP", "TOK_BISON_LAB", "TOK_BISON_TRY",
  "TOK_BISON_EX", "TOK_BISON_OUT", "TOK_BISON_IN", "TOK_BISON_STR",
  "TOK_BISON_CLOB", "TOK_BISON_CLOBBER", "TOK_BISON_REF", "TOK_BISON_FNCS",
  "TOK_BISON_CSTS", "TOK_BISON_RSLT", "TOK_BISON_INST", "TOK_BISON_SPCS",
  "TOK_BISON_CLS", "TOK_BISON_BFLD", "TOK_BISON_CTOR", "TOK_BISON_NEXT",
  "TOK_BISON_COND", "TOK_BISON_EXPR", "TOK_BISON_THEN", "TOK_BISON_ELSE",
  "TOK_BISON_CRNT", "TOK_BISON_HDLR", "TOK_BISON_USE_TMPL",
  "TOK_BISON_TMPL_PARMS", "TOK_BISON_TMPL_ARGS", "TOK_BISON_ARTIFICIAL",
  "TOK_BISON_SYSTEM", "TOK_BISON_OPERATING_SYSTEM",
  "TOK_BISON_LIBRARY_SYSTEM", "TOK_BISON_EXTERN", "TOK_BISON_C",
  "TOK_BISON_LSHIFT", "TOK_BISON_GLOBAL_INIT", "TOK_BISON_GLOBAL_FINI",
  "TOK_BISON_UNDEFINED", "TOK_BISON_BUILTIN", "TOK_BISON_OPERATOR",
  "TOK_BISON_OVERFLOW", "TOK_BISON_VIRT", "TOK_BISON_UNSIGNED",
  "TOK_BISON_STRUCT", "TOK_BISON_UNION", "TOK_BISON_REGISTER",
  "TOK_BISON_STATIC", "TOK_BISON_REVERSE_RESTRICT", "TOK_BISON_DEFAULT",
  "TOK_BISON_VOLATILE", "TOK_BISON_INF", "TOK_BISON_NAN",
  "TOK_BISON_VARARGS", "TOK_BISON_ENTRY", "TOK_BISON_EXIT",
  "TOK_BISON_NEW", "TOK_BISON_DELETE", "TOK_BISON_ASSIGN",
  "TOK_BISON_MEMBER", "TOK_BISON_PUBLIC", "TOK_BISON_PRIVATE",
  "TOK_BISON_PROTECTED", "TOK_BISON_NORETURN", "TOK_BISON_NOINLINE",
  "TOK_BISON_ALWAYS_INLINE", "TOK_BISON_UNUSED", "TOK_BISON_CONST",
  "TOK_BISON_TRANSPARENT_UNION", "TOK_BISON_MODE", "TOK_BISON_SECTION",
  "TOK_BISON_ALIGNED", "TOK_BISON_PACKED", "TOK_BISON_WEAK",
  "TOK_BISON_ALIAS", "TOK_BISON_NO_INSTRUMENT_FUNCTION",
  "TOK_BISON_MALLOC", "TOK_BISON_NO_STACK_LIMIT", "TOK_BISON_NO_STACK",
  "TOK_BISON_PURE", "TOK_BISON_DEPRECATED", "TOK_BISON_VECTOR_SIZE",
  "TOK_BISON_VISIBILITY", "TOK_BISON_TLS_MODEL", "TOK_BISON_NONNULL",
  "TOK_BISON_NOTHROW", "TOK_BISON_MAY_ALIAS",
  "TOK_BISON_WARN_UNUSED_RESULT", "TOK_BISON_FORMAT",
  "TOK_BISON_FORMAT_ARG", "TOK_BISON_NULL", "TOK_BISON_CONVERSION",
  "TOK_BISON_VIRTUAL", "TOK_BISON_MUTABLE", "TOK_BISON_PSEUDO_TMPL",
  "TOK_BISON_SPEC", "TOK_BISON_LINE", "TOK_BISON_FIXD", "TOK_BISON_VECNEW",
  "TOK_BISON_VECDELETE", "TOK_BISON_POS", "TOK_BISON_NEG",
  "TOK_BISON_ADDR", "TOK_BISON_DEREF", "TOK_BISON_NOT", "TOK_BISON_LNOT",
  "TOK_BISON_PREINC", "TOK_BISON_PREDEC", "TOK_BISON_PLUSASSIGN",
  "TOK_BISON_PLUS", "TOK_BISON_MINUSASSIGN", "TOK_BISON_MINUS",
  "TOK_BISON_MULTASSIGN", "TOK_BISON_MULT", "TOK_BISON_DIVASSIGN",
  "TOK_BISON_DIV", "TOK_BISON_MODASSIGN", "TOK_BISON_MOD",
  "TOK_BISON_ANDASSIGN", "TOK_BISON_AND", "TOK_BISON_ORASSIGN",
  "TOK_BISON_OR", "TOK_BISON_XORASSIGN", "TOK_BISON_XOR",
  "TOK_BISON_LSHIFTASSIGN", "TOK_BISON_RSHIFTASSIGN", "TOK_BISON_RSHIFT",
  "TOK_BISON_EQ", "TOK_BISON_NE", "TOK_BISON_LT", "TOK_BISON_GT",
  "TOK_BISON_LE", "TOK_BISON_GE", "TOK_BISON_LAND", "TOK_BISON_LOR",
  "TOK_BISON_COMPOUND", "TOK_BISON_MEMREF", "TOK_BISON_SUBS",
  "TOK_BISON_POSTINC", "TOK_BISON_POSTDEC", "TOK_BISON_CALL",
  "TOK_BISON_THUNK", "TOK_BISON_THIS_ADJUSTING",
  "TOK_BISON_RESULT_ADJUSTING", "TOK_BISON_PTRMEM", "TOK_BISON_QUAL_R",
  "TOK_BISON_QUAL_V", "TOK_BISON_QUAL_VR", "TOK_BISON_QUAL_C",
  "TOK_BISON_QUAL_CR", "TOK_BISON_QUAL_CV", "TOK_BISON_QUAL_CVR",
  "TOK_BISON_TEMPLATE_PARM_INDEX", "TOK_BISON_INLINE_BODY",
  "TOK_BISON_BITFIELD", "TOK_BISON_WITH_SIZE_EXPR",
  "TOK_BISON_OBJ_TYPE_REF", "TOK_BISON_VUSE", "TOK_BISON_VDEF",
  "TOK_BISON_REDEF_VUSE", "TOK_BISON_VDEP_VUSE", "TOK_BISON_PTR_INFO",
  "TOK_BISON_TRUE_EDGE", "TOK_BISON_FALSE_EDGE",
  "TOK_BISON_POINTER_PLUS_EXPR", "TOK_BISON_TARGET_MEM_REF",
  "TOK_BISON_TARGET_MEM_REF461", "TOK_BISON_SYMBOL", "TOK_BISON_BASE",
  "TOK_BISON_IDX", "TOK_BISON_IDX2", "TOK_BISON_STEP", "TOK_BISON_OFFSET",
  "TOK_BISON_ORIG", "TOK_BISON_TAG", "TOK_BISON_SMT_ANN",
  "TOK_BISON_TRAIT_EXPR", "TOK_BISON_GIMPLE_PREDICT", "TOK_BISON_MEM_REF",
  "TOK_BISON_WIDEN_SUM_EXPR", "TOK_BISON_WIDEN_MULT_EXPR",
  "TOK_BISON_ASSERT_EXPR", "TOK_BISON_RTL", "TOK_BISON_ABS_R",
  "TOK_BISON_AND_R", "TOK_BISON_ASHIFT_R", "TOK_BISON_ASHIFTRT_R",
  "TOK_BISON_BSWAP_R", "TOK_BISON_CALL_R", "TOK_BISON_CALL_INSN_R",
  "TOK_BISON_CLZ_R", "TOK_BISON_CODE_LABEL_R", "TOK_BISON_COMPARE_R",
  "TOK_BISON_CONCAT_R", "TOK_BISON_CTZ_R", "TOK_BISON_DIV_R",
  "TOK_BISON_EQ_R", "TOK_BISON_FFS_R", "TOK_BISON_FIX_R",
  "TOK_BISON_FLOAT_R", "TOK_BISON_FLOAT_EXTEND_R",
  "TOK_BISON_FLOAT_TRUNCATE_R", "TOK_BISON_FRACT_CONVERT_R",
  "TOK_BISON_GE_R", "TOK_BISON_GEU_R", "TOK_BISON_GT_R", "TOK_BISON_GTU_R",
  "TOK_BISON_HIGH_R", "TOK_BISON_IF_THEN_ELSE_R", "TOK_BISON_INSN_R",
  "TOK_BISON_IOR_R", "TOK_BISON_JUMP_INSN_R", "TOK_BISON_LABEL_REF_R",
  "TOK_BISON_LE_R", "TOK_BISON_LEU_R", "TOK_BISON_LSHIFTRT_R",
  "TOK_BISON_LO_SUM_R", "TOK_BISON_LT_R", "TOK_BISON_LTGT_R",
  "TOK_BISON_LTU_R", "TOK_BISON_WRITE_MEM_R", "TOK_BISON_READ_MEM_R",
  "TOK_BISON_MINUS_R", "TOK_BISON_MOD_R", "TOK_BISON_MULT_R",
  "TOK_BISON_NE_R", "TOK_BISON_NEG_R", "TOK_BISON_NOT_R",
  "TOK_BISON_ORDERED_R", "TOK_BISON_PARALLEL_R", "TOK_BISON_PARITY_R",
  "TOK_BISON_PC_R", "TOK_BISON_PLUS_R", "TOK_BISON_POPCOUNT_R",
  "TOK_BISON_REG_R", "TOK_BISON_ROTATE_R", "TOK_BISON_ROTATERT_R",
  "TOK_BISON_SAT_FRACT_R", "TOK_BISON_SET_R", "TOK_BISON_SIGN_EXTEND_R",
  "TOK_BISON_SMAX_R", "TOK_BISON_SMIN_R", "TOK_BISON_SQRT_R",
  "TOK_BISON_SYMBOL_REF_R", "TOK_BISON_TRUNCATE_R", "TOK_BISON_UDIV_R",
  "TOK_BISON_UMAX_R", "TOK_BISON_UMIN_R", "TOK_BISON_UMOD_R",
  "TOK_BISON_UNEQ_R", "TOK_BISON_UNGE_R", "TOK_BISON_UNGT_R",
  "TOK_BISON_UNLE_R", "TOK_BISON_UNLT_R", "TOK_BISON_UNORDERED_R",
  "TOK_BISON_UNSIGNED_FIX_R", "TOK_BISON_UNSIGNED_FLOAT_R",
  "TOK_BISON_UNSIGNED_FRACT_CONVERT_R", "TOK_BISON_UNSIGNED_SAT_FRACT_R",
  "TOK_BISON_XOR_R", "TOK_BISON_ZERO_EXTEND_R", "TOK_BISON_QC_R",
  "TOK_BISON_HC_R", "TOK_BISON_SC_R", "TOK_BISON_DC_R", "TOK_BISON_TC_R",
  "TOK_BISON_CQI_R", "TOK_BISON_CHI_R", "TOK_BISON_CSI_R",
  "TOK_BISON_CDI_R", "TOK_BISON_CTI_R", "TOK_BISON_QF_R", "TOK_BISON_HF_R",
  "TOK_BISON_SF_R", "TOK_BISON_DF_R", "TOK_BISON_TF_R", "TOK_BISON_QI_R",
  "TOK_BISON_HI_R", "TOK_BISON_SI_R", "TOK_BISON_DO_R", "TOK_BISON_TI_R",
  "TOK_BISON_V2SI_R", "TOK_BISON_V4HI_R", "TOK_BISON_V8QI_R",
  "TOK_BISON_CC_R", "TOK_BISON_CCFP_R", "TOK_BISON_CCFPE_R",
  "TOK_BISON_CCZ_R", "TOK_BISON_CLB", "TOK_BISON_CLB_VARS",
  "TOK_BISON_USE", "TOK_BISON_USE_VARS", "$accept", "root", "raw_unit",
  "version", "$@1", "node", "$@2", "node_d", "fun_ext_static",
  "reverse_restrict_flag", "tok_undefined", "tok_builtin", "fun_args",
  "fun_op", "$@3", "fun_op_strings", "fun_tmpl", "$@4", "wfunction_decl",
  "$@5", "$@6", "$@7", "$@8", "$@9", "$@10", "$@11", "$@12", "$@13",
  "$@14", "$@15", "$@16", "$@17", "$@18", "widentifier_node", "$@19",
  "wfunction_type", "$@20", "$@21", "$@22", "tok_varargs",
  "parm_tok_register", "wparm_decl", "$@23", "$@24", "$@25", "$@26",
  "$@27", "$@28", "$@29", "$@30", "gimple_bind_vars", "$@31",
  "wgimple_bind", "$@32", "winteger_cst", "$@33", "$@34", "wtree_list",
  "$@35", "$@36", "$@37", "var_decl_ext_stat", "var_tok_register",
  "wvar_decl", "$@38", "$@39", "$@40", "$@41", "$@42", "$@43", "$@44",
  "$@45", "$@46", "$@47", "$@48", "$@49", "$@50", "$@51",
  "var_decl_definitions", "var_decl_definition", "var_decl_uses",
  "var_decl_use", "var_decl_addressings", "var_decl_addressing",
  "wstring_cst", "$@52", "$@53", "$@54", "wvoid_type", "$@55",
  "wboolean_type", "$@56", "wlang_type", "$@57", "complex_tok_unsigned",
  "wcomplex_type", "$@58", "wvector_type", "$@59", "wCharType", "$@60",
  "woffset_type", "$@61", "wset_type", "$@62", "wqual_union_type", "$@63",
  "wtypename_type", "$@64", "wtemplate_type_parm", "$@65", "wreal_type",
  "$@66", "int_tok_unsigned", "winteger_type", "$@67", "$@68", "$@69",
  "$@70", "enum_tok_unsigned", "wenumeral_type", "$@71", "$@72", "$@73",
  "$@74", "wpointer_type", "$@75", "wreference_type", "$@76",
  "warray_type", "$@77", "$@78", "tok_ptrmem", "tok_spec", "tok_struct",
  "record_flds", "record_fncs", "rec_tmpl", "$@79", "wrecord_type", "$@80",
  "$@81", "$@82", "$@83", "$@84", "union_flds", "union_fncs",
  "wunion_type", "$@85", "wmethod_type", "$@86", "$@87", "$@88",
  "type_tmpl", "$@89", "wtype_decl", "$@90", "wtranslation_unit_decl",
  "$@91", "wlabel_decl", "$@92", "wconst_decl", "$@93", "wnamespace_decl",
  "$@94", "wresult_decl", "$@95", "$@96", "$@97", "$@98", "$@99", "$@100",
  "wfield_decl", "$@101", "$@102", "$@103", "$@104", "$@105", "$@106",
  "$@107", "$@108", "wtemplate_decl", "$@109", "$@110", "$@111", "$@112",
  "wreturn_expr", "$@113", "wgoto_expr", "$@114", "wgimple_label", "$@115",
  "wunsave_expr", "$@116", "wgimple_resx", "$@117", "wva_arg_expr",
  "$@118", "wexit_expr", "$@119", "wloop_expr", "$@120", "wnop_expr",
  "$@121", "wgimple_nop", "$@122", "wnegate_expr", "$@123",
  "wtruth_not_expr", "$@124", "wconvert_expr", "$@125", "waddr_expr",
  "$@126", "wreference_expr", "$@127", "wcleanup_point_expr", "$@128",
  "wfix_trunc_expr", "$@129", "wfix_ceil_expr", "$@130", "wfix_floor_expr",
  "$@131", "wfix_round_expr", "$@132", "wfloat_expr", "$@133", "wabs_expr",
  "$@134", "wconj_expr", "$@135", "wrealpart_expr", "$@136",
  "wimagpart_expr", "$@137", "wcard_expr", "$@138", "wnon_lvalue_expr",
  "$@139", "wview_convert_expr", "$@140", "windirect_ref", "$@141",
  "wmisaligned_indirect_ref", "$@142", "wcast_expr", "$@143",
  "wstatic_cast_expr", "$@144", "wreinterpret_cast_expr", "$@145",
  "wsizeof_expr", "$@146", "wthrow_expr", "$@147", "warrow_expr", "$@148",
  "wbuffer_ref", "$@149", "wgimple_predict", "$@150", "wgimple_assign",
  "$@151", "$@152", "$@153", "$@154", "$@155", "$@156",
  "gimple_modify_uses", "gimple_modify_use", "gimple_modify_clbs",
  "gimple_modify_clb", "wgt_expr", "$@157", "wreduc_max_expr", "$@158",
  "wreduc_min_expr", "$@159", "wreduc_plus_expr", "$@160", "wplus_expr",
  "$@161", "wpointer_plus_expr", "$@162", "wminus_expr", "$@163",
  "wmult_expr", "$@164", "wtrunc_div_expr", "$@165", "wtrunc_mod_expr",
  "$@166", "wle_expr", "$@167", "wtruth_or_expr", "$@168", "wne_expr",
  "$@169", "weq_expr", "$@170", "wcompound_expr", "$@171", "winit_expr",
  "$@172", "wceil_div_expr", "$@173", "wfloor_div_expr", "$@174",
  "wround_div_expr", "$@175", "wceil_mod_expr", "$@176", "wfloor_mod_expr",
  "$@177", "wround_mod_expr", "$@178", "wrdiv_expr", "$@179",
  "wexact_div_expr", "$@180", "wmin_expr", "$@181", "wmax_expr", "$@182",
  "wlshift_expr", "$@183", "wrshift_expr", "$@184", "wlrotate_expr",
  "$@185", "wrrotate_expr", "$@186", "wbit_ior_expr", "$@187",
  "wbit_xor_expr", "$@188", "wbit_and_expr", "$@189", "wbit_not_expr",
  "$@190", "wtruth_andif_expr", "$@191", "wtruth_orif_expr", "$@192",
  "wtruth_and_expr", "$@193", "wtruth_xor_expr", "$@194", "wlt_expr",
  "$@195", "wge_expr", "$@196", "wunordered_expr", "$@197",
  "wordered_expr", "$@198", "wunlt_expr", "$@199", "wunle_expr", "$@200",
  "wungt_expr", "$@201", "wunge_expr", "$@202", "wuneq_expr", "$@203",
  "wltgt_expr", "$@204", "win_expr", "$@205", "wset_le_expr", "$@206",
  "wrange_expr", "$@207", "wcomplex_expr", "$@208", "wpredecrement_expr",
  "$@209", "wpreincrement_expr", "$@210", "wwith_cleanup_expr", "$@211",
  "vector_valu", "wvector_cst", "$@212", "$@213", "wtry_catch_expr",
  "$@214", "wvtable_ref", "$@215", "wtry_finally", "$@216", "wmem_ref",
  "$@217", "wassert_expr", "$@218", "wwiden_sum_expr", "$@219",
  "wwiden_mult_expr", "$@220", "wvec_new_expr", "$@221", "wtry_block",
  "$@222", "$@223", "$@224", "$@225", "tree_vec_op", "tree_vec_op_opt",
  "wtree_vec", "$@226", "$@227", "wtemplate_id_expr", "$@228",
  "wtarget_expr", "$@229", "wssa_name", "$@230", "$@231", "$@232", "$@233",
  "$@234", "$@235", "$@236", "$@237", "$@238", "ptr_info_opt", "ptr_info",
  "tok_ssa_name_def", "def_stmt_list", "tok_volatile_ssa_name",
  "wgimple_switch", "$@239", "$@240", "wsave_expr", "$@241", "$@242",
  "ternary_opt_ops", "$@243", "wscope_ref", "$@244", "$@245",
  "statement_list_stmt", "statement_list_bloc", "statement_list_stmt_bloc",
  "wstatement_list", "$@246", "wnew_expr", "$@247", "wdot_prod_expr",
  "$@248", "wvec_cond_expr", "$@249", "wvec_perm_expr", "$@250",
  "wvec_lshift_expr", "$@251", "wvec_rshift_expr", "$@252",
  "wwiden_mult_hi_expr", "$@253", "wwiden_mult_lo_expr", "$@254",
  "wvec_unpack_hi_expr", "$@255", "wvec_unpack_lo_expr", "$@256",
  "wvec_unpack_float_hi_expr", "$@257", "wvec_unpack_float_lo_expr",
  "$@258", "wvec_pack_trunc_expr", "$@259", "wvec_pack_sat_expr", "$@260",
  "wvec_pack_fix_trunc_expr", "$@261", "wvec_extracteven_expr", "$@262",
  "wvec_extractodd_expr", "$@263", "wvec_interleavehigh_expr", "$@264",
  "wvec_interleavelow_expr", "$@265", "wreturn_stmt", "$@266", "$@267",
  "wpostincrement_expr", "$@268", "wmodop_expr", "gimple_phi_tuples",
  "gimple_phi_tuples_pair", "wgimple_phi", "$@269", "$@270", "$@271",
  "$@272", "gimple_multi_way_if_tuples", "gimple_multi_way_if_tuples_pair",
  "wgimple_multi_way_if", "$@273", "$@274", "wpostdecrement_expr", "$@275",
  "tok_overflow", "wreal_cst", "$@276", "$@277", "$@278",
  "wplaceholder_expr", "woverload", "$@279", "$@280", "wfdesc_expr",
  "$@281", "whandler", "$@282", "$@283", "wcatch_expr", "$@284",
  "wcomponent_ref", "$@285", "wexpr_stmt", "$@286", "$@287", "$@288",
  "wcomplex_cst", "$@289", "$@290", "$@291", "weh_filter_expr", "$@292",
  "wctor_initializer", "wconstructor", "$@293", "$@294",
  "constructor_elts", "constructor_elts_pair", "$@295", "wblock", "$@296",
  "wcond_expr", "$@297", "$@298", "$@299", "wgimple_cond", "$@300",
  "wgoto_subroutine", "$@301", "tok_volatile_asm", "wgimple_asm", "$@302",
  "$@303", "$@304", "$@305", "op_op_default", "$@306", "wcase_label_expr",
  "$@307", "binfo_baseinfos", "binfo_baseinfo_elts_pair", "$@308", "$@309",
  "$@310", "binfo_tok_virt", "wbinfo", "$@311", "$@312", "wbaselink",
  "$@313", "warray_ref", "$@314", "wcall_expr", "$@315", "$@316",
  "call_expr_args", "$@317", "wgimple_call", "$@318", "$@319",
  "gimple_call_args", "$@320", "gimple_call_uses", "gimple_call_use",
  "gimple_call_clbs", "gimple_call_clb", "wbit_field_ref", "$@321",
  "warray_range_ref", "$@322", "wtemplate_parm_index", "wwith_size_expr",
  "$@323", "wobj_type_ref", "$@324", "tok_C", "tok_artificial_opt",
  "tok_system_opt", "tok_virtual_opt", "tok_default_opt",
  "tok_operating_system_opt", "tok_library_system_opt", "tok_packed_opt",
  "wdecl_node", "$@325", "$@326", "$@327", "$@328", "$@329", "$@330",
  "$@331", "$@332", "$@333", "$@334", "wexpr_node", "$@335", "dep_vuses",
  "wgimple_node", "$@336", "$@337", "$@338", "$@339", "$@340", "$@341",
  "$@342", "$@343", "$@344", "$@345", "wtype_node", "$@346", "$@347",
  "$@348", "$@349", "$@350", "$@351", "$@352", "qual_opt", "wattr",
  "wattr_el", "wunary_expr", "wbinary_expr", "$@353", "wternary_expr",
  "$@354", "$@355", "wquaternary_expr", "$@356", "$@357",
  "quaternary_opt_op", "$@358", "wsrcp_opt", "$@359", "$@360", "tok_hpl",
  "wloop_id", "wpred_star", "wsucc_star", "phi_star", "stmt_star",
  "rtl_star", "rtl", "rtl_mode", "wbloc", "$@361", "$@362", "$@363",
  "$@364", "$@365", "$@366", "$@367", "$@368", "$@369", "$@370", "wpred",
  "wsucc", "wvuse", "wvuse_opt", "wvdef", "wvdef_opt", "wredef_vuse",
  "wredef_vuse_opt", "wtarget_mem_ref461", "$@371", "$@372", "$@373",
  "$@374", "$@375", "$@376", "wtarget_mem_ref", "$@377", "$@378", "$@379",
  "$@380", "$@381", "$@382", "$@383", "$@384", "wtrait_expr",
  "time_weight_opt", "size_weight_opt", "rtl_size_weight_opt", "phi",
  "stmt", "def", "dep_vuse", "def_stmt", "edge", "arg", "vars", "flds",
  "fncs", "valu", "op", "strg", "valr", "valx", "type", "body", "size",
  "csts", "retn", "elts", "ptd", "refd", "fn", "got", "real", "imag",
  "binf", "res", "var_opt", "str", "next", "expr", "crnt", "hdlr",
  "tmpl_parms", "tmpl_args", "vers", "orig_vers_opt", "algn", "prec",
  "bb_index", "value", "lngt", "used", "line", "orig", "idx", "name_opt",
  "type_opt", "body_opt", "inline_body_opt", "size_opt", "chan_opt",
  "attrib_opt", "prms_opt", "argt_opt", "retn_opt", "scpe_opt", "unql_opt",
  "op_opt", "domn_opt", "dcls_opt", "mngl_opt", "ptd_opt", "vfld_opt",
  "bpos_opt", "fn_opt", "binf_opt", "init_opt", "purp_opt", "valu_opt",
  "cnst_opt", "clas_opt", "clob_opt", "clobber_opt", "rslt_opt",
  "inst_opt", "spcs_opt", "cls_opt", "bfld_opt", "next_opt", "expr_opt",
  "uid_opt", "algn_opt", "virt_opt", "fixd_opt", "use_tmpl_opt", "min_opt",
  "max_opt", "out_opt", "in_opt", "true_edge_opt", "false_edge_opt",
  "symbol_opt", "base_opt", "step_opt", "offset_opt", "tag_opt",
  "orig_opt", "smt_ann_opt", "idx_opt", "idx2_opt", "node_id", "string_id",
  "number_id", "string_id_opt", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,   472,   473,   474,
     475,   476,   477,   478,   479,   480,   481,   482,   483,   484,
     485,   486,   487,   488,   489,   490,   491,   492,   493,   494,
     495,   496,   497,   498,   499,   500,   501,   502,   503,   504,
     505,   506,   507,   508,   509,   510,   511,   512,   513,   514,
     515,   516,   517,   518,   519,   520,   521,   522,   523,   524,
     525,   526,   527,   528,   529,   530,   531,   532,   533,   534,
     535,   536,   537,   538,   539,   540,   541,   542,   543,   544,
     545,   546,   547,   548,   549,   550,   551,   552,   553,   554,
     555,   556,   557,   558,   559,   560,   561,   562,   563,   564,
     565,   566,   567,   568,   569,   570,   571,   572,   573,   574,
     575,   576,   577,   578,   579,   580,   581,   582,   583,   584,
     585,   586,   587,   588,   589,   590,   591,   592,   593,   594,
     595,   596,   597,   598,   599,   600,   601,   602,   603,   604,
     605,   606,   607,   608,   609,   610,   611,   612,   613,   614,
     615,   616,   617,   618,   619,   620,   621,   622,   623,   624,
     625,   626,   627,   628,   629,   630,   631,   632,   633,   634,
     635,   636,   637,   638,   639,   640,   641,   642,   643,   644,
     645,   646,   647,   648,   649,   650,   651,   652,   653,   654,
     655,   656,   657,   658,   659,   660,   661,   662,   663,   664,
     665,   666,   667,   668,   669,   670,   671,   672,   673,   674,
     675,   676,   677,   678,   679,   680,   681,   682,   683,   684,
     685,   686,   687,   688,   689,   690,   691,   692,   693,   694,
     695,   696,   697,   698,   699,   700,   701,   702,   703,   704,
     705,   706,   707,   708,   709,   710,   711,   712,   713,   714,
     715,   716,   717,   718,   719,   720,   721,   722,   723,   724,
     725,   726,   727,   728,   729,   730,   731,   732,   733,   734,
     735,   736,   737,   738,   739,   740,   741,   742,   743,   744,
     745,   746,   747,   748,   749,   750,   751,   752,   753,   754,
     755,   756,   757,   758,   759,   760,   761,   762,   763,   764,
     765,   766,   767,   768,   769,   770,   771,   772,   773,   774,
     775,   776,   777,   778,   779,   780,   781,   782,   783,   784,
     785,   786,   787,   788,   789,   790,   791,   792,   793,   794,
     795,   796,   797,   798,   799,   800,   801,   802,   803,   804,
     805,   806,   807,   808,   809,   810,   811,   812,   813,   814,
     815,   816,   817,   818,   819,   820,   821,   822,   823,   824,
     825,   826
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   572,   573,   573,   574,   574,   576,   575,   578,   577,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   580,   580,   581,   581,   582,
     582,   583,   583,   584,   584,   585,   586,   585,   587,   587,
     588,   589,   588,   591,   592,   593,   594,   595,   596,   597,
     598,   599,   600,   601,   602,   603,   604,   590,   606,   605,
     605,   608,   609,   610,   607,   611,   611,   612,   612,   614,
     615,   616,   617,   618,   619,   620,   621,   613,   622,   623,
     622,   625,   624,   627,   628,   626,   630,   631,   632,   629,
     633,   633,   633,   633,   633,   634,   634,   636,   637,   638,
     639,   640,   641,   642,   643,   644,   645,   646,   647,   648,
     649,   635,   650,   650,   651,   652,   652,   653,   654,   654,
     655,   657,   658,   659,   656,   661,   660,   663,   662,   665,
     664,   666,   666,   668,   667,   670,   669,   672,   671,   674,
     673,   676,   675,   678,   677,   680,   679,   682,   681,   684,
     683,   685,   685,   687,   688,   689,   690,   686,   691,   691,
     693,   694,   695,   696,   692,   698,   697,   700,   699,   702,
     703,   701,   704,   704,   705,   705,   706,   706,   707,   707,
     708,   708,   709,   710,   709,   712,   713,   714,   715,   716,
     711,   717,   717,   718,   718,   720,   719,   722,   723,   724,
     721,   725,   726,   725,   728,   727,   730,   729,   732,   731,
     734,   733,   736,   735,   738,   739,   740,   741,   742,   743,
     737,   745,   746,   747,   748,   749,   750,   751,   752,   744,
     754,   755,   756,   757,   753,   759,   758,   761,   760,   763,
     762,   765,   764,   767,   766,   769,   768,   771,   770,   773,
     772,   775,   774,   777,   776,   779,   778,   781,   780,   783,
     782,   785,   784,   787,   786,   789,   788,   791,   790,   793,
     792,   795,   794,   797,   796,   799,   798,   801,   800,   803,
     802,   805,   804,   807,   806,   809,   808,   811,   810,   813,
     812,   815,   814,   817,   816,   819,   818,   821,   820,   823,
     822,   825,   824,   827,   826,   829,   828,   831,   830,   833,
     832,   835,   836,   837,   838,   839,   840,   834,   841,   841,
     842,   842,   843,   843,   844,   844,   846,   845,   848,   847,
     850,   849,   852,   851,   854,   853,   856,   855,   858,   857,
     860,   859,   862,   861,   864,   863,   866,   865,   868,   867,
     870,   869,   872,   871,   874,   873,   876,   875,   878,   877,
     880,   879,   882,   881,   884,   883,   886,   885,   888,   887,
     890,   889,   892,   891,   894,   893,   896,   895,   898,   897,
     900,   899,   902,   901,   904,   903,   906,   905,   908,   907,
     910,   909,   912,   911,   914,   913,   916,   915,   918,   917,
     920,   919,   922,   921,   924,   923,   926,   925,   928,   927,
     930,   929,   932,   931,   934,   933,   936,   935,   938,   937,
     940,   939,   942,   941,   944,   943,   946,   945,   948,   947,
     950,   949,   952,   951,   954,   953,   955,   955,   957,   958,
     956,   960,   959,   962,   961,   964,   963,   966,   965,   968,
     967,   970,   969,   972,   971,   974,   973,   976,   977,   978,
     979,   975,   980,   980,   981,   981,   983,   984,   982,   986,
     985,   988,   987,   990,   991,   992,   993,   994,   995,   996,
     997,   998,   989,   999,   999,  1000,  1000,  1001,  1001,  1002,
    1002,  1003,  1003,  1005,  1006,  1004,  1008,  1009,  1007,  1010,
    1010,  1011,  1010,  1013,  1014,  1012,  1015,  1015,  1016,  1016,
    1017,  1017,  1017,  1019,  1018,  1021,  1020,  1023,  1022,  1025,
    1024,  1027,  1026,  1029,  1028,  1031,  1030,  1033,  1032,  1035,
    1034,  1037,  1036,  1039,  1038,  1041,  1040,  1043,  1042,  1045,
    1044,  1047,  1046,  1049,  1048,  1051,  1050,  1053,  1052,  1055,
    1054,  1057,  1056,  1059,  1060,  1058,  1062,  1061,  1063,  1064,
    1064,  1065,  1067,  1068,  1069,  1070,  1066,  1071,  1071,  1072,
    1074,  1075,  1073,  1077,  1076,  1078,  1078,  1080,  1081,  1082,
    1079,  1083,  1085,  1086,  1084,  1088,  1087,  1090,  1091,  1089,
    1093,  1092,  1095,  1094,  1097,  1098,  1099,  1096,  1101,  1102,
    1103,  1100,  1105,  1104,  1106,  1108,  1109,  1107,  1110,  1110,
    1112,  1111,  1111,  1114,  1113,  1116,  1117,  1118,  1115,  1120,
    1119,  1122,  1121,  1123,  1123,  1125,  1126,  1127,  1128,  1124,
    1130,  1129,  1129,  1132,  1131,  1133,  1133,  1135,  1134,  1136,
    1134,  1137,  1134,  1138,  1138,  1140,  1141,  1139,  1143,  1142,
    1145,  1144,  1147,  1148,  1146,  1149,  1150,  1149,  1152,  1153,
    1151,  1154,  1155,  1154,  1156,  1156,  1157,  1157,  1158,  1158,
    1159,  1159,  1161,  1160,  1163,  1162,  1164,  1166,  1165,  1168,
    1167,  1169,  1169,  1170,  1170,  1171,  1171,  1172,  1172,  1173,
    1173,  1174,  1174,  1175,  1175,  1176,  1176,  1178,  1179,  1180,
    1181,  1182,  1183,  1184,  1185,  1186,  1187,  1177,  1189,  1188,
    1190,  1190,  1192,  1193,  1194,  1195,  1196,  1197,  1198,  1199,
    1200,  1201,  1191,  1203,  1204,  1205,  1206,  1207,  1208,  1209,
    1202,  1210,  1210,  1210,  1210,  1210,  1210,  1210,  1210,  1211,
    1211,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,
    1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,
    1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,
    1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,
    1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,
    1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,
    1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,
    1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,
    1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,  1212,
    1212,  1212,  1212,  1213,  1215,  1214,  1217,  1218,  1216,  1220,
    1221,  1219,  1222,  1222,  1223,  1222,  1224,  1225,  1226,  1224,
    1227,  1227,  1228,  1228,  1229,  1229,  1230,  1230,  1231,  1231,
    1232,  1232,  1233,  1233,  1234,  1234,  1234,  1234,  1234,  1234,
    1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,
    1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,
    1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,
    1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,
    1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,
    1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,
    1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,  1234,
    1234,  1234,  1235,  1235,  1235,  1235,  1235,  1235,  1235,  1235,
    1235,  1235,  1235,  1235,  1235,  1235,  1235,  1235,  1235,  1235,
    1235,  1235,  1235,  1235,  1235,  1235,  1235,  1235,  1235,  1235,
    1237,  1238,  1239,  1240,  1241,  1242,  1243,  1244,  1245,  1246,
    1236,  1247,  1247,  1248,  1248,  1249,  1250,  1250,  1251,  1252,
    1252,  1253,  1254,  1254,  1256,  1257,  1258,  1259,  1260,  1261,
    1255,  1263,  1264,  1265,  1266,  1267,  1268,  1269,  1270,  1262,
    1271,  1272,  1272,  1273,  1273,  1274,  1274,  1275,  1276,  1277,
    1278,  1279,  1280,  1281,  1282,  1283,  1284,  1285,  1286,  1287,
    1288,  1289,  1290,  1291,  1292,  1293,  1294,  1295,  1296,  1297,
    1298,  1299,  1300,  1301,  1302,  1303,  1304,  1304,  1305,  1306,
    1307,  1308,  1309,  1310,  1311,  1312,  1313,  1313,  1314,  1315,
    1316,  1317,  1318,  1319,  1320,  1321,  1322,  1323,  1323,  1324,
    1324,  1325,  1325,  1326,  1326,  1327,  1327,  1328,  1328,  1329,
    1329,  1330,  1330,  1331,  1331,  1332,  1332,  1333,  1333,  1334,
    1334,  1335,  1335,  1336,  1336,  1337,  1337,  1338,  1338,  1339,
    1339,  1340,  1340,  1341,  1341,  1342,  1342,  1343,  1343,  1344,
    1344,  1345,  1345,  1346,  1346,  1347,  1347,  1348,  1348,  1349,
    1349,  1350,  1350,  1351,  1351,  1352,  1352,  1353,  1353,  1354,
    1354,  1355,  1355,  1356,  1356,  1357,  1357,  1358,  1358,  1358,
    1359,  1359,  1360,  1360,  1361,  1361,  1362,  1362,  1363,  1363,
    1364,  1364,  1365,  1365,  1366,  1366,  1367,  1367,  1368,  1368,
    1369,  1369,  1370,  1370,  1371,  1371,  1372,  1372,  1373,  1373,
    1374,  1374,  1375,  1375,  1376,  1376,  1377,  1377,  1378,  1379,
    1380,  1381,  1381
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     1,     2,     0,     5,     0,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     1,     0,     1,     0,
       1,     0,     1,     0,     2,     0,     0,     3,     0,     2,
       0,     0,     3,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    29,     0,     4,
       2,     0,     0,     0,     8,     0,     1,     0,     1,     0,
       0,     0,     0,     0,     0,     0,     0,    17,     0,     0,
       3,     0,     5,     0,     0,     5,     0,     0,     0,     7,
       0,     2,     1,     1,     2,     0,     1,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    29,     0,     2,     2,     0,     2,     2,     0,     2,
       2,     0,     0,     0,     7,     0,     3,     0,     3,     0,
       3,     0,     1,     0,     4,     0,     4,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       4,     0,     1,     0,     0,     0,     0,    11,     0,     1,
       0,     0,     0,     0,    11,     0,     4,     0,     4,     0,
       0,     6,     0,     1,     0,     1,     0,     1,     0,     2,
       0,     2,     0,     0,     3,     0,     0,     0,     0,     0,
      18,     0,     2,     0,     2,     0,     7,     0,     0,     0,
       8,     0,     0,     3,     0,     4,     0,     3,     0,     3,
       0,     4,     0,     4,     0,     0,     0,     0,     0,     0,
      13,     0,     0,     0,     0,     0,     0,     0,     0,    17,
       0,     0,     0,     0,    10,     0,     4,     0,     4,     0,
       4,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     0,     0,     0,     0,     0,    13,     0,     2,
       2,     2,     0,     2,     2,     2,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     0,     3,     0,     3,
       0,     3,     0,     3,     0,     3,     1,     2,     0,     0,
       5,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     0,     0,
       0,     9,     1,     2,     0,     1,     0,     0,     5,     0,
       3,     0,     3,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    20,     0,     2,     2,     2,     1,     1,     1,
       2,     0,     1,     0,     0,     6,     0,     0,     6,     0,
       1,     0,     3,     0,     0,     6,     1,     2,     1,     2,
       0,     1,     1,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     3,     0,     3,     0,     3,     0,
       3,     0,     3,     0,     0,     5,     0,     3,     1,     0,
       2,     2,     0,     0,     0,     0,     9,     0,     2,     2,
       0,     0,     5,     0,     3,     0,     1,     0,     0,     0,
       8,     1,     0,     0,     5,     0,     3,     0,     0,     5,
       0,     3,     0,     3,     0,     0,     0,     7,     0,     0,
       0,     7,     0,     3,     1,     0,     0,     5,     0,     2,
       0,     3,     1,     0,     3,     0,     0,     0,     8,     0,
       4,     0,     3,     0,     1,     0,     0,     0,     0,    11,
       0,     3,     1,     0,     5,     0,     2,     0,     3,     0,
       3,     0,     3,     0,     1,     0,     0,     6,     0,     3,
       0,     3,     0,     0,     6,     0,     0,     3,     0,     0,
       8,     0,     0,     3,     0,     2,     2,     2,     0,     2,
       2,     2,     0,     3,     0,     3,     1,     0,     3,     0,
       3,     0,     1,     0,     1,     0,     1,     0,     1,     0,
       1,     0,     1,     0,     1,     0,     1,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    23,     0,     3,
       0,     2,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    21,     0,     0,     0,     0,     0,     0,     0,
      15,     0,     2,     2,     2,     2,     2,     2,     2,     0,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     2,     0,     4,     0,     0,     6,     0,
       0,     6,     0,     1,     0,     3,     0,     0,     0,     6,
       0,     1,     0,     2,     0,     2,     0,     2,     0,     2,
       0,     2,     0,     4,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     0,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      21,     2,     2,     2,     2,     2,     0,     1,     2,     0,
       1,     2,     0,     1,     0,     0,     0,     0,     0,     0,
      13,     0,     0,     0,     0,     0,     0,     0,     0,    17,
       1,     0,     2,     0,     2,     0,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     0,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     0,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     0,     2,     0,
       2,     0,     2,     0,     2,     0,     2,     0,     2,     0,
       2,     0,     2,     0,     2,     0,     2,     0,     2,     0,
       2,     0,     2,     0,     2,     0,     2,     0,     2,     0,
       2,     0,     2,     0,     2,     0,     2,     0,     2,     0,
       2,     0,     2,     0,     1,     0,     2,     0,     2,     0,
       2,     0,     1,     0,     2,     0,     2,     0,     2,     0,
       2,     0,     2,     0,     2,     0,     2,     0,     2,     2,
       0,     2,     0,     2,     0,     2,     0,     2,     0,     2,
       0,     2,     0,     2,     0,     2,     0,     2,     0,     2,
       0,     2,     0,     2,     0,     2,     0,     2,     0,     2,
       0,     2,     0,     2,     0,     2,     0,     2,     1,     1,
       1,     0,     1
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,     0,     0,     2,     4,  1369,     6,     1,     3,     8,
       5,     0,     0,     0,     0,   276,   656,   803,   315,   343,
     339,   323,   325,   350,   317,   327,   355,   329,   357,   387,
     359,   331,   375,   385,   333,   251,   319,   273,   767,   788,
     628,   311,   233,   398,   400,   394,   287,   259,   404,   411,
     402,   396,   782,   862,   481,   483,   497,   840,   864,   633,
     795,   544,   501,   546,   661,   805,   809,   271,   848,   842,
     624,   443,   455,   771,   518,   520,   522,   524,   528,   530,
     532,   548,   550,   552,   534,   554,   556,   558,   560,   562,
     457,   459,   461,   463,   465,   445,   564,   566,   467,   568,
     570,   572,   574,   576,   578,   580,   582,   584,   586,   588,
     538,   590,   447,   592,   536,   516,   594,   542,   540,   596,
     598,   600,   602,   604,   606,   608,   610,   612,   614,   475,
     616,   449,   441,   477,   479,   686,   431,   451,   453,   775,
     618,   469,   471,   473,   620,   622,   763,   746,   435,   631,
     635,   429,   427,   811,   425,   437,   439,   683,   760,   823,
     433,   815,   663,   752,   780,   792,   703,   420,   337,   485,
     487,   335,   491,   693,   794,   784,   743,   748,   705,   709,
     711,   707,   713,   715,   717,   719,   721,   723,   725,   727,
     729,   731,   733,   735,   737,   739,   741,   645,   772,   489,
     659,   493,   647,   495,   777,   838,   835,   866,   867,   869,
     526,  1191,  1184,  1200,   499,   637,   641,   643,   639,     9,
      10,    11,    12,    13,    14,    15,    17,    22,    24,    18,
      29,    52,    54,    74,    77,    78,    80,    82,   164,   163,
      57,    16,    76,    51,    53,    25,    61,    81,    79,    20,
      60,    55,    92,    50,    56,    62,   162,    21,    42,    43,
     141,   156,   151,   154,   155,    44,    45,    47,    41,    59,
      83,    89,    87,   106,   107,   108,   109,   169,   112,   144,
     145,   146,   137,   139,   140,    85,    86,   165,   166,   196,
     167,   198,   200,    69,   209,    23,    28,    31,    32,    33,
      30,    34,    35,    36,    37,    38,    39,    40,    46,    49,
      95,    96,    97,   100,   101,   102,   103,   104,   170,   105,
     110,   111,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   138,   143,   147,   148,
      71,    91,   152,    94,   153,   210,   213,   211,   212,   194,
     199,    67,   197,    99,    48,    66,    88,   168,    19,   175,
     178,   176,   177,   179,   180,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   173,   150,
     174,    98,   214,   149,    58,    72,   195,   142,   202,   158,
      65,   172,    90,   159,   171,   160,    68,    26,    27,    73,
     157,    75,   161,   201,    84,    63,    64,    93,    70,   203,
     204,   205,   207,   206,   208,     7,     0,   250,   248,  1301,
       0,  1371,   921,   921,   921,   921,   921,   921,   921,   921,
     921,   921,   921,   921,   921,   921,   921,   921,   921,   921,
     921,     0,     0,     0,     0,  1259,  1257,  1257,  1257,  1257,
    1257,  1257,  1257,  1257,  1257,  1257,  1259,  1259,  1259,  1259,
    1259,  1259,  1259,  1259,  1259,  1259,  1277,  1259,  1259,  1259,
    1277,  1259,  1277,  1259,  1259,  1277,  1259,  1259,  1259,  1259,
    1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,
    1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,
    1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,
    1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,
    1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,
    1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,
    1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,
    1259,  1259,  1259,  1259,  1277,  1277,  1259,  1277,  1259,  1259,
    1277,  1277,  1259,  1259,  1277,  1259,  1277,  1259,  1259,   700,
    1257,   921,  1259,  1259,   921,  1259,  1259,     0,     0,  1259,
    1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,
    1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,  1259,     0,
    1259,  1259,  1259,     0,  1259,     0,  1259,  1259,  1259,  1259,
    1259,     0,     0,  1259,  1259,  1259,  1259,  1259,  1219,     0,
       0,   277,     0,   657,  1372,   804,     0,   316,   913,     0,
       0,   321,     0,     0,   318,   328,     0,   330,     0,  1275,
       0,   332,   372,     0,   334,     0,   320,     0,   274,   768,
     789,   629,     0,   312,     0,   234,   887,   399,  1305,   391,
     288,   260,   405,   412,  1285,   397,     0,   783,   898,   863,
    1281,   482,   484,   498,     0,   841,   865,   634,   796,     0,
     545,     0,   502,   902,   547,   662,     0,     0,   268,     0,
       0,   625,   444,   456,   519,   521,   523,   525,   529,   531,
     533,   549,   551,   553,   535,   555,   557,   559,   561,   563,
     458,   460,   462,   464,   466,   446,   565,   567,   468,   569,
     571,   573,   575,   577,   579,   581,   583,   585,   587,   589,
     539,   591,   448,   593,   537,   517,   595,   543,   541,   597,
     599,   601,   603,   605,   607,   609,   611,   613,   615,   476,
     617,   450,   442,   478,   480,     0,   432,   452,   454,   776,
     619,   470,   472,   474,   621,   623,   764,   747,   436,   632,
     636,     0,     0,   812,  1281,   438,   440,     0,   761,     0,
     434,   813,   664,   753,   781,   793,     0,  1160,   701,   702,
     704,   698,   696,  1313,   338,   486,   488,   336,   492,     0,
       0,   785,   744,   706,   710,   712,   708,   714,   716,   718,
     720,   722,   724,   726,   728,   730,   732,   734,   736,   738,
     740,   742,   646,     0,   773,   490,   660,   494,   648,   496,
     778,   839,   836,   868,   870,   527,  1192,  1185,   500,   638,
     642,   644,   640,   249,  1368,  1302,  1303,  1370,  1252,   654,
     922,   923,   924,   925,   926,   927,   928,  1257,     0,   344,
     340,   322,   324,     0,   326,   351,     0,   356,     0,   358,
       0,   388,   360,     0,   362,   373,   381,     0,   252,  1222,
       0,   765,     0,     0,  1260,     0,  1258,   225,  1287,     0,
     401,   395,   392,  1336,  1273,  1299,   929,     0,   403,     0,
    1026,  1036,     0,  1023,  1029,   798,  1024,  1278,     0,     0,
     806,   810,     0,     0,   269,     0,   849,   843,   687,   430,
     428,   426,   684,   757,   822,     0,   820,   814,     0,  1236,
       0,  1208,     0,   697,   699,     0,   421,   694,  1254,     0,
    1325,  1241,  1267,     0,     0,   833,  1350,  1352,     0,  1304,
     278,   655,   658,   652,   914,  1249,  1371,  1227,   348,  1228,
    1229,  1276,  1271,  1283,  1243,   363,  1289,     0,   383,  1226,
    1271,     0,   275,   766,     0,     0,   790,   630,   626,   313,
     226,   235,     0,   888,  1306,     0,     0,   289,     0,   261,
       0,   406,   413,  1286,  1218,     0,     0,   899,  1282,     0,
     797,  1281,   503,     0,   903,     0,  1214,     0,   272,   268,
    1230,   851,   845,   689,     0,   762,     0,   824,  1281,     0,
     816,     0,   665,     0,   754,  1161,  1314,  1315,     0,     0,
     786,     0,   745,     0,   774,   649,   779,   834,   825,     0,
    1193,     0,  1186,  1217,  1267,   653,  1279,   345,   349,  1338,
       0,   389,     0,   361,     0,   376,     0,   374,     0,  1297,
     382,   253,  1251,     0,   769,  1232,     0,   627,     0,   228,
     929,  1288,  1360,   393,  1337,   929,  1274,  1265,  1300,     0,
     946,   947,   942,  1014,   972,   968,   969,   939,   931,   932,
     933,   934,   935,   937,   936,   938,   940,   941,   943,   944,
     945,   948,   949,   950,   951,   952,   953,   954,   955,   956,
     957,   958,   959,   960,   961,   962,   963,   964,   965,   966,
     967,   970,   971,   973,   974,   975,   976,   977,   978,   979,
     980,   982,   981,   983,   984,   985,   986,   987,   988,   989,
     990,   991,   992,   993,   994,   995,   996,   997,   998,   999,
    1000,  1001,  1002,  1003,  1004,  1005,  1006,  1007,  1008,  1009,
    1010,  1011,  1012,  1013,  1015,  1016,  1017,  1018,  1019,  1020,
    1021,  1022,  1299,   930,  1027,  1037,  1030,     0,   799,   802,
     800,  1025,     0,  1250,  1176,   807,  1223,   270,     0,   854,
     852,   844,   846,   688,   690,   685,     0,   758,     0,  1231,
     821,  1238,  1342,  1237,     0,  1235,   749,  1040,     0,   422,
     695,  1240,     0,  1326,  1268,     0,   837,  1351,  1352,  1353,
    1356,   279,     0,   915,   341,     0,   352,  1272,  1307,  1284,
    1290,  1319,  1244,  1215,     0,     0,   384,   386,   255,  1220,
       0,     0,   791,   314,   227,   236,     0,   889,   290,     0,
     262,     0,   407,   414,  1281,     0,  1032,  1256,     0,   504,
       0,  1177,   904,     0,  1213,     0,     0,   858,   854,   851,
     845,     0,  1209,     0,   759,     0,   817,     0,   666,   755,
    1041,  1162,  1316,  1317,     0,   787,     0,   650,   827,   829,
     831,   826,  1194,     0,  1187,  1280,  1265,   342,  1338,  1339,
    1340,     0,   390,     0,   377,  1298,  1216,   256,   254,     0,
     770,  1233,   229,   230,  1361,  1259,   280,  1266,     0,  1224,
       0,  1265,  1028,  1038,  1031,  1033,   801,   508,  1175,  1179,
     808,   856,   857,     0,     0,   850,   858,   855,   853,   847,
     692,  1212,  1343,  1344,  1245,  1246,   750,   877,     0,  1042,
       0,   423,  1239,  1242,  1323,     0,     0,     0,  1364,  1357,
    1364,   916,   346,     0,   353,  1308,  1320,  1321,  1221,   237,
     231,   890,   283,   282,   291,     0,   263,   408,   415,     0,
       0,     0,     0,   505,   508,     0,  1180,   905,   860,   861,
     859,     0,   818,     0,   667,   878,   756,   751,     0,  1163,
    1318,  1271,     0,   651,     0,   828,   830,   832,     0,  1195,
    1188,  1277,  1340,  1341,     0,     0,   378,  1334,     0,  1277,
     284,   281,  1299,  1248,   873,   885,     0,  1039,  1035,   510,
     511,   512,   509,  1178,   900,  1345,  1309,  1247,   673,  1043,
    1044,   424,  1324,  1234,  1365,  1354,  1354,   917,   347,     0,
     354,  1322,  1291,     0,   238,   232,   891,   292,   874,   264,
     886,   409,   416,     0,     0,   506,   512,   906,     0,   819,
       0,     0,   668,   673,  1164,     0,  1196,  1189,  1330,  1225,
       0,   379,  1335,  1332,  1036,  1265,     0,  1362,   885,   514,
     515,  1311,   513,     0,  1182,   901,  1310,   675,   676,   681,
     674,     0,  1046,  1045,  1355,  1356,  1366,     0,   918,  1292,
     364,     0,   239,  1267,   293,     0,   265,     0,   410,   417,
    1312,   507,  1210,     0,  1183,   907,     0,   682,   877,   678,
     677,   679,  1171,  1172,  1165,  1197,     0,  1190,  1331,   885,
     365,   366,  1333,  1295,   892,     0,  1253,   257,  1363,  1293,
    1181,  1036,  1211,   669,   680,     0,  1346,  1047,     0,  1367,
     919,   367,   368,     0,   240,  1269,   294,   258,   266,     0,
     418,   908,   879,  1173,  1174,     0,  1166,     0,  1198,   875,
     370,  1296,   223,     0,   893,   885,  1362,  1294,  1362,  1201,
     880,   670,  1347,  1348,  1255,  1358,   876,   920,  1297,   369,
     241,  1270,   873,   295,   267,   419,     0,   909,  1338,     0,
    1167,     0,  1199,   371,   380,   219,   224,   894,     0,  1202,
    1203,   671,  1349,  1048,  1359,   220,   242,   881,   296,     0,
     910,  1340,  1168,   221,   882,   895,   302,  1204,  1205,   672,
       0,  1050,  1049,   222,   243,   883,     0,   297,   302,     0,
     911,  1207,  1169,   215,   884,   896,   304,   305,   303,  1206,
    1052,  1052,  1051,   216,   244,   871,     0,   298,   305,   912,
    1170,   217,   872,  1327,   307,   308,   306,     0,   218,   245,
       0,     0,   897,     0,   299,   308,  1054,  1055,  1056,  1057,
    1058,  1059,  1060,  1061,  1062,  1063,  1064,  1065,  1066,  1067,
    1068,  1069,  1070,  1071,  1072,  1073,  1074,  1075,  1076,  1077,
    1078,  1079,  1080,  1081,  1082,  1083,  1084,  1085,  1087,  1086,
    1088,  1089,  1090,  1091,  1092,  1093,  1094,  1095,  1096,  1097,
    1098,  1099,  1100,  1101,  1102,  1103,  1104,  1105,  1106,  1107,
    1108,  1109,  1110,  1111,  1112,  1113,  1114,  1115,  1116,  1117,
    1118,  1119,  1120,  1121,  1122,  1123,  1124,  1125,  1126,  1127,
    1128,  1129,  1130,  1131,  1132,  1261,  1328,  1329,   310,   285,
     309,  1133,  1134,  1135,  1136,  1137,  1138,  1139,  1140,  1141,
    1142,  1143,  1144,  1145,  1146,  1147,  1148,  1149,  1150,  1151,
    1152,  1153,  1154,  1155,  1156,  1157,  1158,  1159,  1053,     0,
     246,   286,   300,  1262,  1263,  1362,     0,   247,   301,  1264
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,     4,    11,    10,    12,   219,  1674,  1689,
    1636,  1654,  1610,   991,  1079,  1254,  1379,  1428,   220,   456,
     897,  1080,  1323,  1427,  1493,  1553,  1592,  1625,  1643,  1663,
    1681,  1775,  1814,   221,   629,   222,   449,   980,  1248,  1318,
    1578,   223,   461,   904,  1087,  1328,  1434,  1496,  1557,  1596,
     923,  1019,   224,   481,   225,   451,   890,   226,   429,   856,
    1054,  1384,  1812,   227,   460,   903,  1085,  1326,  1432,  1495,
    1555,  1595,  1628,  1646,  1667,  1685,  1779,  1815,  1657,  1658,
    1677,  1678,  1694,  1695,   228,   455,   895,  1078,   229,   432,
     230,   438,   231,   450,   872,   232,   435,   233,   436,   234,
     439,   235,   441,   236,   445,   237,   448,   238,   584,   239,
     581,   240,   434,  1308,   241,   433,   966,  1234,  1422,  1059,
     242,   437,   968,  1310,  1424,   243,   440,   244,   442,   245,
     444,   973,   976,  1551,  1572,  1590,  1608,   884,   977,   246,
     446,  1241,  1377,  1462,  1520,   978,  1069,   247,   447,   248,
     443,   972,  1238,   901,   995,   249,   459,   250,   465,   251,
     457,   252,   458,   253,   464,   254,   462,   905,  1089,  1330,
    1435,  1497,   255,   463,   906,  1182,  1331,  1436,  1498,  1559,
    1598,   256,   580,  1037,  1293,  1411,   257,   567,   258,   565,
     259,   564,   260,   549,   261,   573,   262,   561,   263,   568,
     264,   569,   265,   545,   266,   485,   267,   508,   268,   525,
     269,   544,   270,   550,   271,   551,   272,   486,   273,   503,
     274,   504,   275,   505,   276,   506,   277,   507,   278,   511,
     279,   554,   280,   555,   281,   556,   282,   542,   283,   546,
     284,   547,   285,   468,   286,   469,   287,   582,   288,   583,
     289,   610,   290,   585,   291,   612,   292,   614,   293,   470,
     294,   623,   295,   476,   918,  1192,  1337,  1441,  1501,  1393,
    1394,  1475,  1476,   296,   528,   297,   487,   298,   488,   299,
     489,   300,   490,   301,   620,   302,   491,   303,   492,   304,
     493,   305,   497,   306,   527,   307,   523,   308,   531,   309,
     530,   310,   475,   311,   477,   312,   494,   313,   495,   314,
     496,   315,   498,   316,   499,   317,   500,   318,   501,   319,
     502,   320,   509,   321,   510,   322,   512,   323,   513,   324,
     514,   325,   515,   326,   516,   327,   517,   328,   518,   329,
     519,   330,   520,   331,   521,   332,   522,   333,   524,   334,
     526,   335,   529,   336,   532,   337,   533,   338,   534,   339,
     535,   340,   536,   341,   537,   342,   538,   343,   539,   344,
     540,   345,   541,   346,   543,   347,   553,   348,   557,   349,
     558,   350,   484,   987,   351,   454,   893,   352,   562,   353,
     473,   354,   563,   355,   624,   356,   627,   357,   625,   358,
     626,   359,   608,   360,   613,   953,  1225,  1364,   961,   962,
     361,   430,   859,   362,   611,   363,   478,   364,   575,   939,
    1214,  1355,  1448,  1509,  1582,  1618,  1641,  1482,  1483,  1538,
    1539,  1540,   365,   570,  1024,   366,   548,  1023,  1203,  1281,
     367,   586,  1038,   798,   799,   800,   368,   579,   369,   589,
     370,   592,   371,   590,   372,   591,   373,   593,   374,   594,
     375,   595,   376,   596,   377,   597,   378,   598,   379,   599,
     380,   600,   381,   601,   382,   602,   383,   603,   384,   604,
     385,   605,   386,   606,   387,   607,   388,   588,   950,   389,
     560,   390,  1289,  1356,   391,   576,   940,  1216,  1357,  1025,
    1207,   392,   571,   933,   393,   559,   984,   394,   452,   891,
    1250,   395,   396,   609,   952,   397,   552,   398,   615,   954,
     399,   577,   400,   466,   401,   587,   949,  1222,   402,   453,
     892,  1076,   403,   578,   404,   405,   474,   915,  1010,  1188,
    1268,   406,   431,   407,   479,  1015,  1273,   408,   480,   409,
     566,   938,   410,   574,  1212,  1353,  1446,   935,  1028,   411,
     572,  1226,  1301,  1365,  1366,  1367,  1048,   412,   617,   955,
     413,   616,   414,   471,   415,   483,  1022,  1201,  1280,   416,
     482,  1021,  1199,  1279,  1277,  1278,  1345,  1346,   417,   467,
     418,   472,   419,   420,   618,   421,   619,  1683,  1469,  1607,
    1406,  1601,  1645,  1665,  1471,   665,   898,  1082,  1325,  1429,
    1494,  1575,  1612,  1637,  1655,  1675,   689,   911,  1477,   692,
     919,  1194,  1339,  1444,  1504,  1561,  1599,  1630,  1648,  1670,
     637,   867,  1056,  1306,  1421,  1488,  1549,  1589,   638,  1002,
    1183,   681,   690,  1011,   677,  1005,  1264,   685,  1009,  1266,
    1334,  1390,  1007,  1265,  1389,  1291,  1409,  1484,  1544,  1642,
    1662,  1679,  1774,  1808,   801,   942,  1217,  1359,  1450,  1512,
    1566,  1603,  1633,  1651,  1671,  1513,  1567,  1271,  1272,  1396,
    1397,  1534,  1535,   422,   622,   957,  1230,  1370,  1456,  1516,
     423,   621,   956,  1228,  1368,  1455,  1515,  1568,  1605,   424,
    1617,  1640,  1660,  1652,   802,  1208,  1505,  1541,  1284,  1200,
     924,  1070,  1246,   959,   910,   428,  1074,  1320,   658,  1018,
    1262,  1460,   888,   874,   877,   879,   926,  1027,   986,  1252,
    1415,  1034,  1032,  1030,  1295,  1040,   834,  1297,   885,  1067,
    1288,  1404,  1386,   869,  1014,   982,   633,  1526,   811,  1588,
    1190,   666,   678,  1810,  1817,  1260,  1044,  1594,  1061,   999,
     881,   693,  1233,   913,  1063,   908,   993,  1065,  1491,  1580,
    1574,  1247,  1001,   631,   960,   900,  1312,  1479,  1531,   946,
    1219,  1361,  1314,  1426,  1413,  1042,  1692,  1518,  1522,  1464,
     997,  1236,  1374,  1286,  1402,  1586,  1620,  1050,  1052,  1486,
    1304,  1622,  1257,  1528,  1419,  1547,   855,   634,   858,   635
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -1571
static const yytype_int16 yypact[] =
{
      62,    75,    60,    78, -1571, -1571, -1571, -1571,    78, -1571,
   -1571,    76,  1471,    75,  -176, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571,    75, -1571, -1571,  -177,
    -119,    75,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -107,  -107,  -107,  -107,  -103,   -99,   -99,   -99,   -99,
     -99,   -99,   -99,   -99,   -99,   -99,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -105,  -103,  -103,  -103,
    -105,  -103,  -105,  -103,  -103,  -105,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -105,  -105,  -103,  -105,  -103,  -103,
    -105,  -105,  -103,  -103,  -105,  -103,  -105,  -103,  -103,  -174,
     -99,  -144,  -103,  -103,  -144,  -103,  -103,  -253,  -253,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -180,
    -103,  -103,  -103,  -253,  -103,  -253,  -103,  -103,  -103,  -103,
    -103,  -107,  -107,  -103,  -103,  -103,  -103,  -103, -1571,  -119,
     122, -1571,   121, -1571, -1571, -1571,  -354, -1571, -1571,   -96,
     -96,  -195,  -106,   -96, -1571, -1571,  -112, -1571,  -111,   -85,
    -106, -1571,  -173,  -186, -1571,   -76, -1571,   122, -1571, -1571,
   -1571, -1571,   122, -1571,   122, -1571, -1571, -1571,  -136,  -173,
   -1571, -1571, -1571, -1571,   -94, -1571,   -84, -1571, -1571, -1571,
     -83, -1571, -1571, -1571,   -84, -1571, -1571, -1571, -1571,   -84,
   -1571,   122, -1571, -1571, -1571, -1571,   -84,   -84,   -86,  -100,
    -100, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571,   -84, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571,   -84,   -84, -1571,   -83, -1571, -1571,   -84, -1571,  -192,
   -1571,  -179, -1571, -1571, -1571, -1571,   122, -1571,   -79,   -82,
   -1571, -1571, -1571,  -141, -1571, -1571, -1571, -1571, -1571,   -84,
     121, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571,   122, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571,   -74, -1571, -1571,   -84,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571,   -99,   121, -1571,
   -1571, -1571, -1571,   122, -1571, -1571,   122, -1571,   122, -1571,
     122, -1571, -1571,   122,  -270, -1571, -1571,   122, -1571, -1571,
     -66,  -161,   -90,   -74, -1571,   -51, -1571,  -152,   -70,   122,
   -1571, -1571, -1571,  -135,   -48,   -81, -1571,   122, -1571,   122,
   -1571,   -35,   122, -1571, -1571, -1571, -1571, -1571,   -84,   -44,
   -1571, -1571,   122,   -31, -1571,   122, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571,   -67, -1571, -1571,   -98,   -88,
     -78, -1571,   121, -1571, -1571,   122, -1571, -1571, -1571,  -114,
    -113, -1571,   -38,   -31,   -31,  -129,  -255,  -254,   122, -1571,
   -1571,   -84, -1571, -1571, -1571, -1571,    75, -1571,  -128, -1571,
   -1571, -1571,   -22,   -39, -1571, -1571,   -42,  -104,   -43, -1571,
     -22,   121, -1571, -1571,   -37,   122, -1571,   -74, -1571, -1571,
   -1571, -1571,   122, -1571, -1571,  -104,   121, -1571,   122, -1571,
     122, -1571,   826, -1571, -1571,   -84,    75, -1571, -1571,   -84,
    -208,   -83, -1571,   121, -1571,   -84, -1571,   122, -1571,   -86,
   -1571,    -9,    -9,   -84,   -84,   -65,   122, -1571,   -83,    75,
   -1571,   122, -1571,   122, -1571, -1571, -1571,   -75,   -84,   122,
   -1571,   122, -1571,   122, -1571, -1571, -1571, -1571, -1571,   122,
   -1571,   122, -1571, -1571,   -38, -1571,   -29, -1571, -1571,   -17,
     122, -1571,   122, -1571,   122, -1571,   122, -1571,   122,  -205,
   -1571, -1571, -1571,    75, -1571, -1571,   -41, -1571,  -119, -1571,
   -1571, -1571,  -244, -1571, -1571, -1571, -1571,    -5, -1571,    -3,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571,   -81, -1571, -1571, -1571, -1571,   122, -1571, -1571,
   -1571, -1571,   -84, -1571,  -224, -1571, -1571, -1571,   122,  -477,
   -1571, -1571, -1571, -1571,   -15, -1571,   122, -1571,   -55, -1571,
   -1571, -1571,   -64, -1571,   -53, -1571, -1571,   -40,   122, -1571,
   -1571, -1571,   -73, -1571, -1571,   -77,  -166, -1571,  -254, -1571,
    -223, -1571,   122, -1571,   -93,   122, -1571, -1571,   -47, -1571,
   -1571,   -63, -1571, -1571,   122,   122, -1571, -1571,   -97, -1571,
     -11,   122, -1571, -1571,    75,   826,   122, -1571,   826,   122,
   -1571,   122, -1571, -1571,   -83,   121,   -84, -1571,   -74, -1571,
     122, -1571, -1571,   -84, -1571,    75,   122,  -471,  -477,    -9,
      -9,   -84, -1571,   121, -1571,   122, -1571,   121, -1571,   -65,
   -1571, -1571, -1571,   -60,   122, -1571,   122, -1571, -1571, -1571,
   -1571, -1571, -1571,   122, -1571, -1571,    -5, -1571,   -17, -1571,
      11,   122, -1571,   122, -1571, -1571, -1571, -1571, -1571,    75,
   -1571, -1571, -1571,  -173, -1571,  -103,  -257, -1571,    22, -1571,
      22,    -5, -1571, -1571, -1571,     9, -1571,  -470, -1571,  -196,
   -1571, -1571, -1571,    75,   122, -1571,  -471, -1571, -1571, -1571,
   -1571, -1571, -1571,   -36, -1571,   -30, -1571,  -124,   -55,   -14,
     122, -1571, -1571, -1571,   -50,     1,     1,     1,  -190, -1571,
    -190, -1571, -1571,   122, -1571, -1571, -1571,   -33, -1571, -1571,
   -1571, -1571,   -62,   -59, -1571,   121, -1571, -1571, -1571,   121,
     -84,    75,   122, -1571,  -470,   122, -1571, -1571, -1571, -1571,
   -1571,   122, -1571,   121, -1571, -1571, -1571, -1571,   121, -1571,
   -1571,   -22,   122, -1571,   122, -1571, -1571, -1571,   122, -1571,
   -1571,  -105,    11, -1571,   -32,   122, -1571,  -123,  -104,  -105,
   -1571, -1571,   -81, -1571,   -28,   -91,    22, -1571, -1571, -1571,
   -1571,  -464, -1571, -1571, -1571, -1571,   -26, -1571,  -459, -1571,
   -1571, -1571, -1571, -1571, -1571,  -184,  -184, -1571, -1571,   122,
   -1571, -1571,    23,   121, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571,    75,   122, -1571,  -464,  -169,   122, -1571,
      75,   122, -1571,  -459,    10,   122, -1571, -1571,    56, -1571,
     122, -1571, -1571,   -46,   -35,    -5,    54,  -178,   -91, -1571,
   -1571,   -10, -1571,   122,  -159, -1571, -1571, -1571, -1571,  -234,
   -1571,    19, -1571, -1571, -1571,  -223,  -172,   121, -1571, -1571,
     -92,   121, -1571,   -38, -1571,   121, -1571,   122, -1571, -1571,
   -1571, -1571, -1571,   122, -1571, -1571,   122, -1571,  -124,    12,
   -1571, -1571, -1571, -1571,    18, -1571,   122, -1571, -1571,   -91,
   -1571,   -34, -1571,    35, -1571,    22, -1571,   -27, -1571,    38,
   -1571,   -35, -1571, -1571, -1571,    13,  -154, -1571,  -162, -1571,
   -1571, -1571, -1571,   122, -1571,    25, -1571, -1571, -1571,   122,
   -1571, -1571,    -8, -1571, -1571,   121, -1571,   122, -1571,   -16,
     -43, -1571, -1571,   122, -1571,   -91,  -178, -1571,  -178,    64,
   -1571, -1571, -1571,  -150, -1571,  -158, -1571, -1571,  -205, -1571,
      -9, -1571,   -28, -1571, -1571, -1571,   121, -1571,   -17,   121,
   -1571,   122, -1571, -1571, -1571,   -21, -1571, -1571,    54, -1571,
      66, -1571, -1571, -1571, -1571, -1571, -1571,    14, -1571,   121,
   -1571,    11,    32,   -18, -1571, -1571,    49, -1571,    90, -1571,
     122, -1571, -1571, -1571, -1571,    15,   122, -1571,    49,   121,
   -1571, -1571,   -79,     5, -1571, -1571, -1571,    51, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571,    20,   122, -1571,    51,  -138,
    -138,     0, -1571,  -142, -1571,    59, -1571,  1470, -1571, -1571,
     121,   121, -1571,   122, -1571,    59, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571,   532,   125, -1571, -1571, -1571,    17,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,   122,
   -1571, -1571, -1571, -1571,   -95,  -178,   122, -1571, -1571, -1571
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -1571, -1571,   337, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
    -679, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1313, -1571,
   -1332, -1571, -1348, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1043,
   -1571, -1124, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1129, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571,  -925, -1571, -1571,
   -1571, -1571,  -923, -1571,  -921, -1571,  -988, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1253, -1571,
   -1178, -1571, -1571, -1571, -1457,  -415, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571,   441, -1571, -1571,  -398,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
    -130, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1022,
   -1571,   782,  1205, -1571,  -433, -1571, -1571,  -411, -1571, -1571,
   -1571, -1571, -1458, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1309, -1571, -1571,  -436, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571,  -795,  -924, -1571, -1173,  -991, -1017,
   -1571, -1222, -1239,  -877,  -674,  -524, -1571, -1571,  -421,  -835,
   -1571, -1571, -1571,  -278, -1571, -1571,  -327, -1571, -1571, -1571,
   -1246, -1571, -1571, -1571, -1571, -1571, -1571, -1571,  -665,  -981,
   -1571, -1571, -1300,  -581, -1571, -1571,  -620, -1254,  -535, -1571,
   -1571,  -491,  -453, -1571, -1571, -1277, -1047, -1571,  -963, -1571,
   -1571, -1364, -1571,  -771, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1230, -1157, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571, -1571,
   -1571, -1288, -1395, -1571, -1571, -1571, -1571, -1571,  -849, -1076,
   -1134, -1571, -1571, -1570,  -986, -1571,  -656,    -1,  -799,  -584
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1035
static const yytype_int16 yytable[] =
{
       6,   889,   663,   943,   902,  1202,   894,  1231,   896,   853,
     914,   948,   425,   931,  1083,   916,   988,  1071,   857,   958,
    1372,   688,   920,   921,   857,  1263,  1614,  1458,  1615,  1371,
    1387,   659,   660,   661,   679,   917,  1523,   426,   909,  1536,
     687,  1529,   667,   668,   669,   670,   671,   672,   673,   674,
     675,   701,  1244,   812,  1388,   796,  1382,  1457,  1255,   870,
       7,   686,   875,  1258,   797,  1466,     1,   695,     1,   965,
    1383,   860,   861,   862,   863,   864,   865,   866,   838,     5,
     840,     9,   697,    13,   699,   630,  1245,   702,  1298,  1299,
    1300,   928,  1570,  1275,  1276,   632,  1537,  1343,  1344,   636,
    1391,  1392,   657,  1581,  1473,  1474,   662,   929,   930,   664,
    1077,  1480,  1481,   932,   691,   936,  1690,  1691,  1045,  1046,
    1416,  1417,   792,   810,   833,   854,   857,   868,   871,   876,
     873,   878,   880,  1189,   883,   947,  1472,   934,  1613,   886,
     941,   887,   899,  1035,   427,   907,   909,   912,   922,   925,
     796,   937,   945,   958,   975,   981,   797,   814,   815,   816,
     983,   985,   426,   841,   842,   803,   781,   782,   990,   784,
     992,   996,   787,   788,   998,  1006,   791,   951,   793,  1000,
    1013,  1017,  1072,  1026,  1031,   963,   844,  1039,  1041,  1029,
    1043,  1033,  1049,  1047,  1051,  1058,  1060,  1084,  1062,  1064,
     846,   847,  1198,  1068,  1066,  1206,  1232,  1073,  1235,  1256,
    1259,  1251,  1261,  1270,  1193,  -691,  1283,   967,  1524,  1218,
     969,  1285,   970,  1287,   971,  1290,  1294,   974,  1296,  1303,
    1307,   979,  1311,  1313,  1319,  1360,  1317,  1373,  1385, -1034,
    1191,  1187,  1395,   994,  1012,  1818,  1649,  1403,  1405,  1412,
    1401,  1003,  1408,  1004,  1463,  1576,  1008,  1210,  1414,  1418,
    1459,  1470,  1478,  1202,  1425,  1430,  1016,  1485,  1431,  1020,
    1490,  1503,  1517,  1511,  1525,  1467,  1521,  1527,  1546,  1530,
    1533,  1468,  1565,  1550,  1573,  1536,  1579,  1055,  1585,  1036,
    1571,  1587,  1593,  1619,  1606,  1616,  1621,  1635,  1639,  1577,
    1650,  1653,  1053,   639,   640,   641,   642,   643,   644,   645,
     646,   647,   648,   649,   650,   651,   652,   653,   654,   655,
     656,  1600,  1656,  1659,  1687,  1644,  1676,  1664,  1688,  1075,
    1631,  1184,  1673,  1693,  1682,  1186,  1081,  1809,  1816,     8,
    1197,  1195,  1086,  1811,  1088,  1668,  1686,  1780,  1583,  1204,
    1205,  1442,  1502,  1542,  1510,  1349,  1348,  1347,  1400,  1627,
    1563,  1196,  1680,   944,  1220,  1358,  1564,  1407,  1609,  1623,
    1209,   989,   882,   927,  1638,  1213,   964,  1215,  1624,  1302,
    1487,  1545,  1057,  1221,  1420,  1223,     0,  1224,     0,     0,
       0,  1336,     0,  1227,     0,  1229,     0,     0,     0,     0,
       0,     0,     0,     0,  1237,     0,  1239,     0,  1240,     0,
    1242,     0,  1243,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   628,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  1465,  1451,     0,
       0,   804,     0,     0,   807,     0,     0,     0,  1253,     0,
       0,     0,     0,     0,     0,     0,  1333,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1554,     0,     0,     0,
       0,     0,     0,     0,  1351,     0,     0,     0,  1354,     0,
       0,     0,     0,  1332,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  1269,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1267,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  1274,     0,     0,     0,     0,     0,     0,     0,
    1282,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  1292,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1305,     0,     0,  1309,
       0,     0,     0,     0,     0,     0,  1433,     0,  1315,  1316,
    1437,     0,  1335,  1626,     0,  1321,     0,     0,     0,  1340,
    1324,     0,     0,  1327,  1447,  1329,     0,  1350,     0,  1449,
       0,     0,     0,     0,  1338,     0,     0,     0,     0,     0,
    1342,     0,     0,     0,     0,     0,     0,     0,     0,  1352,
       0,     0,     0,     0,     0,     0,     0,     0,  1362,     0,
    1363,     0,     0,     0,     0,     0,     0,  1369,     0,     0,
       0,     0,     0,     0,     0,  1375,     0,  1376,  1380,     0,
       0,     0,     0,     0,  1492,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  1399,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,  1410,     0,     0,     0,     0,     0,
       0,     0,  1543,     0,     0,     0,  1438,  1423,  1548,     0,
       0,     0,  1552,     0,     0,     0,  1556,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1440,     0,     0,  1443,
       0,     0,     0,     0,     0,  1445,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1452,     0,  1453,     0,
       0,     0,  1454,     0,     0,     0,  1584,     0,     0,  1461,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1602,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  1489,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  1629,  1500,     0,
    1632,     0,  1506,     0,     0,  1508,     0,     0,     0,  1514,
       0,     0,     0,     0,  1519,     0,     0,     0,     0,     0,
    1647,     0,     0,     0,     0,     0,     0,  1532,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1669,     0,     0,     0,     0,     0,     0,  1672,     0,     0,
       0,  1558,  1381,     0,     0,     0,     0,  1560,     0,     0,
    1562,  1090,  1091,     0,     0,     0,     0,     0,     0,     0,
    1569,  1776,  1777,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   676,   676,   680,
     680,   680,   684,   684,   676,     0,     0,  1591,     0,   684,
     696,     0,   698,  1597,   700,   676,     0,   680,   680,   680,
     680,  1604,     0,     0,     0,     0,     0,  1611,     0,     0,
       0,     0,     0,     0,   680,   680,   680,   680,   680,   680,
       0,     0,   680,     0,     0,     0,     0,     0,     0,     0,
     680,     0,     0,     0,     0,  1634,   680,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   680,     0,   680,   680,   680,   680,   765,
     680,   680,   680,     0,  1661,   680,   680,   680,     0,     0,
    1666,     0,   680,     0,     0,  1185,     0,     0,     0,   680,
     680,     0,     0,   789,   680,     0,     0,     0,     0,     0,
    1684,     0,     0,   680,   680,     0,   680,   809,  1211,     0,
     813,   676,   676,   676,     0,     0,     0,  1778,   680,   680,
     680,   680,     0,     0,     0,     0,  1092,     0,     0,   832,
       0,   680,   836,   680,     0,   680,     0,     0,     0,     0,
     676,     0,     0,     0,   680,     0,     0,     0,     0,     0,
       0,     0,  1249,  1781,  1782,  1783,  1784,  1785,  1786,  1787,
    1788,  1789,  1790,  1791,  1792,  1793,  1794,  1795,  1796,  1797,
    1798,  1799,  1800,  1801,  1802,  1803,  1804,  1805,  1806,  1807,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  1093,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1094,  1095,  1096,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  1813,     0,     0,  1097,     0,     0,     0,
    1819,     0,  1098,  1099,  1100,  1101,  1102,  1103,  1104,  1105,
    1106,  1107,  1108,  1109,  1110,  1111,  1112,  1113,     0,  1114,
    1115,  1116,  1117,  1118,     0,  1119,  1120,  1121,  1122,  1123,
    1124,  1125,  1126,  1127,  1128,  1129,  1130,  1131,  1132,  1133,
    1134,     0,     0,     0,  1135,  1136,  1137,  1138,  1139,  1140,
    1141,  1142,  1143,  1144,  1145,  1146,  1147,  1148,  1149,  1150,
    1151,  1152,  1153,  1154,  1155,  1156,  1157,  1158,  1159,  1160,
    1161,  1162,  1163,  1164,  1165,  1166,  1167,  1168,  1169,  1170,
    1171,  1172,  1173,  1174,  1175,  1176,  1177,  1178,  1179,  1180,
       0,   682,   683,  1322,     0,     0,     0,     0,     0,     0,
    1181,     0,     0,     0,     0,     0,     0,     0,   703,   704,
     705,   706,     0,     0,  1341,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   720,   721,   722,   723,   724,
     725,     0,     0,   728,     0,     0,     0,     0,     0,     0,
       0,   736,     0,     0,     0,     0,     0,   742,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  1378,     0,
       0,     0,     0,     0,   759,     0,   761,   762,   763,   764,
       0,   766,   767,   768,     0,     0,   771,   772,   773,     0,
       0,     0,  1398,   778,     0,     0,     0,     0,     0,     0,
     785,   786,     0,     0,     0,   790,     0,     0,     0,     0,
       0,     0,     0,     0,   805,   806,     0,   808,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   821,
     822,   823,   824,     0,     0,     0,     0,     0,     0,     0,
    1439,     0,   835,     0,   837,     0,   839,     0,     0,     0,
       0,     0,     0,     0,     0,   848,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  1499,     0,     0,     0,     0,     0,     0,  1507,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,     0,    61,     0,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,     0,   173,   174,     0,   175,     0,     0,   176,     0,
     177,   178,   179,   180,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
     197,   198,   199,   200,   201,   202,   203,   204,   205,     0,
       0,     0,   694,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   707,   708,   709,   710,   711,
     712,   713,   714,   715,   716,   717,   718,   719,     0,     0,
       0,     0,     0,     0,   726,   727,     0,   729,   730,   731,
     732,   733,   734,   735,   206,   737,   738,   739,   740,   741,
       0,   743,   744,   745,   746,   747,   748,   749,   750,   751,
     752,   753,   754,   755,   756,   757,   758,     0,   760,     0,
       0,     0,     0,     0,     0,     0,     0,   769,   770,     0,
       0,     0,   774,   775,   776,   777,     0,   779,   780,     0,
       0,   783,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   794,   795,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   817,   818,
     819,   820,     0,     0,     0,     0,   825,   826,   827,   828,
     829,   830,   831,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   843,     0,   845,     0,     0,     0,   849,
     850,   851,   852,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   207,     0,     0,   208,   209,     0,     0,
       0,     0,     0,     0,     0,   210,   211,   212,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   213,   214,   215,
     216,   217,   218,  1696,  1697,  1698,  1699,  1700,  1701,  1702,
    1703,  1704,  1705,  1706,  1707,  1708,  1709,  1710,  1711,  1712,
    1713,  1714,  1715,  1716,  1717,  1718,  1719,  1720,  1721,  1722,
    1723,  1724,  1725,  1726,  1727,  1728,  1729,  1730,  1731,  1732,
    1733,  1734,  1735,  1736,  1737,  1738,  1739,  1740,  1741,  1742,
    1743,  1744,  1745,  1746,  1747,  1748,  1749,  1750,  1751,  1752,
    1753,  1754,  1755,  1756,  1757,  1758,  1759,  1760,  1761,  1762,
    1763,  1764,  1765,  1766,  1767,  1768,  1769,  1770,  1771,  1772,
    1773
};

#define yypact_value_is_default(yystate) \
  ((yystate) == (-1571))

#define yytable_value_is_error(yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
       1,   657,   455,   798,   669,  1022,   662,  1054,   664,   629,
     684,   810,    13,   784,   995,   689,   893,   980,     5,   227,
    1308,   474,   696,   697,     5,  1182,  1596,  1422,  1598,  1306,
    1330,   452,   453,   454,   467,   691,  1494,   213,   230,   273,
     473,  1498,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   484,   257,   588,  1331,   229,   313,  1421,  1080,   640,
       0,   472,   643,  1085,   238,  1429,     6,   478,     6,   868,
     327,   425,   426,   427,   428,   429,   430,   431,   613,     4,
     615,     3,   480,     7,   482,   262,   291,   485,   254,   255,
     256,   765,  1549,   570,   571,   214,   330,   568,   569,   243,
     570,   571,   209,  1561,   568,   569,   209,   781,   782,   208,
     987,   570,   571,   787,   219,   789,   258,   259,   953,   954,
    1366,  1367,   575,   376,   304,     3,     5,   223,   323,   241,
     236,   242,   217,  1010,   307,   809,  1436,   329,  1595,   325,
     796,   217,   278,   942,   320,   239,   230,   230,   234,   249,
     229,   330,   293,   227,   424,   221,   238,   590,   591,   592,
     321,   251,   213,   616,   617,   580,   564,   565,   320,   567,
     240,   306,   570,   571,   222,   210,   574,   833,   576,   260,
     224,   212,   981,   250,   272,   859,   619,   301,   301,   287,
     228,   269,   447,   322,   448,   323,   218,   996,   237,   241,
     621,   622,   211,   246,   308,   270,   235,   244,   225,   453,
     215,   252,   215,   437,  1013,   230,   271,   873,  1495,   294,
     876,   285,   878,   276,   880,   265,   299,   883,   305,   452,
     323,   887,   279,   296,   245,   295,   333,   226,   216,   230,
    1011,   449,   438,   899,   918,  1815,  1641,   277,   372,   299,
     286,   907,   266,   909,   377,  1555,   912,  1028,   257,   449,
     292,   352,   288,  1280,   297,   327,   922,   451,   327,   925,
     247,   440,   216,   263,   220,  1432,   322,   455,   450,   289,
     439,   309,   264,   375,   249,   273,   248,   961,   442,   945,
     324,   453,   267,   443,   310,   231,   454,   318,   232,   326,
     268,   319,   958,   433,   434,   435,   436,   437,   438,   439,
     440,   441,   442,   443,   444,   445,   446,   447,   448,   449,
     450,   329,   273,   233,   462,   311,   275,   312,   328,   985,
    1618,  1005,   327,   274,   314,  1009,   992,   212,   433,     2,
    1019,  1015,   998,   326,  1000,  1658,  1678,  1695,   335,  1023,
    1024,  1394,  1476,   334,  1483,  1280,  1279,  1278,  1346,  1612,
    1538,  1017,  1671,   799,  1038,  1289,  1539,  1358,  1590,  1608,
    1026,   895,   650,   700,  1628,  1031,   867,  1033,  1608,  1228,
    1456,  1515,   966,  1039,  1370,  1041,    -1,  1043,    -1,    -1,
      -1,  1268,    -1,  1049,    -1,  1051,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1060,    -1,  1062,    -1,  1064,    -1,
    1066,    -1,  1068,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   426,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1428,  1411,    -1,
      -1,   581,    -1,    -1,   584,    -1,    -1,    -1,  1078,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1265,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1523,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1283,    -1,    -1,    -1,  1287,    -1,
      -1,    -1,    -1,  1264,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1192,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1187,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1198,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1206,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1218,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1232,    -1,    -1,  1235,
      -1,    -1,    -1,    -1,    -1,    -1,  1385,    -1,  1244,  1245,
    1389,    -1,  1266,  1610,    -1,  1251,    -1,    -1,    -1,  1273,
    1256,    -1,    -1,  1259,  1403,  1261,    -1,  1281,    -1,  1408,
      -1,    -1,    -1,    -1,  1270,    -1,    -1,    -1,    -1,    -1,
    1276,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1285,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1294,    -1,
    1296,    -1,    -1,    -1,    -1,    -1,    -1,  1303,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1311,    -1,  1313,  1323,    -1,
      -1,    -1,    -1,    -1,  1463,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1344,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1360,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1511,    -1,    -1,    -1,  1390,  1373,  1517,    -1,
      -1,    -1,  1521,    -1,    -1,    -1,  1525,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1392,    -1,    -1,  1395,
      -1,    -1,    -1,    -1,    -1,  1401,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1412,    -1,  1414,    -1,
      -1,    -1,  1418,    -1,    -1,    -1,  1565,    -1,    -1,  1425,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1585,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  1459,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1616,  1474,    -1,
    1619,    -1,  1478,    -1,    -1,  1481,    -1,    -1,    -1,  1485,
      -1,    -1,    -1,    -1,  1490,    -1,    -1,    -1,    -1,    -1,
    1639,    -1,    -1,    -1,    -1,    -1,    -1,  1503,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1659,    -1,    -1,    -1,    -1,    -1,    -1,  1662,    -1,    -1,
      -1,  1527,  1325,    -1,    -1,    -1,    -1,  1533,    -1,    -1,
    1536,    55,    56,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1546,  1690,  1691,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   466,   467,   468,
     469,   470,   471,   472,   473,    -1,    -1,  1573,    -1,   478,
     479,    -1,   481,  1579,   483,   484,    -1,   486,   487,   488,
     489,  1587,    -1,    -1,    -1,    -1,    -1,  1593,    -1,    -1,
      -1,    -1,    -1,    -1,   503,   504,   505,   506,   507,   508,
      -1,    -1,   511,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     519,    -1,    -1,    -1,    -1,  1621,   525,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   542,    -1,   544,   545,   546,   547,   548,
     549,   550,   551,    -1,  1650,   554,   555,   556,    -1,    -1,
    1656,    -1,   561,    -1,    -1,  1006,    -1,    -1,    -1,   568,
     569,    -1,    -1,   572,   573,    -1,    -1,    -1,    -1,    -1,
    1676,    -1,    -1,   582,   583,    -1,   585,   586,  1029,    -1,
     589,   590,   591,   592,    -1,    -1,    -1,  1693,   597,   598,
     599,   600,    -1,    -1,    -1,    -1,   220,    -1,    -1,   608,
      -1,   610,   611,   612,    -1,   614,    -1,    -1,    -1,    -1,
     619,    -1,    -1,    -1,   623,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1073,   541,   542,   543,   544,   545,   546,   547,
     548,   549,   550,   551,   552,   553,   554,   555,   556,   557,
     558,   559,   560,   561,   562,   563,   564,   565,   566,   567,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   290,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   315,   316,   317,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  1809,    -1,    -1,   330,    -1,    -1,    -1,
    1816,    -1,   336,   337,   338,   339,   340,   341,   342,   343,
     344,   345,   346,   347,   348,   349,   350,   351,    -1,   353,
     354,   355,   356,   357,    -1,   359,   360,   361,   362,   363,
     364,   365,   366,   367,   368,   369,   370,   371,   372,   373,
     374,    -1,    -1,    -1,   378,   379,   380,   381,   382,   383,
     384,   385,   386,   387,   388,   389,   390,   391,   392,   393,
     394,   395,   396,   397,   398,   399,   400,   401,   402,   403,
     404,   405,   406,   407,   408,   409,   410,   411,   412,   413,
     414,   415,   416,   417,   418,   419,   420,   421,   422,   423,
      -1,   469,   470,  1254,    -1,    -1,    -1,    -1,    -1,    -1,
     434,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   486,   487,
     488,   489,    -1,    -1,  1275,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   503,   504,   505,   506,   507,
     508,    -1,    -1,   511,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   519,    -1,    -1,    -1,    -1,    -1,   525,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1319,    -1,
      -1,    -1,    -1,    -1,   542,    -1,   544,   545,   546,   547,
      -1,   549,   550,   551,    -1,    -1,   554,   555,   556,    -1,
      -1,    -1,  1343,   561,    -1,    -1,    -1,    -1,    -1,    -1,
     568,   569,    -1,    -1,    -1,   573,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   582,   583,    -1,   585,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   597,
     598,   599,   600,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1391,    -1,   610,    -1,   612,    -1,   614,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   623,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1473,    -1,    -1,    -1,    -1,    -1,    -1,  1480,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    -1,    57,    -1,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     169,    -1,   171,   172,    -1,   174,    -1,    -1,   177,    -1,
     179,   180,   181,   182,   183,   184,   185,   186,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,   197,   198,
     199,   200,   201,   202,   203,   204,   205,   206,   207,    -1,
      -1,    -1,   477,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   490,   491,   492,   493,   494,
     495,   496,   497,   498,   499,   500,   501,   502,    -1,    -1,
      -1,    -1,    -1,    -1,   509,   510,    -1,   512,   513,   514,
     515,   516,   517,   518,   253,   520,   521,   522,   523,   524,
      -1,   526,   527,   528,   529,   530,   531,   532,   533,   534,
     535,   536,   537,   538,   539,   540,   541,    -1,   543,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   552,   553,    -1,
      -1,    -1,   557,   558,   559,   560,    -1,   562,   563,    -1,
      -1,   566,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   577,   578,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   593,   594,
     595,   596,    -1,    -1,    -1,    -1,   601,   602,   603,   604,
     605,   606,   607,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   618,    -1,   620,    -1,    -1,    -1,   624,
     625,   626,   627,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   432,    -1,    -1,   435,   436,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   444,   445,   446,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   456,   457,   458,
     459,   460,   461,   463,   464,   465,   466,   467,   468,   469,
     470,   471,   472,   473,   474,   475,   476,   477,   478,   479,
     480,   481,   482,   483,   484,   485,   486,   487,   488,   489,
     490,   491,   492,   493,   494,   495,   496,   497,   498,   499,
     500,   501,   502,   503,   504,   505,   506,   507,   508,   509,
     510,   511,   512,   513,   514,   515,   516,   517,   518,   519,
     520,   521,   522,   523,   524,   525,   526,   527,   528,   529,
     530,   531,   532,   533,   534,   535,   536,   537,   538,   539,
     540
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,     6,   573,   574,   575,     4,  1379,     0,   574,     3,
     577,   576,   578,     7,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    57,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   171,   172,   174,   177,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,   207,   253,   432,   435,   436,
     444,   445,   446,   456,   457,   458,   459,   460,   461,   579,
     590,   605,   607,   613,   624,   626,   629,   635,   656,   660,
     662,   664,   667,   669,   671,   673,   675,   677,   679,   681,
     683,   686,   692,   697,   699,   701,   711,   719,   721,   727,
     729,   731,   733,   735,   737,   744,   753,   758,   760,   762,
     764,   766,   768,   770,   772,   774,   776,   778,   780,   782,
     784,   786,   788,   790,   792,   794,   796,   798,   800,   802,
     804,   806,   808,   810,   812,   814,   816,   818,   820,   822,
     824,   826,   828,   830,   832,   834,   845,   847,   849,   851,
     853,   855,   857,   859,   861,   863,   865,   867,   869,   871,
     873,   875,   877,   879,   881,   883,   885,   887,   889,   891,
     893,   895,   897,   899,   901,   903,   905,   907,   909,   911,
     913,   915,   917,   919,   921,   923,   925,   927,   929,   931,
     933,   935,   937,   939,   941,   943,   945,   947,   949,   951,
     953,   956,   959,   961,   963,   965,   967,   969,   971,   973,
     975,   982,   985,   987,   989,  1004,  1007,  1012,  1018,  1020,
    1022,  1024,  1026,  1028,  1030,  1032,  1034,  1036,  1038,  1040,
    1042,  1044,  1046,  1048,  1050,  1052,  1054,  1056,  1058,  1061,
    1063,  1066,  1073,  1076,  1079,  1083,  1084,  1087,  1089,  1092,
    1094,  1096,  1100,  1104,  1106,  1107,  1113,  1115,  1119,  1121,
    1124,  1131,  1139,  1142,  1144,  1146,  1151,  1160,  1162,  1164,
    1165,  1167,  1255,  1262,  1271,  1379,   213,   320,  1287,   630,
     983,  1114,   661,   687,   684,   668,   670,   693,   663,   672,
     698,   674,   700,   722,   702,   676,   712,   720,   678,   608,
     665,   627,  1080,  1101,   957,   657,   591,   732,   734,   728,
     636,   614,   738,   745,   736,   730,  1095,  1161,   815,   817,
     831,  1145,  1163,   962,  1108,   874,   835,   876,   988,  1116,
    1120,   625,  1152,  1147,   954,   777,   789,   848,   850,   852,
     854,   858,   860,   862,   878,   880,   882,   864,   884,   886,
     888,   890,   892,   791,   793,   795,   797,   799,   779,   894,
     896,   801,   898,   900,   902,   904,   906,   908,   910,   912,
     914,   916,   918,   868,   920,   781,   922,   866,   846,   924,
     872,   870,   926,   928,   930,   932,   934,   936,   938,   940,
     942,   944,   809,   946,   783,   775,   811,   813,  1008,   765,
     785,   787,  1088,   948,   803,   805,   807,   950,   952,  1077,
    1062,   769,   960,   964,   763,   761,  1122,   759,   771,   773,
    1005,  1074,  1132,   767,  1125,   990,  1067,  1093,  1105,  1019,
     754,   682,   819,   821,   680,   825,  1013,  1097,  1059,  1021,
    1025,  1027,  1023,  1029,  1031,  1033,  1035,  1037,  1039,  1041,
    1043,  1045,  1047,  1049,  1051,  1053,  1055,  1057,   974,  1085,
     823,   986,   827,   976,   829,  1090,  1143,  1140,  1166,  1168,
     856,  1263,  1256,   833,   966,   970,   972,   968,  1379,   606,
     262,  1345,   214,  1318,  1379,  1381,   243,  1202,  1210,  1202,
    1202,  1202,  1202,  1202,  1202,  1202,  1202,  1202,  1202,  1202,
    1202,  1202,  1202,  1202,  1202,  1202,  1202,   209,  1290,  1290,
    1290,  1290,   209,  1324,   208,  1177,  1323,  1177,  1177,  1177,
    1177,  1177,  1177,  1177,  1177,  1177,  1188,  1216,  1324,  1216,
    1188,  1213,  1213,  1213,  1188,  1219,  1219,  1216,  1324,  1188,
    1214,   219,  1191,  1333,  1214,  1219,  1188,  1191,  1188,  1191,
    1188,  1216,  1191,  1213,  1213,  1213,  1213,  1214,  1214,  1214,
    1214,  1214,  1214,  1214,  1214,  1214,  1214,  1214,  1214,  1214,
    1213,  1213,  1213,  1213,  1213,  1213,  1214,  1214,  1213,  1214,
    1214,  1214,  1214,  1214,  1214,  1214,  1213,  1214,  1214,  1214,
    1214,  1214,  1213,  1214,  1214,  1214,  1214,  1214,  1214,  1214,
    1214,  1214,  1214,  1214,  1214,  1214,  1214,  1214,  1214,  1213,
    1214,  1213,  1213,  1213,  1213,  1188,  1213,  1213,  1213,  1214,
    1214,  1213,  1213,  1213,  1214,  1214,  1214,  1214,  1213,  1214,
    1214,  1191,  1191,  1214,  1191,  1213,  1213,  1191,  1191,  1188,
    1213,  1191,  1324,  1191,  1214,  1214,   229,   238,  1015,  1016,
    1017,  1236,  1276,  1177,  1202,  1213,  1213,  1202,  1213,  1188,
     376,  1320,  1320,  1188,  1216,  1216,  1216,  1214,  1214,  1214,
    1214,  1213,  1213,  1213,  1213,  1214,  1214,  1214,  1214,  1214,
    1214,  1214,  1188,   304,  1308,  1213,  1188,  1213,  1320,  1213,
    1320,  1324,  1324,  1214,  1216,  1214,  1290,  1290,  1213,  1214,
    1214,  1214,  1214,  1318,     3,  1378,   631,     5,  1380,   984,
     425,   426,   427,   428,   429,   430,   431,  1203,   223,  1315,
    1315,   323,   666,   236,  1295,  1315,   241,  1296,   242,  1297,
     217,  1332,  1295,   307,   709,  1310,   325,   217,  1294,  1378,
     628,  1081,  1102,   958,  1378,   658,  1378,   592,  1178,   278,
    1347,   725,  1310,   637,   615,   739,   746,   239,  1337,   230,
    1286,  1189,   230,  1335,  1286,  1109,  1286,  1378,   836,  1192,
    1286,  1286,   234,   622,  1282,   249,  1298,  1298,  1286,  1286,
    1286,  1335,  1286,  1075,   329,  1129,  1286,   330,  1123,   991,
    1068,  1378,  1237,  1276,  1236,   293,  1351,  1286,  1380,  1098,
    1060,  1378,  1086,   977,  1091,  1141,  1264,  1257,   227,  1285,
    1346,   980,   981,  1286,  1323,  1380,   688,  1378,   694,  1378,
    1378,  1378,   723,   703,  1378,   424,   704,   710,   717,  1378,
     609,   221,  1317,   321,  1078,   251,  1300,   955,  1285,  1287,
     320,   585,   240,  1338,  1378,   726,   306,  1362,   222,  1331,
     260,  1344,  1211,  1378,  1378,  1217,   210,  1224,  1378,  1220,
    1110,  1215,  1286,   224,  1316,  1117,  1378,   212,  1291,   623,
    1378,  1153,  1148,  1009,  1006,  1071,   250,  1299,  1130,   287,
    1305,   272,  1304,   269,  1303,  1380,  1378,   755,  1014,   301,
    1307,   301,  1357,   228,  1328,  1291,  1291,   322,  1138,   447,
    1369,   448,  1370,  1378,   632,  1286,  1204,  1381,   323,   691,
     218,  1330,   237,  1336,   241,  1339,   308,  1311,   246,   718,
    1283,  1330,  1380,   244,  1288,  1378,  1103,  1285,   659,   586,
     593,  1378,  1179,  1311,  1380,   638,  1378,   616,  1378,   740,
      55,    56,   220,   290,   315,   316,   317,   330,   336,   337,
     338,   339,   340,   341,   342,   343,   344,   345,   346,   347,
     348,   349,   350,   351,   353,   354,   355,   356,   357,   359,
     360,   361,   362,   363,   364,   365,   366,   367,   368,   369,
     370,   371,   372,   373,   374,   378,   379,   380,   381,   382,
     383,   384,   385,   386,   387,   388,   389,   390,   391,   392,
     393,   394,   395,   396,   397,   398,   399,   400,   401,   402,
     403,   404,   405,   406,   407,   408,   409,   410,   411,   412,
     413,   414,   415,   416,   417,   418,   419,   420,   421,   422,
     423,   434,   747,  1212,  1286,  1379,  1286,   449,  1111,  1285,
    1322,  1335,   837,  1380,  1193,  1286,  1378,   622,   211,  1154,
    1281,  1149,  1281,  1010,  1286,  1286,   270,  1072,  1277,  1378,
    1335,  1379,  1126,  1378,   992,  1378,  1069,  1238,   294,  1352,
    1286,  1378,  1099,  1378,  1378,   978,  1133,  1378,  1265,  1378,
    1258,  1328,   235,  1334,   689,   225,  1363,  1378,   724,  1378,
    1378,   713,  1378,  1378,   257,   291,  1284,  1343,   610,  1379,
    1082,   252,  1301,  1318,   587,  1211,   453,  1374,  1211,   215,
    1327,   215,  1292,  1344,  1218,  1225,  1221,  1378,  1112,  1286,
     437,  1249,  1250,  1118,  1378,   570,   571,  1156,  1157,  1155,
    1150,  1011,  1378,   271,  1280,   285,  1365,   276,  1312,  1064,
     265,  1227,  1378,   756,   299,  1306,   305,  1309,   254,   255,
     256,  1134,  1370,   452,  1372,  1378,  1205,   323,   685,  1378,
     695,   279,  1348,   296,  1354,  1378,  1378,   333,   611,   245,
    1289,  1378,  1379,   594,  1378,  1180,   639,  1378,   617,  1378,
     741,   748,  1335,  1380,  1222,  1286,  1285,   838,  1378,  1194,
    1286,  1379,  1378,   568,   569,  1158,  1159,  1156,  1154,  1149,
    1286,  1380,  1378,  1127,  1380,   993,  1065,  1070,  1277,  1239,
     295,  1353,  1378,  1378,   979,  1135,  1136,  1137,  1266,  1378,
    1259,  1327,  1363,   226,  1364,  1378,  1378,   714,  1379,   588,
    1310,  1324,   313,   327,   633,   216,  1314,  1314,  1327,  1226,
    1223,   570,   571,   841,   842,   438,  1251,  1252,  1379,  1378,
    1158,   286,  1366,   277,  1313,   372,  1172,  1280,   266,  1228,
    1378,   757,   299,  1356,   257,  1302,  1302,  1302,   449,  1376,
    1376,  1206,   690,  1378,   696,   297,  1355,   595,   589,  1181,
     327,   327,   640,  1380,   618,   742,   749,  1380,  1286,  1379,
    1378,   839,   841,  1378,  1195,  1378,  1128,  1380,   994,  1380,
    1240,  1330,  1378,  1378,  1378,  1267,  1260,  1333,  1364,   292,
    1293,  1378,   715,   377,  1361,  1311,  1333,  1344,   309,  1170,
     352,  1176,  1314,   568,   569,   843,   844,  1190,   288,  1349,
     570,   571,   999,  1000,  1229,   451,  1371,  1371,  1207,  1378,
     247,  1340,  1380,   596,  1182,   641,   619,   743,   750,  1379,
    1378,   840,   843,   440,  1196,  1278,  1378,  1379,  1378,   995,
     999,   263,  1241,  1247,  1378,  1268,  1261,   216,  1359,  1378,
     716,   322,  1360,  1224,  1327,   220,  1319,   455,  1375,  1176,
     289,  1350,  1378,   439,  1253,  1254,   273,   330,  1001,  1002,
    1003,  1279,   334,  1380,  1230,  1372,   450,  1377,  1380,  1208,
     375,   705,  1380,   597,  1328,   642,  1380,   620,  1378,   751,
    1378,  1197,  1378,  1172,  1279,   264,  1242,  1248,  1269,  1378,
    1176,   324,   706,   249,  1342,  1183,  1314,   326,   612,   248,
    1341,  1224,   996,   335,  1380,   442,  1367,   453,  1321,  1209,
     707,  1378,   598,   267,  1329,   643,   621,  1378,   752,  1198,
     329,  1173,  1380,  1243,  1378,  1270,   310,  1171,   708,  1283,
     584,  1378,  1184,  1176,  1375,  1375,   231,  1272,   997,   443,
    1368,   454,  1373,  1284,  1343,   599,  1281,  1170,   644,  1380,
    1199,  1363,  1380,  1244,  1378,   318,   582,  1185,  1319,   232,
    1273,   998,  1231,   600,   311,  1174,   645,  1380,  1200,  1364,
     268,  1245,  1275,   319,   583,  1186,   273,   650,   651,   233,
    1274,  1378,  1232,   601,   312,  1175,  1378,   646,   650,  1380,
    1201,  1246,  1276,   327,   580,  1187,   275,   652,   653,  1233,
    1233,   602,   314,  1169,  1378,   647,   652,   462,   328,   581,
     258,   259,  1358,   274,   654,   655,   463,   464,   465,   466,
     467,   468,   469,   470,   471,   472,   473,   474,   475,   476,
     477,   478,   479,   480,   481,   482,   483,   484,   485,   486,
     487,   488,   489,   490,   491,   492,   493,   494,   495,   496,
     497,   498,   499,   500,   501,   502,   503,   504,   505,   506,
     507,   508,   509,   510,   511,   512,   513,   514,   515,   516,
     517,   518,   519,   520,   521,   522,   523,   524,   525,   526,
     527,   528,   529,   530,   531,   532,   533,   534,   535,   536,
     537,   538,   539,   540,  1234,   603,  1380,  1380,  1378,   648,
     654,   541,   542,   543,   544,   545,   546,   547,   548,   549,
     550,   551,   552,   553,   554,   555,   556,   557,   558,   559,
     560,   561,   562,   563,   564,   565,   566,   567,  1235,   212,
    1325,   326,   634,  1378,   604,   649,   433,  1326,  1375,  1378
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (data, lexer, YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* This macro is provided for backward compatibility. */

#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval, lexer)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value, data, lexer); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, const BisonParserDataRef data, const TreeFlexLexerRef lexer)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep, data, lexer)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    const BisonParserDataRef data;
    const TreeFlexLexerRef lexer;
#endif
{
  if (!yyvaluep)
    return;
  YYUSE (data);
  YYUSE (lexer);
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, const BisonParserDataRef data, const TreeFlexLexerRef lexer)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep, data, lexer)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    const BisonParserDataRef data;
    const TreeFlexLexerRef lexer;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep, data, lexer);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule, const BisonParserDataRef data, const TreeFlexLexerRef lexer)
#else
static void
yy_reduce_print (yyvsp, yyrule, data, lexer)
    YYSTYPE *yyvsp;
    int yyrule;
    const BisonParserDataRef data;
    const TreeFlexLexerRef lexer;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       , data, lexer);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule, data, lexer); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  YYSIZE_T yysize1;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = 0;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                yysize1 = yysize + yytnamerr (0, yytname[yyx]);
                if (! (yysize <= yysize1
                       && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                  return 2;
                yysize = yysize1;
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  yysize1 = yysize + yystrlen (yyformat);
  if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
    return 2;
  yysize = yysize1;

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, const BisonParserDataRef data, const TreeFlexLexerRef lexer)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, data, lexer)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    const BisonParserDataRef data;
    const TreeFlexLexerRef lexer;
#endif
{
  YYUSE (yyvaluep);
  YYUSE (data);
  YYUSE (lexer);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (const BisonParserDataRef data, const TreeFlexLexerRef lexer);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (const BisonParserDataRef data, const TreeFlexLexerRef lexer)
#else
int
yyparse (data, lexer)
    const BisonParserDataRef data;
    const TreeFlexLexerRef lexer;
#endif
#endif
{
/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 6:

    {data->gcc_version = data->curr_string;}
    break;

  case 7:

    {
                GccWrapper::CheckGccCompatibleVersion(data->gcc_version, data->curr_string);
                if(data->final_TM)
                   data->final_TM->merge_tree_managers(data->current_TM);
                else if(data->current_TM)
                   data->final_TM = data->current_TM;
                data->current_TM = tree_managerRef(new tree_manager(data->Param));
             }
    break;

  case 8:

    {data->curr_NODE_ID=data->id=(yyvsp[(1) - (1)].value);}
    break;

  case 9:

    {
                      data->current_TM->AddTreeNode(data->id, data->curr_tree_nodeRef);
                      data->curr_tree_nodeRef=tree_nodeRef();
                  }
    break;

  case 216:

    {NSV(function_decl, static_flag, true)}
    break;

  case 218:

    {NSV(function_decl, reverse_restrict_flag, true)}
    break;

  case 220:

    {NSV(function_decl, undefined_flag, true)}
    break;

  case 222:

    {NSV(function_decl, builtin_flag, true)}
    break;

  case 224:

    {ADDV(function_decl,AddArg)}
    break;

  case 226:

    {NSV(function_decl, operator_flag, true)}
    break;

  case 229:

    {ADD_OP_NAME(data->curr_string)}
    break;

  case 231:

    {NS(function_decl,tmpl_parms)}
    break;

  case 232:

    {NS(function_decl,tmpl_args)}
    break;

  case 233:

    {CTN(function_decl) do_fun_decl(data->implement_node, data->curr_NODE_ID_BIS, data->curr_NODE_ID, data, data->current_TM);}
    break;

  case 234:

    {;}
    break;

  case 235:

    {;}
    break;

  case 236:

    {;}
    break;

  case 237:

    {;}
    break;

  case 238:

    {OPT((yyvsp[(11) - (11)].pred), {NSV(function_decl,fixd,data->curr_number); NSV(function_decl,fixd_flag,true)})}
    break;

  case 239:

    {OPT((yyvsp[(13) - (13)].pred), {NSV(function_decl,virt,data->curr_number); NSV(function_decl,virt_flag,true)})}
    break;

  case 240:

    {OPT((yyvsp[(15) - (15)].pred), NS(function_decl,fn))}
    break;

  case 241:

    {;}
    break;

  case 242:

    {;}
    break;

  case 243:

    {;}
    break;

  case 244:

    {;}
    break;

  case 245:

    {;}
    break;

  case 246:

    {OPT((yyvsp[(27) - (27)].pred), NS(function_decl,body))}
    break;

  case 247:

    {OPT((yyvsp[(29) - (29)].pred), NS(function_decl,inline_body))}
    break;

  case 248:

    {CTN_ID(data->curr_string)}
    break;

  case 250:

    {CTN_ID(true)}
    break;

  case 251:

    {CTN(function_type)}
    break;

  case 252:

    {NS(function_type, retn)}
    break;

  case 253:

    {OPT((yyvsp[(6) - (6)].pred), NS(function_type, prms))}
    break;

  case 256:

    {

                    NSV(function_type, varargs_flag, true);
                 }
    break;

  case 258:

    {NSV(parm_decl,register_flag, true)}
    break;

  case 259:

    {CTN(parm_decl)}
    break;

  case 260:

    {}
    break;

  case 261:

    {OPT((yyvsp[(5) - (5)].pred), NS(parm_decl, argt))}
    break;

  case 262:

    {OPT((yyvsp[(7) - (7)].pred), NS(parm_decl, size))}
    break;

  case 263:

    {NSV(parm_decl,algn, data->curr_unumber)}
    break;

  case 264:

    {OPT((yyvsp[(11) - (11)].pred), NSV(decl_node, artificial_flag, true))}
    break;

  case 265:

    {NSV(parm_decl, used, data->curr_number)}
    break;

  case 266:

    {}
    break;

  case 267:

    {OPT((yyvsp[(17) - (17)].pred), NS(parm_decl, smt_ann))}
    break;

  case 269:

    {ADDV(gimple_bind,add_vars)}
    break;

  case 271:

    {CTN(gimple_bind)}
    break;

  case 272:

    {NS(gimple_bind,body)}
    break;

  case 273:

    {
                     CTN(integer_cst)
                  }
    break;

  case 274:

    {
                     NS(integer_cst,type)
                  }
    break;

  case 275:

    {
                    NSV(integer_cst, value, data->curr_long_long_number);
                  }
    break;

  case 276:

    {CTN(tree_list)}
    break;

  case 277:

    {OPT((yyvsp[(3) - (3)].pred), NS(tree_list, purp))}
    break;

  case 278:

    {OPT((yyvsp[(5) - (5)].pred), NS(tree_list, valu))}
    break;

  case 279:

    {OPT((yyvsp[(7) - (7)].pred), NS(tree_list, chan))}
    break;

  case 281:

    {NSV(var_decl,static_static_flag, true)}
    break;

  case 282:

    {NSV(var_decl,static_flag, true)}
    break;

  case 283:

    {NSV(var_decl,extern_flag, true)}
    break;

  case 284:

    {NSV(var_decl,extern_flag, true)NSV(var_decl,static_flag, true)}
    break;

  case 286:

    {NSV(var_decl,register_flag, true)}
    break;

  case 287:

    {CTN(var_decl)}
    break;

  case 288:

    {}
    break;

  case 289:

    {OPT((yyvsp[(5) - (5)].pred), NSV(var_decl,use_tmpl,data->curr_number))}
    break;

  case 290:

    {}
    break;

  case 291:

    {}
    break;

  case 292:

    {OPT((yyvsp[(11) - (11)].pred), NS(var_decl, init))}
    break;

  case 293:

    {OPT((yyvsp[(13) - (13)].pred), NS(var_decl, size))}
    break;

  case 294:

    {NSV(var_decl,algn, data->curr_unumber)}
    break;

  case 295:

    {OPT((yyvsp[(17) - (17)].pred), NSV(var_decl, packed_flag, true))}
    break;

  case 296:

    {NSV(var_decl, used, data->curr_number)}
    break;

  case 297:

    {;}
    break;

  case 298:

    {;}
    break;

  case 299:

    {;}
    break;

  case 300:

    {}
    break;

  case 301:

    {OPT((yyvsp[(29) - (29)].pred), NS(var_decl, smt_ann))}
    break;

  case 304:

    {GetPointer<var_decl>(data->curr_tree_nodeRef)->defs.insert(data->current_TM->GetTreeReindex(data->curr_NODE_ID));}
    break;

  case 307:

    {GetPointer<var_decl>(data->curr_tree_nodeRef)->uses.insert(data->current_TM->GetTreeReindex(data->curr_NODE_ID));}
    break;

  case 310:

    {GetPointer<var_decl>(data->curr_tree_nodeRef)->addressings.insert(data->current_TM->GetTreeReindex(data->curr_NODE_ID));}
    break;

  case 311:

    {CTN(string_cst)}
    break;

  case 312:

    {OPT((yyvsp[(3) - (3)].pred), NS(string_cst,type))}
    break;

  case 313:

    {NSV(string_cst, strg, data->curr_string)}
    break;

  case 314:

    {NSV(string_cst, lngt, data->curr_number)}
    break;

  case 315:

    {CTN(void_type)}
    break;

  case 317:

    {CTN(boolean_type)}
    break;

  case 319:

    {CTN(lang_type)}
    break;

  case 322:

    {NSV(complex_type, unsigned_flag, true)}
    break;

  case 323:

    {CTN(complex_type)}
    break;

  case 325:

    {CTN(vector_type)}
    break;

  case 326:

    {NS(vector_type, elts)}
    break;

  case 327:

    {CTN(CharType)}
    break;

  case 329:

    {CTN(offset_type)}
    break;

  case 331:

    {CTN(set_type)}
    break;

  case 333:

    {CTN(qual_union_type)}
    break;

  case 335:

    {CTN(typename_type)}
    break;

  case 337:

    {CTN(template_type_parm)}
    break;

  case 339:

    {CTN(real_type)}
    break;

  case 340:

    {NSV(real_type, prec, data->curr_unumber)}
    break;

  case 342:

    {NSV(integer_type, unsigned_flag, true)}
    break;

  case 343:

    {CTN(integer_type)}
    break;

  case 344:

    {NSV(integer_type, prec, data->curr_unumber)}
    break;

  case 345:

    {OPT((yyvsp[(6) - (6)].pred), NSV(integer_type, str, data->curr_string))}
    break;

  case 346:

    {OPT((yyvsp[(9) - (9)].pred), NS(integer_type,min))}
    break;

  case 347:

    {OPT((yyvsp[(11) - (11)].pred), NS(integer_type,max))}
    break;

  case 349:

    {NSV(enumeral_type, unsigned_flag, true)}
    break;

  case 350:

    {CTN(enumeral_type)}
    break;

  case 351:

    {NSV(enumeral_type, prec, data->curr_unumber)}
    break;

  case 352:

    {OPT((yyvsp[(7) - (7)].pred),NS(enumeral_type,min))}
    break;

  case 353:

    {OPT((yyvsp[(9) - (9)].pred), NS(enumeral_type,max))}
    break;

  case 354:

    {NS(enumeral_type,csts)}
    break;

  case 355:

    {CTN(pointer_type)}
    break;

  case 356:

    {NS(pointer_type, ptd)}
    break;

  case 357:

    {CTN(reference_type)}
    break;

  case 358:

    {NS(reference_type, refd)}
    break;

  case 359:

    {CTN(array_type)}
    break;

  case 360:

    {NS(array_type, elts)}
    break;

  case 361:

    {OPT((yyvsp[(6) - (6)].pred), NS(array_type, domn))}
    break;

  case 363:

    {NSV(record_type, ptrmem_flag, true)}
    break;

  case 365:

    {NSV(record_type, spec_flag, true)}
    break;

  case 367:

    {NSV(record_type, struct_flag, true)}
    break;

  case 369:

    {ADDV(record_type, add_flds)}
    break;

  case 371:

    {ADDV(record_type, add_fncs)}
    break;

  case 373:

    {NS(record_type,tmpl_parms)}
    break;

  case 374:

    {NS(record_type,tmpl_args)}
    break;

  case 375:

    {CTN(record_type)}
    break;

  case 376:

    {OPT((yyvsp[(6) - (6)].pred), NS(record_type, ptd))}
    break;

  case 377:

    {OPT((yyvsp[(8) - (8)].pred), NS(record_type, cls))}
    break;

  case 378:

    {OPT((yyvsp[(10) - (10)].pred), NS(record_type, bfld))}
    break;

  case 379:

    {OPT((yyvsp[(12) - (12)].pred), NS(record_type, vfld))}
    break;

  case 380:

    {OPT((yyvsp[(18) - (18)].pred), NS(record_type, binf))}
    break;

  case 382:

    {ADDV(union_type, add_flds)}
    break;

  case 384:

    {ADDV(union_type, add_fncs)}
    break;

  case 385:

    {CTN(union_type)}
    break;

  case 386:

    {OPT((yyvsp[(7) - (7)].pred), NS(union_type, binf))}
    break;

  case 387:

    {CTN(method_type)}
    break;

  case 388:

    {OPT((yyvsp[(4) - (4)].pred), NS(method_type, retn))}
    break;

  case 389:

    {OPT((yyvsp[(6) - (6)].pred), NS(method_type, prms))}
    break;

  case 390:

    {OPT((yyvsp[(8) - (8)].pred), NS(method_type, clas))}
    break;

  case 392:

    {NS(type_decl,tmpl_parms)}
    break;

  case 393:

    {NS(type_decl,tmpl_args)}
    break;

  case 394:

    {CTN(type_decl)}
    break;

  case 396:

    {CTN(translation_unit_decl)}
    break;

  case 398:

    {CTN(label_decl)}
    break;

  case 400:

    {CTN(const_decl)}
    break;

  case 401:

    {OPT((yyvsp[(4) - (4)].pred), NS(const_decl, cnst))}
    break;

  case 402:

    {CTN(namespace_decl)}
    break;

  case 403:

    {OPT((yyvsp[(4) - (4)].pred), NS(namespace_decl, dcls))}
    break;

  case 404:

    {CTN(result_decl)}
    break;

  case 405:

    {}
    break;

  case 406:

    {OPT((yyvsp[(5) - (5)].pred), NS(result_decl, init))}
    break;

  case 407:

    {NS(result_decl, size)}
    break;

  case 408:

    {NSV(result_decl,algn, data->curr_unumber)}
    break;

  case 409:

    {OPT((yyvsp[(11) - (11)].pred), NSV(result_decl, packed_flag, true))}
    break;

  case 410:

    {OPT((yyvsp[(13) - (13)].pred), NS(result_decl, smt_ann))}
    break;

  case 411:

    {CTN(field_decl)}
    break;

  case 412:

    {}
    break;

  case 413:

    {}
    break;

  case 414:

    {OPT((yyvsp[(7) - (7)].pred), NS(field_decl, init))}
    break;

  case 415:

    {OPT((yyvsp[(9) - (9)].pred), NS(field_decl, size))}
    break;

  case 416:

    {NSV(field_decl,algn, data->curr_unumber)}
    break;

  case 417:

    {OPT((yyvsp[(13) - (13)].pred), NSV(field_decl, packed_flag, true))}
    break;

  case 418:

    {OPT((yyvsp[(15) - (15)].pred), NS(field_decl, bpos))}
    break;

  case 419:

    {OPT((yyvsp[(17) - (17)].pred), NS(field_decl, smt_ann))}
    break;

  case 420:

    {CTN(template_decl)}
    break;

  case 421:

    {OPT((yyvsp[(4) - (4)].pred), NS(template_decl,rslt))}
    break;

  case 422:

    {OPT((yyvsp[(6) - (6)].pred), NS(template_decl,inst))}
    break;

  case 423:

    {OPT((yyvsp[(8) - (8)].pred), NS(template_decl,spcs))}
    break;

  case 424:

    {OPT((yyvsp[(10) - (10)].pred), NS(template_decl,prms))}
    break;

  case 425:

    {CTN(gimple_return)}
    break;

  case 426:

    {OPT((yyvsp[(4) - (4)].pred), NS(gimple_return,op))}
    break;

  case 427:

    {CTN(gimple_goto)}
    break;

  case 428:

    {NS(gimple_goto,op)}
    break;

  case 429:

    {CTN(gimple_label)}
    break;

  case 430:

    {NS(gimple_label,op)}
    break;

  case 431:

    {CTN(unsave_expr)}
    break;

  case 433:

    {CTN(gimple_resx)}
    break;

  case 435:

    {CTN(va_arg_expr)}
    break;

  case 437:

    {CTN(exit_expr)}
    break;

  case 439:

    {CTN(loop_expr)}
    break;

  case 441:

    {CTN(nop_expr)}
    break;

  case 443:

    {CTN(gimple_nop)}
    break;

  case 445:

    {CTN(negate_expr)}
    break;

  case 447:

    {CTN(truth_not_expr)}
    break;

  case 449:

    {CTN(convert_expr)}
    break;

  case 451:

    {CTN(addr_expr)}
    break;

  case 453:

    {CTN(reference_expr)}
    break;

  case 455:

    {CTN(cleanup_point_expr)}
    break;

  case 457:

    {CTN(fix_trunc_expr)}
    break;

  case 459:

    {CTN(fix_ceil_expr)}
    break;

  case 461:

    {CTN(fix_floor_expr)}
    break;

  case 463:

    {CTN(fix_round_expr)}
    break;

  case 465:

    {CTN(float_expr)}
    break;

  case 467:

    {CTN(abs_expr)}
    break;

  case 469:

    {CTN(conj_expr)}
    break;

  case 471:

    {CTN(realpart_expr)}
    break;

  case 473:

    {CTN(imagpart_expr)}
    break;

  case 475:

    {CTN(card_expr)}
    break;

  case 477:

    {CTN(non_lvalue_expr)}
    break;

  case 479:

    {CTN(view_convert_expr)}
    break;

  case 481:

    {CTN(indirect_ref)}
    break;

  case 483:

    {CTN(misaligned_indirect_ref)}
    break;

  case 485:

    {CTN(cast_expr)}
    break;

  case 487:

    {CTN(static_cast_expr)}
    break;

  case 489:

    {CTN(reinterpret_cast_expr)}
    break;

  case 491:

    {CTN(sizeof_expr)}
    break;

  case 493:

    {CTN(throw_expr)}
    break;

  case 495:

    {CTN(arrow_expr)}
    break;

  case 497:

    {CTN(buffer_ref)}
    break;

  case 499:

    {CTN(gimple_predict)}
    break;

  case 501:

    {CTN(gimple_assign)}
    break;

  case 502:

    {;}
    break;

  case 503:

    {NS(gimple_assign,op0)}
    break;

  case 504:

    {NS(gimple_assign,op1)}
    break;

  case 505:

    {;}
    break;

  case 506:

    {;}
    break;

  case 507:

    {OPT((yyvsp[(13) - (13)].pred),GetPointer<gimple_assign>(data->curr_tree_nodeRef)->clobber = true;)}
    break;

  case 510:

    {GetPointer<gimple_assign>(data->curr_tree_nodeRef)->use_set->Add(data->curr_string);}
    break;

  case 511:

    {GetPointer<gimple_assign>(data->curr_tree_nodeRef)->use_set->Add(data->current_TM->GetTreeReindex(data->curr_NODE_ID));}
    break;

  case 514:

    {GetPointer<gimple_assign>(data->curr_tree_nodeRef)->clobbered_set->Add(data->curr_string);}
    break;

  case 515:

    {GetPointer<gimple_assign>(data->curr_tree_nodeRef)->clobbered_set->Add(data->current_TM->GetTreeReindex(data->curr_NODE_ID));}
    break;

  case 516:

    {CTN(gt_expr)}
    break;

  case 518:

    {CTN(reduc_max_expr)}
    break;

  case 520:

    {CTN(reduc_min_expr)}
    break;

  case 522:

    {CTN(reduc_plus_expr)}
    break;

  case 524:

    {CTN(plus_expr)}
    break;

  case 526:

    {CTN(pointer_plus_expr)}
    break;

  case 528:

    {CTN(minus_expr)}
    break;

  case 530:

    {CTN(mult_expr)}
    break;

  case 532:

    {CTN(trunc_div_expr)}
    break;

  case 534:

    {CTN(trunc_mod_expr)}
    break;

  case 536:

    {CTN(le_expr)}
    break;

  case 538:

    {CTN(truth_or_expr)}
    break;

  case 540:

    {CTN(ne_expr)}
    break;

  case 542:

    {CTN(eq_expr)}
    break;

  case 544:

    {CTN(compound_expr)}
    break;

  case 546:

    {CTN(init_expr)}
    break;

  case 548:

    {CTN(ceil_div_expr)}
    break;

  case 550:

    {CTN(floor_div_expr)}
    break;

  case 552:

    {CTN(round_div_expr)}
    break;

  case 554:

    {CTN(ceil_mod_expr)}
    break;

  case 556:

    {CTN(floor_mod_expr)}
    break;

  case 558:

    {CTN(round_mod_expr)}
    break;

  case 560:

    {CTN(rdiv_expr)}
    break;

  case 562:

    {CTN(exact_div_expr)}
    break;

  case 564:

    {CTN(min_expr)}
    break;

  case 566:

    {CTN(max_expr)}
    break;

  case 568:

    {CTN(lshift_expr)}
    break;

  case 570:

    {CTN(rshift_expr)}
    break;

  case 572:

    {CTN(lrotate_expr)}
    break;

  case 574:

    {CTN(rrotate_expr)}
    break;

  case 576:

    {CTN(bit_ior_expr)}
    break;

  case 578:

    {CTN(bit_xor_expr)}
    break;

  case 580:

    {CTN(bit_and_expr)}
    break;

  case 582:

    {CTN(bit_not_expr)}
    break;

  case 584:

    {CTN(truth_andif_expr)}
    break;

  case 586:

    {CTN(truth_orif_expr)}
    break;

  case 588:

    {CTN(truth_and_expr)}
    break;

  case 590:

    {CTN(truth_xor_expr)}
    break;

  case 592:

    {CTN(lt_expr)}
    break;

  case 594:

    {CTN(ge_expr)}
    break;

  case 596:

    {CTN(unordered_expr)}
    break;

  case 598:

    {CTN(ordered_expr)}
    break;

  case 600:

    {CTN(unlt_expr)}
    break;

  case 602:

    {CTN(unle_expr)}
    break;

  case 604:

    {CTN(ungt_expr)}
    break;

  case 606:

    {CTN(unge_expr)}
    break;

  case 608:

    {CTN(uneq_expr)}
    break;

  case 610:

    {CTN(ltgt_expr)}
    break;

  case 612:

    {CTN(in_expr)}
    break;

  case 614:

    {CTN(set_le_expr)}
    break;

  case 616:

    {CTN(range_expr)}
    break;

  case 618:

    {CTN(complex_expr)}
    break;

  case 620:

    {CTN(predecrement_expr)}
    break;

  case 622:

    {CTN(preincrement_expr)}
    break;

  case 624:

    {CTN(with_cleanup_expr)}
    break;

  case 626:

    {ADDV(vector_cst, add_valu)}
    break;

  case 627:

    {ADDV(vector_cst, add_valu)}
    break;

  case 628:

    {CTN(vector_cst)}
    break;

  case 629:

    {NS(vector_cst,type)}
    break;

  case 631:

    {CTN(try_catch_expr)}
    break;

  case 633:

    {CTN(vtable_ref)}
    break;

  case 635:

    {CTN(try_finally)}
    break;

  case 637:

    {CTN(mem_ref)}
    break;

  case 639:

    {CTN(assert_expr)}
    break;

  case 641:

    {CTN(widen_sum_expr)}
    break;

  case 643:

    {CTN(widen_mult_expr)}
    break;

  case 645:

    {CTN(vec_new_expr)}
    break;

  case 647:

    {CTN(try_block)}
    break;

  case 648:

    {NSV(try_block,line,data->curr_number)}
    break;

  case 649:

    {NS(try_block,body)}
    break;

  case 650:

    {NS(try_block,hdlr)}
    break;

  case 651:

    {OPT((yyvsp[(9) - (9)].pred), NS(try_block,next))}
    break;

  case 652:

    {ADDV(tree_vec, add_op)}
    break;

  case 653:

    {ADDV(tree_vec, add_op)}
    break;

  case 656:

    {CTN(tree_vec)}
    break;

  case 657:

    {NSV(tree_vec, lngt, data->curr_size_t_number)}
    break;

  case 659:

    {CTN(template_id_expr)}
    break;

  case 661:

    {CTN(array_ref)}
    break;

  case 663:

    {CTN(ssa_name)}
    break;

  case 664:

    {OPT((yyvsp[(3) - (3)].pred), NS(ssa_name,type))}
    break;

  case 665:

    {OPT((yyvsp[(5) - (5)].pred), NS(ssa_name, var))}
    break;

  case 666:

    {NSV(ssa_name, vers, (BisonParserData::global_uniq_vers_id++))NSV(ssa_name, orig_vers, data->curr_unumber)}
    break;

  case 667:

    {OPT((yyvsp[(9) - (9)].pred), NSV(ssa_name, orig_vers, data->curr_unumber))}
    break;

  case 668:

    {;}
    break;

  case 669:

    {OPT((yyvsp[(14) - (14)].pred), NSV(ssa_name, virtual_flag, true));}
    break;

  case 670:

    {OPT((yyvsp[(16) - (16)].pred), NSV(ssa_name, default_flag, true));}
    break;

  case 671:

    {OPT((yyvsp[(18) - (18)].pred), NS(ssa_name,min))}
    break;

  case 672:

    {OPT((yyvsp[(20) - (20)].pred), NS(ssa_name,max))}
    break;

  case 675:

    {GetPointer<ssa_name>(data->curr_tree_nodeRef)->use_set->Add(data->curr_string);}
    break;

  case 676:

    {GetPointer<ssa_name>(data->curr_tree_nodeRef)->use_set->Add(data->current_TM->GetTreeReindex(data->curr_NODE_ID));}
    break;

  case 679:

    {ADDV(ssa_name, add_def)}
    break;

  case 680:

    {ADDV(ssa_name, add_def)}
    break;

  case 682:

    {NSV(ssa_name, volatile_flag, true)}
    break;

  case 683:

    {CTN(gimple_switch)}
    break;

  case 684:

    {NS(gimple_switch,op0)}
    break;

  case 685:

    {NS(gimple_switch,op1)}
    break;

  case 686:

    {CTN(save_expr)}
    break;

  case 687:

    {NS(ternary_expr,op0)}
    break;

  case 690:

    {NS(ternary_expr, op1)}
    break;

  case 691:

    {NS(ternary_expr, op1)}
    break;

  case 692:

    {NS(ternary_expr, op2)}
    break;

  case 693:

    {CTN(scope_ref)}
    break;

  case 694:

    {NS(scope_ref,op0)}
    break;

  case 695:

    {NS(scope_ref,op1)}
    break;

  case 696:

    {ADDV(statement_list, add_stmt)}
    break;

  case 697:

    {ADDV(statement_list, add_stmt)}
    break;

  case 698:

    {ADDB(statement_list, add_bloc,data->curr_blocRef)}
    break;

  case 699:

    {ADDB(statement_list, add_bloc,data->curr_blocRef)}
    break;

  case 703:

    { CTN(statement_list) }
    break;

  case 704:

    {;}
    break;

  case 705:

    {CTN(new_expr)}
    break;

  case 707:

    {CTN(dot_prod_expr)}
    break;

  case 709:

    {CTN(vec_cond_expr)}
    break;

  case 711:

    {CTN(vec_perm_expr)}
    break;

  case 713:

    {CTN(vec_lshift_expr)}
    break;

  case 715:

    {CTN(vec_rshift_expr)}
    break;

  case 717:

    {CTN(widen_mult_hi_expr)}
    break;

  case 719:

    {CTN(widen_mult_lo_expr)}
    break;

  case 721:

    {CTN(vec_unpack_hi_expr)}
    break;

  case 723:

    {CTN(vec_unpack_lo_expr)}
    break;

  case 725:

    {CTN(vec_unpack_float_hi_expr)}
    break;

  case 727:

    {CTN(vec_unpack_float_lo_expr)}
    break;

  case 729:

    {CTN(vec_pack_trunc_expr)}
    break;

  case 731:

    {CTN(vec_pack_sat_expr)}
    break;

  case 733:

    {CTN(vec_pack_fix_trunc_expr)}
    break;

  case 735:

    {CTN(vec_extracteven_expr)}
    break;

  case 737:

    {CTN(vec_extractodd_expr)}
    break;

  case 739:

    {CTN(vec_interleavehigh_expr)}
    break;

  case 741:

    {CTN(vec_interleavelow_expr)}
    break;

  case 743:

    {CTN(return_stmt)}
    break;

  case 744:

    {NSV(return_stmt,line,data->curr_number)}
    break;

  case 745:

    {OPT((yyvsp[(5) - (5)].pred), NS(return_stmt,expr))}
    break;

  case 746:

    {CTN(postincrement_expr)}
    break;

  case 748:

    {CTN(modop_expr)}
    break;

  case 751:

    {ADD_UNSIGNED(gimple_phi, add_def_edge)}
    break;

  case 752:

    {CTN(gimple_phi)}
    break;

  case 753:

    {;}
    break;

  case 754:

    {NS                (gimple_phi,res)}
    break;

  case 755:

    {;}
    break;

  case 756:

    {OPT((yyvsp[(9) - (9)].pred), NSV(gimple_phi, virtual_flag, true));}
    break;

  case 759:

    {ADD_UNSIGNED(gimple_multi_way_if, add_cond)}
    break;

  case 760:

    {CTN(gimple_multi_way_if)}
    break;

  case 761:

    {;}
    break;

  case 762:

    {;}
    break;

  case 763:

    {CTN(postdecrement_expr)}
    break;

  case 766:

    {NSV(real_cst, overflow_flag, true)}
    break;

  case 767:

    {CTN(real_cst)}
    break;

  case 768:

    {NS(real_cst,type)}
    break;

  case 769:

    {NSV(real_cst, valr,data->curr_string)}
    break;

  case 770:

    {NSV(real_cst, valx, data->curr_string)}
    break;

  case 771:

    {CTN(placeholder_expr)}
    break;

  case 772:

    {CTN(overload)}
    break;

  case 773:

    {NS(overload,crnt)}
    break;

  case 774:

    {OPT((yyvsp[(5) - (5)].pred), NS(overload,chan))}
    break;

  case 775:

    {CTN(fdesc_expr)}
    break;

  case 777:

    {CTN(handler)}
    break;

  case 778:

    {NSV(handler,line,data->curr_number)}
    break;

  case 779:

    {NS(handler,body)}
    break;

  case 780:

    {CTN(catch_expr)}
    break;

  case 782:

    {CTN(component_ref)}
    break;

  case 784:

    {CTN(expr_stmt)}
    break;

  case 785:

    {NSV(expr_stmt,line,data->curr_number)}
    break;

  case 786:

    {NS(expr_stmt,expr)}
    break;

  case 787:

    {NS(expr_stmt,next)}
    break;

  case 788:

    {CTN(complex_cst)}
    break;

  case 789:

    {NS(complex_cst,type)}
    break;

  case 790:

    {NS(complex_cst,real)}
    break;

  case 791:

    {NS(complex_cst,imag)}
    break;

  case 792:

    {CTN(eh_filter_expr)}
    break;

  case 794:

    {CTN(ctor_initializer)}
    break;

  case 795:

    {CTN(constructor)}
    break;

  case 796:

    {OPT((yyvsp[(3) - (3)].pred),NS(constructor,type))}
    break;

  case 800:

    {data->curr_NODE_ID_BIS=data->curr_NODE_ID;}
    break;

  case 801:

    {ADDV2(constructor, add_idx_valu)}
    break;

  case 802:

    {ADDV(constructor, add_valu)}
    break;

  case 803:

    {CTN(block)}
    break;

  case 804:

    {OPT((yyvsp[(3) - (3)].pred), {NSV(block, bl, data->curr_string); NSV(block,bl_flag,true)})}
    break;

  case 805:

    {CTN(cond_expr)}
    break;

  case 806:

    {NS(cond_expr, op0)}
    break;

  case 807:

    {NS(cond_expr, op1)}
    break;

  case 808:

    {NS(cond_expr, op2)}
    break;

  case 809:

    {CTN(gimple_cond)}
    break;

  case 810:

    {NS(gimple_cond, op0)}
    break;

  case 811:

    {CTN(goto_subroutine)}
    break;

  case 814:

    {NSV(gimple_asm, volatile_flag, true)}
    break;

  case 815:

    {CTN(gimple_asm)}
    break;

  case 816:

    {NSV(gimple_asm,str,data->curr_string)}
    break;

  case 817:

    {OPT((yyvsp[(7) - (7)].pred), NS(gimple_asm,out))}
    break;

  case 818:

    {OPT((yyvsp[(9) - (9)].pred), NS(gimple_asm,in))}
    break;

  case 819:

    {OPT((yyvsp[(11) - (11)].pred), NS(gimple_asm,clob))}
    break;

  case 820:

    {NS(case_label_expr,op0)}
    break;

  case 821:

    {OPT((yyvsp[(3) - (3)].pred), NS(case_label_expr,op1))}
    break;

  case 822:

    {NSV(case_label_expr, default_flag, true)}
    break;

  case 823:

    {CTN(case_label_expr)}
    break;

  case 824:

    {NS(case_label_expr,got)}
    break;

  case 827:

    {data->curr_token_enum=lexer->bison2token(TOK_BISON_PUB); }
    break;

  case 828:

    {ADD_ACCESS_BINF(binfo, add_access_binf)}
    break;

  case 829:

    {data->curr_token_enum=lexer->bison2token(TOK_BISON_PROT);}
    break;

  case 830:

    {ADD_ACCESS_BINF(binfo, add_access_binf)}
    break;

  case 831:

    {data->curr_token_enum=lexer->bison2token(TOK_BISON_PRIV);}
    break;

  case 832:

    {ADD_ACCESS_BINF(binfo, add_access_binf)}
    break;

  case 834:

    {NSV(binfo, virt_flag, true)}
    break;

  case 835:

    {CTN(binfo)}
    break;

  case 836:

    {OPT((yyvsp[(3) - (3)].pred), NS(binfo,type))}
    break;

  case 838:

    {CTN(baselink)}
    break;

  case 839:

    {OPT((yyvsp[(3) - (3)].pred), NS(baselink,type))}
    break;

  case 840:

    {CTN(array_ref)}
    break;

  case 842:

    {CTN(call_expr)}
    break;

  case 843:

    {NS(call_expr,fn)}
    break;

  case 846:

    {ADDV(call_expr,AddArg)}
    break;

  case 848:

    {CTN(gimple_call)}
    break;

  case 849:

    {NS(gimple_call,fn)}
    break;

  case 852:

    {ADDV(gimple_call,AddArg)}
    break;

  case 856:

    {GetPointer<gimple_call>(data->curr_tree_nodeRef)->use_set->Add(data->curr_string);}
    break;

  case 857:

    {GetPointer<gimple_call>(data->curr_tree_nodeRef)->use_set->Add(data->current_TM->GetTreeReindex(data->curr_NODE_ID));}
    break;

  case 860:

    {GetPointer<gimple_call>(data->curr_tree_nodeRef)->clobbered_set->Add(data->curr_string);}
    break;

  case 861:

    {GetPointer<gimple_call>(data->curr_tree_nodeRef)->clobbered_set->Add(data->current_TM->GetTreeReindex(data->curr_NODE_ID));}
    break;

  case 862:

    {CTN(bit_field_ref)}
    break;

  case 864:

    {CTN(array_range_ref)}
    break;

  case 866:

    {CTN(template_parm_index)}
    break;

  case 867:

    {CTN(with_size_expr)}
    break;

  case 869:

    {CTN(obj_type_ref)}
    break;

  case 872:

    {NSV(decl_node, C_flag, true)}
    break;

  case 873:

    {(yyval.pred)=false;}
    break;

  case 874:

    {(yyval.pred)=true;}
    break;

  case 875:

    {(yyval.pred)=false;}
    break;

  case 876:

    {(yyval.pred)=true;}
    break;

  case 877:

    {(yyval.pred)=false;}
    break;

  case 878:

    {(yyval.pred)=true;}
    break;

  case 879:

    {(yyval.pred)=false;}
    break;

  case 880:

    {(yyval.pred)=true;}
    break;

  case 881:

    {(yyval.pred)=false;}
    break;

  case 882:

    {(yyval.pred)=true;}
    break;

  case 883:

    {(yyval.pred)=false;}
    break;

  case 884:

    {(yyval.pred)=true;}
    break;

  case 885:

    { (yyval.pred)=false; }
    break;

  case 886:

    {(yyval.pred)=true;}
    break;

  case 887:

    {OPT((yyvsp[(1) - (1)].pred), NS(decl_node,name))}
    break;

  case 888:

    {OPT((yyvsp[(3) - (3)].pred), NS(decl_node,mngl))}
    break;

  case 889:

    {OPT((yyvsp[(5) - (5)].pred), NS(decl_node,orig))}
    break;

  case 890:

    {OPT((yyvsp[(7) - (7)].pred), NS(decl_node,type))}
    break;

  case 891:

    {OPT((yyvsp[(9) - (9)].pred), NS(decl_node,scpe))}
    break;

  case 892:

    {OPT((yyvsp[(12) - (12)].pred), NS(decl_node,chan))}
    break;

  case 893:

    {OPT((yyvsp[(14) - (14)].pred), NS(decl_node,attributes))}
    break;

  case 894:

    {OPT((yyvsp[(16) - (16)].pred), NSV(decl_node, artificial_flag, true))}
    break;

  case 895:

    {OPT((yyvsp[(18) - (18)].pred), NSV(decl_node, operating_system_flag, true))}
    break;

  case 896:

    {OPT((yyvsp[(20) - (20)].pred), NSV(decl_node, library_system_flag, true))}
    break;

  case 897:

    {NSV(decl_node, uid, data->curr_unumber)}
    break;

  case 898:

    {OPT((yyvsp[(1) - (1)].pred), NS(expr_node,type))}
    break;

  case 899:

    {;}
    break;

  case 901:

    {ADDV(gimple_node,add_dep_vuse)}
    break;

  case 902:

    {OPT((yyvsp[(1) - (1)].pred), NS(gimple_node, scpe))}
    break;

  case 903:

    {NSV(gimple_node, bb_index, data->curr_unumber)}
    break;

  case 904:

    {;}
    break;

  case 905:

    {;}
    break;

  case 906:

    {;}
    break;

  case 907:

    {;}
    break;

  case 908:

    {;}
    break;

  case 909:

    {
#if HAVE_MAPPING_BUILT && HAVE_CODE_ESTIMATION_BUILT
                     std::map<ComponentTypeConstRef, ProbabilityDistribution> weights;
                     if((data->Param->getOption<bool>(OPT_gcc_costs)))
                     {
                        weights[data->driving_component] = data->curr_double_number;
                     }
                     OPT((yyvsp[(15) - (15)].pred), GetPointer<gimple_node>(data->curr_tree_nodeRef)->weight_information->recursive_weight = weights);
#endif
                  }
    break;

  case 910:

    {
#if HAVE_CODE_ESTIMATION_BUILT
                     OPT((yyvsp[(17) - (17)].pred), GetPointer<gimple_node>(data->curr_tree_nodeRef)->weight_information->instruction_size = data->curr_unumber);
#endif
                  }
    break;

  case 911:

    {
#if HAVE_CODE_ESTIMATION_BUILT && HAVE_RTL_BUILT
                     OPT((yyvsp[(19) - (19)].pred), GetPointer<gimple_node>(data->curr_tree_nodeRef)->weight_information->rtl_instruction_size = data->curr_unumber);
#endif
                  }
    break;

  case 912:

    {
#if HAVE_CODE_ESTIMATION_BUILT && HAVE_RTL_BUILT
                       GetPointer<gimple_node>(data->curr_tree_nodeRef)->weight_information->rtl_nodes = data->rtls;
                       data->rtls.clear();
#endif
                  }
    break;

  case 913:

    {OPT((yyvsp[(1) - (1)].pred), do_qual<type_node>(data->curr_tree_nodeRef, data->curr_number);)}
    break;

  case 914:

    {OPT((yyvsp[(3) - (3)].pred), NS(type_node,name))}
    break;

  case 915:

    {OPT((yyvsp[(5) - (5)].pred), NS(type_node,unql))}
    break;

  case 916:

    {OPT((yyvsp[(7) - (7)].pred), NS(type_node,size))}
    break;

  case 917:

    {OPT((yyvsp[(9) - (9)].pred), NS(type_node,scpe))}
    break;

  case 918:

    {OPT((yyvsp[(11) - (11)].pred), NSV(type_node,algn, data->curr_unumber))}
    break;

  case 919:

    {OPT((yyvsp[(13) - (13)].pred), NSV(type_node, packed_flag, true))}
    break;

  case 920:

    {OPT((yyvsp[(15) - (15)].pred), NSV(type_node, system_flag, true))}
    break;

  case 921:

    {(yyval.pred)=false;}
    break;

  case 922:

    {data->curr_number=lexer->bison2token(TOK_BISON_QUAL_R);(yyval.pred)=true;}
    break;

  case 923:

    {data->curr_number=lexer->bison2token(TOK_BISON_QUAL_V);(yyval.pred)=true;}
    break;

  case 924:

    {data->curr_number=lexer->bison2token(TOK_BISON_QUAL_VR);(yyval.pred)=true;}
    break;

  case 925:

    {data->curr_number=lexer->bison2token(TOK_BISON_QUAL_C);(yyval.pred)=true;}
    break;

  case 926:

    {data->curr_number=lexer->bison2token(TOK_BISON_QUAL_CR);(yyval.pred)=true;}
    break;

  case 927:

    {data->curr_number=lexer->bison2token(TOK_BISON_QUAL_CV);(yyval.pred)=true;}
    break;

  case 928:

    {data->curr_number=lexer->bison2token(TOK_BISON_QUAL_CVR);(yyval.pred)=true;}
    break;

  case 931:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NEW))}
    break;

  case 932:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_DELETE))}
    break;

  case 933:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_ASSIGN))}
    break;

  case 934:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MEMBER))}
    break;

  case 935:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_PUBLIC))}
    break;

  case 936:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_PROTECTED))}
    break;

  case 937:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_PRIVATE))}
    break;

  case 938:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NORETURN))}
    break;

  case 939:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_VOLATILE))}
    break;

  case 940:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NOINLINE))}
    break;

  case 941:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_ALWAYS_INLINE))}
    break;

  case 942:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_USED))}
    break;

  case 943:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_UNUSED))}
    break;

  case 944:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_CONST))}
    break;

  case 945:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_TRANSPARENT_UNION))}
    break;

  case 946:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_CONSTRUCTOR))}
    break;

  case 947:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_DESTRUCTOR))}
    break;

  case 948:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MODE))}
    break;

  case 949:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_SECTION))}
    break;

  case 950:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_ALIGNED))}
    break;

  case 951:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_WEAK))}
    break;

  case 952:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_ALIAS))}
    break;

  case 953:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NO_INSTRUMENT_FUNCTION))}
    break;

  case 954:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MALLOC))}
    break;

  case 955:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NO_STACK_LIMIT))}
    break;

  case 956:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_PURE))}
    break;

  case 957:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_DEPRECATED))}
    break;

  case 958:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_VECTOR_SIZE))}
    break;

  case 959:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_VISIBILITY))}
    break;

  case 960:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_TLS_MODEL))}
    break;

  case 961:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NONNULL))}
    break;

  case 962:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NOTHROW))}
    break;

  case 963:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MAY_ALIAS))}
    break;

  case 964:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_WARN_UNUSED_RESULT))}
    break;

  case 965:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_FORMAT))}
    break;

  case 966:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_FORMAT_ARG))}
    break;

  case 967:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NULL))}
    break;

  case 968:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_GLOBAL_INIT))}
    break;

  case 969:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_GLOBAL_FINI))}
    break;

  case 970:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_CONVERSION))}
    break;

  case 971:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_VIRTUAL))}
    break;

  case 972:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_LSHIFT))}
    break;

  case 973:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MUTABLE))}
    break;

  case 974:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_PSEUDO_TMPL))}
    break;

  case 975:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_VECNEW))}
    break;

  case 976:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_VECDELETE))}
    break;

  case 977:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_POS))}
    break;

  case 978:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NEG))}
    break;

  case 979:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_ADDR))}
    break;

  case 980:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_DEREF))}
    break;

  case 981:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_LNOT))}
    break;

  case 982:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NOT))}
    break;

  case 983:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_PREINC))}
    break;

  case 984:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_PREDEC))}
    break;

  case 985:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_PLUSASSIGN))}
    break;

  case 986:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_PLUS))}
    break;

  case 987:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MINUSASSIGN))}
    break;

  case 988:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MINUS))}
    break;

  case 989:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MULTASSIGN))}
    break;

  case 990:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MULT))}
    break;

  case 991:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_DIVASSIGN))}
    break;

  case 992:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_DIV))}
    break;

  case 993:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MODASSIGN))}
    break;

  case 994:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MOD))}
    break;

  case 995:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_ANDASSIGN))}
    break;

  case 996:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_AND))}
    break;

  case 997:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_ORASSIGN))}
    break;

  case 998:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_OR))}
    break;

  case 999:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_XORASSIGN))}
    break;

  case 1000:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_XOR))}
    break;

  case 1001:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_LSHIFTASSIGN))}
    break;

  case 1002:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_RSHIFTASSIGN))}
    break;

  case 1003:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_RSHIFT))}
    break;

  case 1004:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_EQ))}
    break;

  case 1005:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_NE))}
    break;

  case 1006:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_LT))}
    break;

  case 1007:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_GT))}
    break;

  case 1008:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_LE))}
    break;

  case 1009:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_GE))}
    break;

  case 1010:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_LAND))}
    break;

  case 1011:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_LOR))}
    break;

  case 1012:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_COMPOUND))}
    break;

  case 1013:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_MEMREF))}
    break;

  case 1014:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_REF))}
    break;

  case 1015:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_SUBS))}
    break;

  case 1016:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_POSTINC))}
    break;

  case 1017:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_POSTDEC))}
    break;

  case 1018:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_CALL))}
    break;

  case 1019:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_THUNK))}
    break;

  case 1020:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_THIS_ADJUSTING))}
    break;

  case 1021:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_RESULT_ADJUSTING))}
    break;

  case 1022:

    {ADD_ATTR(lexer->bison2token(TOK_BISON_BITFIELD))}
    break;

  case 1023:

    {OPT((yyvsp[(2) - (2)].pred), NS(unary_expr,op))}
    break;

  case 1024:

    {NS(binary_expr,op0)}
    break;

  case 1025:

    {OPT((yyvsp[(4) - (4)].pred), NS(binary_expr,op1))}
    break;

  case 1026:

    {NS(ternary_expr,op0)}
    break;

  case 1027:

    {NS(ternary_expr,op1)}
    break;

  case 1028:

    {OPT((yyvsp[(6) - (6)].pred), NS(ternary_expr,op2))}
    break;

  case 1029:

    {NS(quaternary_expr,op0)}
    break;

  case 1030:

    {NS(quaternary_expr,op1)}
    break;

  case 1033:

    {NS(quaternary_expr, op2)}
    break;

  case 1034:

    {NS(quaternary_expr, op2)}
    break;

  case 1035:

    {NS(quaternary_expr, op3)}
    break;

  case 1037:

    {NSV(srcp, include_name, data->curr_string)}
    break;

  case 1038:

    {NSV(srcp, line_number, data->curr_unumber)}
    break;

  case 1039:

    {NSV(srcp, column_number, data->curr_unumber)}
    break;

  case 1041:

    {data->current_TM->add_parallel_loop();NBV(bloc, hpl, 1)}
    break;

  case 1043:

    {NBV(bloc, loop_id, data->curr_unumber)}
    break;

  case 1045:

    {ADDVALUE(bloc, add_pred, data->curr_unumber)}
    break;

  case 1047:

    {ADDVALUE(bloc, add_succ, data->curr_unumber)}
    break;

  case 1049:

    {ADDP(bloc, add_phi)}
    break;

  case 1051:

    {ADDP(bloc, add_stmt)}
    break;

  case 1053:

    {
#if HAVE_RTL_BUILT
                 data->rtls.push_back(std::pair<enum rtl_kind, enum mode_kind>(data->single_rtl, data->mode_rtl));
#endif
              }
    break;

  case 1054:

    {SET_DATA(data->single_rtl, abs_R);}
    break;

  case 1055:

    {SET_DATA(data->single_rtl, and_R);}
    break;

  case 1056:

    {SET_DATA(data->single_rtl, ashift_R);}
    break;

  case 1057:

    {SET_DATA(data->single_rtl, ashiftrt_R);}
    break;

  case 1058:

    {SET_DATA(data->single_rtl, bswap_R);}
    break;

  case 1059:

    {SET_DATA(data->single_rtl, call_R);}
    break;

  case 1060:

    {SET_DATA(data->single_rtl, call_insn_R);}
    break;

  case 1061:

    {SET_DATA(data->single_rtl, clz_R);}
    break;

  case 1062:

    {SET_DATA(data->single_rtl, code_label_R);}
    break;

  case 1063:

    {SET_DATA(data->single_rtl, compare_R);}
    break;

  case 1064:

    {SET_DATA(data->single_rtl, concat_R);}
    break;

  case 1065:

    {SET_DATA(data->single_rtl, ctz_R);}
    break;

  case 1066:

    {SET_DATA(data->single_rtl, div_R);}
    break;

  case 1067:

    {SET_DATA(data->single_rtl, eq_R);}
    break;

  case 1068:

    {SET_DATA(data->single_rtl, ffs_R);}
    break;

  case 1069:

    {SET_DATA(data->single_rtl, fix_R);}
    break;

  case 1070:

    {SET_DATA(data->single_rtl, float_R);}
    break;

  case 1071:

    {SET_DATA(data->single_rtl, float_extend_R);}
    break;

  case 1072:

    {SET_DATA(data->single_rtl, float_truncate_R);}
    break;

  case 1073:

    {SET_DATA(data->single_rtl, fract_convert_R);}
    break;

  case 1074:

    {SET_DATA(data->single_rtl, ge_R);}
    break;

  case 1075:

    {SET_DATA(data->single_rtl, geu_R);}
    break;

  case 1076:

    {SET_DATA(data->single_rtl, gt_R);}
    break;

  case 1077:

    {SET_DATA(data->single_rtl, gtu_R);}
    break;

  case 1078:

    {SET_DATA(data->single_rtl, high_R);}
    break;

  case 1079:

    {SET_DATA(data->single_rtl, if_then_else_R);}
    break;

  case 1080:

    {SET_DATA(data->single_rtl, insn_R);}
    break;

  case 1081:

    {SET_DATA(data->single_rtl, ior_R);}
    break;

  case 1082:

    {SET_DATA(data->single_rtl, jump_insn_R);}
    break;

  case 1083:

    {SET_DATA(data->single_rtl, label_ref_R);}
    break;

  case 1084:

    {SET_DATA(data->single_rtl, le_R);}
    break;

  case 1085:

    {SET_DATA(data->single_rtl, leu_R);}
    break;

  case 1086:

    {SET_DATA(data->single_rtl, lo_sum_R);}
    break;

  case 1087:

    {SET_DATA(data->single_rtl, lshiftrt_R);}
    break;

  case 1088:

    {SET_DATA(data->single_rtl, lt_R);}
    break;

  case 1089:

    {SET_DATA(data->single_rtl, ltgt_R);}
    break;

  case 1090:

    {SET_DATA(data->single_rtl, ltu_R);}
    break;

  case 1091:

    {SET_DATA(data->single_rtl, write_mem_R);}
    break;

  case 1092:

    {SET_DATA(data->single_rtl, read_mem_R);}
    break;

  case 1093:

    {SET_DATA(data->single_rtl, minus_R);}
    break;

  case 1094:

    {SET_DATA(data->single_rtl, mod_R);}
    break;

  case 1095:

    {SET_DATA(data->single_rtl, mult_R);}
    break;

  case 1096:

    {SET_DATA(data->single_rtl, ne_R);}
    break;

  case 1097:

    {SET_DATA(data->single_rtl, neg_R);}
    break;

  case 1098:

    {SET_DATA(data->single_rtl, not_R);}
    break;

  case 1099:

    {SET_DATA(data->single_rtl, ordered_R);}
    break;

  case 1100:

    {SET_DATA(data->single_rtl, parallel_R);}
    break;

  case 1101:

    {SET_DATA(data->single_rtl, parity_R);}
    break;

  case 1102:

    {SET_DATA(data->single_rtl, pc_R);}
    break;

  case 1103:

    {SET_DATA(data->single_rtl, plus_R);}
    break;

  case 1104:

    {SET_DATA(data->single_rtl, popcount_R);}
    break;

  case 1105:

    {SET_DATA(data->single_rtl, reg_R);}
    break;

  case 1106:

    {SET_DATA(data->single_rtl, rotate_R);}
    break;

  case 1107:

    {SET_DATA(data->single_rtl, rotatert_R);}
    break;

  case 1108:

    {SET_DATA(data->single_rtl, sat_fract_R);}
    break;

  case 1109:

    {SET_DATA(data->single_rtl, set_R);}
    break;

  case 1110:

    {SET_DATA(data->single_rtl, sign_extend_R);}
    break;

  case 1111:

    {SET_DATA(data->single_rtl, smax_R);}
    break;

  case 1112:

    {SET_DATA(data->single_rtl, smin_R);}
    break;

  case 1113:

    {SET_DATA(data->single_rtl, sqrt_R);}
    break;

  case 1114:

    {SET_DATA(data->single_rtl, symbol_ref_R);}
    break;

  case 1115:

    {SET_DATA(data->single_rtl, truncate_R);}
    break;

  case 1116:

    {SET_DATA(data->single_rtl, udiv_R);}
    break;

  case 1117:

    {SET_DATA(data->single_rtl, umax_R);}
    break;

  case 1118:

    {SET_DATA(data->single_rtl, umin_R);}
    break;

  case 1119:

    {SET_DATA(data->single_rtl, umod_R);}
    break;

  case 1120:

    {SET_DATA(data->single_rtl, uneq_R);}
    break;

  case 1121:

    {SET_DATA(data->single_rtl, unge_R);}
    break;

  case 1122:

    {SET_DATA(data->single_rtl, ungt_R);}
    break;

  case 1123:

    {SET_DATA(data->single_rtl, unle_R);}
    break;

  case 1124:

    {SET_DATA(data->single_rtl, unlt_R);}
    break;

  case 1125:

    {SET_DATA(data->single_rtl, unordered_R);}
    break;

  case 1126:

    {SET_DATA(data->single_rtl, unsigned_fix_R);}
    break;

  case 1127:

    {SET_DATA(data->single_rtl, unsigned_float_R);}
    break;

  case 1128:

    {SET_DATA(data->single_rtl, unsigned_fract_convert_R);}
    break;

  case 1129:

    {SET_DATA(data->single_rtl, unsigned_sat_fract_R);}
    break;

  case 1130:

    {SET_DATA(data->single_rtl, xor_R);}
    break;

  case 1131:

    {SET_DATA(data->single_rtl, zero_extend_R);}
    break;

  case 1132:

    {SET_DATA(data->mode_rtl, none_R);}
    break;

  case 1133:

    {SET_DATA(data->mode_rtl, qc_R);}
    break;

  case 1134:

    {SET_DATA(data->mode_rtl, hc_R);}
    break;

  case 1135:

    {SET_DATA(data->mode_rtl, sc_R);}
    break;

  case 1136:

    {SET_DATA(data->mode_rtl, dc_R);}
    break;

  case 1137:

    {SET_DATA(data->mode_rtl, tc_R);}
    break;

  case 1138:

    {SET_DATA(data->mode_rtl, cqi_R);}
    break;

  case 1139:

    {SET_DATA(data->mode_rtl, chi_R);}
    break;

  case 1140:

    {SET_DATA(data->mode_rtl, csi_R);}
    break;

  case 1141:

    {SET_DATA(data->mode_rtl, cdi_R);}
    break;

  case 1142:

    {SET_DATA(data->mode_rtl, cti_R);}
    break;

  case 1143:

    {SET_DATA(data->mode_rtl, qf_R);}
    break;

  case 1144:

    {SET_DATA(data->mode_rtl, hf_R);}
    break;

  case 1145:

    {SET_DATA(data->mode_rtl, sf_R);}
    break;

  case 1146:

    {SET_DATA(data->mode_rtl, df_R);}
    break;

  case 1147:

    {SET_DATA(data->mode_rtl, tf_R);}
    break;

  case 1148:

    {SET_DATA(data->mode_rtl, qi_R);}
    break;

  case 1149:

    {SET_DATA(data->mode_rtl, hi_R);}
    break;

  case 1150:

    {SET_DATA(data->mode_rtl, si_R);}
    break;

  case 1151:

    {SET_DATA(data->mode_rtl, di_R);}
    break;

  case 1152:

    {SET_DATA(data->mode_rtl, ti_R);}
    break;

  case 1153:

    {SET_DATA(data->mode_rtl, v2si_R);}
    break;

  case 1154:

    {SET_DATA(data->mode_rtl, v4hi_R);}
    break;

  case 1155:

    {SET_DATA(data->mode_rtl, v8qi_R);}
    break;

  case 1156:

    {SET_DATA(data->mode_rtl, cc_R);}
    break;

  case 1157:

    {SET_DATA(data->mode_rtl, ccfp_R);}
    break;

  case 1158:

    {SET_DATA(data->mode_rtl, ccfpe_R);}
    break;

  case 1159:

    {SET_DATA(data->mode_rtl, ccz_R);}
    break;

  case 1160:

    {CB()}
    break;

  case 1161:

    {NBV(bloc, number, data->curr_unumber)}
    break;

  case 1162:

    {;}
    break;

  case 1163:

    {;}
    break;

  case 1164:

    {;}
    break;

  case 1165:

    {;}
    break;

  case 1166:

    {OPT((yyvsp[(13) - (13)].pred),NBV(bloc, true_edge, data->curr_unumber))}
    break;

  case 1167:

    {OPT((yyvsp[(15) - (15)].pred),NBV(bloc, false_edge, data->curr_unumber))}
    break;

  case 1168:

    {;}
    break;

  case 1169:

    {;}
    break;

  case 1170:

    {;}
    break;

  case 1171:

    {data->curr_unumber=bloc::ENTRY_BLOCK_ID;}
    break;

  case 1173:

    {data->curr_unumber=bloc::EXIT_BLOCK_ID;}
    break;

  case 1175:

    {NS(gimple_node,vuse)}
    break;

  case 1178:

    {NS(gimple_node,vdef)}
    break;

  case 1181:

    {NS(gimple_node,redef_vuse)}
    break;

  case 1184:

    {CTN(target_mem_ref461)}
    break;

  case 1185:

    {NS(target_mem_ref461,type)}
    break;

  case 1186:

    {OPT((yyvsp[(5) - (5)].pred), NS(target_mem_ref461,base))}
    break;

  case 1187:

    {OPT((yyvsp[(7) - (7)].pred), NS(target_mem_ref461,offset))}
    break;

  case 1188:

    {OPT((yyvsp[(9) - (9)].pred), NS(target_mem_ref461,idx))}
    break;

  case 1189:

    {OPT((yyvsp[(11) - (11)].pred), NS(target_mem_ref461,step))}
    break;

  case 1190:

    {OPT((yyvsp[(13) - (13)].pred), NS(target_mem_ref461,idx2))}
    break;

  case 1191:

    {CTN(target_mem_ref)}
    break;

  case 1192:

    {NS(target_mem_ref,type)}
    break;

  case 1193:

    {OPT((yyvsp[(5) - (5)].pred), NS(target_mem_ref,symbol))}
    break;

  case 1194:

    {OPT((yyvsp[(7) - (7)].pred), NS(target_mem_ref,base))}
    break;

  case 1195:

    {OPT((yyvsp[(9) - (9)].pred), NS(target_mem_ref,idx))}
    break;

  case 1196:

    {OPT((yyvsp[(11) - (11)].pred), NS(target_mem_ref,step))}
    break;

  case 1197:

    {OPT((yyvsp[(13) - (13)].pred), NS(target_mem_ref,offset))}
    break;

  case 1198:

    {NS(target_mem_ref,orig)}
    break;

  case 1199:

    {OPT((yyvsp[(17) - (17)].pred), NS(target_mem_ref,tag));}
    break;

  case 1200:

    {CTN(trait_expr)}
    break;

  case 1201:

    {(yyval.pred)=false;}
    break;

  case 1202:

    {(yyval.pred)=true;}
    break;

  case 1203:

    {(yyval.pred)=false;}
    break;

  case 1204:

    {(yyval.pred)=true;}
    break;

  case 1205:

    {(yyval.pred)=false;}
    break;

  case 1206:

    {(yyval.pred)=true;}
    break;

  case 1236:

    {(yyval.pred)=false;}
    break;

  case 1237:

    {(yyval.pred)=true;}
    break;

  case 1246:

    {(yyval.pred)=false;}
    break;

  case 1247:

    {(yyval.pred)=true;}
    break;

  case 1257:

    {(yyval.pred)=false;}
    break;

  case 1258:

    {(yyval.pred)=true;}
    break;

  case 1259:

    {(yyval.pred)=false;}
    break;

  case 1260:

    {(yyval.pred)=true;}
    break;

  case 1261:

    {(yyval.pred)=false;}
    break;

  case 1262:

    {(yyval.pred)=true;}
    break;

  case 1263:

    {(yyval.pred)=false;}
    break;

  case 1264:

    {(yyval.pred)=true;}
    break;

  case 1265:

    {(yyval.pred)=false;}
    break;

  case 1266:

    {(yyval.pred)=true;}
    break;

  case 1267:

    {(yyval.pred)=false;}
    break;

  case 1268:

    {(yyval.pred)=true;}
    break;

  case 1269:

    {(yyval.pred)=false;}
    break;

  case 1270:

    {(yyval.pred)=true;}
    break;

  case 1271:

    {(yyval.pred)=false;}
    break;

  case 1272:

    {(yyval.pred)=true;}
    break;

  case 1273:

    {(yyval.pred)=false;}
    break;

  case 1274:

    {(yyval.pred)=true;}
    break;

  case 1275:

    {(yyval.pred)=false;}
    break;

  case 1276:

    {(yyval.pred)=true;}
    break;

  case 1277:

    {(yyval.pred)=false;}
    break;

  case 1278:

    {(yyval.pred)=true;}
    break;

  case 1279:

    {(yyval.pred)=false;}
    break;

  case 1280:

    {(yyval.pred)=true;}
    break;

  case 1281:

    {(yyval.pred)=false;}
    break;

  case 1282:

    {(yyval.pred)=true;}
    break;

  case 1283:

    {(yyval.pred)=false;}
    break;

  case 1284:

    {(yyval.pred)=true;}
    break;

  case 1285:

    {(yyval.pred)=false;}
    break;

  case 1286:

    {(yyval.pred)=true;}
    break;

  case 1287:

    {(yyval.pred)=false;}
    break;

  case 1288:

    {(yyval.pred)=true;}
    break;

  case 1289:

    {(yyval.pred)=false;}
    break;

  case 1290:

    {(yyval.pred)=true;}
    break;

  case 1291:

    {(yyval.pred)=false;}
    break;

  case 1292:

    {(yyval.pred)=true;}
    break;

  case 1293:

    {(yyval.pred)=false;}
    break;

  case 1294:

    {(yyval.pred)=true;}
    break;

  case 1295:

    {(yyval.pred)=false;}
    break;

  case 1296:

    {(yyval.pred)=true;}
    break;

  case 1297:

    {(yyval.pred)=false;}
    break;

  case 1298:

    {(yyval.pred)=true;}
    break;

  case 1299:

    {(yyval.pred)=false;}
    break;

  case 1300:

    {(yyval.pred)=true;}
    break;

  case 1301:

    {(yyval.pred)=false;}
    break;

  case 1302:

    {(yyval.pred)=true;}
    break;

  case 1303:

    {(yyval.pred)=false;}
    break;

  case 1304:

    {(yyval.pred)=true;}
    break;

  case 1305:

    {(yyval.pred)=false;}
    break;

  case 1306:

    {(yyval.pred)=true;}
    break;

  case 1307:

    {(yyval.pred)=false;}
    break;

  case 1308:

    {(yyval.pred)=true;}
    break;

  case 1309:

    {(yyval.pred)=false;}
    break;

  case 1310:

    {(yyval.pred)=true;}
    break;

  case 1311:

    {(yyval.pred)=false;}
    break;

  case 1312:

    {(yyval.pred)=true;}
    break;

  case 1313:

    {(yyval.pred)=false;}
    break;

  case 1314:

    {(yyval.pred)=true;}
    break;

  case 1315:

    {(yyval.pred)=false;}
    break;

  case 1316:

    {(yyval.pred)=true;}
    break;

  case 1317:

    {(yyval.pred)=false;}
    break;

  case 1318:

    {(yyval.pred)=true;}
    break;

  case 1319:

    {(yyval.pred)=false;}
    break;

  case 1320:

    {(yyval.pred)=true;}
    break;

  case 1321:

    {(yyval.pred)=false;}
    break;

  case 1322:

    {(yyval.pred)=true;}
    break;

  case 1323:

    {(yyval.pred)=false;}
    break;

  case 1324:

    {(yyval.pred)=true;}
    break;

  case 1325:

    {(yyval.pred)=false;}
    break;

  case 1326:

    {(yyval.pred)=true;}
    break;

  case 1327:

    {(yyval.pred)=false;}
    break;

  case 1328:

    {(yyval.pred)=true;}
    break;

  case 1329:

    {(yyval.pred)=true;}
    break;

  case 1330:

    {(yyval.pred)=false;}
    break;

  case 1331:

    {(yyval.pred)=true;}
    break;

  case 1332:

    {(yyval.pred)=false;}
    break;

  case 1333:

    {(yyval.pred)=true;}
    break;

  case 1334:

    {(yyval.pred)=false;}
    break;

  case 1335:

    {(yyval.pred)=true;}
    break;

  case 1336:

    {(yyval.pred)=false;}
    break;

  case 1337:

    {(yyval.pred)=true;}
    break;

  case 1338:

    {(yyval.pred)=false;}
    break;

  case 1339:

    {(yyval.pred)=true;}
    break;

  case 1340:

    {(yyval.pred)=false;}
    break;

  case 1341:

    {(yyval.pred)=true;}
    break;

  case 1342:

    {(yyval.pred)=false;}
    break;

  case 1343:

    {(yyval.pred)=true;}
    break;

  case 1344:

    {(yyval.pred)=false;}
    break;

  case 1345:

    {(yyval.pred)=true;}
    break;

  case 1346:

    {(yyval.pred)=false;}
    break;

  case 1347:

    {(yyval.pred)=true;}
    break;

  case 1348:

    {(yyval.pred)=false;}
    break;

  case 1349:

    {(yyval.pred)=true;}
    break;

  case 1350:

    {(yyval.pred)=false;}
    break;

  case 1351:

    {(yyval.pred)=true;}
    break;

  case 1352:

    {(yyval.pred)=false;}
    break;

  case 1353:

    {(yyval.pred)=true;}
    break;

  case 1354:

    {(yyval.pred)=false;}
    break;

  case 1355:

    {(yyval.pred)=true;}
    break;

  case 1356:

    {(yyval.pred)=false;}
    break;

  case 1357:

    {(yyval.pred)=true;}
    break;

  case 1358:

    {(yyval.pred)=false;}
    break;

  case 1359:

    {(yyval.pred)=true;}
    break;

  case 1360:

    {(yyval.pred)=false;}
    break;

  case 1361:

    {(yyval.pred)=true;}
    break;

  case 1362:

    {(yyval.pred)=false;}
    break;

  case 1363:

    {(yyval.pred)=true;}
    break;

  case 1364:

    {(yyval.pred)=false;}
    break;

  case 1365:

    {(yyval.pred)=true;}
    break;

  case 1366:

    {(yyval.pred)=false;}
    break;

  case 1367:

    {(yyval.pred)=true;}
    break;

  case 1368:

    {data->curr_NODE_ID=(yyvsp[(1) - (1)].value);}
    break;

  case 1369:

    {data->curr_string=std::string((yyvsp[(1) - (1)].text)+1, (yyvsp[(1) - (1)].text)+strlen((yyvsp[(1) - (1)].text))-1);}
    break;

  case 1370:

    {data->curr_number=(yyvsp[(1) - (1)].long_value); data->curr_unumber = (yyvsp[(1) - (1)].long_value); data->curr_size_t_number=(yyvsp[(1) - (1)].long_value); data->curr_long_long_number=(yyvsp[(1) - (1)].long_value);  data->curr_double_number=(yyvsp[(1) - (1)].long_value);}
    break;

  case 1371:

    {(yyval.pred) = false;}
    break;

  case 1372:

    {(yyval.pred) = true;}
    break;



      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (data, lexer, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (data, lexer, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval, data, lexer);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp, data, lexer);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (data, lexer, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, data, lexer);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, data, lexer);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}




/**
* EPILOGUE
*/

extern tree_managerRef tree_parseY(const ParameterConstRef Param, std::string fn)
{
    fileIO_istreamRef sname = fileIO_istream_open(fn);
    if(sname->fail()) THROW_ERROR(std::string("FILE does not exist: ")+fn);
    const TreeFlexLexerRef lexer(new TreeFlexLexer(sname.get(), 0));
    const BisonParserDataRef data(new BisonParserData(Param, Param->get_class_debug_level("tree_parse")));
    data->final_TM = tree_managerRef();
    data->current_TM = tree_managerRef();
#if YYDEBUG
    yydebug = 1;
#endif
    yyparse(data, lexer);
    if(data->final_TM)
      data->final_TM->merge_tree_managers(data->current_TM);
    else
      data->final_TM = data->current_TM;
    return data->final_TM;
}

void yyerror(const BisonParserDataRef data, const TreeFlexLexerRef lexer, char *msg)
{
  lexer->yyerror(msg);
}
int yylex(YYSTYPE *lvalp, const TreeFlexLexerRef lexer)
{
  lexer->lvalp=lvalp;
  return lexer->yylex();
}

