/*
 *
 *                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
 *                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
 *                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
 *                _/      _/    _/ _/    _/ _/   _/ _/    _/
 *               _/      _/    _/ _/    _/ _/_/_/  _/    _/
 *
 *             ***********************************************
 *                              PandA Project 
 *                     URL: http://panda.dei.polimi.it
 *                       Politecnico di Milano - DEIB
 *                        System Architectures Group
 *             ***********************************************
 *              Copyright (c) 2004-2014 Politecnico di Milano
 *
 *   This file is part of the PandA framework.
 *
 *   The PandA framework is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
/**
 * @file xml_dom_parser.hpp
 * @brief XML DOM parser.
 *
 * @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
 * $Revision$
 * $Date$
 * Last modified by $Author$
 *
 */
#ifndef XML_DOM_PARSER_HPP
#define XML_DOM_PARSER_HPP

#include "refcount.hpp"

#include <string>
#include <sstream>

/**
 * @name forward declarations
 */
//@{
REF_FORWARD_DECL(xml_dom_parser);
REF_FORWARD_DECL(xml_document);
//@}

/** 
 * XML DOM parser.
 */
struct xml_dom_parser
{
      ///default constructor
      xml_dom_parser() {}
      /** Instantiate the parser and parse a document immediately.
       * @throw exception
       * @param filename The path to the file.
       */
      xml_dom_parser(const std::string& filename) {parse_file(filename);}

      /** Parse an XML document from a file.
       * @throw exception
       * @param filename The path to the file.
      */
      void parse_file(const std::string& filename);

      /** Parse an XML document from a string.
       * @throw exception  
       * @param contents The XML document as a string.
      */ 
      void parse_memory(const std::string& contents)
      {
         std::istringstream stm(contents);
         parse_stream(stm);
      }

      /** Parse an XML document from a stream.
       * @throw exception
       * @param in The stream.
       */
      void parse_stream(std::istream& in)
      {
         extern xml_documentRef xml_parseY(std::istream&in);
         doc = xml_parseY(in);
      }

      /** Test whether a document has been parsed.
       */
      operator bool() const {return doc != nullptr;}

      /** Obtain the parsed document.
       * @return the xml document pointer.
       */
      xml_documentRef get_document() {return doc;}

      /** Obtain the parsed document. Const version.
       * @return the xml document pointer.
       */
      const xml_documentRef get_document() const {return doc;}

   private:
      xml_documentRef doc;

};

#endif
