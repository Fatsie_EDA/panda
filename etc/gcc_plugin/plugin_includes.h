/*
*
*                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
*                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
*                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
*                _/      _/    _/ _/    _/ _/   _/ _/    _/
*               _/      _/    _/ _/    _/ _/_/_/  _/    _/
*
*             ***********************************************
*                              PandA Project
*                     URL: http://panda.dei.polimi.it
*                       Politecnico di Milano - DEIB
*                        System Architectures Group
*             ***********************************************
*              Copyright (c) 2004-2014 Politecnico di Milano
*
*   This file is part of the PandA framework.
*
*   The PandA framework is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
* @file plugin_includes.h
* @brief Common includes for plugin
*
* @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
* @author Marco Lattuada <marco.lattuada@polimi.it>
*
*/
#ifndef PLUGIN_INCLUDES
#define PLUGIN_INCLUDES
#include "gcc-plugin.h"

#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "tree.h"
#include "intl.h"
#include "tm.h"

#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 8)
#include "tree-ssa-alias.h"
#include "internal-fn.h"
#include "is-a.h"
#include "predict.h"
#include "function.h"
#include "basic-block.h"
#include "gimple-expr.h"
#endif

#include "gimple.h"
#include "tree-pass.h"
#include "plugin-version.h"
#include "langhooks.h"
#include <string.h>
#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 8)
typedef bool (*gimple_predicate)(tree);
#define GET_TREE_CODE_NAME(code) (get_tree_code_name(code))
#define ENTRY_BLOCK_PTR ENTRY_BLOCK_PTR_FOR_FN (cfun)
#define EXIT_BLOCK_PTR EXIT_BLOCK_PTR_FOR_FN (cfun)
#include "gimple-iterator.h"
#include "tree-cfg.h"
#include "cgraph.h"
#else
#include "tree-flow.h"
#define GET_TREE_CODE_NAME(code) (tree_code_name[(int)code])
#endif
#include "tree-dump.h"
#include "tree-pass.h"
#include "basic-block.h"
#include "toplev.h"
#include "tree-inline.h"
#include "tree-iterator.h"
#include "real.h"
#include "fixed-value.h"
#include "tree-ssa-operands.h"
#if (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)) && !defined(SPARC) && !defined(ARM)
#include "c-family/c-common.h"
#else
#include "c-common.h"
#endif
#include <assert.h>

#include "VRP_data.h"

#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 7) && ! defined(SPARC) && !defined(ARM)
#include "gimple-pretty-print.h"
#include "tree-pretty-print.h"
#endif
#include "diagnostic.h"
#include "ggc.h"

#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 8)
#include "context.h"
/* Type of value ranges.  See value_range_d In tree-vrp.c for a
   description of these types.  */
enum value_range_type { VR_UNDEFINED, VR_RANGE, VR_ANTI_RANGE, VR_VARYING };
extern enum value_range_type  get_range_info (const_tree name, double_int *min,
					      double_int *max);
#endif
#include "cfgloop.h"

extern int plugin_is_GPL_compatible;

extern int serialize_state;
extern GTY((param1_is (int), param2_is (int))) splay_tree di_local_nodes_index;
extern GTY((param1_is (int), param2_is (int))) splay_tree di_local_nodes_ann;

static bool
is_alwaysTrue (void)
{
  return 1;
}
#endif
