/*
*
*                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
*                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
*                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
*                _/      _/    _/ _/    _/ _/   _/ _/    _/
*               _/      _/    _/ _/    _/ _/_/_/  _/    _/
*
*             ***********************************************
*                              PandA Project
*                     URL: http://panda.dei.polimi.it
*                       Politecnico di Milano - DEIB
*                        System Architectures Group
*             ***********************************************
*              Copyright (c) 2004-2014 Politecnico di Milano
*
*   This file is part of the PandA framework.
*
*   The PandA framework is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
* @file plugin_dumpGimpleSSA.c
* @brief Plugin to dump functions and global variables
*
* @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
* @author Marco Lattuada <marco.lattuada@polimi.it>
*
*/
#include "plugin_includes.h"

extern unsigned int gimplePssa (void);
extern void DumpGimpleInit(void);


extern int serialize_state;

#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 8)
namespace {
const pass_data pass_data_dump_GSSA =
#else
static struct
gimple_opt_pass pass_dump_GSSA =
#endif
{
#if (__GNUC__ == 4 && __GNUC_MINOR__ <= 8)
  {
#endif
    GIMPLE_PASS,
    "gimplePSSA",				/* name */
#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 8)
  OPTGROUP_NONE,                        /* optinfo_flags */
#endif
#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 8)
    true, /* has_gate */
    true, /* has_execute */
#else
    is_alwaysTrue,				/* gate */
    gimplePssa, 				/* execute */
    NULL,					/* sub */
    NULL,					/* next */
    0,						/* static_pass_number */
#endif
    TV_DUMP,					/* tv_id */
    (PROP_cfg | PROP_ssa),			/* properties_required */
    0,						/* properties_provided */
    0,						/* properties_destroyed */
    0,						/* todo_flags_start */
    (TODO_verify_ssa				/* todo_flags_finish */
    | TODO_cleanup_cfg
    | TODO_update_ssa)
#if (__GNUC__ == 4 && __GNUC_MINOR__ <= 8)
  }
#endif
};

#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 8)
class pass_dump_GSSA : public gimple_opt_pass
{
public:
  pass_dump_GSSA(gcc::context *ctxt)
    : gimple_opt_pass(pass_data_dump_GSSA, ctxt)
  {}

  /* opt_pass methods: */
  bool gate () { return true; }
  unsigned int execute () { return gimplePssa (); }

}; // class pass_dump_GSSA
	
} // anon namespace
gimple_opt_pass *
make_pass_dump_GSSA (gcc::context *ctxt)
{
  return new pass_dump_GSSA (ctxt);
}
#endif


int
plugin_init (struct plugin_name_args *plugin_info,
             struct plugin_gcc_version *version)
{
  if (!plugin_default_version_check (version, &gcc_version))
    return 1;

  struct register_pass_info pass_info;

  const char *plugin_name = plugin_info->base_name;
  int argc = plugin_info->argc;
  struct plugin_argument *argv = plugin_info->argv;

#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 8)
  pass_info.pass = make_pass_dump_GSSA(g);
#else
  pass_info.pass = &pass_dump_GSSA.pass;
#endif
  pass_info.reference_pass_name = "optimized";

  DumpGimpleInit();

  /* Do it for every instance if it is 0. */
  pass_info.ref_pass_instance_number = 1;

  /* This pass can go almost anywhere as long as you're in SSA form */
  pass_info.pos_op = PASS_POS_INSERT_AFTER;

  pass_init_dump_file (pass_info.pass);

  /* Register this new pass with GCC */
  register_callback (plugin_info->base_name, PLUGIN_PASS_MANAGER_SETUP, NULL,
                     &pass_info);
  serialize_state = -1;

  return 0;
}


