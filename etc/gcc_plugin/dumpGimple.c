/*
*
*                   _/_/_/    _/_/   _/    _/ _/_/_/    _/_/
*                  _/   _/ _/    _/ _/_/  _/ _/   _/ _/    _/
*                 _/_/_/  _/_/_/_/ _/  _/_/ _/   _/ _/_/_/_/
*                _/      _/    _/ _/    _/ _/   _/ _/    _/
*               _/      _/    _/ _/    _/ _/_/_/  _/    _/
*
*             ***********************************************
*                              PandA Project
*                     URL: http://panda.dei.polimi.it
*                       Politecnico di Milano - DEIB
*                        System Architectures Group
*             ***********************************************
*              Copyright (c) 2004-2014 Politecnico di Milano
*
*   This file is part of the PandA framework.
*
*   The PandA framework is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/
/**
* @file dumpGimple.c
* @brief Functions to print gimple and tree representations
*
* @author Fabrizio Ferrandi <fabrizio.ferrandi@polimi.it>
* @author Marco Lattuada <marco.lattuada@polimi.it>
*
*/
#include "plugin_includes.h"

/* Value range array.  */
static unsigned num_vr_values=0;
tree* VRP_min_array=0;
tree* VRP_max_array=0;
bool* p_VRP_min=0;
bool* p_VRP_max=0;

void set_num_vr_values(unsigned int n)
{
  num_vr_values = n;
}


int plugin_is_GPL_compatible;

int serialize_state;

static FILE *GimpleSSA_dump_file;

/* Information about a node to be dumped.  */
#define ZERO_OR_ONE(val) (val != 0 ? 1 : 0)
#define MAKE_ANN(binfo_val, gimple_val, statement_val) (ZERO_OR_ONE(binfo_val) | (ZERO_OR_ONE(gimple_val) << 1) | (ZERO_OR_ONE(statement_val) << 2) )
#define IS_BINFO(val) (val&1)
#define IS_GIMPLE(val) (val&2)
#define IS_STATEMENT(val) (val&4)

typedef struct same_vuse_linked_list
{
      gimple data;
      struct same_vuse_linked_list *next;
} * same_vuse_linked_list_p;

/* A serialize_queue is a link in the queue of things to be dumped.  */
typedef struct serialize_queue
{
  /* The queued tree node.  */
  splay_tree_node node_index;
  splay_tree_node node_ann;
  /* The next node in the queue.  */
  struct serialize_queue *next;
} *serialize_queue_p;

/* A SerializeInfo gives information about how we should perform the serialization
   and about the current state of the serialization.  */
struct SerializeGimpleInfo
{
  /* The stream on which to dump the information.  */
  FILE *stream;
  /* The original node.  */
  const_tree node;
  /* The next column.  */
  unsigned int column;
  /* The first node in the queue of nodes to be written out.  */
  serialize_queue_p queue;
  /* The last node in the queue.  */
  serialize_queue_p queue_end;
  /* Free queue nodes.  */
  serialize_queue_p free_list;
  /* list of gimple nodes*/
  serialize_queue_p gimple_list;
  /* list of cfg nodes*/
  serialize_queue_p cfg_list;
};

/** The current state of the gimple serialization */
struct SerializeGimpleInfo serialize_gimple_info;

/* The next unused node index.  */
static GTY(()) unsigned int di_local_index;

/* The tree nodes which we have already written out.  The
     keys are the addresses of the nodes; the values are the integer
     indices we assigned them.  */
splay_tree di_local_nodes_index;
splay_tree di_local_nodes_ann;
#if (__GNUC__ == 4 && __GNUC_MINOR__ == 9)
#include "gtype_roots_gcc49.h"
#endif
#if (__GNUC__ == 4 && __GNUC_MINOR__ == 8)
#include "gtype_roots_gcc48.h"
#endif
#if (__GNUC__ == 4 && __GNUC_MINOR__ == 7)
#include "gtype_roots_gcc47.h"
#endif
#if (__GNUC__ == 4 && __GNUC_MINOR__ == 6)
#include "gtype_roots_gcc46.h"
#endif

/// map putting in relation vdef index with the associated gimple statement that used it
splay_tree di_local_vdef_vuse_map;

/* Serialize the CHILD and its children.  */
#define serialize_child(field, child) \
  queue_and_serialize_index (field, child, DUMP_NONE)
/* Serialize the GIMPLE CHILD and its children.  */
#define serialize_gimple_child(field, child) \
  queue_and_serialize_gimple_index (field, child, DUMP_NONE)
/* Serialize the STATEMENT CHILD and its children.  */
#define serialize_statement_child(field, child) \
  queue_and_serialize_statement_index (field, child, DUMP_NONE)

#define LOCATION_COLUMN(LOC) ((expand_location (LOC)).column)
#define EXPR_COLUMNNO(NODE) LOCATION_COLUMN (EXPR_CHECK (NODE)->exp.locus)


static unsigned int queue (const_tree, int);
static unsigned int queue_gimple ( gimple t, int flags);
static unsigned int queue_statement (struct control_flow_graph * cfg, int flags);
static void serialize_index (unsigned int);
static void queue_and_serialize_index (const char *field, const_tree t, int);
static void queue_and_serialize_gimple_index (const char *field, gimple t, int);
static void queue_and_serialize_statement_index (const char *field, struct control_flow_graph * cfg, int);
static void queue_and_serialize_type (const_tree t);
static void dequeue_and_serialize ();
static void dequeue_and_serialize_gimple ();
static void dequeue_and_serialize_statement ();
static void serialize_new_line ();
static void serialize_maybe_newline ();
static void serialize_pointer (const char *field, void *ptr);
static void serialize_int (const char *field, int i);
static void serialize_wide_int (const char *field, HOST_WIDE_INT i);
static void serialize_real (tree t);
static int serialize_with_double_quote(const char * input, FILE * output, int length);
static int serialize_with_escape(const char * input, FILE * output, int length);
static void serialize_fixed (const char *field, const FIXED_VALUE_TYPE *f);
static void serialize_string (const char *string);
static void serialize_string_field (const char *field, const char *str);
static void serialize_gimple_dependent_stmts_load(gimple gs);
static void serialize_gimple_dependent_stmts_store(gimple gs);
void compute_vuse_vdef_map(tree t);


/**
 * Initialize structure to perform gimple serialization:
 * - open gimple serialization file; if it already exists open in append mode
 * - initialize global splay tree node if they have not yet been initialized
 * @param file_name is the name of the file to be written
 */
void SerializeGimpleBegin(char * file_name);

/**
 * Close gimple serialization file and remove gimples from splay tree tables
 */
void SerializeGimpleEnd(void);

/**
 * Serialize the header of a function declaration
 * @param fn is the function decl
 */
static void SerializeGimpleFunctionHeader(tree fn);

/**
 * Serialize a global symbol (function_decl or global var_decl
 * @param t is the symbol to be serialized
 */
void SerializeGimpleGlobalTreeNode(tree t);

static void serialize_globals()
{
   struct varpool_node *vnode;

#if (__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 8)
   FOR_EACH_DEFINED_VARIABLE (vnode)
   {
#if (__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 9)
      tree var = vnode->decl;
#else
      tree var = vnode->symbol.decl;
#endif
#else
   FOR_EACH_STATIC_VARIABLE (vnode)
   {
      tree var = vnode->decl;
#endif

      if (TREE_CODE (var) == VAR_DECL && DECL_FILE_SCOPE_P(var))
      {
         SerializeGimpleGlobalTreeNode(var);
      }
   }

}

#if (__GNUC__ == 4 && __GNUC_MINOR__ >= 6) && !defined(SPARC) && (__GNUC__ == 4 && __GNUC_MINOR__ < 8)
static void serialize_symbols (const char * field, bitmap syms);
#endif
static void serialize_vops (gimple stmt);
static tree gimple_call_expr_arglist (gimple g);
static tree build_custom_function_call_expr (location_t loc, tree fn, tree arglist);

static int
free_vdef_vuse_map_element (splay_tree_node node, void *data)
{
   same_vuse_linked_list_p next_dq, dq;
   for (dq = (same_vuse_linked_list_p) node->value; dq; dq = next_dq)
   {
      next_dq = dq->next;
      free (dq);
   }
  return 0;
}

static void deallocate_vdef_vuse_map()
{
   if(di_local_vdef_vuse_map)
   {
     splay_tree_foreach (di_local_vdef_vuse_map, free_vdef_vuse_map_element,0);
     splay_tree_delete (di_local_vdef_vuse_map);
     di_local_vdef_vuse_map = NULL;

   }
}

static void add_gimple_to_vdef_vuse_map(gimple gs)
{
   tree vuse = gimple_vuse(gs);
   if(vuse)
   {
      int ssa_index = SSA_NAME_VERSION(vuse);
      same_vuse_linked_list_p ll_head;
      splay_tree_node n;
      n = splay_tree_lookup(di_local_vdef_vuse_map, (splay_tree_key) ssa_index);
      ll_head = XNEW (struct same_vuse_linked_list);
      ll_head->data = gs;
      if(n)
      {
         ll_head->next = (same_vuse_linked_list_p)n->value;
         n->value= (splay_tree_value) ll_head;
      }
      else
      {
         ll_head->next = 0;
         splay_tree_insert (di_local_vdef_vuse_map, (splay_tree_key) ssa_index, (splay_tree_value) ll_head);
      }
   }
}

static void
DumpGimpleWalker(void *arg ATTRIBUTE_UNUSED)
{
  if(di_local_nodes_index) ggc_mark(di_local_nodes_index);
  if(di_local_nodes_ann) ggc_mark(di_local_nodes_ann);
}

static struct ggc_root_tab DumpGimpleRoot = { (char*)"", 1, 1, DumpGimpleWalker, NULL };

/* Mark the NODE into the splay tree given by DATA.  Used by
   DumpGimpleTreeWalker via splay_tree_foreach.  */

static int
mark_tree_element (splay_tree_node node, void *data)
{
  ggc_mark((tree)node->key);

  return 0;
}

static void
DumpGimpleTreeWalker(void *arg ATTRIBUTE_UNUSED)
{
  if(di_local_nodes_index)
  {
    splay_tree_foreach (di_local_nodes_index, mark_tree_element,0);
  }
}

static struct ggc_root_tab DumpGimpleTreeRoot = { (char*)"", 1, 1, DumpGimpleTreeWalker, NULL };

void
DumpGimpleInit(void)
{
    /* Register our GC root-walking callback: */
#if (__GNUC__ == 4 && __GNUC_MINOR__ == 9)
    ggc_register_root_tab(gt_ggc_r_gtype_roots_gcc49_h);
#elif (__GNUC__ == 4 && __GNUC_MINOR__ == 8)
    ggc_register_root_tab(gt_ggc_r_gtype_roots_gcc48_h);
#elif (__GNUC__ == 4 && __GNUC_MINOR__ == 7)
    ggc_register_root_tab(gt_ggc_r_gtype_roots_gcc47_h);
#elif (__GNUC__ == 4 && __GNUC_MINOR__ == 6)
    ggc_register_root_tab(gt_ggc_r_gtype_roots_gcc46_h);
#else
    ggc_register_root_tab(&DumpGimpleRoot);
#endif
    ggc_register_root_tab(&DumpGimpleTreeRoot);
    di_local_index = 0;
    di_local_nodes_index = 0;
    di_local_nodes_ann = 0;
    di_local_vdef_vuse_map = 0;
}


/**
 * Dump the current version of gcc + current version of plugin
 */
static
void DumpVersion(FILE * stream)
{
   char * panda_plugin_version = (char *) PANDA_PLUGIN_VERSION;
   int version = __GNUC__, minor = __GNUC_MINOR__, patchlevel = __GNUC_PATCHLEVEL__;
   fprintf(stream, "GCC_VERSION: \"%d.%d.%d\"\n", version, minor, patchlevel);
   fprintf(stream, "PLUGIN_VERSION: \"%s\"\n", panda_plugin_version);
}

/**
 * Initialize structure to perform gimple serialization:
 * - open gimple serialization file; if it already exists open in append mode
 * - initialize global splay tree node if they have not yet been initialized
 * @param file_name is the name of the file to be written
 */
void
SerializeGimpleBegin(char * name)
{
   serialize_gimple_info.stream = fopen (name, serialize_state < 0 ? "w" : "a");
   serialize_gimple_info.node = 0;
   serialize_gimple_info.column = 0;
   serialize_gimple_info.queue = 0;
   serialize_gimple_info.queue_end = 0;
   serialize_gimple_info.free_list = 0;
   serialize_gimple_info.gimple_list = 0;
   serialize_gimple_info.cfg_list = 0;
   if (serialize_gimple_info.stream && serialize_state < 0)
   {
      DumpVersion(serialize_gimple_info.stream);
   }
   if (!serialize_gimple_info.stream)
      error ("could not open serialize file %qs: %m", name);
   else
      serialize_state = 1;
   free (name);
   /* initialization of di_local */
   if (!di_local_nodes_index)
   {
#if (__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)
      di_local_nodes_index = splay_tree_new_ggc (splay_tree_compare_pointers,ggc_alloc_splay_tree_scalar_scalar_splay_tree_s,ggc_alloc_splay_tree_scalar_scalar_splay_tree_node_s);
      di_local_nodes_ann = splay_tree_new_ggc (splay_tree_compare_pointers,ggc_alloc_splay_tree_scalar_scalar_splay_tree_s,ggc_alloc_splay_tree_scalar_scalar_splay_tree_node_s);
#else
      di_local_nodes_index = splay_tree_new_ggc (splay_tree_compare_pointers);
      di_local_nodes_ann = splay_tree_new_ggc (splay_tree_compare_pointers);
#endif
   }
   gcc_assert(!di_local_vdef_vuse_map);
   di_local_vdef_vuse_map = splay_tree_new (splay_tree_compare_ints, 0, 0);
}

/**
 * Close gimple serialization file and remove gimples from splay tree tables
 */
void
SerializeGimpleEnd()
{
   /* Pointer to the current element of the queue */
   serialize_queue_p dq;

   /* Pointer to the next element of the queue */
   serialize_queue_p next_dq;

   /* Now, clean up.  */
   for (dq = serialize_gimple_info.free_list; dq; dq = next_dq)
   {
      next_dq = dq->next;
      free (dq);
   }
   /* Now remove gimple objects. They could not be used anymore */
   for(dq = serialize_gimple_info.gimple_list; dq; dq = next_dq)
   {
      next_dq = dq->next;
      splay_tree_remove(di_local_nodes_index, dq->node_index->key);
      splay_tree_remove(di_local_nodes_ann, dq->node_ann->key);
      free (dq);
   }
   /* Now remove cfg objects. They could not be used anymore */
   for(dq = serialize_gimple_info.cfg_list; dq; dq = next_dq)
   {
      next_dq = dq->next;
      splay_tree_remove(di_local_nodes_index, dq->node_index->key);
      splay_tree_remove(di_local_nodes_ann, dq->node_ann->key);
      free (dq);
   }
   /*splay_tree_delete (di.nodes);*/
   fclose (serialize_gimple_info.stream);
   deallocate_vdef_vuse_map();
}

unsigned int
gimplePssa (void)
{
   /* dummy pass */
   if (!(cfun->curr_properties & PROP_cfg))
      fatal_error("PROP_cfg marked in the pass descriptor but not found");
   if (!(cfun->curr_properties & PROP_ssa))
      fatal_error("PROP_ssa marked in the pass descriptor but not found");
#if (__GNUC__ >= 5 || (__GNUC__ == 4 && __GNUC_MINOR__ > 8))
   if(cfun->cfg && basic_block_info_for_fn(cfun))
#else
   if(cfun->cfg && basic_block_info)
#endif
   {
      char * name = concat (dump_base_name, ".gimplePSSA", NULL);
      bool has_loop = current_loops != NULL;
      if(!has_loop)
         loop_optimizer_init (AVOID_CFG_MODIFICATIONS);
      SerializeGimpleBegin(name);
      compute_vuse_vdef_map(cfun->decl);
      serialize_globals();
      SerializeGimpleGlobalTreeNode(cfun->decl);
      SerializeGimpleEnd();
      if(!has_loop)
         loop_optimizer_finalize ();
    }

 return 0;
}


#if (__GNUC__ == 4 && __GNUC_MINOR__ >= 6) && !defined(SPARC) && !defined(ARM) && (__GNUC__ == 4 && __GNUC_MINOR__ < 8)
static
void dump_pt_solution(struct pt_solution *pt, const char * first_tag, const char* second_tag)
{
    if (pt->anything)
        serialize_string_field (first_tag, "anything");
    if (pt->nonlocal)
        serialize_string_field (first_tag, "nonlocal");
    if (pt->escaped)
        serialize_string_field (first_tag, "escaped");
    if (pt->ipa_escaped)
        serialize_string_field (first_tag, "ipa_escaped");
    if (pt->null)
        serialize_string_field (first_tag, "null");
    if (pt->vars && !bitmap_empty_p (pt->vars))
        serialize_symbols(second_tag, pt->vars);
}
#endif

/* Add T to the end of the queue of nodes to serialize.  Returns the index
   assigned to T.  */
static unsigned int
queue (const_tree t, int flags)
{
  serialize_queue_p dq;
  unsigned int index;

  /* Assign the next available index to T.  */
  index = ++di_local_index;

  /* Obtain a new queue node.  */
  if (serialize_gimple_info.free_list)
    {
      dq = serialize_gimple_info.free_list;
      serialize_gimple_info.free_list = dq->next;
    }
  else
    dq = XNEW (struct serialize_queue);

  /* Create a new entry in the splay-tree.  */
  dq->node_index = splay_tree_insert (di_local_nodes_index, (splay_tree_key) t, (splay_tree_value) index);
  dq->node_ann = splay_tree_insert (di_local_nodes_ann, (splay_tree_key) t, (splay_tree_value) MAKE_ANN((flags & DUMP_BINFO) != 0, 0, 0));

  /* Add it to the end of the queue.  */
  dq->next = 0;
  if (!serialize_gimple_info.queue_end)
    serialize_gimple_info.queue = dq;
  else
    serialize_gimple_info.queue_end->next = dq;
  serialize_gimple_info.queue_end = dq;

  /* Return the index.  */
  return index;
}

/* Add G to the end of the queue of nodes to serialize.  Returns the index
   assigned to G.  */
static unsigned int
queue_gimple (gimple g, int flags)
{
  serialize_queue_p dq, dq_gimple;
  unsigned int index;

  assert (!splay_tree_lookup (di_local_nodes_index, (splay_tree_key) g));
  assert (!splay_tree_lookup (di_local_nodes_ann, (splay_tree_key) g));
  
  /* Assign the next available index to G.  */
  index = ++di_local_index;

  /* Obtain a new queue node.  */
  if (serialize_gimple_info.free_list)
    {
      dq = serialize_gimple_info.free_list;
      serialize_gimple_info.free_list = dq->next;
    }
  else
    dq = XNEW (struct serialize_queue);

  /* obtain another one for the gimple list */
  if (serialize_gimple_info.free_list)
    {
      dq_gimple = serialize_gimple_info.free_list;
      serialize_gimple_info.free_list = dq_gimple->next;
    }
  else
    dq_gimple = XNEW (struct serialize_queue);

  /* Create a new entry in the splay-tree.  */
  dq_gimple->node_index = dq->node_index = splay_tree_insert (di_local_nodes_index, (splay_tree_key) g,
				(splay_tree_value) index);

  dq_gimple->node_ann = dq->node_ann = splay_tree_insert (di_local_nodes_ann, (splay_tree_key) g,
				(splay_tree_value) MAKE_ANN(((flags & DUMP_BINFO) != 0), 1, 0));
  /* Add it to the end of the queue.  */
  dq->next = 0;
  if (!serialize_gimple_info.queue_end)
    serialize_gimple_info.queue = dq;
  else
    serialize_gimple_info.queue_end->next = dq;
  serialize_gimple_info.queue_end = dq;

  /* Add in front of the gimple list.  */
  dq_gimple->next = serialize_gimple_info.gimple_list;
  serialize_gimple_info.gimple_list = dq_gimple;

  /* Return the index.  */
  return index;
}

/* Add CFG to the end of the queue of nodes to serialize.  Returns the index
   assigned to CFG.  */
static unsigned int
queue_statement (struct control_flow_graph * cfg, int flags)
{
  serialize_queue_p dq, dq_cfg;
  unsigned int index;

  assert (!splay_tree_lookup (di_local_nodes_index, (splay_tree_key) cfg));
  assert (!splay_tree_lookup (di_local_nodes_ann, (splay_tree_key) cfg));
  /* Assign the next available index to G.  */
  index = ++di_local_index;

  /* Obtain a new queue node.  */
  if (serialize_gimple_info.free_list)
    {
      dq = serialize_gimple_info.free_list;
      serialize_gimple_info.free_list = dq->next;
    }
  else
    dq = XNEW (struct serialize_queue);

  /* obtain another one for the cfg list */
  if (serialize_gimple_info.free_list)
    {
      dq_cfg = serialize_gimple_info.free_list;
      serialize_gimple_info.free_list = dq_cfg->next;
    }
  else
    dq_cfg = XNEW (struct serialize_queue);

  /* Create a new entry in the splay-tree.  */
  dq_cfg->node_index = dq->node_index = splay_tree_insert (di_local_nodes_index, (splay_tree_key) cfg,
				(splay_tree_value) index);
  dq_cfg->node_ann = dq->node_ann = splay_tree_insert (di_local_nodes_ann, (splay_tree_key) cfg,
				(splay_tree_value) MAKE_ANN(((flags & DUMP_BINFO) != 0),0,1));

  /* Add it to the end of the queue.  */
  dq->next = 0;
  if (!serialize_gimple_info.queue_end)
    serialize_gimple_info.queue = dq;
  else
    serialize_gimple_info.queue_end->next = dq;
  serialize_gimple_info.queue_end = dq;

  /* Add in front of the gimple list.  */
  dq_cfg->next = serialize_gimple_info.cfg_list;
  serialize_gimple_info.cfg_list = dq_cfg;

  /* Return the index.  */
  return index;
}

static void
serialize_index (unsigned int index)
{
  fprintf (serialize_gimple_info.stream, "@%-6u ", index);
  serialize_gimple_info.column += 8;
}

/* If T has not already been output, queue it for subsequent output.
   FIELD is a string to print before printing the index.  Then, the
   index of T is printed.  */
static void
queue_and_serialize_index (const char *field, const_tree t, int flags)
{
  unsigned int index;
  splay_tree_node n;

  /* If there's no node, just return.  This makes for fewer checks in
     our callers.  */
  if (!t)
    return;

  /* See if we've already queued or dumped this node.  */
  n = splay_tree_lookup(di_local_nodes_index, (splay_tree_key) t);
  if (n)
  {
     index = (int) n->value;
  }
  else
  {
    /* If we haven't, add it to the queue.  */
    index = queue (t, flags);
  }

  /* Print the index of the node.  */
  serialize_maybe_newline ();
  fprintf (serialize_gimple_info.stream, "%-4s: ", field);
  serialize_gimple_info.column += 6;
  serialize_index (index);
}

/* If G has not already been output, queue it for subsequent output.
   FIELD is a string to print before printing the index.  Then, the
   index of G is printed.  */
static void
queue_and_serialize_gimple_index (const char *field, gimple g, int flags)
{
  unsigned int index;
  splay_tree_node n;

  /* If there's no node, just return.  This makes for fewer checks in
     our callers.  */
  if (!g)
    return;

  /* See if we've already queued or dumped this node.  */
  n = splay_tree_lookup(di_local_nodes_index, (splay_tree_key) g);
  if (n)
  {
     index = (int) n->value;
  }
  else
  {
    /* If we haven't, add it to the queue.  */
    index = queue_gimple (g, flags);
  }

  /* Print the index of the node.  */
  serialize_maybe_newline ();
  fprintf (serialize_gimple_info.stream, "%-4s: ", field);
  serialize_gimple_info.column += 6;
  serialize_index (index);
}

/* If T has not already been output, queue it for subsequent output.
   FIELD is a string to print before printing the index.  Then, the
   index of T is printed.  */
static void
queue_and_serialize_statement_index (const char *field, struct control_flow_graph * cfg, int flags)
{
  unsigned int index;
  splay_tree_node n;

  /* If there's no node, just return.  This makes for fewer checks in
     our callers.  */
  if (!cfg)
    return;

  /* See if we've already queued or dumped this node.  */
  n = splay_tree_lookup(di_local_nodes_index, (splay_tree_key) cfg);
  if (n)
  {
     index = (int) n->value;
  }
  else
  {
    /* If we haven't, add it to the queue.  */
    index = queue_statement (cfg, flags);
  }

  /* Print the index of the node.  */
  serialize_maybe_newline ();
  fprintf (serialize_gimple_info.stream, "%-4s: ", field);
  serialize_gimple_info.column += 6;
  serialize_index (index);
}

/* Serialize the type of T.  */
static void
queue_and_serialize_type (const_tree t)
{
  queue_and_serialize_index ("type", TREE_TYPE (t), DUMP_NONE);
}

/* Serialize column control */
#define SOL_COLUMN 25		/* Start of line column.  */
#define EOL_COLUMN 55		/* End of line column.  */
#define COLUMN_ALIGNMENT 15	/* Alignment.  */

/* Insert a new line in the serialize output, and indent to an appropriate
   place to start printing more fields.  */

static void
serialize_new_line ()
{
  fprintf (serialize_gimple_info.stream, "\n%*s", SOL_COLUMN, "");
  serialize_gimple_info.column = SOL_COLUMN;
}

/* If necessary, insert a new line.  */
static void
serialize_maybe_newline ()
{
  int extra;

  /* See if we need a new line.  */
  if (serialize_gimple_info.column > EOL_COLUMN)
    serialize_new_line ();
  /* See if we need any padding.  */
  else if ((extra = (serialize_gimple_info.column - SOL_COLUMN) % COLUMN_ALIGNMENT) != 0)
    {
      fprintf (serialize_gimple_info.stream, "%*s", COLUMN_ALIGNMENT - extra, "");
      serialize_gimple_info.column += COLUMN_ALIGNMENT - extra;
    }
}

/* Serialize pointer PTR using FIELD to identify it.  */
static void
serialize_pointer (const char *field, void *ptr)
{
  serialize_maybe_newline ();
  fprintf (serialize_gimple_info.stream, "%-4s: %-8lx ", field, (unsigned long) ptr);
  serialize_gimple_info.column += 15;
}

/* Serialize integer I using FIELD to identify it.  */
static void
serialize_int (const char *field, int i)
{
  serialize_maybe_newline ();
  fprintf (serialize_gimple_info.stream, "%-4s: %-7d ", field, i);
  serialize_gimple_info.column += 14;
}

/* Serialize wide integer i using FIELD to identify it.  */
static void
serialize_wide_int (const char *field, HOST_WIDE_INT i)
{
   serialize_maybe_newline ();
   fprintf (serialize_gimple_info.stream, "%-4s: "HOST_WIDE_INT_PRINT_DEC, field, i);
   serialize_gimple_info.column += 21;
}

/* Serialize real r using FIELD to identify it.  */
static void
serialize_real (tree t)
{
#if !defined(REAL_IS_NOT_DOUBLE) || defined(REAL_ARITHMETIC)
   REAL_VALUE_TYPE d;
#endif
   static char string[1024];
   serialize_maybe_newline ();
   /* Code copied from print_node.  */
   if (TREE_OVERFLOW (t))
   {
      fprintf (serialize_gimple_info.stream, "%s ", "overflow");
      serialize_gimple_info.column += 8;
   }
#if !defined(REAL_IS_NOT_DOUBLE) || defined(REAL_ARITHMETIC)
   d = TREE_REAL_CST (t);
   if (REAL_VALUE_ISINF (d))
      fprintf (serialize_gimple_info.stream, "valr: %-7s ", "\"Inf\"");
   else if (REAL_VALUE_ISNAN (d))
      fprintf (serialize_gimple_info.stream, "valr: %-7s ", "\"Nan\"");
   else
   {
      real_to_decimal (string, &d, sizeof (string), 0, 1);
      fprintf (serialize_gimple_info.stream, "valr: \"%s\" ", string);
   }
#endif
   {
      fprintf (serialize_gimple_info.stream, "valx: \"");
      real_to_hexadecimal(string, &d, sizeof (string), 0, 0);
      fprintf (serialize_gimple_info.stream, "%s", string);
      fprintf (serialize_gimple_info.stream, "\"");
   }
   serialize_gimple_info.column += 21;
}

/* Serialize a string and add double quotes */
static int
serialize_with_double_quote(const char * input, FILE * output, int length)
{
   int new_length;
   fprintf(output, "\"");
   new_length = serialize_with_escape(input, output, length);
   fprintf(output, "\"");
   return new_length + 2;
}


/* Add a backslash before an escape sequence to serialize the string
   with the escape sequence */
static int
serialize_with_escape(const char * input, FILE * output, int length)
{
   int i;
   int k = 0;
   for (i = 0; i < length; i++)
   {
      switch (input[i])
      {
         case '\n':
         {
            /* new line*/
            fprintf(output, "\\");
            fprintf(output, "n");
            k += 2;
            break;
         }
         case '\t':
         {
            /* horizontal tab */
            fprintf(output, "\\");
            fprintf(output, "t");
            k += 2;
            break;
         }
         case '\v':
         {
            /* vertical tab */
            fprintf(output, "\\");
            fprintf(output, "v");
            k += 2;
            break;
         }
         case '\b':
         {
            /* backspace */
            fprintf(output, "\\");
            fprintf(output, "b");
            k += 2;
            break;
         }
         case '\r':
         {
            /* carriage return */
            fprintf(output, "\\");
            fprintf(output, "r");
            k += 2;
            break;
         }
         case '\f':
         {
            /* jump page */
            fprintf(output, "\\");
            fprintf(output, "f");
            k += 2;
            break;
         }
         case '\a':
         {
            /* alarm */
            fprintf(output, "\\");
            fprintf(output, "a");
            k += 2;
            break;
         }
         case '\\':
         {
            /* backslash */
            fprintf(output, "\\");
            fprintf(output, "\\");
            k += 2;
            break;
         }
         case '\"':
         {
            /* double quote */
            fprintf(output, "\\");
            fprintf(output, "\"");
            k += 2;
            break;
         }
         case '\'':
         {
            /* quote */
            fprintf(output, "\\");
            fprintf(output, "\'");
            k += 2;
            break;
         }
         case '\0':
         {
            /* null */
            fprintf(output, "\\");
            fprintf(output, "0");
            k += 2;
            break;
         }
         default:
         {
            fprintf(output, "%c", input[i]);
            k++;
         }
      }
   }
   return k;
}

/* Serialize the fixed-point value F, using FIELD to identify it.  */
static void
serialize_fixed (const char *field, const FIXED_VALUE_TYPE *f)
{
  char buf[32];
  fixed_to_decimal (buf, f, sizeof (buf));
  serialize_maybe_newline ();
  fprintf (serialize_gimple_info.stream, "%-4s: %s ", field, buf);
  serialize_gimple_info.column += strlen (buf) + 7;
}

/* Serialize the string S.  */
static void
serialize_string (const char *string)
{
  serialize_maybe_newline ();
  fprintf (serialize_gimple_info.stream, "%-13s ", string);
  if (strlen (string) > 13)
    serialize_gimple_info.column += strlen (string) + 1;
  else
    serialize_gimple_info.column += 14;
}

/* Serialize the string field S.  */
static void
serialize_string_field (const char *field, const char *str)
{
   int length;
   serialize_maybe_newline ();
   fprintf (serialize_gimple_info.stream, "%-4s: ", field);
   length = strlen(str);
   length = serialize_with_double_quote(str, serialize_gimple_info.stream, length);
   if (length > 7)
      serialize_gimple_info.column += 6 + length + 1;
   else
      serialize_gimple_info.column += 14;
}

#if (__GNUC__ == 4 && __GNUC_MINOR__ >= 6) && !defined(SPARC) && (__GNUC__ == 4 && __GNUC_MINOR__ < 8)
/* Serialize symbols */
static void
serialize_symbols (const char * field, bitmap syms)
{
   unsigned i;
   bitmap_iterator bi;

   if (syms == NULL)
      serialize_string_field (field, "NIL");
   else
   {

      EXECUTE_IF_SET_IN_BITMAP (syms, 0, i, bi)
      {
#if (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)) && !defined(SPARC) && !defined(ARM)
         tree sym = referenced_var_lookup (cfun, i);
#else
         tree sym = referenced_var_lookup (i);
#endif
         serialize_child (field, sym);
      }
   }
}
#endif
/* Serialize virtual operands */
static void
serialize_vops (gimple gs)
{

   tree vdef = gimple_vdef (gs);
   tree vuse = gimple_vuse (gs);

#if (__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 8)
   if (!ssa_operands_active (DECL_STRUCT_FUNCTION (current_function_decl)) || !gimple_references_memory_p (gs))
      return;
#else
   if (!ssa_operands_active () || !gimple_references_memory_p (gs))
      return;
#endif
   if (vuse != NULL_TREE)
   {
      serialize_child ("vuse", vuse);
   }
   if (vdef != NULL_TREE)
   {
      serialize_child ("vdef", vdef);
   }

   /// check for true dependencies by exploiting GCC alias analysis infrastructure
   if(vdef != NULL_TREE)
   {
      if(vuse != NULL_TREE)
         serialize_gimple_dependent_stmts_store(gs);//
         //serialize_child ("vdep_vuse", vuse);
   }
   else if (vuse != NULL_TREE)
   {
      serialize_gimple_dependent_stmts_load(gs);
   }

}

static void
serialize_string_cst (const char *field, const char *str, int length, unsigned int precision)
{
   int new_length;
   serialize_maybe_newline ();
   if (precision == 8)
   {
      fprintf (serialize_gimple_info.stream, "%-4s: ", field);
      new_length = serialize_with_double_quote(str, serialize_gimple_info.stream, length - 1);
      if (new_length > 7)
         serialize_gimple_info.column += 6 + new_length + 1;
  else
    serialize_gimple_info.column += 14;
      serialize_int ("lngt", length);
   }
   else
   {
      const unsigned int * string = (const unsigned int *) str;
      unsigned int i, lngt = length / 4 - 1;
      fprintf (serialize_gimple_info.stream, "%-4s: \"", field);
      for (i = 0; i < lngt; i++)
         fprintf (serialize_gimple_info.stream, "\\x%x", string[i]);
      fprintf (serialize_gimple_info.stream, "\" ");
      serialize_gimple_info.column += 7 + lngt;
      serialize_int ("lngt", lngt + 1);
   }
}


/* Serialize the next node in the queue.  */
static void
dequeue_and_serialize ()
{
  serialize_queue_p dq;
  splay_tree_node stn_index;
  splay_tree_node stn_ann;
  tree t, arg;
  unsigned int index;
  enum tree_code code;
  enum tree_code_class code_class;
  const char* code_name;
  int ann;

  /* Get the next node from the queue.  */
  dq = serialize_gimple_info.queue;
  stn_index = dq->node_index;
  stn_ann = dq->node_ann;
  index = (int) stn_index->value;
  ann = (int) stn_ann->value;
  if(IS_GIMPLE(ann))
  {
    dequeue_and_serialize_gimple ();
    return;
  }
  else if(IS_STATEMENT(ann))
  {
    dequeue_and_serialize_statement ();
    return;
  }

  t = (tree) stn_index->key;

  /* Remove the node from the queue, and put it on the free list.  */
  serialize_gimple_info.queue = dq->next;
  if (!serialize_gimple_info.queue)
    serialize_gimple_info.queue_end = 0;
  dq->next = serialize_gimple_info.free_list;
  serialize_gimple_info.free_list = dq;

  /* Print the node index.  */
  serialize_index (index);

  /* And the type of node this is.  */
  if (IS_BINFO(ann))
    code_name = "binfo";
  else if(TREE_CODE (t) == TARGET_MEM_REF)
    code_name = "target_mem_ref461";
#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 7) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6 && __GNUC_PATCHLEVEL__>= 1)
  else if(TREE_CODE (t) == BIT_NOT_EXPR && TREE_CODE (TREE_TYPE (t)) == BOOLEAN_TYPE) /*incorrect encoding of not of a boolean expression*/
    code_name ="truth_not_expr";
#endif
  else
    code_name = GET_TREE_CODE_NAME(TREE_CODE (t));
  fprintf (serialize_gimple_info.stream, "%-16s ", code_name);
  serialize_gimple_info.column = 25;

  /* Figure out what kind of node this is.  */
  code = TREE_CODE (t);
  code_class = TREE_CODE_CLASS (code);

   /* We use has_stmt_ann because CALL_EXPR can be both an expression
      and a statement, and we have no guarantee that it will have a
      stmt_ann when it is used as an RHS expression.  stmt_ann will assert
      if you call it on something with a non-stmt annotation attached.  */

  // if (code != ERROR_MARK
  //       && is_gimple_stmt (t)
  //       && has_stmt_ann (t)
  //       && code != PHI_NODE)
  // {
  //    serialize_vops(t);
  // }

  /* Although BINFOs are TREE_VECs, we serialize them specially so as to be
     more informative.  */
  if (IS_BINFO(ann))
  {
    unsigned ix;
    tree base;
#if (__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 8)
    vec<tree,va_gc> *accesses = BINFO_BASE_ACCESSES (t);
#else
    VEC(tree,gc) *accesses = BINFO_BASE_ACCESSES (t);
#endif
    serialize_child ("type", BINFO_TYPE (t));

    if (BINFO_VIRTUAL_P (t))
      serialize_string ("virt");

    serialize_int ("bases", BINFO_N_BASE_BINFOS (t));
    for (ix = 0; BINFO_BASE_ITERATE (t, ix, base); ix++)
    {
#if (__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 8)
      tree access = (accesses ? (*accesses)[ix] : access_public_node);
#else
      tree access = (accesses ? VEC_index (tree, accesses, ix) : access_public_node);
#endif
      const char *string = NULL;

      if (access == access_public_node)
        string = "pub";
      else if (access == access_protected_node)
        string = "prot";
      else if (access == access_private_node)
        string = "priv";
      else
        gcc_unreachable ();

      serialize_string (string);
      queue_and_serialize_index ("binf", base, DUMP_BINFO);
    }

    goto done;
  }

  /* We can knock off a bunch of expression nodes in exactly the same
     way.  */
  if (IS_EXPR_CODE_CLASS (code_class))
  {
    expanded_location xloc;
    /* If we're serializing children, serialize them now.  */
    if (!(code == MODIFY_EXPR))
      queue_and_serialize_type (t);
      if (EXPR_HAS_LOCATION(t))
      {
        serialize_maybe_newline ();
        fprintf (serialize_gimple_info.stream, "srcp: \"%s\":%-d:%-6d ", EXPR_FILENAME(t), EXPR_LINENO(t), EXPR_COLUMNNO(t));
        serialize_gimple_info.column += 12 + strlen(EXPR_FILENAME(t)) + 8;
     }

     switch (code_class)
     {
       case tcc_unary:
         serialize_child ("op", TREE_OPERAND (t, 0));
         break;

       case tcc_binary:
       case tcc_comparison:
         serialize_child ("op", TREE_OPERAND (t, 0));
         serialize_child ("op", TREE_OPERAND (t, 1));
         break;

       case tcc_expression:
       case tcc_reference:
       case tcc_statement:
       case tcc_vl_exp:
         /* These nodes are handled explicitly below.  */
         break;

       default:
         gcc_unreachable ();
    }
  }
  else if (DECL_P (t))
  {
    expanded_location xloc;
    /* All declarations have names.  */
    if (DECL_NAME (t))
      serialize_child ("name", DECL_NAME (t));
    if (DECL_ASSEMBLER_NAME_SET_P (t) && DECL_ASSEMBLER_NAME (t) != DECL_NAME (t))
      serialize_child ("mngl", DECL_ASSEMBLER_NAME (t));
    if (DECL_ABSTRACT_ORIGIN (t))
      serialize_child ("orig", DECL_ABSTRACT_ORIGIN (t));
    /* And types.  */
    queue_and_serialize_type (t);
    serialize_child ("scpe", DECL_CONTEXT (t));
    if (!DECL_SOURCE_LOCATION (t))
    {
      serialize_maybe_newline ();
      fprintf (serialize_gimple_info.stream, "srcp: \"<built-in>\":0:0 ");
      serialize_gimple_info.column += 12 + strlen ("<built-in>") + 8;
    }
    else
    {
       /* And a source position.  */
       xloc = expand_location (DECL_SOURCE_LOCATION (t));
       if (xloc.file)
       {
          serialize_maybe_newline ();
          fprintf (serialize_gimple_info.stream, "srcp: \"%s\":%-d:%-6d ", xloc.file,
                xloc.line, xloc.column);
          serialize_gimple_info.column += 12 + strlen (xloc.file) + 8;
       }
    }
    /* And any declaration can be compiler-generated.  */
    if (CODE_CONTAINS_STRUCT (TREE_CODE (t), TS_DECL_COMMON) && DECL_ARTIFICIAL (t))
       serialize_string ("artificial");
    if (code == LABEL_DECL && LABEL_DECL_UID(t) != -1)
       serialize_int("uid", LABEL_DECL_UID (t));
    else
       serialize_int("uid", DECL_UID(t));
    /*if (DECL_ATTRIBUTES(t) && !MTAG_P(t))
      {
      serialize_child("attributes", DECL_ATTRIBUTES(t));
      }*/
  }
  else if (code_class == tcc_type)
  {
     /* All types have qualifiers.  */
     int quals = lang_hooks.tree_dump.type_quals (t);

     if (quals != TYPE_UNQUALIFIED)
     {
        if(quals & TYPE_QUAL_VOLATILE)
           fprintf (serialize_gimple_info.stream, "qual: %c%c%c     ",
                 (quals & TYPE_QUAL_CONST) ? 'c' : ' ',
                 'v',
                 (quals & TYPE_QUAL_RESTRICT) ? 'r' : ' ');
        else
           fprintf (serialize_gimple_info.stream, "qual: %c%c     ",
                 (quals & TYPE_QUAL_CONST) ? 'c' : ' ',
                 (quals & TYPE_QUAL_RESTRICT) ? 'r' : ' ');
        serialize_gimple_info.column += 14;
     }

     /* All types have associated declarations.  */
     serialize_child ("name", TYPE_NAME (t));

     /* All types have a main variant.  */
     if (TYPE_MAIN_VARIANT (t) != t)
        serialize_child ("unql", TYPE_MAIN_VARIANT (t));

     /* And sizes.  */
     serialize_child ("size", TYPE_SIZE (t));

     /* All types have context.  */ /*NB TBC*/
     serialize_child ("scpe", TYPE_CONTEXT (t));/*NB TBC*/

     /* All types have alignments.  */
     serialize_int ("algn", TYPE_ALIGN (t));

     if(TYPE_PACKED(t))
     {
        serialize_string("packed");
     }
  }
  else if (code_class == tcc_constant)
     /* All constants can have types.  */
     queue_and_serialize_type (t);

  /* Give the language-specific code a chance to print something.  If
     it's completely taken care of things, don't bother printing
     anything more ourselves.
     FIXME this language specific part cannot be used since the type of the parameters used by this plugin is different from the GCC one.
     FIXME Currently, only C is supported correctly.
     if (lang_hooks.tree_dump.dump_tree (t))
     goto done;
     */
  /* Now handle the various kinds of nodes.  */
  switch (code)
    {
      int i;

    case IDENTIFIER_NODE:
      serialize_string_field ("strg", IDENTIFIER_POINTER (t));
      serialize_int ("lngt", IDENTIFIER_LENGTH (t));
      break;

    case TREE_LIST:
      serialize_child ("purp", TREE_PURPOSE (t));
      serialize_child ("valu", TREE_VALUE (t));
      serialize_child ("chan", TREE_CHAIN (t));
      break;

    case STATEMENT_LIST:
      {
	tree_stmt_iterator it;
	for (i = 0, it = tsi_start (t); !tsi_end_p (it); tsi_next (&it), i++)
	  {
	    char buffer[32];
	    sprintf (buffer, "%u", i);
	    serialize_child (buffer, tsi_stmt (it));
	  }
      }
      break;

    case TREE_VEC:
      serialize_int ("lngt", TREE_VEC_LENGTH (t));
      for (i = 0; i < TREE_VEC_LENGTH (t); ++i)
	{
	  serialize_child ("op", TREE_VEC_ELT (t, i));
	}
      break;

    case INTEGER_TYPE:
    case ENUMERAL_TYPE:
      serialize_int ("prec", TYPE_PRECISION (t));
      if (TYPE_UNSIGNED (t))
        serialize_string ("unsigned");
      serialize_child ("min", TYPE_MIN_VALUE (t));
      serialize_child ("max", TYPE_MAX_VALUE (t));

      if (code == ENUMERAL_TYPE)
	serialize_child ("csts", TYPE_VALUES (t));
      break;

    case COMPLEX_TYPE:
      if (TYPE_UNSIGNED (t))
        serialize_string ("unsigned");
      break;

    case REAL_TYPE:
      serialize_int ("prec", TYPE_PRECISION (t));
      break;

    case FIXED_POINT_TYPE:
      serialize_int ("prec", TYPE_PRECISION (t));
      serialize_string_field ("sign", TYPE_UNSIGNED (t) ? "unsigned": "signed");
      serialize_string_field ("saturating",
			 TYPE_SATURATING (t) ? "saturating": "non-saturating");
      break;

    case POINTER_TYPE:
      serialize_child ("ptd", TREE_TYPE (t));
      break;

    case REFERENCE_TYPE:
      serialize_child ("refd", TREE_TYPE (t));
      break;

    case METHOD_TYPE:
      serialize_child ("retn", TREE_TYPE (t));
      serialize_child ("prms", TYPE_ARG_TYPES (t));
      serialize_child ("clas", TYPE_METHOD_BASETYPE (t));
      break;

    case FUNCTION_TYPE:
      serialize_child ("retn", TREE_TYPE (t));
      serialize_child ("prms", TYPE_ARG_TYPES (t));
      if (stdarg_p(t))
        serialize_string("varargs");
      break;

    case ARRAY_TYPE:
      serialize_child ("elts", TREE_TYPE (t));
      serialize_child ("domn", TYPE_DOMAIN (t));
      break;

    case VECTOR_TYPE:
      serialize_child ("elts", TREE_TYPE (t));
      break;

    case RECORD_TYPE:
    case UNION_TYPE:
    {
      tree op;
      if (TREE_CODE (t) == RECORD_TYPE)
	serialize_string ("struct");
      else
	serialize_string ("union");
      if (TYPE_FIELDS(t))
         for ( op = TYPE_FIELDS (t); op; op = TREE_CHAIN(op))
         {
            serialize_child ("flds", op);
         }

      if (TYPE_METHODS(t))
         for ( op = TYPE_METHODS (t); op; op = TREE_CHAIN(op))
         {
            serialize_child ("fncs", op);
         }

      /*serialize_child ("flds", TYPE_FIELDS (t));
      serialize_child ("fncs", TYPE_METHODS (t));*/
      queue_and_serialize_index ("binf", TYPE_BINFO (t), DUMP_BINFO);
      break;
    }
    case CONST_DECL:
      serialize_child ("cnst", DECL_INITIAL (t));
      break;
    case SSA_NAME:
    {
      size_t index_ssa;
#if (__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 7)
      queue_and_serialize_type (t);
#endif
      serialize_child ("var", SSA_NAME_VAR (t));
      serialize_int ("vers", SSA_NAME_VERSION (t));
#if (__GNUC__ == 4 && __GNUC_MINOR__ >= 6) && !defined(SPARC) && !defined(ARM) && (__GNUC__ == 4 && __GNUC_MINOR__ < 8)
      if (SSA_NAME_PTR_INFO (t))
        dump_pt_solution(&(SSA_NAME_PTR_INFO (t)->pt), "use", "use_vars");
#endif
      //  serialize_child ("ptr_info", SSA_NAME_PTR_INFO (t)->name_mem_tag);
      if (TREE_THIS_VOLATILE (t))
        serialize_string("volatile");
      else
        serialize_gimple_child("def_stmt", SSA_NAME_DEF_STMT(t));
      if (SSA_NAME_VAR (t) && !is_gimple_reg (SSA_NAME_VAR (t)))
        serialize_string("virtual");
      if (SSA_NAME_IS_DEFAULT_DEF(t))
         serialize_string("default");

#if (__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 9)
if (!POINTER_TYPE_P (TREE_TYPE (t))
      && SSA_NAME_RANGE_INFO (t))
    {
      double_int min, max;
      const_tree t_const = t;
      value_range_type range_type = get_range_info (t_const, &min, &max);

      if (range_type == VR_RANGE)
	{
          serialize_child("min", double_int_to_tree(TREE_TYPE (t), min));
          serialize_child("max", double_int_to_tree(TREE_TYPE (t), max));
	}
    }
#else
      index_ssa = SSA_NAME_VERSION (t);
      if(num_vr_values > 0 && (p_VRP_min[index_ssa] || p_VRP_max[index_ssa]))
      {
          if (p_VRP_min[index_ssa]) serialize_child("min", VRP_min_array[index_ssa]);
          if (p_VRP_max[index_ssa]) serialize_child("max", VRP_max_array[index_ssa]);
      }
#endif
      break;
    }
    case DEBUG_EXPR_DECL:
      serialize_int ("-uid", DEBUG_TEMP_UID (t));
      /* Fall through.  */

    case VAR_DECL:
    case PARM_DECL:
    case FIELD_DECL:
    case RESULT_DECL:
      if (TREE_CODE (t) == FIELD_DECL && DECL_C_BIT_FIELD (t))
         serialize_string ("bitfield");
      if(TREE_CODE (t) == VAR_DECL)
      {
         if (DECL_EXTERNAL (t))
            serialize_string ("extern");
         else if (!TREE_PUBLIC (t) && TREE_STATIC (t))
            serialize_string ("static");
      }
      if (TREE_CODE (t) == PARM_DECL)
        serialize_child ("argt", DECL_ARG_TYPE (t));
      else
        serialize_child ("init", DECL_INITIAL (t));
      serialize_child ("size", DECL_SIZE (t));
      serialize_int ("algn", DECL_ALIGN (t));
      if (TREE_CODE (t) == FIELD_DECL && DECL_PACKED(t))
      {
         serialize_string("packed");
      }

      if (TREE_CODE (t) == FIELD_DECL)
      {
        if (DECL_FIELD_OFFSET (t))
          serialize_child ("bpos", bit_position (t));
      }
      else if (TREE_CODE (t) == VAR_DECL || TREE_CODE (t) == PARM_DECL)
      {
        serialize_int ("used", TREE_USED (t));
        if (DECL_REGISTER (t))
          serialize_string ("register");
      }
      break;

    case FUNCTION_DECL:
      arg = DECL_ARGUMENTS (t);
      while (arg)
        {
          serialize_child ("arg", arg);
          arg = TREE_CHAIN (arg);
        }
      if (DECL_EXTERNAL (t))
         serialize_string ("undefined");
      if(is_builtin_fn(t))
         serialize_string ("builtin");
      if (!TREE_PUBLIC (t))
         serialize_string ("static");
      if (gimple_has_body_p (t) && t == cfun->decl)
         serialize_statement_child("body", cfun->cfg);
      break;

    case INTEGER_CST:
      serialize_wide_int("value", TREE_INT_CST_LOW(t));
      break;

    case STRING_CST:
      if (TREE_TYPE (t))
        serialize_string_cst("strg" , TREE_STRING_POINTER (t), TREE_STRING_LENGTH (t), TYPE_ALIGN(TREE_TYPE (t)));
      else
        serialize_string_cst("strg" , TREE_STRING_POINTER (t), TREE_STRING_LENGTH (t), 8);
      break;

    case REAL_CST:
      serialize_real (t);
      break;

    case COMPLEX_CST:
      serialize_child ("real", TREE_REALPART (t));
      serialize_child ("imag", TREE_IMAGPART (t));
      break;

    case FIXED_CST:
      serialize_fixed ("valu", TREE_FIXED_CST_PTR (t));
      break;

    case VECTOR_CST:
      {
         tree elt;

#if (__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 8)
	unsigned i;
	for (i = 0; i < VECTOR_CST_NELTS (t); ++i)
            serialize_child ("valu", VECTOR_CST_ELT (t, i));
#else
         for (elt = TREE_VECTOR_CST_ELTS (t); elt; elt = TREE_CHAIN (elt))
            serialize_child ("valu", TREE_VALUE (elt));
#endif
      }
      break;

    case TRUTH_NOT_EXPR:
    case ADDR_EXPR:
    case INDIRECT_REF:
#if __GNUC__ == 4 && __GNUC_MINOR__ ==  5
    case ALIGN_INDIRECT_REF:
    case MISALIGNED_INDIRECT_REF:
#endif
    case CLEANUP_POINT_EXPR:
    case SAVE_EXPR:
    case REALPART_EXPR:
    case IMAGPART_EXPR:
    case VIEW_CONVERT_EXPR:
      /* These nodes are unary, but do not have code class `1'.  */
      serialize_child ("op", TREE_OPERAND (t, 0));
      break;

#if (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)) && !defined(SPARC) && !defined(ARM)
    case MEM_REF:
#endif
    case TRUTH_ANDIF_EXPR:
    case TRUTH_ORIF_EXPR:
    case TRUTH_AND_EXPR:
    case TRUTH_OR_EXPR:
    case TRUTH_XOR_EXPR:
    case INIT_EXPR:
    case MODIFY_EXPR:
      serialize_child ("op", TREE_OPERAND (t, 0));
      serialize_child ("op", TREE_OPERAND (t, 1));
      break;
    case COMPOUND_EXPR:
    case PREDECREMENT_EXPR:
    case PREINCREMENT_EXPR:
    case POSTDECREMENT_EXPR:
    case POSTINCREMENT_EXPR:
    case WITH_SIZE_EXPR:
      /* These nodes are binary, but do not have code class `2'.  */
      serialize_child ("op", TREE_OPERAND (t, 0));
      serialize_child ("op", TREE_OPERAND (t, 1));
      break;

    case COMPONENT_REF:
      serialize_child ("op", TREE_OPERAND (t, 0));
      serialize_child ("op", TREE_OPERAND (t, 1));
      serialize_child ("op", TREE_OPERAND (t, 2));
      break;

    case ARRAY_REF:
    case ARRAY_RANGE_REF:
      serialize_child ("op", TREE_OPERAND (t, 0));
      serialize_child ("op", TREE_OPERAND (t, 1));
      serialize_child ("op", TREE_OPERAND (t, 2));
      serialize_child ("op", TREE_OPERAND (t, 3));
      break;

    case COND_EXPR:
      serialize_child ("op", TREE_OPERAND (t, 0));
      serialize_child ("op", TREE_OPERAND (t, 1));
      serialize_child ("op", TREE_OPERAND (t, 2));
      break;

    case BIT_FIELD_REF:
      serialize_child ("op", TREE_OPERAND (t, 0));
      serialize_child ("op", TREE_OPERAND (t, 1));
      serialize_child ("op", TREE_OPERAND (t, 2));
      /*in case t is the last statement of a basic block we have to dump the true and the false edge*/
      /*since it is more easy to compute that edge during the basic block that dump is performed there*/
      break;

    case TRY_FINALLY_EXPR:
      serialize_child ("op", TREE_OPERAND (t, 0));
      serialize_child ("op", TREE_OPERAND (t, 1));
      break;

    case CALL_EXPR:
      serialize_child ("fn", TREE_OPERAND (t, 1));
      tree arg;
      call_expr_arg_iterator iter;
      FOR_EACH_CALL_EXPR_ARG (arg, iter, t)
      {
         serialize_child ("arg", arg);
      }
      break;
    case CONSTRUCTOR:
      {
        unsigned HOST_WIDE_INT cnt;
        tree index, value;
        queue_and_serialize_type (t);
        FOR_EACH_CONSTRUCTOR_ELT (CONSTRUCTOR_ELTS (t), cnt, index, value)
        {
          serialize_child ("idx", index);
          serialize_child ("valu", value);
        }
      }
      break;

    case BIND_EXPR:
      serialize_child ("vars", TREE_OPERAND (t, 0));
      serialize_child ("body", TREE_OPERAND (t, 1));
      break;

    case LOOP_EXPR:
    case EXIT_EXPR:
    case RETURN_EXPR:
      serialize_child ("op", TREE_OPERAND (t, 0));
      break;

    case TARGET_EXPR:
      serialize_child ("decl", TREE_OPERAND (t, 0));
      serialize_child ("init", TREE_OPERAND (t, 1));
      serialize_child ("clnp", TREE_OPERAND (t, 2));
      /* There really are two possible places the initializer can be.
	 After RTL expansion, the second operand is moved to the
	 position of the fourth operand, and the second operand
	 becomes NULL.  */
      serialize_child ("init", TREE_OPERAND (t, 3));
      break;

    case CASE_LABEL_EXPR:
         if (CASE_LOW (t) && CASE_HIGH (t))
         {
            serialize_child ("op", CASE_LOW (t));
            serialize_child ("op", CASE_HIGH (t));
            serialize_child ("goto", CASE_LABEL (t));
         }
         else if (CASE_LOW (t))
         {
            serialize_child ("op", CASE_LOW (t));
            serialize_child ("goto", CASE_LABEL (t));
         }
         else
         {
            serialize_string ("default");
            serialize_child ("goto", CASE_LABEL (t));
         }
      break;
    case LABEL_EXPR:
      serialize_child ("name", TREE_OPERAND (t,0));
      break;
    case GOTO_EXPR:
      serialize_child ("labl", TREE_OPERAND (t, 0));
      break;
    case SWITCH_EXPR:
      serialize_child ("cond", TREE_OPERAND (t, 0));
      serialize_child ("body", TREE_OPERAND (t, 1));
      if (TREE_OPERAND (t, 2))
        {
      	  serialize_child ("labl", TREE_OPERAND (t,2));
        }
      break;
    case OMP_CLAUSE:
      {
	int i;
	fprintf (serialize_gimple_info.stream, "%s\n", omp_clause_code_name[OMP_CLAUSE_CODE (t)]);
	for (i = 0; i < omp_clause_num_ops[OMP_CLAUSE_CODE (t)]; i++)
	  serialize_child ("op: ", OMP_CLAUSE_OPERAND (t, i));
      }
      break;
      case TARGET_MEM_REF:
      {
#if (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 7) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6 && __GNUC_PATCHLEVEL__>= 1)) && !defined(SPARC) && !defined(ARM)
         serialize_child ("base", TMR_BASE(t));
         serialize_child ("offset", TMR_OFFSET(t));
         serialize_child ("idx", TMR_INDEX(t));
         serialize_child ("step", TMR_STEP(t));
         serialize_child ("idx2", TMR_INDEX2(t));
#else
         if(TMR_SYMBOL(t))
         {
           serialize_child ("base", TMR_SYMBOL(t));
           serialize_child ("offset", TMR_OFFSET(t));
           serialize_child ("idx", TMR_INDEX(t));
           serialize_child ("step", TMR_STEP(t));
           serialize_child ("idx2", TMR_BASE(t));
         }
         else
         {
           serialize_child ("base", TMR_BASE(t));
           serialize_child ("offset", TMR_OFFSET(t));
           serialize_child ("idx", TMR_INDEX(t));
           serialize_child ("step", TMR_STEP(t));
           serialize_child ("idx2", TMR_SYMBOL(t));
         }
         //serialize_child ("orig", TMR_ORIGINAL(t));
#endif
         break;
      }
      case ASSERT_EXPR:
         serialize_child ("op", TREE_OPERAND (t, 0));
         serialize_child ("op", TREE_OPERAND (t, 1));
         break;
      case VEC_COND_EXPR:
#if (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 7))
      case VEC_PERM_EXPR:
#endif
      case DOT_PROD_EXPR:
         serialize_child ("op", TREE_OPERAND (t, 0));
         serialize_child ("op", TREE_OPERAND (t, 1));
         serialize_child ("op", TREE_OPERAND (t, 2));
         break;
    default:
      /* There are no additional fields to print.  */
      break;
    }

 done:

  /* Terminate the line.  */
  fprintf (serialize_gimple_info.stream, "\n");
}

/* Serialize the next gimple node in the queue.  */
static void
dequeue_and_serialize_gimple ()
{
  serialize_queue_p dq;
  splay_tree_node stn_index;
  splay_tree_node stn_ann;
  gimple g;
  unsigned int index;
  unsigned int ann;
  enum tree_code code;
  enum tree_code_class code_class;
  const char* code_name;
  expanded_location xloc;
  int i;
  tree t;

  /* Get the next node from the queue.  */
  dq = serialize_gimple_info.queue;
  stn_index = dq->node_index;
  stn_ann = dq->node_ann;
  index = (unsigned int) stn_index->value;
  ann =  (unsigned int) stn_ann->value;
  g = (gimple) stn_index->key;

  /* Remove the node from the queue, and put it on the free list.  */
  serialize_gimple_info.queue = dq->next;
  if (!serialize_gimple_info.queue)
    serialize_gimple_info.queue_end = 0;
  dq->next = serialize_gimple_info.free_list;
  serialize_gimple_info.free_list = dq;

  serialize_gimple_info.column = 0;
  /* Print the node index.  */
  serialize_index (index);
  if(gimple_code (g) == GIMPLE_CALL && gimple_call_lhs (g))
     code_name = "gimple_assign";
  else
     code_name = gimple_code_name[(int) gimple_code(g)];
  fprintf (serialize_gimple_info.stream, "%-16s ", code_name);

  serialize_gimple_info.column = 25;
   serialize_child("scpe", cfun->decl);
   if(gimple_bb(g))
      serialize_int("bb_index", gimple_bb(g)->index);
   else
      serialize_int("bb_index", 0);

   if (gimple_has_mem_ops (g))
     serialize_vops (g);

  if(gimple_has_location (g))
  {
    xloc = expand_location (gimple_location (g));
    serialize_maybe_newline ();
    fprintf (serialize_gimple_info.stream, "srcp: \"%s\":%-d:%-6d ", xloc.file, xloc.line, xloc.column);
    serialize_gimple_info.column += 12 + strlen(xloc.file) + 8;
  }

  serialize_int ("time_weight", estimate_num_insns(g, &eni_time_weights));
  serialize_int ("size_weight", estimate_num_insns(g, &eni_size_weights));

  switch (gimple_code (g))
    {

    case GIMPLE_ASM:
      {
        int i, n;
        if (gimple_asm_volatile_p (g))
          serialize_string ("volatile");  
        serialize_string_field ("str", gimple_asm_string (g));
        n = gimple_asm_noutputs (g);
        if (n > 0)
          {
            tree arglist = NULL_TREE;
            for (i = n - 1; i >= 0; i--)
            {
              tree item = gimple_asm_output_op (g, i);
              arglist = tree_cons (TREE_PURPOSE (item), TREE_VALUE (item), arglist);
            }
            serialize_child ("out", arglist);
          }
        n = gimple_asm_ninputs (g);
        if (n > 0)
          {
            tree arglist = NULL_TREE;
            for (i = n - 1; i >= 0; i--)
            {
              tree item = gimple_asm_input_op (g, i);
              arglist = tree_cons (TREE_PURPOSE (item), TREE_VALUE (item), arglist);
            }

            serialize_child ("in", arglist);
          }
        n = gimple_asm_nclobbers (g); 
        if (n > 0)
          {
            tree arglist = NULL_TREE;
            for (i = n - 1; i >= 0; i--)
            {
              tree item = gimple_asm_clobber_op (g, i);
              arglist = tree_cons (TREE_PURPOSE (item), TREE_VALUE (item), arglist);
            }

            serialize_child ("clob", arglist);
          }
      }
      break;

    /* For instance, given the GENERIC expression, a = b + c, its tree representation is:
    MODIFY_EXPR <VAR_DECL  <a>, PLUS_EXPR <VAR_DECL <b>, VAR_DECL <c>>>
    In this case, the GIMPLE form for this statement is logically identical to its GENERIC form but in GIMPLE, 
    the PLUS_EXPR on the RHS of the assignment is not represented as a tree, instead the two operands are taken 
    out of the PLUS_EXPR sub-tree and flattened into the GIMPLE tuple as follows:
    GIMPLE_ASSIGN <PLUS_EXPR, VAR_DECL <a>, VAR_DECL <b>, VAR_DECL <c>> */

    /* The first operand is what to set; the second, the new value - consistent with olderversions */

    case GIMPLE_ASSIGN:
#if (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)) && !defined(SPARC) && !defined(ARM)
      if (get_gimple_rhs_class (gimple_expr_code (g)) == GIMPLE_TERNARY_RHS) 
      {
          serialize_child ("op", gimple_assign_lhs (g)); 
          serialize_child ("op", build3 (gimple_assign_rhs_code (g), TREE_TYPE (gimple_assign_lhs (g)), gimple_assign_rhs1 (g), gimple_assign_rhs2 (g), gimple_assign_rhs3 (g))); 
      }
      else
#endif
      if (get_gimple_rhs_class (gimple_expr_code (g)) == GIMPLE_BINARY_RHS) 
        {
          serialize_child ("op", gimple_assign_lhs (g)); 
          serialize_child ("op", build2 (gimple_assign_rhs_code (g), TREE_TYPE (gimple_assign_lhs (g)), gimple_assign_rhs1 (g), gimple_assign_rhs2 (g))); 
        }
      else if (get_gimple_rhs_class (gimple_expr_code (g)) == GIMPLE_UNARY_RHS)
        {
          serialize_child ("op", gimple_assign_lhs (g)); 
          serialize_child ("op", build1 (gimple_assign_rhs_code (g), TREE_TYPE (gimple_assign_lhs (g)), gimple_assign_rhs1 (g))); 
        }
      else if (get_gimple_rhs_class (gimple_expr_code (g)) == GIMPLE_SINGLE_RHS)
        {
          t = gimple_assign_rhs1 (g);
          serialize_child ("op", gimple_assign_lhs (g)); 
          serialize_child ("op", gimple_assign_rhs1 (g)); 
        }
      else
        gcc_unreachable ();

#if (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 7)) && !defined(SPARC) && !defined(ARM)
          if (gimple_clobber_p (g))
            serialize_string("clobber");
#endif

      break;

    case GIMPLE_BIND:
      {
        tree op;
        if (gimple_bind_vars(g))
          for ( op = gimple_bind_vars (g); op; op = TREE_CHAIN (op))
            {
              serialize_child ("vars", op);
            }
        serialize_child ("body", gimple_op (g, 1));
      }
      break;

    case GIMPLE_CALL:
      /* Function call.  Operand 0 is the function.
      * Operand 1 is the argument list, a list of expressions made out of a chain of TREE_LIST nodes.
      * Operand 2 is the static chain argument, or NULL. */
      {
#if (__GNUC__ == 4 && __GNUC_MINOR__ >= 6) && !defined(SPARC) && !defined(ARM) && (__GNUC__ == 4 && __GNUC_MINOR__ < 8)
        struct pt_solution *pt;
#endif
        tree lhs = gimple_call_lhs (g);
        if(lhs)
        {
           tree arg_list = gimple_call_expr_arglist (g);
           t = build_custom_function_call_expr (UNKNOWN_LOCATION, gimple_call_fn (g), arg_list);
           serialize_child ("op",lhs);
           serialize_child ("op", t);
        }
        else
        {
           serialize_child ("fn", gimple_call_fn (g));
           unsigned int arg_index;
           for(arg_index = 0; arg_index < gimple_call_num_args(g); arg_index++)
           {
              serialize_child ("arg", gimple_call_arg(g, arg_index));
           }
        }
#if (__GNUC__ == 4 && __GNUC_MINOR__ >= 6) && !defined(SPARC) && !defined(ARM) && (__GNUC__ == 4 && __GNUC_MINOR__ < 8)
        pt = gimple_call_use_set (g);
        if (!pt_solution_empty_p (pt))
           dump_pt_solution(pt, "use", "use_vars");
        pt = gimple_call_clobber_set (g);
        if (!pt_solution_empty_p (pt))
           dump_pt_solution(pt, "clb", "clb_vars");
#endif
      }
      break;

    case GIMPLE_COND:
      GIMPLE_CHECK (g, GIMPLE_COND);
      t = build2 (gimple_cond_code (g), boolean_type_node, gimple_cond_lhs (g), gimple_cond_rhs (g));
      serialize_child ("op", t);
      break;

    case GIMPLE_LABEL:
      serialize_child ("op", gimple_label_label (g));
      break;

    case GIMPLE_GOTO:
      serialize_child ("op", gimple_goto_dest (g));
      break;

    case GIMPLE_NOP:
      break;

    case GIMPLE_RETURN:
      serialize_child ("op", gimple_return_retval (g));
      break;

    case GIMPLE_SWITCH:
      {
         tree label_vec;
         size_t i, num_elmts = gimple_switch_num_labels (g);

         label_vec = make_tree_vec (num_elmts);


         for (i = 0; i < num_elmts; i++)
         {
            TREE_VEC_ELT (label_vec, i) = gimple_switch_label (g, i);
         }
         serialize_child ("op", gimple_switch_index (g));
         serialize_child ("op", label_vec);
      }
      break;

    case GIMPLE_TRY:
      error ("Encountered gimple instruction set GIMPLE_TRY");
      break;

    case GIMPLE_PHI:
      serialize_child ("res", gimple_phi_result (g));
      for (i = 0; i < gimple_phi_num_args (g); i++)
        {
          serialize_child ("def", gimple_phi_arg_def (g, i));
          serialize_int ("edge", gimple_phi_arg_edge (g, i)->src->index);
        }
      if(!is_gimple_reg (gimple_phi_result (g)))
        serialize_string("virtual");
      break;

    case GIMPLE_OMP_PARALLEL:
      error ("Encountered gimple instruction set GIMPLE_OMP_PARALLEL");
      break;

    case GIMPLE_OMP_TASK:
      error ("Encountered gimple instruction set GIMPLE_TASK");
      break;

    case GIMPLE_OMP_ATOMIC_LOAD:
      error ("Encountered gimple instruction set GIMPLE_OMP_ATOMIC_LOAD");
      break;

    case GIMPLE_OMP_ATOMIC_STORE:
      error ("Encountered gimple instruction set GIMPLE_OMP_ATOMIC_STORE");
      break;

    case GIMPLE_OMP_FOR:
      error ("Encountered gimple instruction set GIMPLE_OMP_FOR");
      break;

    case GIMPLE_OMP_CONTINUE:
      error ("Encountered gimple instruction set GIMPLE_OMP_CONTINUE");
      break;

    case GIMPLE_OMP_SINGLE:
      error ("Encountered gimple instruction set GIMPLE_OMP_SINGLE");
      break;

    case GIMPLE_OMP_RETURN:
      error ("Encountered gimple instruction set GIMPLE_OMP_RETURN");
      break;

    case GIMPLE_OMP_SECTIONS:
      error ("Encountered gimple instruction set GIMPLE_OMP_SECTIONS");
      break;

    case GIMPLE_OMP_SECTIONS_SWITCH:
      error ("Encountered gimple instruction set GIMPLE_OMP_SECTIONS_SWITCH");
      break;

    case GIMPLE_OMP_MASTER:
      error ("Encountered gimple instruction set GIMPLE_OMP_MASTER");
      break;

    case GIMPLE_OMP_ORDERED:
      error ("Encountered gimple instruction set GIMPLE_OMP_ORDERED");
      break;

    case GIMPLE_OMP_SECTION:
      error ("Encountered gimple instruction set GIMPLE_OMP_SECTION");
      break;

    case GIMPLE_OMP_CRITICAL:
      error ("Encountered gimple instruction set GIMPLE_OMP_CRITICAL");
      break;

    case GIMPLE_CATCH:
      error ("Encountered gimple instruction set GIMPLE_CATCH");
      break;

    case GIMPLE_EH_FILTER:
      error ("Encountered gimple instruction set GIMPLE_EH_FILTER");
      break;

    case GIMPLE_EH_MUST_NOT_THROW:
      error ("Encountered gimple instruction set GIMPLE_EH_MUST_NOT_THROW");
      break;

    case GIMPLE_RESX:
      error ("Encountered gimple instruction set GIMPLE_RESX");
      break;

    case GIMPLE_EH_DISPATCH:
      error ("Encountered gimple instruction set GIMPLE_EH_DISPATCH");
      break;

    case GIMPLE_DEBUG:
      if (gimple_debug_bind_p (g)) 
        {
          serialize_child ("var", gimple_debug_bind_get_var (g));
          serialize_child ("val", gimple_debug_bind_get_value (g));
        }
      break;

    case GIMPLE_PREDICT:
      error ("Encountered gimple instruction set GIMPLE_PREDICT");
      // t = build1 (NOP_EXPR, void_type_node, size_zero_node);
      break;

    default:
      error ("Encountered UNKNOWN gimple instruction set");
      break;
    }
  fprintf (serialize_gimple_info.stream, "\n");

}

/* Serialize the next statement node in the queue.  */
static void
dequeue_and_serialize_statement ()
{
  serialize_queue_p dq;
  splay_tree_node stn_index;
  splay_tree_node stn_ann;
  unsigned int index;
  unsigned int ann;
  enum tree_code code;
  enum tree_code_class code_class;
  const char* code_name;
  basic_block bb;

  /* Get the next node from the queue.  */
  dq = serialize_gimple_info.queue;
  stn_index = dq->node_index;
  stn_ann = dq->node_ann;
  index = (unsigned int) stn_index->value;
  ann = (unsigned int) stn_ann->value;
  // cfg = (tree) stn->key;

  /* Remove the node from the queue, and put it on the free list.  */
  serialize_gimple_info.queue = dq->next;
  if (!serialize_gimple_info.queue)
    serialize_gimple_info.queue_end = 0;
  dq->next = serialize_gimple_info.free_list;
  serialize_gimple_info.free_list = dq;

  /* Print the node index.  */
  serialize_index (index);

  /* And the type of node this is.  */
  if (IS_BINFO(ann))
    code_name = "binfo";
  else
    code_name = "statement_list";
  fprintf (serialize_gimple_info.stream, "%-16s ", code_name);
  serialize_gimple_info.column = 25;

  /* In case of basic blocks the function print:
                 + first a list of all statements
                 +  then for each basic block it prints:
                    - the number of the basic block
                    - the predecessor basic block
                    - the successor basic block
                    - list of statement
                 + otherwise it prints only the list of statements */
  FOR_EACH_BB_FN (bb, cfun)
    {
      gimple_stmt_iterator gsi;
      gimple stmt;
      edge e;
      edge_iterator ei;
      const char *field;

      if (bb->index != 0)
        serialize_new_line ();
      serialize_int ("bloc", bb->index );
      if(bb->loop_father)
        serialize_int ("loop_id", bb->loop_father->num );
      else
        serialize_int ("loop_id", bb->index );


      if(bb->loop_father && bb->loop_father->can_be_parallel)
        serialize_string("hpl");
      
      FOR_EACH_EDGE (e, ei, bb->preds)
        if (e->src == ENTRY_BLOCK_PTR)
          {
            serialize_maybe_newline ();
            field = "pred: ENTRY";
            fprintf (serialize_gimple_info.stream, "%-4s ", field);
            serialize_gimple_info.column += 14;
	  }
        else
          serialize_int ("pred", e->src->index);
      FOR_EACH_EDGE (e, ei, bb->succs)
        if (e->dest == EXIT_BLOCK_PTR)
          {
            serialize_maybe_newline ();
            field = "succ: EXIT";
            fprintf (serialize_gimple_info.stream, "%-4s ", field);
            serialize_gimple_info.column += 14;
          }
        else
          serialize_int ("succ", e->dest->index);
      /* in case the last statement is a cond_expr we print also the type of the outgoing edges*/
      stmt = last_stmt (bb);
      if (stmt && gimple_code (stmt) == GIMPLE_COND)
        {
          edge true_edge, false_edge;
          /* When we are emitting the code or changing CFG, it is possible that
          the edges are not yet created.  When we are using debug_bb in such
          a situation, we do not want it to crash.  */
          if (EDGE_COUNT (bb->succs) == 2)
            {
              extract_true_false_edges_from_block (bb, &true_edge, &false_edge);
              serialize_int ("true_edge", true_edge->dest->index);
              serialize_int ("false_edge", false_edge->dest->index);
            }
        }
      /* dump phi information*/
      {
        gimple_stmt_iterator i;
        for (i = gsi_start_phis (bb); !gsi_end_p (i); gsi_next (&i))
          {
            gimple phi = gsi_stmt (i);
            serialize_gimple_child("phi", phi);
          }
      }

      for (gsi = gsi_start_bb (bb); !gsi_end_p (gsi); gsi_next (&gsi))
        {
          stmt = gsi_stmt (gsi);
          serialize_gimple_child("stmt", stmt);
        }
    }
  /* Terminate the line.  */
  fprintf (serialize_gimple_info.stream, "\n");
}

/**
 * Serialize a global symbol (function_decl or global var_decl
 * @param t is the symbol to be serialized
 */
void
SerializeGimpleGlobalTreeNode(tree t)
{
   splay_tree_node n;

   /* Print function header */
   if(TREE_CODE(t) == FUNCTION_DECL)
   {
      SerializeGimpleFunctionHeader(t);
   }

   /* Queue up the first node when not yet considered.  */
   n = splay_tree_lookup(di_local_nodes_index, (splay_tree_key) t);
   if (!n)
      queue (t, DUMP_NONE);
   else if (FUNCTION_DECL == TREE_CODE(t))
   {
      queue (t, DUMP_NONE);
   }


  /* Until the queue is empty, keep dumping nodes.  */
  while (serialize_gimple_info.queue)
    dequeue_and_serialize();
}

/**
 * Serialize the header of a function declaration
 * @param fn is the function decl
 */
static void
SerializeGimpleFunctionHeader(tree fn)
{
  tree arg;
  fprintf (serialize_gimple_info.stream, "\n;; Function %s (%s)\n\n", lang_hooks.decl_printable_name (fn, 2), lang_hooks.decl_printable_name (fn, 2)); 
  fprintf (serialize_gimple_info.stream, ";; %s (", lang_hooks.decl_printable_name (fn, 2));
  arg = DECL_ARGUMENTS (fn);
  while (arg)
    {
      print_generic_expr (serialize_gimple_info.stream, TREE_TYPE (arg), TDF_SLIM);
      fprintf (serialize_gimple_info.stream, " ");
      print_generic_expr (serialize_gimple_info.stream, arg, TDF_SLIM);
      if (TREE_CHAIN (arg))
        fprintf (serialize_gimple_info.stream, ", ");
      arg = TREE_CHAIN (arg);
    }
  fprintf (serialize_gimple_info.stream, ")\n");
}

tree
gimple_call_expr_arglist (gimple g)
{
  tree arglist = NULL_TREE;
  int i;
  for (i = gimple_call_num_args (g) - 1; i >= 0; i--)
    arglist = tree_cons (NULL_TREE, gimple_call_arg (g, i), arglist);
  return arglist;
}

tree
build_custom_function_call_expr (location_t loc, tree fn, tree arglist)
{
  tree fntype = TREE_TYPE (fn);
  int n = list_length (arglist);
  tree *argarray = (tree *) alloca (n * sizeof (tree));
  int i;

  for (i = 0; i < n; i++, arglist = TREE_CHAIN (arglist))
    argarray[i] = TREE_VALUE (arglist);
  return fold_builtin_call_array (loc, TREE_TYPE (fntype), fn, n, argarray);
}

/**
 * Serialize globlal variable on files
 */
void serialize_global_variable(void * gcc_data, void * user_data)
{
   char * name = concat (dump_base_name, ".001t.tu", NULL);
   SerializeGimpleBegin(name);
   serialize_globals();
   SerializeGimpleEnd();
}


/// alias based dependency analysis
///

/// auxiliary functions
///

/*
 * Worker for the walker that checks for true clobbering.
 */
static bool
serialize_gimple_aliased_reaching_defs_1 (ao_ref *dummy_1, tree vdef, void *dummy_2)
{
  serialize_child ("vdep_vuse", vdef);
  gimple def_stmt = SSA_NAME_DEF_STMT (vdef);
  tree vuse = gimple_vuse (def_stmt);
  gimple vuse_stmt = SSA_NAME_DEF_STMT (vuse);
  if (gimple_code (vuse_stmt) == GIMPLE_PHI)
     return true;
  else
     return false;
}

static void
serialize_gimple_aliased_reaching_defs (gimple stmt, tree ref)
{
  ao_ref refd;
  ao_ref_init (&refd, ref);
  walk_aliased_vdefs (&refd, gimple_vuse (stmt),
                  serialize_gimple_aliased_reaching_defs_1,
                  gimple_bb (stmt), NULL);
}

static void
serialize_all_gimple_aliased_reaching_defs (gimple stmt)
{
  walk_aliased_vdefs (NULL, gimple_vuse (stmt),
                  serialize_gimple_aliased_reaching_defs_1,
                  NULL, NULL);
}


void serialize_gimple_dependent_stmts_load(gimple gs)
{
   gcc_assert (gs);
   tree use;
   use = gimple_vuse (gs);
   gcc_assert (use);
   if (is_gimple_call (gs))
   {
      tree callee = gimple_call_fndecl (gs);

      /* Calls to functions that are merely acting as barriers
               or that only store to memory do not make any previous
               stores necessary.  */
      if (callee != NULL_TREE
          && DECL_BUILT_IN_CLASS (callee) == BUILT_IN_NORMAL
          && (DECL_FUNCTION_CODE (callee) == BUILT_IN_MEMSET
              || DECL_FUNCTION_CODE (callee) == BUILT_IN_MALLOC
              || DECL_FUNCTION_CODE (callee) == BUILT_IN_FREE
#if (__GNUC__ == 4 && __GNUC_MINOR__ >= 7)
              || DECL_FUNCTION_CODE (callee) == BUILT_IN_MEMSET_CHK
              || DECL_FUNCTION_CODE (callee) == BUILT_IN_CALLOC
              || DECL_FUNCTION_CODE (callee) == BUILT_IN_VA_END
              || DECL_FUNCTION_CODE (callee) == BUILT_IN_ALLOCA
              || (DECL_FUNCTION_CODE (callee)
              == BUILT_IN_ALLOCA_WITH_ALIGN)
              || DECL_FUNCTION_CODE (callee) == BUILT_IN_STACK_SAVE
              || DECL_FUNCTION_CODE (callee) == BUILT_IN_STACK_RESTORE
              || DECL_FUNCTION_CODE (callee) == BUILT_IN_ASSUME_ALIGNED
#endif
              ))
         return;
      serialize_all_gimple_aliased_reaching_defs(gs);
      //serialize_child ("vdep_vuse", use);
   }
   else if (gimple_assign_single_p (gs))
   {
      tree rhs;
      /* If this is a load mark things necessary.  */
      rhs = gimple_assign_rhs1 (gs);
      if (TREE_CODE (rhs) != SSA_NAME
          && !is_gimple_min_invariant (rhs)
          && TREE_CODE (rhs) != CONSTRUCTOR)
      {
         serialize_gimple_aliased_reaching_defs (gs, rhs);
      }

   }
   else if (gimple_code (gs) == GIMPLE_RETURN)
   {
      tree rhs = gimple_return_retval (gs);
      /* A return statement may perform a load.  */
      if (rhs
          && TREE_CODE (rhs) != SSA_NAME
          && !is_gimple_min_invariant (rhs)
          && TREE_CODE (rhs) != CONSTRUCTOR)
      {
         serialize_gimple_aliased_reaching_defs (gs, rhs);
      }
   }
   else if (gimple_code (gs) == GIMPLE_ASM)
   {
      serialize_all_gimple_aliased_reaching_defs(gs);
      //serialize_child ("vdep_vuse", use);
   }
#if (__GNUC__ == 4 && __GNUC_MINOR__ >= 7)
   else if (gimple_code (gs) == GIMPLE_TRANSACTION)
   {
      serialize_all_gimple_aliased_reaching_defs(gs);
      //serialize_child ("vdep_vuse", use);
   }
#endif
   else
      gcc_unreachable ();
}

void compute_vuse_vdef_map(tree t)
{
   if(t && TREE_CODE(t) == FUNCTION_DECL && gimple_has_body_p (t))
   {
      basic_block bb;
      FOR_EACH_BB_FN (bb, cfun)
      {
         gimple_stmt_iterator gsi;
         gimple stmt;
         for (gsi = gsi_start_bb (bb); !gsi_end_p (gsi); gsi_next (&gsi))
         {
            stmt = gsi_stmt (gsi);
            add_gimple_to_vdef_vuse_map(stmt);
         }
      }
   }
}

static
bool gimples_may_conflict(gimple gs, gimple next_gs)
{
   tree *op0, *op0_next, *op1=0;

   if ((gimple_code (next_gs) == GIMPLE_CALL
        && !(gimple_call_flags (next_gs) & (ECF_CONST | ECF_PURE)))
       || (gimple_code (next_gs) == GIMPLE_ASM
           && (gimple_asm_volatile_p (next_gs) || gimple_vuse (next_gs))))
      return true;

   if (gimple_code (next_gs) == GIMPLE_ASSIGN)
   {
      op0_next = gimple_assign_lhs_ptr (next_gs);
   }
   else if (gimple_code (next_gs) == GIMPLE_CALL)
   {
      op0_next = gimple_call_lhs_ptr (next_gs);
   }
   else
      return true;


   if ((gimple_code (gs) == GIMPLE_CALL
        && !(gimple_call_flags (gs) & (ECF_CONST | ECF_PURE)))
       || (gimple_code (gs) == GIMPLE_ASM
           && (gimple_asm_volatile_p (gs) || gimple_vuse (gs))))
      return true;

   if (gimple_code (gs) == GIMPLE_ASSIGN)
   {
      tree base;
      tree *rhs = gimple_assign_rhs1_ptr (gs);
      if (DECL_P (*rhs)
      || (REFERENCE_CLASS_P (*rhs)
          && (base = get_base_address (*rhs))
          && TREE_CODE (base) != SSA_NAME))
         op1 = rhs;

      op0 = gimple_assign_lhs_ptr (gs);
   }
   else if (gimple_code (gs) == GIMPLE_CALL)
   {
      op0 = gimple_call_lhs_ptr (gs);
   }
   else
      return true;

   if ((op0
         && (DECL_P (*op0)
         || (REFERENCE_CLASS_P (*op0) && get_base_address (*op0))))
       && (op0_next
           && (DECL_P (*op0_next)
           || (REFERENCE_CLASS_P (*op0_next) && get_base_address (*op0_next)))))
   {
      ao_ref refd, refd_next;
      ao_ref_init (&refd, *op0);
      ao_ref_init (&refd_next, *op0_next);
      if (refs_may_alias_p_1 (&refd, &refd_next, true))
         return true;
   }
   if((op1
            && (DECL_P (*op1)
            || (REFERENCE_CLASS_P (*op1) && get_base_address (*op1))))
          && (op0_next
              && (DECL_P (*op0_next)
              || (REFERENCE_CLASS_P (*op0_next) && get_base_address (*op0_next)))))
      {
         ao_ref refd, refd_next;
         ao_ref_init (&refd, *op1);
         ao_ref_init (&refd_next, *op0_next);
         if (refs_may_alias_p_1 (&refd, &refd_next, true))
            return true;

      }
   return false;
}

void serialize_gimple_dependent_stmts_store(gimple gs)
{
   tree vuse, last_valid_vuse;
   same_vuse_linked_list_p cur;
   int ssa_index;
   splay_tree_node n;
   bool conflict_p = false;
   gimple curr_store;

   curr_store = gs;
   do
   {
      gcc_assert (curr_store);
      vuse = gimple_vuse (curr_store);
      if(!vuse) return;
      last_valid_vuse = vuse;
      ssa_index = SSA_NAME_VERSION(vuse);
      n = splay_tree_lookup(di_local_vdef_vuse_map, (splay_tree_key) ssa_index);
      for (cur = (same_vuse_linked_list_p) n->value; cur && !conflict_p; cur = cur->next)
      {
         gimple load_stmt = cur->data;
         if(load_stmt != curr_store)
         {
            if (is_gimple_call (load_stmt))
            {
               tree callee = gimple_call_fndecl (load_stmt);

               /* Calls to functions that are merely acting as barriers
                     or that only store to memory do not make any previous
                     stores necessary.  */
               if (callee != NULL_TREE
                   && DECL_BUILT_IN_CLASS (callee) == BUILT_IN_NORMAL
                   && (DECL_FUNCTION_CODE (callee) == BUILT_IN_MEMSET
                       || DECL_FUNCTION_CODE (callee) == BUILT_IN_MALLOC
                       || DECL_FUNCTION_CODE (callee) == BUILT_IN_FREE
        #if (__GNUC__ == 4 && __GNUC_MINOR__ >= 7)
                       || DECL_FUNCTION_CODE (callee) == BUILT_IN_MEMSET_CHK
                       || DECL_FUNCTION_CODE (callee) == BUILT_IN_CALLOC
                       || DECL_FUNCTION_CODE (callee) == BUILT_IN_VA_END
                       || DECL_FUNCTION_CODE (callee) == BUILT_IN_ALLOCA
                       || (DECL_FUNCTION_CODE (callee)
                           == BUILT_IN_ALLOCA_WITH_ALIGN)
                       || DECL_FUNCTION_CODE (callee) == BUILT_IN_STACK_SAVE
                       || DECL_FUNCTION_CODE (callee) == BUILT_IN_STACK_RESTORE
                       || DECL_FUNCTION_CODE (callee) == BUILT_IN_ASSUME_ALIGNED
        #endif
                       ))
                  continue;
               conflict_p = true;
            }
            else if (gimple_assign_single_p (load_stmt))
            {
               tree rhs;
               /* If this is a load mark things necessary.  */
               rhs = gimple_assign_rhs1 (load_stmt);
               if (TREE_CODE (rhs) != SSA_NAME
                   && !is_gimple_min_invariant (rhs)
                   && TREE_CODE (rhs) != CONSTRUCTOR)
               {
                  conflict_p = gimples_may_conflict(load_stmt, curr_store);
               }

            }
            else if (gimple_code (load_stmt) == GIMPLE_RETURN)
            {
               tree rhs = gimple_return_retval (load_stmt);
               /* A return statement may perform a load.  */
               if (rhs
                   && TREE_CODE (rhs) != SSA_NAME
                   && !is_gimple_min_invariant (rhs)
                   && TREE_CODE (rhs) != CONSTRUCTOR)
               {
                  conflict_p = gimples_may_conflict(load_stmt, curr_store);
               }
            }
            else if (gimple_code (load_stmt) == GIMPLE_ASM)
            {
               conflict_p = true;
            }
#if (__GNUC__ == 4 && __GNUC_MINOR__ >= 7)
            else if (gimple_code (load_stmt) == GIMPLE_TRANSACTION)
            {
               conflict_p = true;
            }
#endif
            else
               gcc_unreachable ();
         }
      }
      if(conflict_p)
      {
         serialize_all_gimple_aliased_reaching_defs(curr_store);
         serialize_child ("redef_vuse", vuse);
         return;
      }
      gimple next_store = SSA_NAME_DEF_STMT (vuse);
      conflict_p = gimples_may_conflict(gs, next_store);
      if(conflict_p)
      {
         if(gimple_vuse(curr_store))
            serialize_child ("vdep_vuse", gimple_vuse(curr_store));
         conflict_p = false;
      }
      curr_store = next_store;

   } while(true);

   

}
