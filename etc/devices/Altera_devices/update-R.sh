#! /bin/bash

#update
for devices in EP2C70F896C6; do
   mkdir -p "$devices-DIR-R"
   cd "$devices-DIR-R"
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-DSP.xml" -v4 --update=mult_expr_FU > "$devices-R.log" 2>&1;
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=ui_mult_expr_FU >> "$devices-R.log" 2>&1;
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=widen_mult_expr_FU >> "$devices-R.log" 2>&1;
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=ui_widen_mult_expr_FU >> "$devices-R.log" 2>&1
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=dot_prod_expr_FU >> "$devices-R.log" 2>&1
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=ui_dot_prod_expr_FU >> "$devices-R.log" 2>&1
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=vec_mult_expr_FU >> "$devices-R.log" 2>&1
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=ui_vec_mult_expr_FU >> "$devices-R.log" 2>&1
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=vec_widen_mult_hi_expr_FU >> "$devices-R.log" 2>&1
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=ui_vec_widen_mult_hi_expr_FU >> "$devices-R.log" 2>&1
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=vec_widen_mult_lo_expr_FU >> "$devices-R.log" 2>&1
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=ui_vec_widen_mult_lo_expr_FU >> "$devices-R.log" 2>&1
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=vec_dot_prod_expr_FU >> "$devices-R.log" 2>&1
   /opt/panda/bin/eucalyptus --estimate-library="../$devices-R.xml" --target-scriptfile=../CycloneII-R.xml --target-datafile="../$devices-R.xml" -v4 --update=ui_vec_dot_prod_expr_FU >> "$devices-R.log" 2>&1
   cd ..
done

