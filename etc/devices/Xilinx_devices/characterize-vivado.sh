#! /bin/bash

for device in xc7z020-1clg484-VVD; do
   mkdir -p "$device-DIR"
   cd "$device-DIR"
   /opt/panda/bin/eucalyptus --estimate-library="../$device.xml" --target-scriptfile=../Zynq-VVD.xml --target-datafile="../$device-seed.xml" -v4 >& "$device.log" 
   cd ..
done

