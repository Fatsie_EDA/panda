
/*============================================================================

This C header file is part of the SoftFloat IEC/IEEE Floating-point Arithmetic
Package, Release 2b.

Written by John R. Hauser.  This work was made possible in part by the
International Computer Science Institute, located at Suite 600, 1947 Center
Street, Berkeley, California 94704.  Funding was partially provided by the
National Science Foundation under grant MIP-9311980.  The original version
of this code was written as part of a project to build a fixed-point vector
processor in collaboration with the University of California at Berkeley,
overseen by Profs. Nelson Morgan and John Wawrzynek.  More information
is available through the Web page `http://www.cs.berkeley.edu/~jhauser/
arithmetic/SoftFloat.html'.

THIS SOFTWARE IS DISTRIBUTED AS IS, FOR FREE.  Although reasonable effort has
been made to avoid it, THIS SOFTWARE MAY CONTAIN FAULTS THAT WILL AT TIMES
RESULT IN INCORRECT BEHAVIOR.  USE OF THIS SOFTWARE IS RESTRICTED TO PERSONS
AND ORGANIZATIONS WHO CAN AND WILL TAKE FULL RESPONSIBILITY FOR ALL LOSSES,
COSTS, OR OTHER PROBLEMS THEY INCUR DUE TO THE SOFTWARE, AND WHO FURTHERMORE
EFFECTIVELY INDEMNIFY JOHN HAUSER AND THE INTERNATIONAL COMPUTER SCIENCE
INSTITUTE (possibly via similar legal warning) AGAINST ALL LOSSES, COSTS, OR
OTHER PROBLEMS INCURRED BY THEIR CUSTOMERS AND CLIENTS DUE TO THE SOFTWARE.

Derivative works are acceptable, even for commercial purposes, so long as
(1) the source code for the derivative work includes prominent notice that
the work is derivative, and (2) the source code includes prominent notice with
these four paragraphs for those parts of this code that are retained.

=============================================================================*/


/*----------------------------------------------------------------------------
| The macro `FLOATX80' must be defined to enable the extended double-precision
| floating-point format `floatx80'.  If this macro is not defined, the
| `floatx80' type will not be defined, and none of the functions that either
| input or output the `floatx80' type will be defined.  The same applies to
| the `FLOAT128' macro and the quadruple-precision format `float128'.
*----------------------------------------------------------------------------*/
//#define FLOATX80
//#define FLOAT128

/*----------------------------------------------------------------------------
| Software IEC/IEEE floating-point types.
*----------------------------------------------------------------------------*/
typedef __bits32 float32;
typedef __bits64 float64;
#ifdef FLOATX80
typedef struct {
    __bits16 high;
    __bits64 low;
} floatx80;
#endif
#ifdef FLOAT128
typedef struct {
    __bits64 high, low;
} float128;
#endif


/**
 * View convert expr data structures
*/
typedef float Tfloat32;
typedef double Tfloat64;
#ifdef FLOATX80
typedef long double Tfloatx80;
#endif
#ifdef FLOAT128
typedef __float128 Tfloat128;
#endif
typedef union 
{ float32 b;
  Tfloat32 f;
} convert32;
typedef union 
{ float64 b;
  Tfloat64 f;
} convert64;
#ifdef FLOATX80
typedef union 
{ floatx80 b;
  Tfloatx80 f;
} convertx80;
#endif
#ifdef FLOAT128
typedef union 
{ float128 b;
  Tfloat128 f;
} convert128;
#endif

/**
 * Floating point macro interfaces
*/
#define SF_ADAPTER1(fun_name, prec) \
inline Tfloat##prec fun_name##if(Tfloat##prec a, Tfloat##prec b) {\
  convert##prec a_c, b_c, res_c;\
  a_c.f = a;\
  b_c.f = b;\
  res_c.b = fun_name(a_c.b, b_c.b);\
  return res_c.f;\
}
#define SF_ADAPTER1_unary(fun_name, prec) \
inline Tfloat##prec fun_name##if(Tfloat##prec a) {\
  convert##prec a_c, res_c;\
  a_c.f = a;\
  res_c.b = fun_name(a_c.b);\
  return res_c.f;\
}
#define SF_ADAPTER2(fun_name, prec) \
inline __flag fun_name##if(Tfloat##prec a, Tfloat##prec b) {\
  convert##prec a_c, b_c, res_c;\
  a_c.f = a;\
  b_c.f = b;\
  return fun_name(a_c.b, b_c.b);\
}
#define SF_ADAPTER2_unary(fun_name, prec_in, prec_out) \
inline __int##prec_out fun_name##if(Tfloat##prec_in a) {\
  convert##prec_in a_c;\
  a_c.f = a;\
  return fun_name(a_c.b);\
}
#define SF_UADAPTER2_unary(fun_name, prec_in, prec_out) \
inline __uint##prec_out fun_name##if(Tfloat##prec_in a) {\
  convert##prec_in a_c;\
  a_c.f = a;\
  return fun_name(a_c.b);\
}
#define SF_ADAPTER3_unary(fun_name, prec_in, prec_out) \
inline Tfloat##prec_out fun_name##if(__int##prec_in a) {\
  convert##prec_out res_c;\
  res_c.b = fun_name(a);\
  return res_c.f;\
}
#define SF_UADAPTER3_unary(fun_name, prec_in, prec_out) \
inline Tfloat##prec_out fun_name##if(__uint##prec_in a) {\
  convert##prec_out res_c;\
  res_c.b = fun_name(a);\
  return res_c.f;\
}
#define SF_ADAPTER4_unary(fun_name, prec_in, prec_out) \
inline Tfloat##prec_out fun_name##if(Tfloat##prec_in a) {\
  convert##prec_in a_c;\
  convert##prec_out res_c;\
  a_c.f = a;\
  res_c.b = fun_name(a_c.b);\
  return res_c.f;\
}

/*----------------------------------------------------------------------------
| Software IEC/IEEE floating-point underflow tininess-detection mode.
*----------------------------------------------------------------------------*/
#ifdef NO_PARAMETRIC
static const __int8 float_detect_tininess = 0;
#else
extern __int8 float_detect_tininess;
#endif
enum {
    float_tininess_after_rounding  = 0,
    float_tininess_before_rounding = 1
};

/*----------------------------------------------------------------------------
| Software IEC/IEEE floating-point rounding mode.
*----------------------------------------------------------------------------*/
#ifdef NO_PARAMETRIC
static const __int8 float_rounding_mode = 0;
#else
extern __int8 float_rounding_mode;
#endif
enum {
    float_round_nearest_even = 0,
    float_round_to_zero      = 1,
    float_round_down         = 2,
    float_round_up           = 3
};

/*----------------------------------------------------------------------------
| Software IEC/IEEE floating-point exception flags.
*----------------------------------------------------------------------------*/
#ifdef NO_PARAMETRIC
static const __int8 float_exception_flags = 0;
#else
extern __int8 float_exception_flags;
#endif
enum {
    float_flag_inexact   =  1,
    float_flag_underflow =  2,
    float_flag_overflow  =  4,
    float_flag_divbyzero =  8,
    float_flag_invalid   = 16
};

/*----------------------------------------------------------------------------
| Routine to raise any or all of the software IEC/IEEE floating-point
| exception flags.
*----------------------------------------------------------------------------*/
void float_raise( __int8 );

/*----------------------------------------------------------------------------
| Software IEC/IEEE integer-to-floating-point conversion routines.
*----------------------------------------------------------------------------*/
float32 int32_to_float32( __int32 );SF_ADAPTER3_unary(int32_to_float32,32,32);
float32 uint32_to_float32( __uint32 );SF_UADAPTER3_unary(uint32_to_float32,32,32);
float64 int32_to_float64( __int32 );SF_ADAPTER3_unary(int32_to_float64,32,64);
float64 uint32_to_float64( __uint32 );SF_UADAPTER3_unary(uint32_to_float64,32,64);
#ifdef FLOATX80
floatx80 int32_to_floatx80( __int32 );
#endif
#ifdef FLOAT128
float128 int32_to_float128( __int32 );
#endif
float32 int64_to_float32( __int64 );SF_ADAPTER3_unary(int64_to_float32,64,32);
float64 uint64_to_float32( __uint64 ); SF_UADAPTER3_unary(uint64_to_float32,64,32);
float64 int64_to_float64( __int64 );SF_ADAPTER3_unary(int64_to_float64,64,64);
float64 uint64_to_float64( __uint64 a);SF_UADAPTER3_unary(uint64_to_float64,64,64);
#ifdef FLOATX80
floatx80 int64_to_floatx80( __int64 );
#endif
#ifdef FLOAT128
float128 int64_to_float128( __int64 );
#endif

/*----------------------------------------------------------------------------
| Software IEC/IEEE single-precision conversion routines.
*----------------------------------------------------------------------------*/
__int32 float32_to_int32( float32 );SF_ADAPTER2_unary(float32_to_int32,32,32);
__int32 float32_to_int32_round_to_zero( float32 );SF_ADAPTER2_unary(float32_to_int32_round_to_zero,32,32);
__uint32 float32_to_uint32_round_to_zero( float32 a); SF_UADAPTER2_unary(float32_to_uint32_round_to_zero,32,32);
__int64 float32_to_int64( float32 );SF_ADAPTER2_unary(float32_to_int64,32,64);
__int64 float32_to_int64_round_to_zero( float32 );SF_ADAPTER2_unary(float32_to_int64_round_to_zero,32,64);
__uint64 float32_to_uint64_round_to_zero( float32 ); SF_UADAPTER2_unary(float32_to_uint64_round_to_zero,32,64);
float64 float32_to_float64( float32 );SF_ADAPTER4_unary(float32_to_float64,32,64);
#ifdef FLOATX80
floatx80 float32_to_floatx80( float32 );SF_ADAPTER4_unary(float32_to_floatx80,32,x80);
#endif
#ifdef FLOAT128
float128 float32_to_float128( float32 );SF_ADAPTER4_unary(float32_to_float128,32,128);
#endif

/*----------------------------------------------------------------------------
| Software IEC/IEEE single-precision operations.
*----------------------------------------------------------------------------*/
float32 float32_round_to_int( float32 );
float32 float32_add( float32, float32 );SF_ADAPTER1(float32_add,32);
float32 float32_sub( float32, float32 );SF_ADAPTER1(float32_sub,32);
float32 float32_mul( float32, float32 );SF_ADAPTER1(float32_mul,32);
float32 float32_div( float32, float32 );SF_ADAPTER1(float32_div,32);
float32 float32_rem( float32, float32 );
float32 float32_sqrt( float32 );SF_ADAPTER1_unary(float32_sqrt,32); inline Tfloat32 __builtin_sqrtf(Tfloat32 a) {return float32_sqrtif(a);}
__flag float32_eq( float32, float32 );
__flag float32_le( float32, float32 );SF_ADAPTER2(float32_le,32);
__flag float32_lt( float32, float32 );SF_ADAPTER2(float32_lt,32);
inline __flag float32_ge( float32 a, float32 b) {return !float32_lt(a,b);}SF_ADAPTER2(float32_ge,32);
inline __flag float32_gt( float32 a, float32 b) {return !float32_le(a,b);}SF_ADAPTER2(float32_gt,32);
__flag float32_eq_signaling( float32, float32 );
__flag float32_le_quiet( float32, float32 );
__flag float32_lt_quiet( float32, float32 );
__flag float32_is_signaling_nan( float32 );

/*----------------------------------------------------------------------------
| Software IEC/IEEE double-precision conversion routines.
*----------------------------------------------------------------------------*/
__int32 float64_to_int32( float64 );SF_ADAPTER2_unary(float64_to_int32,64,32);
__int32 float64_to_int32_round_to_zero( float64 );SF_ADAPTER2_unary(float64_to_int32_round_to_zero,64,32);
__uint32 float64_to_uint32_round_to_zero( float64 a); SF_UADAPTER2_unary(float64_to_uint32_round_to_zero,64,32);
__int64 float64_to_int64( float64 );SF_ADAPTER2_unary(float64_to_int64,64,64);
__int64 float64_to_int64_round_to_zero( float64 );SF_ADAPTER2_unary(float64_to_int64_round_to_zero,64,64);
__uint64 float64_to_uint64_round_to_zero( float64 a); SF_UADAPTER2_unary(float64_to_uint64_round_to_zero,64,64);
float32 float64_to_float32( float64 );SF_ADAPTER4_unary(float64_to_float32,64,32);
#ifdef FLOATX80
floatx80 float64_to_floatx80( float64 );SF_ADAPTER4_unary(float64_to_floatx80,64,x80);
#endif
#ifdef FLOAT128
float128 float64_to_float128( float64 );SF_ADAPTER4_unary(float64_to_float128,64,128);
#endif

/*----------------------------------------------------------------------------
| Software IEC/IEEE double-precision operations.
*----------------------------------------------------------------------------*/
float64 float64_round_to_int( float64 );
float64 float64_add( float64, float64 );SF_ADAPTER1(float64_add,64);
float64 float64_sub( float64, float64 );SF_ADAPTER1(float64_sub,64);
float64 float64_mul( float64, float64 );SF_ADAPTER1(float64_mul,64);
float64 float64_div( float64, float64 );SF_ADAPTER1(float64_div,64);
float64 float64_rem( float64, float64 );
float64 float64_sqrt( float64 );SF_ADAPTER1_unary(float64_sqrt,64); inline Tfloat64 __builtin_sqrt(Tfloat64 a) {return float64_sqrtif(a);}
__flag float64_eq( float64, float64 );
__flag float64_le( float64, float64 );SF_ADAPTER2(float64_le,64);
__flag float64_lt( float64, float64 );SF_ADAPTER2(float64_lt,64);
inline __flag float64_ge( float64 a, float64 b) {return !float64_lt(a,b);}SF_ADAPTER2(float64_ge,64);
inline __flag float64_gt( float64 a, float64 b) {return !float64_le(a,b);}SF_ADAPTER2(float64_gt,64);
__flag float64_eq_signaling( float64, float64 );
__flag float64_le_quiet( float64, float64 );
__flag float64_lt_quiet( float64, float64 );
__flag float64_is_signaling_nan( float64 );

#ifdef FLOATX80

/*----------------------------------------------------------------------------
| Software IEC/IEEE extended double-precision conversion routines.
*----------------------------------------------------------------------------*/
__int32 floatx80_to_int32( floatx80 );
__int32 floatx80_to_int32_round_to_zero( floatx80 );
__int64 floatx80_to_int64( floatx80 );
__int64 floatx80_to_int64_round_to_zero( floatx80 );
float32 floatx80_to_float32( floatx80 );
float64 floatx80_to_float64( floatx80 );
#ifdef FLOAT128
float128 floatx80_to_float128( floatx80 );
#endif

/*----------------------------------------------------------------------------
| Software IEC/IEEE extended double-precision rounding precision.  Valid
| values are 32, 64, and 80.
*----------------------------------------------------------------------------*/
extern __int8 floatx80_rounding_precision;

/*----------------------------------------------------------------------------
| Software IEC/IEEE extended double-precision operations.
*----------------------------------------------------------------------------*/
floatx80 floatx80_round_to_int( floatx80 );
floatx80 floatx80_add( floatx80, floatx80 );SF_ADAPTER1(floatx80_add,x80);
floatx80 floatx80_sub( floatx80, floatx80 );SF_ADAPTER1(floatx80_sub,x80);
floatx80 floatx80_mul( floatx80, floatx80 );SF_ADAPTER1(floatx80_mul,x80);
floatx80 floatx80_div( floatx80, floatx80 );SF_ADAPTER1(floatx80_div,x80);
floatx80 floatx80_rem( floatx80, floatx80 );
floatx80 floatx80_sqrt( floatx80 );SF_ADAPTER1_unary(floatx80_sqrt,x80); inline Tfloatx80 __builtin_sqrtl(Tfloatx80 a) {return floatx80_sqrtif(a);}
__flag floatx80_eq( floatx80, floatx80 );
__flag floatx80_le( floatx80, floatx80 );SF_ADAPTER2(floatx80_le,x80);
__flag floatx80_lt( floatx80, floatx80 );SF_ADAPTER2(floatx80_lt,x80);
inline __flag floatx80_ge( floatx80 a, floatx80 b) {return !floatx80_lt(a,b);}SF_ADAPTER2(floatx80_ge,x80);
inline __flag floatx80_gt( floatx80 a, floatx80 b) {return !floatx80_le(a,b);}SF_ADAPTER2(floatx80_gt,x80);
__flag floatx80_eq_signaling( floatx80, floatx80 );
__flag floatx80_le_quiet( floatx80, floatx80 );
__flag floatx80_lt_quiet( floatx80, floatx80 );
__flag floatx80_is_signaling_nan( floatx80 );

#endif

#ifdef FLOAT128

/*----------------------------------------------------------------------------
| Software IEC/IEEE quadruple-precision conversion routines.
*----------------------------------------------------------------------------*/
__int32 float128_to_int32( float128 );
__int32 float128_to_int32_round_to_zero( float128 );
__int64 float128_to_int64( float128 );
__int64 float128_to_int64_round_to_zero( float128 );
float32 float128_to_float32( float128 );
float64 float128_to_float64( float128 );
#ifdef FLOATX80
floatx80 float128_to_floatx80( float128 );
#endif

/*----------------------------------------------------------------------------
| Software IEC/IEEE quadruple-precision operations.
*----------------------------------------------------------------------------*/
float128 float128_round_to_int( float128 );
float128 float128_add( float128, float128 );SF_ADAPTER1(float128_add,128);
float128 float128_sub( float128, float128 );SF_ADAPTER1(float128_sub,128);
float128 float128_mul( float128, float128 );SF_ADAPTER1(float128_mul,128);
float128 float128_div( float128, float128 );SF_ADAPTER1(float128_div,128);
float128 float128_rem( float128, float128 );
float128 float128_sqrt( float128 );SF_ADAPTER1_unary(float128_sqrt,128); inline Tfloat128 __builtin_sqrtl(Tfloat128 a) {return float128_sqrtif(a);}
__flag float128_eq( float128, float128 );
__flag float128_le( float128, float128 );SF_ADAPTER2(float128_le,128);
__flag float128_lt( float128, float128 );SF_ADAPTER2(float128_lt,128);
inline __flag float128_ge( float128 a, float128 b) {return !float128_lt(a,b);}SF_ADAPTER2(float128_ge,128);
inline __flag float128_gt( float128 a, float128 b) {return !float128_le(a,b);}SF_ADAPTER2(float128_gt,128);
__flag float128_eq_signaling( float128, float128 );
__flag float128_le_quiet( float128, float128 );
__flag float128_lt_quiet( float128, float128 );
__flag float128_is_signaling_nan( float128 );

#endif

