/** 
 * Porting of the libm library to the PandA framework 
 * starting from the original FDLIBM 5.3 (Freely Distributable LIBM) developed by SUN
 * plus the newlib version 1.19 from RedHat and plus uClibc version 0.9.32.1 developed by Erik Andersen.
 * The author of this port is Fabrizio Ferrandi from Politecnico di Milano.
 * The porting fall under the LGPL v2.1, see the files COPYING.LIB and COPYING.LIBM_PANDA in this directory.
 * Date: September, 11 2013.
*/
/* sincos -- currently no more efficient than two separate calls to
   sin and cos. */

#include "math_private.h"

#ifndef _DOUBLE_IS_32BITS

void __attribute__((optimize("O0"))) __builtin_sincos(double x, double *sinx, double *cosx)
{
  *sinx = __builtin_sin (x);
  *cosx = __builtin_cos (x);
}

double _Complex __attribute__((optimize("O0"))) __builtin_cexpi (double _Complex Z)
{
  double _Complex  Res;
  __real__ Res = __builtin_cosf(__imag__ Z);
  __imag__ Res = __builtin_sinf(__imag__ Z);
  return Res;
}


#endif /* defined(_DOUBLE_IS_32BITS) */
