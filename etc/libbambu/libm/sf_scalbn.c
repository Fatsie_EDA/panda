/** 
 * Porting of the libm library to the PandA framework 
 * starting from the original FDLIBM 5.3 (Freely Distributable LIBM) developed by SUN
 * plus the newlib version 1.19 from RedHat and plus uClibc version 0.9.32.1 developed by Erik Andersen.
 * The author of this port is Fabrizio Ferrandi from Politecnico di Milano.
 * The porting fall under the LGPL v2.1, see the files COPYING.LIB and COPYING.LIBM_PANDA in this directory.
 * Date: September, 11 2013.
*/
/* sf_scalbn.c -- float version of s_scalbn.c.
 * Conversion to float by Ian Lance Taylor, Cygnus Support, ian@cygnus.com.
 */

/*
 * ====================================================
 * Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
 *
 * Developed at SunPro, a Sun Microsystems, Inc. business.
 * Permission to use, copy, modify, and distribute this
 * software is freely granted, provided that this notice 
 * is preserved.
 * ====================================================
 */

#include "math_privatef.h"

float __builtin_scalbnf (float x, int n)
{
	float scale;

	if (n > 127) {
		x *= 0x1p127f;
		n -= 127;
		if (n > 127) {
			x *= 0x1p127f;
			n -= 127;
			if (n > 127) {
				STRICT_ASSIGN(float, x, x * 0x1p127f);
				return x;
			}
		}
	} else if (n < -126) {
		x *= 0x1p-126f;
		n += 126;
		if (n < -126) {
			x *= 0x1p-126f;
			n += 126;
			if (n < -126) {
				STRICT_ASSIGN(float, x, x * 0x1p-126f);
				return x;
			}
		}
	}
	SET_FLOAT_WORD(scale, (unsigned int)(0x7f+n)<<23);
	STRICT_ASSIGN(float, x, x * scale);
	return x;
}

#ifdef _DOUBLE_IS_32BITS

double __builtin_scalbn(double x, int n)
{
	return (double) __builtin_scalbnf((float) x, n);
}

#endif /* defined(_DOUBLE_IS_32BITS) */
