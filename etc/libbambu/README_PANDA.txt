September, 11 2013.

The libbambu subdirectory is a collection of public domain implementations of libc functions.
The memory allocator is a porting of the memory manager allocator written by by Bendersky (eliben@gmail.com)

For further information send an e-mail to panda-info@elet.polimi.it.

Fabrizio Ferrandi Politecnico di Milano (Italy)
