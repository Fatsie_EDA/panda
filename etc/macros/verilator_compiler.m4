dnl
dnl check for Verilator Verilog Compiler
dnl
AC_DEFUN([AC_CHECK_VERILATOR],[

dnl 
dnl Check for Verilator Verilog Compiler
dnl
verilator_executable=`which verilator`
if test -n "$verilator_executable"; then
  echo "checking Verilator Verilog Compiler... yes: $verilator_executable"
fi
if test "x$verilator_executable" = x; then
   panda_USE_VERILATOR=no;
   AC_MSG_ERROR(Verilator Verilog Compiler NOT correctly configured! Please install it)
else
   panda_USE_VERILATOR=yes;
fi

AC_PROVIDE([$0])dnl
])

