dnl
dnl check where the LATTICE synthesis tools are
dnl
AC_DEFUN([AC_CHECK_LATTICE],[
    AC_ARG_WITH(lattice-dir,
    [  --with-lattice-dir=DIR  set where the root of LATTICE tools are installed ],
    [
       ac_lattice_dir="$withval"
    ])

if test "x$ac_lattice_dir" = x; then
   ac_lattice_dirs="/usr/local/diamond/*";
else
   ac_lattice_dirs=$ac_lattice_dir;
fi
enable_lattice_64bit=no;
for dirs in $ac_lattice_dirs; do
      for dir in $dirs; do
         if test -n "`ls -1 $dir/bin/lin/diamondc 2> /dev/null`"; then
            ac_lattice_settings="export TEMP=/tmp;export LSC_INI_PATH=\"\";export LSC_DIAMOND=true;export TCL_LIBRARY=$dir/tcltk/lib/tcl8.5;export FOUNDRY=$dir/ispfpga;export PATH=$FOUNDRY/bin/lin:$dir/bin/lin:$PATH";
            LATTICE_SETTINGS=$ac_lattice_settings
            ac_lattice_dir=$dir
            echo "checking if LATTICE diamondc [32bit] is present in $dir... yes"
         else
            echo "checking if LATTICE diamondc [32bit] is present in $dir... no"
         fi
         if test -n "`ls -1 $dir/bin/lin64/diamondc 2> /dev/null`"; then
            ac_lattice_settings="export TEMP=/tmp;export LSC_INI_PATH=\"\";export LSC_DIAMOND=true;export TCL_LIBRARY=$dir/tcltk/lib/tcl8.5;export FOUNDRY=$dir/ispfpga;export PATH=$FOUNDRY/bin/lin64:$dir/bin/lin64:$PATH";
            LATTICE_SETTINGS=$ac_lattice_settings
            ac_lattice_dir=$dir
            enable_lattice_64bit=yes
            echo "checking if LATTICE diamondc [64bit] is present in $dir... yes"
         else
            echo "checking if LATTICE diamondc [64bit] is present in $dir... no"
         fi
      done
   done

if test "x$ac_lattice_settings" != "x"; then
   AC_DEFINE_UNQUOTED(LATTICE_SETTINGS, "${LATTICE_SETTINGS}", "Define the lattice settings script")
else
   AC_MSG_ERROR(LATTICE settings script required)
fi

if test "x$enable_lattice_64bit" = xyes; then
  AC_DEFINE(HAVE_LATTICE_64bit, 1, "define if lattice distribution has a working 64bit version")
fi
AC_PROVIDE([$0])dnl
])



