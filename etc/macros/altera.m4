dnl
dnl check where the ALTERA synthesis tools are
dnl
AC_DEFUN([AC_CHECK_ALTERA],[
    AC_ARG_WITH(altera-dir,
    [  --with-altera-dir=DIR   set where the root of ALTERA tools are installed ],
    [
       ac_altera_dir="$withval"
    ])
    AC_ARG_ENABLE(altera-64bit,
    [  --enable-altera-64bit        enable the 64bit version],
    [enable_altera_64bit=yes],
    [enable_altera_64bit=no])

if test "x$ac_altera_dir" = x; then
   ac_altera_dirs="/opt/altera/quartus /opt/altera/*/quartus";
else
   ac_altera_dirs=$ac_altera_dir;
fi

for dirs in $ac_altera_dirs; do
      for dir in $dirs; do
         if test -n "`ls -1 $dir/bin/quartus_sh 2> /dev/null`"; then
            ac_altera_settings="export PATH=$PATH:$dir/bin/";
            ALTERA_SETTINGS=$ac_altera_settings
            ac_altera_dir=$dir
            echo "checking if ALTERA quartus_sh is present in $dir... yes"
         else
            echo "checking if ALTERA quartus_sh is present in $dir... no"
         fi
      done
   done

if test "x$ac_altera_settings" != "x"; then
   AC_DEFINE_UNQUOTED(ALTERA_SETTINGS, "${ALTERA_SETTINGS}", "Define the altera settings script")
else
   AC_MSG_ERROR(ALTERA settings script required)
fi

if test "x$enable_altera_64bit" = xyes; then
  AC_DEFINE(HAVE_ALTERA_64bit, 1, "define if altera distribution has a working 64bit version")
fi
AC_PROVIDE([$0])dnl
])



