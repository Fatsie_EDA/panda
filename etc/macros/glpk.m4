dnl
dnl check for GLPK solver
dnl
AC_DEFUN([AC_CHECK_GLPK],[

AC_CHECK_HEADERS([glpk.h])
if test "$ac_cv_header_glpk_h" = yes; then
  LIBS="-lglpk $LIBS"
  AC_DEFINE(HAVE_GLPK, 1, "Define if glpk is used")
else
  AC_MSG_ERROR("Error in glpk configuration")
fi

AC_PROVIDE([$0])dnl
])

