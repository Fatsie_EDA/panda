=========== PANDA 0.9.2 ===========
mer 12 feb 2014, 13.22.48, CET PandA release 0.9.2
Third public release of PandA framework. 

New features introduced:
- added an initial support to GCC 4.9.0,
- stable support to GCC versions: v4.5, v4.6, v4.7 (default) and v4.8,
- added an experimental support to Verilator simulator,
- new dataflow dependency analysis for LOADs and STOREs; we now use GCC alias analysis to see if a LOAD and STORE pair or a STORE and STORE pair may conflict,
- added a frontend step that optimizes PHI nodes,
- added a frontend step that performs conditionally if conversions,
- added a frontend step that performs simple code motions,
- added a frontend step that converts if chains in a single multi-if control construct,
- added a frontend step that simplifies short circuits based control constructs,
- added a proxy-based approach to the LOADs/STOREs of statically resolved pointers,
- improved EBR inference for Lattice based devices,
- now, memory models are different for Lattice, Altera, Virtex5 and Virtex6-7 based devices,
- updated FloPoCo to a more recent version,
- now, register allocation maps storage values on registers without write enable when possible,
- added support to CentOS/Scientific Linux distributions,
- added support to ArchLinux distribution,
- added support to Ubuntu 13.10 distribution,
- now, testbenches accept a user defined error for float based computations; the error is specified in ULPs units; a Unit in the Last Place is the spacing between floating-point numbers,
- improved architectural timing model,
- added a very simple symbolic estimator of number of cycles taken by a function, it mainly covers function without loops and without unbounded operations,
- general refactoring of automatic HLS testbench generation,
- added support to libm function lceil and lceilf,
- added skip-pipe-parameter option to bambu; it is is used to select a faster pipelined unit (xilinx devices have the default equal to 1 while lattice and altera devices have the default equal to 0),
- improved memory allocation when byte-enabled write memories are not needed,
- added support to variable lenght arrays,
- added support to memalign builtin,
- added EXT_PIPELINED_BRAM memory allocation policy, bambu with this option assumes that a block ram memory is allocated outside the core (LOAD with II=1 and latency=2 and STORE with latency=1),
- added 2D matrix multiplication examples for integers and single precision floating point numbers,
- added some synthesis scripts checking bambu quality of results w.r.t. 72 single precision libm functions (e.g., sqrtf, sinf, expf, tanhf, etc.),
- added spider tool to automatically generate latex tables from bambu synthesis results,
- moved all the dot generated files into directory HLS_output/dot/. Such files (scheduling, FSM, CFG, DFG, etc) are now generated when --print-dot option is passed,
- VIVADO is now the default backend flow for Zynq based devices.

Problems fixed:
- fixed all the Bison related compilation problems,
- fixed some problems with testbench generation of 2D arrays,
- fixed configuration scripts for manually installed Boost libraries; now, we need at least Boost 1.48.0,
- fixed some problems with C pretty-printing of the internal IR,
- fixed some ISE/Vivado synthesis problems when System Verilog based model are used,
- fixed some problems with --soft-float based synthesis,
- fixed RTL-backend configuration scripts looking for tools (e.g., ISE, Vivado, Quartus and Diamond) already installed,
- fixed some problems with real-to-int and int-to-real conversions, added some explicit tests to the panda regressions.

Main contributions to this release come from Fabrizio Ferrandi, Marco Minutoli, Marco Lattuada from Politecnico di Milano (Italy).
Many thanks to Razvan Nane, Berke Durak and Staf Verhaegen for their feedbacks and suggestions.

=========== PANDA 0.9.1 ===========
mar 17 set 2013, 12.33.30, CET PandA release 0.9.1

Second public release of PandA framework. 
New features introduced:
- complete support of CHSTone benchmarks synthesis and verification (http://www.ertl.jp/chstone/),
- better support of multi-ported memories,
- local memory exploitation,
- read-only-memory exploitation,
- support of multi-bus for parallel memory accesses,
- support of unaligned memory accesses,
- better support of pipelined resources,
- improved module binding algorithms (e.g., slack-based module binding),
- support of resource constraints through user xml file,
- support of libc primitives: memcpy, memcmp, memset and memmove,
- better support of printf primitive for RTL debugging purposes,
- support of dynamic memory allocation,
- synthesis of libm builtin primitives such as sin, cos, acosh, etc,
- better integration with FloPoCo library (http://flopoco.gforge.inria.fr/),
- soft-float based HW synthesis,
- support of Vivado Xilinx backend,
- support of Diamond Lattice backend,
- support of XSIM Xilinx simulator,
- synthesis and testbench generation of WISHBONE B4 Compliant Accelerators (see http://cdn.opencores.org/downloads/wbspec_b4.pdf for details on the WISHBONE specification),
- synthesis of AXI4LITE Compliant Accelerators (experimental),
- inclusion of GCC regression tests to test HLS synthesis (tested HLS synthesis and RTL simulation),
- inclusion of libm regression tests to test HLS synthesis of libm (tested HLS synthesis and RTL simulation),
- support of multiple versions of GCC compiler: v4.5, v4.6 and v4.7.
- support of GCC vectorizing capability (experimental).

Main contributions to this release come from Fabrizio Ferrandi, Christian Pilato, Marco Minutoli, Marco Lattuada, Vito Giovanni Castellana and Silvia Lovergine from Politecnico di Milano (Italy).
Many thanks to Jorge Lacoste for his feedbacks and suggestions.

=========== PANDA 0.9.0 ===========
Wed Mar 14 06:50:32 CET 2012 PandA release 0.9.0

The first public release of the PandA framework mainly covers the high-level synthesis of C based descriptions.
Main contributions to this release come from Fabrizio Ferrandi, Christian Pilato and Marco Lattuada from Politecnico di Milano (Italy). 
