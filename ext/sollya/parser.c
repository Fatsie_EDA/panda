/* A Bison parser, made by GNU Bison 2.7.12-4996.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.7.12-4996"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
/* Line 371 of yacc.c  */
#line 56 "parser.y"

#define YYERROR_VERBOSE 1
#define YYPARSE_PARAM scanner
#define YYLEX_PARAM   scanner

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "expression.h"
#include "assignment.h"
#include "chain.h"
#include "general.h"
#include "execute.h"
#include "parser.h"
#include "library.h"
#include "help.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

extern int yylex(YYSTYPE *lvalp, void *scanner);
extern FILE *yyget_in(void *scanner);
extern char *getCurrentLexSymbol();

void yyerror(void * YYSTYPE, char *message) {
  char *str;
  if (!feof(yyget_in(scanner))) {
    str = getCurrentLexSymbol();
    printMessage(1,"Warning: %s.\nThe last symbol read has been \"%s\".\nWill skip input until next semicolon after the unexpected token. May leak memory.\n",message,str);
    free(str);
    promptToBePrinted = 1;
    lastWasSyntaxError = 1;
    considerDyingOnError();
  } 
}

int parserCheckEof() {
  FILE *myFd;

  myFd = yyget_in(scanner);
  if (myFd == NULL) return 0;
  
  return feof(myFd);
}

// #define WARN_IF_NO_HELP_TEXT 1


/* Line 371 of yacc.c  */
#line 119 "parser.c"

# ifndef YY_NULL
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULL nullptr
#  else
#   define YY_NULL 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     CONSTANTTOKEN = 258,
     MIDPOINTCONSTANTTOKEN = 259,
     DYADICCONSTANTTOKEN = 260,
     HEXCONSTANTTOKEN = 261,
     HEXADECIMALCONSTANTTOKEN = 262,
     BINARYCONSTANTTOKEN = 263,
     PITOKEN = 264,
     IDENTIFIERTOKEN = 265,
     STRINGTOKEN = 266,
     LPARTOKEN = 267,
     RPARTOKEN = 268,
     LBRACKETTOKEN = 269,
     RBRACKETTOKEN = 270,
     EQUALTOKEN = 271,
     ASSIGNEQUALTOKEN = 272,
     COMPAREEQUALTOKEN = 273,
     COMMATOKEN = 274,
     EXCLAMATIONTOKEN = 275,
     SEMICOLONTOKEN = 276,
     STARLEFTANGLETOKEN = 277,
     LEFTANGLETOKEN = 278,
     RIGHTANGLEUNDERSCORETOKEN = 279,
     RIGHTANGLEDOTTOKEN = 280,
     RIGHTANGLESTARTOKEN = 281,
     RIGHTANGLETOKEN = 282,
     DOTSTOKEN = 283,
     DOTTOKEN = 284,
     QUESTIONMARKTOKEN = 285,
     VERTBARTOKEN = 286,
     ATTOKEN = 287,
     DOUBLECOLONTOKEN = 288,
     COLONTOKEN = 289,
     DOTCOLONTOKEN = 290,
     COLONDOTTOKEN = 291,
     EXCLAMATIONEQUALTOKEN = 292,
     APPROXTOKEN = 293,
     ANDTOKEN = 294,
     ORTOKEN = 295,
     PLUSTOKEN = 296,
     MINUSTOKEN = 297,
     MULTOKEN = 298,
     DIVTOKEN = 299,
     POWTOKEN = 300,
     SQRTTOKEN = 301,
     EXPTOKEN = 302,
     LOGTOKEN = 303,
     LOG2TOKEN = 304,
     LOG10TOKEN = 305,
     SINTOKEN = 306,
     COSTOKEN = 307,
     TANTOKEN = 308,
     ASINTOKEN = 309,
     ACOSTOKEN = 310,
     ATANTOKEN = 311,
     SINHTOKEN = 312,
     COSHTOKEN = 313,
     TANHTOKEN = 314,
     ASINHTOKEN = 315,
     ACOSHTOKEN = 316,
     ATANHTOKEN = 317,
     ABSTOKEN = 318,
     ERFTOKEN = 319,
     ERFCTOKEN = 320,
     LOG1PTOKEN = 321,
     EXPM1TOKEN = 322,
     DOUBLETOKEN = 323,
     SINGLETOKEN = 324,
     HALFPRECISIONTOKEN = 325,
     QUADTOKEN = 326,
     DOUBLEDOUBLETOKEN = 327,
     TRIPLEDOUBLETOKEN = 328,
     DOUBLEEXTENDEDTOKEN = 329,
     CEILTOKEN = 330,
     FLOORTOKEN = 331,
     NEARESTINTTOKEN = 332,
     HEADTOKEN = 333,
     REVERTTOKEN = 334,
     SORTTOKEN = 335,
     TAILTOKEN = 336,
     MANTISSATOKEN = 337,
     EXPONENTTOKEN = 338,
     PRECISIONTOKEN = 339,
     ROUNDCORRECTLYTOKEN = 340,
     PRECTOKEN = 341,
     POINTSTOKEN = 342,
     DIAMTOKEN = 343,
     DISPLAYTOKEN = 344,
     VERBOSITYTOKEN = 345,
     CANONICALTOKEN = 346,
     AUTOSIMPLIFYTOKEN = 347,
     TAYLORRECURSIONSTOKEN = 348,
     TIMINGTOKEN = 349,
     TIMETOKEN = 350,
     FULLPARENTHESESTOKEN = 351,
     MIDPOINTMODETOKEN = 352,
     DIEONERRORMODETOKEN = 353,
     SUPPRESSWARNINGSTOKEN = 354,
     RATIONALMODETOKEN = 355,
     HOPITALRECURSIONSTOKEN = 356,
     ONTOKEN = 357,
     OFFTOKEN = 358,
     DYADICTOKEN = 359,
     POWERSTOKEN = 360,
     BINARYTOKEN = 361,
     HEXADECIMALTOKEN = 362,
     FILETOKEN = 363,
     POSTSCRIPTTOKEN = 364,
     POSTSCRIPTFILETOKEN = 365,
     PERTURBTOKEN = 366,
     MINUSWORDTOKEN = 367,
     PLUSWORDTOKEN = 368,
     ZEROWORDTOKEN = 369,
     NEARESTTOKEN = 370,
     HONORCOEFFPRECTOKEN = 371,
     TRUETOKEN = 372,
     FALSETOKEN = 373,
     DEFAULTTOKEN = 374,
     MATCHTOKEN = 375,
     WITHTOKEN = 376,
     ABSOLUTETOKEN = 377,
     DECIMALTOKEN = 378,
     RELATIVETOKEN = 379,
     FIXEDTOKEN = 380,
     FLOATINGTOKEN = 381,
     ERRORTOKEN = 382,
     QUITTOKEN = 383,
     FALSEQUITTOKEN = 384,
     RESTARTTOKEN = 385,
     LIBRARYTOKEN = 386,
     LIBRARYCONSTANTTOKEN = 387,
     DIFFTOKEN = 388,
     SIMPLIFYTOKEN = 389,
     REMEZTOKEN = 390,
     BASHEVALUATETOKEN = 391,
     FPMINIMAXTOKEN = 392,
     HORNERTOKEN = 393,
     EXPANDTOKEN = 394,
     SIMPLIFYSAFETOKEN = 395,
     TAYLORTOKEN = 396,
     TAYLORFORMTOKEN = 397,
     AUTODIFFTOKEN = 398,
     DEGREETOKEN = 399,
     NUMERATORTOKEN = 400,
     DENOMINATORTOKEN = 401,
     SUBSTITUTETOKEN = 402,
     COEFFTOKEN = 403,
     SUBPOLYTOKEN = 404,
     ROUNDCOEFFICIENTSTOKEN = 405,
     RATIONALAPPROXTOKEN = 406,
     ACCURATEINFNORMTOKEN = 407,
     ROUNDTOFORMATTOKEN = 408,
     EVALUATETOKEN = 409,
     LENGTHTOKEN = 410,
     INFTOKEN = 411,
     MIDTOKEN = 412,
     SUPTOKEN = 413,
     MINTOKEN = 414,
     MAXTOKEN = 415,
     READXMLTOKEN = 416,
     PARSETOKEN = 417,
     PRINTTOKEN = 418,
     PRINTXMLTOKEN = 419,
     PLOTTOKEN = 420,
     PRINTHEXATOKEN = 421,
     PRINTFLOATTOKEN = 422,
     PRINTBINARYTOKEN = 423,
     PRINTEXPANSIONTOKEN = 424,
     BASHEXECUTETOKEN = 425,
     EXTERNALPLOTTOKEN = 426,
     WRITETOKEN = 427,
     ASCIIPLOTTOKEN = 428,
     RENAMETOKEN = 429,
     INFNORMTOKEN = 430,
     SUPNORMTOKEN = 431,
     FINDZEROSTOKEN = 432,
     FPFINDZEROSTOKEN = 433,
     DIRTYINFNORMTOKEN = 434,
     NUMBERROOTSTOKEN = 435,
     INTEGRALTOKEN = 436,
     DIRTYINTEGRALTOKEN = 437,
     WORSTCASETOKEN = 438,
     IMPLEMENTPOLYTOKEN = 439,
     IMPLEMENTCONSTTOKEN = 440,
     CHECKINFNORMTOKEN = 441,
     ZERODENOMINATORSTOKEN = 442,
     ISEVALUABLETOKEN = 443,
     SEARCHGALTOKEN = 444,
     GUESSDEGREETOKEN = 445,
     DIRTYFINDZEROSTOKEN = 446,
     IFTOKEN = 447,
     THENTOKEN = 448,
     ELSETOKEN = 449,
     FORTOKEN = 450,
     INTOKEN = 451,
     FROMTOKEN = 452,
     TOTOKEN = 453,
     BYTOKEN = 454,
     DOTOKEN = 455,
     BEGINTOKEN = 456,
     ENDTOKEN = 457,
     LEFTCURLYBRACETOKEN = 458,
     RIGHTCURLYBRACETOKEN = 459,
     WHILETOKEN = 460,
     READFILETOKEN = 461,
     ISBOUNDTOKEN = 462,
     EXECUTETOKEN = 463,
     EXTERNALPROCTOKEN = 464,
     VOIDTOKEN = 465,
     CONSTANTTYPETOKEN = 466,
     FUNCTIONTOKEN = 467,
     RANGETOKEN = 468,
     INTEGERTOKEN = 469,
     STRINGTYPETOKEN = 470,
     BOOLEANTOKEN = 471,
     LISTTOKEN = 472,
     OFTOKEN = 473,
     VARTOKEN = 474,
     PROCTOKEN = 475,
     PROCEDURETOKEN = 476,
     RETURNTOKEN = 477,
     NOPTOKEN = 478,
     HELPTOKEN = 479,
     VERSIONTOKEN = 480
   };
#endif
/* Tokens.  */
#define CONSTANTTOKEN 258
#define MIDPOINTCONSTANTTOKEN 259
#define DYADICCONSTANTTOKEN 260
#define HEXCONSTANTTOKEN 261
#define HEXADECIMALCONSTANTTOKEN 262
#define BINARYCONSTANTTOKEN 263
#define PITOKEN 264
#define IDENTIFIERTOKEN 265
#define STRINGTOKEN 266
#define LPARTOKEN 267
#define RPARTOKEN 268
#define LBRACKETTOKEN 269
#define RBRACKETTOKEN 270
#define EQUALTOKEN 271
#define ASSIGNEQUALTOKEN 272
#define COMPAREEQUALTOKEN 273
#define COMMATOKEN 274
#define EXCLAMATIONTOKEN 275
#define SEMICOLONTOKEN 276
#define STARLEFTANGLETOKEN 277
#define LEFTANGLETOKEN 278
#define RIGHTANGLEUNDERSCORETOKEN 279
#define RIGHTANGLEDOTTOKEN 280
#define RIGHTANGLESTARTOKEN 281
#define RIGHTANGLETOKEN 282
#define DOTSTOKEN 283
#define DOTTOKEN 284
#define QUESTIONMARKTOKEN 285
#define VERTBARTOKEN 286
#define ATTOKEN 287
#define DOUBLECOLONTOKEN 288
#define COLONTOKEN 289
#define DOTCOLONTOKEN 290
#define COLONDOTTOKEN 291
#define EXCLAMATIONEQUALTOKEN 292
#define APPROXTOKEN 293
#define ANDTOKEN 294
#define ORTOKEN 295
#define PLUSTOKEN 296
#define MINUSTOKEN 297
#define MULTOKEN 298
#define DIVTOKEN 299
#define POWTOKEN 300
#define SQRTTOKEN 301
#define EXPTOKEN 302
#define LOGTOKEN 303
#define LOG2TOKEN 304
#define LOG10TOKEN 305
#define SINTOKEN 306
#define COSTOKEN 307
#define TANTOKEN 308
#define ASINTOKEN 309
#define ACOSTOKEN 310
#define ATANTOKEN 311
#define SINHTOKEN 312
#define COSHTOKEN 313
#define TANHTOKEN 314
#define ASINHTOKEN 315
#define ACOSHTOKEN 316
#define ATANHTOKEN 317
#define ABSTOKEN 318
#define ERFTOKEN 319
#define ERFCTOKEN 320
#define LOG1PTOKEN 321
#define EXPM1TOKEN 322
#define DOUBLETOKEN 323
#define SINGLETOKEN 324
#define HALFPRECISIONTOKEN 325
#define QUADTOKEN 326
#define DOUBLEDOUBLETOKEN 327
#define TRIPLEDOUBLETOKEN 328
#define DOUBLEEXTENDEDTOKEN 329
#define CEILTOKEN 330
#define FLOORTOKEN 331
#define NEARESTINTTOKEN 332
#define HEADTOKEN 333
#define REVERTTOKEN 334
#define SORTTOKEN 335
#define TAILTOKEN 336
#define MANTISSATOKEN 337
#define EXPONENTTOKEN 338
#define PRECISIONTOKEN 339
#define ROUNDCORRECTLYTOKEN 340
#define PRECTOKEN 341
#define POINTSTOKEN 342
#define DIAMTOKEN 343
#define DISPLAYTOKEN 344
#define VERBOSITYTOKEN 345
#define CANONICALTOKEN 346
#define AUTOSIMPLIFYTOKEN 347
#define TAYLORRECURSIONSTOKEN 348
#define TIMINGTOKEN 349
#define TIMETOKEN 350
#define FULLPARENTHESESTOKEN 351
#define MIDPOINTMODETOKEN 352
#define DIEONERRORMODETOKEN 353
#define SUPPRESSWARNINGSTOKEN 354
#define RATIONALMODETOKEN 355
#define HOPITALRECURSIONSTOKEN 356
#define ONTOKEN 357
#define OFFTOKEN 358
#define DYADICTOKEN 359
#define POWERSTOKEN 360
#define BINARYTOKEN 361
#define HEXADECIMALTOKEN 362
#define FILETOKEN 363
#define POSTSCRIPTTOKEN 364
#define POSTSCRIPTFILETOKEN 365
#define PERTURBTOKEN 366
#define MINUSWORDTOKEN 367
#define PLUSWORDTOKEN 368
#define ZEROWORDTOKEN 369
#define NEARESTTOKEN 370
#define HONORCOEFFPRECTOKEN 371
#define TRUETOKEN 372
#define FALSETOKEN 373
#define DEFAULTTOKEN 374
#define MATCHTOKEN 375
#define WITHTOKEN 376
#define ABSOLUTETOKEN 377
#define DECIMALTOKEN 378
#define RELATIVETOKEN 379
#define FIXEDTOKEN 380
#define FLOATINGTOKEN 381
#define ERRORTOKEN 382
#define QUITTOKEN 383
#define FALSEQUITTOKEN 384
#define RESTARTTOKEN 385
#define LIBRARYTOKEN 386
#define LIBRARYCONSTANTTOKEN 387
#define DIFFTOKEN 388
#define SIMPLIFYTOKEN 389
#define REMEZTOKEN 390
#define BASHEVALUATETOKEN 391
#define FPMINIMAXTOKEN 392
#define HORNERTOKEN 393
#define EXPANDTOKEN 394
#define SIMPLIFYSAFETOKEN 395
#define TAYLORTOKEN 396
#define TAYLORFORMTOKEN 397
#define AUTODIFFTOKEN 398
#define DEGREETOKEN 399
#define NUMERATORTOKEN 400
#define DENOMINATORTOKEN 401
#define SUBSTITUTETOKEN 402
#define COEFFTOKEN 403
#define SUBPOLYTOKEN 404
#define ROUNDCOEFFICIENTSTOKEN 405
#define RATIONALAPPROXTOKEN 406
#define ACCURATEINFNORMTOKEN 407
#define ROUNDTOFORMATTOKEN 408
#define EVALUATETOKEN 409
#define LENGTHTOKEN 410
#define INFTOKEN 411
#define MIDTOKEN 412
#define SUPTOKEN 413
#define MINTOKEN 414
#define MAXTOKEN 415
#define READXMLTOKEN 416
#define PARSETOKEN 417
#define PRINTTOKEN 418
#define PRINTXMLTOKEN 419
#define PLOTTOKEN 420
#define PRINTHEXATOKEN 421
#define PRINTFLOATTOKEN 422
#define PRINTBINARYTOKEN 423
#define PRINTEXPANSIONTOKEN 424
#define BASHEXECUTETOKEN 425
#define EXTERNALPLOTTOKEN 426
#define WRITETOKEN 427
#define ASCIIPLOTTOKEN 428
#define RENAMETOKEN 429
#define INFNORMTOKEN 430
#define SUPNORMTOKEN 431
#define FINDZEROSTOKEN 432
#define FPFINDZEROSTOKEN 433
#define DIRTYINFNORMTOKEN 434
#define NUMBERROOTSTOKEN 435
#define INTEGRALTOKEN 436
#define DIRTYINTEGRALTOKEN 437
#define WORSTCASETOKEN 438
#define IMPLEMENTPOLYTOKEN 439
#define IMPLEMENTCONSTTOKEN 440
#define CHECKINFNORMTOKEN 441
#define ZERODENOMINATORSTOKEN 442
#define ISEVALUABLETOKEN 443
#define SEARCHGALTOKEN 444
#define GUESSDEGREETOKEN 445
#define DIRTYFINDZEROSTOKEN 446
#define IFTOKEN 447
#define THENTOKEN 448
#define ELSETOKEN 449
#define FORTOKEN 450
#define INTOKEN 451
#define FROMTOKEN 452
#define TOTOKEN 453
#define BYTOKEN 454
#define DOTOKEN 455
#define BEGINTOKEN 456
#define ENDTOKEN 457
#define LEFTCURLYBRACETOKEN 458
#define RIGHTCURLYBRACETOKEN 459
#define WHILETOKEN 460
#define READFILETOKEN 461
#define ISBOUNDTOKEN 462
#define EXECUTETOKEN 463
#define EXTERNALPROCTOKEN 464
#define VOIDTOKEN 465
#define CONSTANTTYPETOKEN 466
#define FUNCTIONTOKEN 467
#define RANGETOKEN 468
#define INTEGERTOKEN 469
#define STRINGTYPETOKEN 470
#define BOOLEANTOKEN 471
#define LISTTOKEN 472
#define OFTOKEN 473
#define VARTOKEN 474
#define PROCTOKEN 475
#define PROCEDURETOKEN 476
#define RETURNTOKEN 477
#define NOPTOKEN 478
#define HELPTOKEN 479
#define VERSIONTOKEN 480



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 387 of yacc.c  */
#line 115 "parser.y"

  doubleNode *dblnode;
  struct entryStruct *association;
  char *value;
  node *tree;
  chain *list;
  int *integerval;
  int count;
  void *other;


/* Line 387 of yacc.c  */
#line 624 "parser.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void * YYLEX_PARAM);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

/* Line 390 of yacc.c  */
#line 651 "parser.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef __attribute__
/* This feature is available in gcc versions 2.5 and later.  */
# if (! defined __GNUC__ || __GNUC__ < 2 \
      || (__GNUC__ == 2 && __GNUC_MINOR__ < 5))
#  define __attribute__(Spec) /* empty */
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif


/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(N) (N)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  387
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   8236

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  226
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  47
/* YYNRULES -- Number of rules.  */
#define YYNRULES  605
/* YYNRULES -- Number of states.  */
#define YYNSTATES  1279

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   480

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     6,     9,    11,    15,    18,    21,    23,
      25,    27,    29,    31,    33,    37,    42,    46,    49,    52,
      57,    60,    64,    70,    78,    88,    94,    97,   101,   104,
     108,   111,   113,   117,   123,   130,   136,   141,   150,   160,
     169,   177,   184,   192,   199,   205,   215,   226,   236,   245,
     254,   264,   273,   281,   293,   306,   318,   329,   331,   333,
     335,   340,   344,   346,   351,   358,   366,   373,   378,   383,
     388,   393,   398,   403,   416,   421,   428,   436,   443,   448,
     453,   460,   468,   481,   488,   500,   502,   504,   508,   510,
     513,   515,   518,   522,   526,   533,   540,   544,   548,   552,
     556,   560,   564,   568,   572,   576,   580,   584,   588,   592,
     596,   600,   604,   608,   612,   616,   620,   624,   628,   632,
     636,   640,   644,   648,   652,   656,   660,   664,   668,   672,
     676,   680,   682,   686,   688,   692,   694,   696,   701,   703,
     708,   710,   714,   718,   721,   726,   728,   732,   736,   740,
     744,   749,   754,   758,   760,   764,   768,   772,   776,   780,
     784,   786,   788,   791,   794,   796,   799,   802,   806,   810,
     815,   820,   825,   830,   832,   836,   841,   846,   848,   850,
     852,   854,   856,   858,   860,   862,   864,   866,   868,   870,
     872,   874,   876,   878,   880,   882,   884,   886,   888,   890,
     892,   894,   896,   898,   900,   902,   904,   906,   908,   910,
     912,   914,   916,   921,   926,   930,   932,   934,   936,   938,
     942,   946,   948,   950,   954,   961,   968,   971,   976,   978,
     981,   991,   998,  1007,  1013,  1022,  1028,  1036,  1041,  1047,
    1049,  1051,  1053,  1055,  1057,  1059,  1061,  1066,  1070,  1076,
    1083,  1085,  1089,  1095,  1101,  1107,  1111,  1115,  1119,  1123,
    1128,  1133,  1138,  1143,  1148,  1153,  1160,  1169,  1174,  1179,
    1190,  1195,  1200,  1205,  1210,  1219,  1228,  1237,  1242,  1247,
    1252,  1259,  1266,  1273,  1280,  1287,  1296,  1305,  1312,  1317,
    1322,  1329,  1342,  1349,  1356,  1363,  1370,  1377,  1384,  1399,
    1408,  1415,  1422,  1427,  1436,  1443,  1448,  1453,  1458,  1463,
    1468,  1473,  1478,  1483,  1488,  1493,  1498,  1503,  1510,  1515,
    1520,  1525,  1530,  1535,  1540,  1545,  1550,  1555,  1560,  1565,
    1570,  1575,  1580,  1585,  1590,  1595,  1600,  1605,  1610,  1615,
    1620,  1625,  1630,  1635,  1640,  1645,  1650,  1655,  1660,  1665,
    1668,  1669,  1672,  1675,  1678,  1681,  1684,  1687,  1690,  1693,
    1696,  1699,  1702,  1705,  1708,  1711,  1714,  1716,  1718,  1720,
    1722,  1724,  1726,  1730,  1734,  1738,  1742,  1746,  1750,  1752,
    1754,  1756,  1760,  1762,  1766,  1768,  1770,  1772,  1774,  1776,
    1778,  1780,  1782,  1784,  1786,  1788,  1790,  1793,  1796,  1798,
    1800,  1802,  1804,  1806,  1808,  1810,  1813,  1815,  1817,  1820,
    1822,  1824,  1826,  1828,  1830,  1832,  1834,  1836,  1838,  1840,
    1842,  1844,  1846,  1848,  1850,  1852,  1854,  1856,  1858,  1860,
    1862,  1864,  1866,  1868,  1870,  1872,  1874,  1876,  1878,  1880,
    1882,  1884,  1886,  1888,  1890,  1892,  1894,  1896,  1898,  1900,
    1902,  1904,  1906,  1908,  1910,  1912,  1914,  1916,  1918,  1920,
    1922,  1924,  1926,  1928,  1930,  1932,  1934,  1936,  1938,  1940,
    1942,  1944,  1946,  1948,  1950,  1952,  1954,  1956,  1958,  1960,
    1962,  1964,  1966,  1968,  1970,  1972,  1974,  1976,  1978,  1980,
    1982,  1984,  1986,  1988,  1990,  1992,  1994,  1996,  1998,  2000,
    2002,  2004,  2006,  2008,  2010,  2012,  2014,  2016,  2018,  2020,
    2022,  2024,  2026,  2028,  2030,  2032,  2034,  2036,  2038,  2040,
    2042,  2044,  2046,  2048,  2050,  2052,  2054,  2056,  2058,  2060,
    2062,  2064,  2066,  2068,  2070,  2072,  2074,  2076,  2078,  2080,
    2082,  2084,  2086,  2088,  2090,  2092,  2094,  2096,  2098,  2100,
    2102,  2104,  2106,  2108,  2110,  2112,  2114,  2116,  2118,  2120,
    2122,  2124,  2126,  2128,  2130,  2132,  2134,  2136,  2138,  2140,
    2142,  2144,  2146,  2148,  2150,  2152,  2154,  2156,  2158,  2160,
    2162,  2164,  2166,  2168,  2170,  2172,  2174,  2176,  2178,  2180,
    2182,  2184,  2186,  2188,  2190,  2192,  2194,  2196,  2198,  2200,
    2202,  2204,  2206,  2208,  2210,  2212
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     227,     0,    -1,   231,    21,    -1,   228,    21,    -1,    30,
      -1,   228,   272,    21,    -1,   225,    21,    -1,     1,    21,
      -1,   224,    -1,   201,    -1,   203,    -1,   202,    -1,   204,
      -1,   239,    -1,   229,   234,   230,    -1,   229,   235,   234,
     230,    -1,   229,   235,   230,    -1,   229,   230,    -1,   192,
     232,    -1,   205,   249,   200,   231,    -1,   195,   233,    -1,
     249,   193,   231,    -1,   249,   193,   231,   194,   231,    -1,
      10,   197,   249,   198,   249,   200,   231,    -1,    10,   197,
     249,   198,   249,   199,   249,   200,   231,    -1,    10,   196,
     249,   200,   231,    -1,   231,    21,    -1,   231,    21,   234,
      -1,   236,    21,    -1,   236,    21,   235,    -1,   219,   237,
      -1,    10,    -1,    10,    19,   237,    -1,    12,    13,   229,
     234,   230,    -1,    12,    13,   229,   235,   234,   230,    -1,
      12,    13,   229,   235,   230,    -1,    12,    13,   229,   230,
      -1,    12,    13,   229,   234,   222,   249,    21,   230,    -1,
      12,    13,   229,   235,   234,   222,   249,    21,   230,    -1,
      12,    13,   229,   235,   222,   249,    21,   230,    -1,    12,
      13,   229,   222,   249,    21,   230,    -1,    12,   237,    13,
     229,   234,   230,    -1,    12,   237,    13,   229,   235,   234,
     230,    -1,    12,   237,    13,   229,   235,   230,    -1,    12,
     237,    13,   229,   230,    -1,    12,   237,    13,   229,   234,
     222,   249,    21,   230,    -1,    12,   237,    13,   229,   235,
     234,   222,   249,    21,   230,    -1,    12,   237,    13,   229,
     235,   222,   249,    21,   230,    -1,    12,   237,    13,   229,
     222,   249,    21,   230,    -1,    12,    10,    16,    28,    13,
     229,   234,   230,    -1,    12,    10,    16,    28,    13,   229,
     235,   234,   230,    -1,    12,    10,    16,    28,    13,   229,
     235,   230,    -1,    12,    10,    16,    28,    13,   229,   230,
      -1,    12,    10,    16,    28,    13,   229,   234,   222,   249,
      21,   230,    -1,    12,    10,    16,    28,    13,   229,   235,
     234,   222,   249,    21,   230,    -1,    12,    10,    16,    28,
      13,   229,   235,   222,   249,    21,   230,    -1,    12,    10,
      16,    28,    13,   229,   222,   249,    21,   230,    -1,   128,
      -1,   129,    -1,   223,    -1,   223,    12,   249,    13,    -1,
     223,    12,    13,    -1,   130,    -1,   163,    12,   245,    13,
      -1,   163,    12,   245,    13,    27,   249,    -1,   163,    12,
     245,    13,    27,    27,   249,    -1,   165,    12,   249,    19,
     245,    13,    -1,   166,    12,   249,    13,    -1,   167,    12,
     249,    13,    -1,   168,    12,   249,    13,    -1,   169,    12,
     249,    13,    -1,   185,    12,   245,    13,    -1,   170,    12,
     249,    13,    -1,   171,    12,   249,    19,   249,    19,   249,
      19,   249,    19,   245,    13,    -1,   172,    12,   245,    13,
      -1,   172,    12,   245,    13,    27,   249,    -1,   172,    12,
     245,    13,    27,    27,   249,    -1,   173,    12,   249,    19,
     249,    13,    -1,   164,    12,   249,    13,    -1,   208,    12,
     249,    13,    -1,   164,    12,   249,    13,    27,   249,    -1,
     164,    12,   249,    13,    27,    27,   249,    -1,   183,    12,
     249,    19,   249,    19,   249,    19,   249,    19,   245,    13,
      -1,   174,    12,    10,    19,    10,    13,    -1,   209,    12,
      10,    19,   249,    19,   271,    42,    27,   269,    13,    -1,
     240,    -1,   245,    -1,   221,    10,   238,    -1,   243,    -1,
     244,    20,    -1,   241,    -1,   241,    20,    -1,    10,    16,
     249,    -1,    10,    17,   249,    -1,    10,    16,   131,    12,
     249,    13,    -1,    10,    16,   132,    12,   249,    13,    -1,
     251,    16,   249,    -1,   251,    17,   249,    -1,   242,    16,
     249,    -1,   242,    17,   249,    -1,   257,    29,    10,    -1,
      86,    16,   249,    -1,    87,    16,   249,    -1,    88,    16,
     249,    -1,    89,    16,   249,    -1,    90,    16,   249,    -1,
      91,    16,   249,    -1,    92,    16,   249,    -1,    93,    16,
     249,    -1,    94,    16,   249,    -1,    96,    16,   249,    -1,
      97,    16,   249,    -1,    98,    16,   249,    -1,   100,    16,
     249,    -1,    99,    16,   249,    -1,   101,    16,   249,    -1,
      86,    16,   249,    -1,    87,    16,   249,    -1,    88,    16,
     249,    -1,    89,    16,   249,    -1,    90,    16,   249,    -1,
      91,    16,   249,    -1,    92,    16,   249,    -1,    93,    16,
     249,    -1,    94,    16,   249,    -1,    96,    16,   249,    -1,
      97,    16,   249,    -1,    98,    16,   249,    -1,   100,    16,
     249,    -1,    99,    16,   249,    -1,   101,    16,   249,    -1,
     249,    -1,   249,    19,   245,    -1,   248,    -1,   248,   247,
     246,    -1,    19,    -1,    21,    -1,    29,    10,    16,   249,
      -1,   250,    -1,   120,   250,   121,   258,    -1,   252,    -1,
     249,    39,   252,    -1,   249,    40,   252,    -1,    20,   252,
      -1,   257,    14,   249,    15,    -1,   253,    -1,   252,    18,
     253,    -1,   252,   196,   253,    -1,   252,    23,   253,    -1,
     252,    27,   253,    -1,   252,    23,    16,   253,    -1,   252,
      27,    16,   253,    -1,   252,    37,   253,    -1,   255,    -1,
     253,    41,   255,    -1,   253,    42,   255,    -1,   253,    32,
     255,    -1,   253,    33,   255,    -1,   253,    35,   255,    -1,
     253,    36,   255,    -1,    41,    -1,    42,    -1,    41,   254,
      -1,    42,   254,    -1,   256,    -1,   254,   256,    -1,    38,
     256,    -1,   255,    43,   256,    -1,   255,    44,   256,    -1,
     255,    43,   254,   256,    -1,   255,    44,   254,   256,    -1,
     255,    43,    38,   256,    -1,   255,    44,    38,   256,    -1,
     257,    -1,   257,    45,   256,    -1,   257,    45,   254,   256,
      -1,   257,    45,    38,   256,    -1,   102,    -1,   103,    -1,
     104,    -1,   105,    -1,   106,    -1,   107,    -1,   108,    -1,
     109,    -1,   110,    -1,   111,    -1,   112,    -1,   113,    -1,
     114,    -1,   115,    -1,   116,    -1,   117,    -1,   210,    -1,
     118,    -1,   119,    -1,   123,    -1,   122,    -1,   124,    -1,
     125,    -1,   126,    -1,   127,    -1,    68,    -1,    69,    -1,
      71,    -1,    70,    -1,    74,    -1,    72,    -1,    73,    -1,
      11,    -1,   260,    -1,    10,    -1,   207,    12,    10,    13,
      -1,    10,    12,   245,    13,    -1,    10,    12,    13,    -1,
     261,    -1,   263,    -1,   264,    -1,   265,    -1,    12,   249,
      13,    -1,   203,   246,   204,    -1,   267,    -1,   251,    -1,
     257,    29,    10,    -1,   257,    29,    10,    12,   245,    13,
      -1,    12,   249,    13,    12,   245,    13,    -1,   220,   238,
      -1,    95,    12,   231,    13,    -1,   259,    -1,   259,   258,
      -1,   249,    34,   229,   235,   234,   222,   249,    21,   230,
      -1,   249,    34,   229,   235,   234,   230,    -1,   249,    34,
     229,   235,   222,   249,    21,   230,    -1,   249,    34,   229,
     235,   230,    -1,   249,    34,   229,   234,   222,   249,    21,
     230,    -1,   249,    34,   229,   234,   230,    -1,   249,    34,
     229,   222,   249,    21,   230,    -1,   249,    34,   229,   230,
      -1,   249,    34,    12,   249,    13,    -1,     3,    -1,     4,
      -1,     5,    -1,     6,    -1,     7,    -1,     8,    -1,     9,
      -1,    14,    31,    31,    15,    -1,    14,    40,    15,    -1,
      14,    31,   262,    31,    15,    -1,    14,    31,   262,    28,
      31,    15,    -1,   249,    -1,   262,    19,   249,    -1,   262,
      19,    28,    19,   249,    -1,    14,   249,    19,   249,    15,
      -1,    14,   249,    21,   249,    15,    -1,    14,   249,    15,
      -1,    22,   249,    26,    -1,    22,   249,    25,    -1,    22,
     249,    24,    -1,   158,    12,   249,    13,    -1,   157,    12,
     249,    13,    -1,   156,    12,   249,    13,    -1,   133,    12,
     249,    13,    -1,   134,    12,   249,    13,    -1,   136,    12,
     249,    13,    -1,   136,    12,   249,    19,   249,    13,    -1,
     135,    12,   249,    19,   249,    19,   245,    13,    -1,   159,
      12,   245,    13,    -1,   160,    12,   245,    13,    -1,   137,
      12,   249,    19,   249,    19,   249,    19,   245,    13,    -1,
     138,    12,   249,    13,    -1,    91,    12,   249,    13,    -1,
     139,    12,   249,    13,    -1,   140,    12,   249,    13,    -1,
     141,    12,   249,    19,   249,    19,   249,    13,    -1,   142,
      12,   249,    19,   249,    19,   245,    13,    -1,   143,    12,
     249,    19,   249,    19,   249,    13,    -1,   144,    12,   249,
      13,    -1,   145,    12,   249,    13,    -1,   146,    12,   249,
      13,    -1,   147,    12,   249,    19,   249,    13,    -1,   148,
      12,   249,    19,   249,    13,    -1,   149,    12,   249,    19,
     249,    13,    -1,   150,    12,   249,    19,   249,    13,    -1,
     151,    12,   249,    19,   249,    13,    -1,   152,    12,   249,
      19,   249,    19,   245,    13,    -1,   153,    12,   249,    19,
     249,    19,   249,    13,    -1,   154,    12,   249,    19,   249,
      13,    -1,   162,    12,   249,    13,    -1,   161,    12,   249,
      13,    -1,   175,    12,   249,    19,   245,    13,    -1,   176,
      12,   249,    19,   249,    19,   249,    19,   249,    19,   249,
      13,    -1,   177,    12,   249,    19,   249,    13,    -1,   178,
      12,   249,    19,   249,    13,    -1,   179,    12,   249,    19,
     249,    13,    -1,   180,    12,   249,    19,   249,    13,    -1,
     181,    12,   249,    19,   249,    13,    -1,   182,    12,   249,
      19,   249,    13,    -1,   184,    12,   249,    19,   249,    19,
     249,    19,   249,    19,   249,    19,   245,    13,    -1,   186,
      12,   249,    19,   249,    19,   249,    13,    -1,   187,    12,
     249,    19,   249,    13,    -1,   188,    12,   249,    19,   249,
      13,    -1,   189,    12,   245,    13,    -1,   190,    12,   249,
      19,   249,    19,   245,    13,    -1,   191,    12,   249,    19,
     249,    13,    -1,    78,    12,   249,    13,    -1,    85,    12,
     249,    13,    -1,   206,    12,   249,    13,    -1,    79,    12,
     249,    13,    -1,    80,    12,   249,    13,    -1,    82,    12,
     249,    13,    -1,    83,    12,   249,    13,    -1,    84,    12,
     249,    13,    -1,    81,    12,   249,    13,    -1,    46,    12,
     249,    13,    -1,    47,    12,   249,    13,    -1,   212,    12,
     249,    13,    -1,   212,    12,   249,    19,   249,    13,    -1,
      48,    12,   249,    13,    -1,    49,    12,   249,    13,    -1,
      50,    12,   249,    13,    -1,    51,    12,   249,    13,    -1,
      52,    12,   249,    13,    -1,    53,    12,   249,    13,    -1,
      54,    12,   249,    13,    -1,    55,    12,   249,    13,    -1,
      56,    12,   249,    13,    -1,    57,    12,   249,    13,    -1,
      58,    12,   249,    13,    -1,    59,    12,   249,    13,    -1,
      60,    12,   249,    13,    -1,    61,    12,   249,    13,    -1,
      62,    12,   249,    13,    -1,    63,    12,   249,    13,    -1,
      64,    12,   249,    13,    -1,    65,    12,   249,    13,    -1,
      66,    12,   249,    13,    -1,    67,    12,   249,    13,    -1,
      68,    12,   249,    13,    -1,    69,    12,   249,    13,    -1,
      71,    12,   249,    13,    -1,    70,    12,   249,    13,    -1,
      72,    12,   249,    13,    -1,    73,    12,   249,    13,    -1,
      74,    12,   249,    13,    -1,    75,    12,   249,    13,    -1,
      76,    12,   249,    13,    -1,    77,    12,   249,    13,    -1,
     155,    12,   249,    13,    -1,    16,    30,    -1,    -1,    86,
     266,    -1,    87,   266,    -1,    88,   266,    -1,    89,   266,
      -1,    90,   266,    -1,    91,   266,    -1,    92,   266,    -1,
      93,   266,    -1,    94,   266,    -1,    96,   266,    -1,    97,
     266,    -1,    98,   266,    -1,   100,   266,    -1,    99,   266,
      -1,   101,   266,    -1,   211,    -1,   212,    -1,   213,    -1,
     214,    -1,   215,    -1,   216,    -1,   217,   218,   211,    -1,
     217,   218,   212,    -1,   217,   218,   213,    -1,   217,   218,
     214,    -1,   217,   218,   215,    -1,   217,   218,   216,    -1,
     210,    -1,   268,    -1,   268,    -1,   268,    19,   270,    -1,
     269,    -1,    12,   270,    13,    -1,     3,    -1,     5,    -1,
       6,    -1,     7,    -1,     8,    -1,     9,    -1,    10,    -1,
      11,    -1,    12,    -1,    13,    -1,    14,    -1,    15,    -1,
      14,    31,    -1,    31,    15,    -1,    16,    -1,    17,    -1,
      18,    -1,    19,    -1,    20,    -1,    22,    -1,    23,    -1,
      23,    16,    -1,    24,    -1,    25,    -1,    27,    16,    -1,
      29,    -1,    26,    -1,    27,    -1,    28,    -1,    30,    -1,
      31,    -1,    32,    -1,    33,    -1,    35,    -1,    36,    -1,
      37,    -1,    39,    -1,    40,    -1,    41,    -1,    42,    -1,
      38,    -1,    43,    -1,    44,    -1,    45,    -1,    46,    -1,
      47,    -1,    48,    -1,    49,    -1,    50,    -1,    51,    -1,
      52,    -1,    53,    -1,    54,    -1,    55,    -1,    56,    -1,
      57,    -1,    58,    -1,    59,    -1,    60,    -1,    61,    -1,
      62,    -1,    63,    -1,    64,    -1,    65,    -1,    66,    -1,
      67,    -1,    68,    -1,    69,    -1,    71,    -1,    70,    -1,
      72,    -1,    73,    -1,    74,    -1,    75,    -1,    76,    -1,
      77,    -1,    78,    -1,    85,    -1,   206,    -1,    79,    -1,
      80,    -1,    81,    -1,    86,    -1,    87,    -1,    88,    -1,
      89,    -1,    90,    -1,    91,    -1,    92,    -1,    93,    -1,
      94,    -1,    95,    -1,    96,    -1,    97,    -1,    98,    -1,
     100,    -1,    99,    -1,   101,    -1,   102,    -1,   103,    -1,
     104,    -1,   105,    -1,   106,    -1,   107,    -1,   108,    -1,
     109,    -1,   110,    -1,   111,    -1,   112,    -1,   113,    -1,
     114,    -1,   115,    -1,   116,    -1,   117,    -1,   118,    -1,
     119,    -1,   120,    -1,   121,    -1,   122,    -1,   123,    -1,
     124,    -1,   125,    -1,   126,    -1,   127,    -1,   128,    -1,
     129,    -1,   130,    -1,   131,    -1,   132,    -1,   133,    -1,
     136,    -1,   134,    -1,   135,    -1,   159,    -1,   160,    -1,
     137,    -1,   138,    -1,   139,    -1,   140,    -1,   141,    -1,
     142,    -1,   143,    -1,   144,    -1,   145,    -1,   146,    -1,
     147,    -1,   148,    -1,   149,    -1,   150,    -1,   151,    -1,
     152,    -1,   153,    -1,   154,    -1,   155,    -1,   162,    -1,
     163,    -1,   164,    -1,   161,    -1,   165,    -1,   166,    -1,
     167,    -1,   168,    -1,   169,    -1,   170,    -1,   171,    -1,
     172,    -1,   173,    -1,   174,    -1,   175,    -1,   176,    -1,
     177,    -1,   178,    -1,   179,    -1,   180,    -1,   181,    -1,
     182,    -1,   183,    -1,   184,    -1,   185,    -1,   186,    -1,
     187,    -1,   188,    -1,   189,    -1,   190,    -1,   191,    -1,
     192,    -1,   193,    -1,   194,    -1,   195,    -1,   196,    -1,
     197,    -1,   198,    -1,   199,    -1,   200,    -1,   229,    -1,
     230,    -1,   205,    -1,   156,    -1,   157,    -1,   158,    -1,
      83,    -1,    82,    -1,    84,    -1,   208,    -1,   207,    -1,
     225,    -1,   209,    -1,   210,    -1,   211,    -1,   212,    -1,
     213,    -1,   214,    -1,   215,    -1,   216,    -1,   217,    -1,
     218,    -1,   219,    -1,   223,    -1,   220,    -1,   221,    -1,
     222,    -1,   224,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   427,   427,   433,   441,   449,   455,   467,   475,   482,
     486,   492,   496,   502,   506,   510,   514,   518,   522,   526,
     530,   536,   540,   548,   553,   558,   566,   570,   576,   580,
     586,   593,   597,   603,   607,   611,   615,   619,   623,   627,
     631,   635,   639,   643,   647,   651,   655,   659,   663,   667,
     671,   675,   679,   683,   687,   691,   695,   702,   706,   710,
     714,   718,   722,   726,   730,   734,   738,   742,   746,   750,
     754,   758,   762,   766,   770,   774,   778,   782,   786,   790,
     794,   798,   802,   806,   812,   817,   821,   825,   832,   836,
     840,   844,   850,   855,   860,   865,   870,   875,   880,   884,
     890,   897,   901,   905,   909,   913,   917,   921,   925,   929,
     933,   937,   941,   945,   949,   953,   959,   963,   967,   971,
     975,   979,   983,   987,   991,   995,   999,  1003,  1007,  1011,
    1015,  1021,  1025,  1031,  1035,  1041,  1045,  1051,  1061,  1065,
    1071,  1075,  1079,  1083,  1089,  1098,  1102,  1106,  1110,  1114,
    1118,  1122,  1126,  1132,  1136,  1140,  1144,  1148,  1152,  1156,
    1162,  1166,  1170,  1174,  1181,  1185,  1192,  1196,  1200,  1204,
    1211,  1218,  1222,  1228,  1232,  1236,  1243,  1250,  1254,  1258,
    1262,  1266,  1270,  1274,  1278,  1282,  1286,  1290,  1294,  1298,
    1302,  1306,  1310,  1314,  1318,  1322,  1326,  1330,  1334,  1338,
    1342,  1346,  1350,  1354,  1358,  1362,  1366,  1370,  1374,  1378,
    1389,  1393,  1398,  1403,  1408,  1413,  1417,  1421,  1425,  1429,
    1433,  1437,  1441,  1446,  1451,  1456,  1460,  1464,  1470,  1474,
    1480,  1484,  1488,  1492,  1496,  1500,  1504,  1508,  1512,  1518,
    1523,  1528,  1533,  1538,  1543,  1548,  1556,  1560,  1564,  1568,
    1574,  1578,  1582,  1589,  1593,  1597,  1603,  1607,  1611,  1615,
    1619,  1623,  1629,  1633,  1637,  1641,  1645,  1649,  1653,  1657,
    1661,  1665,  1669,  1673,  1677,  1681,  1685,  1689,  1693,  1697,
    1701,  1705,  1709,  1713,  1717,  1721,  1725,  1729,  1733,  1737,
    1741,  1745,  1749,  1753,  1757,  1761,  1765,  1769,  1773,  1777,
    1781,  1785,  1789,  1793,  1797,  1801,  1805,  1809,  1813,  1817,
    1821,  1825,  1829,  1833,  1837,  1841,  1845,  1849,  1853,  1857,
    1861,  1865,  1869,  1873,  1877,  1881,  1885,  1889,  1893,  1897,
    1901,  1905,  1909,  1913,  1917,  1921,  1925,  1929,  1933,  1937,
    1941,  1945,  1949,  1953,  1957,  1961,  1965,  1969,  1973,  1979,
    1984,  1989,  1993,  1997,  2001,  2005,  2009,  2013,  2017,  2021,
    2025,  2029,  2033,  2037,  2041,  2045,  2051,  2057,  2063,  2069,
    2075,  2081,  2087,  2093,  2099,  2105,  2111,  2117,  2125,  2131,
    2138,  2142,  2148,  2152,  2159,  2164,  2169,  2174,  2179,  2184,
    2195,  2200,  2205,  2209,  2213,  2217,  2221,  2225,  2229,  2240,
    2251,  2262,  2266,  2277,  2281,  2292,  2303,  2307,  2311,  2322,
    2326,  2330,  2341,  2345,  2349,  2353,  2364,  2368,  2379,  2390,
    2401,  2412,  2423,  2434,  2445,  2456,  2467,  2478,  2489,  2500,
    2511,  2522,  2533,  2544,  2555,  2566,  2577,  2588,  2599,  2610,
    2621,  2632,  2643,  2654,  2665,  2677,  2688,  2699,  2710,  2721,
    2732,  2743,  2754,  2765,  2776,  2787,  2798,  2809,  2820,  2831,
    2842,  2853,  2864,  2875,  2886,  2897,  2908,  2919,  2930,  2941,
    2952,  2963,  2974,  2985,  2996,  3007,  3018,  3029,  3040,  3051,
    3062,  3073,  3084,  3095,  3106,  3117,  3128,  3139,  3150,  3161,
    3172,  3183,  3194,  3205,  3216,  3227,  3238,  3249,  3260,  3271,
    3282,  3293,  3304,  3315,  3326,  3337,  3348,  3359,  3370,  3381,
    3392,  3403,  3414,  3425,  3436,  3447,  3458,  3469,  3480,  3491,
    3502,  3513,  3524,  3535,  3546,  3557,  3568,  3579,  3590,  3601,
    3612,  3623,  3634,  3645,  3656,  3667,  3678,  3689,  3700,  3711,
    3722,  3733,  3744,  3756,  3767,  3778,  3789,  3800,  3811,  3822,
    3833,  3844,  3855,  3866,  3877,  3888,  3899,  3910,  3918,  3929,
    3940,  3951,  3963,  3975,  3986,  3997,  4008,  4019,  4030,  4041,
    4045,  4049,  4053,  4057,  4068,  4072,  4076,  4080,  4085,  4089,
    4093,  4097,  4108,  4119,  4130,  4141,  4152,  4163,  4174,  4185,
    4189,  4199,  4209,  4219,  4229,  4239,  4249,  4259,  4269,  4279,
    4289,  4299,  4309,  4319,  4329,  4339
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "CONSTANTTOKEN", "MIDPOINTCONSTANTTOKEN",
  "DYADICCONSTANTTOKEN", "HEXCONSTANTTOKEN", "HEXADECIMALCONSTANTTOKEN",
  "BINARYCONSTANTTOKEN", "PITOKEN", "IDENTIFIERTOKEN", "STRINGTOKEN",
  "LPARTOKEN", "RPARTOKEN", "LBRACKETTOKEN", "RBRACKETTOKEN", "EQUALTOKEN",
  "ASSIGNEQUALTOKEN", "COMPAREEQUALTOKEN", "COMMATOKEN",
  "EXCLAMATIONTOKEN", "SEMICOLONTOKEN", "STARLEFTANGLETOKEN",
  "LEFTANGLETOKEN", "RIGHTANGLEUNDERSCORETOKEN", "RIGHTANGLEDOTTOKEN",
  "RIGHTANGLESTARTOKEN", "RIGHTANGLETOKEN", "DOTSTOKEN", "DOTTOKEN",
  "QUESTIONMARKTOKEN", "VERTBARTOKEN", "ATTOKEN", "DOUBLECOLONTOKEN",
  "COLONTOKEN", "DOTCOLONTOKEN", "COLONDOTTOKEN", "EXCLAMATIONEQUALTOKEN",
  "APPROXTOKEN", "ANDTOKEN", "ORTOKEN", "PLUSTOKEN", "MINUSTOKEN",
  "MULTOKEN", "DIVTOKEN", "POWTOKEN", "SQRTTOKEN", "EXPTOKEN", "LOGTOKEN",
  "LOG2TOKEN", "LOG10TOKEN", "SINTOKEN", "COSTOKEN", "TANTOKEN",
  "ASINTOKEN", "ACOSTOKEN", "ATANTOKEN", "SINHTOKEN", "COSHTOKEN",
  "TANHTOKEN", "ASINHTOKEN", "ACOSHTOKEN", "ATANHTOKEN", "ABSTOKEN",
  "ERFTOKEN", "ERFCTOKEN", "LOG1PTOKEN", "EXPM1TOKEN", "DOUBLETOKEN",
  "SINGLETOKEN", "HALFPRECISIONTOKEN", "QUADTOKEN", "DOUBLEDOUBLETOKEN",
  "TRIPLEDOUBLETOKEN", "DOUBLEEXTENDEDTOKEN", "CEILTOKEN", "FLOORTOKEN",
  "NEARESTINTTOKEN", "HEADTOKEN", "REVERTTOKEN", "SORTTOKEN", "TAILTOKEN",
  "MANTISSATOKEN", "EXPONENTTOKEN", "PRECISIONTOKEN",
  "ROUNDCORRECTLYTOKEN", "PRECTOKEN", "POINTSTOKEN", "DIAMTOKEN",
  "DISPLAYTOKEN", "VERBOSITYTOKEN", "CANONICALTOKEN", "AUTOSIMPLIFYTOKEN",
  "TAYLORRECURSIONSTOKEN", "TIMINGTOKEN", "TIMETOKEN",
  "FULLPARENTHESESTOKEN", "MIDPOINTMODETOKEN", "DIEONERRORMODETOKEN",
  "SUPPRESSWARNINGSTOKEN", "RATIONALMODETOKEN", "HOPITALRECURSIONSTOKEN",
  "ONTOKEN", "OFFTOKEN", "DYADICTOKEN", "POWERSTOKEN", "BINARYTOKEN",
  "HEXADECIMALTOKEN", "FILETOKEN", "POSTSCRIPTTOKEN",
  "POSTSCRIPTFILETOKEN", "PERTURBTOKEN", "MINUSWORDTOKEN", "PLUSWORDTOKEN",
  "ZEROWORDTOKEN", "NEARESTTOKEN", "HONORCOEFFPRECTOKEN", "TRUETOKEN",
  "FALSETOKEN", "DEFAULTTOKEN", "MATCHTOKEN", "WITHTOKEN", "ABSOLUTETOKEN",
  "DECIMALTOKEN", "RELATIVETOKEN", "FIXEDTOKEN", "FLOATINGTOKEN",
  "ERRORTOKEN", "QUITTOKEN", "FALSEQUITTOKEN", "RESTARTTOKEN",
  "LIBRARYTOKEN", "LIBRARYCONSTANTTOKEN", "DIFFTOKEN", "SIMPLIFYTOKEN",
  "REMEZTOKEN", "BASHEVALUATETOKEN", "FPMINIMAXTOKEN", "HORNERTOKEN",
  "EXPANDTOKEN", "SIMPLIFYSAFETOKEN", "TAYLORTOKEN", "TAYLORFORMTOKEN",
  "AUTODIFFTOKEN", "DEGREETOKEN", "NUMERATORTOKEN", "DENOMINATORTOKEN",
  "SUBSTITUTETOKEN", "COEFFTOKEN", "SUBPOLYTOKEN",
  "ROUNDCOEFFICIENTSTOKEN", "RATIONALAPPROXTOKEN", "ACCURATEINFNORMTOKEN",
  "ROUNDTOFORMATTOKEN", "EVALUATETOKEN", "LENGTHTOKEN", "INFTOKEN",
  "MIDTOKEN", "SUPTOKEN", "MINTOKEN", "MAXTOKEN", "READXMLTOKEN",
  "PARSETOKEN", "PRINTTOKEN", "PRINTXMLTOKEN", "PLOTTOKEN",
  "PRINTHEXATOKEN", "PRINTFLOATTOKEN", "PRINTBINARYTOKEN",
  "PRINTEXPANSIONTOKEN", "BASHEXECUTETOKEN", "EXTERNALPLOTTOKEN",
  "WRITETOKEN", "ASCIIPLOTTOKEN", "RENAMETOKEN", "INFNORMTOKEN",
  "SUPNORMTOKEN", "FINDZEROSTOKEN", "FPFINDZEROSTOKEN",
  "DIRTYINFNORMTOKEN", "NUMBERROOTSTOKEN", "INTEGRALTOKEN",
  "DIRTYINTEGRALTOKEN", "WORSTCASETOKEN", "IMPLEMENTPOLYTOKEN",
  "IMPLEMENTCONSTTOKEN", "CHECKINFNORMTOKEN", "ZERODENOMINATORSTOKEN",
  "ISEVALUABLETOKEN", "SEARCHGALTOKEN", "GUESSDEGREETOKEN",
  "DIRTYFINDZEROSTOKEN", "IFTOKEN", "THENTOKEN", "ELSETOKEN", "FORTOKEN",
  "INTOKEN", "FROMTOKEN", "TOTOKEN", "BYTOKEN", "DOTOKEN", "BEGINTOKEN",
  "ENDTOKEN", "LEFTCURLYBRACETOKEN", "RIGHTCURLYBRACETOKEN", "WHILETOKEN",
  "READFILETOKEN", "ISBOUNDTOKEN", "EXECUTETOKEN", "EXTERNALPROCTOKEN",
  "VOIDTOKEN", "CONSTANTTYPETOKEN", "FUNCTIONTOKEN", "RANGETOKEN",
  "INTEGERTOKEN", "STRINGTYPETOKEN", "BOOLEANTOKEN", "LISTTOKEN",
  "OFTOKEN", "VARTOKEN", "PROCTOKEN", "PROCEDURETOKEN", "RETURNTOKEN",
  "NOPTOKEN", "HELPTOKEN", "VERSIONTOKEN", "$accept", "startsymbol",
  "helpmeta", "beginsymbol", "endsymbol", "command", "ifcommand",
  "forcommand", "commandlist", "variabledeclarationlist",
  "variabledeclaration", "identifierlist", "procbody", "simplecommand",
  "assignment", "simpleassignment", "structuring", "stateassignment",
  "stillstateassignment", "thinglist", "structelementlist",
  "structelementseparator", "structelement", "thing", "supermegaterm",
  "indexing", "megaterm", "hyperterm", "unaryplusminus", "term", "subterm",
  "basicthing", "matchlist", "matchelement", "constant", "list",
  "simplelist", "range", "debound", "headfunction", "egalquestionmark",
  "statedereference", "externalproctype", "extendedexternalproctype",
  "externalproctypesimplelist", "externalproctypelist", "help", YY_NULL
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,   472,   473,   474,
     475,   476,   477,   478,   479,   480
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   226,   227,   227,   227,   227,   227,   227,   228,   229,
     229,   230,   230,   231,   231,   231,   231,   231,   231,   231,
     231,   232,   232,   233,   233,   233,   234,   234,   235,   235,
     236,   237,   237,   238,   238,   238,   238,   238,   238,   238,
     238,   238,   238,   238,   238,   238,   238,   238,   238,   238,
     238,   238,   238,   238,   238,   238,   238,   239,   239,   239,
     239,   239,   239,   239,   239,   239,   239,   239,   239,   239,
     239,   239,   239,   239,   239,   239,   239,   239,   239,   239,
     239,   239,   239,   239,   239,   239,   239,   239,   240,   240,
     240,   240,   241,   241,   241,   241,   241,   241,   241,   241,
     242,   243,   243,   243,   243,   243,   243,   243,   243,   243,
     243,   243,   243,   243,   243,   243,   244,   244,   244,   244,
     244,   244,   244,   244,   244,   244,   244,   244,   244,   244,
     244,   245,   245,   246,   246,   247,   247,   248,   249,   249,
     250,   250,   250,   250,   251,   252,   252,   252,   252,   252,
     252,   252,   252,   253,   253,   253,   253,   253,   253,   253,
     254,   254,   254,   254,   255,   255,   255,   255,   255,   255,
     255,   255,   255,   256,   256,   256,   256,   257,   257,   257,
     257,   257,   257,   257,   257,   257,   257,   257,   257,   257,
     257,   257,   257,   257,   257,   257,   257,   257,   257,   257,
     257,   257,   257,   257,   257,   257,   257,   257,   257,   257,
     257,   257,   257,   257,   257,   257,   257,   257,   257,   257,
     257,   257,   257,   257,   257,   257,   257,   257,   258,   258,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   260,
     260,   260,   260,   260,   260,   260,   261,   261,   261,   261,
     262,   262,   262,   263,   263,   263,   264,   264,   264,   264,
     264,   264,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   265,   265,   265,   265,   265,   265,   266,
     266,   267,   267,   267,   267,   267,   267,   267,   267,   267,
     267,   267,   267,   267,   267,   267,   268,   268,   268,   268,
     268,   268,   268,   268,   268,   268,   268,   268,   269,   269,
     270,   270,   271,   271,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     2,     1,     3,     2,     2,     1,     1,
       1,     1,     1,     1,     3,     4,     3,     2,     2,     4,
       2,     3,     5,     7,     9,     5,     2,     3,     2,     3,
       2,     1,     3,     5,     6,     5,     4,     8,     9,     8,
       7,     6,     7,     6,     5,     9,    10,     9,     8,     8,
       9,     8,     7,    11,    12,    11,    10,     1,     1,     1,
       4,     3,     1,     4,     6,     7,     6,     4,     4,     4,
       4,     4,     4,    12,     4,     6,     7,     6,     4,     4,
       6,     7,    12,     6,    11,     1,     1,     3,     1,     2,
       1,     2,     3,     3,     6,     6,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     1,     3,     1,     3,     1,     1,     4,     1,     4,
       1,     3,     3,     2,     4,     1,     3,     3,     3,     3,
       4,     4,     3,     1,     3,     3,     3,     3,     3,     3,
       1,     1,     2,     2,     1,     2,     2,     3,     3,     4,
       4,     4,     4,     1,     3,     4,     4,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     4,     4,     3,     1,     1,     1,     1,     3,
       3,     1,     1,     3,     6,     6,     2,     4,     1,     2,
       9,     6,     8,     5,     8,     5,     7,     4,     5,     1,
       1,     1,     1,     1,     1,     1,     4,     3,     5,     6,
       1,     3,     5,     5,     5,     3,     3,     3,     3,     4,
       4,     4,     4,     4,     4,     6,     8,     4,     4,    10,
       4,     4,     4,     4,     8,     8,     8,     4,     4,     4,
       6,     6,     6,     6,     6,     8,     8,     6,     4,     4,
       6,    12,     6,     6,     6,     6,     6,     6,    14,     8,
       6,     6,     4,     8,     6,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     6,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     2,
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     1,     1,     1,
       1,     1,     3,     3,     3,     3,     3,     3,     1,     1,
       1,     3,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     2,     2,     1,     1,
       1,     1,     1,     1,     1,     2,     1,     1,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,     0,   239,   240,   241,   242,   243,   244,   245,   211,
     209,     0,     0,     0,     0,     4,     0,   160,   161,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   202,   203,   205,   204,   207,   208,   206,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   350,
     350,   350,   350,   350,   350,   350,   350,   350,     0,   350,
     350,   350,   350,   350,   350,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   194,   195,     0,   197,   196,   198,   199,   200,   201,
      57,    58,    62,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     9,    10,     0,     0,     0,     0,
       0,   193,     0,     0,     0,    59,     8,     0,     0,     0,
       0,     0,    13,    85,    90,     0,    88,     0,    86,   131,
     138,   222,   140,   145,     0,   153,   164,   173,   210,   215,
     216,   217,   218,   221,     7,     0,     0,     0,   211,   350,
     350,   350,   350,   350,   350,   350,   350,   350,   350,   350,
     350,   350,   350,   350,     0,     0,   222,   173,     0,     0,
       0,   143,     0,   166,   162,   163,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   351,     0,   352,
       0,   353,     0,   354,     0,   355,     0,     0,   356,     0,
     357,     0,   358,     0,   359,     0,     0,   360,     0,   361,
       0,   362,     0,   364,     0,   363,     0,   365,     0,   138,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    18,
       0,     0,    20,     0,     0,   133,     0,     0,     0,     0,
       0,     0,     0,   226,     0,     0,     6,     1,   384,   385,
     386,   387,   388,   389,   390,   391,   392,   393,   394,   395,
     398,   399,   400,   401,   402,     3,   403,   404,   406,   407,
     410,   411,   412,   409,   413,   414,   415,   416,   417,   418,
     419,   424,   420,   421,   422,   423,   425,   426,   427,   428,
     429,   430,   431,   432,   433,   434,   435,   436,   437,   438,
     439,   440,   441,   442,   443,   444,   445,   446,   447,   448,
     449,   450,   451,   453,   452,   454,   455,   456,   457,   458,
     459,   460,   463,   464,   465,   585,   584,   586,   461,   466,
     467,   468,   469,   470,   471,   472,   473,   474,   475,   476,
     477,   478,   480,   479,   481,   482,   483,   484,   485,   486,
     487,   488,   489,   490,   491,   492,   493,   494,   495,   496,
     497,   498,   499,   500,   501,   502,   503,   504,   505,   506,
     507,   508,   509,   510,   511,   512,   513,   515,   516,   514,
     519,   520,   521,   522,   523,   524,   525,   526,   527,   528,
     529,   530,   531,   532,   533,   534,   535,   536,   537,   581,
     582,   583,   517,   518,   541,   538,   539,   540,   542,   543,
     544,   545,   546,   547,   548,   549,   550,   551,   552,   553,
     554,   555,   556,   557,   558,   559,   560,   561,   562,   563,
     564,   565,   566,   567,   568,   569,   570,   571,   572,   573,
     574,   575,   576,   577,    11,    10,    12,   580,   462,   588,
     587,   590,   591,   592,   593,   594,   595,   596,   597,   598,
     599,   600,   602,   603,   604,   601,   605,   589,   578,   579,
       0,     0,    17,     0,     0,     0,     0,     2,    91,     0,
       0,    89,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   165,     0,
       0,     0,     0,     0,   214,     0,     0,     0,    92,    93,
       0,   219,     0,     0,   250,     0,   247,   255,     0,     0,
     258,   257,   256,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   349,   101,   102,   103,   104,   105,     0,
     106,   107,   108,   109,     0,   110,   111,   112,   114,   113,
     115,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   220,   135,   136,     0,     0,
       0,     0,     0,     0,     0,    31,     0,     0,    87,    61,
       0,   396,   405,   408,   397,     5,    31,    30,    26,    14,
      16,     0,    28,    98,    99,   132,   141,   142,    96,    97,
     146,     0,   148,     0,   149,   152,   147,   156,   157,   158,
     159,   154,   155,     0,     0,   167,     0,     0,   168,     0,
     223,     0,     0,   174,   213,     0,     0,     0,   223,   246,
       0,     0,     0,     0,     0,   314,   315,   318,   319,   320,
     321,   322,   323,   324,   325,   326,   327,   328,   329,   330,
     331,   332,   333,   334,   335,   336,   337,   338,   339,   341,
     340,   342,   343,   344,   345,   346,   347,   305,   308,   309,
     313,   310,   311,   312,   306,   271,   227,     0,   139,   228,
     262,   263,     0,   264,     0,     0,   270,   272,   273,     0,
       0,     0,   277,   278,   279,     0,     0,     0,     0,     0,
       0,     0,     0,   348,   261,   260,   259,   267,   268,   289,
     288,    63,    78,     0,    67,    68,    69,    70,    72,     0,
      74,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    71,     0,     0,     0,   302,     0,     0,
      21,     0,     0,     0,   134,    19,   307,   212,    79,     0,
     316,     0,     0,     0,     0,     0,    60,    27,    15,    29,
     150,   151,   171,   169,   172,   170,   144,     0,   176,   175,
       0,     0,     0,     0,   251,     0,   248,   253,   254,     0,
     229,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     137,     0,     0,     0,    32,     0,    36,     0,     0,     0,
       0,    94,    95,   225,     0,   249,     0,     0,     0,   265,
       0,     0,     0,     0,   280,   281,   282,   283,   284,     0,
       0,   287,     0,    64,     0,    80,    66,     0,     0,    75,
      77,    83,   290,     0,   292,   293,   294,   295,   296,   297,
       0,     0,     0,   300,   301,     0,   304,    22,    25,     0,
       0,   317,     0,     0,     0,    33,     0,    35,     0,     0,
      44,     0,     0,   224,   252,     0,     0,   237,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    65,    81,     0,
      76,     0,     0,     0,     0,     0,     0,     0,     0,   378,
     366,   367,   368,   369,   370,   371,     0,   379,   382,     0,
       0,     0,     0,     0,     0,    34,     0,     0,    41,     0,
      43,     0,   238,     0,     0,   235,     0,   233,     0,   266,
       0,   274,   275,   276,   285,   286,     0,     0,     0,     0,
     299,   303,     0,    23,   380,     0,     0,     0,     0,    52,
       0,     0,    40,     0,     0,     0,     0,     0,     0,     0,
      42,     0,     0,     0,     0,   231,     0,     0,     0,     0,
       0,     0,     0,   383,   372,   373,   374,   375,   376,   377,
       0,     0,     0,    49,     0,    51,     0,    37,    39,     0,
      48,     0,     0,     0,   236,     0,     0,     0,   269,     0,
       0,     0,     0,    24,   381,     0,     0,     0,     0,     0,
      50,    38,    45,    47,     0,   234,   232,     0,     0,     0,
       0,     0,    84,    56,     0,     0,     0,    46,   230,    73,
     291,    82,     0,    53,    55,     0,     0,    54,   298
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,   178,   179,   180,   609,   613,   369,   372,   614,   615,
     616,   797,   383,   182,   183,   184,   185,   186,   187,   188,
     374,   788,   375,   189,   190,   226,   192,   193,   194,   195,
     196,   227,   898,   899,   198,   199,   655,   200,   201,   202,
     277,   203,  1147,  1148,  1185,  1149,   610
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -1122
static const yytype_int16 yypact[] =
{
    1433,    24, -1122, -1122, -1122, -1122, -1122, -1122, -1122,   521,
   -1122,  6490,  4310,  7144,  6490, -1122,  8016,   111,   111,    46,
      52,    61,   146,   148,   195,   200,   212,   260,   267,   269,
     271,   276,   278,   286,   317,   339,   343,   351,   379,   385,
     406,   413,   466,   469,   475,   518,   524,   532,   554,   621,
     625,   648,   649,   652,   654,   673,   675,   688,   690,   167,
     345,   430,   468,   474,    94,   483,   526,   531,   692,   596,
     630,   678,   693,   702,   718, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122,  6490, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122,   696,   732,   734,   746,   756,   768,   774,
     778,   780,   795,   796,   802,   826,   831,   849,   853,   856,
     860,   861,   863,   871,   885,   901,   902,   903,   906,   907,
     908,   909,   911,   912,   913,   914,   939,   943,   944,   958,
     976,   984,   988,   998,  1001,  1011,  1020,  1028,  1029,  1042,
    1048,  1058,  1065,  1070,  1094,  1095,  1097,  1099,  1102,  1103,
    1104,  1107,  6490,   138, -1122,   366,  6490,  1118,  1119,  1127,
    1128, -1122,  1132,  1133,   431,  1135, -1122,   408,   433,  1656,
    2763,   410, -1122, -1122,   428,    21, -1122,   481, -1122,   171,
   -1122,   309,     2,  1100,  8016,   544, -1122,    14, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122,  4746,  4528,  6490,  1137,   797,
     797,   797,   797,   797,   189,   797,   797,   797,   797,   797,
     797,   797,   797,   797,   366,    29, -1122,    20,  4964,   420,
     667,     2,  1098, -1122, -1122, -1122,  6490,  6490,  6490,  6490,
    6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,
    6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,
    6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,
    6490,  6490,  6490,  6490,  6490,  6490,  5182, -1122,  5182, -1122,
    5182, -1122,  5182, -1122,  5182, -1122,  6490,  5182, -1122,  5182,
   -1122,  5182, -1122,  5182, -1122,  4089,  5182, -1122,  5182, -1122,
    5182, -1122,  5182, -1122,  5182, -1122,  5182, -1122,   555,   316,
    6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,
    6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,
    6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,
    6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,
    6490,   484,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,
    6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490,  6490, -1122,
      -7,   473, -1122,   740,   235,    32,   -18,  6490,   807,  6490,
     813,  6490,    80, -1122,  1133,  5400, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,   461, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122,   810, -1122, -1122,
   -1122,   858, -1122, -1122, -1122,   564, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
     671,   936, -1122,   707,  -107,  3868,   711, -1122, -1122,  6490,
    6490, -1122,  6490,  7144,  7144,  6490,  6490,  7144,  6708,  6926,
    7144,  7144,  7144,  7144,  7144,  7144,  7144,  7144, -1122,  7362,
    7580,  6490,   955,  7798, -1122,   683,  1138,  1142,   555,   555,
     706,  1144,   965,   727,   555,   185, -1122, -1122,  6490,  6490,
   -1122, -1122, -1122,    31,    35,    37,    39,    41,    44,    47,
      59,    72,    76,    79,    81,    83,    87,    89,    91,    96,
     100,   104,   125,   128,   132,   134,   136,   141,   153,   175,
     181,   257,   325,   347,   349,   359,   377,   381,   383,   387,
     411,   415,   417, -1122,    16,    68,    85,   122,   130,   419,
     353,   561,   847,   862,   769,   892,   963,   977,   979,   981,
     986,  6490,   421,   423,   183,    22,   634,   425,   427,   429,
     658,   672,   674,   432,   434,   436,   676,   680,   682,   684,
     686,   698,   708,   712,   440,   457,   464,   470,   844,   941,
     472,   476,   985,   478,   714,   480,   482,   485,   487,   489,
     716,  1017,   720,  1034,   722,   724,   726,   730,   738,   748,
     754,   757,   764,   766,  1049,   770,   772,   776,  1063,   779,
     782,  4089,  6490,  6490,  1141, -1122, -1122, -1122,   366,  4089,
     492,  1145,   495,  1140,    27,   258,  -100,  1147, -1122, -1122,
     500, -1122, -1122, -1122, -1122, -1122,  1150, -1122,  4089, -1122,
   -1122,  -107,   951,   555,   555, -1122,     2,     2,   555,   555,
    1100,  7144,  1100,  7144,  1100,  1100,  1100,   544,   544,   544,
     544,   544,   544,  8016,  8016, -1122,  8016,  8016, -1122,   117,
     573,  8016,  8016, -1122, -1122,  6490,  6490,  6490,  1152, -1122,
    5618,  1143,  1157,   344,   650, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122, -1122,   543, -1122,  6490,
   -1122, -1122,  6490, -1122,  6490,  6490, -1122, -1122, -1122,  6490,
    6490,  6490, -1122, -1122, -1122,  6490,  6490,  6490,  6490,  6490,
    6490,  6490,  6490, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
   -1122,  1146,  1148,  6490, -1122, -1122, -1122, -1122, -1122,  6490,
    1149,  6490,  1176,  6490,  6490,  6490,  6490,  6490,  6490,  6490,
    6490,  6490,  6490, -1122,  6490,  6490,  6490, -1122,  6490,  6490,
     993,   -16,   -13,  6490, -1122, -1122, -1122, -1122, -1122,  6490,
   -1122,  6490,  1161,   936,  1879,  -100, -1122, -1122, -1122, -1122,
    1100,  1100, -1122, -1122, -1122, -1122, -1122,  6490, -1122, -1122,
     510,   528,  1177,  1172,   555,  1195, -1122, -1122, -1122,    -4,
   -1122,   801,   530,   806,   808,   811,   814,   533,   535,   552,
     558,   563,   823,   830,   565,  5836,  6054,  1179,   837,  6272,
     567,  1199,  1201,   839,   571,   580,   583,   586,   600,   603,
     841,   845,   877,   611,   616,   910,   619,  4089,  4089,  6490,
     555,   923,   623,  1202, -1122,  6490, -1122,    64,  2984,  2100,
    1203, -1122, -1122, -1122,  6490, -1122,  6490,  2321,  6490, -1122,
    6490,  6490,  6490,  6490, -1122, -1122, -1122, -1122, -1122,  6490,
    6490, -1122,  6490,   555,  6490,   555, -1122,  6490,  6490,   555,
   -1122, -1122, -1122,  6490, -1122, -1122, -1122, -1122, -1122, -1122,
    6490,  6490,  6490, -1122, -1122,  6490, -1122, -1122, -1122,   -21,
     165, -1122,  -100,   179,  6490, -1122,  6490, -1122,   284,  6490,
   -1122,   577,  3205, -1122,   555,   628,  6490, -1122,   805,  3426,
    1205,   934,   632,  1206,   636,  1207,   639,   555,   555,   938,
     555,   940,   942,   945,   641,  1208,  6490,  4089,  1041, -1122,
   -1122, -1122, -1122, -1122, -1122, -1122,  1004, -1122, -1122,  1181,
    2542,  -107,   850,  1008,  6490, -1122,  1018,  6490, -1122,  6490,
   -1122,   820, -1122,  1035,  6490, -1122,  6490, -1122,   829, -1122,
    6490, -1122, -1122, -1122, -1122, -1122,  6490,  6490,  6490,  6490,
   -1122, -1122,    -9, -1122,  1209,  1214,   983,  1210,  6490, -1122,
     833,  3647, -1122,  -107,  -107,  1044,  -107,  1047,  1050,  6490,
   -1122,  -107,  1052,  1057,  6490, -1122,  1217,   947,   950,   953,
     972,  4089,  1041, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
     968,  1064,  6490, -1122,  6490, -1122,   842, -1122, -1122,  -107,
   -1122,  -107,  -107,  1073, -1122,  -107,  -107,  1078, -1122,  6490,
    6490,  6490,  6490, -1122, -1122,  1219,  -107,  1081,  1087,  6490,
   -1122, -1122, -1122, -1122,  -107, -1122, -1122,  -107,  1221,   644,
    1226,   975, -1122, -1122,  -107,  -107,  1089, -1122, -1122, -1122,
   -1122, -1122,  6490, -1122, -1122,  -107,  1227, -1122, -1122
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -1122, -1122, -1122,  -175,   160,    63, -1122, -1122,  -605,  -765,
   -1122,  -602,   857, -1122, -1122, -1122, -1122, -1122, -1122,  -203,
     454, -1122, -1122,   -11,  1153,   114,    -6,  -615,   -12,   569,
      -5,   187,   346, -1122, -1122, -1122, -1122, -1122, -1122, -1122,
     341, -1122, -1121,    23,    36, -1122, -1122
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -131
static const yytype_int16 yytable[] =
{
     225,   230,   645,   232,   608,   234,   235,   231,  1056,   807,
     811,   233,   820,   822,   824,   825,   826,  1184,   623,   624,
     627,   623,   624,   623,   624,   628,   623,   624,   641,   629,
     623,   624,   623,   624,   641,   903,  -116,   619,   620,   630,
     970,   904,   651,   642,   855,   204,   971,   979,   856,   652,
     857,   786,   858,   787,   859,   623,   624,   860,   236,   643,
     861,   623,   624,   181,   237,   643,   623,   624,   623,   624,
     623,   624,   862,   238,   623,   624,   623,   624,   623,   624,
     623,   624,   308,   623,   624,   863,   623,   624,  -117,   864,
     795,  1184,   865,   796,   866,   584,   867,   586,   623,   624,
     868,   164,   869,   585,   870,  -118,   286,   623,   624,   871,
     287,   623,   624,   872,   191,   623,   624,   873,   623,   624,
     623,   624,   623,   624,   623,   624,   623,   624,   623,   624,
     623,   624,   986,   748,   749,   623,   624,   752,   874,   623,
     624,   875,  -119,   623,   624,   876,   761,   877,   371,   878,
    -120,   370,    17,    18,   879,   376,   623,   624,   239,   774,
     240,   623,   624,   778,   623,   624,   880,   623,   624,   623,
     624,   623,   624,   623,   624,   623,   624,  1138,  1136,  1137,
     623,   624,   789,   276,  1038,  1039,   781,   197,   881,   638,
     622,  1211,   623,   624,   882,   648,   649,   164,   631,   585,
    1151,   286,   902,   977,   850,   650,   980,   241,   981,  1048,
     623,   624,   242,   851,   623,   624,   852,   654,   623,   624,
     623,   624,   623,   624,   243,   663,   664,   665,   666,   667,
     668,   669,   670,   671,   672,   673,   674,   675,   676,   677,
     678,   679,   680,   681,   682,   683,   684,   685,   686,   687,
     688,   689,   690,   691,   692,   693,   694,   695,   696,   697,
     698,   699,   700,   701,   702,   704,   584,   705,   586,   706,
     883,   707,   244,   708,   972,   709,   710,   973,   711,   245,
     712,   246,   713,   247,  1112,   715,  1104,   716,   248,   717,
     249,   718,  1119,   719,   191,   720,   623,   624,   250,   722,
     723,   724,   725,   726,   727,   728,   729,   730,   731,   732,
     733,   734,   735,   736,   737,   738,   739,   740,   741,   742,
     743,   744,   745,   746,   747,   625,   626,   750,   751,   251,
     753,   754,   755,   756,   757,   758,   759,   760,   884,   762,
     612,   764,   765,   766,   767,   768,   769,   770,   771,   772,
     773,   252,   775,   776,   777,   253,   779,   780,   714,   997,
     885,   278,   886,   254,   623,   624,   790,   197,   792,  1047,
     794,  1044,   887,  -121,   800,  1139,  1140,  1141,  1142,  1143,
    1144,  1145,  1146,   623,   624,  1191,   623,   624,   623,   624,
     888,   255,   623,   624,   889,   373,   890,   256,   623,   624,
     891,   279,   281,   283,   285,   288,   290,   292,   294,   191,
     297,   299,   301,   303,   305,   307,   623,   624,   257,   815,
     623,   624,   623,   624,   892,   258,   623,   624,   893,   386,
     894,   617,   895,   387,   900,   656,   901,   721,   906,   785,
     907,   384,   908,  1108,  1111,   912,   280,   913,   618,   914,
     623,   624,  1118,   923,   623,   624,   623,   624,   623,   624,
     623,   624,   623,   624,   623,   624,   623,   624,   623,   624,
     924,   623,   624,   623,   624,   623,   624,   925,   259,   623,
     624,   260,   197,   926,   282,   929,   584,   261,   586,   930,
     284,   932,   801,   934,   763,   935,   623,   624,   936,   289,
     937,   621,   938,   623,   624,   966,  1154,  1161,   968,   623,
     624,   623,   624,   976,  1168,   623,   624,   623,   624,   623,
     624,   623,   624,  1051,   623,   624,   623,   624,   623,   624,
     262,   623,   624,   205,   623,   624,   263,   206,   207,   623,
     624,  1052,   291,  1059,   264,  1190,  1064,   293,  1065,   623,
     624,   279,   281,   283,   285,   288,   290,   292,   294,   297,
     299,   301,   303,   305,   307,  1066,   265,   623,   624,   623,
     624,  1067,   623,   624,   623,   624,  1068,   999,  1071,   804,
    1080,  -122,   623,   624,  1084,   987,  1226,   639,   640,  -100,
    -100,   623,   624,  1085,   623,   624,  1086,   623,   624,  1087,
     623,   624,   623,   624,   623,   624,   623,   624,   813,   814,
     623,   624,   296,  1088,   818,   819,  1089,   816,   817,   623,
     624,   974,   623,   624,  1093,   623,   624,   834,   837,  1094,
     839,   842,  1096,   266,   835,   838,  1101,   267,   843,   623,
     624,  1162,   623,   624,   992,  1171,   298,   853,   854,  1173,
     623,   624,  1175,   905,  1180,   623,   624,  1270,   623,   624,
     268,   269,   623,   624,   270,   998,   271,   623,   624,   782,
     783,   623,   624,   623,   624,   623,   624,   909,   623,   624,
     623,   624,   657,   623,   624,   272,   658,   273,   659,   623,
     624,   910,   805,   911,   300,   915,   844,   623,   624,   916,
     274,   917,   275,   918,   295,   919,   623,   624,   310,   302,
     897,   623,   624,   623,   624,   623,   624,   920,   304,   623,
     624,   623,   624,   623,   624,   623,   624,   921,   808,   191,
    1017,   922,   812,   933,   306,   939,   703,   623,   624,   941,
    1022,   943,   849,   944,   311,   945,   312,   623,   624,   946,
     784,   623,   624,   623,   624,   623,   624,   947,   313,   623,
     624,   623,   624,   623,   624,   623,   624,   948,   314,   623,
     624,   961,   962,   949,   809,   810,   950,   623,   624,   584,
     315,   586,   896,   951,  1050,   952,   316,   623,   624,   954,
     317,   955,   318,   623,   624,   956,   623,   624,   958,  1157,
    1049,   959,   197,   623,   624,   623,   624,   319,   320,   623,
     624,   623,   624,   650,   321,   623,   624,   791,   623,   624,
    1058,   623,   624,   793,  1057,  1060,   802,  1061,   982,   983,
    1062,   984,   985,  1063,   990,   991,   988,   989,   322,   994,
     623,   624,  1069,   323,   960,   623,   624,   623,   624,  1070,
     623,   624,   965,   623,   624,  1120,  1077,   927,  1083,  1123,
    1090,   324,   623,   624,  1091,   325,  1125,  -123,   326,   623,
     624,  1193,   327,   328,   803,   329,   623,   624,   623,   624,
     623,   624,  -124,   330,   623,   624,   623,   624,   897,   623,
     624,  1001,  1135,  1002,  1003,   191,  1092,   331,  1004,  1005,
    1006,   623,   624,   191,  1007,  1008,  1009,  1010,  1011,  1012,
    1013,  1014,  -125,   332,   333,   334,   623,   624,   335,   336,
     337,   338,   191,   339,   340,   341,   342,  1150,  1018,  1095,
    1020,   623,   624,  1023,  1024,  1025,  1026,  1027,  1028,  1029,
    1030,  1031,  1100,  1032,  1033,  1034,   806,  1035,  1036,   623,
     624,   343,  1040,  1170,   928,   344,   345,  1176,  1041,  1177,
    1042,  1178,   623,   624,  1179,   840,  1239,  1206,   197,  1240,
     346,   978,  1241,   623,   624,   848,   197,   623,   624,   623,
     624,   623,   624,  -126,   623,   624,   623,   624,   347,   623,
     624,  1242,   623,   624,  1272,   197,   348,  -127,   931,  -129,
     349,  -128,   623,   624,  1073,  1075,  -130,   584,  1079,   586,
     350,   623,   624,   351,   623,   624,   623,   624,   623,   624,
     623,   624,   584,   352,   586,   623,   624,  1164,  1099,  1194,
     940,   584,   353,   586,  1103,   584,  1258,   586,  1260,  1196,
     354,   355,  1199,  1114,   584,  1115,   586,   623,   624,  1121,
    1122,  1204,  1124,   942,   356,  1222,  1201,   623,   624,  1126,
     357,  1127,   953,  1128,  1249,  1229,  1129,  1130,  1231,  1276,
     358,  1232,  1131,  1235,   623,   624,   957,   359,  1236,  1132,
    1133,  1134,   360,   623,   624,  1246,   623,   624,   191,   623,
     624,   623,   624,  1152,  1254,  1153,   623,   624,  1156,  1257,
    1097,  1098,  1264,   623,   624,  1163,   361,   362,  1265,   363,
    1275,   364,   623,   624,   365,   366,   367,   623,   624,   368,
     623,   624,   660,   661,   662,  1182,   623,   624,   623,   624,
     377,   378,   632,   633,  1046,   634,   635,   623,   624,   379,
     380,   636,   637,  1195,   381,   382,  1197,   385,  1198,   205,
     845,   191,   191,  1202,   846,  1203,   847,   963,   967,   969,
     975,   197,   191,   191,   987,  1207,  1208,  1209,  1210,   973,
     611,   191,   996,  1015,   995,  1016,  1019,  1221,  1139,  1140,
    1141,  1142,  1143,  1144,  1145,  1146,  1021,  1037,  1233,  1043,
    1053,  1054,  1076,  1237,  1214,  1215,  1216,  1217,  1218,  1219,
    1183,   827,   828,   829,   830,   831,   832,  1105,  1107,  1110,
    1055,  1247,  1081,  1248,  1082,  1102,  1113,  1117,  1169,  1172,
    1174,  1181,  1186,  1187,   197,   197,   191,  1213,  1212,  1259,
    1238,  1261,  1262,   191,  1269,   197,   197,  1220,  1266,  1271,
    1278,   798,   964,  1245,   197,  1000,   309,     0,  1244,     0,
       0,   191,  1140,  1141,  1142,  1143,  1144,  1145,  1146,     0,
       0,     0,     0,     0,   191,     0,     0,     0,  1155,     0,
       0,  1158,  1160,     0,  1243,     0,     0,     0,  1165,  1167,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   197,
       0,     0,     0,     0,     0,   191,   197,     0,     0,     0,
    1189,  1192,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1200,     0,     0,   197,   191,     0,     0,  1205,     0,
       0,     0,     0,     0,     0,     0,     0,   197,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1223,  1225,     0,  1227,  1228,     0,  1230,     0,     0,     0,
       0,  1234,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   197,     0,
       0,     0,     0,     0,     0,     0,  1250,     0,     0,  1251,
       0,  1252,  1253,     0,     0,  1255,  1256,     0,   197,     0,
       0,     0,     0,     0,     0,     0,  1263,     0,     0,     0,
       0,     0,     0,     0,  1267,     0,     0,  1268,     0,     0,
       0,     0,     0,     0,  1273,  1274,     0,     0,     0,     0,
       0,     0,     0,     0,     1,  1277,     2,     3,     4,     5,
       6,     7,     8,     9,    10,    11,     0,    12,     0,     0,
       0,     0,     0,    13,     0,    14,     0,     0,     0,     0,
       0,     0,     0,    15,     0,     0,     0,     0,     0,     0,
       0,    16,     0,     0,    17,    18,     0,     0,     0,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,     0,    94,    95,    96,    97,    98,
      99,   100,   101,   102,     0,     0,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,     0,     0,   163,     0,
       0,     0,     0,     0,   164,     0,   165,     0,   166,   167,
     168,   169,   170,   171,     0,   172,     0,     0,     0,     0,
       0,     0,     0,   173,   174,     0,   175,   176,   177,   388,
       0,   389,   390,   391,   392,   393,   394,   395,   396,   397,
     398,   399,   400,   401,   402,   403,   404,   405,   406,   407,
     408,   409,   410,   411,   412,   413,   414,   415,   416,   417,
       0,   418,   419,   420,   421,   422,   423,   424,   425,   426,
     427,   428,   429,   430,   431,   432,   433,   434,   435,   436,
     437,   438,   439,   440,   441,   442,   443,   444,   445,   446,
     447,   448,   449,   450,   451,   452,   453,   454,   455,   456,
     457,   458,   459,   460,   461,   462,   463,   464,   465,   466,
     467,   468,   469,   470,   471,   472,   473,   474,   475,   476,
     477,   478,   479,   480,   481,   482,   483,   484,   485,   486,
     487,   488,   489,   490,   491,   492,   493,   494,   495,   496,
     497,   498,   499,   500,   501,   502,   503,   504,   505,   506,
     507,   508,   509,   510,   511,   512,   513,   514,   515,   516,
     517,   518,   519,   520,   521,   522,   523,   524,   525,   526,
     527,   528,   529,   530,   531,   532,   533,   534,   535,   536,
     537,   538,   539,   540,   541,   542,   543,   544,   545,   546,
     547,   548,   549,   550,   551,   552,   553,   554,   555,   556,
     557,   558,   559,   560,   561,   562,   563,   564,   565,   566,
     567,   568,   569,   570,   571,   572,   573,   574,   575,   576,
     577,   578,   579,   580,   581,   582,   583,   164,   584,   585,
     586,   587,   588,   589,   590,   591,   592,   593,   594,   595,
     596,   597,   598,   599,   600,   601,   602,   603,   604,   605,
     606,   607,     2,     3,     4,     5,     6,     7,     8,     9,
      10,    11,     0,    12,     0,     0,     0,     0,     0,    13,
       0,    14,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    16,     0,     0,
      17,    18,     0,     0,     0,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
       0,    94,    95,    96,    97,    98,    99,   100,   101,   102,
       0,     0,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,     0,     0,   163,     0,     0,     0,     0,     0,
     164,   584,   165,   586,   166,   167,   168,   169,   170,   171,
       0,   172,     0,     0,     0,     0,     0,     0,   611,   173,
     174,  1045,   175,     2,     3,     4,     5,     6,     7,     8,
       9,    10,    11,     0,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    16,     0,
       0,    17,    18,     0,     0,     0,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,     0,    94,    95,    96,    97,    98,    99,   100,   101,
     102,     0,     0,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,     0,     0,   163,     0,     0,     0,     0,
       0,   164,   584,   165,   586,   166,   167,   168,   169,   170,
     171,     0,   172,     0,     0,     0,     0,     0,     0,   611,
     173,   174,  1109,   175,     2,     3,     4,     5,     6,     7,
       8,     9,    10,    11,     0,    12,     0,     0,     0,     0,
       0,    13,     0,    14,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    16,
       0,     0,    17,    18,     0,     0,     0,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,     0,    94,    95,    96,    97,    98,    99,   100,
     101,   102,     0,     0,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,     0,     0,   163,     0,     0,     0,
       0,     0,   164,   584,   165,   586,   166,   167,   168,   169,
     170,   171,     0,   172,     0,     0,     0,     0,     0,     0,
     611,   173,   174,  1116,   175,     2,     3,     4,     5,     6,
       7,     8,     9,    10,    11,     0,    12,     0,     0,     0,
       0,     0,    13,     0,    14,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      16,     0,     0,    17,    18,     0,     0,     0,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,     0,    94,    95,    96,    97,    98,    99,
     100,   101,   102,     0,     0,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,     0,     0,   163,     0,     0,
       0,     0,     0,   164,   584,   165,   586,   166,   167,   168,
     169,   170,   171,     0,   172,     0,     0,     0,     0,     0,
       0,   611,   173,   174,  1188,   175,     2,     3,     4,     5,
       6,     7,     8,     9,    10,    11,     0,    12,     0,     0,
       0,     0,     0,    13,     0,    14,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    16,     0,     0,    17,    18,     0,     0,     0,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,     0,    94,    95,    96,    97,    98,
      99,   100,   101,   102,     0,     0,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,     0,     0,   163,     0,
       0,     0,     0,     0,   164,   584,   165,   586,   166,   167,
     168,   169,   170,   171,     0,   172,     0,     0,     0,     0,
       0,     0,   611,   173,   174,     0,   175,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    16,     0,     0,    17,    18,     0,     0,     0,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,     0,    94,    95,    96,    97,
      98,    99,   100,   101,   102,     0,     0,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,     0,     0,   163,
       0,     0,     0,     0,     0,   164,   584,   165,   586,   166,
     167,   168,   169,   170,   171,     0,   172,     0,     0,     0,
       0,     0,     0,     0,   173,   174,  1106,   175,     2,     3,
       4,     5,     6,     7,     8,     9,    10,    11,     0,    12,
       0,     0,     0,     0,     0,    13,     0,    14,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    16,     0,     0,    17,    18,     0,     0,
       0,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,     0,    94,    95,    96,
      97,    98,    99,   100,   101,   102,     0,     0,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,     0,     0,
     163,     0,     0,     0,     0,     0,   164,   584,   165,   586,
     166,   167,   168,   169,   170,   171,     0,   172,     0,     0,
       0,     0,     0,     0,     0,   173,   174,  1159,   175,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    16,     0,     0,    17,    18,     0,
       0,     0,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,     0,    94,    95,
      96,    97,    98,    99,   100,   101,   102,     0,     0,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,     0,
       0,   163,     0,     0,     0,     0,     0,   164,   584,   165,
     586,   166,   167,   168,   169,   170,   171,     0,   172,     0,
       0,     0,     0,     0,     0,     0,   173,   174,  1166,   175,
       2,     3,     4,     5,     6,     7,     8,     9,    10,    11,
       0,    12,     0,     0,     0,     0,     0,    13,     0,    14,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    16,     0,     0,    17,    18,
       0,     0,     0,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,     0,    94,
      95,    96,    97,    98,    99,   100,   101,   102,     0,     0,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
       0,     0,   163,     0,     0,     0,     0,     0,   164,   584,
     165,   586,   166,   167,   168,   169,   170,   171,     0,   172,
       0,     0,     0,     0,     0,     0,     0,   173,   174,  1224,
     175,     2,     3,     4,     5,     6,     7,     8,     9,    10,
      11,     0,    12,     0,     0,     0,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,    17,
      18,     0,     0,     0,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,     0,
      94,    95,    96,    97,    98,    99,   100,   101,   102,     0,
       0,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,     0,     0,   163,     0,     0,     0,     0,     0,   164,
     584,   165,   586,   166,   167,   168,   169,   170,   171,     0,
     172,     0,     0,     0,     0,     0,     0,     0,   173,   174,
       0,   175,     2,     3,     4,     5,     6,     7,     8,     9,
      10,    11,     0,    12,     0,     0,     0,     0,     0,    13,
       0,    14,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    16,     0,     0,
      17,    18,     0,     0,     0,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
       0,    94,    95,    96,    97,    98,    99,   100,   101,   102,
       0,     0,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,     0,     0,   163,     0,     0,     0,     0,     0,
     164,     0,   165,     0,   166,   167,   168,   169,   170,   171,
       0,   172,     0,     0,     0,     0,     0,     0,     0,   173,
     174,     0,   175,     2,     3,     4,     5,     6,     7,     8,
     208,    10,    11,     0,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,   228,     0,     0,     0,     0,     0,     0,    16,     0,
     229,    17,    18,     0,     0,     0,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,   209,   210,   211,   212,
     213,   214,   215,   216,   217,    68,   218,   219,   220,   221,
     222,   223,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,     0,    94,    95,    96,    97,    98,    99,     0,     0,
       0,     0,     0,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   145,   146,   147,   148,   149,
     150,   151,   152,     0,   154,     0,   156,   157,   158,   159,
     160,   161,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   224,     0,     0,   167,   168,     0,     0,
     171,     0,   172,     0,     0,     0,     0,     0,     0,     0,
     173,     2,     3,     4,     5,     6,     7,     8,   208,    10,
      11,     0,    12,     0,     0,     0,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,    17,
      18,     0,     0,     0,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,   209,   210,   211,   212,   213,   214,
     215,   216,   217,    68,   218,   219,   220,   221,   222,   223,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,     0,
      94,    95,    96,    97,    98,    99,     0,     0,     0,   646,
     647,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   145,   146,   147,   148,   149,   150,   151,
     152,     0,   154,     0,   156,   157,   158,   159,   160,   161,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   224,     0,     0,   167,   168,     0,     0,   171,     0,
     172,     0,     0,     0,     0,     0,     0,     0,   173,     2,
       3,     4,     5,     6,     7,     8,   208,    10,    11,   644,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    16,     0,     0,    17,    18,     0,
       0,     0,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,   209,   210,   211,   212,   213,   214,   215,   216,
     217,    68,   218,   219,   220,   221,   222,   223,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,     0,    94,    95,
      96,    97,    98,    99,     0,     0,     0,     0,     0,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   145,   146,   147,   148,   149,   150,   151,   152,     0,
     154,     0,   156,   157,   158,   159,   160,   161,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   224,
       0,     0,   167,   168,     0,     0,   171,     0,   172,     0,
       0,     0,     0,     0,     0,     0,   173,     2,     3,     4,
       5,     6,     7,     8,   208,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,     0,     0,     0,
       0,     0,     0,     0,     0,   653,     0,     0,     0,     0,
       0,     0,    16,     0,     0,    17,    18,     0,     0,     0,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
     209,   210,   211,   212,   213,   214,   215,   216,   217,    68,
     218,   219,   220,   221,   222,   223,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,     0,    94,    95,    96,    97,
      98,    99,     0,     0,     0,     0,     0,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   145,
     146,   147,   148,   149,   150,   151,   152,     0,   154,     0,
     156,   157,   158,   159,   160,   161,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   224,     0,     0,
     167,   168,     0,     0,   171,     0,   172,     0,     0,     0,
       0,     0,     0,     0,   173,     2,     3,     4,     5,     6,
       7,     8,   208,    10,    11,     0,    12,     0,     0,     0,
       0,     0,    13,     0,    14,     0,     0,     0,     0,     0,
       0,     0,   703,     0,     0,     0,     0,     0,     0,     0,
      16,     0,     0,    17,    18,     0,     0,     0,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,   209,   210,
     211,   212,   213,   214,   215,   216,   217,    68,   218,   219,
     220,   221,   222,   223,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,     0,    94,    95,    96,    97,    98,    99,
       0,     0,     0,     0,     0,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   145,   146,   147,
     148,   149,   150,   151,   152,     0,   154,     0,   156,   157,
     158,   159,   160,   161,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   224,     0,     0,   167,   168,
       0,     0,   171,     0,   172,     0,     0,     0,     0,     0,
       0,     0,   173,     2,     3,     4,     5,     6,     7,     8,
     208,    10,    11,   799,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    16,     0,
       0,    17,    18,     0,     0,     0,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,   209,   210,   211,   212,
     213,   214,   215,   216,   217,    68,   218,   219,   220,   221,
     222,   223,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,     0,    94,    95,    96,    97,    98,    99,     0,     0,
       0,     0,     0,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   145,   146,   147,   148,   149,
     150,   151,   152,     0,   154,     0,   156,   157,   158,   159,
     160,   161,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   224,     0,     0,   167,   168,     0,     0,
     171,     0,   172,     0,     0,     0,     0,     0,     0,     0,
     173,     2,     3,     4,     5,     6,     7,     8,   208,    10,
      11,     0,    12,     0,     0,     0,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,   993,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,    17,
      18,     0,     0,     0,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,   209,   210,   211,   212,   213,   214,
     215,   216,   217,    68,   218,   219,   220,   221,   222,   223,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,     0,
      94,    95,    96,    97,    98,    99,     0,     0,     0,     0,
       0,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   145,   146,   147,   148,   149,   150,   151,
     152,     0,   154,     0,   156,   157,   158,   159,   160,   161,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   224,     0,     0,   167,   168,     0,     0,   171,     0,
     172,     0,     0,     0,     0,     0,     0,     0,   173,     2,
       3,     4,     5,     6,     7,     8,   208,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,  1072,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    16,     0,     0,    17,    18,     0,
       0,     0,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,   209,   210,   211,   212,   213,   214,   215,   216,
     217,    68,   218,   219,   220,   221,   222,   223,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,     0,    94,    95,
      96,    97,    98,    99,     0,     0,     0,     0,     0,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   145,   146,   147,   148,   149,   150,   151,   152,     0,
     154,     0,   156,   157,   158,   159,   160,   161,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   224,
       0,     0,   167,   168,     0,     0,   171,     0,   172,     0,
       0,     0,     0,     0,     0,     0,   173,     2,     3,     4,
       5,     6,     7,     8,   208,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,     0,     0,     0,
       0,  1074,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    16,     0,     0,    17,    18,     0,     0,     0,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
     209,   210,   211,   212,   213,   214,   215,   216,   217,    68,
     218,   219,   220,   221,   222,   223,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,     0,    94,    95,    96,    97,
      98,    99,     0,     0,     0,     0,     0,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   145,
     146,   147,   148,   149,   150,   151,   152,     0,   154,     0,
     156,   157,   158,   159,   160,   161,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   224,     0,     0,
     167,   168,     0,     0,   171,     0,   172,     0,     0,     0,
       0,     0,     0,     0,   173,     2,     3,     4,     5,     6,
       7,     8,   208,    10,    11,     0,    12,     0,     0,     0,
       0,     0,    13,     0,    14,     0,     0,     0,     0,  1078,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      16,     0,     0,    17,    18,     0,     0,     0,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,   209,   210,
     211,   212,   213,   214,   215,   216,   217,    68,   218,   219,
     220,   221,   222,   223,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,     0,    94,    95,    96,    97,    98,    99,
       0,     0,     0,     0,     0,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   145,   146,   147,
     148,   149,   150,   151,   152,     0,   154,     0,   156,   157,
     158,   159,   160,   161,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   224,     0,     0,   167,   168,
       0,     0,   171,     0,   172,     0,     0,     0,     0,     0,
       0,     0,   173,     2,     3,     4,     5,     6,     7,     8,
     208,    10,    11,     0,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    16,     0,
       0,    17,    18,     0,     0,     0,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,   209,   210,   211,   212,
     213,   214,   215,   216,   217,    68,   218,   219,   220,   221,
     222,   223,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,     0,    94,    95,    96,    97,    98,    99,     0,     0,
       0,     0,     0,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   145,   146,   147,   148,   149,
     150,   151,   152,     0,   154,     0,   156,   157,   158,   159,
     160,   161,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   224,     0,     0,   167,   168,     0,     0,
     171,     0,   172,     0,     0,     0,     0,     0,     0,     0,
     173,     2,     3,     4,     5,     6,     7,     8,   208,    10,
      11,     0,    12,     0,   821,     0,     0,     0,     0,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,    17,
      18,     0,     0,     0,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,   209,   210,   211,   212,   213,   214,
     215,   216,   217,    68,   218,   219,   220,   221,   222,   223,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,     0,     0,
      94,    95,    96,    97,    98,    99,     0,     0,     0,     0,
       0,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   145,   146,   147,   148,   149,   150,   151,
     152,     0,   154,     0,   156,   157,   158,   159,   160,   161,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   224,     0,     0,   167,   168,     0,     0,   171,     0,
     172,     0,     0,     0,     0,     0,     0,     0,   173,     2,
       3,     4,     5,     6,     7,     8,   208,    10,    11,     0,
      12,     0,   823,     0,     0,     0,     0,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    16,     0,     0,    17,    18,     0,
       0,     0,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,   209,   210,   211,   212,   213,   214,   215,   216,
     217,    68,   218,   219,   220,   221,   222,   223,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,     0,     0,    94,    95,
      96,    97,    98,    99,     0,     0,     0,     0,     0,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   145,   146,   147,   148,   149,   150,   151,   152,     0,
     154,     0,   156,   157,   158,   159,   160,   161,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   224,
       0,     0,   167,   168,     0,     0,   171,     0,   172,     0,
       0,     0,     0,     0,     0,     0,   173,     2,     3,     4,
       5,     6,     7,     8,   208,    10,    11,     0,    12,     0,
       0,     0,     0,     0,     0,     0,    14,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    16,     0,     0,    17,    18,     0,     0,     0,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
     209,   210,   211,   212,   213,   214,   215,   216,   217,    68,
     218,   219,   220,   221,   222,   223,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,     0,     0,    94,    95,    96,    97,
      98,    99,     0,     0,     0,     0,     0,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   145,
     146,   147,   148,   149,   150,   151,   152,     0,   154,     0,
     156,   157,   158,   159,   160,   161,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   224,     0,     0,
     167,   168,     0,     0,   171,     0,   172,     0,     0,     0,
       0,     0,     0,     0,   173,     2,     3,     4,     5,     6,
       7,     8,   208,    10,    11,     0,    12,     0,     0,     0,
       0,     0,     0,     0,    14,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     833,     0,     0,    17,    18,     0,     0,     0,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,   209,   210,
     211,   212,   213,   214,   215,   216,   217,    68,   218,   219,
     220,   221,   222,   223,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,     0,     0,    94,    95,    96,    97,    98,    99,
       0,     0,     0,     0,     0,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   145,   146,   147,
     148,   149,   150,   151,   152,     0,   154,     0,   156,   157,
     158,   159,   160,   161,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   224,     0,     0,   167,   168,
       0,     0,   171,     0,   172,     0,     0,     0,     0,     0,
       0,     0,   173,     2,     3,     4,     5,     6,     7,     8,
     208,    10,    11,     0,    12,     0,     0,     0,     0,     0,
       0,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   836,     0,
       0,    17,    18,     0,     0,     0,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,   209,   210,   211,   212,
     213,   214,   215,   216,   217,    68,   218,   219,   220,   221,
     222,   223,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
       0,     0,    94,    95,    96,    97,    98,    99,     0,     0,
       0,     0,     0,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   145,   146,   147,   148,   149,
     150,   151,   152,     0,   154,     0,   156,   157,   158,   159,
     160,   161,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   224,     0,     0,   167,   168,     0,     0,
     171,     0,   172,     0,     0,     0,     0,     0,     0,     0,
     173,     2,     3,     4,     5,     6,     7,     8,   208,    10,
      11,     0,    12,     0,     0,     0,     0,     0,     0,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   841,     0,     0,    17,
      18,     0,     0,     0,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,   209,   210,   211,   212,   213,   214,
     215,   216,   217,    68,   218,   219,   220,   221,   222,   223,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,     0,     0,
      94,    95,    96,    97,    98,    99,     0,     0,     0,     0,
       0,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   145,   146,   147,   148,   149,   150,   151,
     152,     0,   154,     0,   156,   157,   158,   159,   160,   161,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   224,     0,     0,   167,   168,     0,     0,   171,     0,
     172,     0,     0,     0,     0,     0,     0,     0,   173,     2,
       3,     4,     5,     6,     7,     8,   208,    10,    11,     0,
      12,     0,     0,     0,     0,     0,     0,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,   209,   210,   211,   212,   213,   214,   215,   216,
     217,    68,   218,   219,   220,   221,   222,   223,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,     0,     0,    94,    95,
      96,    97,    98,    99,     0,     0,     0,     0,     0,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   145,   146,   147,   148,   149,   150,   151,   152,     0,
     154,     0,   156,   157,   158,   159,   160,   161,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   224,
       0,     0,   167,   168,     0,     0,   171,     0,   172,     0,
       0,     0,     0,     0,     0,     0,   173
};

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-1122)))

#define yytable_value_is_error(Yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
      11,    12,   205,    14,   179,    17,    18,    13,    12,   611,
     615,    16,   627,   628,   629,   630,   631,  1138,    39,    40,
      18,    39,    40,    39,    40,    23,    39,    40,    14,    27,
      39,    40,    39,    40,    14,    13,    20,    16,    17,    37,
      13,    19,    13,    29,    13,    21,    19,   812,    13,    29,
      13,    19,    13,    21,    13,    39,    40,    13,    12,    45,
      13,    39,    40,     0,    12,    45,    39,    40,    39,    40,
      39,    40,    13,    12,    39,    40,    39,    40,    39,    40,
      39,    40,    93,    39,    40,    13,    39,    40,    20,    13,
      10,  1212,    13,    13,    13,   202,    13,   204,    39,    40,
      13,   201,    13,   203,    13,    20,    12,    39,    40,    13,
      16,    39,    40,    13,     0,    39,    40,    13,    39,    40,
      39,    40,    39,    40,    39,    40,    39,    40,    39,    40,
      39,    40,    15,   336,   337,    39,    40,   340,    13,    39,
      40,    13,    20,    39,    40,    13,   349,    13,    10,    13,
      20,   162,    41,    42,    13,   166,    39,    40,    12,   362,
      12,    39,    40,   366,    39,    40,    13,    39,    40,    39,
      40,    39,    40,    39,    40,    39,    40,    12,   199,   200,
      39,    40,   200,    16,   200,   198,   193,     0,    13,   194,
      19,   200,    39,    40,    13,   206,   207,   201,   196,   203,
      21,    12,    19,   808,    19,    16,   821,    12,   823,   974,
      39,    40,    12,    28,    39,    40,    31,   228,    39,    40,
      39,    40,    39,    40,    12,   236,   237,   238,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,   254,   255,   256,   257,   258,   259,   260,
     261,   262,   263,   264,   265,   266,   267,   268,   269,   270,
     271,   272,   273,   274,   275,   276,   202,   278,   204,   280,
      13,   282,    12,   284,    16,   286,   287,    19,   289,    12,
     291,    12,   293,    12,  1049,   296,   222,   298,    12,   300,
      12,   302,  1057,   304,   180,   306,    39,    40,    12,   310,
     311,   312,   313,   314,   315,   316,   317,   318,   319,   320,
     321,   322,   323,   324,   325,   326,   327,   328,   329,   330,
     331,   332,   333,   334,   335,    16,    17,   338,   339,    12,
     341,   342,   343,   344,   345,   346,   347,   348,    13,   350,
     180,   352,   353,   354,   355,   356,   357,   358,   359,   360,
     361,    12,   363,   364,   365,    12,   367,   368,   295,    15,
      13,    16,    13,    12,    39,    40,   377,   180,   379,   974,
     381,   973,    13,    20,   385,   210,   211,   212,   213,   214,
     215,   216,   217,    39,    40,  1150,    39,    40,    39,    40,
      13,    12,    39,    40,    13,    29,    13,    12,    39,    40,
      13,    60,    61,    62,    63,    64,    65,    66,    67,   295,
      69,    70,    71,    72,    73,    74,    39,    40,    12,   622,
      39,    40,    39,    40,    13,    12,    39,    40,    13,    21,
      13,    21,    13,     0,    13,    15,    13,   121,    13,   204,
      13,    10,    13,  1048,  1049,    13,    16,    13,    20,    13,
      39,    40,  1057,    13,    39,    40,    39,    40,    39,    40,
      39,    40,    39,    40,    39,    40,    39,    40,    39,    40,
      13,    39,    40,    39,    40,    39,    40,    13,    12,    39,
      40,    12,   295,    13,    16,    13,   202,    12,   204,    13,
      16,    13,    31,    13,    10,    13,    39,    40,    13,    16,
      13,    20,    13,    39,    40,    13,   222,  1112,    13,    39,
      40,    39,    40,    13,  1119,    39,    40,    39,    40,    39,
      40,    39,    40,    13,    39,    40,    39,    40,    39,    40,
      12,    39,    40,    12,    39,    40,    12,    16,    17,    39,
      40,    13,    16,    13,    12,  1150,    13,    16,    13,    39,
      40,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,   220,   221,   222,   223,    13,    12,    39,    40,    39,
      40,    13,    39,    40,    39,    40,    13,    34,    13,    15,
      13,    20,    39,    40,    13,    12,  1191,    43,    44,    16,
      17,    39,    40,    13,    39,    40,    13,    39,    40,    13,
      39,    40,    39,    40,    39,    40,    39,    40,   619,   620,
      39,    40,    16,    13,   625,   626,    13,   623,   624,    39,
      40,   796,    39,    40,    13,    39,    40,   639,   640,    13,
     641,   643,    13,    12,   639,   640,    13,    12,   643,    39,
      40,    13,    39,    40,   847,    13,    16,   658,   659,    13,
      39,    40,    13,    19,    13,    39,    40,    13,    39,    40,
      12,    12,    39,    40,    12,    15,    12,    39,    40,   196,
     197,    39,    40,    39,    40,    39,    40,    19,    39,    40,
      39,    40,    15,    39,    40,    12,    19,    12,    21,    39,
      40,    19,    21,    19,    16,    19,    13,    39,    40,    19,
      12,    19,    12,    19,    12,    19,    39,    40,    12,    16,
     721,    39,    40,    39,    40,    39,    40,    19,    16,    39,
      40,    39,    40,    39,    40,    39,    40,    19,    21,   615,
     933,    19,    21,    19,    16,    19,    30,    39,    40,    19,
     943,    19,    15,    19,    12,    19,    12,    39,    40,    19,
      10,    39,    40,    39,    40,    39,    40,    19,    12,    39,
      40,    39,    40,    39,    40,    39,    40,    19,    12,    39,
      40,   782,   783,    19,   614,   615,    19,    39,    40,   202,
      12,   204,    13,    19,   987,    19,    12,    39,    40,    19,
      12,    19,    12,    39,    40,    19,    39,    40,    19,   222,
     975,    19,   615,    39,    40,    39,    40,    12,    12,    39,
      40,    39,    40,    16,    12,    39,    40,    10,    39,    40,
      19,    39,    40,    10,   999,    19,    16,    19,   833,   834,
      19,   836,   837,    19,   845,   846,   841,   842,    12,   850,
      39,    40,    19,    12,   781,    39,    40,    39,    40,    19,
      39,    40,   789,    39,    40,  1058,    19,    13,    19,  1062,
      19,    12,    39,    40,    19,    12,  1069,    20,    12,    39,
      40,    21,    12,    12,    16,    12,    39,    40,    39,    40,
      39,    40,    20,    12,    39,    40,    39,    40,   899,    39,
      40,   902,  1095,   904,   905,   781,    19,    12,   909,   910,
     911,    39,    40,   789,   915,   916,   917,   918,   919,   920,
     921,   922,    20,    12,    12,    12,    39,    40,    12,    12,
      12,    12,   808,    12,    12,    12,    12,  1102,   939,    19,
     941,    39,    40,   944,   945,   946,   947,   948,   949,   950,
     951,   952,    19,   954,   955,   956,    10,   958,   959,    39,
      40,    12,   963,    19,    13,    12,    12,    19,   969,    19,
     971,    19,    39,    40,    19,    10,    19,  1170,   781,    19,
      12,   811,    19,    39,    40,    10,   789,    39,    40,    39,
      40,    39,    40,    20,    39,    40,    39,    40,    12,    39,
      40,    19,    39,    40,    19,   808,    12,    20,    13,    20,
      12,    20,    39,    40,  1015,  1016,    20,   202,  1019,   204,
      12,    39,    40,    12,    39,    40,    39,    40,    39,    40,
      39,    40,   202,    12,   204,    39,    40,   222,  1039,    21,
      13,   202,    12,   204,  1045,   202,  1239,   204,  1241,    21,
      12,    12,   222,  1054,   202,  1056,   204,    39,    40,  1060,
    1061,   222,  1063,    19,    12,   222,    21,    39,    40,  1070,
      12,  1072,    13,  1074,   222,    21,  1077,  1078,    21,  1272,
      12,    21,  1083,    21,    39,    40,    13,    12,    21,  1090,
    1091,  1092,    12,    39,    40,    21,    39,    40,   974,    39,
      40,    39,    40,  1104,    21,  1106,    39,    40,  1109,    21,
    1037,  1038,    21,    39,    40,  1116,    12,    12,    21,    12,
      21,    12,    39,    40,    12,    12,    12,    39,    40,    12,
      39,    40,    24,    25,    26,  1136,    39,    40,    39,    40,
      12,    12,    32,    33,   974,    35,    36,    39,    40,    12,
      12,    41,    42,  1154,    12,    12,  1157,    12,  1159,    12,
      12,  1037,  1038,  1164,    12,  1166,    12,    16,    13,    19,
      13,   974,  1048,  1049,    12,  1176,  1177,  1178,  1179,    19,
     219,  1057,    15,    27,    31,    27,    27,  1188,   210,   211,
     212,   213,   214,   215,   216,   217,    10,   194,  1199,    28,
      13,    19,    13,  1204,   211,   212,   213,   214,   215,   216,
    1137,   632,   633,   634,   635,   636,   637,  1047,  1048,  1049,
      15,  1222,    13,  1224,    13,    13,    13,  1057,    13,    13,
      13,    13,   218,    42,  1037,  1038,  1112,    13,    19,  1240,
      13,  1242,    13,  1119,    13,  1048,  1049,    27,  1249,    13,
      13,   384,   788,  1220,  1057,   899,    93,    -1,  1212,    -1,
      -1,  1137,   211,   212,   213,   214,   215,   216,   217,    -1,
      -1,    -1,    -1,    -1,  1150,    -1,    -1,    -1,  1108,    -1,
      -1,  1111,  1112,    -1,  1211,    -1,    -1,    -1,  1118,  1119,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1112,
      -1,    -1,    -1,    -1,    -1,  1191,  1119,    -1,    -1,    -1,
    1150,  1151,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1161,    -1,    -1,  1137,  1211,    -1,    -1,  1168,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1150,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1190,  1191,    -1,  1193,  1194,    -1,  1196,    -1,    -1,    -1,
      -1,  1201,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1191,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1226,    -1,    -1,  1229,
      -1,  1231,  1232,    -1,    -1,  1235,  1236,    -1,  1211,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1246,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1254,    -1,    -1,  1257,    -1,    -1,
      -1,    -1,    -1,    -1,  1264,  1265,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     1,  1275,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    -1,    14,    -1,    -1,
      -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    30,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,    -1,   122,   123,   124,   125,   126,
     127,   128,   129,   130,    -1,    -1,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,   171,   172,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,    -1,   195,    -1,
      -1,    -1,    -1,    -1,   201,    -1,   203,    -1,   205,   206,
     207,   208,   209,   210,    -1,   212,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   220,   221,    -1,   223,   224,   225,     3,
      -1,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      -1,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,   225,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,
      -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,
      41,    42,    -1,    -1,    -1,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
      -1,   122,   123,   124,   125,   126,   127,   128,   129,   130,
      -1,    -1,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
     171,   172,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,    -1,   195,    -1,    -1,    -1,    -1,    -1,
     201,   202,   203,   204,   205,   206,   207,   208,   209,   210,
      -1,   212,    -1,    -1,    -1,    -1,    -1,    -1,   219,   220,
     221,   222,   223,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,    -1,   122,   123,   124,   125,   126,   127,   128,   129,
     130,    -1,    -1,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   191,   192,    -1,    -1,   195,    -1,    -1,    -1,    -1,
      -1,   201,   202,   203,   204,   205,   206,   207,   208,   209,
     210,    -1,   212,    -1,    -1,    -1,    -1,    -1,    -1,   219,
     220,   221,   222,   223,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,
      -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,    -1,   122,   123,   124,   125,   126,   127,   128,
     129,   130,    -1,    -1,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     169,   170,   171,   172,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,   185,   186,   187,   188,
     189,   190,   191,   192,    -1,    -1,   195,    -1,    -1,    -1,
      -1,    -1,   201,   202,   203,   204,   205,   206,   207,   208,
     209,   210,    -1,   212,    -1,    -1,    -1,    -1,    -1,    -1,
     219,   220,   221,   222,   223,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,    -1,   122,   123,   124,   125,   126,   127,
     128,   129,   130,    -1,    -1,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,    -1,    -1,   195,    -1,    -1,
      -1,    -1,    -1,   201,   202,   203,   204,   205,   206,   207,
     208,   209,   210,    -1,   212,    -1,    -1,    -1,    -1,    -1,
      -1,   219,   220,   221,   222,   223,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    -1,    14,    -1,    -1,
      -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,    -1,   122,   123,   124,   125,   126,
     127,   128,   129,   130,    -1,    -1,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,   171,   172,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   191,   192,    -1,    -1,   195,    -1,
      -1,    -1,    -1,    -1,   201,   202,   203,   204,   205,   206,
     207,   208,   209,   210,    -1,   212,    -1,    -1,    -1,    -1,
      -1,    -1,   219,   220,   221,    -1,   223,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,    -1,   122,   123,   124,   125,
     126,   127,   128,   129,   130,    -1,    -1,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,    -1,    -1,   195,
      -1,    -1,    -1,    -1,    -1,   201,   202,   203,   204,   205,
     206,   207,   208,   209,   210,    -1,   212,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   220,   221,   222,   223,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    -1,    14,
      -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,
      -1,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,    -1,   122,   123,   124,
     125,   126,   127,   128,   129,   130,    -1,    -1,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,    -1,    -1,
     195,    -1,    -1,    -1,    -1,    -1,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,    -1,   212,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   220,   221,   222,   223,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,    -1,   122,   123,
     124,   125,   126,   127,   128,   129,   130,    -1,    -1,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,    -1,
      -1,   195,    -1,    -1,    -1,    -1,    -1,   201,   202,   203,
     204,   205,   206,   207,   208,   209,   210,    -1,   212,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   220,   221,   222,   223,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,
      -1,    -1,    -1,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,    -1,   122,
     123,   124,   125,   126,   127,   128,   129,   130,    -1,    -1,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   169,   170,   171,   172,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,   185,   186,   187,   188,   189,   190,   191,   192,
      -1,    -1,   195,    -1,    -1,    -1,    -1,    -1,   201,   202,
     203,   204,   205,   206,   207,   208,   209,   210,    -1,   212,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   220,   221,   222,
     223,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,    -1,
     122,   123,   124,   125,   126,   127,   128,   129,   130,    -1,
      -1,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,    -1,    -1,   195,    -1,    -1,    -1,    -1,    -1,   201,
     202,   203,   204,   205,   206,   207,   208,   209,   210,    -1,
     212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   220,   221,
      -1,   223,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,
      -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,
      41,    42,    -1,    -1,    -1,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
      -1,   122,   123,   124,   125,   126,   127,   128,   129,   130,
      -1,    -1,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
     171,   172,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,   185,   186,   187,   188,   189,   190,
     191,   192,    -1,    -1,   195,    -1,    -1,    -1,    -1,    -1,
     201,    -1,   203,    -1,   205,   206,   207,   208,   209,   210,
      -1,   212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   220,
     221,    -1,   223,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    31,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      40,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,    -1,   122,   123,   124,   125,   126,   127,    -1,    -1,
      -1,    -1,    -1,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   175,   176,   177,   178,   179,
     180,   181,   182,    -1,   184,    -1,   186,   187,   188,   189,
     190,   191,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   203,    -1,    -1,   206,   207,    -1,    -1,
     210,    -1,   212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     220,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,    -1,
     122,   123,   124,   125,   126,   127,    -1,    -1,    -1,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   175,   176,   177,   178,   179,   180,   181,
     182,    -1,   184,    -1,   186,   187,   188,   189,   190,   191,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   203,    -1,    -1,   206,   207,    -1,    -1,   210,    -1,
     212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   220,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,    -1,   122,   123,
     124,   125,   126,   127,    -1,    -1,    -1,    -1,    -1,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   175,   176,   177,   178,   179,   180,   181,   182,    -1,
     184,    -1,   186,   187,   188,   189,   190,   191,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   203,
      -1,    -1,   206,   207,    -1,    -1,   210,    -1,   212,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   220,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    31,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,    -1,   122,   123,   124,   125,
     126,   127,    -1,    -1,    -1,    -1,    -1,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   175,
     176,   177,   178,   179,   180,   181,   182,    -1,   184,    -1,
     186,   187,   188,   189,   190,   191,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   203,    -1,    -1,
     206,   207,    -1,    -1,   210,    -1,   212,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   220,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    30,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,    -1,   122,   123,   124,   125,   126,   127,
      -1,    -1,    -1,    -1,    -1,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   175,   176,   177,
     178,   179,   180,   181,   182,    -1,   184,    -1,   186,   187,
     188,   189,   190,   191,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   203,    -1,    -1,   206,   207,
      -1,    -1,   210,    -1,   212,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   220,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,    -1,   122,   123,   124,   125,   126,   127,    -1,    -1,
      -1,    -1,    -1,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   175,   176,   177,   178,   179,
     180,   181,   182,    -1,   184,    -1,   186,   187,   188,   189,
     190,   191,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   203,    -1,    -1,   206,   207,    -1,    -1,
     210,    -1,   212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     220,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    28,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,    -1,
     122,   123,   124,   125,   126,   127,    -1,    -1,    -1,    -1,
      -1,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   175,   176,   177,   178,   179,   180,   181,
     182,    -1,   184,    -1,   186,   187,   188,   189,   190,   191,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   203,    -1,    -1,   206,   207,    -1,    -1,   210,    -1,
     212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   220,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    27,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,    -1,   122,   123,
     124,   125,   126,   127,    -1,    -1,    -1,    -1,    -1,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   175,   176,   177,   178,   179,   180,   181,   182,    -1,
     184,    -1,   186,   187,   188,   189,   190,   191,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   203,
      -1,    -1,   206,   207,    -1,    -1,   210,    -1,   212,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   220,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,
      -1,    27,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,    -1,   122,   123,   124,   125,
     126,   127,    -1,    -1,    -1,    -1,    -1,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   175,
     176,   177,   178,   179,   180,   181,   182,    -1,   184,    -1,
     186,   187,   188,   189,   190,   191,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   203,    -1,    -1,
     206,   207,    -1,    -1,   210,    -1,   212,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   220,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    27,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,    -1,   122,   123,   124,   125,   126,   127,
      -1,    -1,    -1,    -1,    -1,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   175,   176,   177,
     178,   179,   180,   181,   182,    -1,   184,    -1,   186,   187,
     188,   189,   190,   191,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   203,    -1,    -1,   206,   207,
      -1,    -1,   210,    -1,   212,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   220,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,    -1,   122,   123,   124,   125,   126,   127,    -1,    -1,
      -1,    -1,    -1,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   175,   176,   177,   178,   179,
     180,   181,   182,    -1,   184,    -1,   186,   187,   188,   189,
     190,   191,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   203,    -1,    -1,   206,   207,    -1,    -1,
     210,    -1,   212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     220,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    16,    -1,    -1,    -1,    -1,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,    -1,    -1,
     122,   123,   124,   125,   126,   127,    -1,    -1,    -1,    -1,
      -1,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   175,   176,   177,   178,   179,   180,   181,
     182,    -1,   184,    -1,   186,   187,   188,   189,   190,   191,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   203,    -1,    -1,   206,   207,    -1,    -1,   210,    -1,
     212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   220,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    16,    -1,    -1,    -1,    -1,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,    -1,   122,   123,
     124,   125,   126,   127,    -1,    -1,    -1,    -1,    -1,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   175,   176,   177,   178,   179,   180,   181,   182,    -1,
     184,    -1,   186,   187,   188,   189,   190,   191,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   203,
      -1,    -1,   206,   207,    -1,    -1,   210,    -1,   212,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   220,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,    -1,    -1,   122,   123,   124,   125,
     126,   127,    -1,    -1,    -1,    -1,    -1,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   175,
     176,   177,   178,   179,   180,   181,   182,    -1,   184,    -1,
     186,   187,   188,   189,   190,   191,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   203,    -1,    -1,
     206,   207,    -1,    -1,   210,    -1,   212,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   220,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,    -1,    -1,   122,   123,   124,   125,   126,   127,
      -1,    -1,    -1,    -1,    -1,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   175,   176,   177,
     178,   179,   180,   181,   182,    -1,   184,    -1,   186,   187,
     188,   189,   190,   191,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   203,    -1,    -1,   206,   207,
      -1,    -1,   210,    -1,   212,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   220,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
      -1,    -1,   122,   123,   124,   125,   126,   127,    -1,    -1,
      -1,    -1,    -1,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   175,   176,   177,   178,   179,
     180,   181,   182,    -1,   184,    -1,   186,   187,   188,   189,
     190,   191,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   203,    -1,    -1,   206,   207,    -1,    -1,
     210,    -1,   212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     220,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,    -1,    -1,
     122,   123,   124,   125,   126,   127,    -1,    -1,    -1,    -1,
      -1,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   175,   176,   177,   178,   179,   180,   181,
     182,    -1,   184,    -1,   186,   187,   188,   189,   190,   191,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   203,    -1,    -1,   206,   207,    -1,    -1,   210,    -1,
     212,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   220,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,    -1,   122,   123,
     124,   125,   126,   127,    -1,    -1,    -1,    -1,    -1,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   175,   176,   177,   178,   179,   180,   181,   182,    -1,
     184,    -1,   186,   187,   188,   189,   190,   191,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   203,
      -1,    -1,   206,   207,    -1,    -1,   210,    -1,   212,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   220
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,     1,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    14,    20,    22,    30,    38,    41,    42,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   191,   192,   195,   201,   203,   205,   206,   207,   208,
     209,   210,   212,   220,   221,   223,   224,   225,   227,   228,
     229,   231,   239,   240,   241,   242,   243,   244,   245,   249,
     250,   251,   252,   253,   254,   255,   256,   257,   260,   261,
     263,   264,   265,   267,    21,    12,    16,    17,    10,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    96,    97,
      98,    99,   100,   101,   203,   249,   251,   257,    31,    40,
     249,   252,   249,   256,   254,   254,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    16,   266,    16,   266,
      16,   266,    16,   266,    16,   266,    12,    16,   266,    16,
     266,    16,   266,    16,   266,    12,    16,   266,    16,   266,
      16,   266,    16,   266,    16,   266,    16,   266,   249,   250,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,   232,
     249,    10,   233,    29,   246,   248,   249,    12,    12,    12,
      12,    12,    12,   238,    10,    12,    21,     0,     3,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,   171,   172,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
     197,   198,   199,   200,   202,   203,   204,   205,   206,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   229,   230,
     272,   219,   230,   231,   234,   235,   236,    21,    20,    16,
      17,    20,    19,    39,    40,    16,    17,    18,    23,    27,
      37,   196,    32,    33,    35,    36,    41,    42,   256,    43,
      44,    14,    29,    45,    13,   245,   131,   132,   249,   249,
      16,    13,    29,    31,   249,   262,    15,    15,    19,    21,
      24,    25,    26,   249,   249,   249,   249,   249,   249,   249,
     249,   249,   249,   249,   249,   249,   249,   249,   249,   249,
     249,   249,   249,   249,   249,   249,   249,   249,   249,   249,
     249,   249,   249,   249,   249,   249,   249,   249,   249,   249,
     249,   249,   249,    30,   249,   249,   249,   249,   249,   249,
     249,   249,   249,   249,   231,   249,   249,   249,   249,   249,
     249,   121,   249,   249,   249,   249,   249,   249,   249,   249,
     249,   249,   249,   249,   249,   249,   249,   249,   249,   249,
     249,   249,   249,   249,   249,   249,   249,   249,   245,   245,
     249,   249,   245,   249,   249,   249,   249,   249,   249,   249,
     249,   245,   249,    10,   249,   249,   249,   249,   249,   249,
     249,   249,   249,   249,   245,   249,   249,   249,   245,   249,
     249,   193,   196,   197,    10,   204,    19,    21,   247,   200,
     249,    10,   249,    10,   249,    10,    13,   237,   238,    13,
     249,    31,    16,    16,    15,    21,    10,   237,    21,   230,
     230,   234,    21,   249,   249,   245,   252,   252,   249,   249,
     253,    16,   253,    16,   253,   253,   253,   255,   255,   255,
     255,   255,   255,    38,   254,   256,    38,   254,   256,   249,
      10,    38,   254,   256,    13,    12,    12,    12,    10,    15,
      19,    28,    31,   249,   249,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,   249,   258,   259,
      13,    13,    19,    13,    19,    19,    13,    13,    13,    19,
      19,    19,    13,    13,    13,    19,    19,    19,    19,    19,
      19,    19,    19,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    19,    13,    13,    13,    13,    13,    19,
      13,    19,    19,    19,    19,    19,    19,    19,    19,    19,
      19,    19,    19,    13,    19,    19,    19,    13,    19,    19,
     231,   249,   249,    16,   246,   231,    13,    13,    13,    19,
      13,    19,    16,    19,   229,    13,    13,   234,   230,   235,
     253,   253,   256,   256,   256,   256,    15,    12,   256,   256,
     249,   249,   245,    28,   249,    31,    15,    15,    15,    34,
     258,   249,   249,   249,   249,   249,   249,   249,   249,   249,
     249,   249,   249,   249,   249,    27,    27,   245,   249,    27,
     249,    10,   245,   249,   249,   249,   249,   249,   249,   249,
     249,   249,   249,   249,   249,   249,   249,   194,   200,   198,
     249,   249,   249,    28,   237,   222,   230,   234,   235,   229,
     245,    13,    13,    13,    19,    15,    12,   229,    19,    13,
      19,    19,    19,    19,    13,    13,    13,    13,    13,    19,
      19,    13,    27,   249,    27,   249,    13,    19,    27,   249,
      13,    13,    13,    19,    13,    13,    13,    13,    13,    13,
      19,    19,    19,    13,    13,    19,    13,   231,   231,   249,
      19,    13,    13,   249,   222,   230,   222,   230,   234,   222,
     230,   234,   235,    13,   249,   249,   222,   230,   234,   235,
     245,   249,   249,   245,   249,   245,   249,   249,   249,   249,
     249,   249,   249,   249,   249,   245,   199,   200,    12,   210,
     211,   212,   213,   214,   215,   216,   217,   268,   269,   271,
     229,    21,   249,   249,   222,   230,   249,   222,   230,   222,
     230,   234,    13,   249,   222,   230,   222,   230,   234,    13,
      19,    13,    13,    13,    13,    13,    19,    19,    19,    19,
      13,    13,   249,   231,   268,   270,   218,    42,   222,   230,
     234,   235,   230,    21,    21,   249,    21,   249,   249,   222,
     230,    21,   249,   249,   222,   230,   245,   249,   249,   249,
     249,   200,    19,    13,   211,   212,   213,   214,   215,   216,
      27,   249,   222,   230,   222,   230,   234,   230,   230,    21,
     230,    21,    21,   249,   230,    21,    21,   249,    13,    19,
      19,    19,    19,   231,   270,   269,    21,   249,   249,   222,
     230,   230,   230,   230,    21,   230,   230,    21,   245,   249,
     245,   249,    13,   230,    21,    21,   249,   230,   230,    13,
      13,    13,    19,   230,   230,    21,   245,   230,    13
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YYLEX_PARAM, YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))

/* Error token number */
#define YYTERROR	1
#define YYERRCODE	256


/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */
#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval, YYLEX_PARAM)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value, YYLEX_PARAM); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, void * YYLEX_PARAM)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep, YYLEX_PARAM)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    void * YYLEX_PARAM;
#endif
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
  YYUSE (YYLEX_PARAM);
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, void * YYLEX_PARAM)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep, YYLEX_PARAM)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    void * YYLEX_PARAM;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep, YYLEX_PARAM);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule, void * YYLEX_PARAM)
#else
static void
yy_reduce_print (yyvsp, yyrule, YYLEX_PARAM)
    YYSTYPE *yyvsp;
    int yyrule;
    void * YYLEX_PARAM;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       , YYLEX_PARAM);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule, YYLEX_PARAM); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULL, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULL;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULL, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, void * YYLEX_PARAM)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, YYLEX_PARAM)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    void * YYLEX_PARAM;
#endif
{
  YYUSE (yyvaluep);
  YYUSE (YYLEX_PARAM);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YYUSE (yytype);
}




/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void * YYLEX_PARAM)
#else
int
yyparse (YYLEX_PARAM)
    void * YYLEX_PARAM;
#endif
#endif
{
/* The lookahead symbol.  */
int yychar;


#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
static YYSTYPE yyval_default;
# define YY_INITIAL_VALUE(Value) = Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval YY_INITIAL_VALUE(yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
/* Line 1787 of yacc.c  */
#line 428 "parser.y"
    {
			    parsedThing = (yyvsp[(1) - (2)].tree);
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
    break;

  case 3:
/* Line 1787 of yacc.c  */
#line 434 "parser.y"
    {
			    outputMode();
                            sollyaPrintf("This is %s.\nType 'help help;' for the list of available commands. Type 'help <command>;' for help on the specific command <command>.\nType 'quit;' for quitting the %s interpreter.\n\nYou can get moral support and help with bugs by writing to %s.\n\n",PACKAGE_NAME,PACKAGE_NAME,PACKAGE_BUGREPORT);
			    parsedThing = NULL;
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
    break;

  case 4:
/* Line 1787 of yacc.c  */
#line 442 "parser.y"
    {
			    outputMode();
                            sollyaPrintf("This is %s.\nType 'help help;' for the list of available commands. Type 'help <command>;' for help on the specific command <command>.\nType 'quit;' for quitting the %s interpreter.\n\nYou can get moral support and help with bugs by writing to %s.\n\n",PACKAGE_NAME,PACKAGE_NAME,PACKAGE_BUGREPORT);
			    parsedThing = NULL;
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
    break;

  case 5:
/* Line 1787 of yacc.c  */
#line 450 "parser.y"
    {
			    parsedThing = NULL;
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
    break;

  case 6:
/* Line 1787 of yacc.c  */
#line 456 "parser.y"
    {
			    outputMode();
			    sollyaPrintf("This is\n\n\t%s.\n\nCopyright 2006-2011 by\n\n    Laboratoire de l'Informatique du Parallelisme,\n    UMR CNRS - ENS Lyon - UCB Lyon 1 - INRIA 5668, Lyon, France,\n\n    LORIA (CNRS, INPL, INRIA, UHP, U-Nancy 2), Nancy, France,\n\n    Laboratoire d'Informatique de Paris 6, equipe PEQUAN,\n    UPMC Universite Paris 06 - CNRS - UMR 7606 - LIP6, Paris, France,\n\nand by\n\n    INRIA Sophia-Antipolis Mediterranee, APICS Team,\n    Sophia-Antipolis, France.\n\nAll rights reserved.\n\nContributors are S. Chevillard, N. Jourdan, M. Joldes and Ch. Lauter.\n\nThis software is governed by the CeCILL-C license under French law and\nabiding by the rules of distribution of free software.  You can  use,\nmodify and/ or redistribute the software under the terms of the CeCILL-C\nlicense as circulated by CEA, CNRS and INRIA at the following URL\n\"http://www.cecill.info\".\n\nPlease send bug reports to %s.\n\nThis program is distributed WITHOUT ANY WARRANTY; without even the\nimplied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\nThis build of %s is based on GMP %s, MPFR %s and MPFI %s.\n",PACKAGE_STRING,PACKAGE_BUGREPORT,PACKAGE_STRING,gmp_version,mpfr_get_version(),sollya_mpfi_get_version());
#if defined(HAVE_FPLLL_VERSION_STRING)
			    sollyaPrintf("%s uses FPLLL as: \"%s\"\n",PACKAGE_STRING,HAVE_FPLLL_VERSION_STRING);
#endif
			    sollyaPrintf("\n");
			    parsedThing = NULL;
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
    break;

  case 7:
/* Line 1787 of yacc.c  */
#line 468 "parser.y"
    {
			    parsedThing = NULL;
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
    break;

  case 8:
/* Line 1787 of yacc.c  */
#line 476 "parser.y"
    {
			    helpNotFinished = 1;
			    (yyval.other) = NULL;
			  }
    break;

  case 9:
/* Line 1787 of yacc.c  */
#line 483 "parser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 10:
/* Line 1787 of yacc.c  */
#line 487 "parser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 11:
/* Line 1787 of yacc.c  */
#line 493 "parser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 12:
/* Line 1787 of yacc.c  */
#line 497 "parser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 13:
/* Line 1787 of yacc.c  */
#line 503 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 14:
/* Line 1787 of yacc.c  */
#line 507 "parser.y"
    {
			    (yyval.tree) = makeCommandList((yyvsp[(2) - (3)].list));
                          }
    break;

  case 15:
/* Line 1787 of yacc.c  */
#line 511 "parser.y"
    {
			    (yyval.tree) = makeCommandList(concatChains((yyvsp[(2) - (4)].list), (yyvsp[(3) - (4)].list)));
                          }
    break;

  case 16:
/* Line 1787 of yacc.c  */
#line 515 "parser.y"
    {
			    (yyval.tree) = makeCommandList((yyvsp[(2) - (3)].list));
                          }
    break;

  case 17:
/* Line 1787 of yacc.c  */
#line 519 "parser.y"
    {
			    (yyval.tree) = makeNop();
                          }
    break;

  case 18:
/* Line 1787 of yacc.c  */
#line 523 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(2) - (2)].tree);
			  }
    break;

  case 19:
/* Line 1787 of yacc.c  */
#line 527 "parser.y"
    {
			    (yyval.tree) = makeWhile((yyvsp[(2) - (4)].tree), (yyvsp[(4) - (4)].tree));
			  }
    break;

  case 20:
/* Line 1787 of yacc.c  */
#line 531 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(2) - (2)].tree);
			  }
    break;

  case 21:
/* Line 1787 of yacc.c  */
#line 537 "parser.y"
    {
			    (yyval.tree) = makeIf((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
                          }
    break;

  case 22:
/* Line 1787 of yacc.c  */
#line 541 "parser.y"
    {
			    (yyval.tree) = makeIfElse((yyvsp[(1) - (5)].tree),(yyvsp[(3) - (5)].tree),(yyvsp[(5) - (5)].tree));
                          }
    break;

  case 23:
/* Line 1787 of yacc.c  */
#line 549 "parser.y"
    {
			    (yyval.tree) = makeFor((yyvsp[(1) - (7)].value), (yyvsp[(3) - (7)].tree), (yyvsp[(5) - (7)].tree), makeConstantDouble(1.0), (yyvsp[(7) - (7)].tree));
			    free((yyvsp[(1) - (7)].value));
                          }
    break;

  case 24:
/* Line 1787 of yacc.c  */
#line 554 "parser.y"
    {
			    (yyval.tree) = makeFor((yyvsp[(1) - (9)].value), (yyvsp[(3) - (9)].tree), (yyvsp[(5) - (9)].tree), (yyvsp[(7) - (9)].tree), (yyvsp[(9) - (9)].tree));
			    free((yyvsp[(1) - (9)].value));
                          }
    break;

  case 25:
/* Line 1787 of yacc.c  */
#line 559 "parser.y"
    {
			    (yyval.tree) = makeForIn((yyvsp[(1) - (5)].value), (yyvsp[(3) - (5)].tree), (yyvsp[(5) - (5)].tree));
			    free((yyvsp[(1) - (5)].value));
                          }
    break;

  case 26:
/* Line 1787 of yacc.c  */
#line 567 "parser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (2)].tree));
			  }
    break;

  case 27:
/* Line 1787 of yacc.c  */
#line 571 "parser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].tree));
			  }
    break;

  case 28:
/* Line 1787 of yacc.c  */
#line 577 "parser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (2)].tree));
			  }
    break;

  case 29:
/* Line 1787 of yacc.c  */
#line 581 "parser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].tree));
			  }
    break;

  case 30:
/* Line 1787 of yacc.c  */
#line 587 "parser.y"
    {
			    (yyval.tree) = makeVariableDeclaration((yyvsp[(2) - (2)].list));
			  }
    break;

  case 31:
/* Line 1787 of yacc.c  */
#line 594 "parser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].value));
			  }
    break;

  case 32:
/* Line 1787 of yacc.c  */
#line 598 "parser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].value));
			  }
    break;

  case 33:
/* Line 1787 of yacc.c  */
#line 604 "parser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[(4) - (5)].list)), makeUnit());
                          }
    break;

  case 34:
/* Line 1787 of yacc.c  */
#line 608 "parser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(concatChains((yyvsp[(4) - (6)].list), (yyvsp[(5) - (6)].list))), makeUnit());
                          }
    break;

  case 35:
/* Line 1787 of yacc.c  */
#line 612 "parser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[(4) - (5)].list)), makeUnit());
                          }
    break;

  case 36:
/* Line 1787 of yacc.c  */
#line 616 "parser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
    break;

  case 37:
/* Line 1787 of yacc.c  */
#line 620 "parser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[(4) - (8)].list)), (yyvsp[(6) - (8)].tree));
                          }
    break;

  case 38:
/* Line 1787 of yacc.c  */
#line 624 "parser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(concatChains((yyvsp[(4) - (9)].list), (yyvsp[(5) - (9)].list))), (yyvsp[(7) - (9)].tree));
                          }
    break;

  case 39:
/* Line 1787 of yacc.c  */
#line 628 "parser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[(4) - (8)].list)), (yyvsp[(6) - (8)].tree));
                          }
    break;

  case 40:
/* Line 1787 of yacc.c  */
#line 632 "parser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(addElement(NULL,makeNop())), (yyvsp[(5) - (7)].tree));
                          }
    break;

  case 41:
/* Line 1787 of yacc.c  */
#line 636 "parser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (6)].list), makeCommandList((yyvsp[(5) - (6)].list)), makeUnit());
                          }
    break;

  case 42:
/* Line 1787 of yacc.c  */
#line 640 "parser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (7)].list), makeCommandList(concatChains((yyvsp[(5) - (7)].list), (yyvsp[(6) - (7)].list))), makeUnit());
                          }
    break;

  case 43:
/* Line 1787 of yacc.c  */
#line 644 "parser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (6)].list), makeCommandList((yyvsp[(5) - (6)].list)), makeUnit());
                          }
    break;

  case 44:
/* Line 1787 of yacc.c  */
#line 648 "parser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (5)].list), makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
    break;

  case 45:
/* Line 1787 of yacc.c  */
#line 652 "parser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (9)].list), makeCommandList((yyvsp[(5) - (9)].list)), (yyvsp[(7) - (9)].tree));
                          }
    break;

  case 46:
/* Line 1787 of yacc.c  */
#line 656 "parser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (10)].list), makeCommandList(concatChains((yyvsp[(5) - (10)].list), (yyvsp[(6) - (10)].list))), (yyvsp[(8) - (10)].tree));
                          }
    break;

  case 47:
/* Line 1787 of yacc.c  */
#line 660 "parser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (9)].list), makeCommandList((yyvsp[(5) - (9)].list)), (yyvsp[(7) - (9)].tree));
                          }
    break;

  case 48:
/* Line 1787 of yacc.c  */
#line 664 "parser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (8)].list), makeCommandList(addElement(NULL, makeNop())), (yyvsp[(6) - (8)].tree));
                          }
    break;

  case 49:
/* Line 1787 of yacc.c  */
#line 668 "parser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (8)].value), makeCommandList((yyvsp[(7) - (8)].list)), makeUnit());
                          }
    break;

  case 50:
/* Line 1787 of yacc.c  */
#line 672 "parser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (9)].value), makeCommandList(concatChains((yyvsp[(7) - (9)].list), (yyvsp[(8) - (9)].list))), makeUnit());
                          }
    break;

  case 51:
/* Line 1787 of yacc.c  */
#line 676 "parser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (8)].value), makeCommandList((yyvsp[(7) - (8)].list)), makeUnit());
                          }
    break;

  case 52:
/* Line 1787 of yacc.c  */
#line 680 "parser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (7)].value), makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
    break;

  case 53:
/* Line 1787 of yacc.c  */
#line 684 "parser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (11)].value), makeCommandList((yyvsp[(7) - (11)].list)), (yyvsp[(9) - (11)].tree));
                          }
    break;

  case 54:
/* Line 1787 of yacc.c  */
#line 688 "parser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (12)].value), makeCommandList(concatChains((yyvsp[(7) - (12)].list), (yyvsp[(8) - (12)].list))), (yyvsp[(10) - (12)].tree));
                          }
    break;

  case 55:
/* Line 1787 of yacc.c  */
#line 692 "parser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (11)].value), makeCommandList((yyvsp[(7) - (11)].list)), (yyvsp[(9) - (11)].tree));
                          }
    break;

  case 56:
/* Line 1787 of yacc.c  */
#line 696 "parser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (10)].value), makeCommandList(addElement(NULL, makeNop())), (yyvsp[(8) - (10)].tree));
                          }
    break;

  case 57:
/* Line 1787 of yacc.c  */
#line 703 "parser.y"
    {
			    (yyval.tree) = makeQuit();
			  }
    break;

  case 58:
/* Line 1787 of yacc.c  */
#line 707 "parser.y"
    {
			    (yyval.tree) = makeFalseQuit();
			  }
    break;

  case 59:
/* Line 1787 of yacc.c  */
#line 711 "parser.y"
    {
			    (yyval.tree) = makeNop();
			  }
    break;

  case 60:
/* Line 1787 of yacc.c  */
#line 715 "parser.y"
    {
			    (yyval.tree) = makeNopArg((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 61:
/* Line 1787 of yacc.c  */
#line 719 "parser.y"
    {
			    (yyval.tree) = makeNopArg(makeDefault());
			  }
    break;

  case 62:
/* Line 1787 of yacc.c  */
#line 723 "parser.y"
    {
			    (yyval.tree) = makeRestart();
			  }
    break;

  case 63:
/* Line 1787 of yacc.c  */
#line 727 "parser.y"
    {
			    (yyval.tree) = makePrint((yyvsp[(3) - (4)].list));
			  }
    break;

  case 64:
/* Line 1787 of yacc.c  */
#line 731 "parser.y"
    {
			    (yyval.tree) = makeNewFilePrint((yyvsp[(6) - (6)].tree), (yyvsp[(3) - (6)].list));
			  }
    break;

  case 65:
/* Line 1787 of yacc.c  */
#line 735 "parser.y"
    {
			    (yyval.tree) = makeAppendFilePrint((yyvsp[(7) - (7)].tree), (yyvsp[(3) - (7)].list));
			  }
    break;

  case 66:
/* Line 1787 of yacc.c  */
#line 739 "parser.y"
    {
			    (yyval.tree) = makePlot(addElement((yyvsp[(5) - (6)].list), (yyvsp[(3) - (6)].tree)));
			  }
    break;

  case 67:
/* Line 1787 of yacc.c  */
#line 743 "parser.y"
    {
			    (yyval.tree) = makePrintHexa((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 68:
/* Line 1787 of yacc.c  */
#line 747 "parser.y"
    {
			    (yyval.tree) = makePrintFloat((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 69:
/* Line 1787 of yacc.c  */
#line 751 "parser.y"
    {
			    (yyval.tree) = makePrintBinary((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 70:
/* Line 1787 of yacc.c  */
#line 755 "parser.y"
    {
			    (yyval.tree) = makePrintExpansion((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 71:
/* Line 1787 of yacc.c  */
#line 759 "parser.y"
    {
			    (yyval.tree) = makeImplementConst((yyvsp[(3) - (4)].list));
			  }
    break;

  case 72:
/* Line 1787 of yacc.c  */
#line 763 "parser.y"
    {
			    (yyval.tree) = makeBashExecute((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 73:
/* Line 1787 of yacc.c  */
#line 767 "parser.y"
    {
			    (yyval.tree) = makeExternalPlot(addElement(addElement(addElement(addElement((yyvsp[(11) - (12)].list),(yyvsp[(9) - (12)].tree)),(yyvsp[(7) - (12)].tree)),(yyvsp[(5) - (12)].tree)),(yyvsp[(3) - (12)].tree)));
			  }
    break;

  case 74:
/* Line 1787 of yacc.c  */
#line 771 "parser.y"
    {
			    (yyval.tree) = makeWrite((yyvsp[(3) - (4)].list));
			  }
    break;

  case 75:
/* Line 1787 of yacc.c  */
#line 775 "parser.y"
    {
			    (yyval.tree) = makeNewFileWrite((yyvsp[(6) - (6)].tree), (yyvsp[(3) - (6)].list));
			  }
    break;

  case 76:
/* Line 1787 of yacc.c  */
#line 779 "parser.y"
    {
			    (yyval.tree) = makeAppendFileWrite((yyvsp[(7) - (7)].tree), (yyvsp[(3) - (7)].list));
			  }
    break;

  case 77:
/* Line 1787 of yacc.c  */
#line 783 "parser.y"
    {
			    (yyval.tree) = makeAsciiPlot((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 78:
/* Line 1787 of yacc.c  */
#line 787 "parser.y"
    {
			    (yyval.tree) = makePrintXml((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 79:
/* Line 1787 of yacc.c  */
#line 791 "parser.y"
    {
			    (yyval.tree) = makeExecute((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 80:
/* Line 1787 of yacc.c  */
#line 795 "parser.y"
    {
			    (yyval.tree) = makePrintXmlNewFile((yyvsp[(3) - (6)].tree),(yyvsp[(6) - (6)].tree));
			  }
    break;

  case 81:
/* Line 1787 of yacc.c  */
#line 799 "parser.y"
    {
			    (yyval.tree) = makePrintXmlAppendFile((yyvsp[(3) - (7)].tree),(yyvsp[(7) - (7)].tree));
			  }
    break;

  case 82:
/* Line 1787 of yacc.c  */
#line 803 "parser.y"
    {
			    (yyval.tree) = makeWorstCase(addElement(addElement(addElement(addElement((yyvsp[(11) - (12)].list), (yyvsp[(9) - (12)].tree)), (yyvsp[(7) - (12)].tree)), (yyvsp[(5) - (12)].tree)), (yyvsp[(3) - (12)].tree)));
			  }
    break;

  case 83:
/* Line 1787 of yacc.c  */
#line 807 "parser.y"
    {
			    (yyval.tree) = makeRename((yyvsp[(3) - (6)].value), (yyvsp[(5) - (6)].value));
			    free((yyvsp[(3) - (6)].value));
			    free((yyvsp[(5) - (6)].value));
			  }
    break;

  case 84:
/* Line 1787 of yacc.c  */
#line 813 "parser.y"
    {
			    (yyval.tree) = makeExternalProc((yyvsp[(3) - (11)].value), (yyvsp[(5) - (11)].tree), addElement((yyvsp[(7) - (11)].list), (yyvsp[(10) - (11)].integerval)));
			    free((yyvsp[(3) - (11)].value));
			  }
    break;

  case 85:
/* Line 1787 of yacc.c  */
#line 818 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 86:
/* Line 1787 of yacc.c  */
#line 822 "parser.y"
    {
			    (yyval.tree) = makeAutoprint((yyvsp[(1) - (1)].list));
			  }
    break;

  case 87:
/* Line 1787 of yacc.c  */
#line 826 "parser.y"
    {
			    (yyval.tree) = makeAssignment((yyvsp[(2) - (3)].value), (yyvsp[(3) - (3)].tree));
			    free((yyvsp[(2) - (3)].value));
			  }
    break;

  case 88:
/* Line 1787 of yacc.c  */
#line 833 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 89:
/* Line 1787 of yacc.c  */
#line 837 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (2)].tree);
			  }
    break;

  case 90:
/* Line 1787 of yacc.c  */
#line 841 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 91:
/* Line 1787 of yacc.c  */
#line 845 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (2)].tree);
			  }
    break;

  case 92:
/* Line 1787 of yacc.c  */
#line 851 "parser.y"
    {
			    (yyval.tree) = makeAssignment((yyvsp[(1) - (3)].value), (yyvsp[(3) - (3)].tree));
			    free((yyvsp[(1) - (3)].value));
			  }
    break;

  case 93:
/* Line 1787 of yacc.c  */
#line 856 "parser.y"
    {
			    (yyval.tree) = makeFloatAssignment((yyvsp[(1) - (3)].value), (yyvsp[(3) - (3)].tree));
			    free((yyvsp[(1) - (3)].value));
			  }
    break;

  case 94:
/* Line 1787 of yacc.c  */
#line 861 "parser.y"
    {
			    (yyval.tree) = makeLibraryBinding((yyvsp[(1) - (6)].value), (yyvsp[(5) - (6)].tree));
			    free((yyvsp[(1) - (6)].value));
			  }
    break;

  case 95:
/* Line 1787 of yacc.c  */
#line 866 "parser.y"
    {
			    (yyval.tree) = makeLibraryConstantBinding((yyvsp[(1) - (6)].value), (yyvsp[(5) - (6)].tree));
			    free((yyvsp[(1) - (6)].value));
			  }
    break;

  case 96:
/* Line 1787 of yacc.c  */
#line 871 "parser.y"
    {
			    (yyval.tree) = makeAssignmentInIndexing((yyvsp[(1) - (3)].dblnode)->a,(yyvsp[(1) - (3)].dblnode)->b,(yyvsp[(3) - (3)].tree));
			    free((yyvsp[(1) - (3)].dblnode));
			  }
    break;

  case 97:
/* Line 1787 of yacc.c  */
#line 876 "parser.y"
    {
			    (yyval.tree) = makeFloatAssignmentInIndexing((yyvsp[(1) - (3)].dblnode)->a,(yyvsp[(1) - (3)].dblnode)->b,(yyvsp[(3) - (3)].tree));
			    free((yyvsp[(1) - (3)].dblnode));
			  }
    break;

  case 98:
/* Line 1787 of yacc.c  */
#line 881 "parser.y"
    {
			    (yyval.tree) = makeProtoAssignmentInStructure((yyvsp[(1) - (3)].tree),(yyvsp[(3) - (3)].tree));
			  }
    break;

  case 99:
/* Line 1787 of yacc.c  */
#line 885 "parser.y"
    {
			    (yyval.tree) = makeProtoFloatAssignmentInStructure((yyvsp[(1) - (3)].tree),(yyvsp[(3) - (3)].tree));
			  }
    break;

  case 100:
/* Line 1787 of yacc.c  */
#line 891 "parser.y"
    {
			    (yyval.tree) = makeStructAccess((yyvsp[(1) - (3)].tree),(yyvsp[(3) - (3)].value));
			    free((yyvsp[(3) - (3)].value));
			  }
    break;

  case 101:
/* Line 1787 of yacc.c  */
#line 898 "parser.y"
    {
			    (yyval.tree) = makePrecAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 102:
/* Line 1787 of yacc.c  */
#line 902 "parser.y"
    {
			    (yyval.tree) = makePointsAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 103:
/* Line 1787 of yacc.c  */
#line 906 "parser.y"
    {
			    (yyval.tree) = makeDiamAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 104:
/* Line 1787 of yacc.c  */
#line 910 "parser.y"
    {
			    (yyval.tree) = makeDisplayAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 105:
/* Line 1787 of yacc.c  */
#line 914 "parser.y"
    {
			    (yyval.tree) = makeVerbosityAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 106:
/* Line 1787 of yacc.c  */
#line 918 "parser.y"
    {
			    (yyval.tree) = makeCanonicalAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 107:
/* Line 1787 of yacc.c  */
#line 922 "parser.y"
    {
			    (yyval.tree) = makeAutoSimplifyAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 108:
/* Line 1787 of yacc.c  */
#line 926 "parser.y"
    {
			    (yyval.tree) = makeTaylorRecursAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 109:
/* Line 1787 of yacc.c  */
#line 930 "parser.y"
    {
			    (yyval.tree) = makeTimingAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 110:
/* Line 1787 of yacc.c  */
#line 934 "parser.y"
    {
			    (yyval.tree) = makeFullParenAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 111:
/* Line 1787 of yacc.c  */
#line 938 "parser.y"
    {
			    (yyval.tree) = makeMidpointAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 112:
/* Line 1787 of yacc.c  */
#line 942 "parser.y"
    {
			    (yyval.tree) = makeDieOnErrorAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 113:
/* Line 1787 of yacc.c  */
#line 946 "parser.y"
    {
			    (yyval.tree) = makeRationalModeAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 114:
/* Line 1787 of yacc.c  */
#line 950 "parser.y"
    {
			    (yyval.tree) = makeSuppressWarningsAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 115:
/* Line 1787 of yacc.c  */
#line 954 "parser.y"
    {
			    (yyval.tree) = makeHopitalRecursAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 116:
/* Line 1787 of yacc.c  */
#line 960 "parser.y"
    {
			    (yyval.tree) = makePrecStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 117:
/* Line 1787 of yacc.c  */
#line 964 "parser.y"
    {
			    (yyval.tree) = makePointsStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 118:
/* Line 1787 of yacc.c  */
#line 968 "parser.y"
    {
			    (yyval.tree) = makeDiamStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 119:
/* Line 1787 of yacc.c  */
#line 972 "parser.y"
    {
			    (yyval.tree) = makeDisplayStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 120:
/* Line 1787 of yacc.c  */
#line 976 "parser.y"
    {
			    (yyval.tree) = makeVerbosityStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 121:
/* Line 1787 of yacc.c  */
#line 980 "parser.y"
    {
			    (yyval.tree) = makeCanonicalStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 122:
/* Line 1787 of yacc.c  */
#line 984 "parser.y"
    {
			    (yyval.tree) = makeAutoSimplifyStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 123:
/* Line 1787 of yacc.c  */
#line 988 "parser.y"
    {
			    (yyval.tree) = makeTaylorRecursStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 124:
/* Line 1787 of yacc.c  */
#line 992 "parser.y"
    {
			    (yyval.tree) = makeTimingStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 125:
/* Line 1787 of yacc.c  */
#line 996 "parser.y"
    {
			    (yyval.tree) = makeFullParenStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 126:
/* Line 1787 of yacc.c  */
#line 1000 "parser.y"
    {
			    (yyval.tree) = makeMidpointStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 127:
/* Line 1787 of yacc.c  */
#line 1004 "parser.y"
    {
			    (yyval.tree) = makeDieOnErrorStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 128:
/* Line 1787 of yacc.c  */
#line 1008 "parser.y"
    {
			    (yyval.tree) = makeRationalModeStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 129:
/* Line 1787 of yacc.c  */
#line 1012 "parser.y"
    {
			    (yyval.tree) = makeSuppressWarningsStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 130:
/* Line 1787 of yacc.c  */
#line 1016 "parser.y"
    {
			    (yyval.tree) = makeHopitalRecursStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 131:
/* Line 1787 of yacc.c  */
#line 1022 "parser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].tree));
			  }
    break;

  case 132:
/* Line 1787 of yacc.c  */
#line 1026 "parser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].tree));
			  }
    break;

  case 133:
/* Line 1787 of yacc.c  */
#line 1032 "parser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].association));
			  }
    break;

  case 134:
/* Line 1787 of yacc.c  */
#line 1036 "parser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].association));
			  }
    break;

  case 135:
/* Line 1787 of yacc.c  */
#line 1042 "parser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 136:
/* Line 1787 of yacc.c  */
#line 1046 "parser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 137:
/* Line 1787 of yacc.c  */
#line 1052 "parser.y"
    {
			    (yyval.association) = (entry *) safeMalloc(sizeof(entry));
			    (yyval.association)->name = (char *) safeCalloc(strlen((yyvsp[(2) - (4)].value)) + 1, sizeof(char));
			    strcpy((yyval.association)->name,(yyvsp[(2) - (4)].value));
			    free((yyvsp[(2) - (4)].value));
			    (yyval.association)->value = (void *) ((yyvsp[(4) - (4)].tree));
			  }
    break;

  case 138:
/* Line 1787 of yacc.c  */
#line 1062 "parser.y"
    {
			   (yyval.tree) = (yyvsp[(1) - (1)].tree);
			 }
    break;

  case 139:
/* Line 1787 of yacc.c  */
#line 1066 "parser.y"
    {
			    (yyval.tree) = makeMatch((yyvsp[(2) - (4)].tree),(yyvsp[(4) - (4)].list));
			  }
    break;

  case 140:
/* Line 1787 of yacc.c  */
#line 1072 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 141:
/* Line 1787 of yacc.c  */
#line 1076 "parser.y"
    {
			    (yyval.tree) = makeAnd((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 142:
/* Line 1787 of yacc.c  */
#line 1080 "parser.y"
    {
			    (yyval.tree) = makeOr((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 143:
/* Line 1787 of yacc.c  */
#line 1084 "parser.y"
    {
			    (yyval.tree) = makeNegation((yyvsp[(2) - (2)].tree));
			  }
    break;

  case 144:
/* Line 1787 of yacc.c  */
#line 1090 "parser.y"
    {
			    (yyval.dblnode) = (doubleNode *) safeMalloc(sizeof(doubleNode));
			    (yyval.dblnode)->a = (yyvsp[(1) - (4)].tree);
			    (yyval.dblnode)->b = (yyvsp[(3) - (4)].tree);
			  }
    break;

  case 145:
/* Line 1787 of yacc.c  */
#line 1099 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 146:
/* Line 1787 of yacc.c  */
#line 1103 "parser.y"
    {
			    (yyval.tree) = makeCompareEqual((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 147:
/* Line 1787 of yacc.c  */
#line 1107 "parser.y"
    {
			    (yyval.tree) = makeCompareIn((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 148:
/* Line 1787 of yacc.c  */
#line 1111 "parser.y"
    {
			    (yyval.tree) = makeCompareLess((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 149:
/* Line 1787 of yacc.c  */
#line 1115 "parser.y"
    {
			    (yyval.tree) = makeCompareGreater((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 150:
/* Line 1787 of yacc.c  */
#line 1119 "parser.y"
    {
			    (yyval.tree) = makeCompareLessEqual((yyvsp[(1) - (4)].tree), (yyvsp[(4) - (4)].tree));
			  }
    break;

  case 151:
/* Line 1787 of yacc.c  */
#line 1123 "parser.y"
    {
			    (yyval.tree) = makeCompareGreaterEqual((yyvsp[(1) - (4)].tree), (yyvsp[(4) - (4)].tree));
			  }
    break;

  case 152:
/* Line 1787 of yacc.c  */
#line 1127 "parser.y"
    {
			    (yyval.tree) = makeCompareNotEqual((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 153:
/* Line 1787 of yacc.c  */
#line 1133 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 154:
/* Line 1787 of yacc.c  */
#line 1137 "parser.y"
    {
			    (yyval.tree) = makeAdd((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 155:
/* Line 1787 of yacc.c  */
#line 1141 "parser.y"
    {
			    (yyval.tree) = makeSub((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 156:
/* Line 1787 of yacc.c  */
#line 1145 "parser.y"
    {
			    (yyval.tree) = makeConcat((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 157:
/* Line 1787 of yacc.c  */
#line 1149 "parser.y"
    {
			    (yyval.tree) = makeAddToList((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 158:
/* Line 1787 of yacc.c  */
#line 1153 "parser.y"
    {
			    (yyval.tree) = makePrepend((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 159:
/* Line 1787 of yacc.c  */
#line 1157 "parser.y"
    {
			    (yyval.tree) = makeAppend((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 160:
/* Line 1787 of yacc.c  */
#line 1163 "parser.y"
    {
			    (yyval.count) = 0;
                          }
    break;

  case 161:
/* Line 1787 of yacc.c  */
#line 1167 "parser.y"
    {
			    (yyval.count) = 1;
                          }
    break;

  case 162:
/* Line 1787 of yacc.c  */
#line 1171 "parser.y"
    {
			    (yyval.count) = (yyvsp[(2) - (2)].count);
                          }
    break;

  case 163:
/* Line 1787 of yacc.c  */
#line 1175 "parser.y"
    {
			    (yyval.count) = (yyvsp[(2) - (2)].count)+1;
                          }
    break;

  case 164:
/* Line 1787 of yacc.c  */
#line 1182 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
                          }
    break;

  case 165:
/* Line 1787 of yacc.c  */
#line 1186 "parser.y"
    {
			    tempNode = (yyvsp[(2) - (2)].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[(1) - (2)].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = tempNode;
                          }
    break;

  case 166:
/* Line 1787 of yacc.c  */
#line 1193 "parser.y"
    {
			    (yyval.tree) = makeEvalConst((yyvsp[(2) - (2)].tree));
                          }
    break;

  case 167:
/* Line 1787 of yacc.c  */
#line 1197 "parser.y"
    {
			    (yyval.tree) = makeMul((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
                          }
    break;

  case 168:
/* Line 1787 of yacc.c  */
#line 1201 "parser.y"
    {
			    (yyval.tree) = makeDiv((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
                          }
    break;

  case 169:
/* Line 1787 of yacc.c  */
#line 1205 "parser.y"
    {
			    tempNode = (yyvsp[(4) - (4)].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[(3) - (4)].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makeMul((yyvsp[(1) - (4)].tree), tempNode);
                          }
    break;

  case 170:
/* Line 1787 of yacc.c  */
#line 1212 "parser.y"
    {
			    tempNode = (yyvsp[(4) - (4)].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[(3) - (4)].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makeDiv((yyvsp[(1) - (4)].tree), tempNode);
                          }
    break;

  case 171:
/* Line 1787 of yacc.c  */
#line 1219 "parser.y"
    {
			    (yyval.tree) = makeMul((yyvsp[(1) - (4)].tree), makeEvalConst((yyvsp[(4) - (4)].tree)));
                          }
    break;

  case 172:
/* Line 1787 of yacc.c  */
#line 1223 "parser.y"
    {
			    (yyval.tree) = makeDiv((yyvsp[(1) - (4)].tree), makeEvalConst((yyvsp[(4) - (4)].tree)));
                          }
    break;

  case 173:
/* Line 1787 of yacc.c  */
#line 1229 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
                          }
    break;

  case 174:
/* Line 1787 of yacc.c  */
#line 1233 "parser.y"
    {
			    (yyval.tree) = makePow((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
                          }
    break;

  case 175:
/* Line 1787 of yacc.c  */
#line 1237 "parser.y"
    {
			    tempNode = (yyvsp[(4) - (4)].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[(3) - (4)].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makePow((yyvsp[(1) - (4)].tree), tempNode);
                          }
    break;

  case 176:
/* Line 1787 of yacc.c  */
#line 1244 "parser.y"
    {
			    (yyval.tree) = makePow((yyvsp[(1) - (4)].tree), makeEvalConst((yyvsp[(4) - (4)].tree)));
                          }
    break;

  case 177:
/* Line 1787 of yacc.c  */
#line 1251 "parser.y"
    {
			    (yyval.tree) = makeOn();
			  }
    break;

  case 178:
/* Line 1787 of yacc.c  */
#line 1255 "parser.y"
    {
			    (yyval.tree) = makeOff();
			  }
    break;

  case 179:
/* Line 1787 of yacc.c  */
#line 1259 "parser.y"
    {
			    (yyval.tree) = makeDyadic();
			  }
    break;

  case 180:
/* Line 1787 of yacc.c  */
#line 1263 "parser.y"
    {
			    (yyval.tree) = makePowers();
			  }
    break;

  case 181:
/* Line 1787 of yacc.c  */
#line 1267 "parser.y"
    {
			    (yyval.tree) = makeBinaryThing();
			  }
    break;

  case 182:
/* Line 1787 of yacc.c  */
#line 1271 "parser.y"
    {
			    (yyval.tree) = makeHexadecimalThing();
			  }
    break;

  case 183:
/* Line 1787 of yacc.c  */
#line 1275 "parser.y"
    {
			    (yyval.tree) = makeFile();
			  }
    break;

  case 184:
/* Line 1787 of yacc.c  */
#line 1279 "parser.y"
    {
			    (yyval.tree) = makePostscript();
			  }
    break;

  case 185:
/* Line 1787 of yacc.c  */
#line 1283 "parser.y"
    {
			    (yyval.tree) = makePostscriptFile();
			  }
    break;

  case 186:
/* Line 1787 of yacc.c  */
#line 1287 "parser.y"
    {
			    (yyval.tree) = makePerturb();
			  }
    break;

  case 187:
/* Line 1787 of yacc.c  */
#line 1291 "parser.y"
    {
			    (yyval.tree) = makeRoundDown();
			  }
    break;

  case 188:
/* Line 1787 of yacc.c  */
#line 1295 "parser.y"
    {
			    (yyval.tree) = makeRoundUp();
			  }
    break;

  case 189:
/* Line 1787 of yacc.c  */
#line 1299 "parser.y"
    {
			    (yyval.tree) = makeRoundToZero();
			  }
    break;

  case 190:
/* Line 1787 of yacc.c  */
#line 1303 "parser.y"
    {
			    (yyval.tree) = makeRoundToNearest();
			  }
    break;

  case 191:
/* Line 1787 of yacc.c  */
#line 1307 "parser.y"
    {
			    (yyval.tree) = makeHonorCoeff();
			  }
    break;

  case 192:
/* Line 1787 of yacc.c  */
#line 1311 "parser.y"
    {
			    (yyval.tree) = makeTrue();
			  }
    break;

  case 193:
/* Line 1787 of yacc.c  */
#line 1315 "parser.y"
    {
			    (yyval.tree) = makeUnit();
			  }
    break;

  case 194:
/* Line 1787 of yacc.c  */
#line 1319 "parser.y"
    {
			    (yyval.tree) = makeFalse();
			  }
    break;

  case 195:
/* Line 1787 of yacc.c  */
#line 1323 "parser.y"
    {
			    (yyval.tree) = makeDefault();
			  }
    break;

  case 196:
/* Line 1787 of yacc.c  */
#line 1327 "parser.y"
    {
			    (yyval.tree) = makeDecimal();
			  }
    break;

  case 197:
/* Line 1787 of yacc.c  */
#line 1331 "parser.y"
    {
			    (yyval.tree) = makeAbsolute();
			  }
    break;

  case 198:
/* Line 1787 of yacc.c  */
#line 1335 "parser.y"
    {
			    (yyval.tree) = makeRelative();
			  }
    break;

  case 199:
/* Line 1787 of yacc.c  */
#line 1339 "parser.y"
    {
			    (yyval.tree) = makeFixed();
			  }
    break;

  case 200:
/* Line 1787 of yacc.c  */
#line 1343 "parser.y"
    {
			    (yyval.tree) = makeFloating();
			  }
    break;

  case 201:
/* Line 1787 of yacc.c  */
#line 1347 "parser.y"
    {
			    (yyval.tree) = makeError();
			  }
    break;

  case 202:
/* Line 1787 of yacc.c  */
#line 1351 "parser.y"
    {
			    (yyval.tree) = makeDoubleSymbol();
			  }
    break;

  case 203:
/* Line 1787 of yacc.c  */
#line 1355 "parser.y"
    {
			    (yyval.tree) = makeSingleSymbol();
			  }
    break;

  case 204:
/* Line 1787 of yacc.c  */
#line 1359 "parser.y"
    {
			    (yyval.tree) = makeQuadSymbol();
			  }
    break;

  case 205:
/* Line 1787 of yacc.c  */
#line 1363 "parser.y"
    {
			    (yyval.tree) = makeHalfPrecisionSymbol();
			  }
    break;

  case 206:
/* Line 1787 of yacc.c  */
#line 1367 "parser.y"
    {
			    (yyval.tree) = makeDoubleextendedSymbol();
			  }
    break;

  case 207:
/* Line 1787 of yacc.c  */
#line 1371 "parser.y"
    {
			    (yyval.tree) = makeDoubleDoubleSymbol();
			  }
    break;

  case 208:
/* Line 1787 of yacc.c  */
#line 1375 "parser.y"
    {
			    (yyval.tree) = makeTripleDoubleSymbol();
			  }
    break;

  case 209:
/* Line 1787 of yacc.c  */
#line 1379 "parser.y"
    {
			    tempString = safeCalloc(strlen((yyvsp[(1) - (1)].value)) + 1, sizeof(char));
			    strcpy(tempString, (yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			    tempString2 = safeCalloc(strlen(tempString) + 1, sizeof(char));
			    strcpy(tempString2, tempString);
			    free(tempString);
			    (yyval.tree) = makeString(tempString2);
			    free(tempString2);
			  }
    break;

  case 210:
/* Line 1787 of yacc.c  */
#line 1390 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 211:
/* Line 1787 of yacc.c  */
#line 1394 "parser.y"
    {
			    (yyval.tree) = makeTableAccess((yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 212:
/* Line 1787 of yacc.c  */
#line 1399 "parser.y"
    {
			    (yyval.tree) = makeIsBound((yyvsp[(3) - (4)].value));
			    free((yyvsp[(3) - (4)].value));
			  }
    break;

  case 213:
/* Line 1787 of yacc.c  */
#line 1404 "parser.y"
    {
			    (yyval.tree) = makeTableAccessWithSubstitute((yyvsp[(1) - (4)].value), (yyvsp[(3) - (4)].list));
			    free((yyvsp[(1) - (4)].value));
			  }
    break;

  case 214:
/* Line 1787 of yacc.c  */
#line 1409 "parser.y"
    {
			    (yyval.tree) = makeTableAccessWithSubstitute((yyvsp[(1) - (3)].value), NULL);
			    free((yyvsp[(1) - (3)].value));
			  }
    break;

  case 215:
/* Line 1787 of yacc.c  */
#line 1414 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 216:
/* Line 1787 of yacc.c  */
#line 1418 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 217:
/* Line 1787 of yacc.c  */
#line 1422 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 218:
/* Line 1787 of yacc.c  */
#line 1426 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 219:
/* Line 1787 of yacc.c  */
#line 1430 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(2) - (3)].tree);
			  }
    break;

  case 220:
/* Line 1787 of yacc.c  */
#line 1434 "parser.y"
    {
			    (yyval.tree) = makeStructure((yyvsp[(2) - (3)].list));
			  }
    break;

  case 221:
/* Line 1787 of yacc.c  */
#line 1438 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 222:
/* Line 1787 of yacc.c  */
#line 1442 "parser.y"
    {
			    (yyval.tree) = makeIndex((yyvsp[(1) - (1)].dblnode)->a, (yyvsp[(1) - (1)].dblnode)->b);
			    free((yyvsp[(1) - (1)].dblnode));
			  }
    break;

  case 223:
/* Line 1787 of yacc.c  */
#line 1447 "parser.y"
    {
			    (yyval.tree) = makeStructAccess((yyvsp[(1) - (3)].tree),(yyvsp[(3) - (3)].value));
			    free((yyvsp[(3) - (3)].value));
			  }
    break;

  case 224:
/* Line 1787 of yacc.c  */
#line 1452 "parser.y"
    {
			    (yyval.tree) = makeApply(makeStructAccess((yyvsp[(1) - (6)].tree),(yyvsp[(3) - (6)].value)),(yyvsp[(5) - (6)].list));
			    free((yyvsp[(3) - (6)].value));
			  }
    break;

  case 225:
/* Line 1787 of yacc.c  */
#line 1457 "parser.y"
    {
			    (yyval.tree) = makeApply((yyvsp[(2) - (6)].tree),(yyvsp[(5) - (6)].list));
			  }
    break;

  case 226:
/* Line 1787 of yacc.c  */
#line 1461 "parser.y"
    {
			    (yyval.tree) = (yyvsp[(2) - (2)].tree);
			  }
    break;

  case 227:
/* Line 1787 of yacc.c  */
#line 1465 "parser.y"
    {
			    (yyval.tree) = makeTime((yyvsp[(3) - (4)].tree));
                          }
    break;

  case 228:
/* Line 1787 of yacc.c  */
#line 1471 "parser.y"
    {
			    (yyval.list) = addElement(NULL,(yyvsp[(1) - (1)].tree));
			  }
    break;

  case 229:
/* Line 1787 of yacc.c  */
#line 1475 "parser.y"
    {
			    (yyval.list) = addElement((yyvsp[(2) - (2)].list),(yyvsp[(1) - (2)].tree));
			  }
    break;

  case 230:
/* Line 1787 of yacc.c  */
#line 1481 "parser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (9)].tree),makeCommandList(concatChains((yyvsp[(4) - (9)].list), (yyvsp[(5) - (9)].list))),(yyvsp[(7) - (9)].tree));
			  }
    break;

  case 231:
/* Line 1787 of yacc.c  */
#line 1485 "parser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (6)].tree),makeCommandList(concatChains((yyvsp[(4) - (6)].list), (yyvsp[(5) - (6)].list))),makeUnit());
			  }
    break;

  case 232:
/* Line 1787 of yacc.c  */
#line 1489 "parser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (8)].tree),makeCommandList((yyvsp[(4) - (8)].list)),(yyvsp[(6) - (8)].tree));
			  }
    break;

  case 233:
/* Line 1787 of yacc.c  */
#line 1493 "parser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (5)].tree),makeCommandList((yyvsp[(4) - (5)].list)),makeUnit());
			  }
    break;

  case 234:
/* Line 1787 of yacc.c  */
#line 1497 "parser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (8)].tree),makeCommandList((yyvsp[(4) - (8)].list)),(yyvsp[(6) - (8)].tree));
			  }
    break;

  case 235:
/* Line 1787 of yacc.c  */
#line 1501 "parser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (5)].tree),makeCommandList((yyvsp[(4) - (5)].list)),makeUnit());
			  }
    break;

  case 236:
/* Line 1787 of yacc.c  */
#line 1505 "parser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (7)].tree), makeCommandList(addElement(NULL,makeNop())), (yyvsp[(5) - (7)].tree));
			  }
    break;

  case 237:
/* Line 1787 of yacc.c  */
#line 1509 "parser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (4)].tree), makeCommandList(addElement(NULL,makeNop())), makeUnit());
			  }
    break;

  case 238:
/* Line 1787 of yacc.c  */
#line 1513 "parser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (5)].tree), makeCommandList(addElement(NULL,makeNop())), (yyvsp[(4) - (5)].tree));
			  }
    break;

  case 239:
/* Line 1787 of yacc.c  */
#line 1519 "parser.y"
    {
			    (yyval.tree) = makeDecimalConstant((yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 240:
/* Line 1787 of yacc.c  */
#line 1524 "parser.y"
    {
			    (yyval.tree) = makeMidpointConstant((yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 241:
/* Line 1787 of yacc.c  */
#line 1529 "parser.y"
    {
			    (yyval.tree) = makeDyadicConstant((yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 242:
/* Line 1787 of yacc.c  */
#line 1534 "parser.y"
    {
			    (yyval.tree) = makeHexConstant((yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 243:
/* Line 1787 of yacc.c  */
#line 1539 "parser.y"
    {
			    (yyval.tree) = makeHexadecimalConstant((yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 244:
/* Line 1787 of yacc.c  */
#line 1544 "parser.y"
    {
			    (yyval.tree) = makeBinaryConstant((yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 245:
/* Line 1787 of yacc.c  */
#line 1549 "parser.y"
    {
			    (yyval.tree) = makePi();
			  }
    break;

  case 246:
/* Line 1787 of yacc.c  */
#line 1557 "parser.y"
    {
			    (yyval.tree) = makeEmptyList();
			  }
    break;

  case 247:
/* Line 1787 of yacc.c  */
#line 1561 "parser.y"
    {
			    (yyval.tree) = makeEmptyList();
			  }
    break;

  case 248:
/* Line 1787 of yacc.c  */
#line 1565 "parser.y"
    {
			    (yyval.tree) = makeRevertedList((yyvsp[(3) - (5)].list));
			  }
    break;

  case 249:
/* Line 1787 of yacc.c  */
#line 1569 "parser.y"
    {
			    (yyval.tree) = makeRevertedFinalEllipticList((yyvsp[(3) - (6)].list));
			  }
    break;

  case 250:
/* Line 1787 of yacc.c  */
#line 1575 "parser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].tree));
			  }
    break;

  case 251:
/* Line 1787 of yacc.c  */
#line 1579 "parser.y"
    {
			    (yyval.list) = addElement((yyvsp[(1) - (3)].list), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 252:
/* Line 1787 of yacc.c  */
#line 1583 "parser.y"
    {
			    (yyval.list) = addElement(addElement((yyvsp[(1) - (5)].list), makeElliptic()), (yyvsp[(5) - (5)].tree));
			  }
    break;

  case 253:
/* Line 1787 of yacc.c  */
#line 1590 "parser.y"
    {
			    (yyval.tree) = makeRange((yyvsp[(2) - (5)].tree), (yyvsp[(4) - (5)].tree));
			  }
    break;

  case 254:
/* Line 1787 of yacc.c  */
#line 1594 "parser.y"
    {
			    (yyval.tree) = makeRange((yyvsp[(2) - (5)].tree), (yyvsp[(4) - (5)].tree));
			  }
    break;

  case 255:
/* Line 1787 of yacc.c  */
#line 1598 "parser.y"
    {
			    (yyval.tree) = makeRange((yyvsp[(2) - (3)].tree), copyThing((yyvsp[(2) - (3)].tree)));
			  }
    break;

  case 256:
/* Line 1787 of yacc.c  */
#line 1604 "parser.y"
    {
			    (yyval.tree) = makeDeboundMax((yyvsp[(2) - (3)].tree));
			  }
    break;

  case 257:
/* Line 1787 of yacc.c  */
#line 1608 "parser.y"
    {
			    (yyval.tree) = makeDeboundMid((yyvsp[(2) - (3)].tree));
			  }
    break;

  case 258:
/* Line 1787 of yacc.c  */
#line 1612 "parser.y"
    {
			    (yyval.tree) = makeDeboundMin((yyvsp[(2) - (3)].tree));
			  }
    break;

  case 259:
/* Line 1787 of yacc.c  */
#line 1616 "parser.y"
    {
			    (yyval.tree) = makeDeboundMax((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 260:
/* Line 1787 of yacc.c  */
#line 1620 "parser.y"
    {
			    (yyval.tree) = makeDeboundMid((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 261:
/* Line 1787 of yacc.c  */
#line 1624 "parser.y"
    {
			    (yyval.tree) = makeDeboundMin((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 262:
/* Line 1787 of yacc.c  */
#line 1630 "parser.y"
    {
			    (yyval.tree) = makeDiff((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 263:
/* Line 1787 of yacc.c  */
#line 1634 "parser.y"
    {
			    (yyval.tree) = makeSimplify((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 264:
/* Line 1787 of yacc.c  */
#line 1638 "parser.y"
    {
			    (yyval.tree) = makeBashevaluate(addElement(NULL,(yyvsp[(3) - (4)].tree)));
			  }
    break;

  case 265:
/* Line 1787 of yacc.c  */
#line 1642 "parser.y"
    {
			    (yyval.tree) = makeBashevaluate(addElement(addElement(NULL,(yyvsp[(5) - (6)].tree)),(yyvsp[(3) - (6)].tree)));
			  }
    break;

  case 266:
/* Line 1787 of yacc.c  */
#line 1646 "parser.y"
    {
			    (yyval.tree) = makeRemez(addElement(addElement((yyvsp[(7) - (8)].list), (yyvsp[(5) - (8)].tree)), (yyvsp[(3) - (8)].tree)));
			  }
    break;

  case 267:
/* Line 1787 of yacc.c  */
#line 1650 "parser.y"
    {
			    (yyval.tree) = makeMin((yyvsp[(3) - (4)].list));
			  }
    break;

  case 268:
/* Line 1787 of yacc.c  */
#line 1654 "parser.y"
    {
			    (yyval.tree) = makeMax((yyvsp[(3) - (4)].list));
			  }
    break;

  case 269:
/* Line 1787 of yacc.c  */
#line 1658 "parser.y"
    {
			    (yyval.tree) = makeFPminimax(addElement(addElement(addElement((yyvsp[(9) - (10)].list), (yyvsp[(7) - (10)].tree)), (yyvsp[(5) - (10)].tree)), (yyvsp[(3) - (10)].tree)));
			  }
    break;

  case 270:
/* Line 1787 of yacc.c  */
#line 1662 "parser.y"
    {
			    (yyval.tree) = makeHorner((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 271:
/* Line 1787 of yacc.c  */
#line 1666 "parser.y"
    {
			    (yyval.tree) = makeCanonicalThing((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 272:
/* Line 1787 of yacc.c  */
#line 1670 "parser.y"
    {
			    (yyval.tree) = makeExpand((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 273:
/* Line 1787 of yacc.c  */
#line 1674 "parser.y"
    {
			    (yyval.tree) = makeSimplifySafe((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 274:
/* Line 1787 of yacc.c  */
#line 1678 "parser.y"
    {
			    (yyval.tree) = makeTaylor((yyvsp[(3) - (8)].tree), (yyvsp[(5) - (8)].tree), (yyvsp[(7) - (8)].tree));
			  }
    break;

  case 275:
/* Line 1787 of yacc.c  */
#line 1682 "parser.y"
    {
                            (yyval.tree) = makeTaylorform(addElement(addElement((yyvsp[(7) - (8)].list), (yyvsp[(5) - (8)].tree)), (yyvsp[(3) - (8)].tree)));
			  }
    break;

  case 276:
/* Line 1787 of yacc.c  */
#line 1686 "parser.y"
    {
                            (yyval.tree) = makeAutodiff(addElement(addElement(addElement(NULL, (yyvsp[(7) - (8)].tree)), (yyvsp[(5) - (8)].tree)), (yyvsp[(3) - (8)].tree)));
			  }
    break;

  case 277:
/* Line 1787 of yacc.c  */
#line 1690 "parser.y"
    {
			    (yyval.tree) = makeDegree((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 278:
/* Line 1787 of yacc.c  */
#line 1694 "parser.y"
    {
			    (yyval.tree) = makeNumerator((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 279:
/* Line 1787 of yacc.c  */
#line 1698 "parser.y"
    {
			    (yyval.tree) = makeDenominator((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 280:
/* Line 1787 of yacc.c  */
#line 1702 "parser.y"
    {
			    (yyval.tree) = makeSubstitute((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 281:
/* Line 1787 of yacc.c  */
#line 1706 "parser.y"
    {
			    (yyval.tree) = makeCoeff((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 282:
/* Line 1787 of yacc.c  */
#line 1710 "parser.y"
    {
			    (yyval.tree) = makeSubpoly((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 283:
/* Line 1787 of yacc.c  */
#line 1714 "parser.y"
    {
			    (yyval.tree) = makeRoundcoefficients((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 284:
/* Line 1787 of yacc.c  */
#line 1718 "parser.y"
    {
			    (yyval.tree) = makeRationalapprox((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 285:
/* Line 1787 of yacc.c  */
#line 1722 "parser.y"
    {
			    (yyval.tree) = makeAccurateInfnorm(addElement(addElement((yyvsp[(7) - (8)].list), (yyvsp[(5) - (8)].tree)), (yyvsp[(3) - (8)].tree)));
			  }
    break;

  case 286:
/* Line 1787 of yacc.c  */
#line 1726 "parser.y"
    {
			    (yyval.tree) = makeRoundToFormat((yyvsp[(3) - (8)].tree), (yyvsp[(5) - (8)].tree), (yyvsp[(7) - (8)].tree));
			  }
    break;

  case 287:
/* Line 1787 of yacc.c  */
#line 1730 "parser.y"
    {
			    (yyval.tree) = makeEvaluate((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 288:
/* Line 1787 of yacc.c  */
#line 1734 "parser.y"
    {
			    (yyval.tree) = makeParse((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 289:
/* Line 1787 of yacc.c  */
#line 1738 "parser.y"
    {
			    (yyval.tree) = makeReadXml((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 290:
/* Line 1787 of yacc.c  */
#line 1742 "parser.y"
    {
			    (yyval.tree) = makeInfnorm(addElement((yyvsp[(5) - (6)].list), (yyvsp[(3) - (6)].tree)));
			  }
    break;

  case 291:
/* Line 1787 of yacc.c  */
#line 1746 "parser.y"
    {
			    (yyval.tree) = makeSupnorm(addElement(addElement(addElement(addElement(addElement(NULL,(yyvsp[(11) - (12)].tree)),(yyvsp[(9) - (12)].tree)),(yyvsp[(7) - (12)].tree)),(yyvsp[(5) - (12)].tree)),(yyvsp[(3) - (12)].tree)));
			  }
    break;

  case 292:
/* Line 1787 of yacc.c  */
#line 1750 "parser.y"
    {
			    (yyval.tree) = makeFindZeros((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 293:
/* Line 1787 of yacc.c  */
#line 1754 "parser.y"
    {
			    (yyval.tree) = makeFPFindZeros((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 294:
/* Line 1787 of yacc.c  */
#line 1758 "parser.y"
    {
			    (yyval.tree) = makeDirtyInfnorm((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 295:
/* Line 1787 of yacc.c  */
#line 1762 "parser.y"
    {
			    (yyval.tree) = makeNumberRoots((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 296:
/* Line 1787 of yacc.c  */
#line 1766 "parser.y"
    {
			    (yyval.tree) = makeIntegral((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 297:
/* Line 1787 of yacc.c  */
#line 1770 "parser.y"
    {
			    (yyval.tree) = makeDirtyIntegral((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 298:
/* Line 1787 of yacc.c  */
#line 1774 "parser.y"
    {
			    (yyval.tree) = makeImplementPoly(addElement(addElement(addElement(addElement(addElement((yyvsp[(13) - (14)].list), (yyvsp[(11) - (14)].tree)), (yyvsp[(9) - (14)].tree)), (yyvsp[(7) - (14)].tree)), (yyvsp[(5) - (14)].tree)), (yyvsp[(3) - (14)].tree)));
			  }
    break;

  case 299:
/* Line 1787 of yacc.c  */
#line 1778 "parser.y"
    {
			    (yyval.tree) = makeCheckInfnorm((yyvsp[(3) - (8)].tree), (yyvsp[(5) - (8)].tree), (yyvsp[(7) - (8)].tree));
			  }
    break;

  case 300:
/* Line 1787 of yacc.c  */
#line 1782 "parser.y"
    {
			    (yyval.tree) = makeZeroDenominators((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 301:
/* Line 1787 of yacc.c  */
#line 1786 "parser.y"
    {
			    (yyval.tree) = makeIsEvaluable((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 302:
/* Line 1787 of yacc.c  */
#line 1790 "parser.y"
    {
			    (yyval.tree) = makeSearchGal((yyvsp[(3) - (4)].list));
			  }
    break;

  case 303:
/* Line 1787 of yacc.c  */
#line 1794 "parser.y"
    {
			    (yyval.tree) = makeGuessDegree(addElement(addElement((yyvsp[(7) - (8)].list), (yyvsp[(5) - (8)].tree)), (yyvsp[(3) - (8)].tree)));
			  }
    break;

  case 304:
/* Line 1787 of yacc.c  */
#line 1798 "parser.y"
    {
			    (yyval.tree) = makeDirtyFindZeros((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 305:
/* Line 1787 of yacc.c  */
#line 1802 "parser.y"
    {
			    (yyval.tree) = makeHead((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 306:
/* Line 1787 of yacc.c  */
#line 1806 "parser.y"
    {
			    (yyval.tree) = makeRoundCorrectly((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 307:
/* Line 1787 of yacc.c  */
#line 1810 "parser.y"
    {
			    (yyval.tree) = makeReadFile((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 308:
/* Line 1787 of yacc.c  */
#line 1814 "parser.y"
    {
			    (yyval.tree) = makeRevert((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 309:
/* Line 1787 of yacc.c  */
#line 1818 "parser.y"
    {
			    (yyval.tree) = makeSort((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 310:
/* Line 1787 of yacc.c  */
#line 1822 "parser.y"
    {
			    (yyval.tree) = makeMantissa((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 311:
/* Line 1787 of yacc.c  */
#line 1826 "parser.y"
    {
			    (yyval.tree) = makeExponent((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 312:
/* Line 1787 of yacc.c  */
#line 1830 "parser.y"
    {
			    (yyval.tree) = makePrecision((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 313:
/* Line 1787 of yacc.c  */
#line 1834 "parser.y"
    {
			    (yyval.tree) = makeTail((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 314:
/* Line 1787 of yacc.c  */
#line 1838 "parser.y"
    {
			    (yyval.tree) = makeSqrt((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 315:
/* Line 1787 of yacc.c  */
#line 1842 "parser.y"
    {
			    (yyval.tree) = makeExp((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 316:
/* Line 1787 of yacc.c  */
#line 1846 "parser.y"
    {
			    (yyval.tree) = makeProcedureFunction((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 317:
/* Line 1787 of yacc.c  */
#line 1850 "parser.y"
    {
			    (yyval.tree) = makeSubstitute(makeProcedureFunction((yyvsp[(3) - (6)].tree)),(yyvsp[(5) - (6)].tree));
			  }
    break;

  case 318:
/* Line 1787 of yacc.c  */
#line 1854 "parser.y"
    {
			    (yyval.tree) = makeLog((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 319:
/* Line 1787 of yacc.c  */
#line 1858 "parser.y"
    {
			    (yyval.tree) = makeLog2((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 320:
/* Line 1787 of yacc.c  */
#line 1862 "parser.y"
    {
			    (yyval.tree) = makeLog10((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 321:
/* Line 1787 of yacc.c  */
#line 1866 "parser.y"
    {
			    (yyval.tree) = makeSin((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 322:
/* Line 1787 of yacc.c  */
#line 1870 "parser.y"
    {
			    (yyval.tree) = makeCos((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 323:
/* Line 1787 of yacc.c  */
#line 1874 "parser.y"
    {
			    (yyval.tree) = makeTan((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 324:
/* Line 1787 of yacc.c  */
#line 1878 "parser.y"
    {
			    (yyval.tree) = makeAsin((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 325:
/* Line 1787 of yacc.c  */
#line 1882 "parser.y"
    {
			    (yyval.tree) = makeAcos((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 326:
/* Line 1787 of yacc.c  */
#line 1886 "parser.y"
    {
			    (yyval.tree) = makeAtan((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 327:
/* Line 1787 of yacc.c  */
#line 1890 "parser.y"
    {
			    (yyval.tree) = makeSinh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 328:
/* Line 1787 of yacc.c  */
#line 1894 "parser.y"
    {
			    (yyval.tree) = makeCosh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 329:
/* Line 1787 of yacc.c  */
#line 1898 "parser.y"
    {
			    (yyval.tree) = makeTanh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 330:
/* Line 1787 of yacc.c  */
#line 1902 "parser.y"
    {
			    (yyval.tree) = makeAsinh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 331:
/* Line 1787 of yacc.c  */
#line 1906 "parser.y"
    {
			    (yyval.tree) = makeAcosh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 332:
/* Line 1787 of yacc.c  */
#line 1910 "parser.y"
    {
			    (yyval.tree) = makeAtanh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 333:
/* Line 1787 of yacc.c  */
#line 1914 "parser.y"
    {
			    (yyval.tree) = makeAbs((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 334:
/* Line 1787 of yacc.c  */
#line 1918 "parser.y"
    {
			    (yyval.tree) = makeErf((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 335:
/* Line 1787 of yacc.c  */
#line 1922 "parser.y"
    {
			    (yyval.tree) = makeErfc((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 336:
/* Line 1787 of yacc.c  */
#line 1926 "parser.y"
    {
			    (yyval.tree) = makeLog1p((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 337:
/* Line 1787 of yacc.c  */
#line 1930 "parser.y"
    {
			    (yyval.tree) = makeExpm1((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 338:
/* Line 1787 of yacc.c  */
#line 1934 "parser.y"
    {
			    (yyval.tree) = makeDouble((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 339:
/* Line 1787 of yacc.c  */
#line 1938 "parser.y"
    {
			    (yyval.tree) = makeSingle((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 340:
/* Line 1787 of yacc.c  */
#line 1942 "parser.y"
    {
			    (yyval.tree) = makeQuad((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 341:
/* Line 1787 of yacc.c  */
#line 1946 "parser.y"
    {
			    (yyval.tree) = makeHalfPrecision((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 342:
/* Line 1787 of yacc.c  */
#line 1950 "parser.y"
    {
			    (yyval.tree) = makeDoubledouble((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 343:
/* Line 1787 of yacc.c  */
#line 1954 "parser.y"
    {
			    (yyval.tree) = makeTripledouble((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 344:
/* Line 1787 of yacc.c  */
#line 1958 "parser.y"
    {
			    (yyval.tree) = makeDoubleextended((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 345:
/* Line 1787 of yacc.c  */
#line 1962 "parser.y"
    {
			    (yyval.tree) = makeCeil((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 346:
/* Line 1787 of yacc.c  */
#line 1966 "parser.y"
    {
			    (yyval.tree) = makeFloor((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 347:
/* Line 1787 of yacc.c  */
#line 1970 "parser.y"
    {
			    (yyval.tree) = makeNearestInt((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 348:
/* Line 1787 of yacc.c  */
#line 1974 "parser.y"
    {
			    (yyval.tree) = makeLength((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 349:
/* Line 1787 of yacc.c  */
#line 1980 "parser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 350:
/* Line 1787 of yacc.c  */
#line 1984 "parser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 351:
/* Line 1787 of yacc.c  */
#line 1990 "parser.y"
    {
			    (yyval.tree) = makePrecDeref();
			  }
    break;

  case 352:
/* Line 1787 of yacc.c  */
#line 1994 "parser.y"
    {
			    (yyval.tree) = makePointsDeref();
			  }
    break;

  case 353:
/* Line 1787 of yacc.c  */
#line 1998 "parser.y"
    {
			    (yyval.tree) = makeDiamDeref();
			  }
    break;

  case 354:
/* Line 1787 of yacc.c  */
#line 2002 "parser.y"
    {
			    (yyval.tree) = makeDisplayDeref();
			  }
    break;

  case 355:
/* Line 1787 of yacc.c  */
#line 2006 "parser.y"
    {
			    (yyval.tree) = makeVerbosityDeref();
			  }
    break;

  case 356:
/* Line 1787 of yacc.c  */
#line 2010 "parser.y"
    {
			    (yyval.tree) = makeCanonicalDeref();
			  }
    break;

  case 357:
/* Line 1787 of yacc.c  */
#line 2014 "parser.y"
    {
			    (yyval.tree) = makeAutoSimplifyDeref();
			  }
    break;

  case 358:
/* Line 1787 of yacc.c  */
#line 2018 "parser.y"
    {
			    (yyval.tree) = makeTaylorRecursDeref();
			  }
    break;

  case 359:
/* Line 1787 of yacc.c  */
#line 2022 "parser.y"
    {
			    (yyval.tree) = makeTimingDeref();
			  }
    break;

  case 360:
/* Line 1787 of yacc.c  */
#line 2026 "parser.y"
    {
			    (yyval.tree) = makeFullParenDeref();
			  }
    break;

  case 361:
/* Line 1787 of yacc.c  */
#line 2030 "parser.y"
    {
			    (yyval.tree) = makeMidpointDeref();
			  }
    break;

  case 362:
/* Line 1787 of yacc.c  */
#line 2034 "parser.y"
    {
			    (yyval.tree) = makeDieOnErrorDeref();
			  }
    break;

  case 363:
/* Line 1787 of yacc.c  */
#line 2038 "parser.y"
    {
			    (yyval.tree) = makeRationalModeDeref();
			  }
    break;

  case 364:
/* Line 1787 of yacc.c  */
#line 2042 "parser.y"
    {
			    (yyval.tree) = makeSuppressWarningsDeref();
			  }
    break;

  case 365:
/* Line 1787 of yacc.c  */
#line 2046 "parser.y"
    {
			    (yyval.tree) = makeHopitalRecursDeref();
			  }
    break;

  case 366:
/* Line 1787 of yacc.c  */
#line 2052 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = CONSTANT_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 367:
/* Line 1787 of yacc.c  */
#line 2058 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = FUNCTION_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 368:
/* Line 1787 of yacc.c  */
#line 2064 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = RANGE_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 369:
/* Line 1787 of yacc.c  */
#line 2070 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = INTEGER_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 370:
/* Line 1787 of yacc.c  */
#line 2076 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = STRING_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 371:
/* Line 1787 of yacc.c  */
#line 2082 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = BOOLEAN_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 372:
/* Line 1787 of yacc.c  */
#line 2088 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = CONSTANT_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 373:
/* Line 1787 of yacc.c  */
#line 2094 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = FUNCTION_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 374:
/* Line 1787 of yacc.c  */
#line 2100 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = RANGE_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 375:
/* Line 1787 of yacc.c  */
#line 2106 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = INTEGER_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 376:
/* Line 1787 of yacc.c  */
#line 2112 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = STRING_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 377:
/* Line 1787 of yacc.c  */
#line 2118 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = BOOLEAN_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 378:
/* Line 1787 of yacc.c  */
#line 2126 "parser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = VOID_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 379:
/* Line 1787 of yacc.c  */
#line 2132 "parser.y"
    {
			    (yyval.integerval) = (yyvsp[(1) - (1)].integerval);
		          }
    break;

  case 380:
/* Line 1787 of yacc.c  */
#line 2139 "parser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].integerval));
			  }
    break;

  case 381:
/* Line 1787 of yacc.c  */
#line 2143 "parser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].integerval));
			  }
    break;

  case 382:
/* Line 1787 of yacc.c  */
#line 2149 "parser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].integerval));
			  }
    break;

  case 383:
/* Line 1787 of yacc.c  */
#line 2153 "parser.y"
    {
			    (yyval.list) = (yyvsp[(2) - (3)].list);
			  }
    break;

  case 384:
/* Line 1787 of yacc.c  */
#line 2160 "parser.y"
    {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a base 10 constant.\n",(yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 385:
/* Line 1787 of yacc.c  */
#line 2165 "parser.y"
    {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a dyadic number constant.\n",(yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
                          }
    break;

  case 386:
/* Line 1787 of yacc.c  */
#line 2170 "parser.y"
    {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a double or single precision constant.\n",(yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
                          }
    break;

  case 387:
/* Line 1787 of yacc.c  */
#line 2175 "parser.y"
    {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a hexadecimal constant.\n",(yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
                          }
    break;

  case 388:
/* Line 1787 of yacc.c  */
#line 2180 "parser.y"
    {
			    outputMode(); sollyaPrintf("\"%s_2\" is recognized as a base 2 constant.\n",(yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
                          }
    break;

  case 389:
/* Line 1787 of yacc.c  */
#line 2185 "parser.y"
    {
#ifdef HELP_PI_TEXT
			    outputMode(); sollyaPrintf(HELP_PI_TEXT);
#else
			    outputMode(); sollyaPrintf("Ratio circonference and diameter of a circle.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PI"
#endif
#endif
                          }
    break;

  case 390:
/* Line 1787 of yacc.c  */
#line 2196 "parser.y"
    {
			    outputMode(); sollyaPrintf("\"%s\" is an identifier.\n",(yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
                          }
    break;

  case 391:
/* Line 1787 of yacc.c  */
#line 2201 "parser.y"
    {
			    outputMode(); sollyaPrintf("\"%s\" is a string constant.\n",(yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
                          }
    break;

  case 392:
/* Line 1787 of yacc.c  */
#line 2206 "parser.y"
    {
			    outputMode(); sollyaPrintf("Left parenthesis.\n");
                          }
    break;

  case 393:
/* Line 1787 of yacc.c  */
#line 2210 "parser.y"
    {
			    outputMode(); sollyaPrintf("Right parenthesis.\n");
                          }
    break;

  case 394:
/* Line 1787 of yacc.c  */
#line 2214 "parser.y"
    {
			    outputMode(); sollyaPrintf("Left bracket - indicates a range.\n");
                          }
    break;

  case 395:
/* Line 1787 of yacc.c  */
#line 2218 "parser.y"
    {
			    outputMode(); sollyaPrintf("Right bracket - indicates a range.\n");
                          }
    break;

  case 396:
/* Line 1787 of yacc.c  */
#line 2222 "parser.y"
    {
			    outputMode(); sollyaPrintf("Left bracket-bar - indicates a list.\n");
                          }
    break;

  case 397:
/* Line 1787 of yacc.c  */
#line 2226 "parser.y"
    {
			    outputMode(); sollyaPrintf("Bar-right bracket - indicates a list.\n");
                          }
    break;

  case 398:
/* Line 1787 of yacc.c  */
#line 2230 "parser.y"
    {
#ifdef HELP_ASSIGNMENT_TEXT
			    outputMode(); sollyaPrintf(HELP_ASSIGNMENT_TEXT);
#else
			    outputMode(); sollyaPrintf("Assignment operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ASSIGNMENT"
#endif
#endif
                          }
    break;

  case 399:
/* Line 1787 of yacc.c  */
#line 2241 "parser.y"
    {
#ifdef HELP_FLOATASSIGNMENT_TEXT
			    outputMode(); sollyaPrintf(HELP_FLOATASSIGNMENT_TEXT);
#else
			    outputMode(); sollyaPrintf("Evaluating assignment operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FLOATASSIGNMENT"
#endif
#endif
                          }
    break;

  case 400:
/* Line 1787 of yacc.c  */
#line 2252 "parser.y"
    {
#ifdef HELP_EQUAL_TEXT
			    outputMode(); sollyaPrintf(HELP_EQUAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Equality test.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EQUAL"
#endif
#endif
                          }
    break;

  case 401:
/* Line 1787 of yacc.c  */
#line 2263 "parser.y"
    {
			    outputMode(); sollyaPrintf("Separator in lists or ranges.\n");
                          }
    break;

  case 402:
/* Line 1787 of yacc.c  */
#line 2267 "parser.y"
    {
#ifdef HELP_NOT_TEXT
			    outputMode(); sollyaPrintf(HELP_NOT_TEXT);
#else
			    outputMode(); sollyaPrintf("Suppresses output on assignments or boolean negation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for NOT"
#endif
#endif
                          }
    break;

  case 403:
/* Line 1787 of yacc.c  */
#line 2278 "parser.y"
    {
			    outputMode(); sollyaPrintf("Dereferences range bounds.\n");
                          }
    break;

  case 404:
/* Line 1787 of yacc.c  */
#line 2282 "parser.y"
    {
#ifdef HELP_LT_TEXT
			    outputMode(); sollyaPrintf(HELP_LT_TEXT);
#else
			    outputMode(); sollyaPrintf("Comparison less than.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LT"
#endif
#endif
                          }
    break;

  case 405:
/* Line 1787 of yacc.c  */
#line 2293 "parser.y"
    {
#ifdef HELP_LE_TEXT
			    outputMode(); sollyaPrintf(HELP_LE_TEXT);
#else
			    outputMode(); sollyaPrintf("Comparison less than or equal to.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LE"
#endif
#endif
                          }
    break;

  case 406:
/* Line 1787 of yacc.c  */
#line 2304 "parser.y"
    {
			    outputMode(); sollyaPrintf("Dereferences the lower range bound.\n");
                          }
    break;

  case 407:
/* Line 1787 of yacc.c  */
#line 2308 "parser.y"
    {
			    outputMode(); sollyaPrintf("Dereferences the mid-point of a range.\n");
                          }
    break;

  case 408:
/* Line 1787 of yacc.c  */
#line 2312 "parser.y"
    {
#ifdef HELP_GE_TEXT
			    outputMode(); sollyaPrintf(HELP_GE_TEXT);
#else
			    outputMode(); sollyaPrintf("Comparison greater than or equal to.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for GE"
#endif
#endif
			  }
    break;

  case 409:
/* Line 1787 of yacc.c  */
#line 2323 "parser.y"
    {
			    outputMode(); sollyaPrintf("Accessing an element in a structured type.\n");
			  }
    break;

  case 410:
/* Line 1787 of yacc.c  */
#line 2327 "parser.y"
    {
			    outputMode(); sollyaPrintf("Dereferences the upper range bound.\n");
                          }
    break;

  case 411:
/* Line 1787 of yacc.c  */
#line 2331 "parser.y"
    {
#ifdef HELP_GT_TEXT
			    outputMode(); sollyaPrintf(HELP_GT_TEXT);
#else
			    outputMode(); sollyaPrintf("Comparison greater than.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for GT"
#endif
#endif
                          }
    break;

  case 412:
/* Line 1787 of yacc.c  */
#line 2342 "parser.y"
    {
			    outputMode(); sollyaPrintf("Ellipsis.\n");
                          }
    break;

  case 413:
/* Line 1787 of yacc.c  */
#line 2346 "parser.y"
    {
			    outputMode(); sollyaPrintf("Dereferences global environment variables.\n");
                          }
    break;

  case 414:
/* Line 1787 of yacc.c  */
#line 2350 "parser.y"
    {
			    outputMode(); sollyaPrintf("Starts or ends a list.\n");
                          }
    break;

  case 415:
/* Line 1787 of yacc.c  */
#line 2354 "parser.y"
    {
#ifdef HELP_CONCAT_TEXT
			    outputMode(); sollyaPrintf(HELP_CONCAT_TEXT);
#else
			    outputMode(); sollyaPrintf("Concatenation of lists or strings.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CONCAT"
#endif
#endif
                          }
    break;

  case 416:
/* Line 1787 of yacc.c  */
#line 2365 "parser.y"
    {
			    outputMode(); sollyaPrintf("a::b prepends a to list b or appends b to list a, preprending list a to list b if both are lists.\n");
                          }
    break;

  case 417:
/* Line 1787 of yacc.c  */
#line 2369 "parser.y"
    {
#ifdef HELP_PREPEND_TEXT
			    outputMode(); sollyaPrintf(HELP_PREPEND_TEXT);
#else
			    outputMode(); sollyaPrintf("a.:b prepends a to list b.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PREPEND"
#endif
#endif
                          }
    break;

  case 418:
/* Line 1787 of yacc.c  */
#line 2380 "parser.y"
    {
#ifdef HELP_APPEND_TEXT
			    outputMode(); sollyaPrintf(HELP_APPEND_TEXT);
#else
			    outputMode(); sollyaPrintf("a:.b appends b to list a.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for APPEND"
#endif
#endif
                          }
    break;

  case 419:
/* Line 1787 of yacc.c  */
#line 2391 "parser.y"
    {
#ifdef HELP_NEQ_TEXT
			    outputMode(); sollyaPrintf(HELP_NEQ_TEXT);
#else
			    outputMode(); sollyaPrintf("Comparison not equal.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for NEQ"
#endif
#endif
                          }
    break;

  case 420:
/* Line 1787 of yacc.c  */
#line 2402 "parser.y"
    {
#ifdef HELP_AND_TEXT
			    outputMode(); sollyaPrintf(HELP_AND_TEXT);
#else
			    outputMode(); sollyaPrintf("Boolean and.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for AND"
#endif
#endif
                          }
    break;

  case 421:
/* Line 1787 of yacc.c  */
#line 2413 "parser.y"
    {
#ifdef HELP_OR_TEXT
			    outputMode(); sollyaPrintf(HELP_OR_TEXT);
#else
			    outputMode(); sollyaPrintf("Boolean or.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for OR"
#endif
#endif
                          }
    break;

  case 422:
/* Line 1787 of yacc.c  */
#line 2424 "parser.y"
    {
#ifdef HELP_PLUS_TEXT
			    outputMode(); sollyaPrintf(HELP_PLUS_TEXT);
#else
			    outputMode(); sollyaPrintf("Addition.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PLUS"
#endif
#endif
                          }
    break;

  case 423:
/* Line 1787 of yacc.c  */
#line 2435 "parser.y"
    {
#ifdef HELP_MINUS_TEXT
			    outputMode(); sollyaPrintf(HELP_MINUS_TEXT);
#else
			    outputMode(); sollyaPrintf("Substraction.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MINUS"
#endif
#endif
                          }
    break;

  case 424:
/* Line 1787 of yacc.c  */
#line 2446 "parser.y"
    {
#ifdef HELP_APPROX_TEXT
			    outputMode(); sollyaPrintf(HELP_APPROX_TEXT);
#else
			    outputMode(); sollyaPrintf("Floating-point approximation of a constant expression.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for APPROX"
#endif
#endif
                          }
    break;

  case 425:
/* Line 1787 of yacc.c  */
#line 2457 "parser.y"
    {
#ifdef HELP_MULT_TEXT
			    outputMode(); sollyaPrintf(HELP_MULT_TEXT);
#else
			    outputMode(); sollyaPrintf("Multiplication.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MULT"
#endif
#endif
                          }
    break;

  case 426:
/* Line 1787 of yacc.c  */
#line 2468 "parser.y"
    {
#ifdef HELP_DIVIDE_TEXT
			    outputMode(); sollyaPrintf(HELP_DIVIDE_TEXT);
#else
			    outputMode(); sollyaPrintf("Division.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIVIDE"
#endif
#endif
                          }
    break;

  case 427:
/* Line 1787 of yacc.c  */
#line 2479 "parser.y"
    {
#ifdef HELP_POWER_TEXT
			    outputMode(); sollyaPrintf(HELP_POWER_TEXT);
#else
			    outputMode(); sollyaPrintf("Exponentiation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for POWER"
#endif
#endif
                          }
    break;

  case 428:
/* Line 1787 of yacc.c  */
#line 2490 "parser.y"
    {
#ifdef HELP_SQRT_TEXT
			    outputMode(); sollyaPrintf(HELP_SQRT_TEXT);
#else
			    outputMode(); sollyaPrintf("Square root.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SQRT"
#endif
#endif
                          }
    break;

  case 429:
/* Line 1787 of yacc.c  */
#line 2501 "parser.y"
    {
#ifdef HELP_EXP_TEXT
			    outputMode(); sollyaPrintf(HELP_EXP_TEXT);
#else
			    outputMode(); sollyaPrintf("Exponential.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXP"
#endif
#endif
                          }
    break;

  case 430:
/* Line 1787 of yacc.c  */
#line 2512 "parser.y"
    {
#ifdef HELP_LOG_TEXT
			    outputMode(); sollyaPrintf(HELP_LOG_TEXT);
#else
			    outputMode(); sollyaPrintf("Natural logarithm.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LOG"
#endif
#endif
                          }
    break;

  case 431:
/* Line 1787 of yacc.c  */
#line 2523 "parser.y"
    {
#ifdef HELP_LOG2_TEXT
			    outputMode(); sollyaPrintf(HELP_LOG2_TEXT);
#else
			    outputMode(); sollyaPrintf("Logarithm in base 2.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LOG2"
#endif
#endif
                          }
    break;

  case 432:
/* Line 1787 of yacc.c  */
#line 2534 "parser.y"
    {
#ifdef HELP_LOG10_TEXT
			    outputMode(); sollyaPrintf(HELP_LOG10_TEXT);
#else
			    outputMode(); sollyaPrintf("Logarithm in base 10.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LOG10"
#endif
#endif
                          }
    break;

  case 433:
/* Line 1787 of yacc.c  */
#line 2545 "parser.y"
    {
#ifdef HELP_SIN_TEXT
			    outputMode(); sollyaPrintf(HELP_SIN_TEXT);
#else
			    outputMode(); sollyaPrintf("Sine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SIN"
#endif
#endif
                          }
    break;

  case 434:
/* Line 1787 of yacc.c  */
#line 2556 "parser.y"
    {
#ifdef HELP_COS_TEXT
			    outputMode(); sollyaPrintf(HELP_COS_TEXT);
#else
			    outputMode(); sollyaPrintf("Cosine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for COS"
#endif
#endif
                          }
    break;

  case 435:
/* Line 1787 of yacc.c  */
#line 2567 "parser.y"
    {
#ifdef HELP_TAN_TEXT
			    outputMode(); sollyaPrintf(HELP_TAN_TEXT);
#else
			    outputMode(); sollyaPrintf("Tangent.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TAN"
#endif
#endif
                          }
    break;

  case 436:
/* Line 1787 of yacc.c  */
#line 2578 "parser.y"
    {
#ifdef HELP_ASIN_TEXT
			    outputMode(); sollyaPrintf(HELP_ASIN_TEXT);
#else
			    outputMode(); sollyaPrintf("Arcsine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ASIN"
#endif
#endif
                          }
    break;

  case 437:
/* Line 1787 of yacc.c  */
#line 2589 "parser.y"
    {
#ifdef HELP_ACOS_TEXT
			    outputMode(); sollyaPrintf(HELP_ACOS_TEXT);
#else
			    outputMode(); sollyaPrintf("Arcosine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ACOS"
#endif
#endif
                          }
    break;

  case 438:
/* Line 1787 of yacc.c  */
#line 2600 "parser.y"
    {
#ifdef HELP_ATAN_TEXT
			    outputMode(); sollyaPrintf(HELP_ATAN_TEXT);
#else
			    outputMode(); sollyaPrintf("Arctangent.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ATAN"
#endif
#endif
                          }
    break;

  case 439:
/* Line 1787 of yacc.c  */
#line 2611 "parser.y"
    {
#ifdef HELP_SINH_TEXT
			    outputMode(); sollyaPrintf(HELP_SINH_TEXT);
#else
			    outputMode(); sollyaPrintf("Hyperbolic sine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SINH"
#endif
#endif
                          }
    break;

  case 440:
/* Line 1787 of yacc.c  */
#line 2622 "parser.y"
    {
#ifdef HELP_COSH_TEXT
			    outputMode(); sollyaPrintf(HELP_COSH_TEXT);
#else
			    outputMode(); sollyaPrintf("Hyperbolic cosine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for COSH"
#endif
#endif
                          }
    break;

  case 441:
/* Line 1787 of yacc.c  */
#line 2633 "parser.y"
    {
#ifdef HELP_TANH_TEXT
			    outputMode(); sollyaPrintf(HELP_TANH_TEXT);
#else
			    outputMode(); sollyaPrintf("Hyperbolic tangent.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TANH"
#endif
#endif
                          }
    break;

  case 442:
/* Line 1787 of yacc.c  */
#line 2644 "parser.y"
    {
#ifdef HELP_ASINH_TEXT
			    outputMode(); sollyaPrintf(HELP_ASINH_TEXT);
#else
			    outputMode(); sollyaPrintf("Area sine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ASINH"
#endif
#endif
                          }
    break;

  case 443:
/* Line 1787 of yacc.c  */
#line 2655 "parser.y"
    {
#ifdef HELP_ACOSH_TEXT
			    outputMode(); sollyaPrintf(HELP_ACOSH_TEXT);
#else
			    outputMode(); sollyaPrintf("Area cosine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ACOSH"
#endif
#endif
                          }
    break;

  case 444:
/* Line 1787 of yacc.c  */
#line 2666 "parser.y"
    {
#ifdef HELP_ATANH_TEXT
			    outputMode(); sollyaPrintf(HELP_ATANH_TEXT);
#else

			    outputMode(); sollyaPrintf("Area tangent.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ATANH"
#endif
#endif
                          }
    break;

  case 445:
/* Line 1787 of yacc.c  */
#line 2678 "parser.y"
    {
#ifdef HELP_ABS_TEXT
			    outputMode(); sollyaPrintf(HELP_ABS_TEXT);
#else
			    outputMode(); sollyaPrintf("Absolute value.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ABS"
#endif
#endif
                          }
    break;

  case 446:
/* Line 1787 of yacc.c  */
#line 2689 "parser.y"
    {
#ifdef HELP_ERF_TEXT
			    outputMode(); sollyaPrintf(HELP_ERF_TEXT);
#else
			    outputMode(); sollyaPrintf("Error function.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ERF"
#endif
#endif
                          }
    break;

  case 447:
/* Line 1787 of yacc.c  */
#line 2700 "parser.y"
    {
#ifdef HELP_ERFC_TEXT
			    outputMode(); sollyaPrintf(HELP_ERFC_TEXT);
#else
			    outputMode(); sollyaPrintf("Complementary error function.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ERFC"
#endif
#endif
                          }
    break;

  case 448:
/* Line 1787 of yacc.c  */
#line 2711 "parser.y"
    {
#ifdef HELP_LOG1P_TEXT
			    outputMode(); sollyaPrintf(HELP_LOG1P_TEXT);
#else
			    outputMode(); sollyaPrintf("Natural logarithm of 1 plus argument.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LOG1P"
#endif
#endif
                          }
    break;

  case 449:
/* Line 1787 of yacc.c  */
#line 2722 "parser.y"
    {
#ifdef HELP_EXPM1_TEXT
			    outputMode(); sollyaPrintf(HELP_EXPM1_TEXT);
#else
			    outputMode(); sollyaPrintf("Exponential of argument minus 1.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXPM1"
#endif
#endif
                          }
    break;

  case 450:
/* Line 1787 of yacc.c  */
#line 2733 "parser.y"
    {
#ifdef HELP_DOUBLE_TEXT
			    outputMode(); sollyaPrintf(HELP_DOUBLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Double precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DOUBLE"
#endif
#endif
                          }
    break;

  case 451:
/* Line 1787 of yacc.c  */
#line 2744 "parser.y"
    {
#ifdef HELP_SINGLE_TEXT
			    outputMode(); sollyaPrintf(HELP_SINGLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Single precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SINGLE"
#endif
#endif
                          }
    break;

  case 452:
/* Line 1787 of yacc.c  */
#line 2755 "parser.y"
    {
#ifdef HELP_QUAD_TEXT
			    outputMode(); sollyaPrintf(HELP_QUAD_TEXT);
#else
			    outputMode(); sollyaPrintf("Quad precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for QUAD"
#endif
#endif
                          }
    break;

  case 453:
/* Line 1787 of yacc.c  */
#line 2766 "parser.y"
    {
#ifdef HELP_HALFPRECISION_TEXT
			    outputMode(); sollyaPrintf(HELP_HALFPRECISION_TEXT);
#else
			    outputMode(); sollyaPrintf("Half-precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HALFPRECISION"
#endif
#endif
                          }
    break;

  case 454:
/* Line 1787 of yacc.c  */
#line 2777 "parser.y"
    {
#ifdef HELP_DOUBLEDOUBLE_TEXT
			    outputMode(); sollyaPrintf(HELP_DOUBLEDOUBLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Double-double precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DOUBLEDOUBLE"
#endif
#endif
                          }
    break;

  case 455:
/* Line 1787 of yacc.c  */
#line 2788 "parser.y"
    {
#ifdef HELP_TRIPLEDOUBLE_TEXT
			    outputMode(); sollyaPrintf(HELP_TRIPLEDOUBLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Triple-double precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TRIPLEDOUBLE"
#endif
#endif
                          }
    break;

  case 456:
/* Line 1787 of yacc.c  */
#line 2799 "parser.y"
    {
#ifdef HELP_DOUBLEEXTENDED_TEXT
			    outputMode(); sollyaPrintf(HELP_DOUBLEEXTENDED_TEXT);
#else
			    outputMode(); sollyaPrintf("Double-extended precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DOUBLEEXTENDED"
#endif
#endif
                          }
    break;

  case 457:
/* Line 1787 of yacc.c  */
#line 2810 "parser.y"
    {
#ifdef HELP_CEIL_TEXT
			    outputMode(); sollyaPrintf(HELP_CEIL_TEXT);
#else
			    outputMode(); sollyaPrintf("Ceiling.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CEIL"
#endif
#endif
                          }
    break;

  case 458:
/* Line 1787 of yacc.c  */
#line 2821 "parser.y"
    {
#ifdef HELP_FLOOR_TEXT
			    outputMode(); sollyaPrintf(HELP_FLOOR_TEXT);
#else
			    outputMode(); sollyaPrintf("Floor.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FLOOR"
#endif
#endif
                          }
    break;

  case 459:
/* Line 1787 of yacc.c  */
#line 2832 "parser.y"
    {
#ifdef HELP_NEARESTINT_TEXT
			    outputMode(); sollyaPrintf(HELP_NEARESTINT_TEXT);
#else
			    outputMode(); sollyaPrintf("Nearest integer with even tie cases rule.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for NEARESTINT"
#endif
#endif
                          }
    break;

  case 460:
/* Line 1787 of yacc.c  */
#line 2843 "parser.y"
    {
#ifdef HELP_HEAD_TEXT
			    outputMode(); sollyaPrintf(HELP_HEAD_TEXT);
#else
			    outputMode(); sollyaPrintf("Head of a list.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HEAD"
#endif
#endif
                          }
    break;

  case 461:
/* Line 1787 of yacc.c  */
#line 2854 "parser.y"
    {
#ifdef HELP_ROUNDCORRECTLY_TEXT
			    outputMode(); sollyaPrintf(HELP_ROUNDCORRECTLY_TEXT);
#else
			    outputMode(); sollyaPrintf("Round a bounding to the nearest floating-point value such that correct rounding is possible.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ROUNDCORRECTLY"
#endif
#endif
                          }
    break;

  case 462:
/* Line 1787 of yacc.c  */
#line 2865 "parser.y"
    {
#ifdef HELP_READFILE_TEXT
			    outputMode(); sollyaPrintf(HELP_READFILE_TEXT);
#else
			    outputMode(); sollyaPrintf("Reads a file into a string.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for READFILE"
#endif
#endif
                          }
    break;

  case 463:
/* Line 1787 of yacc.c  */
#line 2876 "parser.y"
    {
#ifdef HELP_REVERT_TEXT
			    outputMode(); sollyaPrintf(HELP_REVERT_TEXT);
#else
			    outputMode(); sollyaPrintf("Reverts a list that is not finally elliptic.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for REVERT"
#endif
#endif
                          }
    break;

  case 464:
/* Line 1787 of yacc.c  */
#line 2887 "parser.y"
    {
#ifdef HELP_SORT_TEXT
			    outputMode(); sollyaPrintf(HELP_SORT_TEXT);
#else
			    outputMode(); sollyaPrintf("Sorts a list of constants in ascending order.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SORT"
#endif
#endif
                          }
    break;

  case 465:
/* Line 1787 of yacc.c  */
#line 2898 "parser.y"
    {
#ifdef HELP_TAIL_TEXT
			    outputMode(); sollyaPrintf(HELP_TAIL_TEXT);
#else
			    outputMode(); sollyaPrintf("Tail of a list.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TAIL"
#endif
#endif
                          }
    break;

  case 466:
/* Line 1787 of yacc.c  */
#line 2909 "parser.y"
    {
#ifdef HELP_PREC_TEXT
			    outputMode(); sollyaPrintf(HELP_PREC_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable precision.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PREC"
#endif
#endif
                          }
    break;

  case 467:
/* Line 1787 of yacc.c  */
#line 2920 "parser.y"
    {
#ifdef HELP_POINTS_TEXT
			    outputMode(); sollyaPrintf(HELP_POINTS_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable number of points.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for POINTS"
#endif
#endif
                          }
    break;

  case 468:
/* Line 1787 of yacc.c  */
#line 2931 "parser.y"
    {
#ifdef HELP_DIAM_TEXT
			    outputMode(); sollyaPrintf(HELP_DIAM_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable diameter.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIAM"
#endif
#endif
                          }
    break;

  case 469:
/* Line 1787 of yacc.c  */
#line 2942 "parser.y"
    {
#ifdef HELP_DISPLAY_TEXT
			    outputMode(); sollyaPrintf(HELP_DISPLAY_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable display mode.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DISPLAY"
#endif
#endif
                          }
    break;

  case 470:
/* Line 1787 of yacc.c  */
#line 2953 "parser.y"
    {
#ifdef HELP_VERBOSITY_TEXT
			    outputMode(); sollyaPrintf(HELP_VERBOSITY_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable verbosity.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for VERBOSITY"
#endif
#endif
                          }
    break;

  case 471:
/* Line 1787 of yacc.c  */
#line 2964 "parser.y"
    {
#ifdef HELP_CANONICAL_TEXT
			    outputMode(); sollyaPrintf(HELP_CANONICAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable canonical output.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CANONICAL"
#endif
#endif
                          }
    break;

  case 472:
/* Line 1787 of yacc.c  */
#line 2975 "parser.y"
    {
#ifdef HELP_AUTOSIMPLIFY_TEXT
			    outputMode(); sollyaPrintf(HELP_AUTOSIMPLIFY_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable automatic simplification.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for AUTOSIMPLIFY"
#endif
#endif
                          }
    break;

  case 473:
/* Line 1787 of yacc.c  */
#line 2986 "parser.y"
    {
#ifdef HELP_TAYLORRECURSIONS_TEXT
			    outputMode(); sollyaPrintf(HELP_TAYLORRECURSIONS_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable recursions of Taylor evaluation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TAYLORRECURSIONS"
#endif
#endif
                          }
    break;

  case 474:
/* Line 1787 of yacc.c  */
#line 2997 "parser.y"
    {
#ifdef HELP_TIMING_TEXT
			    outputMode(); sollyaPrintf(HELP_TIMING_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable timing of computations.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TIMING"
#endif
#endif
                          }
    break;

  case 475:
/* Line 1787 of yacc.c  */
#line 3008 "parser.y"
    {
#ifdef HELP_TIME_TEXT
			    outputMode(); sollyaPrintf(HELP_TIME_TEXT);
#else
			    outputMode(); sollyaPrintf("High-level time procedure.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TIME"
#endif
#endif
                          }
    break;

  case 476:
/* Line 1787 of yacc.c  */
#line 3019 "parser.y"
    {
#ifdef HELP_FULLPARENTHESES_TEXT
			    outputMode(); sollyaPrintf(HELP_FULLPARENTHESES_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable fully parenthized mode.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FULLPARENTHESES"
#endif
#endif
                          }
    break;

  case 477:
/* Line 1787 of yacc.c  */
#line 3030 "parser.y"
    {
#ifdef HELP_MIDPOINTMODE_TEXT
			    outputMode(); sollyaPrintf(HELP_MIDPOINTMODE_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable midpoint mode.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MIDPOINTMODE"
#endif
#endif
                          }
    break;

  case 478:
/* Line 1787 of yacc.c  */
#line 3041 "parser.y"
    {
#ifdef HELP_DIEONERRORMODE_TEXT
			    outputMode(); sollyaPrintf(HELP_DIEONERRORMODE_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable for die-on-error mode.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIEONERRORMODE"
#endif
#endif
                          }
    break;

  case 479:
/* Line 1787 of yacc.c  */
#line 3052 "parser.y"
    {
#ifdef HELP_RATIONALMODE_TEXT
			    outputMode(); sollyaPrintf(HELP_RATIONALMODE_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable rational mode.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RATIONALMODE"
#endif
#endif
                          }
    break;

  case 480:
/* Line 1787 of yacc.c  */
#line 3063 "parser.y"
    {
#ifdef HELP_ROUNDINGWARNINGS_TEXT
			    outputMode(); sollyaPrintf(HELP_ROUNDINGWARNINGS_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable activating warnings about rounding.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ROUNDINGWARNINGS"
#endif
#endif
                          }
    break;

  case 481:
/* Line 1787 of yacc.c  */
#line 3074 "parser.y"
    {
#ifdef HELP_HOPITALRECURSIONS_TEXT
			    outputMode(); sollyaPrintf(HELP_HOPITALRECURSIONS_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable recursions of Hopital evaluation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HOPITALRECURSIONS"
#endif
#endif
                          }
    break;

  case 482:
/* Line 1787 of yacc.c  */
#line 3085 "parser.y"
    {
#ifdef HELP_ON_TEXT
			    outputMode(); sollyaPrintf(HELP_ON_TEXT);
#else
			    outputMode(); sollyaPrintf("Something is switched on.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ON"
#endif
#endif
                          }
    break;

  case 483:
/* Line 1787 of yacc.c  */
#line 3096 "parser.y"
    {
#ifdef HELP_OFF_TEXT
			    outputMode(); sollyaPrintf(HELP_OFF_TEXT);
#else
			    outputMode(); sollyaPrintf("Something is switched off.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for OFF"
#endif
#endif
                          }
    break;

  case 484:
/* Line 1787 of yacc.c  */
#line 3107 "parser.y"
    {
#ifdef HELP_DYADIC_TEXT
			    outputMode(); sollyaPrintf(HELP_DYADIC_TEXT);
#else
			    outputMode(); sollyaPrintf("Display mode is dyadic output.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DYADIC"
#endif
#endif
                          }
    break;

  case 485:
/* Line 1787 of yacc.c  */
#line 3118 "parser.y"
    {
#ifdef HELP_POWERS_TEXT
			    outputMode(); sollyaPrintf(HELP_POWERS_TEXT);
#else
			    outputMode(); sollyaPrintf("Display mode is dyadic output with powers.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for POWERS"
#endif
#endif
                          }
    break;

  case 486:
/* Line 1787 of yacc.c  */
#line 3129 "parser.y"
    {
#ifdef HELP_BINARY_TEXT
			    outputMode(); sollyaPrintf(HELP_BINARY_TEXT);
#else
			    outputMode(); sollyaPrintf("Display mode is binary.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for BINARY"
#endif
#endif
                          }
    break;

  case 487:
/* Line 1787 of yacc.c  */
#line 3140 "parser.y"
    {
#ifdef HELP_HEXADECIMAL_TEXT
			    outputMode(); sollyaPrintf(HELP_HEXADECIMAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Display mode is hexadecimal.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HEXADECIMAL"
#endif
#endif
                          }
    break;

  case 488:
/* Line 1787 of yacc.c  */
#line 3151 "parser.y"
    {
#ifdef HELP_FILE_TEXT
			    outputMode(); sollyaPrintf(HELP_FILE_TEXT);
#else
			    outputMode(); sollyaPrintf("A file will be specified.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FILE"
#endif
#endif
                          }
    break;

  case 489:
/* Line 1787 of yacc.c  */
#line 3162 "parser.y"
    {
#ifdef HELP_POSTSCRIPT_TEXT
			    outputMode(); sollyaPrintf(HELP_POSTSCRIPT_TEXT);
#else
			    outputMode(); sollyaPrintf("A postscript file will be specified.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for POSTSCRIPT"
#endif
#endif
                          }
    break;

  case 490:
/* Line 1787 of yacc.c  */
#line 3173 "parser.y"
    {
#ifdef HELP_POSTSCRIPTFILE_TEXT
			    outputMode(); sollyaPrintf(HELP_POSTSCRIPTFILE_TEXT);
#else
			    outputMode(); sollyaPrintf("A postscript file and a file will be specified.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for POSTSCRIPTFILE"
#endif
#endif
                          }
    break;

  case 491:
/* Line 1787 of yacc.c  */
#line 3184 "parser.y"
    {
#ifdef HELP_PERTURB_TEXT
			    outputMode(); sollyaPrintf(HELP_PERTURB_TEXT);
#else
			    outputMode(); sollyaPrintf("Perturbation is demanded.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PERTURB"
#endif
#endif
                          }
    break;

  case 492:
/* Line 1787 of yacc.c  */
#line 3195 "parser.y"
    {
#ifdef HELP_RD_TEXT
			    outputMode(); sollyaPrintf(HELP_RD_TEXT);
#else
			    outputMode(); sollyaPrintf("Round towards minus infinity.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RD"
#endif
#endif
                          }
    break;

  case 493:
/* Line 1787 of yacc.c  */
#line 3206 "parser.y"
    {
#ifdef HELP_RU_TEXT
			    outputMode(); sollyaPrintf(HELP_RU_TEXT);
#else
			    outputMode(); sollyaPrintf("Round towards plus infinity.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RU"
#endif
#endif
                          }
    break;

  case 494:
/* Line 1787 of yacc.c  */
#line 3217 "parser.y"
    {
#ifdef HELP_RZ_TEXT
			    outputMode(); sollyaPrintf(HELP_RZ_TEXT);
#else
			    outputMode(); sollyaPrintf("Round towards zero.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RZ"
#endif
#endif
                          }
    break;

  case 495:
/* Line 1787 of yacc.c  */
#line 3228 "parser.y"
    {
#ifdef HELP_RN_TEXT
			    outputMode(); sollyaPrintf(HELP_RN_TEXT);
#else
			    outputMode(); sollyaPrintf("Round to nearest.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RN"
#endif
#endif
                          }
    break;

  case 496:
/* Line 1787 of yacc.c  */
#line 3239 "parser.y"
    {
#ifdef HELP_HONORCOEFFPREC_TEXT
			    outputMode(); sollyaPrintf(HELP_HONORCOEFFPREC_TEXT);
#else
			    outputMode(); sollyaPrintf("Honorate the precision of the coefficients.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HONORCOEFFPREC"
#endif
#endif
                          }
    break;

  case 497:
/* Line 1787 of yacc.c  */
#line 3250 "parser.y"
    {
#ifdef HELP_TRUE_TEXT
			    outputMode(); sollyaPrintf(HELP_TRUE_TEXT);
#else
			    outputMode(); sollyaPrintf("Boolean constant true.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TRUE"
#endif
#endif
                          }
    break;

  case 498:
/* Line 1787 of yacc.c  */
#line 3261 "parser.y"
    {
#ifdef HELP_FALSE_TEXT
			    outputMode(); sollyaPrintf(HELP_FALSE_TEXT);
#else
			    outputMode(); sollyaPrintf("Boolean constant false.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FALSE"
#endif
#endif
                          }
    break;

  case 499:
/* Line 1787 of yacc.c  */
#line 3272 "parser.y"
    {
#ifdef HELP_DEFAULT_TEXT
			    outputMode(); sollyaPrintf(HELP_DEFAULT_TEXT);
#else
			    outputMode(); sollyaPrintf("Default value.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DEFAULT"
#endif
#endif
                          }
    break;

  case 500:
/* Line 1787 of yacc.c  */
#line 3283 "parser.y"
    {
#ifdef HELP_MATCH_TEXT
			    outputMode(); sollyaPrintf(HELP_MATCH_TEXT);
#else
			    outputMode(); sollyaPrintf("match ... with ... construct.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MATCH"
#endif
#endif
                          }
    break;

  case 501:
/* Line 1787 of yacc.c  */
#line 3294 "parser.y"
    {
#ifdef HELP_WITH_TEXT
			    outputMode(); sollyaPrintf(HELP_WITH_TEXT);
#else
			    outputMode(); sollyaPrintf("match ... with ... construct.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for WITH"
#endif
#endif
                          }
    break;

  case 502:
/* Line 1787 of yacc.c  */
#line 3305 "parser.y"
    {
#ifdef HELP_ABSOLUTE_TEXT
			    outputMode(); sollyaPrintf(HELP_ABSOLUTE_TEXT);
#else
			    outputMode(); sollyaPrintf("Consider an absolute error.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ABSOLUTE"
#endif
#endif
                          }
    break;

  case 503:
/* Line 1787 of yacc.c  */
#line 3316 "parser.y"
    {
#ifdef HELP_DECIMAL_TEXT
			    outputMode(); sollyaPrintf(HELP_DECIMAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Display mode is decimal.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DECIMAL"
#endif
#endif
                          }
    break;

  case 504:
/* Line 1787 of yacc.c  */
#line 3327 "parser.y"
    {
#ifdef HELP_RELATIVE_TEXT
			    outputMode(); sollyaPrintf(HELP_RELATIVE_TEXT);
#else
			    outputMode(); sollyaPrintf("Consider a relative error.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RELATIVE"
#endif
#endif
                          }
    break;

  case 505:
/* Line 1787 of yacc.c  */
#line 3338 "parser.y"
    {
#ifdef HELP_FIXED_TEXT
			    outputMode(); sollyaPrintf(HELP_FIXED_TEXT);
#else
			    outputMode(); sollyaPrintf("Consider fixed-point numbers.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FIXED"
#endif
#endif
                          }
    break;

  case 506:
/* Line 1787 of yacc.c  */
#line 3349 "parser.y"
    {
#ifdef HELP_FLOATING_TEXT
			    outputMode(); sollyaPrintf(HELP_FLOATING_TEXT);
#else
			    outputMode(); sollyaPrintf("Consider floating-point numbers.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FLOATING"
#endif
#endif
                          }
    break;

  case 507:
/* Line 1787 of yacc.c  */
#line 3360 "parser.y"
    {
#ifdef HELP_ERROR_TEXT
			    outputMode(); sollyaPrintf(HELP_ERROR_TEXT);
#else
			    outputMode(); sollyaPrintf("Type error meta-value.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ERROR"
#endif
#endif
                          }
    break;

  case 508:
/* Line 1787 of yacc.c  */
#line 3371 "parser.y"
    {
#ifdef HELP_QUIT_TEXT
			    outputMode(); sollyaPrintf(HELP_QUIT_TEXT);
#else
			    outputMode(); sollyaPrintf("Exit from the tool.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for QUIT"
#endif
#endif
                          }
    break;

  case 509:
/* Line 1787 of yacc.c  */
#line 3382 "parser.y"
    {
#ifdef HELP_QUIT_TEXT
			    outputMode(); sollyaPrintf(HELP_QUIT_TEXT);
#else
			    outputMode(); sollyaPrintf("Exit from the tool - help is called inside a read macro.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for QUIT"
#endif
#endif
                          }
    break;

  case 510:
/* Line 1787 of yacc.c  */
#line 3393 "parser.y"
    {
#ifdef HELP_RESTART_TEXT
			    outputMode(); sollyaPrintf(HELP_RESTART_TEXT);
#else
			    outputMode(); sollyaPrintf("Restart the tool.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RESTART"
#endif
#endif
                          }
    break;

  case 511:
/* Line 1787 of yacc.c  */
#line 3404 "parser.y"
    {
#ifdef HELP_LIBRARY_TEXT
			    outputMode(); sollyaPrintf(HELP_LIBRARY_TEXT);
#else
			    outputMode(); sollyaPrintf("Library binding dereferencer.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LIBRARY"
#endif
#endif
                          }
    break;

  case 512:
/* Line 1787 of yacc.c  */
#line 3415 "parser.y"
    {
#ifdef HELP_LIBRARYCONSTANT_TEXT
			    outputMode(); sollyaPrintf(HELP_LIBRARYCONSTANT_TEXT);
#else
			    outputMode(); sollyaPrintf("Library constant binding dereferencer.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LIBRARYCONSTANT"
#endif
#endif
                          }
    break;

  case 513:
/* Line 1787 of yacc.c  */
#line 3426 "parser.y"
    {
#ifdef HELP_DIFF_TEXT
			    outputMode(); sollyaPrintf(HELP_DIFF_TEXT);
#else
			    outputMode(); sollyaPrintf("Differentiation: diff(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIFF"
#endif
#endif
                          }
    break;

  case 514:
/* Line 1787 of yacc.c  */
#line 3437 "parser.y"
    {
#ifdef HELP_BASHEVALUATE_TEXT
			    outputMode(); sollyaPrintf(HELP_BASHEVALUATE_TEXT);
#else
			    outputMode(); sollyaPrintf("Executes a string as a bash command and returns the output as a string.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for BASHEVALUATE"
#endif
#endif
                          }
    break;

  case 515:
/* Line 1787 of yacc.c  */
#line 3448 "parser.y"
    {
#ifdef HELP_SIMPLIFY_TEXT
			    outputMode(); sollyaPrintf(HELP_SIMPLIFY_TEXT);
#else
			    outputMode(); sollyaPrintf("Simplify: simplify(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SIMPLIFY"
#endif
#endif
                          }
    break;

  case 516:
/* Line 1787 of yacc.c  */
#line 3459 "parser.y"
    {
#ifdef HELP_REMEZ_TEXT
			    outputMode(); sollyaPrintf(HELP_REMEZ_TEXT);
#else
			    outputMode(); sollyaPrintf("Remez: remez(func,degree|monoms,range[,weight[,quality]]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for REMEZ"
#endif
#endif
                          }
    break;

  case 517:
/* Line 1787 of yacc.c  */
#line 3470 "parser.y"
    {
#ifdef HELP_MIN_TEXT
			    outputMode(); sollyaPrintf(HELP_MIN_TEXT);
#else
			    outputMode(); sollyaPrintf("min(val1,val2,...,valn): computes the minimum of the constant expressions vali.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MIN"
#endif
#endif
                          }
    break;

  case 518:
/* Line 1787 of yacc.c  */
#line 3481 "parser.y"
    {
#ifdef HELP_MAX_TEXT
			    outputMode(); sollyaPrintf(HELP_MAX_TEXT);
#else
			    outputMode(); sollyaPrintf("max(val1,val2,...,valn): computes the maximum of the constant expressions vali.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MAX"
#endif
#endif
                          }
    break;

  case 519:
/* Line 1787 of yacc.c  */
#line 3492 "parser.y"
    {
#ifdef HELP_FPMINIMAX_TEXT
			    outputMode(); sollyaPrintf(HELP_FPMINIMAX_TEXT);
#else
			    outputMode(); sollyaPrintf("Fpminimax: fpminimax(func,degree|monoms,formats,range|pointslist[,absolute|relative[,fixed|floating[,constrainedPart[, minimaxpoly]]]]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FPMINIMAX"
#endif
#endif
                          }
    break;

  case 520:
/* Line 1787 of yacc.c  */
#line 3503 "parser.y"
    {
#ifdef HELP_HORNER_TEXT
			    outputMode(); sollyaPrintf(HELP_HORNER_TEXT);
#else
			    outputMode(); sollyaPrintf("Horner: horner(func)\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HORNER"
#endif
#endif
                          }
    break;

  case 521:
/* Line 1787 of yacc.c  */
#line 3514 "parser.y"
    {
#ifdef HELP_EXPAND_TEXT
			    outputMode(); sollyaPrintf(HELP_EXPAND_TEXT);
#else
			    outputMode(); sollyaPrintf("Expand: expand(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXPAND"
#endif
#endif
                          }
    break;

  case 522:
/* Line 1787 of yacc.c  */
#line 3525 "parser.y"
    {
#ifdef HELP_SIMPLIFYSAFE_TEXT
			    outputMode(); sollyaPrintf(HELP_SIMPLIFYSAFE_TEXT);
#else
			    outputMode(); sollyaPrintf("Safe simplification: simplifysafe(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SIMPLIFYSAFE"
#endif
#endif
                          }
    break;

  case 523:
/* Line 1787 of yacc.c  */
#line 3536 "parser.y"
    {
#ifdef HELP_TAYLOR_TEXT
			    outputMode(); sollyaPrintf(HELP_TAYLOR_TEXT);
#else
			    outputMode(); sollyaPrintf("Taylor expansion: taylor(func,degree,point).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TAYLOR"
#endif
#endif
                          }
    break;

  case 524:
/* Line 1787 of yacc.c  */
#line 3547 "parser.y"
    {
#ifdef HELP_TAYLORFORM_TEXT
			    outputMode(); sollyaPrintf(HELP_TAYLORFORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Taylor form computation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TAYLORFORM"
#endif
#endif
                          }
    break;

  case 525:
/* Line 1787 of yacc.c  */
#line 3558 "parser.y"
    {
#ifdef HELP_AUTODIFF_TEXT
			    outputMode(); sollyaPrintf(HELP_AUTODIFF_TEXT);
#else
			    outputMode(); sollyaPrintf("Automatic differentiation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for AUTODIFF"
#endif
#endif
                          }
    break;

  case 526:
/* Line 1787 of yacc.c  */
#line 3569 "parser.y"
    {
#ifdef HELP_DEGREE_TEXT
			    outputMode(); sollyaPrintf(HELP_DEGREE_TEXT);
#else
			    outputMode(); sollyaPrintf("Degree of a polynomial: degree(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DEGREE"
#endif
#endif
                          }
    break;

  case 527:
/* Line 1787 of yacc.c  */
#line 3580 "parser.y"
    {
#ifdef HELP_NUMERATOR_TEXT
			    outputMode(); sollyaPrintf(HELP_NUMERATOR_TEXT);
#else
			    outputMode(); sollyaPrintf("Numerator of an expression: numerator(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for NUMERATOR"
#endif
#endif
                          }
    break;

  case 528:
/* Line 1787 of yacc.c  */
#line 3591 "parser.y"
    {
#ifdef HELP_DENOMINATOR_TEXT
			    outputMode(); sollyaPrintf(HELP_DENOMINATOR_TEXT);
#else
			    outputMode(); sollyaPrintf("Denominator of an expression: denominator(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DENOMINATOR"
#endif
#endif
                          }
    break;

  case 529:
/* Line 1787 of yacc.c  */
#line 3602 "parser.y"
    {
#ifdef HELP_SUBSTITUTE_TEXT
			    outputMode(); sollyaPrintf(HELP_SUBSTITUTE_TEXT);
#else
			    outputMode(); sollyaPrintf("Substitute func2 for free variable in func: substitute(func,func2).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SUBSTITUTE"
#endif
#endif
                          }
    break;

  case 530:
/* Line 1787 of yacc.c  */
#line 3613 "parser.y"
    {
#ifdef HELP_COEFF_TEXT
			    outputMode(); sollyaPrintf(HELP_COEFF_TEXT);
#else
			    outputMode(); sollyaPrintf("i-th coefficient of a polynomial: coeff(func,degree).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for COEFF"
#endif
#endif
                          }
    break;

  case 531:
/* Line 1787 of yacc.c  */
#line 3624 "parser.y"
    {
#ifdef HELP_SUBPOLY_TEXT
			    outputMode(); sollyaPrintf(HELP_SUBPOLY_TEXT);
#else
			    outputMode(); sollyaPrintf("Subpolynomial consisting in monomials: subpoly(func,list of degrees).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SUBPOLY"
#endif
#endif
                          }
    break;

  case 532:
/* Line 1787 of yacc.c  */
#line 3635 "parser.y"
    {
#ifdef HELP_ROUNDCOEFFICIENTS_TEXT
			    outputMode(); sollyaPrintf(HELP_ROUNDCOEFFICIENTS_TEXT);
#else
			    outputMode(); sollyaPrintf("Round coefficients of a polynomial to format: roundcoefficients(func,list of formats).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ROUNDCOEFFICIENTS"
#endif
#endif
                          }
    break;

  case 533:
/* Line 1787 of yacc.c  */
#line 3646 "parser.y"
    {
#ifdef HELP_RATIONALAPPROX_TEXT
			    outputMode(); sollyaPrintf(HELP_RATIONALAPPROX_TEXT);
#else
			    outputMode(); sollyaPrintf("Rational approximation: rationalapprox(constant).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RATIONALAPPROX"
#endif
#endif
                          }
    break;

  case 534:
/* Line 1787 of yacc.c  */
#line 3657 "parser.y"
    {
#ifdef HELP_ACCURATEINFNORM_TEXT
			    outputMode(); sollyaPrintf(HELP_ACCURATEINFNORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Faithful rounded infinity norm: accurateinfnorm(func,bits,range,domains to exclude).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ACCURATEINFNORM"
#endif
#endif
                          }
    break;

  case 535:
/* Line 1787 of yacc.c  */
#line 3668 "parser.y"
    {
#ifdef HELP_ROUND_TEXT
			    outputMode(); sollyaPrintf(HELP_ROUND_TEXT);
#else
			    outputMode(); sollyaPrintf("Round to a given format: round(constant,precision,rounding mode).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ROUND"
#endif
#endif
                          }
    break;

  case 536:
/* Line 1787 of yacc.c  */
#line 3679 "parser.y"
    {
#ifdef HELP_EVALUATE_TEXT
			    outputMode(); sollyaPrintf(HELP_EVALUATE_TEXT);
#else
			    outputMode(); sollyaPrintf("Evaluate a function in a point or interval: round(func,constant|range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EVALUATE"
#endif
#endif
                          }
    break;

  case 537:
/* Line 1787 of yacc.c  */
#line 3690 "parser.y"
    {
#ifdef HELP_LENGTH_TEXT
			    outputMode(); sollyaPrintf(HELP_LENGTH_TEXT);
#else
			    outputMode(); sollyaPrintf("Length of a list: length(list).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LENGTH"
#endif
#endif
                          }
    break;

  case 538:
/* Line 1787 of yacc.c  */
#line 3701 "parser.y"
    {
#ifdef HELP_PARSE_TEXT
			    outputMode(); sollyaPrintf(HELP_PARSE_TEXT);
#else
			    outputMode(); sollyaPrintf("Parse a string to function: parse(string).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PARSE"
#endif
#endif
                          }
    break;

  case 539:
/* Line 1787 of yacc.c  */
#line 3712 "parser.y"
    {
#ifdef HELP_PRINT_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINT_TEXT);
#else
			    outputMode(); sollyaPrintf("Print something: print(thing1, thing2, ...).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINT"
#endif
#endif
                          }
    break;

  case 540:
/* Line 1787 of yacc.c  */
#line 3723 "parser.y"
    {
#ifdef HELP_PRINTXML_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINTXML_TEXT);
#else
			    outputMode(); sollyaPrintf("Print a function in XML: printxml(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINTXML"
#endif
#endif
                          }
    break;

  case 541:
/* Line 1787 of yacc.c  */
#line 3734 "parser.y"
    {
#ifdef HELP_READXML_TEXT
			    outputMode(); sollyaPrintf(HELP_READXML_TEXT);
#else
			    outputMode(); sollyaPrintf("Reads a function in XML: readxml(filename).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for READXML"
#endif
#endif
                          }
    break;

  case 542:
/* Line 1787 of yacc.c  */
#line 3745 "parser.y"
    {
#ifdef HELP_PLOT_TEXT
			    outputMode(); sollyaPrintf(HELP_PLOT_TEXT);
#else
			    outputMode(); sollyaPrintf("Plot (a) function(s) in a range: plot(func,func2,...,range).\n");
			    outputMode(); sollyaPrintf("There are further options.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PLOT"
#endif
#endif
                          }
    break;

  case 543:
/* Line 1787 of yacc.c  */
#line 3757 "parser.y"
    {
#ifdef HELP_PRINTHEXA_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINTHEXA_TEXT);
#else
			    outputMode(); sollyaPrintf("Print a constant in hexadecimal: printhexa(constant).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINTHEXA"
#endif
#endif
                          }
    break;

  case 544:
/* Line 1787 of yacc.c  */
#line 3768 "parser.y"
    {
#ifdef HELP_PRINTFLOAT_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINTFLOAT_TEXT);
#else
			    outputMode(); sollyaPrintf("Print a constant in hexadecimal simple precision: printfloat(constant).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINTFLOAT"
#endif
#endif
                          }
    break;

  case 545:
/* Line 1787 of yacc.c  */
#line 3779 "parser.y"
    {
#ifdef HELP_PRINTBINARY_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINTBINARY_TEXT);
#else
			    outputMode(); sollyaPrintf("Print a constant in binary: printbinary(constant).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINTBINARY"
#endif
#endif
                          }
    break;

  case 546:
/* Line 1787 of yacc.c  */
#line 3790 "parser.y"
    {
#ifdef HELP_PRINTEXPANSION_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINTEXPANSION_TEXT);
#else
			    outputMode(); sollyaPrintf("Print a polynomial as an expansion of double precision numbers: printexpansion(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINTEXPANSION"
#endif
#endif
                          }
    break;

  case 547:
/* Line 1787 of yacc.c  */
#line 3801 "parser.y"
    {
#ifdef HELP_BASHEXECUTE_TEXT
			    outputMode(); sollyaPrintf(HELP_BASHEXECUTE_TEXT);
#else
			    outputMode(); sollyaPrintf("Execute a command in a shell: bashexecute(string).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for BASHEXECUTE"
#endif
#endif
                          }
    break;

  case 548:
/* Line 1787 of yacc.c  */
#line 3812 "parser.y"
    {
#ifdef HELP_EXTERNALPLOT_TEXT
			    outputMode(); sollyaPrintf(HELP_EXTERNALPLOT_TEXT);
#else
			    outputMode(); sollyaPrintf("Here should be some help text.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXTERNALPLOT"
#endif
#endif
                          }
    break;

  case 549:
/* Line 1787 of yacc.c  */
#line 3823 "parser.y"
    {
#ifdef HELP_WRITE_TEXT
			    outputMode(); sollyaPrintf(HELP_WRITE_TEXT);
#else
			    outputMode(); sollyaPrintf("Write something without adding spaces and newlines: write(thing1, thing2, ...).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for WRITE"
#endif
#endif
                          }
    break;

  case 550:
/* Line 1787 of yacc.c  */
#line 3834 "parser.y"
    {
#ifdef HELP_ASCIIPLOT_TEXT
			    outputMode(); sollyaPrintf(HELP_ASCIIPLOT_TEXT);
#else
			    outputMode(); sollyaPrintf("Plot a function in a range using an ASCII terminal: asciiplot(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ASCIIPLOT"
#endif
#endif
                          }
    break;

  case 551:
/* Line 1787 of yacc.c  */
#line 3845 "parser.y"
    {
#ifdef HELP_RENAME_TEXT
			    outputMode(); sollyaPrintf(HELP_RENAME_TEXT);
#else
			    outputMode(); sollyaPrintf("Rename free variable string1 to string2: rename(string1, string2).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RENAME"
#endif
#endif
                          }
    break;

  case 552:
/* Line 1787 of yacc.c  */
#line 3856 "parser.y"
    {
#ifdef HELP_INFNORM_TEXT
			    outputMode(); sollyaPrintf(HELP_INFNORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Certified infinity norm: infnorm(func,range[,prooffile[,list of funcs]]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for INFNORM"
#endif
#endif
                          }
    break;

  case 553:
/* Line 1787 of yacc.c  */
#line 3867 "parser.y"
    {
#ifdef HELP_SUPNORM_TEXT
			    outputMode(); sollyaPrintf(HELP_SUPNORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Validated supremum norm: supnorm(poly,func,range,mode,accuracy).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SUPNORM"
#endif
#endif
                          }
    break;

  case 554:
/* Line 1787 of yacc.c  */
#line 3878 "parser.y"
    {
#ifdef HELP_FINDZEROS_TEXT
			    outputMode(); sollyaPrintf(HELP_FINDZEROS_TEXT);
#else
			    outputMode(); sollyaPrintf("Certified bounding of zeros: findzeros(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FINDZEROS"
#endif
#endif
                          }
    break;

  case 555:
/* Line 1787 of yacc.c  */
#line 3889 "parser.y"
    {
#ifdef HELP_FPFINDZEROS_TEXT
			    outputMode(); sollyaPrintf(HELP_FPFINDZEROS_TEXT);
#else
			    outputMode(); sollyaPrintf("Approximate zeros of a function: fpfindzeros(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FPFINDZEROS"
#endif
#endif
                          }
    break;

  case 556:
/* Line 1787 of yacc.c  */
#line 3900 "parser.y"
    {
#ifdef HELP_DIRTYINFNORM_TEXT
			    outputMode(); sollyaPrintf(HELP_DIRTYINFNORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Floating-point infinity norm: dirtyinfnorm(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIRTYINFNORM"
#endif
#endif
                          }
    break;

  case 557:
/* Line 1787 of yacc.c  */
#line 3911 "parser.y"
    {
#ifdef HELP_NUMBERROOTS_TEXT
			    outputMode(); sollyaPrintf(HELP_NUMBERROOTS_TEXT);
#else
			    outputMode(); sollyaPrintf("Computes the number of real roots of a polynomial on a domain.\n");
#endif
                          }
    break;

  case 558:
/* Line 1787 of yacc.c  */
#line 3919 "parser.y"
    {
#ifdef HELP_INTEGRAL_TEXT
			    outputMode(); sollyaPrintf(HELP_INTEGRAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Certified integral: integral(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for INTEGRAL"
#endif
#endif
                          }
    break;

  case 559:
/* Line 1787 of yacc.c  */
#line 3930 "parser.y"
    {
#ifdef HELP_DIRTYINTEGRAL_TEXT
			    outputMode(); sollyaPrintf(HELP_DIRTYINTEGRAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Floating-point integral: dirtyintegral(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIRTYINTEGRAL"
#endif
#endif
                          }
    break;

  case 560:
/* Line 1787 of yacc.c  */
#line 3941 "parser.y"
    {
#ifdef HELP_WORSTCASE_TEXT
			    outputMode(); sollyaPrintf(HELP_WORSTCASE_TEXT);
#else
			    outputMode(); sollyaPrintf("Print all worst-cases under a certain bound: worstcase(func,constant,range,constant,constant[,file]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for WORSTCASE"
#endif
#endif
                          }
    break;

  case 561:
/* Line 1787 of yacc.c  */
#line 3952 "parser.y"
    {
#ifdef HELP_IMPLEMENTPOLY_TEXT
			    outputMode(); sollyaPrintf(HELP_IMPLEMENTPOLY_TEXT);
#else
			    outputMode(); sollyaPrintf("Implement a polynomial in C: implementpoly(func,range,constant,format,string,string2[,honorcoeffprec[,string3]]).\n");
			    outputMode(); sollyaPrintf("Implements func in range with error constant with entering format named in function\nstring writing to file string2 honoring the precision of the coefficients or not with a proof in file string3.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for IMPLEMENTPOLY"
#endif
#endif
			  }
    break;

  case 562:
/* Line 1787 of yacc.c  */
#line 3964 "parser.y"
    {
#ifdef HELP_IMPLEMENTCONSTANT_TEXT
			    outputMode(); sollyaPrintf(HELP_IMPLEMENTCONSTANT_TEXT);
#else
			    outputMode(); sollyaPrintf("Implement a constant expression in arbitrary precision with MPFR: implementconstant(constant)\n");
			    outputMode(); sollyaPrintf("Generates code able to evaluate the given constant at any precision, with a guaranteed error.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for IMPLEMENTCONST"
#endif
#endif
                          }
    break;

  case 563:
/* Line 1787 of yacc.c  */
#line 3976 "parser.y"
    {
#ifdef HELP_CHECKINFNORM_TEXT
			    outputMode(); sollyaPrintf(HELP_CHECKINFNORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Checks whether an infinity norm is bounded: checkinfnorm(func,range,constant).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CHECKINFNORM"
#endif
#endif
                          }
    break;

  case 564:
/* Line 1787 of yacc.c  */
#line 3987 "parser.y"
    {
#ifdef HELP_ZERODENOMINATORS_TEXT
			    outputMode(); sollyaPrintf(HELP_ZERODENOMINATORS_TEXT);
#else
			    outputMode(); sollyaPrintf("Searches floating-point approximations to zeros of denominators: zerodenominators(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ZERODENOMINATORS"
#endif
#endif
                          }
    break;

  case 565:
/* Line 1787 of yacc.c  */
#line 3998 "parser.y"
    {
#ifdef HELP_ISEVALUABLE_TEXT
			    outputMode(); sollyaPrintf(HELP_ISEVALUABLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Tests if func is evaluable on range: isevaluable(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ISEVALUABLE"
#endif
#endif
                          }
    break;

  case 566:
/* Line 1787 of yacc.c  */
#line 4009 "parser.y"
    {
#ifdef HELP_SEARCHGAL_TEXT
			    outputMode(); sollyaPrintf(HELP_SEARCHGAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Searches Gal values for func (or list of func): searchgal(func|list of func, constant, integer, integer, format|list of formats, constant|list of constants).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SEARCHGAL"
#endif
#endif
                          }
    break;

  case 567:
/* Line 1787 of yacc.c  */
#line 4020 "parser.y"
    {
#ifdef HELP_GUESSDEGREE_TEXT
			    outputMode(); sollyaPrintf(HELP_GUESSDEGREE_TEXT);
#else
			    outputMode(); sollyaPrintf("Guesses the degree needed for approximating func: guessdegree(func,range,constant[,weight]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for GUESSDEGREE"
#endif
#endif
                          }
    break;

  case 568:
/* Line 1787 of yacc.c  */
#line 4031 "parser.y"
    {
#ifdef HELP_DIRTYFINDZEROS_TEXT
			    outputMode(); sollyaPrintf(HELP_DIRTYFINDZEROS_TEXT);
#else
			    outputMode(); sollyaPrintf("Finds zeros of a function dirtily: dirtyfindzeros(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIRTYFINDZEROS"
#endif
#endif
                          }
    break;

  case 569:
/* Line 1787 of yacc.c  */
#line 4042 "parser.y"
    {
			    outputMode(); sollyaPrintf("If construct: if condition then command or if condition then command else command.\n");
                          }
    break;

  case 570:
/* Line 1787 of yacc.c  */
#line 4046 "parser.y"
    {
			    outputMode(); sollyaPrintf("If construct: if condition then command or if condition then command else command.\n");
                          }
    break;

  case 571:
/* Line 1787 of yacc.c  */
#line 4050 "parser.y"
    {
			    outputMode(); sollyaPrintf("If construct: if condition then command else command\n");
                          }
    break;

  case 572:
/* Line 1787 of yacc.c  */
#line 4054 "parser.y"
    {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command\nor for i in list do command.\n");
                          }
    break;

  case 573:
/* Line 1787 of yacc.c  */
#line 4058 "parser.y"
    {
#ifdef HELP_IN_TEXT
			    outputMode(); sollyaPrintf(HELP_IN_TEXT);
#else
			    outputMode(); sollyaPrintf("In construct: for in construct and containment operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for IN"
#endif
#endif
                          }
    break;

  case 574:
/* Line 1787 of yacc.c  */
#line 4069 "parser.y"
    {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command.\n");
                          }
    break;

  case 575:
/* Line 1787 of yacc.c  */
#line 4073 "parser.y"
    {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command.\n");
                          }
    break;

  case 576:
/* Line 1787 of yacc.c  */
#line 4077 "parser.y"
    {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 by const3 do command.\n");
                          }
    break;

  case 577:
/* Line 1787 of yacc.c  */
#line 4081 "parser.y"
    {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command.\n");
			    outputMode(); sollyaPrintf("While construct: while condition do command.\n");
                          }
    break;

  case 578:
/* Line 1787 of yacc.c  */
#line 4086 "parser.y"
    {
			    outputMode(); sollyaPrintf("Begin-end construct: begin command; command; ... end.\n");
                          }
    break;

  case 579:
/* Line 1787 of yacc.c  */
#line 4090 "parser.y"
    {
			    outputMode(); sollyaPrintf("Begin-end construct: begin command; command; ... end.\n");
                          }
    break;

  case 580:
/* Line 1787 of yacc.c  */
#line 4094 "parser.y"
    {
			    outputMode(); sollyaPrintf("While construct: while condition do command.\n");
                          }
    break;

  case 581:
/* Line 1787 of yacc.c  */
#line 4098 "parser.y"
    {
#ifdef HELP_INF_TEXT
			    outputMode(); sollyaPrintf(HELP_INF_TEXT);
#else
			    outputMode(); sollyaPrintf("Dereferencing the infimum of a range: inf(range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for INF"
#endif
#endif
                          }
    break;

  case 582:
/* Line 1787 of yacc.c  */
#line 4109 "parser.y"
    {
#ifdef HELP_MID_TEXT
			    outputMode(); sollyaPrintf(HELP_MID_TEXT);
#else
			    outputMode(); sollyaPrintf("Dereferencing the midpoint of a range: mid(range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MID"
#endif
#endif
                          }
    break;

  case 583:
/* Line 1787 of yacc.c  */
#line 4120 "parser.y"
    {
#ifdef HELP_SUP_TEXT
			    outputMode(); sollyaPrintf(HELP_SUP_TEXT);
#else
			    outputMode(); sollyaPrintf("Dereferencing the supremum of a range: sup(range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SUP"
#endif
#endif
                          }
    break;

  case 584:
/* Line 1787 of yacc.c  */
#line 4131 "parser.y"
    {
#ifdef HELP_EXPONENT_TEXT
			    outputMode(); sollyaPrintf(HELP_EXPONENT_TEXT);
#else
			    outputMode(); sollyaPrintf("exponent(constant): returns an integer such that constant scaled by the power of 2\nof this integer is an odd or zero integer.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXPONENT"
#endif
#endif
                          }
    break;

  case 585:
/* Line 1787 of yacc.c  */
#line 4142 "parser.y"
    {
#ifdef HELP_MANTISSA_TEXT
			    outputMode(); sollyaPrintf(HELP_MANTISSA_TEXT);
#else
			    outputMode(); sollyaPrintf("mantissa(constant): returns an odd or zero integer equal to constant scaled by an integer power of 2.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MANTISSA"
#endif
#endif
                          }
    break;

  case 586:
/* Line 1787 of yacc.c  */
#line 4153 "parser.y"
    {
#ifdef HELP_PRECISION_TEXT
			    outputMode(); sollyaPrintf(HELP_PRECISION_TEXT);
#else
			    outputMode(); sollyaPrintf("precision(constant): returns the least number of bits constant can be written on.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRECISION"
#endif
#endif
                          }
    break;

  case 587:
/* Line 1787 of yacc.c  */
#line 4164 "parser.y"
    {
#ifdef HELP_EXECUTE_TEXT
			    outputMode(); sollyaPrintf(HELP_EXECUTE_TEXT);
#else
			    outputMode(); sollyaPrintf("execute(string): executes an %s script contained in a file named string.\n",PACKAGE_NAME);
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXECUTE"
#endif
#endif
                          }
    break;

  case 588:
/* Line 1787 of yacc.c  */
#line 4175 "parser.y"
    {
#ifdef HELP_ISBOUND_TEXT
			    outputMode(); sollyaPrintf(HELP_ISBOUND_TEXT);
#else
			    outputMode(); sollyaPrintf("isbound(identifier): returns a boolean indicating if identifier is bound.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ISBOUND"
#endif
#endif
                          }
    break;

  case 589:
/* Line 1787 of yacc.c  */
#line 4186 "parser.y"
    {
			    outputMode(); sollyaPrintf("Prints the version of the software.\n");
                          }
    break;

  case 590:
/* Line 1787 of yacc.c  */
#line 4189 "parser.y"
    {
#ifdef HELP_EXTERNALPROC_TEXT
			    outputMode(); sollyaPrintf(HELP_EXTERNALPROC_TEXT);
#else
			    outputMode(); sollyaPrintf("externalplot(identifier, file, argumentypes -> resulttype): binds identifier to an external procedure with signature argumenttypes -> resulttype in file.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXTERNALPROC"
#endif
#endif
                          }
    break;

  case 591:
/* Line 1787 of yacc.c  */
#line 4199 "parser.y"
    {
#ifdef HELP_VOID_TEXT
			    outputMode(); sollyaPrintf(HELP_VOID_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the void type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for VOID"
#endif
#endif
                          }
    break;

  case 592:
/* Line 1787 of yacc.c  */
#line 4209 "parser.y"
    {
#ifdef HELP_CONSTANT_TEXT
			    outputMode(); sollyaPrintf(HELP_CONSTANT_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the constant type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CONSTANT"
#endif
#endif
                          }
    break;

  case 593:
/* Line 1787 of yacc.c  */
#line 4219 "parser.y"
    {
#ifdef HELP_FUNCTION_TEXT
			    outputMode(); sollyaPrintf(HELP_FUNCTION_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the function type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FUNCTION"
#endif
#endif
                          }
    break;

  case 594:
/* Line 1787 of yacc.c  */
#line 4229 "parser.y"
    {
#ifdef HELP_RANGE_TEXT
			    outputMode(); sollyaPrintf(HELP_RANGE_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the range type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RANGE"
#endif
#endif
                          }
    break;

  case 595:
/* Line 1787 of yacc.c  */
#line 4239 "parser.y"
    {
#ifdef HELP_INTEGER_TEXT
			    outputMode(); sollyaPrintf(HELP_INTEGER_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the integer type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for INTEGER"
#endif
#endif
                          }
    break;

  case 596:
/* Line 1787 of yacc.c  */
#line 4249 "parser.y"
    {
#ifdef HELP_STRING_TEXT
			    outputMode(); sollyaPrintf(HELP_STRING_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the string type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for STRING"
#endif
#endif
                          }
    break;

  case 597:
/* Line 1787 of yacc.c  */
#line 4259 "parser.y"
    {
#ifdef HELP_BOOLEAN_TEXT
			    outputMode(); sollyaPrintf(HELP_BOOLEAN_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the boolean type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for BOOLEAN"
#endif
#endif
                          }
    break;

  case 598:
/* Line 1787 of yacc.c  */
#line 4269 "parser.y"
    {
#ifdef HELP_LISTOF_TEXT
			    outputMode(); sollyaPrintf(HELP_LISTOF_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the list type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LISTOF"
#endif
#endif
                          }
    break;

  case 599:
/* Line 1787 of yacc.c  */
#line 4279 "parser.y"
    {
#ifdef HELP_LISTOF_TEXT
			    outputMode(); sollyaPrintf(HELP_LISTOF_TEXT);
#else
			    outputMode(); sollyaPrintf("Used in list of type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LISTOF"
#endif
#endif
                          }
    break;

  case 600:
/* Line 1787 of yacc.c  */
#line 4289 "parser.y"
    {
#ifdef HELP_VAR_TEXT
			    outputMode(); sollyaPrintf(HELP_VAR_TEXT);
#else
			    outputMode(); sollyaPrintf("Declares a local variable.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for VAR"
#endif
#endif
                          }
    break;

  case 601:
/* Line 1787 of yacc.c  */
#line 4299 "parser.y"
    {
#ifdef HELP_NOP_TEXT
			    outputMode(); sollyaPrintf(HELP_NOP_TEXT);
#else
			    outputMode(); sollyaPrintf("Does nothing.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for NOP"
#endif
#endif
                          }
    break;

  case 602:
/* Line 1787 of yacc.c  */
#line 4309 "parser.y"
    {
#ifdef HELP_PROC_TEXT
			    outputMode(); sollyaPrintf(HELP_PROC_TEXT);
#else
			    outputMode(); sollyaPrintf("Defines a nameless procedure.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PROC"
#endif
#endif
                          }
    break;

  case 603:
/* Line 1787 of yacc.c  */
#line 4319 "parser.y"
    {
#ifdef HELP_PROCEDURE_TEXT
			    outputMode(); sollyaPrintf(HELP_PROCEDURE_TEXT);
#else
			    outputMode(); sollyaPrintf("Defines a named procedure.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PROCEDURE"
#endif
#endif
                          }
    break;

  case 604:
/* Line 1787 of yacc.c  */
#line 4329 "parser.y"
    {
#ifdef HELP_RETURN_TEXT
			    outputMode(); sollyaPrintf(HELP_RETURN_TEXT);
#else
			    outputMode(); sollyaPrintf("Returns an expression in a procedure.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RETURN"
#endif
#endif
                          }
    break;

  case 605:
/* Line 1787 of yacc.c  */
#line 4340 "parser.y"
    {
			    outputMode(); sollyaPrintf("Type \"help <keyword>;\" for help on the keyword <keyword>.\nFor example type \"help implementpoly;\" for help on the command \"implementpoly\".\n\n");
			    sollyaPrintf("Possible keywords in %s are:\n",PACKAGE_NAME);
			    sollyaPrintf("- !\n");
			    sollyaPrintf("- !=\n");
			    sollyaPrintf("- &&\n");
			    sollyaPrintf("- (\n");
			    sollyaPrintf("- )\n");
			    sollyaPrintf("- *\n");
			    sollyaPrintf("- *<\n");
			    sollyaPrintf("- +\n");
			    sollyaPrintf("- ,\n");
			    sollyaPrintf("- -\n");
			    sollyaPrintf("- .\n");
			    sollyaPrintf("- ...\n");
			    sollyaPrintf("- .:\n");
			    sollyaPrintf("- /\n");
			    sollyaPrintf("- :.\n");
			    sollyaPrintf("- ::\n");
			    sollyaPrintf("- :=\n");
			    sollyaPrintf("- ;\n");
			    sollyaPrintf("- <\n");
			    sollyaPrintf("- =\n");
			    sollyaPrintf("- ==\n");
			    sollyaPrintf("- >\n");
			    sollyaPrintf("- >*\n");
			    sollyaPrintf("- >.\n");
			    sollyaPrintf("- >_\n");
			    sollyaPrintf("- ?\n");
			    sollyaPrintf("- @\n");
			    sollyaPrintf("- D\n");
			    sollyaPrintf("- DD\n");
			    sollyaPrintf("- DE\n");
			    sollyaPrintf("- Pi\n");
			    sollyaPrintf("- RD\n");
			    sollyaPrintf("- RN\n");
			    sollyaPrintf("- RU\n");
			    sollyaPrintf("- RZ\n");
			    sollyaPrintf("- SG\n");
			    sollyaPrintf("- TD\n");
			    sollyaPrintf("- [\n");
			    sollyaPrintf("- ]\n");
			    sollyaPrintf("- ^\n");
			    sollyaPrintf("- abs\n");
			    sollyaPrintf("- absolute\n");
			    sollyaPrintf("- accurateinfnorm\n");
			    sollyaPrintf("- acos\n");
			    sollyaPrintf("- acosh\n");
			    sollyaPrintf("- asciiplot\n");
			    sollyaPrintf("- asin\n");
			    sollyaPrintf("- asinh\n");
			    sollyaPrintf("- atan\n");
			    sollyaPrintf("- atanh\n");
			    sollyaPrintf("- autodiff\n");
			    sollyaPrintf("- autosimplify\n");
			    sollyaPrintf("- bashexecute\n");
			    sollyaPrintf("- begin\n");
			    sollyaPrintf("- binary\n");
			    sollyaPrintf("- boolean\n");
			    sollyaPrintf("- by\n");
			    sollyaPrintf("- canonical\n");
			    sollyaPrintf("- ceil\n");
			    sollyaPrintf("- checkinfnorm\n");
			    sollyaPrintf("- coeff\n");
			    sollyaPrintf("- constant\n");
			    sollyaPrintf("- cos\n");
			    sollyaPrintf("- cosh\n");
			    sollyaPrintf("- decimal\n");
			    sollyaPrintf("- default\n");
			    sollyaPrintf("- degree\n");
			    sollyaPrintf("- denominator\n");
			    sollyaPrintf("- diam\n");
			    sollyaPrintf("- dieonerrormode\n");
			    sollyaPrintf("- diff\n");
			    sollyaPrintf("- dirtyfindzeros\n");
			    sollyaPrintf("- dirtyinfnorm\n");
			    sollyaPrintf("- dirtyintegral\n");
			    sollyaPrintf("- display\n");
			    sollyaPrintf("- do\n");
			    sollyaPrintf("- double\n");
			    sollyaPrintf("- doubledouble\n");
			    sollyaPrintf("- doubleextended\n");
			    sollyaPrintf("- dyadic\n");
			    sollyaPrintf("- else\n");
			    sollyaPrintf("- end\n");
			    sollyaPrintf("- erf\n");
			    sollyaPrintf("- erfc\n");
			    sollyaPrintf("- error\n");
			    sollyaPrintf("- evaluate\n");
			    sollyaPrintf("- execute\n");
			    sollyaPrintf("- exp\n");
			    sollyaPrintf("- expand\n");
			    sollyaPrintf("- expm1\n");
			    sollyaPrintf("- exponent\n");
			    sollyaPrintf("- externalplot\n");
			    sollyaPrintf("- externalproc\n");
			    sollyaPrintf("- false\n");
			    sollyaPrintf("- file\n");
			    sollyaPrintf("- findzeros\n");
			    sollyaPrintf("- fixed\n");
			    sollyaPrintf("- floating\n");
			    sollyaPrintf("- floor\n");
			    sollyaPrintf("- for\n");
			    sollyaPrintf("- fpfindzeros\n");
			    sollyaPrintf("- fpminimax\n");
			    sollyaPrintf("- from\n");
			    sollyaPrintf("- fullparentheses\n");
			    sollyaPrintf("- function\n");
			    sollyaPrintf("- guessdegree\n");
			    sollyaPrintf("- head\n");
			    sollyaPrintf("- hexadecimal\n");
			    sollyaPrintf("- honorcoeffprec\n");
			    sollyaPrintf("- hopitalrecursions\n");
			    sollyaPrintf("- horner\n");
			    sollyaPrintf("- if\n");
			    sollyaPrintf("- implementpoly\n");
			    sollyaPrintf("- implementconstant\n");
			    sollyaPrintf("- in\n");
			    sollyaPrintf("- inf\n");
			    sollyaPrintf("- infnorm\n");
			    sollyaPrintf("- integer\n");
			    sollyaPrintf("- integral\n");
			    sollyaPrintf("- isbound\n");
			    sollyaPrintf("- isevaluable\n");
			    sollyaPrintf("- length\n");
			    sollyaPrintf("- library\n");
			    sollyaPrintf("- libraryconstant\n");
			    sollyaPrintf("- list\n");
			    sollyaPrintf("- log\n");
			    sollyaPrintf("- log10\n");
			    sollyaPrintf("- log1p\n");
			    sollyaPrintf("- log2\n");
			    sollyaPrintf("- mantissa\n");
			    sollyaPrintf("- max\n");
			    sollyaPrintf("- mid\n");
			    sollyaPrintf("- midpointmode\n");
			    sollyaPrintf("- min\n");
			    sollyaPrintf("- nearestint\n");
			    sollyaPrintf("- numberroots\n");
			    sollyaPrintf("- nop\n");
			    sollyaPrintf("- numerator\n");
			    sollyaPrintf("- of\n");
			    sollyaPrintf("- off\n");
			    sollyaPrintf("- on\n");
			    sollyaPrintf("- parse\n");
			    sollyaPrintf("- perturb\n");
			    sollyaPrintf("- pi\n");
			    sollyaPrintf("- plot\n");
			    sollyaPrintf("- points\n");
			    sollyaPrintf("- postscript\n");
			    sollyaPrintf("- postscriptfile\n");
			    sollyaPrintf("- powers\n");
			    sollyaPrintf("- prec\n");
			    sollyaPrintf("- precision\n");
			    sollyaPrintf("- print\n");
			    sollyaPrintf("- printbinary\n");
			    sollyaPrintf("- printdouble\n");
			    sollyaPrintf("- printexpansion\n");
			    sollyaPrintf("- printfloat\n");
			    sollyaPrintf("- printhexa\n");
			    sollyaPrintf("- printsingle\n");
			    sollyaPrintf("- printxml\n");
			    sollyaPrintf("- proc\n");
			    sollyaPrintf("- procedure\n");
			    sollyaPrintf("- quit\n");
			    sollyaPrintf("- range\n");
			    sollyaPrintf("- rationalapprox\n");
			    sollyaPrintf("- rationalmode\n");
			    sollyaPrintf("- readfile\n");
			    sollyaPrintf("- readxml\n");
			    sollyaPrintf("- relative\n");
			    sollyaPrintf("- remez\n");
			    sollyaPrintf("- rename\n");
			    sollyaPrintf("- restart\n");
			    sollyaPrintf("- return\n");
			    sollyaPrintf("- revert\n");
			    sollyaPrintf("- round\n");
			    sollyaPrintf("- roundcoefficients\n");
			    sollyaPrintf("- roundcorrectly\n");
			    sollyaPrintf("- roundingwarnings\n");
			    sollyaPrintf("- safesimplify\n");
			    sollyaPrintf("- searchgal\n");
			    sollyaPrintf("- simplify\n");
			    sollyaPrintf("- simplifysafe\n");
			    sollyaPrintf("- sin\n");
			    sollyaPrintf("- single\n");
			    sollyaPrintf("- sinh\n");
			    sollyaPrintf("- sort\n");
			    sollyaPrintf("- sqrt\n");
			    sollyaPrintf("- string\n");
			    sollyaPrintf("- subpoly\n");
			    sollyaPrintf("- substitute\n");
			    sollyaPrintf("- sup\n");
			    sollyaPrintf("- supnorm\n");
			    sollyaPrintf("- tail\n");
			    sollyaPrintf("- tan\n");
			    sollyaPrintf("- tanh\n");
			    sollyaPrintf("- taylor\n");
			    sollyaPrintf("- taylorform\n");
			    sollyaPrintf("- taylorrecursions\n");
			    sollyaPrintf("- then\n");
			    sollyaPrintf("- time\n");
			    sollyaPrintf("- timing\n");
			    sollyaPrintf("- to\n");
			    sollyaPrintf("- tripledouble\n");
			    sollyaPrintf("- true\n");
			    sollyaPrintf("- var\n");
			    sollyaPrintf("- verbosity\n");
			    sollyaPrintf("- version\n");
			    sollyaPrintf("- void\n");
			    sollyaPrintf("- while\n");
			    sollyaPrintf("- worstcase\n");
			    sollyaPrintf("- write\n");
			    sollyaPrintf("- zerodenominators\n");
			    sollyaPrintf("- {\n");
			    sollyaPrintf("- |\n");
			    sollyaPrintf("- ||\n");
			    sollyaPrintf("- }\n");
			    sollyaPrintf("- ~\n");
			    sollyaPrintf("\n");
                          }
    break;


/* Line 1787 of yacc.c  */
#line 10914 "parser.c"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YYLEX_PARAM, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (YYLEX_PARAM, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval, YYLEX_PARAM);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp, YYLEX_PARAM);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YYLEX_PARAM, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, YYLEX_PARAM);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, YYLEX_PARAM);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


