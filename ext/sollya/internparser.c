/* A Bison parser, made by GNU Bison 2.7.12-4996.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.7.12-4996"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         internyyparse
#define yylex           internyylex
#define yyerror         internyyerror
#define yylval          internyylval
#define yychar          internyychar
#define yydebug         internyydebug
#define yynerrs         internyynerrs

/* Copy the first part of user declarations.  */
/* Line 371 of yacc.c  */
#line 58 "internparser.y"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "expression.h"
#include "assignment.h"
#include "chain.h"
#include "general.h"
#include "execute.h"
#include "internparser.h"

#define YYERROR_VERBOSE 1
  // #define YYPARSE_PARAM scanner
  // #define YYLEX_PARAM   scanner


extern int internyylex(YYSTYPE *lvalp, void *scanner);
extern FILE *internyyget_in(void *scanner);

 void internyyerror(void *myScanner, char *message) {
   if (!feof(internyyget_in(myScanner))) {
     printMessage(1,"Warning: %s.\nWill skip input until next semicolon after the unexpected token. May leak memory.\n",message);
     considerDyingOnError();
   }
 }


/* Line 371 of yacc.c  */
#line 104 "/mnt/extra/ferrandi/software/panda/trunk/panda/ext/./sollya/internparser.c"

# ifndef YY_NULL
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULL nullptr
#  else
#   define YY_NULL 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_INTERNYY_Y_TAB_H_INCLUDED
# define YY_INTERNYY_Y_TAB_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int internyydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     CONSTANTTOKEN = 258,
     MIDPOINTCONSTANTTOKEN = 259,
     DYADICCONSTANTTOKEN = 260,
     HEXCONSTANTTOKEN = 261,
     HEXADECIMALCONSTANTTOKEN = 262,
     BINARYCONSTANTTOKEN = 263,
     PITOKEN = 264,
     IDENTIFIERTOKEN = 265,
     STRINGTOKEN = 266,
     LPARTOKEN = 267,
     RPARTOKEN = 268,
     LBRACKETTOKEN = 269,
     RBRACKETTOKEN = 270,
     EQUALTOKEN = 271,
     ASSIGNEQUALTOKEN = 272,
     COMPAREEQUALTOKEN = 273,
     COMMATOKEN = 274,
     EXCLAMATIONTOKEN = 275,
     SEMICOLONTOKEN = 276,
     STARLEFTANGLETOKEN = 277,
     LEFTANGLETOKEN = 278,
     RIGHTANGLEUNDERSCORETOKEN = 279,
     RIGHTANGLEDOTTOKEN = 280,
     RIGHTANGLESTARTOKEN = 281,
     RIGHTANGLETOKEN = 282,
     DOTSTOKEN = 283,
     DOTTOKEN = 284,
     QUESTIONMARKTOKEN = 285,
     VERTBARTOKEN = 286,
     ATTOKEN = 287,
     DOUBLECOLONTOKEN = 288,
     COLONTOKEN = 289,
     DOTCOLONTOKEN = 290,
     COLONDOTTOKEN = 291,
     EXCLAMATIONEQUALTOKEN = 292,
     APPROXTOKEN = 293,
     ANDTOKEN = 294,
     ORTOKEN = 295,
     PLUSTOKEN = 296,
     MINUSTOKEN = 297,
     MULTOKEN = 298,
     DIVTOKEN = 299,
     POWTOKEN = 300,
     SQRTTOKEN = 301,
     EXPTOKEN = 302,
     LOGTOKEN = 303,
     LOG2TOKEN = 304,
     LOG10TOKEN = 305,
     SINTOKEN = 306,
     COSTOKEN = 307,
     TANTOKEN = 308,
     ASINTOKEN = 309,
     ACOSTOKEN = 310,
     ATANTOKEN = 311,
     SINHTOKEN = 312,
     COSHTOKEN = 313,
     TANHTOKEN = 314,
     ASINHTOKEN = 315,
     ACOSHTOKEN = 316,
     ATANHTOKEN = 317,
     ABSTOKEN = 318,
     ERFTOKEN = 319,
     ERFCTOKEN = 320,
     LOG1PTOKEN = 321,
     EXPM1TOKEN = 322,
     DOUBLETOKEN = 323,
     SINGLETOKEN = 324,
     QUADTOKEN = 325,
     HALFPRECISIONTOKEN = 326,
     DOUBLEDOUBLETOKEN = 327,
     TRIPLEDOUBLETOKEN = 328,
     DOUBLEEXTENDEDTOKEN = 329,
     CEILTOKEN = 330,
     FLOORTOKEN = 331,
     NEARESTINTTOKEN = 332,
     HEADTOKEN = 333,
     REVERTTOKEN = 334,
     SORTTOKEN = 335,
     TAILTOKEN = 336,
     MANTISSATOKEN = 337,
     EXPONENTTOKEN = 338,
     PRECISIONTOKEN = 339,
     ROUNDCORRECTLYTOKEN = 340,
     PRECTOKEN = 341,
     POINTSTOKEN = 342,
     DIAMTOKEN = 343,
     DISPLAYTOKEN = 344,
     VERBOSITYTOKEN = 345,
     CANONICALTOKEN = 346,
     AUTOSIMPLIFYTOKEN = 347,
     TAYLORRECURSIONSTOKEN = 348,
     TIMINGTOKEN = 349,
     FULLPARENTHESESTOKEN = 350,
     MIDPOINTMODETOKEN = 351,
     DIEONERRORMODETOKEN = 352,
     SUPPRESSWARNINGSTOKEN = 353,
     HOPITALRECURSIONSTOKEN = 354,
     RATIONALMODETOKEN = 355,
     ONTOKEN = 356,
     OFFTOKEN = 357,
     DYADICTOKEN = 358,
     POWERSTOKEN = 359,
     BINARYTOKEN = 360,
     HEXADECIMALTOKEN = 361,
     FILETOKEN = 362,
     POSTSCRIPTTOKEN = 363,
     POSTSCRIPTFILETOKEN = 364,
     PERTURBTOKEN = 365,
     MINUSWORDTOKEN = 366,
     PLUSWORDTOKEN = 367,
     ZEROWORDTOKEN = 368,
     NEARESTTOKEN = 369,
     HONORCOEFFPRECTOKEN = 370,
     TRUETOKEN = 371,
     FALSETOKEN = 372,
     DEFAULTTOKEN = 373,
     MATCHTOKEN = 374,
     WITHTOKEN = 375,
     ABSOLUTETOKEN = 376,
     DECIMALTOKEN = 377,
     RELATIVETOKEN = 378,
     FIXEDTOKEN = 379,
     FLOATINGTOKEN = 380,
     ERRORTOKEN = 381,
     LIBRARYTOKEN = 382,
     LIBRARYCONSTANTTOKEN = 383,
     DIFFTOKEN = 384,
     BASHEVALUATETOKEN = 385,
     SIMPLIFYTOKEN = 386,
     REMEZTOKEN = 387,
     FPMINIMAXTOKEN = 388,
     HORNERTOKEN = 389,
     EXPANDTOKEN = 390,
     SIMPLIFYSAFETOKEN = 391,
     TAYLORTOKEN = 392,
     TAYLORFORMTOKEN = 393,
     AUTODIFFTOKEN = 394,
     DEGREETOKEN = 395,
     NUMERATORTOKEN = 396,
     DENOMINATORTOKEN = 397,
     SUBSTITUTETOKEN = 398,
     COEFFTOKEN = 399,
     SUBPOLYTOKEN = 400,
     ROUNDCOEFFICIENTSTOKEN = 401,
     RATIONALAPPROXTOKEN = 402,
     ACCURATEINFNORMTOKEN = 403,
     ROUNDTOFORMATTOKEN = 404,
     EVALUATETOKEN = 405,
     LENGTHTOKEN = 406,
     INFTOKEN = 407,
     MIDTOKEN = 408,
     SUPTOKEN = 409,
     MINTOKEN = 410,
     MAXTOKEN = 411,
     READXMLTOKEN = 412,
     PARSETOKEN = 413,
     PRINTTOKEN = 414,
     PRINTXMLTOKEN = 415,
     PLOTTOKEN = 416,
     PRINTHEXATOKEN = 417,
     PRINTFLOATTOKEN = 418,
     PRINTBINARYTOKEN = 419,
     PRINTEXPANSIONTOKEN = 420,
     BASHEXECUTETOKEN = 421,
     EXTERNALPLOTTOKEN = 422,
     WRITETOKEN = 423,
     ASCIIPLOTTOKEN = 424,
     RENAMETOKEN = 425,
     INFNORMTOKEN = 426,
     SUPNORMTOKEN = 427,
     FINDZEROSTOKEN = 428,
     FPFINDZEROSTOKEN = 429,
     DIRTYINFNORMTOKEN = 430,
     NUMBERROOTSTOKEN = 431,
     INTEGRALTOKEN = 432,
     DIRTYINTEGRALTOKEN = 433,
     WORSTCASETOKEN = 434,
     IMPLEMENTPOLYTOKEN = 435,
     IMPLEMENTCONSTTOKEN = 436,
     CHECKINFNORMTOKEN = 437,
     ZERODENOMINATORSTOKEN = 438,
     ISEVALUABLETOKEN = 439,
     SEARCHGALTOKEN = 440,
     GUESSDEGREETOKEN = 441,
     DIRTYFINDZEROSTOKEN = 442,
     IFTOKEN = 443,
     THENTOKEN = 444,
     ELSETOKEN = 445,
     FORTOKEN = 446,
     INTOKEN = 447,
     FROMTOKEN = 448,
     TOTOKEN = 449,
     BYTOKEN = 450,
     DOTOKEN = 451,
     BEGINTOKEN = 452,
     ENDTOKEN = 453,
     LEFTCURLYBRACETOKEN = 454,
     RIGHTCURLYBRACETOKEN = 455,
     WHILETOKEN = 456,
     READFILETOKEN = 457,
     ISBOUNDTOKEN = 458,
     EXECUTETOKEN = 459,
     FALSERESTARTTOKEN = 460,
     FALSEQUITTOKEN = 461,
     EXTERNALPROCTOKEN = 462,
     VOIDTOKEN = 463,
     CONSTANTTYPETOKEN = 464,
     FUNCTIONTOKEN = 465,
     RANGETOKEN = 466,
     INTEGERTOKEN = 467,
     STRINGTYPETOKEN = 468,
     BOOLEANTOKEN = 469,
     LISTTOKEN = 470,
     OFTOKEN = 471,
     VARTOKEN = 472,
     PROCTOKEN = 473,
     TIMETOKEN = 474,
     PROCEDURETOKEN = 475,
     RETURNTOKEN = 476,
     NOPTOKEN = 477
   };
#endif
/* Tokens.  */
#define CONSTANTTOKEN 258
#define MIDPOINTCONSTANTTOKEN 259
#define DYADICCONSTANTTOKEN 260
#define HEXCONSTANTTOKEN 261
#define HEXADECIMALCONSTANTTOKEN 262
#define BINARYCONSTANTTOKEN 263
#define PITOKEN 264
#define IDENTIFIERTOKEN 265
#define STRINGTOKEN 266
#define LPARTOKEN 267
#define RPARTOKEN 268
#define LBRACKETTOKEN 269
#define RBRACKETTOKEN 270
#define EQUALTOKEN 271
#define ASSIGNEQUALTOKEN 272
#define COMPAREEQUALTOKEN 273
#define COMMATOKEN 274
#define EXCLAMATIONTOKEN 275
#define SEMICOLONTOKEN 276
#define STARLEFTANGLETOKEN 277
#define LEFTANGLETOKEN 278
#define RIGHTANGLEUNDERSCORETOKEN 279
#define RIGHTANGLEDOTTOKEN 280
#define RIGHTANGLESTARTOKEN 281
#define RIGHTANGLETOKEN 282
#define DOTSTOKEN 283
#define DOTTOKEN 284
#define QUESTIONMARKTOKEN 285
#define VERTBARTOKEN 286
#define ATTOKEN 287
#define DOUBLECOLONTOKEN 288
#define COLONTOKEN 289
#define DOTCOLONTOKEN 290
#define COLONDOTTOKEN 291
#define EXCLAMATIONEQUALTOKEN 292
#define APPROXTOKEN 293
#define ANDTOKEN 294
#define ORTOKEN 295
#define PLUSTOKEN 296
#define MINUSTOKEN 297
#define MULTOKEN 298
#define DIVTOKEN 299
#define POWTOKEN 300
#define SQRTTOKEN 301
#define EXPTOKEN 302
#define LOGTOKEN 303
#define LOG2TOKEN 304
#define LOG10TOKEN 305
#define SINTOKEN 306
#define COSTOKEN 307
#define TANTOKEN 308
#define ASINTOKEN 309
#define ACOSTOKEN 310
#define ATANTOKEN 311
#define SINHTOKEN 312
#define COSHTOKEN 313
#define TANHTOKEN 314
#define ASINHTOKEN 315
#define ACOSHTOKEN 316
#define ATANHTOKEN 317
#define ABSTOKEN 318
#define ERFTOKEN 319
#define ERFCTOKEN 320
#define LOG1PTOKEN 321
#define EXPM1TOKEN 322
#define DOUBLETOKEN 323
#define SINGLETOKEN 324
#define QUADTOKEN 325
#define HALFPRECISIONTOKEN 326
#define DOUBLEDOUBLETOKEN 327
#define TRIPLEDOUBLETOKEN 328
#define DOUBLEEXTENDEDTOKEN 329
#define CEILTOKEN 330
#define FLOORTOKEN 331
#define NEARESTINTTOKEN 332
#define HEADTOKEN 333
#define REVERTTOKEN 334
#define SORTTOKEN 335
#define TAILTOKEN 336
#define MANTISSATOKEN 337
#define EXPONENTTOKEN 338
#define PRECISIONTOKEN 339
#define ROUNDCORRECTLYTOKEN 340
#define PRECTOKEN 341
#define POINTSTOKEN 342
#define DIAMTOKEN 343
#define DISPLAYTOKEN 344
#define VERBOSITYTOKEN 345
#define CANONICALTOKEN 346
#define AUTOSIMPLIFYTOKEN 347
#define TAYLORRECURSIONSTOKEN 348
#define TIMINGTOKEN 349
#define FULLPARENTHESESTOKEN 350
#define MIDPOINTMODETOKEN 351
#define DIEONERRORMODETOKEN 352
#define SUPPRESSWARNINGSTOKEN 353
#define HOPITALRECURSIONSTOKEN 354
#define RATIONALMODETOKEN 355
#define ONTOKEN 356
#define OFFTOKEN 357
#define DYADICTOKEN 358
#define POWERSTOKEN 359
#define BINARYTOKEN 360
#define HEXADECIMALTOKEN 361
#define FILETOKEN 362
#define POSTSCRIPTTOKEN 363
#define POSTSCRIPTFILETOKEN 364
#define PERTURBTOKEN 365
#define MINUSWORDTOKEN 366
#define PLUSWORDTOKEN 367
#define ZEROWORDTOKEN 368
#define NEARESTTOKEN 369
#define HONORCOEFFPRECTOKEN 370
#define TRUETOKEN 371
#define FALSETOKEN 372
#define DEFAULTTOKEN 373
#define MATCHTOKEN 374
#define WITHTOKEN 375
#define ABSOLUTETOKEN 376
#define DECIMALTOKEN 377
#define RELATIVETOKEN 378
#define FIXEDTOKEN 379
#define FLOATINGTOKEN 380
#define ERRORTOKEN 381
#define LIBRARYTOKEN 382
#define LIBRARYCONSTANTTOKEN 383
#define DIFFTOKEN 384
#define BASHEVALUATETOKEN 385
#define SIMPLIFYTOKEN 386
#define REMEZTOKEN 387
#define FPMINIMAXTOKEN 388
#define HORNERTOKEN 389
#define EXPANDTOKEN 390
#define SIMPLIFYSAFETOKEN 391
#define TAYLORTOKEN 392
#define TAYLORFORMTOKEN 393
#define AUTODIFFTOKEN 394
#define DEGREETOKEN 395
#define NUMERATORTOKEN 396
#define DENOMINATORTOKEN 397
#define SUBSTITUTETOKEN 398
#define COEFFTOKEN 399
#define SUBPOLYTOKEN 400
#define ROUNDCOEFFICIENTSTOKEN 401
#define RATIONALAPPROXTOKEN 402
#define ACCURATEINFNORMTOKEN 403
#define ROUNDTOFORMATTOKEN 404
#define EVALUATETOKEN 405
#define LENGTHTOKEN 406
#define INFTOKEN 407
#define MIDTOKEN 408
#define SUPTOKEN 409
#define MINTOKEN 410
#define MAXTOKEN 411
#define READXMLTOKEN 412
#define PARSETOKEN 413
#define PRINTTOKEN 414
#define PRINTXMLTOKEN 415
#define PLOTTOKEN 416
#define PRINTHEXATOKEN 417
#define PRINTFLOATTOKEN 418
#define PRINTBINARYTOKEN 419
#define PRINTEXPANSIONTOKEN 420
#define BASHEXECUTETOKEN 421
#define EXTERNALPLOTTOKEN 422
#define WRITETOKEN 423
#define ASCIIPLOTTOKEN 424
#define RENAMETOKEN 425
#define INFNORMTOKEN 426
#define SUPNORMTOKEN 427
#define FINDZEROSTOKEN 428
#define FPFINDZEROSTOKEN 429
#define DIRTYINFNORMTOKEN 430
#define NUMBERROOTSTOKEN 431
#define INTEGRALTOKEN 432
#define DIRTYINTEGRALTOKEN 433
#define WORSTCASETOKEN 434
#define IMPLEMENTPOLYTOKEN 435
#define IMPLEMENTCONSTTOKEN 436
#define CHECKINFNORMTOKEN 437
#define ZERODENOMINATORSTOKEN 438
#define ISEVALUABLETOKEN 439
#define SEARCHGALTOKEN 440
#define GUESSDEGREETOKEN 441
#define DIRTYFINDZEROSTOKEN 442
#define IFTOKEN 443
#define THENTOKEN 444
#define ELSETOKEN 445
#define FORTOKEN 446
#define INTOKEN 447
#define FROMTOKEN 448
#define TOTOKEN 449
#define BYTOKEN 450
#define DOTOKEN 451
#define BEGINTOKEN 452
#define ENDTOKEN 453
#define LEFTCURLYBRACETOKEN 454
#define RIGHTCURLYBRACETOKEN 455
#define WHILETOKEN 456
#define READFILETOKEN 457
#define ISBOUNDTOKEN 458
#define EXECUTETOKEN 459
#define FALSERESTARTTOKEN 460
#define FALSEQUITTOKEN 461
#define EXTERNALPROCTOKEN 462
#define VOIDTOKEN 463
#define CONSTANTTYPETOKEN 464
#define FUNCTIONTOKEN 465
#define RANGETOKEN 466
#define INTEGERTOKEN 467
#define STRINGTYPETOKEN 468
#define BOOLEANTOKEN 469
#define LISTTOKEN 470
#define OFTOKEN 471
#define VARTOKEN 472
#define PROCTOKEN 473
#define TIMETOKEN 474
#define PROCEDURETOKEN 475
#define RETURNTOKEN 476
#define NOPTOKEN 477



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 387 of yacc.c  */
#line 98 "internparser.y"

  doubleNode *dblnode;
  struct entryStruct *association;
  char *value;
  node *tree;
  chain *list;
  int *integerval;
  int count;
  void *other;


/* Line 387 of yacc.c  */
#line 603 "/mnt/extra/ferrandi/software/panda/trunk/panda/ext/./sollya/internparser.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int internyyparse (void *YYPARSE_PARAM);
#else
int internyyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int internyyparse (void *myScanner);
#else
int internyyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_INTERNYY_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

/* Line 390 of yacc.c  */
#line 630 "/mnt/extra/ferrandi/software/panda/trunk/panda/ext/./sollya/internparser.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef __attribute__
/* This feature is available in gcc versions 2.5 and later.  */
# if (! defined __GNUC__ || __GNUC__ < 2 \
      || (__GNUC__ == 2 && __GNUC_MINOR__ < 5))
#  define __attribute__(Spec) /* empty */
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif


/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(N) (N)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  382
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   7734

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  223
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  46
/* YYNRULES -- Number of rules.  */
#define YYNRULES  378
/* YYNRULES -- Number of states.  */
#define YYNSTATES  1049

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   477

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     8,    11,    13,    15,    17,    19,
      21,    25,    30,    34,    37,    40,    45,    48,    52,    58,
      66,    76,    82,    85,    89,    92,    96,    99,   101,   105,
     111,   118,   124,   129,   138,   148,   157,   165,   172,   180,
     187,   193,   203,   214,   224,   233,   242,   252,   261,   269,
     281,   294,   306,   317,   319,   321,   323,   328,   332,   337,
     344,   352,   359,   364,   369,   374,   379,   384,   397,   402,
     409,   417,   424,   429,   434,   441,   449,   462,   469,   481,
     483,   485,   489,   491,   494,   496,   499,   503,   507,   514,
     521,   525,   529,   533,   537,   541,   545,   549,   553,   557,
     561,   565,   569,   573,   577,   581,   585,   589,   593,   597,
     601,   605,   609,   613,   617,   621,   625,   629,   633,   637,
     641,   645,   649,   653,   657,   661,   663,   667,   669,   673,
     675,   677,   682,   684,   689,   691,   695,   699,   702,   707,
     709,   713,   717,   721,   725,   730,   735,   739,   741,   745,
     749,   753,   757,   761,   765,   767,   769,   772,   775,   777,
     780,   783,   787,   791,   796,   801,   806,   811,   813,   817,
     822,   827,   829,   831,   833,   835,   837,   839,   841,   843,
     845,   847,   849,   851,   853,   855,   857,   859,   861,   863,
     865,   867,   869,   871,   873,   875,   877,   879,   881,   883,
     885,   887,   889,   891,   893,   895,   897,   902,   907,   911,
     913,   915,   917,   919,   923,   927,   929,   931,   935,   942,
     949,   952,   957,   959,   962,   972,   979,   988,   994,  1003,
    1009,  1017,  1022,  1028,  1030,  1032,  1034,  1036,  1038,  1040,
    1042,  1047,  1051,  1057,  1064,  1066,  1070,  1076,  1082,  1088,
    1092,  1096,  1100,  1104,  1109,  1114,  1119,  1124,  1129,  1136,
    1141,  1150,  1155,  1160,  1171,  1176,  1181,  1186,  1191,  1200,
    1209,  1218,  1223,  1228,  1233,  1240,  1247,  1254,  1261,  1268,
    1277,  1286,  1293,  1298,  1303,  1310,  1323,  1330,  1337,  1344,
    1351,  1358,  1365,  1380,  1385,  1394,  1401,  1408,  1413,  1422,
    1429,  1434,  1439,  1444,  1449,  1454,  1459,  1464,  1469,  1474,
    1479,  1484,  1489,  1496,  1501,  1506,  1511,  1516,  1521,  1526,
    1531,  1536,  1541,  1546,  1551,  1556,  1561,  1566,  1571,  1576,
    1581,  1586,  1591,  1596,  1601,  1606,  1611,  1616,  1621,  1626,
    1631,  1636,  1641,  1646,  1651,  1654,  1655,  1658,  1661,  1664,
    1667,  1670,  1673,  1676,  1679,  1682,  1685,  1688,  1691,  1694,
    1697,  1700,  1702,  1704,  1706,  1708,  1710,  1712,  1716,  1720,
    1724,  1728,  1732,  1736,  1738,  1740,  1742,  1746,  1748
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     224,     0,    -1,   225,    -1,   228,    21,    -1,     1,    21,
      -1,   197,    -1,   199,    -1,   198,    -1,   200,    -1,   236,
      -1,   226,   231,   227,    -1,   226,   232,   231,   227,    -1,
     226,   232,   227,    -1,   226,   227,    -1,   188,   229,    -1,
     201,   246,   196,   228,    -1,   191,   230,    -1,   246,   189,
     228,    -1,   246,   189,   228,   190,   228,    -1,    10,   193,
     246,   194,   246,   196,   228,    -1,    10,   193,   246,   194,
     246,   195,   246,   196,   228,    -1,    10,   192,   246,   196,
     228,    -1,   228,    21,    -1,   228,    21,   231,    -1,   233,
      21,    -1,   233,    21,   232,    -1,   217,   234,    -1,    10,
      -1,    10,    19,   234,    -1,    12,    13,   226,   231,   227,
      -1,    12,    13,   226,   232,   231,   227,    -1,    12,    13,
     226,   232,   227,    -1,    12,    13,   226,   227,    -1,    12,
      13,   226,   231,   221,   246,    21,   227,    -1,    12,    13,
     226,   232,   231,   221,   246,    21,   227,    -1,    12,    13,
     226,   232,   221,   246,    21,   227,    -1,    12,    13,   226,
     221,   246,    21,   227,    -1,    12,   234,    13,   226,   231,
     227,    -1,    12,   234,    13,   226,   232,   231,   227,    -1,
      12,   234,    13,   226,   232,   227,    -1,    12,   234,    13,
     226,   227,    -1,    12,   234,    13,   226,   231,   221,   246,
      21,   227,    -1,    12,   234,    13,   226,   232,   231,   221,
     246,    21,   227,    -1,    12,   234,    13,   226,   232,   221,
     246,    21,   227,    -1,    12,   234,    13,   226,   221,   246,
      21,   227,    -1,    12,    10,    16,    28,    13,   226,   231,
     227,    -1,    12,    10,    16,    28,    13,   226,   232,   231,
     227,    -1,    12,    10,    16,    28,    13,   226,   232,   227,
      -1,    12,    10,    16,    28,    13,   226,   227,    -1,    12,
      10,    16,    28,    13,   226,   231,   221,   246,    21,   227,
      -1,    12,    10,    16,    28,    13,   226,   232,   231,   221,
     246,    21,   227,    -1,    12,    10,    16,    28,    13,   226,
     232,   221,   246,    21,   227,    -1,    12,    10,    16,    28,
      13,   226,   221,   246,    21,   227,    -1,   206,    -1,   205,
      -1,   222,    -1,   222,    12,   246,    13,    -1,   222,    12,
      13,    -1,   159,    12,   242,    13,    -1,   159,    12,   242,
      13,    27,   246,    -1,   159,    12,   242,    13,    27,    27,
     246,    -1,   161,    12,   246,    19,   242,    13,    -1,   162,
      12,   246,    13,    -1,   163,    12,   246,    13,    -1,   164,
      12,   246,    13,    -1,   165,    12,   246,    13,    -1,   166,
      12,   246,    13,    -1,   167,    12,   246,    19,   246,    19,
     246,    19,   246,    19,   242,    13,    -1,   168,    12,   242,
      13,    -1,   168,    12,   242,    13,    27,   246,    -1,   168,
      12,   242,    13,    27,    27,   246,    -1,   169,    12,   246,
      19,   246,    13,    -1,   160,    12,   246,    13,    -1,   204,
      12,   246,    13,    -1,   160,    12,   246,    13,    27,   246,
      -1,   160,    12,   246,    13,    27,    27,   246,    -1,   179,
      12,   246,    19,   246,    19,   246,    19,   246,    19,   242,
      13,    -1,   170,    12,    10,    19,    10,    13,    -1,   207,
      12,    10,    19,   246,    19,   268,    42,    27,   266,    13,
      -1,   237,    -1,   242,    -1,   220,    10,   235,    -1,   240,
      -1,   241,    20,    -1,   238,    -1,   238,    20,    -1,    10,
      16,   246,    -1,    10,    17,   246,    -1,    10,    16,   127,
      12,   246,    13,    -1,    10,    16,   128,    12,   246,    13,
      -1,   248,    16,   246,    -1,   248,    17,   246,    -1,   239,
      16,   246,    -1,   239,    17,   246,    -1,   254,    29,    10,
      -1,    86,    16,   246,    -1,    87,    16,   246,    -1,    88,
      16,   246,    -1,    89,    16,   246,    -1,    90,    16,   246,
      -1,    91,    16,   246,    -1,    92,    16,   246,    -1,    93,
      16,   246,    -1,    94,    16,   246,    -1,    95,    16,   246,
      -1,    96,    16,   246,    -1,    97,    16,   246,    -1,   100,
      16,   246,    -1,    98,    16,   246,    -1,    99,    16,   246,
      -1,    86,    16,   246,    -1,    87,    16,   246,    -1,    88,
      16,   246,    -1,    89,    16,   246,    -1,    90,    16,   246,
      -1,    91,    16,   246,    -1,    92,    16,   246,    -1,    93,
      16,   246,    -1,    94,    16,   246,    -1,    95,    16,   246,
      -1,    96,    16,   246,    -1,    97,    16,   246,    -1,   100,
      16,   246,    -1,    98,    16,   246,    -1,    99,    16,   246,
      -1,   246,    -1,   246,    19,   242,    -1,   245,    -1,   245,
     244,   243,    -1,    19,    -1,    21,    -1,    29,    10,    16,
     246,    -1,   247,    -1,   119,   247,   120,   255,    -1,   249,
      -1,   246,    39,   249,    -1,   246,    40,   249,    -1,    20,
     249,    -1,   254,    14,   246,    15,    -1,   250,    -1,   249,
      18,   250,    -1,   249,   192,   250,    -1,   249,    23,   250,
      -1,   249,    27,   250,    -1,   249,    23,    16,   250,    -1,
     249,    27,    16,   250,    -1,   249,    37,   250,    -1,   252,
      -1,   250,    41,   252,    -1,   250,    42,   252,    -1,   250,
      32,   252,    -1,   250,    33,   252,    -1,   250,    35,   252,
      -1,   250,    36,   252,    -1,    41,    -1,    42,    -1,    41,
     251,    -1,    42,   251,    -1,   253,    -1,   251,   253,    -1,
      38,   253,    -1,   252,    43,   253,    -1,   252,    44,   253,
      -1,   252,    43,   251,   253,    -1,   252,    44,   251,   253,
      -1,   252,    43,    38,   253,    -1,   252,    44,    38,   253,
      -1,   254,    -1,   254,    45,   253,    -1,   254,    45,   251,
     253,    -1,   254,    45,    38,   253,    -1,   101,    -1,   102,
      -1,   103,    -1,   104,    -1,   105,    -1,   106,    -1,   107,
      -1,   108,    -1,   109,    -1,   110,    -1,   111,    -1,   112,
      -1,   113,    -1,   114,    -1,   115,    -1,   116,    -1,   208,
      -1,   117,    -1,   118,    -1,   122,    -1,   121,    -1,   123,
      -1,   124,    -1,   125,    -1,   126,    -1,    68,    -1,    69,
      -1,    70,    -1,    71,    -1,    74,    -1,    72,    -1,    73,
      -1,    11,    -1,   257,    -1,    10,    -1,   203,    12,    10,
      13,    -1,    10,    12,   242,    13,    -1,    10,    12,    13,
      -1,   258,    -1,   260,    -1,   261,    -1,   262,    -1,    12,
     246,    13,    -1,   199,   243,   200,    -1,   264,    -1,   248,
      -1,   254,    29,    10,    -1,   254,    29,    10,    12,   242,
      13,    -1,    12,   246,    13,    12,   242,    13,    -1,   218,
     235,    -1,   219,    12,   228,    13,    -1,   256,    -1,   256,
     255,    -1,   246,    34,   226,   232,   231,   221,   246,    21,
     227,    -1,   246,    34,   226,   232,   231,   227,    -1,   246,
      34,   226,   232,   221,   246,    21,   227,    -1,   246,    34,
     226,   232,   227,    -1,   246,    34,   226,   231,   221,   246,
      21,   227,    -1,   246,    34,   226,   231,   227,    -1,   246,
      34,   226,   221,   246,    21,   227,    -1,   246,    34,   226,
     227,    -1,   246,    34,    12,   246,    13,    -1,     3,    -1,
       4,    -1,     5,    -1,     6,    -1,     7,    -1,     8,    -1,
       9,    -1,    14,    31,    31,    15,    -1,    14,    40,    15,
      -1,    14,    31,   259,    31,    15,    -1,    14,    31,   259,
      28,    31,    15,    -1,   246,    -1,   259,    19,   246,    -1,
     259,    19,    28,    19,   246,    -1,    14,   246,    19,   246,
      15,    -1,    14,   246,    21,   246,    15,    -1,    14,   246,
      15,    -1,    22,   246,    26,    -1,    22,   246,    25,    -1,
      22,   246,    24,    -1,   154,    12,   246,    13,    -1,   153,
      12,   246,    13,    -1,   152,    12,   246,    13,    -1,   129,
      12,   246,    13,    -1,   130,    12,   246,    13,    -1,   130,
      12,   246,    19,   246,    13,    -1,   131,    12,   246,    13,
      -1,   132,    12,   246,    19,   246,    19,   242,    13,    -1,
     155,    12,   242,    13,    -1,   156,    12,   242,    13,    -1,
     133,    12,   246,    19,   246,    19,   246,    19,   242,    13,
      -1,   134,    12,   246,    13,    -1,    91,    12,   246,    13,
      -1,   135,    12,   246,    13,    -1,   136,    12,   246,    13,
      -1,   137,    12,   246,    19,   246,    19,   246,    13,    -1,
     138,    12,   246,    19,   246,    19,   242,    13,    -1,   139,
      12,   246,    19,   246,    19,   246,    13,    -1,   140,    12,
     246,    13,    -1,   141,    12,   246,    13,    -1,   142,    12,
     246,    13,    -1,   143,    12,   246,    19,   246,    13,    -1,
     144,    12,   246,    19,   246,    13,    -1,   145,    12,   246,
      19,   246,    13,    -1,   146,    12,   246,    19,   246,    13,
      -1,   147,    12,   246,    19,   246,    13,    -1,   148,    12,
     246,    19,   246,    19,   242,    13,    -1,   149,    12,   246,
      19,   246,    19,   246,    13,    -1,   150,    12,   246,    19,
     246,    13,    -1,   158,    12,   246,    13,    -1,   157,    12,
     246,    13,    -1,   171,    12,   246,    19,   242,    13,    -1,
     172,    12,   246,    19,   246,    19,   246,    19,   246,    19,
     246,    13,    -1,   173,    12,   246,    19,   246,    13,    -1,
     174,    12,   246,    19,   246,    13,    -1,   175,    12,   246,
      19,   246,    13,    -1,   176,    12,   246,    19,   246,    13,
      -1,   177,    12,   246,    19,   246,    13,    -1,   178,    12,
     246,    19,   246,    13,    -1,   180,    12,   246,    19,   246,
      19,   246,    19,   246,    19,   246,    19,   242,    13,    -1,
     181,    12,   242,    13,    -1,   182,    12,   246,    19,   246,
      19,   246,    13,    -1,   183,    12,   246,    19,   246,    13,
      -1,   184,    12,   246,    19,   246,    13,    -1,   185,    12,
     242,    13,    -1,   186,    12,   246,    19,   246,    19,   242,
      13,    -1,   187,    12,   246,    19,   246,    13,    -1,    78,
      12,   246,    13,    -1,    85,    12,   246,    13,    -1,   202,
      12,   246,    13,    -1,    79,    12,   246,    13,    -1,    80,
      12,   246,    13,    -1,    82,    12,   246,    13,    -1,    83,
      12,   246,    13,    -1,    84,    12,   246,    13,    -1,    81,
      12,   246,    13,    -1,    46,    12,   246,    13,    -1,    47,
      12,   246,    13,    -1,   210,    12,   246,    13,    -1,   210,
      12,   246,    19,   246,    13,    -1,    48,    12,   246,    13,
      -1,    49,    12,   246,    13,    -1,    50,    12,   246,    13,
      -1,    51,    12,   246,    13,    -1,    52,    12,   246,    13,
      -1,    53,    12,   246,    13,    -1,    54,    12,   246,    13,
      -1,    55,    12,   246,    13,    -1,    56,    12,   246,    13,
      -1,    57,    12,   246,    13,    -1,    58,    12,   246,    13,
      -1,    59,    12,   246,    13,    -1,    60,    12,   246,    13,
      -1,    61,    12,   246,    13,    -1,    62,    12,   246,    13,
      -1,    63,    12,   246,    13,    -1,    64,    12,   246,    13,
      -1,    65,    12,   246,    13,    -1,    66,    12,   246,    13,
      -1,    67,    12,   246,    13,    -1,    68,    12,   246,    13,
      -1,    69,    12,   246,    13,    -1,    70,    12,   246,    13,
      -1,    71,    12,   246,    13,    -1,    72,    12,   246,    13,
      -1,    73,    12,   246,    13,    -1,    74,    12,   246,    13,
      -1,    75,    12,   246,    13,    -1,    76,    12,   246,    13,
      -1,    77,    12,   246,    13,    -1,   151,    12,   246,    13,
      -1,    16,    30,    -1,    -1,    86,   263,    -1,    87,   263,
      -1,    88,   263,    -1,    89,   263,    -1,    90,   263,    -1,
      91,   263,    -1,    92,   263,    -1,    93,   263,    -1,    94,
     263,    -1,    95,   263,    -1,    96,   263,    -1,    97,   263,
      -1,   100,   263,    -1,    98,   263,    -1,    99,   263,    -1,
     209,    -1,   210,    -1,   211,    -1,   212,    -1,   213,    -1,
     214,    -1,   215,   216,   209,    -1,   215,   216,   210,    -1,
     215,   216,   211,    -1,   215,   216,   212,    -1,   215,   216,
     213,    -1,   215,   216,   214,    -1,   208,    -1,   265,    -1,
     265,    -1,   265,    19,   267,    -1,   266,    -1,    12,   267,
      13,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   404,   404,   413,   417,   423,   427,   433,   437,   443,
     447,   451,   455,   459,   463,   467,   471,   477,   481,   489,
     494,   499,   507,   511,   517,   521,   527,   534,   538,   544,
     548,   552,   556,   560,   564,   568,   572,   576,   580,   584,
     588,   592,   596,   600,   604,   608,   612,   616,   620,   624,
     628,   632,   636,   644,   648,   652,   656,   660,   664,   668,
     672,   676,   680,   684,   688,   692,   696,   700,   704,   708,
     712,   716,   720,   724,   728,   732,   736,   740,   746,   751,
     755,   759,   766,   770,   774,   778,   784,   789,   794,   799,
     804,   809,   814,   818,   824,   831,   835,   839,   843,   847,
     851,   855,   859,   863,   867,   871,   875,   879,   883,   887,
     893,   897,   901,   905,   909,   913,   917,   921,   925,   929,
     933,   937,   941,   945,   949,   955,   959,   965,   969,   975,
     979,   985,   995,   999,  1005,  1009,  1013,  1017,  1023,  1032,
    1036,  1040,  1044,  1048,  1052,  1056,  1060,  1066,  1070,  1074,
    1078,  1082,  1086,  1090,  1096,  1100,  1104,  1108,  1115,  1119,
    1126,  1130,  1134,  1138,  1145,  1152,  1156,  1162,  1166,  1170,
    1177,  1184,  1188,  1192,  1196,  1200,  1204,  1208,  1212,  1216,
    1220,  1224,  1228,  1232,  1236,  1240,  1244,  1248,  1252,  1256,
    1260,  1264,  1268,  1272,  1276,  1280,  1284,  1288,  1292,  1296,
    1300,  1304,  1308,  1312,  1323,  1327,  1332,  1337,  1342,  1347,
    1351,  1355,  1359,  1363,  1367,  1371,  1375,  1380,  1385,  1390,
    1394,  1398,  1404,  1408,  1414,  1418,  1422,  1426,  1430,  1434,
    1438,  1442,  1446,  1452,  1457,  1462,  1466,  1470,  1474,  1478,
    1486,  1490,  1494,  1498,  1504,  1508,  1512,  1518,  1522,  1526,
    1532,  1536,  1540,  1544,  1548,  1552,  1558,  1562,  1566,  1570,
    1574,  1578,  1582,  1586,  1590,  1594,  1598,  1602,  1606,  1610,
    1614,  1618,  1622,  1626,  1630,  1634,  1638,  1642,  1646,  1650,
    1654,  1658,  1662,  1666,  1670,  1674,  1678,  1682,  1686,  1690,
    1694,  1698,  1702,  1706,  1710,  1714,  1718,  1722,  1726,  1730,
    1734,  1738,  1742,  1746,  1750,  1754,  1758,  1762,  1766,  1770,
    1774,  1778,  1782,  1786,  1790,  1794,  1798,  1802,  1806,  1810,
    1814,  1818,  1822,  1826,  1830,  1834,  1838,  1842,  1846,  1850,
    1854,  1858,  1862,  1866,  1870,  1874,  1878,  1882,  1886,  1890,
    1894,  1898,  1902,  1906,  1912,  1917,  1923,  1927,  1931,  1935,
    1939,  1943,  1947,  1951,  1955,  1959,  1963,  1967,  1971,  1975,
    1979,  1986,  1992,  1998,  2004,  2010,  2016,  2022,  2028,  2034,
    2040,  2046,  2052,  2060,  2066,  2073,  2077,  2083,  2087
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "CONSTANTTOKEN", "MIDPOINTCONSTANTTOKEN",
  "DYADICCONSTANTTOKEN", "HEXCONSTANTTOKEN", "HEXADECIMALCONSTANTTOKEN",
  "BINARYCONSTANTTOKEN", "PITOKEN", "IDENTIFIERTOKEN", "STRINGTOKEN",
  "LPARTOKEN", "RPARTOKEN", "LBRACKETTOKEN", "RBRACKETTOKEN", "EQUALTOKEN",
  "ASSIGNEQUALTOKEN", "COMPAREEQUALTOKEN", "COMMATOKEN",
  "EXCLAMATIONTOKEN", "SEMICOLONTOKEN", "STARLEFTANGLETOKEN",
  "LEFTANGLETOKEN", "RIGHTANGLEUNDERSCORETOKEN", "RIGHTANGLEDOTTOKEN",
  "RIGHTANGLESTARTOKEN", "RIGHTANGLETOKEN", "DOTSTOKEN", "DOTTOKEN",
  "QUESTIONMARKTOKEN", "VERTBARTOKEN", "ATTOKEN", "DOUBLECOLONTOKEN",
  "COLONTOKEN", "DOTCOLONTOKEN", "COLONDOTTOKEN", "EXCLAMATIONEQUALTOKEN",
  "APPROXTOKEN", "ANDTOKEN", "ORTOKEN", "PLUSTOKEN", "MINUSTOKEN",
  "MULTOKEN", "DIVTOKEN", "POWTOKEN", "SQRTTOKEN", "EXPTOKEN", "LOGTOKEN",
  "LOG2TOKEN", "LOG10TOKEN", "SINTOKEN", "COSTOKEN", "TANTOKEN",
  "ASINTOKEN", "ACOSTOKEN", "ATANTOKEN", "SINHTOKEN", "COSHTOKEN",
  "TANHTOKEN", "ASINHTOKEN", "ACOSHTOKEN", "ATANHTOKEN", "ABSTOKEN",
  "ERFTOKEN", "ERFCTOKEN", "LOG1PTOKEN", "EXPM1TOKEN", "DOUBLETOKEN",
  "SINGLETOKEN", "QUADTOKEN", "HALFPRECISIONTOKEN", "DOUBLEDOUBLETOKEN",
  "TRIPLEDOUBLETOKEN", "DOUBLEEXTENDEDTOKEN", "CEILTOKEN", "FLOORTOKEN",
  "NEARESTINTTOKEN", "HEADTOKEN", "REVERTTOKEN", "SORTTOKEN", "TAILTOKEN",
  "MANTISSATOKEN", "EXPONENTTOKEN", "PRECISIONTOKEN",
  "ROUNDCORRECTLYTOKEN", "PRECTOKEN", "POINTSTOKEN", "DIAMTOKEN",
  "DISPLAYTOKEN", "VERBOSITYTOKEN", "CANONICALTOKEN", "AUTOSIMPLIFYTOKEN",
  "TAYLORRECURSIONSTOKEN", "TIMINGTOKEN", "FULLPARENTHESESTOKEN",
  "MIDPOINTMODETOKEN", "DIEONERRORMODETOKEN", "SUPPRESSWARNINGSTOKEN",
  "HOPITALRECURSIONSTOKEN", "RATIONALMODETOKEN", "ONTOKEN", "OFFTOKEN",
  "DYADICTOKEN", "POWERSTOKEN", "BINARYTOKEN", "HEXADECIMALTOKEN",
  "FILETOKEN", "POSTSCRIPTTOKEN", "POSTSCRIPTFILETOKEN", "PERTURBTOKEN",
  "MINUSWORDTOKEN", "PLUSWORDTOKEN", "ZEROWORDTOKEN", "NEARESTTOKEN",
  "HONORCOEFFPRECTOKEN", "TRUETOKEN", "FALSETOKEN", "DEFAULTTOKEN",
  "MATCHTOKEN", "WITHTOKEN", "ABSOLUTETOKEN", "DECIMALTOKEN",
  "RELATIVETOKEN", "FIXEDTOKEN", "FLOATINGTOKEN", "ERRORTOKEN",
  "LIBRARYTOKEN", "LIBRARYCONSTANTTOKEN", "DIFFTOKEN", "BASHEVALUATETOKEN",
  "SIMPLIFYTOKEN", "REMEZTOKEN", "FPMINIMAXTOKEN", "HORNERTOKEN",
  "EXPANDTOKEN", "SIMPLIFYSAFETOKEN", "TAYLORTOKEN", "TAYLORFORMTOKEN",
  "AUTODIFFTOKEN", "DEGREETOKEN", "NUMERATORTOKEN", "DENOMINATORTOKEN",
  "SUBSTITUTETOKEN", "COEFFTOKEN", "SUBPOLYTOKEN",
  "ROUNDCOEFFICIENTSTOKEN", "RATIONALAPPROXTOKEN", "ACCURATEINFNORMTOKEN",
  "ROUNDTOFORMATTOKEN", "EVALUATETOKEN", "LENGTHTOKEN", "INFTOKEN",
  "MIDTOKEN", "SUPTOKEN", "MINTOKEN", "MAXTOKEN", "READXMLTOKEN",
  "PARSETOKEN", "PRINTTOKEN", "PRINTXMLTOKEN", "PLOTTOKEN",
  "PRINTHEXATOKEN", "PRINTFLOATTOKEN", "PRINTBINARYTOKEN",
  "PRINTEXPANSIONTOKEN", "BASHEXECUTETOKEN", "EXTERNALPLOTTOKEN",
  "WRITETOKEN", "ASCIIPLOTTOKEN", "RENAMETOKEN", "INFNORMTOKEN",
  "SUPNORMTOKEN", "FINDZEROSTOKEN", "FPFINDZEROSTOKEN",
  "DIRTYINFNORMTOKEN", "NUMBERROOTSTOKEN", "INTEGRALTOKEN",
  "DIRTYINTEGRALTOKEN", "WORSTCASETOKEN", "IMPLEMENTPOLYTOKEN",
  "IMPLEMENTCONSTTOKEN", "CHECKINFNORMTOKEN", "ZERODENOMINATORSTOKEN",
  "ISEVALUABLETOKEN", "SEARCHGALTOKEN", "GUESSDEGREETOKEN",
  "DIRTYFINDZEROSTOKEN", "IFTOKEN", "THENTOKEN", "ELSETOKEN", "FORTOKEN",
  "INTOKEN", "FROMTOKEN", "TOTOKEN", "BYTOKEN", "DOTOKEN", "BEGINTOKEN",
  "ENDTOKEN", "LEFTCURLYBRACETOKEN", "RIGHTCURLYBRACETOKEN", "WHILETOKEN",
  "READFILETOKEN", "ISBOUNDTOKEN", "EXECUTETOKEN", "FALSERESTARTTOKEN",
  "FALSEQUITTOKEN", "EXTERNALPROCTOKEN", "VOIDTOKEN", "CONSTANTTYPETOKEN",
  "FUNCTIONTOKEN", "RANGETOKEN", "INTEGERTOKEN", "STRINGTYPETOKEN",
  "BOOLEANTOKEN", "LISTTOKEN", "OFTOKEN", "VARTOKEN", "PROCTOKEN",
  "TIMETOKEN", "PROCEDURETOKEN", "RETURNTOKEN", "NOPTOKEN", "$accept",
  "startsymbol", "startsymbolwitherr", "beginsymbol", "endsymbol",
  "command", "ifcommand", "forcommand", "commandlist",
  "variabledeclarationlist", "variabledeclaration", "identifierlist",
  "procbody", "simplecommand", "assignment", "simpleassignment",
  "structuring", "stateassignment", "stillstateassignment", "thinglist",
  "structelementlist", "structelementseparator", "structelement", "thing",
  "supermegaterm", "indexing", "megaterm", "hyperterm", "unaryplusminus",
  "term", "subterm", "basicthing", "matchlist", "matchelement", "constant",
  "list", "simplelist", "range", "debound", "headfunction",
  "egalquestionmark", "statedereference", "externalproctype",
  "extendedexternalproctype", "externalproctypesimplelist",
  "externalproctypelist", YY_NULL
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,   472,   473,   474,
     475,   476,   477
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   223,   224,   225,   225,   226,   226,   227,   227,   228,
     228,   228,   228,   228,   228,   228,   228,   229,   229,   230,
     230,   230,   231,   231,   232,   232,   233,   234,   234,   235,
     235,   235,   235,   235,   235,   235,   235,   235,   235,   235,
     235,   235,   235,   235,   235,   235,   235,   235,   235,   235,
     235,   235,   235,   236,   236,   236,   236,   236,   236,   236,
     236,   236,   236,   236,   236,   236,   236,   236,   236,   236,
     236,   236,   236,   236,   236,   236,   236,   236,   236,   236,
     236,   236,   237,   237,   237,   237,   238,   238,   238,   238,
     238,   238,   238,   238,   239,   240,   240,   240,   240,   240,
     240,   240,   240,   240,   240,   240,   240,   240,   240,   240,
     241,   241,   241,   241,   241,   241,   241,   241,   241,   241,
     241,   241,   241,   241,   241,   242,   242,   243,   243,   244,
     244,   245,   246,   246,   247,   247,   247,   247,   248,   249,
     249,   249,   249,   249,   249,   249,   249,   250,   250,   250,
     250,   250,   250,   250,   251,   251,   251,   251,   252,   252,
     252,   252,   252,   252,   252,   252,   252,   253,   253,   253,
     253,   254,   254,   254,   254,   254,   254,   254,   254,   254,
     254,   254,   254,   254,   254,   254,   254,   254,   254,   254,
     254,   254,   254,   254,   254,   254,   254,   254,   254,   254,
     254,   254,   254,   254,   254,   254,   254,   254,   254,   254,
     254,   254,   254,   254,   254,   254,   254,   254,   254,   254,
     254,   254,   255,   255,   256,   256,   256,   256,   256,   256,
     256,   256,   256,   257,   257,   257,   257,   257,   257,   257,
     258,   258,   258,   258,   259,   259,   259,   260,   260,   260,
     261,   261,   261,   261,   261,   261,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   263,   263,   264,   264,   264,   264,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   264,
     264,   265,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   266,   266,   267,   267,   268,   268
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     2,     1,     1,     1,     1,     1,
       3,     4,     3,     2,     2,     4,     2,     3,     5,     7,
       9,     5,     2,     3,     2,     3,     2,     1,     3,     5,
       6,     5,     4,     8,     9,     8,     7,     6,     7,     6,
       5,     9,    10,     9,     8,     8,     9,     8,     7,    11,
      12,    11,    10,     1,     1,     1,     4,     3,     4,     6,
       7,     6,     4,     4,     4,     4,     4,    12,     4,     6,
       7,     6,     4,     4,     6,     7,    12,     6,    11,     1,
       1,     3,     1,     2,     1,     2,     3,     3,     6,     6,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     1,     3,     1,     3,     1,
       1,     4,     1,     4,     1,     3,     3,     2,     4,     1,
       3,     3,     3,     3,     4,     4,     3,     1,     3,     3,
       3,     3,     3,     3,     1,     1,     2,     2,     1,     2,
       2,     3,     3,     4,     4,     4,     4,     1,     3,     4,
       4,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     4,     4,     3,     1,
       1,     1,     1,     3,     3,     1,     1,     3,     6,     6,
       2,     4,     1,     2,     9,     6,     8,     5,     8,     5,
       7,     4,     5,     1,     1,     1,     1,     1,     1,     1,
       4,     3,     5,     6,     1,     3,     5,     5,     5,     3,
       3,     3,     3,     4,     4,     4,     4,     4,     6,     4,
       8,     4,     4,    10,     4,     4,     4,     4,     8,     8,
       8,     4,     4,     4,     6,     6,     6,     6,     6,     8,
       8,     6,     4,     4,     6,    12,     6,     6,     6,     6,
       6,     6,    14,     4,     8,     6,     6,     4,     8,     6,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     6,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     2,     0,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     1,     1,     1,     1,     1,     1,     3,     3,     3,
       3,     3,     3,     1,     1,     1,     3,     1,     3
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,     0,   233,   234,   235,   236,   237,   238,   239,   205,
     203,     0,     0,     0,     0,     0,   154,   155,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     196,   197,   198,   199,   201,   202,   200,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   345,   345,
     345,   345,   345,   345,   345,   345,   345,   345,   345,   345,
     345,   345,   345,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   188,
     189,     0,   191,   190,   192,   193,   194,   195,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     5,
       6,     0,     0,     0,     0,    54,    53,     0,   187,     0,
       0,     0,     0,    55,     0,     2,     0,     0,     9,    79,
      84,     0,    82,     0,    80,   125,   132,   216,   134,   139,
       0,   147,   158,   167,   204,   209,   210,   211,   212,   215,
       4,     0,     0,     0,   205,   345,   345,   345,   345,   345,
     345,   345,   345,   345,   345,   345,   345,   345,   345,   345,
       0,     0,   216,   167,     0,     0,     0,   137,     0,   160,
     156,   157,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   346,     0,   347,     0,   348,     0,   349,
       0,   350,     0,     0,   351,     0,   352,     0,   353,     0,
     354,     0,   355,     0,   356,     0,   357,     0,   359,     0,
     360,     0,   358,     0,   132,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    14,     0,     0,    16,     0,     0,
     127,     0,     0,     0,     0,     0,     0,     0,   220,     0,
       0,     0,     1,     7,     8,     0,    13,     0,     0,     0,
       0,     3,    85,     0,     0,    83,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   159,     0,     0,     0,     0,     0,   208,     0,
       0,     0,    86,    87,     0,   213,     0,     0,   244,     0,
     241,   249,     0,     0,   252,   251,   250,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   344,    95,    96,
      97,    98,    99,     0,   100,   101,   102,   103,   104,   105,
     106,   108,   109,   107,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   214,   129,
     130,     0,     0,     0,     0,     0,     0,     0,    27,     0,
       0,     0,    81,    57,     0,    27,    26,    22,    10,    12,
       0,    24,    92,    93,   126,   135,   136,    90,    91,   140,
       0,   142,     0,   143,   146,   141,   150,   151,   152,   153,
     148,   149,     0,     0,   161,     0,     0,   162,     0,   217,
       0,     0,   168,   207,     0,     0,     0,   217,   240,     0,
       0,     0,     0,     0,   309,   310,   313,   314,   315,   316,
     317,   318,   319,   320,   321,   322,   323,   324,   325,   326,
     327,   328,   329,   330,   331,   332,   333,   334,   335,   336,
     337,   338,   339,   340,   341,   342,   300,   303,   304,   308,
     305,   306,   307,   301,   265,     0,   133,   222,   256,   257,
       0,   259,     0,     0,   264,   266,   267,     0,     0,     0,
     271,   272,   273,     0,     0,     0,     0,     0,     0,     0,
       0,   343,   255,   254,   253,   261,   262,   283,   282,    58,
      72,     0,    62,    63,    64,    65,    66,     0,    68,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   293,     0,     0,     0,   297,     0,     0,    17,     0,
       0,     0,   128,    15,   302,   206,    73,     0,   311,     0,
       0,     0,     6,     0,     0,   221,    56,    23,    11,    25,
     144,   145,   165,   163,   166,   164,   138,     0,   170,   169,
       0,     0,     0,     0,   245,     0,   242,   247,   248,     0,
     223,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     131,     0,     0,     0,    28,     0,    32,     0,     0,     0,
       0,    88,    89,   219,     0,   243,     0,     0,   258,     0,
       0,     0,     0,     0,   274,   275,   276,   277,   278,     0,
       0,   281,     0,    59,     0,    74,    61,     0,     0,    69,
      71,    77,   284,     0,   286,   287,   288,   289,   290,   291,
       0,     0,     0,   295,   296,     0,   299,    18,    21,     0,
       0,   312,     0,     0,     0,    29,     0,    31,     0,     0,
      40,     0,     0,   218,   246,     0,     0,   231,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    60,    75,     0,
      70,     0,     0,     0,     0,     0,     0,     0,     0,   373,
     361,   362,   363,   364,   365,   366,     0,   374,   377,     0,
       0,     0,     0,     0,     0,    30,     0,     0,    37,     0,
      39,     0,   232,     0,     0,   229,     0,   227,     0,   260,
       0,   268,   269,   270,   279,   280,     0,     0,     0,     0,
     294,   298,     0,    19,   375,     0,     0,     0,     0,    48,
       0,     0,    36,     0,     0,     0,     0,     0,     0,     0,
      38,     0,     0,     0,     0,   225,     0,     0,     0,     0,
       0,     0,     0,   378,   367,   368,   369,   370,   371,   372,
       0,     0,     0,    45,     0,    47,     0,    33,    35,     0,
      44,     0,     0,     0,   230,     0,     0,     0,   263,     0,
       0,     0,     0,    20,   376,     0,     0,     0,     0,     0,
      46,    34,    41,    43,     0,   228,   226,     0,     0,     0,
       0,     0,    78,    52,     0,     0,     0,    42,   224,    67,
     285,    76,     0,    49,    51,     0,     0,    50,   292
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,   174,   175,   176,   386,   387,   364,   367,   388,   389,
     390,   570,   378,   178,   179,   180,   181,   182,   183,   184,
     369,   561,   370,   185,   186,   222,   188,   189,   190,   191,
     192,   223,   666,   667,   194,   195,   429,   196,   197,   198,
     273,   199,   917,   918,   955,   919
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -889
static const yytype_int16 yypact[] =
{
    1186,    13,  -889,  -889,  -889,  -889,  -889,  -889,  -889,   793,
    -889,  5996,  3826,  6647,  5996,  7515,    26,    26,    49,    81,
      96,   111,   170,   175,   184,   202,   256,   265,   267,   269,
     275,   277,   279,   308,   312,   323,   338,   350,   357,   365,
     367,   375,   391,   408,   440,   491,   504,   513,   517,   531,
     541,   561,   563,   566,   629,   637,   643,   648,    10,    68,
      84,   351,   390,   185,   392,   400,   443,   455,   463,   485,
     494,   505,   551,  -889,  -889,  -889,  -889,  -889,  -889,  -889,
    -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,
    -889,  5996,  -889,  -889,  -889,  -889,  -889,  -889,   670,   676,
     682,   683,   688,   718,   725,   729,   743,   750,   776,   783,
     791,   805,   809,   816,   818,   829,   833,   836,   842,   844,
     858,   866,   869,   873,   898,   902,   905,   915,   922,   933,
     940,   944,   955,   962,   967,   968,   976,   979,   980,   995,
    1002,  1004,  1016,  1030,  1056,  1058,  1060,  1061,  1064,  1065,
    1066,  1071,  1081,  1087,  1099,  1110,  1111,  5996,    44,  -889,
     113,  5996,  1124,  1135,  1148,  -889,  -889,  1151,  -889,  1154,
    1159,  1160,   179,  1161,   424,  -889,  2286,   389,  -889,  -889,
     441,   135,  -889,   492,  -889,   429,  -889,   669,    -4,  1398,
    7515,   432,  -889,    14,  -889,  -889,  -889,  -889,  -889,  -889,
    -889,  4260,  4043,  5996,  1167,   604,   604,   604,   604,   604,
     204,   604,   604,   604,   604,   604,   604,   604,   604,   604,
     113,    18,  -889,    21,  4477,   399,   678,    -4,  1397,  -889,
    -889,  -889,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,
    5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,
    5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,
    5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,
    5996,  5996,  4694,  -889,  4694,  -889,  4694,  -889,  4694,  -889,
    4694,  -889,  5996,  4694,  -889,  4694,  -889,  4694,  -889,  4694,
    -889,  4694,  -889,  4694,  -889,  4694,  -889,  4694,  -889,  4694,
    -889,  4694,  -889,   618,   343,  5996,  5996,  5996,  5996,  5996,
    5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,
    5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,
    5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,
    5996,  5996,  5996,  5996,  5996,  5996,   554,  5996,  5996,  5996,
    5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,
    5996,  5996,  5996,  5996,  -889,     5,   591,  -889,   615,   314,
     103,   -18,  5996,   620,  5996,   654,  5996,    43,  -889,  3606,
    1159,  4911,  -889,  -889,  -889,   680,  -889,   539,  -109,  3386,
     704,  -889,  -889,  5996,  5996,  -889,  5996,  6647,  6647,  5996,
    5996,  6647,  6213,  6430,  6647,  6647,  6647,  6647,  6647,  6647,
    6647,  6647,  -889,  6864,  7081,  5996,   723,  7298,  -889,   510,
    1168,  1169,   618,   618,   528,  1174,   734,   801,   618,   749,
    -889,  -889,  5996,  5996,  -889,  -889,  -889,    25,    35,    38,
      42,    47,    56,    58,    63,    66,    70,    72,    75,    77,
      79,    86,    88,    91,    94,   100,   108,   116,   119,   122,
     124,   128,   130,   136,   140,   144,   172,   253,   320,   333,
     341,   345,   353,   355,   358,   373,   394,  -889,   134,   542,
     772,   983,  1040,   396,  1055,  1062,  1076,  1078,  1080,  1085,
    1088,  1090,  1092,  1101,  5996,   398,    23,   402,   644,   652,
     404,   406,   410,   695,   708,   710,   414,   417,   426,   713,
     717,   719,   721,   724,   727,   732,   746,   434,   438,   442,
     445,   756,   851,   447,   449,   899,   451,   751,   454,   457,
     459,   467,   479,   754,   913,   757,   896,   760,   762,   768,
     787,   795,   799,   804,   813,   821,   840,   928,   843,   848,
     850,   969,   852,   854,  3606,  5996,  5996,   839,  -889,  -889,
    -889,   113,  3606,   487,   977,   496,  1107,    33,   254,   -61,
    1121,  1192,  -889,  -889,   498,  1155,  -889,  3606,  -889,  -889,
    -109,   992,   618,   618,  -889,    -4,    -4,   618,   618,  1398,
    6647,  1398,  6647,  1398,  1398,  1398,   432,   432,   432,   432,
     432,   432,  7515,  7515,  -889,  7515,  7515,  -889,   105,   860,
    7515,  7515,  -889,  -889,  5996,  5996,  5996,  1195,  -889,  5128,
    1179,  1197,   360,   537,  -889,  -889,  -889,  -889,  -889,  -889,
    -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,
    -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,
    -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,
    -889,  -889,  -889,  -889,  -889,   126,  -889,  5996,  -889,  -889,
    5996,  -889,  5996,  5996,  -889,  -889,  -889,  5996,  5996,  5996,
    -889,  -889,  -889,  5996,  5996,  5996,  5996,  5996,  5996,  5996,
    5996,  -889,  -889,  -889,  -889,  -889,  -889,  -889,  -889,  1187,
    1189,  5996,  -889,  -889,  -889,  -889,  -889,  5996,  1193,  5996,
    1203,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,  5996,
    5996,  -889,  5996,  5996,  5996,  -889,  5996,  5996,  1029,   -15,
       1,  5996,  -889,  -889,  -889,  -889,  -889,  5996,  -889,  5996,
    1194,   680,  -889,  1406,   -61,  -889,  -889,  -889,  -889,  -889,
    1398,  1398,  -889,  -889,  -889,  -889,  -889,  5996,  -889,  -889,
     500,   502,  1208,  1204,   618,  1210,  -889,  -889,  -889,    20,
    -889,   507,   892,   900,   903,   910,   914,   509,   511,   515,
     544,   546,   925,   929,   548,  5345,  5562,  1213,   932,  5779,
     550,  1218,  1293,   936,   552,   555,   557,   559,   567,   587,
     938,   947,   954,   589,   592,   970,   596,  3606,  3606,  5996,
     618,   978,   598,  1300,  -889,  5996,  -889,  -151,  2506,  1626,
    1301,  -889,  -889,  -889,  5996,  -889,  5996,  1846,  -889,  5996,
    5996,  5996,  5996,  5996,  -889,  -889,  -889,  -889,  -889,  5996,
    5996,  -889,  5996,   618,  5996,   618,  -889,  5996,  5996,   618,
    -889,  -889,  -889,  5996,  -889,  -889,  -889,  -889,  -889,  -889,
    5996,  5996,  5996,  -889,  -889,  5996,  -889,  -889,  -889,   -24,
      -5,  -889,   -61,   613,  5996,  -889,  5996,  -889,    64,  5996,
    -889,   157,  2726,  -889,   618,   600,  5996,  -889,   412,  2946,
    1362,   987,   606,  1365,   608,  1367,   611,   618,   618,   994,
     618,   996,  1000,  1018,   630,  1369,  5996,  3606,  1448,  -889,
    -889,  -889,  -889,  -889,  -889,  -889,  1170,  -889,  -889,  1334,
    2066,  -109,   990,  1112,  5996,  -889,  1114,  5996,  -889,  5996,
    -889,   444,  -889,  1116,  5996,  -889,  5996,  -889,   489,  -889,
    5996,  -889,  -889,  -889,  -889,  -889,  5996,  5996,  5996,  5996,
    -889,  -889,   -10,  -889,  1376,  1388,  1408,  1380,  5996,  -889,
     521,  3166,  -889,  -109,  -109,  1122,  -109,  1125,  1129,  5996,
    -889,  -109,  1136,  1162,  5996,  -889,  1411,  1022,  1046,  1048,
    1050,  3606,  1448,  -889,  -889,  -889,  -889,  -889,  -889,  -889,
    1441,  1164,  5996,  -889,  5996,  -889,   524,  -889,  -889,  -109,
    -889,  -109,  -109,  1178,  -889,  -109,  -109,  1190,  -889,  5996,
    5996,  5996,  5996,  -889,  -889,  1412,  -109,  1358,  1360,  5996,
    -889,  -889,  -889,  -889,  -109,  -889,  -889,  -109,  1414,   641,
    1416,  1052,  -889,  -889,  -109,  -109,  1363,  -889,  -889,  -889,
    -889,  -889,  5996,  -889,  -889,  -109,  1419,  -889,  -889
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -889,  -889,  -889,  -551,   143,    17,  -889,  -889,  -387,  -544,
    -889,  -377,  1039,  -889,  -889,  -889,  -889,  -889,  -889,   173,
     874,  -889,  -889,   -11,  1347,    39,    -7,  -392,   -12,  1431,
      12,   177,   774,  -889,  -889,  -889,  -889,  -889,  -889,  -889,
     837,  -889,  -888,   452,   461,  -889
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -125
static const yytype_int16 yytable[] =
{
     221,   226,   580,   228,   230,   231,   227,   908,   576,   589,
     591,   593,   594,   595,   401,   397,   398,   177,   743,   402,
     954,   397,   398,   403,   397,   398,   272,   229,   415,   397,
     398,   425,   826,   404,   200,   415,   669,   749,   624,   187,
     397,   398,   670,   416,   397,   398,   738,   383,   625,   384,
     426,   626,   739,   568,   366,   627,   569,   397,   398,   417,
     628,   232,   397,   398,   397,   398,   417,    16,    17,   629,
     874,   630,   397,   398,   397,   398,   631,   397,   398,   632,
     303,   397,   398,   633,   274,   634,   397,   398,   635,   383,
     636,   384,   637,   233,   954,   397,   398,   397,   398,   638,
     276,   639,   397,   398,   640,   397,   398,   641,   234,   397,
     398,   397,   398,   642,   397,   398,   397,   398,   397,   398,
     756,   643,   559,   235,   560,   397,   398,   397,   398,   644,
     397,   398,   645,   397,   398,   646,   159,   647,   742,   397,
     398,   648,   368,   649,   397,   398,   365,   397,   398,   650,
     371,   393,   394,   651,  -110,   397,   398,   652,   397,   398,
     769,   397,   398,   397,   398,   397,   398,   397,   398,   397,
     398,   906,   907,   397,   398,   397,   398,   193,   562,   397,
     398,   808,   236,   397,   398,   653,   981,   237,   405,   380,
     747,   422,   423,   819,   554,   809,   238,   282,   750,   818,
     751,   283,   412,   909,   910,   911,   912,   913,   914,   915,
     916,   397,   398,   428,   239,   187,   282,   159,   827,   742,
     424,   437,   438,   439,   440,   441,   442,   443,   444,   445,
     446,   447,   448,   449,   450,   451,   452,   453,   454,   455,
     456,   457,   458,   459,   460,   461,   462,   463,   464,   465,
     466,   467,   468,   469,   470,   471,   472,   473,   474,   475,
     476,   478,   383,   479,   384,   480,   654,   481,   240,   482,
     740,   483,   484,   741,   485,   882,   486,   241,   487,   242,
     488,   243,   489,   889,   490,   924,   491,   244,   492,   245,
     493,   246,   397,   398,   495,   496,   497,   498,   499,   500,
     501,   502,   503,   504,   505,   506,   507,   508,   509,   510,
     511,   512,   513,   514,   515,   516,   517,   518,   519,   520,
     247,   920,   523,   524,   248,   526,   527,   528,   529,   530,
     531,   532,   533,   655,   535,   249,   537,   538,   539,   540,
     541,   542,   543,   544,   545,   546,   656,   548,   549,   550,
     250,   552,   553,   193,   657,   383,   817,   384,   658,   397,
     398,   563,   251,   565,   814,   567,   659,   278,   660,   252,
     574,   661,   397,   398,   419,   767,   961,   253,   927,   254,
     397,   398,   582,   583,   397,   398,   662,   255,   587,   588,
     585,   586,   397,   398,   397,   398,   571,   397,   398,   397,
     398,   603,   606,   256,   608,   611,   280,   663,   285,   664,
     391,   668,   397,   398,   430,   671,   287,   674,   187,   675,
     257,   622,   623,   676,   382,   604,   607,   680,   187,   612,
     681,   878,   881,   397,   398,   397,   398,   397,   398,   682,
     888,   397,   398,   397,   398,   397,   398,   691,   396,   397,
     398,   692,   258,   397,   398,   693,   397,   398,   694,   289,
     697,   392,   698,   494,   700,   397,   398,   702,   397,   398,
     703,   291,   704,   397,   398,   413,   414,   397,   398,   293,
     705,   397,   398,   665,   397,   398,   397,   398,   397,   398,
     397,   398,   706,   397,   398,   931,   397,   398,   397,   398,
     734,   295,   938,   259,   521,   522,   397,   398,   525,   736,
     297,   746,   395,   821,   558,   822,   260,   534,   397,   398,
     828,   299,   834,   613,   835,   261,   397,   398,   836,   262,
     547,   578,   579,   960,   551,   397,   398,   397,   398,   397,
     398,   397,   398,   263,   729,   730,   397,   398,   397,   398,
     397,   398,   768,   264,   397,   398,   193,   837,   477,   838,
     577,   841,  -111,   850,   536,   854,   193,   301,   855,   584,
     856,   728,   857,   265,   996,   266,   397,   398,   267,   733,
     858,   397,   398,   397,   398,   397,   398,   397,   398,   397,
     398,   397,   398,   187,   397,   398,   397,   398,   397,   398,
     859,   187,   863,   760,   761,   864,   397,   398,   764,   866,
     383,   871,   384,   932,   752,   753,   187,   754,   755,   941,
     424,   943,   758,   759,   945,   557,   397,   398,   397,   398,
     564,   397,   398,   934,   921,   397,   398,   397,   398,   397,
     398,   268,   383,   950,   384,   397,   398,   397,   398,   269,
     397,   398,   397,   398,  1040,   270,   665,   397,   398,   771,
     271,   772,   773,   672,   566,   969,   774,   775,   776,   397,
     398,   673,   777,   778,   779,   780,   781,   782,   783,   784,
     397,   398,   305,   397,   398,   399,   400,   383,   306,   384,
     575,   397,   398,   431,   307,   308,   788,   432,   790,   433,
     309,   793,   794,   795,   796,   797,   798,   799,   800,   801,
     974,   802,   803,   804,   677,   805,   806,   397,   398,   383,
     810,   384,   383,   748,   384,   581,   811,   678,   812,   679,
     310,   193,   683,   609,   397,   398,   684,   311,   685,   193,
     686,   312,   992,   687,   617,  1019,   688,   397,   398,   397,
     398,   689,   397,   398,   193,   313,   397,   398,   397,   398,
     397,   398,   314,   397,   398,   690,   397,   398,   619,   695,
     701,   397,   398,   707,   843,   845,   709,   620,   849,   711,
     621,   712,   187,   555,   556,   397,   398,   713,   315,   762,
     397,   398,  -112,   397,   398,   316,   397,   398,   869,   397,
     398,   397,   398,   317,   873,   201,   714,   397,   398,   202,
     203,   397,   398,   884,   715,   885,   618,   318,   716,   891,
     892,   319,   894,   717,   867,   868,   397,   398,   320,   896,
     321,   897,   718,   898,   397,   398,   899,   900,   397,   398,
     719,   322,   901,   397,   398,   323,   187,   187,   324,   902,
     903,   904,   397,   398,   325,   731,   326,   187,   187,   720,
     397,   398,   722,   922,   696,   923,   187,   723,   926,   724,
     327,   726,   757,   727,   787,   933,   -94,   -94,   328,   397,
     398,   329,   397,   398,   792,   330,   816,   397,   398,   397,
     398,   397,   398,   397,   398,   952,   275,   277,   279,   281,
     284,   286,   288,   290,   292,   294,   296,   298,   300,   302,
     331,   829,   699,   965,   332,   710,   967,   333,   968,   830,
     193,   187,   831,   972,   953,   973,   708,   334,   187,   832,
     820,   397,   398,   833,   335,   977,   978,   979,   980,   397,
     398,   721,   397,   398,   839,   336,   187,   991,   840,   397,
     398,   847,   337,   397,   398,   853,   338,   860,  1003,   187,
     875,   877,   880,  1007,   397,   398,   861,   339,   397,   398,
     887,   397,   398,   862,   340,   397,   398,   397,   398,   341,
     342,  1017,   725,  1018,   193,   193,   397,   398,   343,   865,
     735,   344,   345,   397,   398,   193,   193,   870,  1013,  1029,
     187,  1031,   890,  -113,   193,   893,   940,   346,  1036,   397,
     398,   963,   895,   946,   347,   947,   348,   397,   398,   948,
     187,   925,   397,   398,   928,   930,   397,   398,   349,   397,
     398,   935,   937,   397,   398,   397,   398,   949,   905,   397,
     398,  1009,   350,   275,   277,   279,   281,   284,   286,   288,
     290,   292,   294,   296,   298,   300,   302,   397,   398,   193,
    -114,   397,   398,   959,   962,  1010,   193,  1011,   351,  1012,
     352,  1042,   353,   354,   970,  -115,   355,   356,   357,   397,
     398,   975,  -116,   358,   193,   397,   398,   397,   398,   397,
     398,   397,   398,   359,   397,   398,  -117,   193,  -118,   360,
    -119,   397,   398,   993,   995,  -120,   997,   998,  -121,  1000,
    -123,   361,  -124,   976,  1004,   397,   398,   397,   398,   397,
     398,  -122,   362,   363,   397,   398,   737,   397,   398,   397,
     398,   397,   398,   964,   744,   966,   372,   971,   193,  1020,
     397,   398,  1021,   999,  1022,  1023,  1001,   373,  1025,  1026,
    1002,   397,   398,   397,   398,   397,   398,  1005,   193,  1033,
     374,   397,   398,   375,   397,   398,   376,  1037,   397,   398,
    1038,   377,   379,   381,   741,   397,   398,  1043,  1044,   201,
     614,   615,  1028,  1006,  1030,  1016,   616,     1,  1047,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,  1024,
      12,   397,   398,   397,   398,   745,    13,   757,    14,   385,
     765,  1027,   766,   791,   785,  1046,   786,   397,   398,   807,
     789,   823,   813,   824,    15,   825,   846,    16,    17,   397,
     398,   851,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,   852,    92,    93,    94,
      95,    96,    97,   872,   883,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   939,   957,   158,   942,  1034,
     944,  1035,   951,   159,  1045,   160,   956,   161,   162,   163,
     164,   165,   166,   167,   168,   982,   169,   397,   398,   397,
     398,   983,   397,   398,   170,   171,   172,   990,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,   572,
      12,   434,   435,   436,  1008,  1032,    13,  1039,    14,  1041,
     406,   407,  1048,   408,   409,   732,   397,   398,   304,   410,
     411,   770,  1015,  1014,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,   383,   160,   384,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,   984,   985,   986,
     987,   988,   989,   385,   170,   171,   172,   815,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,   909,
     910,   911,   912,   913,   914,   915,   916,   910,   911,   912,
     913,   914,   915,   916,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,   383,   160,   384,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,   596,   597,   598,
     599,   600,   601,   385,   170,   171,   172,   879,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,   383,   160,   384,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,     0,     0,     0,
       0,     0,     0,   385,   170,   171,   172,   886,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,   383,   160,   384,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,     0,     0,     0,
       0,     0,     0,   385,   170,   171,   172,   958,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,   383,   160,   384,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,     0,     0,     0,
       0,     0,     0,   385,   170,   171,   172,     0,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,   383,   160,   384,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,     0,     0,     0,
       0,     0,     0,     0,   170,   171,   172,   876,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,   383,   160,   384,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,     0,     0,     0,
       0,     0,     0,     0,   170,   171,   172,   929,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,   383,   160,   384,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,     0,     0,     0,
       0,     0,     0,     0,   170,   171,   172,   936,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,   383,   160,   384,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,     0,     0,     0,
       0,     0,     0,     0,   170,   171,   172,   994,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,   383,   160,   384,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,     0,     0,     0,
       0,     0,     0,     0,   170,   171,   172,     0,   173,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,     0,     0,   158,     0,     0,
       0,     0,     0,   159,     0,   160,     0,   161,   162,   163,
     164,   165,   166,   167,   168,     0,   169,     0,     0,     0,
       0,     0,     0,     0,   170,   171,   172,     0,   173,     2,
       3,     4,     5,     6,     7,     8,   204,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,   224,     0,     0,
       0,     0,     0,     0,    15,     0,   225,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,   205,   206,   207,   208,   209,   210,   211,   212,
     213,   214,   215,   216,   217,   218,   219,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   140,   141,   142,
     143,   144,   145,   146,   147,     0,   149,   150,   151,   152,
     153,   154,   155,   156,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   220,     0,     0,   162,   163,
       0,     0,     0,     0,   168,     0,   169,     0,     0,     0,
       0,     0,     0,     0,   170,   171,     2,     3,     4,     5,
       6,     7,     8,   204,    10,    11,     0,    12,     0,     0,
       0,     0,     0,    13,     0,    14,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    15,     0,     0,    16,    17,     0,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,   205,
     206,   207,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,     0,    92,    93,    94,    95,    96,    97,
     420,   421,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   140,   141,   142,   143,   144,   145,
     146,   147,     0,   149,   150,   151,   152,   153,   154,   155,
     156,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   220,     0,     0,   162,   163,     0,     0,     0,
       0,   168,     0,   169,     0,     0,     0,     0,     0,     0,
       0,   170,   171,     2,     3,     4,     5,     6,     7,     8,
     204,    10,    11,   418,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    15,     0,
       0,    16,    17,     0,     0,     0,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,   205,   206,   207,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
       0,    92,    93,    94,    95,    96,    97,     0,     0,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   140,   141,   142,   143,   144,   145,   146,   147,     0,
     149,   150,   151,   152,   153,   154,   155,   156,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   220,
       0,     0,   162,   163,     0,     0,     0,     0,   168,     0,
     169,     0,     0,     0,     0,     0,     0,     0,   170,   171,
       2,     3,     4,     5,     6,     7,     8,   204,    10,    11,
       0,    12,     0,     0,     0,     0,     0,    13,     0,    14,
       0,     0,     0,     0,     0,     0,     0,     0,   427,     0,
       0,     0,     0,     0,     0,    15,     0,     0,    16,    17,
       0,     0,     0,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,   205,   206,   207,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,     0,    92,    93,
      94,    95,    96,    97,     0,     0,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   140,   141,
     142,   143,   144,   145,   146,   147,     0,   149,   150,   151,
     152,   153,   154,   155,   156,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   220,     0,     0,   162,
     163,     0,     0,     0,     0,   168,     0,   169,     0,     0,
       0,     0,     0,     0,     0,   170,   171,     2,     3,     4,
       5,     6,     7,     8,   204,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,     0,     0,     0,
       0,     0,     0,     0,   477,     0,     0,     0,     0,     0,
       0,     0,    15,     0,     0,    16,    17,     0,     0,     0,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,     0,    92,    93,    94,    95,    96,
      97,     0,     0,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   140,   141,   142,   143,   144,
     145,   146,   147,     0,   149,   150,   151,   152,   153,   154,
     155,   156,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   220,     0,     0,   162,   163,     0,     0,
       0,     0,   168,     0,   169,     0,     0,     0,     0,     0,
       0,     0,   170,   171,     2,     3,     4,     5,     6,     7,
       8,   204,    10,    11,   573,    12,     0,     0,     0,     0,
       0,    13,     0,    14,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    15,
       0,     0,    16,    17,     0,     0,     0,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,   205,   206,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,     0,    92,    93,    94,    95,    96,    97,     0,     0,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   140,   141,   142,   143,   144,   145,   146,   147,
       0,   149,   150,   151,   152,   153,   154,   155,   156,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     220,     0,     0,   162,   163,     0,     0,     0,     0,   168,
       0,   169,     0,     0,     0,     0,     0,     0,     0,   170,
     171,     2,     3,     4,     5,     6,     7,     8,   204,    10,
      11,     0,    12,     0,     0,     0,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,   763,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    15,     0,     0,    16,
      17,     0,     0,     0,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,   205,   206,   207,   208,   209,   210,
     211,   212,   213,   214,   215,   216,   217,   218,   219,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,     0,    92,
      93,    94,    95,    96,    97,     0,     0,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   140,
     141,   142,   143,   144,   145,   146,   147,     0,   149,   150,
     151,   152,   153,   154,   155,   156,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   220,     0,     0,
     162,   163,     0,     0,     0,     0,   168,     0,   169,     0,
       0,     0,     0,     0,     0,     0,   170,   171,     2,     3,
       4,     5,     6,     7,     8,   204,    10,    11,     0,    12,
       0,     0,     0,     0,     0,    13,     0,    14,     0,     0,
       0,     0,   842,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    15,     0,     0,    16,    17,     0,     0,
       0,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,   205,   206,   207,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,     0,    92,    93,    94,    95,
      96,    97,     0,     0,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   140,   141,   142,   143,
     144,   145,   146,   147,     0,   149,   150,   151,   152,   153,
     154,   155,   156,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   220,     0,     0,   162,   163,     0,
       0,     0,     0,   168,     0,   169,     0,     0,     0,     0,
       0,     0,     0,   170,   171,     2,     3,     4,     5,     6,
       7,     8,   204,    10,    11,     0,    12,     0,     0,     0,
       0,     0,    13,     0,    14,     0,     0,     0,     0,   844,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      15,     0,     0,    16,    17,     0,     0,     0,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,   205,   206,
     207,   208,   209,   210,   211,   212,   213,   214,   215,   216,
     217,   218,   219,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,     0,    92,    93,    94,    95,    96,    97,     0,
       0,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   140,   141,   142,   143,   144,   145,   146,
     147,     0,   149,   150,   151,   152,   153,   154,   155,   156,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   220,     0,     0,   162,   163,     0,     0,     0,     0,
     168,     0,   169,     0,     0,     0,     0,     0,     0,     0,
     170,   171,     2,     3,     4,     5,     6,     7,     8,   204,
      10,    11,     0,    12,     0,     0,     0,     0,     0,    13,
       0,    14,     0,     0,     0,     0,   848,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    15,     0,     0,
      16,    17,     0,     0,     0,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,   205,   206,   207,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,     0,
      92,    93,    94,    95,    96,    97,     0,     0,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     140,   141,   142,   143,   144,   145,   146,   147,     0,   149,
     150,   151,   152,   153,   154,   155,   156,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   220,     0,
       0,   162,   163,     0,     0,     0,     0,   168,     0,   169,
       0,     0,     0,     0,     0,     0,     0,   170,   171,     2,
       3,     4,     5,     6,     7,     8,   204,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,   205,   206,   207,   208,   209,   210,   211,   212,
     213,   214,   215,   216,   217,   218,   219,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,     0,    92,    93,    94,
      95,    96,    97,     0,     0,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   140,   141,   142,
     143,   144,   145,   146,   147,     0,   149,   150,   151,   152,
     153,   154,   155,   156,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   220,     0,     0,   162,   163,
       0,     0,     0,     0,   168,     0,   169,     0,     0,     0,
       0,     0,     0,     0,   170,   171,     2,     3,     4,     5,
       6,     7,     8,   204,    10,    11,     0,    12,     0,   590,
       0,     0,     0,     0,     0,    14,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    15,     0,     0,    16,    17,     0,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,   205,
     206,   207,   208,   209,   210,   211,   212,   213,   214,   215,
     216,   217,   218,   219,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,     0,     0,    92,    93,    94,    95,    96,    97,
       0,     0,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   140,   141,   142,   143,   144,   145,
     146,   147,     0,   149,   150,   151,   152,   153,   154,   155,
     156,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   220,     0,     0,   162,   163,     0,     0,     0,
       0,   168,     0,   169,     0,     0,     0,     0,     0,     0,
       0,   170,   171,     2,     3,     4,     5,     6,     7,     8,
     204,    10,    11,     0,    12,     0,   592,     0,     0,     0,
       0,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    15,     0,
       0,    16,    17,     0,     0,     0,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,   205,   206,   207,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,     0,
       0,    92,    93,    94,    95,    96,    97,     0,     0,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   140,   141,   142,   143,   144,   145,   146,   147,     0,
     149,   150,   151,   152,   153,   154,   155,   156,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   220,
       0,     0,   162,   163,     0,     0,     0,     0,   168,     0,
     169,     0,     0,     0,     0,     0,     0,     0,   170,   171,
       2,     3,     4,     5,     6,     7,     8,   204,    10,    11,
       0,    12,     0,     0,     0,     0,     0,     0,     0,    14,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    15,     0,     0,    16,    17,
       0,     0,     0,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,   205,   206,   207,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,     0,     0,    92,    93,
      94,    95,    96,    97,     0,     0,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   140,   141,
     142,   143,   144,   145,   146,   147,     0,   149,   150,   151,
     152,   153,   154,   155,   156,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   220,     0,     0,   162,
     163,     0,     0,     0,     0,   168,     0,   169,     0,     0,
       0,     0,     0,     0,     0,   170,   171,     2,     3,     4,
       5,     6,     7,     8,   204,    10,    11,     0,    12,     0,
       0,     0,     0,     0,     0,     0,    14,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   602,     0,     0,    16,    17,     0,     0,     0,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,     0,     0,    92,    93,    94,    95,    96,
      97,     0,     0,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   140,   141,   142,   143,   144,
     145,   146,   147,     0,   149,   150,   151,   152,   153,   154,
     155,   156,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   220,     0,     0,   162,   163,     0,     0,
       0,     0,   168,     0,   169,     0,     0,     0,     0,     0,
       0,     0,   170,   171,     2,     3,     4,     5,     6,     7,
       8,   204,    10,    11,     0,    12,     0,     0,     0,     0,
       0,     0,     0,    14,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   605,
       0,     0,    16,    17,     0,     0,     0,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,   205,   206,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
       0,     0,    92,    93,    94,    95,    96,    97,     0,     0,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   140,   141,   142,   143,   144,   145,   146,   147,
       0,   149,   150,   151,   152,   153,   154,   155,   156,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     220,     0,     0,   162,   163,     0,     0,     0,     0,   168,
       0,   169,     0,     0,     0,     0,     0,     0,     0,   170,
     171,     2,     3,     4,     5,     6,     7,     8,   204,    10,
      11,     0,    12,     0,     0,     0,     0,     0,     0,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   610,     0,     0,    16,
      17,     0,     0,     0,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,   205,   206,   207,   208,   209,   210,
     211,   212,   213,   214,   215,   216,   217,   218,   219,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,     0,     0,    92,
      93,    94,    95,    96,    97,     0,     0,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   140,
     141,   142,   143,   144,   145,   146,   147,     0,   149,   150,
     151,   152,   153,   154,   155,   156,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   220,     0,     0,
     162,   163,     0,     0,     0,     0,   168,     0,   169,     0,
       0,     0,     0,     0,     0,     0,   170,   171,     2,     3,
       4,     5,     6,     7,     8,   204,    10,    11,     0,    12,
       0,     0,     0,     0,     0,     0,     0,    14,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,   205,   206,   207,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,     0,     0,    92,    93,    94,    95,
      96,    97,     0,     0,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   140,   141,   142,   143,
     144,   145,   146,   147,     0,   149,   150,   151,   152,   153,
     154,   155,   156,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   220,     0,     0,   162,   163,     0,
       0,     0,     0,   168,     0,   169,     0,     0,     0,     0,
       0,     0,     0,   170,   171
};

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-889)))

#define yytable_value_is_error(Yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
      11,    12,   389,    14,    16,    17,    13,    12,   385,   401,
     402,   403,   404,   405,    18,    39,    40,     0,   569,    23,
     908,    39,    40,    27,    39,    40,    16,    15,    14,    39,
      40,    13,    12,    37,    21,    14,    13,   581,    13,     0,
      39,    40,    19,    29,    39,    40,    13,   198,    13,   200,
      29,    13,    19,    10,    10,    13,    13,    39,    40,    45,
      13,    12,    39,    40,    39,    40,    45,    41,    42,    13,
     221,    13,    39,    40,    39,    40,    13,    39,    40,    13,
      91,    39,    40,    13,    16,    13,    39,    40,    13,   198,
      13,   200,    13,    12,   982,    39,    40,    39,    40,    13,
      16,    13,    39,    40,    13,    39,    40,    13,    12,    39,
      40,    39,    40,    13,    39,    40,    39,    40,    39,    40,
      15,    13,    19,    12,    21,    39,    40,    39,    40,    13,
      39,    40,    13,    39,    40,    13,   197,    13,   199,    39,
      40,    13,    29,    13,    39,    40,   157,    39,    40,    13,
     161,    16,    17,    13,    20,    39,    40,    13,    39,    40,
      34,    39,    40,    39,    40,    39,    40,    39,    40,    39,
      40,   195,   196,    39,    40,    39,    40,     0,   196,    39,
      40,   196,    12,    39,    40,    13,   196,    12,   192,    10,
     577,   202,   203,   744,   189,   194,    12,    12,   590,   743,
     592,    16,   190,   208,   209,   210,   211,   212,   213,   214,
     215,    39,    40,   224,    12,   176,    12,   197,   769,   199,
      16,   232,   233,   234,   235,   236,   237,   238,   239,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     251,   252,   253,   254,   255,   256,   257,   258,   259,   260,
     261,   262,   263,   264,   265,   266,   267,   268,   269,   270,
     271,   272,   198,   274,   200,   276,    13,   278,    12,   280,
      16,   282,   283,    19,   285,   819,   287,    12,   289,    12,
     291,    12,   293,   827,   295,   221,   297,    12,   299,    12,
     301,    12,    39,    40,   305,   306,   307,   308,   309,   310,
     311,   312,   313,   314,   315,   316,   317,   318,   319,   320,
     321,   322,   323,   324,   325,   326,   327,   328,   329,   330,
      12,   872,   333,   334,    12,   336,   337,   338,   339,   340,
     341,   342,   343,    13,   345,    12,   347,   348,   349,   350,
     351,   352,   353,   354,   355,   356,    13,   358,   359,   360,
      12,   362,   363,   176,    13,   198,   743,   200,    13,    39,
      40,   372,    12,   374,   741,   376,    13,    16,    13,    12,
     381,    13,    39,    40,   201,    15,   920,    12,   221,    12,
      39,    40,   393,   394,    39,    40,    13,    12,   399,   400,
     397,   398,    39,    40,    39,    40,   379,    39,    40,    39,
      40,   413,   414,    12,   415,   417,    16,    13,    16,    13,
      21,    13,    39,    40,    15,    13,    16,    13,   379,    13,
      12,   432,   433,    13,     0,   413,   414,    13,   389,   417,
      13,   818,   819,    39,    40,    39,    40,    39,    40,    13,
     827,    39,    40,    39,    40,    39,    40,    13,    19,    39,
      40,    13,    12,    39,    40,    13,    39,    40,    13,    16,
      13,    20,    13,   120,    13,    39,    40,    13,    39,    40,
      13,    16,    13,    39,    40,    43,    44,    39,    40,    16,
      13,    39,    40,   494,    39,    40,    39,    40,    39,    40,
      39,    40,    13,    39,    40,   882,    39,    40,    39,    40,
      13,    16,   889,    12,   331,   332,    39,    40,   335,    13,
      16,    13,    20,    13,   200,    13,    12,   344,    39,    40,
      13,    16,    13,    13,    13,    12,    39,    40,    13,    12,
     357,   388,   389,   920,   361,    39,    40,    39,    40,    39,
      40,    39,    40,    12,   555,   556,    39,    40,    39,    40,
      39,    40,    15,    12,    39,    40,   379,    13,    30,    13,
      21,    13,    20,    13,    10,    13,   389,    16,    13,   396,
      13,   554,    13,    12,   961,    12,    39,    40,    12,   562,
      13,    39,    40,    39,    40,    39,    40,    39,    40,    39,
      40,    39,    40,   554,    39,    40,    39,    40,    39,    40,
      13,   562,    13,   614,   615,    13,    39,    40,   619,    13,
     198,    13,   200,    13,   602,   603,   577,   605,   606,    13,
      16,    13,   610,   611,    13,    10,    39,    40,    39,    40,
      10,    39,    40,   221,    21,    39,    40,    39,    40,    39,
      40,    12,   198,    13,   200,    39,    40,    39,    40,    12,
      39,    40,    39,    40,    13,    12,   667,    39,    40,   670,
      12,   672,   673,    19,    10,   221,   677,   678,   679,    39,
      40,    19,   683,   684,   685,   686,   687,   688,   689,   690,
      39,    40,    12,    39,    40,    16,    17,   198,    12,   200,
      10,    39,    40,    15,    12,    12,   707,    19,   709,    21,
      12,   712,   713,   714,   715,   716,   717,   718,   719,   720,
     221,   722,   723,   724,    19,   726,   727,    39,    40,   198,
     731,   200,   198,   580,   200,    21,   737,    19,   739,    19,
      12,   554,    19,    10,    39,    40,    19,    12,    19,   562,
      19,    12,   221,    19,    10,   221,    19,    39,    40,    39,
      40,    19,    39,    40,   577,    12,    39,    40,    39,    40,
      39,    40,    12,    39,    40,    19,    39,    40,    19,    13,
      19,    39,    40,    19,   785,   786,    19,    28,   789,    19,
      31,    19,   743,   192,   193,    39,    40,    19,    12,   616,
      39,    40,    20,    39,    40,    12,    39,    40,   809,    39,
      40,    39,    40,    12,   815,    12,    19,    39,    40,    16,
      17,    39,    40,   824,    19,   826,    15,    12,    19,   830,
     831,    12,   833,    19,   807,   808,    39,    40,    12,   840,
      12,   842,    19,   844,    39,    40,   847,   848,    39,    40,
      19,    12,   853,    39,    40,    12,   807,   808,    12,   860,
     861,   862,    39,    40,    12,    16,    12,   818,   819,    19,
      39,    40,    19,   874,    13,   876,   827,    19,   879,    19,
      12,    19,    12,    19,   701,   886,    16,    17,    12,    39,
      40,    12,    39,    40,   711,    12,   743,    39,    40,    39,
      40,    39,    40,    39,    40,   906,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      12,    19,    13,   924,    12,    19,   927,    12,   929,    19,
     743,   882,    19,   934,   907,   936,    13,    12,   889,    19,
     757,    39,    40,    19,    12,   946,   947,   948,   949,    39,
      40,    13,    39,    40,    19,    12,   907,   958,    19,    39,
      40,    19,    12,    39,    40,    19,    12,    19,   969,   920,
     817,   818,   819,   974,    39,    40,    19,    12,    39,    40,
     827,    39,    40,    19,    12,    39,    40,    39,    40,    12,
      12,   992,    13,   994,   807,   808,    39,    40,    12,    19,
      13,    12,    12,    39,    40,   818,   819,    19,   981,  1010,
     961,  1012,   829,    20,   827,   832,    19,    12,  1019,    39,
      40,    21,   839,    19,    12,    19,    12,    39,    40,    19,
     981,   878,    39,    40,   881,   882,    39,    40,    12,    39,
      40,   888,   889,    39,    40,    39,    40,    19,   865,    39,
      40,    19,    12,   206,   207,   208,   209,   210,   211,   212,
     213,   214,   215,   216,   217,   218,   219,    39,    40,   882,
      20,    39,    40,   920,   921,    19,   889,    19,    12,    19,
      12,    19,    12,    12,   931,    20,    12,    12,    12,    39,
      40,   938,    20,    12,   907,    39,    40,    39,    40,    39,
      40,    39,    40,    12,    39,    40,    20,   920,    20,    12,
      20,    39,    40,   960,   961,    20,   963,   964,    20,   966,
      20,    12,    20,   940,   971,    39,    40,    39,    40,    39,
      40,    20,    12,    12,    39,    40,    19,    39,    40,    39,
      40,    39,    40,    21,    13,    21,    12,    21,   961,   996,
      39,    40,   999,    21,  1001,  1002,    21,    12,  1005,  1006,
      21,    39,    40,    39,    40,    39,    40,    21,   981,  1016,
      12,    39,    40,    12,    39,    40,    12,  1024,    39,    40,
    1027,    12,    12,    12,    19,    39,    40,  1034,  1035,    12,
      12,    12,  1009,    21,  1011,    21,    12,     1,  1045,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    21,
      14,    39,    40,    39,    40,    13,    20,    12,    22,   217,
      31,    21,    15,    10,    27,  1042,    27,    39,    40,   190,
      27,    13,    28,    19,    38,    15,    13,    41,    42,    39,
      40,    13,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    13,   121,   122,   123,
     124,   125,   126,    13,    13,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    13,    42,   191,    13,    21,
      13,    21,    13,   197,    21,   199,   216,   201,   202,   203,
     204,   205,   206,   207,   208,    19,   210,    39,    40,    39,
      40,    13,    39,    40,   218,   219,   220,    27,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,   380,
      14,    24,    25,    26,    13,    13,    20,    13,    22,    13,
      32,    33,    13,    35,    36,   561,    39,    40,    91,    41,
      42,   667,   990,   982,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,   209,   210,   211,
     212,   213,   214,   217,   218,   219,   220,   221,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,   208,
     209,   210,   211,   212,   213,   214,   215,   209,   210,   211,
     212,   213,   214,   215,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,   406,   407,   408,
     409,   410,   411,   217,   218,   219,   220,   221,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,   217,   218,   219,   220,   221,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,   217,   218,   219,   220,   221,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,   217,   218,   219,   220,    -1,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   218,   219,   220,   221,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   218,   219,   220,   221,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   218,   219,   220,   221,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   218,   219,   220,   221,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   218,   219,   220,    -1,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,    -1,    -1,   191,    -1,    -1,
      -1,    -1,    -1,   197,    -1,   199,    -1,   201,   202,   203,
     204,   205,   206,   207,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   218,   219,   220,    -1,   222,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    31,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    40,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   171,   172,   173,
     174,   175,   176,   177,   178,    -1,   180,   181,   182,   183,
     184,   185,   186,   187,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   199,    -1,    -1,   202,   203,
      -1,    -1,    -1,    -1,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   218,   219,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    -1,    14,    -1,    -1,
      -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,    -1,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   171,   172,   173,   174,   175,   176,
     177,   178,    -1,   180,   181,   182,   183,   184,   185,   186,
     187,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   199,    -1,    -1,   202,   203,    -1,    -1,    -1,
      -1,   208,    -1,   210,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   218,   219,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
      -1,   121,   122,   123,   124,   125,   126,    -1,    -1,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   171,   172,   173,   174,   175,   176,   177,   178,    -1,
     180,   181,   182,   183,   184,   185,   186,   187,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   199,
      -1,    -1,   202,   203,    -1,    -1,    -1,    -1,   208,    -1,
     210,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   218,   219,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    31,    -1,
      -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,
      -1,    -1,    -1,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,    -1,   121,   122,
     123,   124,   125,   126,    -1,    -1,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   171,   172,
     173,   174,   175,   176,   177,   178,    -1,   180,   181,   182,
     183,   184,   185,   186,   187,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   199,    -1,    -1,   202,
     203,    -1,    -1,    -1,    -1,   208,    -1,   210,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   218,   219,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    30,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,    -1,   121,   122,   123,   124,   125,
     126,    -1,    -1,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   171,   172,   173,   174,   175,
     176,   177,   178,    -1,   180,   181,   182,   183,   184,   185,
     186,   187,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   199,    -1,    -1,   202,   203,    -1,    -1,
      -1,    -1,   208,    -1,   210,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   218,   219,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,
      -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,    -1,   121,   122,   123,   124,   125,   126,    -1,    -1,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   171,   172,   173,   174,   175,   176,   177,   178,
      -1,   180,   181,   182,   183,   184,   185,   186,   187,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     199,    -1,    -1,   202,   203,    -1,    -1,    -1,    -1,   208,
      -1,   210,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   218,
     219,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    28,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,    -1,   121,
     122,   123,   124,   125,   126,    -1,    -1,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   171,
     172,   173,   174,   175,   176,   177,   178,    -1,   180,   181,
     182,   183,   184,   185,   186,   187,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   199,    -1,    -1,
     202,   203,    -1,    -1,    -1,    -1,   208,    -1,   210,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   218,   219,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    -1,    14,
      -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,
      -1,    -1,    27,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,
      -1,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,    -1,   121,   122,   123,   124,
     125,   126,    -1,    -1,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   171,   172,   173,   174,
     175,   176,   177,   178,    -1,   180,   181,   182,   183,   184,
     185,   186,   187,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   199,    -1,    -1,   202,   203,    -1,
      -1,    -1,    -1,   208,    -1,   210,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   218,   219,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    27,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,    -1,   121,   122,   123,   124,   125,   126,    -1,
      -1,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   171,   172,   173,   174,   175,   176,   177,
     178,    -1,   180,   181,   182,   183,   184,   185,   186,   187,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   199,    -1,    -1,   202,   203,    -1,    -1,    -1,    -1,
     208,    -1,   210,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     218,   219,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,
      -1,    22,    -1,    -1,    -1,    -1,    27,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,
      41,    42,    -1,    -1,    -1,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,    -1,
     121,   122,   123,   124,   125,   126,    -1,    -1,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     171,   172,   173,   174,   175,   176,   177,   178,    -1,   180,
     181,   182,   183,   184,   185,   186,   187,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   199,    -1,
      -1,   202,   203,    -1,    -1,    -1,    -1,   208,    -1,   210,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   218,   219,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,   121,   122,   123,
     124,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   171,   172,   173,
     174,   175,   176,   177,   178,    -1,   180,   181,   182,   183,
     184,   185,   186,   187,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   199,    -1,    -1,   202,   203,
      -1,    -1,    -1,    -1,   208,    -1,   210,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   218,   219,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    -1,    14,    -1,    16,
      -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,    -1,    -1,   121,   122,   123,   124,   125,   126,
      -1,    -1,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   171,   172,   173,   174,   175,   176,
     177,   178,    -1,   180,   181,   182,   183,   184,   185,   186,
     187,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   199,    -1,    -1,   202,   203,    -1,    -1,    -1,
      -1,   208,    -1,   210,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   218,   219,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    16,    -1,    -1,    -1,
      -1,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,    -1,
      -1,   121,   122,   123,   124,   125,   126,    -1,    -1,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   171,   172,   173,   174,   175,   176,   177,   178,    -1,
     180,   181,   182,   183,   184,   185,   186,   187,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   199,
      -1,    -1,   202,   203,    -1,    -1,    -1,    -1,   208,    -1,
     210,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   218,   219,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,
      -1,    -1,    -1,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,    -1,    -1,   121,   122,
     123,   124,   125,   126,    -1,    -1,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   171,   172,
     173,   174,   175,   176,   177,   178,    -1,   180,   181,   182,
     183,   184,   185,   186,   187,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   199,    -1,    -1,   202,
     203,    -1,    -1,    -1,    -1,   208,    -1,   210,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   218,   219,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,    -1,    -1,   121,   122,   123,   124,   125,
     126,    -1,    -1,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   171,   172,   173,   174,   175,
     176,   177,   178,    -1,   180,   181,   182,   183,   184,   185,
     186,   187,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   199,    -1,    -1,   202,   203,    -1,    -1,
      -1,    -1,   208,    -1,   210,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   218,   219,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,
      -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
      -1,    -1,   121,   122,   123,   124,   125,   126,    -1,    -1,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   171,   172,   173,   174,   175,   176,   177,   178,
      -1,   180,   181,   182,   183,   184,   185,   186,   187,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     199,    -1,    -1,   202,   203,    -1,    -1,    -1,    -1,   208,
      -1,   210,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   218,
     219,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,    -1,    -1,   121,
     122,   123,   124,   125,   126,    -1,    -1,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   171,
     172,   173,   174,   175,   176,   177,   178,    -1,   180,   181,
     182,   183,   184,   185,   186,   187,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   199,    -1,    -1,
     202,   203,    -1,    -1,    -1,    -1,   208,    -1,   210,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   218,   219,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    -1,    14,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,    -1,    -1,   121,   122,   123,   124,
     125,   126,    -1,    -1,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   171,   172,   173,   174,
     175,   176,   177,   178,    -1,   180,   181,   182,   183,   184,
     185,   186,   187,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   199,    -1,    -1,   202,   203,    -1,
      -1,    -1,    -1,   208,    -1,   210,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   218,   219
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,     1,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    14,    20,    22,    38,    41,    42,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   121,   122,   123,   124,   125,   126,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
     171,   172,   173,   174,   175,   176,   177,   178,   179,   180,
     181,   182,   183,   184,   185,   186,   187,   188,   191,   197,
     199,   201,   202,   203,   204,   205,   206,   207,   208,   210,
     218,   219,   220,   222,   224,   225,   226,   228,   236,   237,
     238,   239,   240,   241,   242,   246,   247,   248,   249,   250,
     251,   252,   253,   254,   257,   258,   260,   261,   262,   264,
      21,    12,    16,    17,    10,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     199,   246,   248,   254,    31,    40,   246,   249,   246,   253,
     251,   251,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    16,   263,    16,   263,    16,   263,    16,   263,
      16,   263,    12,    16,   263,    16,   263,    16,   263,    16,
     263,    16,   263,    16,   263,    16,   263,    16,   263,    16,
     263,    16,   263,   246,   247,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,   229,   246,    10,   230,    29,   243,
     245,   246,    12,    12,    12,    12,    12,    12,   235,    12,
      10,    12,     0,   198,   200,   217,   227,   228,   231,   232,
     233,    21,    20,    16,    17,    20,    19,    39,    40,    16,
      17,    18,    23,    27,    37,   192,    32,    33,    35,    36,
      41,    42,   253,    43,    44,    14,    29,    45,    13,   242,
     127,   128,   246,   246,    16,    13,    29,    31,   246,   259,
      15,    15,    19,    21,    24,    25,    26,   246,   246,   246,
     246,   246,   246,   246,   246,   246,   246,   246,   246,   246,
     246,   246,   246,   246,   246,   246,   246,   246,   246,   246,
     246,   246,   246,   246,   246,   246,   246,   246,   246,   246,
     246,   246,   246,   246,   246,   246,   246,    30,   246,   246,
     246,   246,   246,   246,   246,   246,   246,   246,   246,   246,
     246,   246,   246,   246,   120,   246,   246,   246,   246,   246,
     246,   246,   246,   246,   246,   246,   246,   246,   246,   246,
     246,   246,   246,   246,   246,   246,   246,   246,   246,   246,
     246,   242,   242,   246,   246,   242,   246,   246,   246,   246,
     246,   246,   246,   246,   242,   246,    10,   246,   246,   246,
     246,   246,   246,   246,   246,   246,   246,   242,   246,   246,
     246,   242,   246,   246,   189,   192,   193,    10,   200,    19,
      21,   244,   196,   246,    10,   246,    10,   246,    10,    13,
     234,   228,   235,    13,   246,    10,   234,    21,   227,   227,
     231,    21,   246,   246,   242,   249,   249,   246,   246,   250,
      16,   250,    16,   250,   250,   250,   252,   252,   252,   252,
     252,   252,    38,   251,   253,    38,   251,   253,   246,    10,
      38,   251,   253,    13,    12,    12,    12,    10,    15,    19,
      28,    31,   246,   246,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,   246,   255,   256,    13,    13,
      19,    13,    19,    19,    13,    13,    13,    19,    19,    19,
      13,    13,    13,    19,    19,    19,    19,    19,    19,    19,
      19,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    19,    13,    13,    13,    13,    13,    19,    13,    19,
      19,    19,    19,    19,    19,    19,    19,    19,    19,    19,
      19,    13,    19,    19,    19,    13,    19,    19,   228,   246,
     246,    16,   243,   228,    13,    13,    13,    19,    13,    19,
      16,    19,   199,   226,    13,    13,    13,   231,   227,   232,
     250,   250,   253,   253,   253,   253,    15,    12,   253,   253,
     246,   246,   242,    28,   246,    31,    15,    15,    15,    34,
     255,   246,   246,   246,   246,   246,   246,   246,   246,   246,
     246,   246,   246,   246,   246,    27,    27,   242,   246,    27,
     246,    10,   242,   246,   246,   246,   246,   246,   246,   246,
     246,   246,   246,   246,   246,   246,   246,   190,   196,   194,
     246,   246,   246,    28,   234,   221,   227,   231,   232,   226,
     242,    13,    13,    13,    19,    15,    12,   226,    13,    19,
      19,    19,    19,    19,    13,    13,    13,    13,    13,    19,
      19,    13,    27,   246,    27,   246,    13,    19,    27,   246,
      13,    13,    13,    19,    13,    13,    13,    13,    13,    13,
      19,    19,    19,    13,    13,    19,    13,   228,   228,   246,
      19,    13,    13,   246,   221,   227,   221,   227,   231,   221,
     227,   231,   232,    13,   246,   246,   221,   227,   231,   232,
     242,   246,   246,   242,   246,   242,   246,   246,   246,   246,
     246,   246,   246,   246,   246,   242,   195,   196,    12,   208,
     209,   210,   211,   212,   213,   214,   215,   265,   266,   268,
     226,    21,   246,   246,   221,   227,   246,   221,   227,   221,
     227,   231,    13,   246,   221,   227,   221,   227,   231,    13,
      19,    13,    13,    13,    13,    13,    19,    19,    19,    19,
      13,    13,   246,   228,   265,   267,   216,    42,   221,   227,
     231,   232,   227,    21,    21,   246,    21,   246,   246,   221,
     227,    21,   246,   246,   221,   227,   242,   246,   246,   246,
     246,   196,    19,    13,   209,   210,   211,   212,   213,   214,
      27,   246,   221,   227,   221,   227,   231,   227,   227,    21,
     227,    21,    21,   246,   227,    21,    21,   246,    13,    19,
      19,    19,    19,   228,   267,   266,    21,   246,   246,   221,
     227,   227,   227,   227,    21,   227,   227,    21,   242,   246,
     242,   246,    13,   227,    21,    21,   246,   227,   227,    13,
      13,    13,    19,   227,   227,    21,   242,   227,    13
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (myScanner, YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))

/* Error token number */
#define YYTERROR	1
#define YYERRCODE	256


/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */
#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval, myScanner)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value, myScanner); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, void *myScanner)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep, myScanner)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    void *myScanner;
#endif
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
  YYUSE (myScanner);
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, void *myScanner)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep, myScanner)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    void *myScanner;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep, myScanner);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule, void *myScanner)
#else
static void
yy_reduce_print (yyvsp, yyrule, myScanner)
    YYSTYPE *yyvsp;
    int yyrule;
    void *myScanner;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       , myScanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule, myScanner); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULL, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULL;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULL, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, void *myScanner)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, myScanner)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    void *myScanner;
#endif
{
  YYUSE (yyvaluep);
  YYUSE (myScanner);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YYUSE (yytype);
}




/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *myScanner)
#else
int
yyparse (myScanner)
    void *myScanner;
#endif
#endif
{
/* The lookahead symbol.  */
int yychar;


#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
static YYSTYPE yyval_default;
# define YY_INITIAL_VALUE(Value) = Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval YY_INITIAL_VALUE(yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
/* Line 1787 of yacc.c  */
#line 405 "internparser.y"
    {
			    parsedThingIntern = (yyvsp[(1) - (1)].tree);
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
    break;

  case 3:
/* Line 1787 of yacc.c  */
#line 414 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (2)].tree);
			  }
    break;

  case 4:
/* Line 1787 of yacc.c  */
#line 418 "internparser.y"
    {
			    (yyval.tree) = NULL;
			  }
    break;

  case 5:
/* Line 1787 of yacc.c  */
#line 424 "internparser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 6:
/* Line 1787 of yacc.c  */
#line 428 "internparser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 7:
/* Line 1787 of yacc.c  */
#line 434 "internparser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 8:
/* Line 1787 of yacc.c  */
#line 438 "internparser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 9:
/* Line 1787 of yacc.c  */
#line 444 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 10:
/* Line 1787 of yacc.c  */
#line 448 "internparser.y"
    {
			    (yyval.tree) = makeCommandList((yyvsp[(2) - (3)].list));
                          }
    break;

  case 11:
/* Line 1787 of yacc.c  */
#line 452 "internparser.y"
    {
			    (yyval.tree) = makeCommandList(concatChains((yyvsp[(2) - (4)].list), (yyvsp[(3) - (4)].list)));
                          }
    break;

  case 12:
/* Line 1787 of yacc.c  */
#line 456 "internparser.y"
    {
			    (yyval.tree) = makeCommandList((yyvsp[(2) - (3)].list));
                          }
    break;

  case 13:
/* Line 1787 of yacc.c  */
#line 460 "internparser.y"
    {
			    (yyval.tree) = makeNop();
                          }
    break;

  case 14:
/* Line 1787 of yacc.c  */
#line 464 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(2) - (2)].tree);
			  }
    break;

  case 15:
/* Line 1787 of yacc.c  */
#line 468 "internparser.y"
    {
			    (yyval.tree) = makeWhile((yyvsp[(2) - (4)].tree), (yyvsp[(4) - (4)].tree));
			  }
    break;

  case 16:
/* Line 1787 of yacc.c  */
#line 472 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(2) - (2)].tree);
			  }
    break;

  case 17:
/* Line 1787 of yacc.c  */
#line 478 "internparser.y"
    {
			    (yyval.tree) = makeIf((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
                          }
    break;

  case 18:
/* Line 1787 of yacc.c  */
#line 482 "internparser.y"
    {
			    (yyval.tree) = makeIfElse((yyvsp[(1) - (5)].tree),(yyvsp[(3) - (5)].tree),(yyvsp[(5) - (5)].tree));
                          }
    break;

  case 19:
/* Line 1787 of yacc.c  */
#line 490 "internparser.y"
    {
			    (yyval.tree) = makeFor((yyvsp[(1) - (7)].value), (yyvsp[(3) - (7)].tree), (yyvsp[(5) - (7)].tree), makeConstantDouble(1.0), (yyvsp[(7) - (7)].tree));
			    free((yyvsp[(1) - (7)].value));
                          }
    break;

  case 20:
/* Line 1787 of yacc.c  */
#line 495 "internparser.y"
    {
			    (yyval.tree) = makeFor((yyvsp[(1) - (9)].value), (yyvsp[(3) - (9)].tree), (yyvsp[(5) - (9)].tree), (yyvsp[(7) - (9)].tree), (yyvsp[(9) - (9)].tree));
			    free((yyvsp[(1) - (9)].value));
                          }
    break;

  case 21:
/* Line 1787 of yacc.c  */
#line 500 "internparser.y"
    {
			    (yyval.tree) = makeForIn((yyvsp[(1) - (5)].value), (yyvsp[(3) - (5)].tree), (yyvsp[(5) - (5)].tree));
			    free((yyvsp[(1) - (5)].value));
                          }
    break;

  case 22:
/* Line 1787 of yacc.c  */
#line 508 "internparser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (2)].tree));
			  }
    break;

  case 23:
/* Line 1787 of yacc.c  */
#line 512 "internparser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].tree));
			  }
    break;

  case 24:
/* Line 1787 of yacc.c  */
#line 518 "internparser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (2)].tree));
			  }
    break;

  case 25:
/* Line 1787 of yacc.c  */
#line 522 "internparser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].tree));
			  }
    break;

  case 26:
/* Line 1787 of yacc.c  */
#line 528 "internparser.y"
    {
			    (yyval.tree) = makeVariableDeclaration((yyvsp[(2) - (2)].list));
			  }
    break;

  case 27:
/* Line 1787 of yacc.c  */
#line 535 "internparser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].value));
			  }
    break;

  case 28:
/* Line 1787 of yacc.c  */
#line 539 "internparser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].value));
			  }
    break;

  case 29:
/* Line 1787 of yacc.c  */
#line 545 "internparser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[(4) - (5)].list)), makeUnit());
                          }
    break;

  case 30:
/* Line 1787 of yacc.c  */
#line 549 "internparser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(concatChains((yyvsp[(4) - (6)].list), (yyvsp[(5) - (6)].list))), makeUnit());
                          }
    break;

  case 31:
/* Line 1787 of yacc.c  */
#line 553 "internparser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[(4) - (5)].list)), makeUnit());
                          }
    break;

  case 32:
/* Line 1787 of yacc.c  */
#line 557 "internparser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
    break;

  case 33:
/* Line 1787 of yacc.c  */
#line 561 "internparser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[(4) - (8)].list)), (yyvsp[(6) - (8)].tree));
                          }
    break;

  case 34:
/* Line 1787 of yacc.c  */
#line 565 "internparser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(concatChains((yyvsp[(4) - (9)].list), (yyvsp[(5) - (9)].list))), (yyvsp[(7) - (9)].tree));
                          }
    break;

  case 35:
/* Line 1787 of yacc.c  */
#line 569 "internparser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[(4) - (8)].list)), (yyvsp[(6) - (8)].tree));
                          }
    break;

  case 36:
/* Line 1787 of yacc.c  */
#line 573 "internparser.y"
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(addElement(NULL,makeNop())), (yyvsp[(5) - (7)].tree));
                          }
    break;

  case 37:
/* Line 1787 of yacc.c  */
#line 577 "internparser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (6)].list), makeCommandList((yyvsp[(5) - (6)].list)), makeUnit());
                          }
    break;

  case 38:
/* Line 1787 of yacc.c  */
#line 581 "internparser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (7)].list), makeCommandList(concatChains((yyvsp[(5) - (7)].list), (yyvsp[(6) - (7)].list))), makeUnit());
                          }
    break;

  case 39:
/* Line 1787 of yacc.c  */
#line 585 "internparser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (6)].list), makeCommandList((yyvsp[(5) - (6)].list)), makeUnit());
                          }
    break;

  case 40:
/* Line 1787 of yacc.c  */
#line 589 "internparser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (5)].list), makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
    break;

  case 41:
/* Line 1787 of yacc.c  */
#line 593 "internparser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (9)].list), makeCommandList((yyvsp[(5) - (9)].list)), (yyvsp[(7) - (9)].tree));
                          }
    break;

  case 42:
/* Line 1787 of yacc.c  */
#line 597 "internparser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (10)].list), makeCommandList(concatChains((yyvsp[(5) - (10)].list), (yyvsp[(6) - (10)].list))), (yyvsp[(8) - (10)].tree));
                          }
    break;

  case 43:
/* Line 1787 of yacc.c  */
#line 601 "internparser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (9)].list), makeCommandList((yyvsp[(5) - (9)].list)), (yyvsp[(7) - (9)].tree));
                          }
    break;

  case 44:
/* Line 1787 of yacc.c  */
#line 605 "internparser.y"
    {
			    (yyval.tree) = makeProc((yyvsp[(2) - (8)].list), makeCommandList(addElement(NULL, makeNop())), (yyvsp[(6) - (8)].tree));
                          }
    break;

  case 45:
/* Line 1787 of yacc.c  */
#line 609 "internparser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (8)].value), makeCommandList((yyvsp[(7) - (8)].list)), makeUnit());
                          }
    break;

  case 46:
/* Line 1787 of yacc.c  */
#line 613 "internparser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (9)].value), makeCommandList(concatChains((yyvsp[(7) - (9)].list), (yyvsp[(8) - (9)].list))), makeUnit());
                          }
    break;

  case 47:
/* Line 1787 of yacc.c  */
#line 617 "internparser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (8)].value), makeCommandList((yyvsp[(7) - (8)].list)), makeUnit());
                          }
    break;

  case 48:
/* Line 1787 of yacc.c  */
#line 621 "internparser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (7)].value), makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
    break;

  case 49:
/* Line 1787 of yacc.c  */
#line 625 "internparser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (11)].value), makeCommandList((yyvsp[(7) - (11)].list)), (yyvsp[(9) - (11)].tree));
                          }
    break;

  case 50:
/* Line 1787 of yacc.c  */
#line 629 "internparser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (12)].value), makeCommandList(concatChains((yyvsp[(7) - (12)].list), (yyvsp[(8) - (12)].list))), (yyvsp[(10) - (12)].tree));
                          }
    break;

  case 51:
/* Line 1787 of yacc.c  */
#line 633 "internparser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (11)].value), makeCommandList((yyvsp[(7) - (11)].list)), (yyvsp[(9) - (11)].tree));
                          }
    break;

  case 52:
/* Line 1787 of yacc.c  */
#line 637 "internparser.y"
    {
			    (yyval.tree) = makeProcIllim((yyvsp[(2) - (10)].value), makeCommandList(addElement(NULL, makeNop())), (yyvsp[(8) - (10)].tree));
                          }
    break;

  case 53:
/* Line 1787 of yacc.c  */
#line 645 "internparser.y"
    {
			    (yyval.tree) = makeQuit();
			  }
    break;

  case 54:
/* Line 1787 of yacc.c  */
#line 649 "internparser.y"
    {
			    (yyval.tree) = makeFalseRestart();
			  }
    break;

  case 55:
/* Line 1787 of yacc.c  */
#line 653 "internparser.y"
    {
			    (yyval.tree) = makeNop();
			  }
    break;

  case 56:
/* Line 1787 of yacc.c  */
#line 657 "internparser.y"
    {
			    (yyval.tree) = makeNopArg((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 57:
/* Line 1787 of yacc.c  */
#line 661 "internparser.y"
    {
			    (yyval.tree) = makeNopArg(makeDefault());
			  }
    break;

  case 58:
/* Line 1787 of yacc.c  */
#line 665 "internparser.y"
    {
			    (yyval.tree) = makePrint((yyvsp[(3) - (4)].list));
			  }
    break;

  case 59:
/* Line 1787 of yacc.c  */
#line 669 "internparser.y"
    {
			    (yyval.tree) = makeNewFilePrint((yyvsp[(6) - (6)].tree), (yyvsp[(3) - (6)].list));
			  }
    break;

  case 60:
/* Line 1787 of yacc.c  */
#line 673 "internparser.y"
    {
			    (yyval.tree) = makeAppendFilePrint((yyvsp[(7) - (7)].tree), (yyvsp[(3) - (7)].list));
			  }
    break;

  case 61:
/* Line 1787 of yacc.c  */
#line 677 "internparser.y"
    {
			    (yyval.tree) = makePlot(addElement((yyvsp[(5) - (6)].list), (yyvsp[(3) - (6)].tree)));
			  }
    break;

  case 62:
/* Line 1787 of yacc.c  */
#line 681 "internparser.y"
    {
			    (yyval.tree) = makePrintHexa((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 63:
/* Line 1787 of yacc.c  */
#line 685 "internparser.y"
    {
			    (yyval.tree) = makePrintFloat((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 64:
/* Line 1787 of yacc.c  */
#line 689 "internparser.y"
    {
			    (yyval.tree) = makePrintBinary((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 65:
/* Line 1787 of yacc.c  */
#line 693 "internparser.y"
    {
			    (yyval.tree) = makePrintExpansion((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 66:
/* Line 1787 of yacc.c  */
#line 697 "internparser.y"
    {
			    (yyval.tree) = makeBashExecute((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 67:
/* Line 1787 of yacc.c  */
#line 701 "internparser.y"
    {
			    (yyval.tree) = makeExternalPlot(addElement(addElement(addElement(addElement((yyvsp[(11) - (12)].list),(yyvsp[(9) - (12)].tree)),(yyvsp[(7) - (12)].tree)),(yyvsp[(5) - (12)].tree)),(yyvsp[(3) - (12)].tree)));
			  }
    break;

  case 68:
/* Line 1787 of yacc.c  */
#line 705 "internparser.y"
    {
			    (yyval.tree) = makeWrite((yyvsp[(3) - (4)].list));
			  }
    break;

  case 69:
/* Line 1787 of yacc.c  */
#line 709 "internparser.y"
    {
			    (yyval.tree) = makeNewFileWrite((yyvsp[(6) - (6)].tree), (yyvsp[(3) - (6)].list));
			  }
    break;

  case 70:
/* Line 1787 of yacc.c  */
#line 713 "internparser.y"
    {
			    (yyval.tree) = makeAppendFileWrite((yyvsp[(7) - (7)].tree), (yyvsp[(3) - (7)].list));
			  }
    break;

  case 71:
/* Line 1787 of yacc.c  */
#line 717 "internparser.y"
    {
			    (yyval.tree) = makeAsciiPlot((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 72:
/* Line 1787 of yacc.c  */
#line 721 "internparser.y"
    {
			    (yyval.tree) = makePrintXml((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 73:
/* Line 1787 of yacc.c  */
#line 725 "internparser.y"
    {
			    (yyval.tree) = makeExecute((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 74:
/* Line 1787 of yacc.c  */
#line 729 "internparser.y"
    {
			    (yyval.tree) = makePrintXmlNewFile((yyvsp[(3) - (6)].tree),(yyvsp[(6) - (6)].tree));
			  }
    break;

  case 75:
/* Line 1787 of yacc.c  */
#line 733 "internparser.y"
    {
			    (yyval.tree) = makePrintXmlAppendFile((yyvsp[(3) - (7)].tree),(yyvsp[(7) - (7)].tree));
			  }
    break;

  case 76:
/* Line 1787 of yacc.c  */
#line 737 "internparser.y"
    {
			    (yyval.tree) = makeWorstCase(addElement(addElement(addElement(addElement((yyvsp[(11) - (12)].list), (yyvsp[(9) - (12)].tree)), (yyvsp[(7) - (12)].tree)), (yyvsp[(5) - (12)].tree)), (yyvsp[(3) - (12)].tree)));
			  }
    break;

  case 77:
/* Line 1787 of yacc.c  */
#line 741 "internparser.y"
    {
			    (yyval.tree) = makeRename((yyvsp[(3) - (6)].value), (yyvsp[(5) - (6)].value));
			    free((yyvsp[(3) - (6)].value));
			    free((yyvsp[(5) - (6)].value));
			  }
    break;

  case 78:
/* Line 1787 of yacc.c  */
#line 747 "internparser.y"
    {
			    (yyval.tree) = makeExternalProc((yyvsp[(3) - (11)].value), (yyvsp[(5) - (11)].tree), addElement((yyvsp[(7) - (11)].list), (yyvsp[(10) - (11)].integerval)));
			    free((yyvsp[(3) - (11)].value));
			  }
    break;

  case 79:
/* Line 1787 of yacc.c  */
#line 752 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 80:
/* Line 1787 of yacc.c  */
#line 756 "internparser.y"
    {
			    (yyval.tree) = makeAutoprint((yyvsp[(1) - (1)].list));
			  }
    break;

  case 81:
/* Line 1787 of yacc.c  */
#line 760 "internparser.y"
    {
			    (yyval.tree) = makeAssignment((yyvsp[(2) - (3)].value), (yyvsp[(3) - (3)].tree));
			    free((yyvsp[(2) - (3)].value));
			  }
    break;

  case 82:
/* Line 1787 of yacc.c  */
#line 767 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 83:
/* Line 1787 of yacc.c  */
#line 771 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (2)].tree);
			  }
    break;

  case 84:
/* Line 1787 of yacc.c  */
#line 775 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 85:
/* Line 1787 of yacc.c  */
#line 779 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (2)].tree);
			  }
    break;

  case 86:
/* Line 1787 of yacc.c  */
#line 785 "internparser.y"
    {
			    (yyval.tree) = makeAssignment((yyvsp[(1) - (3)].value), (yyvsp[(3) - (3)].tree));
			    free((yyvsp[(1) - (3)].value));
			  }
    break;

  case 87:
/* Line 1787 of yacc.c  */
#line 790 "internparser.y"
    {
			    (yyval.tree) = makeFloatAssignment((yyvsp[(1) - (3)].value), (yyvsp[(3) - (3)].tree));
			    free((yyvsp[(1) - (3)].value));
			  }
    break;

  case 88:
/* Line 1787 of yacc.c  */
#line 795 "internparser.y"
    {
			    (yyval.tree) = makeLibraryBinding((yyvsp[(1) - (6)].value), (yyvsp[(5) - (6)].tree));
			    free((yyvsp[(1) - (6)].value));
			  }
    break;

  case 89:
/* Line 1787 of yacc.c  */
#line 800 "internparser.y"
    {
			    (yyval.tree) = makeLibraryConstantBinding((yyvsp[(1) - (6)].value), (yyvsp[(5) - (6)].tree));
			    free((yyvsp[(1) - (6)].value));
			  }
    break;

  case 90:
/* Line 1787 of yacc.c  */
#line 805 "internparser.y"
    {
			    (yyval.tree) = makeAssignmentInIndexing((yyvsp[(1) - (3)].dblnode)->a,(yyvsp[(1) - (3)].dblnode)->b,(yyvsp[(3) - (3)].tree));
			    free((yyvsp[(1) - (3)].dblnode));
			  }
    break;

  case 91:
/* Line 1787 of yacc.c  */
#line 810 "internparser.y"
    {
			    (yyval.tree) = makeFloatAssignmentInIndexing((yyvsp[(1) - (3)].dblnode)->a,(yyvsp[(1) - (3)].dblnode)->b,(yyvsp[(3) - (3)].tree));
			    free((yyvsp[(1) - (3)].dblnode));
			  }
    break;

  case 92:
/* Line 1787 of yacc.c  */
#line 815 "internparser.y"
    {
			    (yyval.tree) = makeProtoAssignmentInStructure((yyvsp[(1) - (3)].tree),(yyvsp[(3) - (3)].tree));
			  }
    break;

  case 93:
/* Line 1787 of yacc.c  */
#line 819 "internparser.y"
    {
			    (yyval.tree) = makeProtoFloatAssignmentInStructure((yyvsp[(1) - (3)].tree),(yyvsp[(3) - (3)].tree));
			  }
    break;

  case 94:
/* Line 1787 of yacc.c  */
#line 825 "internparser.y"
    {
			    (yyval.tree) = makeStructAccess((yyvsp[(1) - (3)].tree),(yyvsp[(3) - (3)].value));
			    free((yyvsp[(3) - (3)].value));
			  }
    break;

  case 95:
/* Line 1787 of yacc.c  */
#line 832 "internparser.y"
    {
			    (yyval.tree) = makePrecAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 96:
/* Line 1787 of yacc.c  */
#line 836 "internparser.y"
    {
			    (yyval.tree) = makePointsAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 97:
/* Line 1787 of yacc.c  */
#line 840 "internparser.y"
    {
			    (yyval.tree) = makeDiamAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 98:
/* Line 1787 of yacc.c  */
#line 844 "internparser.y"
    {
			    (yyval.tree) = makeDisplayAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 99:
/* Line 1787 of yacc.c  */
#line 848 "internparser.y"
    {
			    (yyval.tree) = makeVerbosityAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 100:
/* Line 1787 of yacc.c  */
#line 852 "internparser.y"
    {
			    (yyval.tree) = makeCanonicalAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 101:
/* Line 1787 of yacc.c  */
#line 856 "internparser.y"
    {
			    (yyval.tree) = makeAutoSimplifyAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 102:
/* Line 1787 of yacc.c  */
#line 860 "internparser.y"
    {
			    (yyval.tree) = makeTaylorRecursAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 103:
/* Line 1787 of yacc.c  */
#line 864 "internparser.y"
    {
			    (yyval.tree) = makeTimingAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 104:
/* Line 1787 of yacc.c  */
#line 868 "internparser.y"
    {
			    (yyval.tree) = makeFullParenAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 105:
/* Line 1787 of yacc.c  */
#line 872 "internparser.y"
    {
			    (yyval.tree) = makeMidpointAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 106:
/* Line 1787 of yacc.c  */
#line 876 "internparser.y"
    {
			    (yyval.tree) = makeDieOnErrorAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 107:
/* Line 1787 of yacc.c  */
#line 880 "internparser.y"
    {
			    (yyval.tree) = makeRationalModeAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 108:
/* Line 1787 of yacc.c  */
#line 884 "internparser.y"
    {
			    (yyval.tree) = makeSuppressWarningsAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 109:
/* Line 1787 of yacc.c  */
#line 888 "internparser.y"
    {
			    (yyval.tree) = makeHopitalRecursAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 110:
/* Line 1787 of yacc.c  */
#line 894 "internparser.y"
    {
			    (yyval.tree) = makePrecStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 111:
/* Line 1787 of yacc.c  */
#line 898 "internparser.y"
    {
			    (yyval.tree) = makePointsStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 112:
/* Line 1787 of yacc.c  */
#line 902 "internparser.y"
    {
			    (yyval.tree) = makeDiamStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 113:
/* Line 1787 of yacc.c  */
#line 906 "internparser.y"
    {
			    (yyval.tree) = makeDisplayStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 114:
/* Line 1787 of yacc.c  */
#line 910 "internparser.y"
    {
			    (yyval.tree) = makeVerbosityStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 115:
/* Line 1787 of yacc.c  */
#line 914 "internparser.y"
    {
			    (yyval.tree) = makeCanonicalStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 116:
/* Line 1787 of yacc.c  */
#line 918 "internparser.y"
    {
			    (yyval.tree) = makeAutoSimplifyStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 117:
/* Line 1787 of yacc.c  */
#line 922 "internparser.y"
    {
			    (yyval.tree) = makeTaylorRecursStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 118:
/* Line 1787 of yacc.c  */
#line 926 "internparser.y"
    {
			    (yyval.tree) = makeTimingStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 119:
/* Line 1787 of yacc.c  */
#line 930 "internparser.y"
    {
			    (yyval.tree) = makeFullParenStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 120:
/* Line 1787 of yacc.c  */
#line 934 "internparser.y"
    {
			    (yyval.tree) = makeMidpointStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 121:
/* Line 1787 of yacc.c  */
#line 938 "internparser.y"
    {
			    (yyval.tree) = makeDieOnErrorStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 122:
/* Line 1787 of yacc.c  */
#line 942 "internparser.y"
    {
			    (yyval.tree) = makeRationalModeStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 123:
/* Line 1787 of yacc.c  */
#line 946 "internparser.y"
    {
			    (yyval.tree) = makeSuppressWarningsStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 124:
/* Line 1787 of yacc.c  */
#line 950 "internparser.y"
    {
			    (yyval.tree) = makeHopitalRecursStillAssign((yyvsp[(3) - (3)].tree));
			  }
    break;

  case 125:
/* Line 1787 of yacc.c  */
#line 956 "internparser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].tree));
			  }
    break;

  case 126:
/* Line 1787 of yacc.c  */
#line 960 "internparser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].tree));
			  }
    break;

  case 127:
/* Line 1787 of yacc.c  */
#line 966 "internparser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].association));
			  }
    break;

  case 128:
/* Line 1787 of yacc.c  */
#line 970 "internparser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].association));
			  }
    break;

  case 129:
/* Line 1787 of yacc.c  */
#line 976 "internparser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 130:
/* Line 1787 of yacc.c  */
#line 980 "internparser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 131:
/* Line 1787 of yacc.c  */
#line 986 "internparser.y"
    {
			    (yyval.association) = (entry *) safeMalloc(sizeof(entry));
			    (yyval.association)->name = (char *) safeCalloc(strlen((yyvsp[(2) - (4)].value)) + 1, sizeof(char));
			    strcpy((yyval.association)->name,(yyvsp[(2) - (4)].value));
			    free((yyvsp[(2) - (4)].value));
			    (yyval.association)->value = (void *) ((yyvsp[(4) - (4)].tree));
			  }
    break;

  case 132:
/* Line 1787 of yacc.c  */
#line 996 "internparser.y"
    {
			   (yyval.tree) = (yyvsp[(1) - (1)].tree);
			 }
    break;

  case 133:
/* Line 1787 of yacc.c  */
#line 1000 "internparser.y"
    {
			    (yyval.tree) = makeMatch((yyvsp[(2) - (4)].tree),(yyvsp[(4) - (4)].list));
			  }
    break;

  case 134:
/* Line 1787 of yacc.c  */
#line 1006 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 135:
/* Line 1787 of yacc.c  */
#line 1010 "internparser.y"
    {
			    (yyval.tree) = makeAnd((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 136:
/* Line 1787 of yacc.c  */
#line 1014 "internparser.y"
    {
			    (yyval.tree) = makeOr((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 137:
/* Line 1787 of yacc.c  */
#line 1018 "internparser.y"
    {
			    (yyval.tree) = makeNegation((yyvsp[(2) - (2)].tree));
			  }
    break;

  case 138:
/* Line 1787 of yacc.c  */
#line 1024 "internparser.y"
    {
			    (yyval.dblnode) = (doubleNode *) safeMalloc(sizeof(doubleNode));
			    (yyval.dblnode)->a = (yyvsp[(1) - (4)].tree);
			    (yyval.dblnode)->b = (yyvsp[(3) - (4)].tree);
			  }
    break;

  case 139:
/* Line 1787 of yacc.c  */
#line 1033 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 140:
/* Line 1787 of yacc.c  */
#line 1037 "internparser.y"
    {
			    (yyval.tree) = makeCompareEqual((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 141:
/* Line 1787 of yacc.c  */
#line 1041 "internparser.y"
    {
			    (yyval.tree) = makeCompareIn((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 142:
/* Line 1787 of yacc.c  */
#line 1045 "internparser.y"
    {
			    (yyval.tree) = makeCompareLess((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 143:
/* Line 1787 of yacc.c  */
#line 1049 "internparser.y"
    {
			    (yyval.tree) = makeCompareGreater((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 144:
/* Line 1787 of yacc.c  */
#line 1053 "internparser.y"
    {
			    (yyval.tree) = makeCompareLessEqual((yyvsp[(1) - (4)].tree), (yyvsp[(4) - (4)].tree));
			  }
    break;

  case 145:
/* Line 1787 of yacc.c  */
#line 1057 "internparser.y"
    {
			    (yyval.tree) = makeCompareGreaterEqual((yyvsp[(1) - (4)].tree), (yyvsp[(4) - (4)].tree));
			  }
    break;

  case 146:
/* Line 1787 of yacc.c  */
#line 1061 "internparser.y"
    {
			    (yyval.tree) = makeCompareNotEqual((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 147:
/* Line 1787 of yacc.c  */
#line 1067 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 148:
/* Line 1787 of yacc.c  */
#line 1071 "internparser.y"
    {
			    (yyval.tree) = makeAdd((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 149:
/* Line 1787 of yacc.c  */
#line 1075 "internparser.y"
    {
			    (yyval.tree) = makeSub((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 150:
/* Line 1787 of yacc.c  */
#line 1079 "internparser.y"
    {
			    (yyval.tree) = makeConcat((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 151:
/* Line 1787 of yacc.c  */
#line 1083 "internparser.y"
    {
			    (yyval.tree) = makeAddToList((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 152:
/* Line 1787 of yacc.c  */
#line 1087 "internparser.y"
    {
			    (yyval.tree) = makePrepend((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 153:
/* Line 1787 of yacc.c  */
#line 1091 "internparser.y"
    {
			    (yyval.tree) = makeAppend((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 154:
/* Line 1787 of yacc.c  */
#line 1097 "internparser.y"
    {
                            (yyval.count) = 0;
                          }
    break;

  case 155:
/* Line 1787 of yacc.c  */
#line 1101 "internparser.y"
    {
                            (yyval.count) = 1;
                          }
    break;

  case 156:
/* Line 1787 of yacc.c  */
#line 1105 "internparser.y"
    {
  	                    (yyval.count) = (yyvsp[(2) - (2)].count);
  	                  }
    break;

  case 157:
/* Line 1787 of yacc.c  */
#line 1109 "internparser.y"
    {
  	                    (yyval.count) = (yyvsp[(2) - (2)].count)+1;
                          }
    break;

  case 158:
/* Line 1787 of yacc.c  */
#line 1116 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
                          }
    break;

  case 159:
/* Line 1787 of yacc.c  */
#line 1120 "internparser.y"
    {
			    tempNode = (yyvsp[(2) - (2)].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[(1) - (2)].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = tempNode;
			  }
    break;

  case 160:
/* Line 1787 of yacc.c  */
#line 1127 "internparser.y"
    {
			    (yyval.tree) = makeEvalConst((yyvsp[(2) - (2)].tree));
                          }
    break;

  case 161:
/* Line 1787 of yacc.c  */
#line 1131 "internparser.y"
    {
			    (yyval.tree) = makeMul((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
                          }
    break;

  case 162:
/* Line 1787 of yacc.c  */
#line 1135 "internparser.y"
    {
			    (yyval.tree) = makeDiv((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
                          }
    break;

  case 163:
/* Line 1787 of yacc.c  */
#line 1139 "internparser.y"
    {
			    tempNode = (yyvsp[(4) - (4)].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[(3) - (4)].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makeMul((yyvsp[(1) - (4)].tree), tempNode);
			  }
    break;

  case 164:
/* Line 1787 of yacc.c  */
#line 1146 "internparser.y"
    {
			    tempNode = (yyvsp[(4) - (4)].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[(3) - (4)].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makeDiv((yyvsp[(1) - (4)].tree), tempNode);
			  }
    break;

  case 165:
/* Line 1787 of yacc.c  */
#line 1153 "internparser.y"
    {
			    (yyval.tree) = makeMul((yyvsp[(1) - (4)].tree), makeEvalConst((yyvsp[(4) - (4)].tree)));
			  }
    break;

  case 166:
/* Line 1787 of yacc.c  */
#line 1157 "internparser.y"
    {
			    (yyval.tree) = makeDiv((yyvsp[(1) - (4)].tree), makeEvalConst((yyvsp[(4) - (4)].tree)));
			  }
    break;

  case 167:
/* Line 1787 of yacc.c  */
#line 1163 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
                          }
    break;

  case 168:
/* Line 1787 of yacc.c  */
#line 1167 "internparser.y"
    {
			    (yyval.tree) = makePow((yyvsp[(1) - (3)].tree), (yyvsp[(3) - (3)].tree));
                          }
    break;

  case 169:
/* Line 1787 of yacc.c  */
#line 1171 "internparser.y"
    {
			    tempNode = (yyvsp[(4) - (4)].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[(3) - (4)].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makePow((yyvsp[(1) - (4)].tree), tempNode);
			  }
    break;

  case 170:
/* Line 1787 of yacc.c  */
#line 1178 "internparser.y"
    {
			    (yyval.tree) = makePow((yyvsp[(1) - (4)].tree), makeEvalConst((yyvsp[(4) - (4)].tree)));
			  }
    break;

  case 171:
/* Line 1787 of yacc.c  */
#line 1185 "internparser.y"
    {
			    (yyval.tree) = makeOn();
			  }
    break;

  case 172:
/* Line 1787 of yacc.c  */
#line 1189 "internparser.y"
    {
			    (yyval.tree) = makeOff();
			  }
    break;

  case 173:
/* Line 1787 of yacc.c  */
#line 1193 "internparser.y"
    {
			    (yyval.tree) = makeDyadic();
			  }
    break;

  case 174:
/* Line 1787 of yacc.c  */
#line 1197 "internparser.y"
    {
			    (yyval.tree) = makePowers();
			  }
    break;

  case 175:
/* Line 1787 of yacc.c  */
#line 1201 "internparser.y"
    {
			    (yyval.tree) = makeBinaryThing();
			  }
    break;

  case 176:
/* Line 1787 of yacc.c  */
#line 1205 "internparser.y"
    {
			    (yyval.tree) = makeHexadecimalThing();
			  }
    break;

  case 177:
/* Line 1787 of yacc.c  */
#line 1209 "internparser.y"
    {
			    (yyval.tree) = makeFile();
			  }
    break;

  case 178:
/* Line 1787 of yacc.c  */
#line 1213 "internparser.y"
    {
			    (yyval.tree) = makePostscript();
			  }
    break;

  case 179:
/* Line 1787 of yacc.c  */
#line 1217 "internparser.y"
    {
			    (yyval.tree) = makePostscriptFile();
			  }
    break;

  case 180:
/* Line 1787 of yacc.c  */
#line 1221 "internparser.y"
    {
			    (yyval.tree) = makePerturb();
			  }
    break;

  case 181:
/* Line 1787 of yacc.c  */
#line 1225 "internparser.y"
    {
			    (yyval.tree) = makeRoundDown();
			  }
    break;

  case 182:
/* Line 1787 of yacc.c  */
#line 1229 "internparser.y"
    {
			    (yyval.tree) = makeRoundUp();
			  }
    break;

  case 183:
/* Line 1787 of yacc.c  */
#line 1233 "internparser.y"
    {
			    (yyval.tree) = makeRoundToZero();
			  }
    break;

  case 184:
/* Line 1787 of yacc.c  */
#line 1237 "internparser.y"
    {
			    (yyval.tree) = makeRoundToNearest();
			  }
    break;

  case 185:
/* Line 1787 of yacc.c  */
#line 1241 "internparser.y"
    {
			    (yyval.tree) = makeHonorCoeff();
			  }
    break;

  case 186:
/* Line 1787 of yacc.c  */
#line 1245 "internparser.y"
    {
			    (yyval.tree) = makeTrue();
			  }
    break;

  case 187:
/* Line 1787 of yacc.c  */
#line 1249 "internparser.y"
    {
			    (yyval.tree) = makeUnit();
			  }
    break;

  case 188:
/* Line 1787 of yacc.c  */
#line 1253 "internparser.y"
    {
			    (yyval.tree) = makeFalse();
			  }
    break;

  case 189:
/* Line 1787 of yacc.c  */
#line 1257 "internparser.y"
    {
			    (yyval.tree) = makeDefault();
			  }
    break;

  case 190:
/* Line 1787 of yacc.c  */
#line 1261 "internparser.y"
    {
			    (yyval.tree) = makeDecimal();
			  }
    break;

  case 191:
/* Line 1787 of yacc.c  */
#line 1265 "internparser.y"
    {
			    (yyval.tree) = makeAbsolute();
			  }
    break;

  case 192:
/* Line 1787 of yacc.c  */
#line 1269 "internparser.y"
    {
			    (yyval.tree) = makeRelative();
			  }
    break;

  case 193:
/* Line 1787 of yacc.c  */
#line 1273 "internparser.y"
    {
			    (yyval.tree) = makeFixed();
			  }
    break;

  case 194:
/* Line 1787 of yacc.c  */
#line 1277 "internparser.y"
    {
			    (yyval.tree) = makeFloating();
			  }
    break;

  case 195:
/* Line 1787 of yacc.c  */
#line 1281 "internparser.y"
    {
			    (yyval.tree) = makeError();
			  }
    break;

  case 196:
/* Line 1787 of yacc.c  */
#line 1285 "internparser.y"
    {
			    (yyval.tree) = makeDoubleSymbol();
			  }
    break;

  case 197:
/* Line 1787 of yacc.c  */
#line 1289 "internparser.y"
    {
			    (yyval.tree) = makeSingleSymbol();
			  }
    break;

  case 198:
/* Line 1787 of yacc.c  */
#line 1293 "internparser.y"
    {
			    (yyval.tree) = makeQuadSymbol();
			  }
    break;

  case 199:
/* Line 1787 of yacc.c  */
#line 1297 "internparser.y"
    {
			    (yyval.tree) = makeHalfPrecisionSymbol();
			  }
    break;

  case 200:
/* Line 1787 of yacc.c  */
#line 1301 "internparser.y"
    {
			    (yyval.tree) = makeDoubleextendedSymbol();
			  }
    break;

  case 201:
/* Line 1787 of yacc.c  */
#line 1305 "internparser.y"
    {
			    (yyval.tree) = makeDoubleDoubleSymbol();
			  }
    break;

  case 202:
/* Line 1787 of yacc.c  */
#line 1309 "internparser.y"
    {
			    (yyval.tree) = makeTripleDoubleSymbol();
			  }
    break;

  case 203:
/* Line 1787 of yacc.c  */
#line 1313 "internparser.y"
    {
			    tempString = safeCalloc(strlen((yyvsp[(1) - (1)].value)) + 1, sizeof(char));
			    strcpy(tempString, (yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			    tempString2 = safeCalloc(strlen(tempString) + 1, sizeof(char));
			    strcpy(tempString2, tempString);
			    free(tempString);
			    (yyval.tree) = makeString(tempString2);
			    free(tempString2);
			  }
    break;

  case 204:
/* Line 1787 of yacc.c  */
#line 1324 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 205:
/* Line 1787 of yacc.c  */
#line 1328 "internparser.y"
    {
			    (yyval.tree) = makeTableAccess((yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 206:
/* Line 1787 of yacc.c  */
#line 1333 "internparser.y"
    {
			    (yyval.tree) = makeIsBound((yyvsp[(3) - (4)].value));
			    free((yyvsp[(3) - (4)].value));
			  }
    break;

  case 207:
/* Line 1787 of yacc.c  */
#line 1338 "internparser.y"
    {
			    (yyval.tree) = makeTableAccessWithSubstitute((yyvsp[(1) - (4)].value), (yyvsp[(3) - (4)].list));
			    free((yyvsp[(1) - (4)].value));
			  }
    break;

  case 208:
/* Line 1787 of yacc.c  */
#line 1343 "internparser.y"
    {
			    (yyval.tree) = makeTableAccessWithSubstitute((yyvsp[(1) - (3)].value), NULL);
			    free((yyvsp[(1) - (3)].value));
			  }
    break;

  case 209:
/* Line 1787 of yacc.c  */
#line 1348 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 210:
/* Line 1787 of yacc.c  */
#line 1352 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 211:
/* Line 1787 of yacc.c  */
#line 1356 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 212:
/* Line 1787 of yacc.c  */
#line 1360 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 213:
/* Line 1787 of yacc.c  */
#line 1364 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(2) - (3)].tree);
			  }
    break;

  case 214:
/* Line 1787 of yacc.c  */
#line 1368 "internparser.y"
    {
			    (yyval.tree) = makeStructure((yyvsp[(2) - (3)].list));
			  }
    break;

  case 215:
/* Line 1787 of yacc.c  */
#line 1372 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(1) - (1)].tree);
			  }
    break;

  case 216:
/* Line 1787 of yacc.c  */
#line 1376 "internparser.y"
    {
			    (yyval.tree) = makeIndex((yyvsp[(1) - (1)].dblnode)->a, (yyvsp[(1) - (1)].dblnode)->b);
			    free((yyvsp[(1) - (1)].dblnode));
			  }
    break;

  case 217:
/* Line 1787 of yacc.c  */
#line 1381 "internparser.y"
    {
			    (yyval.tree) = makeStructAccess((yyvsp[(1) - (3)].tree),(yyvsp[(3) - (3)].value));
			    free((yyvsp[(3) - (3)].value));
			  }
    break;

  case 218:
/* Line 1787 of yacc.c  */
#line 1386 "internparser.y"
    {
			    (yyval.tree) = makeApply(makeStructAccess((yyvsp[(1) - (6)].tree),(yyvsp[(3) - (6)].value)),(yyvsp[(5) - (6)].list));
			    free((yyvsp[(3) - (6)].value));
			  }
    break;

  case 219:
/* Line 1787 of yacc.c  */
#line 1391 "internparser.y"
    {
			    (yyval.tree) = makeApply((yyvsp[(2) - (6)].tree),(yyvsp[(5) - (6)].list));
			  }
    break;

  case 220:
/* Line 1787 of yacc.c  */
#line 1395 "internparser.y"
    {
			    (yyval.tree) = (yyvsp[(2) - (2)].tree);
			  }
    break;

  case 221:
/* Line 1787 of yacc.c  */
#line 1399 "internparser.y"
    {
			    (yyval.tree) = makeTime((yyvsp[(3) - (4)].tree));
                          }
    break;

  case 222:
/* Line 1787 of yacc.c  */
#line 1405 "internparser.y"
    {
			    (yyval.list) = addElement(NULL,(yyvsp[(1) - (1)].tree));
			  }
    break;

  case 223:
/* Line 1787 of yacc.c  */
#line 1409 "internparser.y"
    {
			    (yyval.list) = addElement((yyvsp[(2) - (2)].list),(yyvsp[(1) - (2)].tree));
			  }
    break;

  case 224:
/* Line 1787 of yacc.c  */
#line 1415 "internparser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (9)].tree),makeCommandList(concatChains((yyvsp[(4) - (9)].list), (yyvsp[(5) - (9)].list))),(yyvsp[(7) - (9)].tree));
			  }
    break;

  case 225:
/* Line 1787 of yacc.c  */
#line 1419 "internparser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (6)].tree),makeCommandList(concatChains((yyvsp[(4) - (6)].list), (yyvsp[(5) - (6)].list))),makeUnit());
			  }
    break;

  case 226:
/* Line 1787 of yacc.c  */
#line 1423 "internparser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (8)].tree),makeCommandList((yyvsp[(4) - (8)].list)),(yyvsp[(6) - (8)].tree));
			  }
    break;

  case 227:
/* Line 1787 of yacc.c  */
#line 1427 "internparser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (5)].tree),makeCommandList((yyvsp[(4) - (5)].list)),makeUnit());
			  }
    break;

  case 228:
/* Line 1787 of yacc.c  */
#line 1431 "internparser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (8)].tree),makeCommandList((yyvsp[(4) - (8)].list)),(yyvsp[(6) - (8)].tree));
			  }
    break;

  case 229:
/* Line 1787 of yacc.c  */
#line 1435 "internparser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (5)].tree),makeCommandList((yyvsp[(4) - (5)].list)),makeUnit());
			  }
    break;

  case 230:
/* Line 1787 of yacc.c  */
#line 1439 "internparser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (7)].tree), makeCommandList(addElement(NULL,makeNop())), (yyvsp[(5) - (7)].tree));
			  }
    break;

  case 231:
/* Line 1787 of yacc.c  */
#line 1443 "internparser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (4)].tree), makeCommandList(addElement(NULL,makeNop())), makeUnit());
			  }
    break;

  case 232:
/* Line 1787 of yacc.c  */
#line 1447 "internparser.y"
    {
			    (yyval.tree) = makeMatchElement((yyvsp[(1) - (5)].tree), makeCommandList(addElement(NULL,makeNop())), (yyvsp[(4) - (5)].tree));
			  }
    break;

  case 233:
/* Line 1787 of yacc.c  */
#line 1453 "internparser.y"
    {
			    (yyval.tree) = makeDecimalConstant((yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 234:
/* Line 1787 of yacc.c  */
#line 1458 "internparser.y"
    {
			    (yyval.tree) = makeMidpointConstant((yyvsp[(1) - (1)].value));
			    free((yyvsp[(1) - (1)].value));
			  }
    break;

  case 235:
/* Line 1787 of yacc.c  */
#line 1463 "internparser.y"
    {
			    (yyval.tree) = makeDyadicConstant((yyvsp[(1) - (1)].value));
			  }
    break;

  case 236:
/* Line 1787 of yacc.c  */
#line 1467 "internparser.y"
    {
			    (yyval.tree) = makeHexConstant((yyvsp[(1) - (1)].value));
			  }
    break;

  case 237:
/* Line 1787 of yacc.c  */
#line 1471 "internparser.y"
    {
			    (yyval.tree) = makeHexadecimalConstant((yyvsp[(1) - (1)].value));
			  }
    break;

  case 238:
/* Line 1787 of yacc.c  */
#line 1475 "internparser.y"
    {
			    (yyval.tree) = makeBinaryConstant((yyvsp[(1) - (1)].value));
			  }
    break;

  case 239:
/* Line 1787 of yacc.c  */
#line 1479 "internparser.y"
    {
			    (yyval.tree) = makePi();
			  }
    break;

  case 240:
/* Line 1787 of yacc.c  */
#line 1487 "internparser.y"
    {
			    (yyval.tree) = makeEmptyList();
			  }
    break;

  case 241:
/* Line 1787 of yacc.c  */
#line 1491 "internparser.y"
    {
			    (yyval.tree) = makeEmptyList();
			  }
    break;

  case 242:
/* Line 1787 of yacc.c  */
#line 1495 "internparser.y"
    {
			    (yyval.tree) = makeRevertedList((yyvsp[(3) - (5)].list));
			  }
    break;

  case 243:
/* Line 1787 of yacc.c  */
#line 1499 "internparser.y"
    {
			    (yyval.tree) = makeRevertedFinalEllipticList((yyvsp[(3) - (6)].list));
			  }
    break;

  case 244:
/* Line 1787 of yacc.c  */
#line 1505 "internparser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].tree));
			  }
    break;

  case 245:
/* Line 1787 of yacc.c  */
#line 1509 "internparser.y"
    {
			    (yyval.list) = addElement((yyvsp[(1) - (3)].list), (yyvsp[(3) - (3)].tree));
			  }
    break;

  case 246:
/* Line 1787 of yacc.c  */
#line 1513 "internparser.y"
    {
			    (yyval.list) = addElement(addElement((yyvsp[(1) - (5)].list), makeElliptic()), (yyvsp[(5) - (5)].tree));
			  }
    break;

  case 247:
/* Line 1787 of yacc.c  */
#line 1519 "internparser.y"
    {
			    (yyval.tree) = makeRange((yyvsp[(2) - (5)].tree), (yyvsp[(4) - (5)].tree));
			  }
    break;

  case 248:
/* Line 1787 of yacc.c  */
#line 1523 "internparser.y"
    {
			    (yyval.tree) = makeRange((yyvsp[(2) - (5)].tree), (yyvsp[(4) - (5)].tree));
			  }
    break;

  case 249:
/* Line 1787 of yacc.c  */
#line 1527 "internparser.y"
    {
			    (yyval.tree) = makeRange((yyvsp[(2) - (3)].tree), copyThing((yyvsp[(2) - (3)].tree)));
			  }
    break;

  case 250:
/* Line 1787 of yacc.c  */
#line 1533 "internparser.y"
    {
			    (yyval.tree) = makeDeboundMax((yyvsp[(2) - (3)].tree));
			  }
    break;

  case 251:
/* Line 1787 of yacc.c  */
#line 1537 "internparser.y"
    {
			    (yyval.tree) = makeDeboundMid((yyvsp[(2) - (3)].tree));
			  }
    break;

  case 252:
/* Line 1787 of yacc.c  */
#line 1541 "internparser.y"
    {
			    (yyval.tree) = makeDeboundMin((yyvsp[(2) - (3)].tree));
			  }
    break;

  case 253:
/* Line 1787 of yacc.c  */
#line 1545 "internparser.y"
    {
			    (yyval.tree) = makeDeboundMax((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 254:
/* Line 1787 of yacc.c  */
#line 1549 "internparser.y"
    {
			    (yyval.tree) = makeDeboundMid((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 255:
/* Line 1787 of yacc.c  */
#line 1553 "internparser.y"
    {
			    (yyval.tree) = makeDeboundMin((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 256:
/* Line 1787 of yacc.c  */
#line 1559 "internparser.y"
    {
			    (yyval.tree) = makeDiff((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 257:
/* Line 1787 of yacc.c  */
#line 1563 "internparser.y"
    {
			    (yyval.tree) = makeBashevaluate(addElement(NULL,(yyvsp[(3) - (4)].tree)));
			  }
    break;

  case 258:
/* Line 1787 of yacc.c  */
#line 1567 "internparser.y"
    {
			    (yyval.tree) = makeBashevaluate(addElement(addElement(NULL,(yyvsp[(5) - (6)].tree)),(yyvsp[(3) - (6)].tree)));
			  }
    break;

  case 259:
/* Line 1787 of yacc.c  */
#line 1571 "internparser.y"
    {
			    (yyval.tree) = makeSimplify((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 260:
/* Line 1787 of yacc.c  */
#line 1575 "internparser.y"
    {
			    (yyval.tree) = makeRemez(addElement(addElement((yyvsp[(7) - (8)].list), (yyvsp[(5) - (8)].tree)), (yyvsp[(3) - (8)].tree)));
			  }
    break;

  case 261:
/* Line 1787 of yacc.c  */
#line 1579 "internparser.y"
    {
			    (yyval.tree) = makeMin((yyvsp[(3) - (4)].list));
			  }
    break;

  case 262:
/* Line 1787 of yacc.c  */
#line 1583 "internparser.y"
    {
			    (yyval.tree) = makeMax((yyvsp[(3) - (4)].list));
			  }
    break;

  case 263:
/* Line 1787 of yacc.c  */
#line 1587 "internparser.y"
    {
			    (yyval.tree) = makeFPminimax(addElement(addElement(addElement((yyvsp[(9) - (10)].list), (yyvsp[(7) - (10)].tree)), (yyvsp[(5) - (10)].tree)), (yyvsp[(3) - (10)].tree)));
			  }
    break;

  case 264:
/* Line 1787 of yacc.c  */
#line 1591 "internparser.y"
    {
			    (yyval.tree) = makeHorner((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 265:
/* Line 1787 of yacc.c  */
#line 1595 "internparser.y"
    {
			    (yyval.tree) = makeCanonicalThing((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 266:
/* Line 1787 of yacc.c  */
#line 1599 "internparser.y"
    {
			    (yyval.tree) = makeExpand((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 267:
/* Line 1787 of yacc.c  */
#line 1603 "internparser.y"
    {
			    (yyval.tree) = makeSimplifySafe((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 268:
/* Line 1787 of yacc.c  */
#line 1607 "internparser.y"
    {
			    (yyval.tree) = makeTaylor((yyvsp[(3) - (8)].tree), (yyvsp[(5) - (8)].tree), (yyvsp[(7) - (8)].tree));
			  }
    break;

  case 269:
/* Line 1787 of yacc.c  */
#line 1611 "internparser.y"
    {
                            (yyval.tree) = makeTaylorform(addElement(addElement((yyvsp[(7) - (8)].list), (yyvsp[(5) - (8)].tree)), (yyvsp[(3) - (8)].tree)));
			  }
    break;

  case 270:
/* Line 1787 of yacc.c  */
#line 1615 "internparser.y"
    {
                            (yyval.tree) = makeAutodiff(addElement(addElement(addElement(NULL, (yyvsp[(7) - (8)].tree)), (yyvsp[(5) - (8)].tree)), (yyvsp[(3) - (8)].tree)));
			  }
    break;

  case 271:
/* Line 1787 of yacc.c  */
#line 1619 "internparser.y"
    {
			    (yyval.tree) = makeDegree((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 272:
/* Line 1787 of yacc.c  */
#line 1623 "internparser.y"
    {
			    (yyval.tree) = makeNumerator((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 273:
/* Line 1787 of yacc.c  */
#line 1627 "internparser.y"
    {
			    (yyval.tree) = makeDenominator((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 274:
/* Line 1787 of yacc.c  */
#line 1631 "internparser.y"
    {
			    (yyval.tree) = makeSubstitute((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 275:
/* Line 1787 of yacc.c  */
#line 1635 "internparser.y"
    {
			    (yyval.tree) = makeCoeff((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 276:
/* Line 1787 of yacc.c  */
#line 1639 "internparser.y"
    {
			    (yyval.tree) = makeSubpoly((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 277:
/* Line 1787 of yacc.c  */
#line 1643 "internparser.y"
    {
			    (yyval.tree) = makeRoundcoefficients((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 278:
/* Line 1787 of yacc.c  */
#line 1647 "internparser.y"
    {
			    (yyval.tree) = makeRationalapprox((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 279:
/* Line 1787 of yacc.c  */
#line 1651 "internparser.y"
    {
			    (yyval.tree) = makeAccurateInfnorm(addElement(addElement((yyvsp[(7) - (8)].list), (yyvsp[(5) - (8)].tree)), (yyvsp[(3) - (8)].tree)));
			  }
    break;

  case 280:
/* Line 1787 of yacc.c  */
#line 1655 "internparser.y"
    {
			    (yyval.tree) = makeRoundToFormat((yyvsp[(3) - (8)].tree), (yyvsp[(5) - (8)].tree), (yyvsp[(7) - (8)].tree));
			  }
    break;

  case 281:
/* Line 1787 of yacc.c  */
#line 1659 "internparser.y"
    {
			    (yyval.tree) = makeEvaluate((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 282:
/* Line 1787 of yacc.c  */
#line 1663 "internparser.y"
    {
			    (yyval.tree) = makeParse((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 283:
/* Line 1787 of yacc.c  */
#line 1667 "internparser.y"
    {
			    (yyval.tree) = makeReadXml((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 284:
/* Line 1787 of yacc.c  */
#line 1671 "internparser.y"
    {
			    (yyval.tree) = makeInfnorm(addElement((yyvsp[(5) - (6)].list), (yyvsp[(3) - (6)].tree)));
			  }
    break;

  case 285:
/* Line 1787 of yacc.c  */
#line 1675 "internparser.y"
    {
			    (yyval.tree) = makeSupnorm(addElement(addElement(addElement(addElement(addElement(NULL,(yyvsp[(11) - (12)].tree)),(yyvsp[(9) - (12)].tree)),(yyvsp[(7) - (12)].tree)),(yyvsp[(5) - (12)].tree)),(yyvsp[(3) - (12)].tree)));
			  }
    break;

  case 286:
/* Line 1787 of yacc.c  */
#line 1679 "internparser.y"
    {
			    (yyval.tree) = makeFindZeros((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 287:
/* Line 1787 of yacc.c  */
#line 1683 "internparser.y"
    {
			    (yyval.tree) = makeFPFindZeros((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 288:
/* Line 1787 of yacc.c  */
#line 1687 "internparser.y"
    {
			    (yyval.tree) = makeDirtyInfnorm((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 289:
/* Line 1787 of yacc.c  */
#line 1691 "internparser.y"
    {
			    (yyval.tree) = makeNumberRoots((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 290:
/* Line 1787 of yacc.c  */
#line 1695 "internparser.y"
    {
			    (yyval.tree) = makeIntegral((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 291:
/* Line 1787 of yacc.c  */
#line 1699 "internparser.y"
    {
			    (yyval.tree) = makeDirtyIntegral((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 292:
/* Line 1787 of yacc.c  */
#line 1703 "internparser.y"
    {
			    (yyval.tree) = makeImplementPoly(addElement(addElement(addElement(addElement(addElement((yyvsp[(13) - (14)].list), (yyvsp[(11) - (14)].tree)), (yyvsp[(9) - (14)].tree)), (yyvsp[(7) - (14)].tree)), (yyvsp[(5) - (14)].tree)), (yyvsp[(3) - (14)].tree)));
			  }
    break;

  case 293:
/* Line 1787 of yacc.c  */
#line 1707 "internparser.y"
    {
			    (yyval.tree) = makeImplementConst((yyvsp[(3) - (4)].list));
			  }
    break;

  case 294:
/* Line 1787 of yacc.c  */
#line 1711 "internparser.y"
    {
			    (yyval.tree) = makeCheckInfnorm((yyvsp[(3) - (8)].tree), (yyvsp[(5) - (8)].tree), (yyvsp[(7) - (8)].tree));
			  }
    break;

  case 295:
/* Line 1787 of yacc.c  */
#line 1715 "internparser.y"
    {
			    (yyval.tree) = makeZeroDenominators((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 296:
/* Line 1787 of yacc.c  */
#line 1719 "internparser.y"
    {
			    (yyval.tree) = makeIsEvaluable((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 297:
/* Line 1787 of yacc.c  */
#line 1723 "internparser.y"
    {
			    (yyval.tree) = makeSearchGal((yyvsp[(3) - (4)].list));
			  }
    break;

  case 298:
/* Line 1787 of yacc.c  */
#line 1727 "internparser.y"
    {
			    (yyval.tree) = makeGuessDegree(addElement(addElement((yyvsp[(7) - (8)].list), (yyvsp[(5) - (8)].tree)), (yyvsp[(3) - (8)].tree)));
			  }
    break;

  case 299:
/* Line 1787 of yacc.c  */
#line 1731 "internparser.y"
    {
			    (yyval.tree) = makeDirtyFindZeros((yyvsp[(3) - (6)].tree), (yyvsp[(5) - (6)].tree));
			  }
    break;

  case 300:
/* Line 1787 of yacc.c  */
#line 1735 "internparser.y"
    {
			    (yyval.tree) = makeHead((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 301:
/* Line 1787 of yacc.c  */
#line 1739 "internparser.y"
    {
			    (yyval.tree) = makeRoundCorrectly((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 302:
/* Line 1787 of yacc.c  */
#line 1743 "internparser.y"
    {
			    (yyval.tree) = makeReadFile((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 303:
/* Line 1787 of yacc.c  */
#line 1747 "internparser.y"
    {
			    (yyval.tree) = makeRevert((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 304:
/* Line 1787 of yacc.c  */
#line 1751 "internparser.y"
    {
			    (yyval.tree) = makeSort((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 305:
/* Line 1787 of yacc.c  */
#line 1755 "internparser.y"
    {
			    (yyval.tree) = makeMantissa((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 306:
/* Line 1787 of yacc.c  */
#line 1759 "internparser.y"
    {
			    (yyval.tree) = makeExponent((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 307:
/* Line 1787 of yacc.c  */
#line 1763 "internparser.y"
    {
			    (yyval.tree) = makePrecision((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 308:
/* Line 1787 of yacc.c  */
#line 1767 "internparser.y"
    {
			    (yyval.tree) = makeTail((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 309:
/* Line 1787 of yacc.c  */
#line 1771 "internparser.y"
    {
			    (yyval.tree) = makeSqrt((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 310:
/* Line 1787 of yacc.c  */
#line 1775 "internparser.y"
    {
			    (yyval.tree) = makeExp((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 311:
/* Line 1787 of yacc.c  */
#line 1779 "internparser.y"
    {
			    (yyval.tree) = makeProcedureFunction((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 312:
/* Line 1787 of yacc.c  */
#line 1783 "internparser.y"
    {
			    (yyval.tree) = makeSubstitute(makeProcedureFunction((yyvsp[(3) - (6)].tree)),(yyvsp[(5) - (6)].tree));
			  }
    break;

  case 313:
/* Line 1787 of yacc.c  */
#line 1787 "internparser.y"
    {
			    (yyval.tree) = makeLog((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 314:
/* Line 1787 of yacc.c  */
#line 1791 "internparser.y"
    {
			    (yyval.tree) = makeLog2((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 315:
/* Line 1787 of yacc.c  */
#line 1795 "internparser.y"
    {
			    (yyval.tree) = makeLog10((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 316:
/* Line 1787 of yacc.c  */
#line 1799 "internparser.y"
    {
			    (yyval.tree) = makeSin((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 317:
/* Line 1787 of yacc.c  */
#line 1803 "internparser.y"
    {
			    (yyval.tree) = makeCos((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 318:
/* Line 1787 of yacc.c  */
#line 1807 "internparser.y"
    {
			    (yyval.tree) = makeTan((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 319:
/* Line 1787 of yacc.c  */
#line 1811 "internparser.y"
    {
			    (yyval.tree) = makeAsin((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 320:
/* Line 1787 of yacc.c  */
#line 1815 "internparser.y"
    {
			    (yyval.tree) = makeAcos((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 321:
/* Line 1787 of yacc.c  */
#line 1819 "internparser.y"
    {
			    (yyval.tree) = makeAtan((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 322:
/* Line 1787 of yacc.c  */
#line 1823 "internparser.y"
    {
			    (yyval.tree) = makeSinh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 323:
/* Line 1787 of yacc.c  */
#line 1827 "internparser.y"
    {
			    (yyval.tree) = makeCosh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 324:
/* Line 1787 of yacc.c  */
#line 1831 "internparser.y"
    {
			    (yyval.tree) = makeTanh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 325:
/* Line 1787 of yacc.c  */
#line 1835 "internparser.y"
    {
			    (yyval.tree) = makeAsinh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 326:
/* Line 1787 of yacc.c  */
#line 1839 "internparser.y"
    {
			    (yyval.tree) = makeAcosh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 327:
/* Line 1787 of yacc.c  */
#line 1843 "internparser.y"
    {
			    (yyval.tree) = makeAtanh((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 328:
/* Line 1787 of yacc.c  */
#line 1847 "internparser.y"
    {
			    (yyval.tree) = makeAbs((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 329:
/* Line 1787 of yacc.c  */
#line 1851 "internparser.y"
    {
			    (yyval.tree) = makeErf((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 330:
/* Line 1787 of yacc.c  */
#line 1855 "internparser.y"
    {
			    (yyval.tree) = makeErfc((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 331:
/* Line 1787 of yacc.c  */
#line 1859 "internparser.y"
    {
			    (yyval.tree) = makeLog1p((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 332:
/* Line 1787 of yacc.c  */
#line 1863 "internparser.y"
    {
			    (yyval.tree) = makeExpm1((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 333:
/* Line 1787 of yacc.c  */
#line 1867 "internparser.y"
    {
			    (yyval.tree) = makeDouble((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 334:
/* Line 1787 of yacc.c  */
#line 1871 "internparser.y"
    {
			    (yyval.tree) = makeSingle((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 335:
/* Line 1787 of yacc.c  */
#line 1875 "internparser.y"
    {
			    (yyval.tree) = makeQuad((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 336:
/* Line 1787 of yacc.c  */
#line 1879 "internparser.y"
    {
			    (yyval.tree) = makeHalfPrecision((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 337:
/* Line 1787 of yacc.c  */
#line 1883 "internparser.y"
    {
			    (yyval.tree) = makeDoubledouble((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 338:
/* Line 1787 of yacc.c  */
#line 1887 "internparser.y"
    {
			    (yyval.tree) = makeTripledouble((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 339:
/* Line 1787 of yacc.c  */
#line 1891 "internparser.y"
    {
			    (yyval.tree) = makeDoubleextended((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 340:
/* Line 1787 of yacc.c  */
#line 1895 "internparser.y"
    {
			    (yyval.tree) = makeCeil((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 341:
/* Line 1787 of yacc.c  */
#line 1899 "internparser.y"
    {
			    (yyval.tree) = makeFloor((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 342:
/* Line 1787 of yacc.c  */
#line 1903 "internparser.y"
    {
			    (yyval.tree) = makeNearestInt((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 343:
/* Line 1787 of yacc.c  */
#line 1907 "internparser.y"
    {
			    (yyval.tree) = makeLength((yyvsp[(3) - (4)].tree));
			  }
    break;

  case 344:
/* Line 1787 of yacc.c  */
#line 1913 "internparser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 345:
/* Line 1787 of yacc.c  */
#line 1917 "internparser.y"
    {
			    (yyval.other) = NULL;
			  }
    break;

  case 346:
/* Line 1787 of yacc.c  */
#line 1924 "internparser.y"
    {
			    (yyval.tree) = makePrecDeref();
			  }
    break;

  case 347:
/* Line 1787 of yacc.c  */
#line 1928 "internparser.y"
    {
			    (yyval.tree) = makePointsDeref();
			  }
    break;

  case 348:
/* Line 1787 of yacc.c  */
#line 1932 "internparser.y"
    {
			    (yyval.tree) = makeDiamDeref();
			  }
    break;

  case 349:
/* Line 1787 of yacc.c  */
#line 1936 "internparser.y"
    {
			    (yyval.tree) = makeDisplayDeref();
			  }
    break;

  case 350:
/* Line 1787 of yacc.c  */
#line 1940 "internparser.y"
    {
			    (yyval.tree) = makeVerbosityDeref();
			  }
    break;

  case 351:
/* Line 1787 of yacc.c  */
#line 1944 "internparser.y"
    {
			    (yyval.tree) = makeCanonicalDeref();
			  }
    break;

  case 352:
/* Line 1787 of yacc.c  */
#line 1948 "internparser.y"
    {
			    (yyval.tree) = makeAutoSimplifyDeref();
			  }
    break;

  case 353:
/* Line 1787 of yacc.c  */
#line 1952 "internparser.y"
    {
			    (yyval.tree) = makeTaylorRecursDeref();
			  }
    break;

  case 354:
/* Line 1787 of yacc.c  */
#line 1956 "internparser.y"
    {
			    (yyval.tree) = makeTimingDeref();
			  }
    break;

  case 355:
/* Line 1787 of yacc.c  */
#line 1960 "internparser.y"
    {
			    (yyval.tree) = makeFullParenDeref();
			  }
    break;

  case 356:
/* Line 1787 of yacc.c  */
#line 1964 "internparser.y"
    {
			    (yyval.tree) = makeMidpointDeref();
			  }
    break;

  case 357:
/* Line 1787 of yacc.c  */
#line 1968 "internparser.y"
    {
			    (yyval.tree) = makeDieOnErrorDeref();
			  }
    break;

  case 358:
/* Line 1787 of yacc.c  */
#line 1972 "internparser.y"
    {
			    (yyval.tree) = makeRationalModeDeref();
			  }
    break;

  case 359:
/* Line 1787 of yacc.c  */
#line 1976 "internparser.y"
    {
			    (yyval.tree) = makeSuppressWarningsDeref();
			  }
    break;

  case 360:
/* Line 1787 of yacc.c  */
#line 1980 "internparser.y"
    {
			    (yyval.tree) = makeHopitalRecursDeref();
			  }
    break;

  case 361:
/* Line 1787 of yacc.c  */
#line 1987 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = CONSTANT_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 362:
/* Line 1787 of yacc.c  */
#line 1993 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = FUNCTION_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 363:
/* Line 1787 of yacc.c  */
#line 1999 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = RANGE_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 364:
/* Line 1787 of yacc.c  */
#line 2005 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = INTEGER_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 365:
/* Line 1787 of yacc.c  */
#line 2011 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = STRING_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 366:
/* Line 1787 of yacc.c  */
#line 2017 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = BOOLEAN_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 367:
/* Line 1787 of yacc.c  */
#line 2023 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = CONSTANT_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 368:
/* Line 1787 of yacc.c  */
#line 2029 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = FUNCTION_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 369:
/* Line 1787 of yacc.c  */
#line 2035 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = RANGE_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 370:
/* Line 1787 of yacc.c  */
#line 2041 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = INTEGER_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 371:
/* Line 1787 of yacc.c  */
#line 2047 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = STRING_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 372:
/* Line 1787 of yacc.c  */
#line 2053 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = BOOLEAN_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 373:
/* Line 1787 of yacc.c  */
#line 2061 "internparser.y"
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = VOID_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
    break;

  case 374:
/* Line 1787 of yacc.c  */
#line 2067 "internparser.y"
    {
			    (yyval.integerval) = (yyvsp[(1) - (1)].integerval);
		          }
    break;

  case 375:
/* Line 1787 of yacc.c  */
#line 2074 "internparser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].integerval));
			  }
    break;

  case 376:
/* Line 1787 of yacc.c  */
#line 2078 "internparser.y"
    {
			    (yyval.list) = addElement((yyvsp[(3) - (3)].list), (yyvsp[(1) - (3)].integerval));
			  }
    break;

  case 377:
/* Line 1787 of yacc.c  */
#line 2084 "internparser.y"
    {
			    (yyval.list) = addElement(NULL, (yyvsp[(1) - (1)].integerval));
			  }
    break;

  case 378:
/* Line 1787 of yacc.c  */
#line 2088 "internparser.y"
    {
			    (yyval.list) = (yyvsp[(2) - (3)].list);
			  }
    break;


/* Line 1787 of yacc.c  */
#line 7215 "/mnt/extra/ferrandi/software/panda/trunk/panda/ext/./sollya/internparser.c"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (myScanner, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (myScanner, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval, myScanner);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp, myScanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (myScanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, myScanner);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, myScanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


