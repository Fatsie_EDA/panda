/* A Bison parser, made by GNU Bison 2.7.12-4996.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_FLOPOCOEXPRESSION_MNT_EXTRA_FERRANDI_SOFTWARE_PANDA_TRUNK_PANDA_EXT_FLOPOCO_SRC_FPEXPRESSIONS_EXPRESSIONPARSER_H_INCLUDED
# define YY_FLOPOCOEXPRESSION_MNT_EXTRA_FERRANDI_SOFTWARE_PANDA_TRUNK_PANDA_EXT_FLOPOCO_SRC_FPEXPRESSIONS_EXPRESSIONPARSER_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int FlopocoExpressiondebug;
#endif
/* "%code requires" blocks.  */
/* Line 2053 of yacc.c  */
#line 1 "/mnt/extra/ferrandi/software/panda/trunk/panda/ext/./flopoco/src/FPExpressions/ExpressionParser.y"

	#include "ExpressionParserData.h"
	extern program* p;


/* Line 2053 of yacc.c  */
#line 52 "/mnt/extra/ferrandi/software/panda/trunk/panda/ext/./flopoco/src/FPExpressions/ExpressionParser.h"

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     FPEXPRESSION_UNKNOWN = 258,
     FPEXPRESSION_PV = 259,
     FPEXPRESSION_OUTPUT = 260,
     FPEXPRESSION_LPAR = 261,
     FPEXPRESSION_RPAR = 262,
     FPEXPRESSION_SQR = 263,
     FPEXPRESSION_SQRT = 264,
     FPEXPRESSION_EXP = 265,
     FPEXPRESSION_LOG = 266,
     FPEXPRESSION_PLUS = 267,
     FPEXPRESSION_MINUS = 268,
     FPEXPRESSION_TIMES = 269,
     FPEXPRESSION_DIV = 270,
     FPEXPRESSION_EQUALS = 271,
     FPEXPRESSION_FPNUMBER = 272,
     FPEXPRESSION_VARIABLE = 273
   };
#endif
/* Tokens.  */
#define FPEXPRESSION_UNKNOWN 258
#define FPEXPRESSION_PV 259
#define FPEXPRESSION_OUTPUT 260
#define FPEXPRESSION_LPAR 261
#define FPEXPRESSION_RPAR 262
#define FPEXPRESSION_SQR 263
#define FPEXPRESSION_SQRT 264
#define FPEXPRESSION_EXP 265
#define FPEXPRESSION_LOG 266
#define FPEXPRESSION_PLUS 267
#define FPEXPRESSION_MINUS 268
#define FPEXPRESSION_TIMES 269
#define FPEXPRESSION_DIV 270
#define FPEXPRESSION_EQUALS 271
#define FPEXPRESSION_FPNUMBER 272
#define FPEXPRESSION_VARIABLE 273



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 2053 of yacc.c  */
#line 17 "/mnt/extra/ferrandi/software/panda/trunk/panda/ext/./flopoco/src/FPExpressions/ExpressionParser.y"

	char c_type;
	int i_type;
	double d_type;
	char* s_type;
	node* thisNode;         // each node is either a leaf or an assignement
	nodeList* thisNodeList; //list containing the assignments
	varList*  thisVarList; //list containing the assignments
	program* theProgram; //assignment list + output variables


/* Line 2053 of yacc.c  */
#line 115 "/mnt/extra/ferrandi/software/panda/trunk/panda/ext/./flopoco/src/FPExpressions/ExpressionParser.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE FlopocoExpressionlval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int FlopocoExpressionparse (void *YYPARSE_PARAM);
#else
int FlopocoExpressionparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int FlopocoExpressionparse (void);
#else
int FlopocoExpressionparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_FLOPOCOEXPRESSION_MNT_EXTRA_FERRANDI_SOFTWARE_PANDA_TRUNK_PANDA_EXT_FLOPOCO_SRC_FPEXPRESSIONS_EXPRESSIONPARSER_H_INCLUDED  */
